cmake_minimum_required(VERSION 3.16)

project(face-recognition
    VERSION 0.0.1
    LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

find_package(Qt6 REQUIRED COMPONENTS Core Gui Widgets)
find_package(OpenCV REQUIRED)
# qt_standard_project_setup()
SET(CMAKE_AUTOMOC TRUE)
SET(CMAKE_AUTOUIC TRUE)

add_library(face-id
    src/core/fr_core.cpp
    src/face_tracking/kalman_filter.cpp
    src/face_tracking/face_tracking.cpp
    src/insightface/comm_lib.cpp
    src/insightface/mxnet_mtcnn.cpp
    src/insightface/insightface_preprocess.cpp
    src/insightface/insightface_algorithm.cpp
)

target_include_directories(face-id PUBLIC
    include
    include/detector
    include/vlfeat
    include/core
    include/face_detection
    include/face_tracking
    include/mxnet_include
    include/insightface
)

target_link_directories(face-id PUBLIC
    lib/mxnet_libs
    lib/glnxa64
)

target_link_libraries(face-id PUBLIC
    opencv_core opencv_calib3d opencv_highgui opencv_imgcodecs opencv_imgproc opencv_objdetect opencv_video opencv_videoio opencv_videostab opencv_features2d opencv_face opencv_tracking opencv_dnn
    mxnet mklml_intel mkldnn iomp5 quadmath
)

add_executable(${PROJECT_NAME}
    main.cpp
    mainwindow.cpp
    mainwindow.ui
)

target_link_libraries(${PROJECT_NAME} PRIVATE
    face-id
    Qt6::Core Qt6::Gui Qt6::Widgets
)
