QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += c++11
TARGET = Face_recognition
TEMPLATE = app
DESTDIR = $$PWD

INCLUDEPATH += /usr/include/boost\
               $$PWD/include\
               $$PWD/include/detector\
               $$PWD/include/vlfeat\
               $$PWD/include/core\
               $$PWD/include/face_detection\
               $$PWD/include/face_tracking\
               $$PWD/include/mxnet_include\
               $$PWD/include/insightface\
               /usr/include/opencv4\
               /usr/local/include/opencv4

LIBS += -L/usr/local/lib\
	-L/usr/lib\
        -pthread\
        -lX11\
        -ljpeg\
        -lpng\
        -lcblas\
        -llapack\
        -lopencv_core\
        -lopencv_highgui\
        -lopencv_imgcodecs\
        -lopencv_imgproc\
        -lopencv_objdetect\
        -lopencv_video\
        -lopencv_videoio\
        -lopencv_videostab\
        -lopencv_calib3d\
        -lopencv_features2d\
        -lopencv_face\
        -lopencv_calib3d\
        -lopencv_tracking\
        -lopencv_dnn\
        -lboost_chrono\
        -lboost_system\
        -lboost_thread\
        -lboost_timer\
        -L$$PWD/lib/mxnet_libs\
        -L$$PWD/lib/glnxa64\
        -lmxnet\
        -lmklml_intel\
        -lmkldnn\
        -liomp5\
        -lquadmath


SOURCES += main.cpp\
        mainwindow.cpp\
        src/core/fr_core.cpp\
        src/face_tracking/kalman_filter.cpp\
        src/face_tracking/face_tracking.cpp\
        src/insightface/comm_lib.cpp\
        src/insightface/mxnet_mtcnn.cpp\
        src/insightface/insightface_preprocess.cpp\
        src/insightface/insightface_algorithm.cpp\


HEADERS  += mainwindow.h\
         include/core/fr_core.h\
         include/face_tracking/kalman_filter.h\
         include/face_tracking/face_tracking.h\
         include/insightface/comm_lib.hpp\
         include/insightface/mtcnn.hpp\
         include/insightface/mxnet_mtcnn.hpp\
         include/insightface/mxnet_extract.hpp\
         include/insightface/insightface_preprocess.hpp\
         include/insightface/insightface_algorithm.hpp\

FORMS    += mainwindow.ui

