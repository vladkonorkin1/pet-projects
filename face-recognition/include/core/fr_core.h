﻿#ifndef FACE_RECOGNITION_CORE_H
#define FACE_RECOGNITION_CORE_H

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

#include <stdio.h>
#include <iostream>

#include "insightface_algorithm.hpp"

struct Gallery
{
    int ID;
    bool staff;
    std::string name;
    std::string path_to_imgs;
    std::vector<cv::Mat> faces;
    std::vector<cv::Mat> features;
};

class FaceRecognitionCore
{
    public:
        /** Constructor*/
        FaceRecognitionCore();

        /** Destructor */
        ~FaceRecognitionCore();

        /** Determination count of users in database (gallery)
          - Input parameters: no
          - Output parameter: count of users in database */
        int get_gallery_size();

        /** Performing a search for queried user in database (gallery)
          - Input parameters: image of normalized face
          - Output parameter: Id of user in database; if user is not exist in database, output Id will be 0  */
        int search_in_database(const cv::Mat& face_prepr);

        /** Addition new user to database (gallery)
          - Input parameters: normalized vector of face images; name of the user; belonging user staff or guest
          - Output parameter: Id of new user in database  */
        int add_new_user(const std::vector<cv::Mat>& user_faces, const std::string user_name, const bool staff);

        /** Deleting user from database with Id
          - Input parameters: Id of user which is necessary to delete
          - Output parameter: count of users in database  */
        int unregister_user_id(const int id_unregist);

    private:
        std::ofstream file_records;
        std::vector<Gallery> gallery;
        std::vector<cv::Mat> query_background;
        int ID_last;
        double FAR_MAX;
        double TAU_INSIGHTFACE;
        std::vector<InsightfaceAlgo> fr_inface_vec;
        InsightfaceAlgo inface;
        int n_unit;

        double get_score(const cv::Mat& samp_gallery, const cv::Mat& samp_query, const int dist_type);

        std::string get_time();
};

#endif  /* FACE_RECOGNITION_CORE_H */

