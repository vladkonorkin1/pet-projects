﻿/*
    Face tracking

    Author Alexandr Shusharin
    Date   12.08.2022
*/

#include <opencv2/opencv.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>

#include "kalman_filter.h"

#ifndef FACE_TRACKING_H
#define FACE_TRACKING_H

struct ObjectTracking
{
    cv::Rect2f rect_src;
    std::vector<cv::Rect2f> all_rects;
    unsigned int last_detected_frame;
    cv::Point2f center_src;
    float last_time_detect;
    int count_detection;
    KalmanObject kf;
    bool detected;
    int id;

    ObjectTracking()
    {
        last_detected_frame = -1;
        count_detection = 0;
        detected = true;
        id = -1;
    }
};

struct FaceTrackData
{
    cv::Rect2f rect_src;
    int id;

    FaceTrackData()
    {
        id = -1;
    }
};

class FaceTracking
{
    public:
        /** Constructor*/
        FaceTracking();

        /** Destructor */
        ~FaceTracking();

        void tracking(const std::vector<cv::Rect2f>& rects_faces,
                      const unsigned int num_frame, cv::Mat& img_show);

        std::vector<FaceTrackData> faces_data;

    private:
        std::vector<ObjectTracking> objects;
        float TIME_DISAP;
        int MIN_DETECTION_COUNT;
        float DIST_COEF_HEIGHT;
        int ID_obj;

        void add_object(const cv::Rect2f rect_src, const unsigned int num_frame);

        void update_object(ObjectTracking& obj, const cv::Rect2f rect_src, const unsigned int num_frame);

        void check_objects_absence();

        void draw_objects();

        void check_objects(const cv::Rect2f rect_face, const unsigned int num_frame);

        void kalman_filter();

        void check_detection(const unsigned int num_frame);

        cv::Rect2f get_mean_rect(const std::vector<cv::Rect2f>& rects, const cv::Point2f center, const int frames_num);

        cv::Rect2f get_median_rect(const std::vector<cv::Rect2f>& rects, const cv::Point2f center, const int frames_num);

        cv::Point2f get_rect_center(const cv::Rect2f rect_src);

        void add_faces_data();

};

#endif  /* FACE_TRACKING_H */
