﻿/*
    Kalman Filter class

    Authors Alexandr Shusharin
    Date   12.08.2022
*/

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#ifndef KALMAN_OBJECT_H
#define KALMAN_OBJECT_H

class KalmanObject
{
    public:
        /** Constructor*/
        KalmanObject();

        /** Destructor */
        ~KalmanObject();

        cv::Point predict_kalman(const cv::Point& point_src);

    private:
        int stateSize;
        int measSize;
        int contrSize;
        unsigned int F_type;
        cv::KalmanFilter KF;
        cv::Mat state;
        cv::Mat meas;
        double ticks;
        int call_num;
        int FRAME_COV_MAT;
};

#endif  /* KALMAN_OBJECT_H */

