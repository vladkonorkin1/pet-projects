﻿#ifndef INSIGHT_FACE_H
#define INSIGHT_FACE_H

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>
#include <mxnet/c_predict_api.h>
#include <math.h>

#include "mxnet_mtcnn.hpp"
#include "mxnet_extract.hpp"
#include "comm_lib.hpp"

class InsightfaceAlgo
{
    public:
        /** Constructor*/
        InsightfaceAlgo();

        /** Destructor */
        ~InsightfaceAlgo();

        Mat get_insightface_descriptor(const Mat& img_src);

    private:
        Mxnet_extract extract;

};

#endif  /* INSIGHT_FACE_H */
