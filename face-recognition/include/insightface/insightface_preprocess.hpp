﻿#ifndef INSIGHTFACE_REPR_H
#define INSIGHTFACE_REPR_H

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>
#include <mxnet/c_predict_api.h>
#include <math.h>

#include "mxnet_mtcnn.hpp"
#include "mxnet_extract.hpp"
#include "comm_lib.hpp"

class InsightfacePrepr
{
    public:
        /** Constructor*/
        InsightfacePrepr();

        /** Destructor */
        ~InsightfacePrepr();

        Mat insightface_preprocess(const Mat& img_src, const face_box& face_info);

    private:

        cv::Mat meanAxis0(const cv::Mat &src);

        cv::Mat elementwiseMinus(const cv::Mat &A, const cv::Mat &B);

        cv::Mat varAxis0(const cv::Mat &src);

        int MatrixRank(cv::Mat M);

        cv::Mat similarTransform(cv::Mat src, cv::Mat dst);

};

#endif  /* INSIGHTFACE_REPR_H */
