﻿#include "mainwindow.h"
#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    std::string video_file = "";
    if (argc > 1)
        video_file = argv[1];

    MainWindow w(video_file);

    w.show();

    return a.exec();
}
