﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtCore>
#include <QFileDialog>
#include <QtWidgets>
#include <QVBoxLayout>
#include <QPushButton>

#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

extern "C" {
#include "vl/generic.h"
}

#include <thread>
#include <time.h>

using namespace cv;
using namespace std;

MainWindow::MainWindow(std::string video_file, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Open OpenCV capture
    if (video_file.empty())
    {
        cout << "Capture from the camera" << endl;
        capture.open(0);
        video_file_exist = false;
    }
    else
    {
        cout << "Capture from the video file: " << video_file << endl;
        capture.open(video_file);
        video_file_exist = true;
    }

    // Check if sucessful
    if (!capture.isOpened() == true)
        return;

    feat_algo = INSIGHTFACE;     // Feature extraction algorithm: INSIGHTFACE
    MAX_COLLECT_BASE = 50;       // Max count of images in collection database
    MIN_COUNT_VOTES = 3;         // Minimum number of votes for deciding whether to belong test subject to the gallery
    COUNT_TRIGGER_ALERT = 4;     // Threshold at which persons are considered unrecognized and the alarm is triggered
    TIME_DISAP = 1;    // 1          // Time after the disappearance of a certain person, in sec.
    BACK_TIME_COUNTER = 0;       // Seconds before the face snapshot
    COUNT_FLASH_FRAMES = 3;      // Flash frames count for the face snapshot
    COUNT_SECONDS_SHOW_FACE = 1; // Show this count of seconds the face image in screen after snapshot

    // Define face detection parameters for different algorithms
    face_detect_algo = MTCNN_FD;
    mtcnn.LoadModule("data/mtcnn_model");

    TakeFacePicture = false;
    RegisterUser = false;
    UnregisterUser = false;
    ShowFacePicture = false;
    DeleteFacePicture = false;
    face_detected_status = "0";
    user_id_status = "Unrecognized";
    last_registered_user_id = 0;
    frame_number = 0;
    RegistrationCheckBox = false;
    count_face_picture = 0;
    num_face_screen = 0;
    last_id = 0;
    saved_last_id = false;
    sum_time = 0;
    mean_time = 0;
    count_time = 0;

    capture.read(cvimg);

    string name_icon_img = "data/icon-camera.jpg";
    read_icon_img(name_icon_img, img_icon_dark, img_icon_bright);
    img_bg = Mat(cvimg.size(), CV_8UC3, Scalar(0,0,0));

    // Get size of gallery (database) from the server
    gallery_size = fr_core.get_gallery_size();

    MAX_COUNT_USERS_IN_FRAME = 10;
    for (int i = 0; i < MAX_COUNT_USERS_IN_FRAME; i++)
    {
        InsightfacePrepr pre_inight_single;
        pre_inight_vec.push_back(pre_inight_single);
    }
    n_unit_pre = -1;

    // Timer for UI responsivness
    tmrTimer = new QTimer(this);
    connect(tmrTimer,SIGNAL(timeout()),this,SLOT(ProcessingGUI()));
    tmrTimer->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::collecting_faces(const Mat& img_face, std::vector<FaceImageAngles>& imgs_faces, double& old_time, const Vec3f& face_angles)
{
    double current_time = (double)getTickCount();

    if (imgs_faces.size() < MAX_COLLECT_BASE)
    {
        add_img_angle(img_face, face_angles, imgs_faces);
    }
    else
    {
        imgs_faces.erase(imgs_faces.begin() + 0); // Delete first element
        add_img_angle(img_face, face_angles, imgs_faces);
    }

    old_time = current_time;
}

void MainWindow::add_img_angle(const Mat& img_face, const Vec3f& face_angles, std::vector<FaceImageAngles>& imgs_faces)
{
    FaceImageAngles img_angle;
    img_angle.img = img_face;
    img_angle.angles = face_angles;
    imgs_faces.push_back(img_angle);
    //cout << "imgs_faces size = " << imgs_faces.size() << endl;
}

void MainWindow::add_all_faces(const std::vector<FaceImageAngles>& all_imgs_faces, std::vector<Mat>& faces_for_database)
{
    if (all_imgs_faces.size() > 0)
        for (int i = 0; i < all_imgs_faces.size(); i++)
            faces_for_database.push_back(all_imgs_faces[i].img);
}

void MainWindow::register_new_user(const bool staff)
{
    cout << "+++++ Register new user +++++" << endl;

    for (auto it = faces_data.begin(), end = faces_data.end(); it != end;)
    {
        // Selection faces for database from imgs_faces vector       
        std::vector<Mat> faces_for_database;
        add_all_faces(it->second.imgs_faces, faces_for_database);

        if (faces_for_database.size() > 0) // 1
        {
            // Get name of user from UI
            string user_name = "User";
            int id_database = fr_core.add_new_user(faces_for_database, user_name, staff);

            last_registered_user_id = id_database;
        }

        //#define SHOW_REGISTER_USER

        #ifdef SHOW_REGISTER_USER
            cout << "imgs_faces.size() = " << it->second.imgs_faces.size() << endl;
            cout << "step = " << step << endl;
            cout << "faces_for_database size = " << faces_for_database.size() << endl;
        #endif

        ++it;
    }

    gallery_size = fr_core.get_gallery_size();

    if (faces_data.size() > 0)
    {
        usleep(1000000);    // 1 sec delay
        faces_data.clear();
    }

    RegistrationCheckBox = false;
    RegisterUser = false;
}

void MainWindow::check_person_absence()
{
    if (faces_data.size() > 0 && !RegistrationCheckBox)
        for (auto it = faces_data.begin(), end = faces_data.end(); it != end;)
        {
            double currentTime = getTickCount() / getTickFrequency();

            // In absence of a person more than a certain (TIME_DISAP) time
            if (currentTime - it->second.lastTime > TIME_DISAP)
                faces_data.erase(it++);
            else
                ++it;
        }
}

bool MainWindow::features_extraction_comparison(const int id)
{
    faces_data[id].new_thread = false;

   // #define SHOW_TIME_USING
    #ifdef SHOW_TIME_USING
        double t1 = (double)getTickCount();
    #endif

    // Preprocessing images
    Mat face_prepr, face_rgb, face_gray, img_face_ref;
    std::vector<Point2f> points_face_ref;

    n_unit_pre++;
    if (n_unit_pre >= MAX_COUNT_USERS_IN_FRAME)
        n_unit_pre = 0;

    if (faces_data[id].defined_id == 0 || RegistrationCheckBox)
    {
        //cout << "Preprocessing" << endl;
        if (feat_algo == INSIGHTFACE)
        {
            face_prepr = pre_inight_vec[n_unit_pre].insightface_preprocess(frame, faces_data[id].face_info);
        }
    }

    #ifdef SHOW_TIME_USING
        t1 = ((double)getTickCount() - t1)/getTickFrequency();
        cout << "time1 [s]: " << t1/1.0 << "   id = " << id << endl;
    #endif

    if (RegistrationCheckBox)
    {
        if (TakeFacePicture)
        {
            faces_data[id].time_start = time(0);
            TakeFacePicture = false;
        }

        if (faces_data[id].time_start > 0)
        {
            faces_data[id].time_dif = (BACK_TIME_COUNTER + 1) - difftime(time(0), faces_data[id].time_start);

            if (faces_data[id].time_dif == 0)
                faces_data[id].zero_counter++;

            if (faces_data[id].zero_counter == 3 || faces_data[id].time_dif == -1)
            {
                // Collecting images in vector
                collecting_faces(face_prepr, faces_data[id].imgs_faces, faces_data[id].old_time, faces_data[id].face_angles);
                faces_data[id].time_start = -1;
                faces_data[id].added_new_face_image = true;
                faces_data[id].zero_counter = 0;
            }
        }
    }

    if (faces_data[id].defined_id == 0)
    {
        if (gallery_size > 0 && !RegistrationCheckBox)
        {
            int id_database = fr_core.search_in_database(face_prepr);
            cout << "id_database: " << id_database << "   for user = " << id << "\n" << endl;

            if (id_database != 0)
            {
                faces_data[id].staff = true;
                faces_data[id].recog_id = abs(id_database);
                faces_data[id].similarity_person.push_back(abs(id_database));
            }
            else    // Unknown
            {
                faces_data[id].recog_id = 0;
                faces_data[id].similarity_person.push_back(0);
            }
        }
        else
            faces_data[id].recog_id = 0;

        // Determination of person belonging by voting method
        check_voting(id);
    }

    faces_data[id].new_thread = true;

    return true;
}

void MainWindow::check_voting(const int id)
{
    int voice_max = 0;          // Maximum number of votes among gallery
    int id_won = 0;             // Identifier that has received more votes from the gallery
    int count_not_recog = 0;

  //  #define SHOW_SIMILARITY

    #ifdef SHOW_SIMILARITY
       cout << "similarity_person: ";
       for (int i = 0; i < faces_data[id].similarity_person.size(); i++)
           cout << faces_data[id].similarity_person[i] << " ";
       cout << endl;
    #endif

    // If there are passes on the threshold
    if (faces_data[id].similarity_person.size() > 0 && gallery_size > 0)
    {
        int max_id = INT_MIN;
        for (int i = 0; i < faces_data[id].similarity_person.size(); i++)
        {
            if (faces_data[id].similarity_person[i] > max_id)
                max_id = faces_data[id].similarity_person[i];

            if (faces_data[id].similarity_person[i] == 0)
                count_not_recog++;
        }

        if (max_id > 0)
        {
            std::vector<int> counter_id(max_id, 0);

            for (int i = 0; i < faces_data[id].similarity_person.size(); i++)
            {
                int id_predict = faces_data[id].similarity_person[i];
                if (id_predict > 0 && id_predict <= max_id)
                    counter_id[id_predict-1]++;
            }

            for (int c = 0; c < counter_id.size(); c++)
            {
                if (counter_id[c] > voice_max)
                {
                    voice_max = counter_id[c];
                    id_won = c + 1;
                }
            }
        }
    }

    if (voice_max >= MIN_COUNT_VOTES && faces_data[id].defined_id >= 0)
    {
        faces_data[id].person_defined = true;  // Person is in the gallery
        faces_data[id].defined_id = id_won;
    }

    if (!faces_data[id].person_defined)
    {
        if (count_not_recog >= COUNT_TRIGGER_ALERT)
            faces_data[id].defined_id = -1;
    }

  //  #define SHOW_VOTING

    #ifdef SHOW_VOTING
        cout << "similarity_person size  = " << faces_data[id].similarity_person.size() << endl;
        cout << "voice_max = " << voice_max << endl;
        cout << "count_not_recog = " << count_not_recog << endl;
        cout << "defined_id = " << faces_data[id].defined_id << endl;
    #endif
}

void MainWindow::draw_id(const int i, const int id)
{
    // (checkBox)
    {
        Rect2f face_rect = faces_data[id].face_rect;

        //cout << "Face rect size: " << face_rect.width << " x " << face_rect.height << " pixels" << endl;

        if (faces_data[id].person_defined && !RegistrationCheckBox)
        {
            cv::rectangle(img_show, face_rect, Scalar(0, 250, 0), 2);

            putText(img_show, "Id: " + to_string(faces_data[id].defined_id), Point(face_rect.x, face_rect.y - 5), FONT_HERSHEY_TRIPLEX, 0.85, Scalar(0, 255, 0), 2);
        }
        else if (gallery_size > 0 && !RegistrationCheckBox)
        {
            if (faces_data[id].defined_id < 0)
            {
                cv::rectangle(img_show, face_rect, Scalar(0, 0, 255), 2);
                putText(img_show, "Alert (unrecognized face)", Point(face_rect.x, face_rect.y - 5), FONT_HERSHEY_TRIPLEX, 0.85, Scalar(0, 0, 255), 2);
            }
            else
            {
                cv::rectangle(img_show, face_rect, Scalar(0, 255, 255), 2);
                putText(img_show, "Face checking", Point(face_rect.x, face_rect.y - 5), FONT_HERSHEY_TRIPLEX, 0.85, Scalar(0, 255, 255), 2);
            }
        }
        else if (gallery_size == 0 && !RegistrationCheckBox)
        {
            cv::rectangle(img_show, face_rect, Scalar(255, 255, 255), 2);
            putText(img_show, "Database of persons is empty", Point(face_rect.x, face_rect.y - 5), FONT_HERSHEY_TRIPLEX, 0.85, Scalar(255, 255, 255), 2);
        }
        else
        {
            cv::rectangle(img_show, face_rect, Scalar(255, 255, 255), 2);
            putText(img_show, "Registration", Point(face_rect.x, face_rect.y - 5), FONT_HERSHEY_TRIPLEX, 0.85, Scalar(255, 255, 255), 2);
        }
    }
}

void MainWindow::unregister_user()
{
    cout << "----- Unregister user -----" << endl;

    gallery_size = fr_core.unregister_user_id(last_registered_user_id);
    //cout << "gallery_size = " << gallery_size << endl;

    if (faces_data.size() > 0)
    {
        usleep(1000000);    // 1 sec delay
        faces_data.clear();
    }

    UnregisterUser = false;
    RegistrationCheckBox = false;
}

void MainWindow::read_icon_img(const string name_icon_img, Mat& img_icon_dark, Mat& img_icon_bright)
{
    Mat img_icon = imread(name_icon_img, IMREAD_GRAYSCALE);

    int side = 50;
    Mat img_icon_std(side,side, CV_8UC1, Scalar(0));
    cv::resize(img_icon, img_icon_std, img_icon_std.size());

    cv::threshold(img_icon_std, img_icon_std, 0, 255, THRESH_BINARY | THRESH_OTSU);

    Mat img_icon_rgb_dark(img_icon_std.size(), CV_8UC3, Scalar(0, 0, 255));
    Mat img_icon_rgb_bright(img_icon_std.size(), CV_8UC3, Scalar(0, 0, 255));

    for (int y = 0; y < img_icon_std.rows; y++)
    {
        for (int x = 0; x < img_icon_std.cols; x++)
        {
            int val = (int)img_icon_std.at<uchar>(y,x);
            if (val == 0)
            {
                img_icon_rgb_dark.at<cv::Vec3b>(y,x)[0] = 180;
                img_icon_rgb_dark.at<cv::Vec3b>(y,x)[1] = 180;
                img_icon_rgb_dark.at<cv::Vec3b>(y,x)[2] = 255;

                img_icon_rgb_bright.at<cv::Vec3b>(y,x)[0] = 255;
                img_icon_rgb_bright.at<cv::Vec3b>(y,x)[1] = 255;
                img_icon_rgb_bright.at<cv::Vec3b>(y,x)[2] = 255;
            }
        }
    }

    img_icon_rgb_dark.copyTo(img_icon_dark);
    img_icon_rgb_bright.copyTo(img_icon_bright);
}

void MainWindow::show_time_counter(const int id, const int count_faces_in_screen)
{
    if (faces_data[id].time_start > 0 && count_faces_in_screen == 1)
    {
        faces_data[id].time_counter = true;

        int cols = 640;
        int rows = 50;
        Mat img_strip_std(rows, cols, CV_8UC3, Scalar(0, 0, 255));

        int start_pos = cols * 0.5; // 0.25
        int step = (cols - start_pos * 2) / (BACK_TIME_COUNTER + 1);
        int n = 0;
        Scalar color_bright = Scalar(255, 255, 255);
        Scalar color_dark = Scalar(180, 180, 255);

        for (int t = BACK_TIME_COUNTER; t >= 0; t--)
        {
            Scalar color;
            bool bright = false;
            if (t == faces_data[id].time_dif)
            {
                color = color_bright;
                bright = true;
            }
            else
                color = color_dark;

            if (t != 0)
            {
                putText(img_strip_std, to_string(t), Point(start_pos + step * n, 40), FONT_HERSHEY_DUPLEX, 1.5, color, 3, LINE_AA);
            }
            else
            {
                if (img_icon_bright.rows <= img_strip_std.rows && img_icon_bright.cols <= img_strip_std.cols)
                {
                    if (bright)
                        img_icon_bright.copyTo(img_strip_std(Rect(start_pos + step * n,0,img_icon_bright.cols,img_icon_bright.rows)));
                    else
                        img_icon_dark.copyTo(img_strip_std(Rect(start_pos + step * n,0,img_icon_dark.cols,img_icon_dark.rows)));
                }
            }

            n++;
        }

        float coef_strip = 0.1;
        Mat img_strip(img_show.rows * coef_strip, img_show.cols, img_show.type());
        cv::resize(img_strip_std, img_strip, img_strip.size());
        img_strip.copyTo(img_show(Rect(0,img_show.rows-img_strip.rows,img_show.cols,img_strip.rows)));
    }
}

void MainWindow::show_faces_in_strip(const int id)
{
    if (faces_data[id].imgs_faces.size() > 0 && !faces_data[id].time_counter &&
        !ShowFacePicture && !faces_data[id].shown_new_face_image)
    {
        float coef_faces_strip = 0.2;
        int rows_strip = img_show.rows * coef_faces_strip;
        int cols_strip = img_show.cols;

        float count_imgs_in_strip = (float)cols_strip / (rows_strip * faces_data[id].imgs_faces.size());

        if (count_imgs_in_strip < 1)
        {
            while (true)
            {
                coef_faces_strip -= 0.01;
                rows_strip = img_show.rows * coef_faces_strip;
                count_imgs_in_strip = (float)cols_strip / (rows_strip * faces_data[id].imgs_faces.size());
                if (count_imgs_in_strip >= 1)
                    break;
            }
        }

        Mat img_faces_strip(rows_strip, cols_strip, CV_8UC3, Scalar(180, 180, 180));

        for (int i = 0; i < faces_data[id].imgs_faces.size(); i++)
        {
            Mat img_face_std(rows_strip, rows_strip, faces_data[id].imgs_faces[0].img.type());
            cv::resize(faces_data[id].imgs_faces[i].img, img_face_std, img_face_std.size());
            img_face_std.copyTo(img_faces_strip(Rect(i * img_face_std.cols, 0, img_face_std.cols, img_face_std.rows)));
        }

        img_faces_strip.copyTo(img_show(Rect(0,img_show.rows-img_faces_strip.rows,img_faces_strip.cols,img_faces_strip.rows)));
    }
}

void MainWindow::show_faces_in_screen(const int id)
{
    img_bg.copyTo(img_show);

    int rows_face = img_show.rows;
    int cols_face = rows_face;
    int x = img_show.cols / 2 - cols_face / 2;
    num_face_screen = faces_data[id].imgs_faces.size() - count_face_picture;

    if (num_face_screen == faces_data[id].imgs_faces.size())
        num_face_screen = faces_data[id].imgs_faces.size() - 1;

    if (num_face_screen >= 0)
    {
        Mat img_face_std(rows_face, cols_face, faces_data[id].imgs_faces[num_face_screen].img.type());
        cv::resize(faces_data[id].imgs_faces[num_face_screen].img, img_face_std, img_face_std.size());

        img_face_std.copyTo(img_show(Rect(x, 0, img_face_std.cols, img_face_std.rows)));

        putText(img_show, to_string(num_face_screen + 1), Point(20, 50), FONT_HERSHEY_DUPLEX, 1.5, Scalar(180,180,180), 3, LINE_AA);
    }
    else
    {
        ShowFacePicture = false;
        count_face_picture = 0;
    }
}

void MainWindow::erase_face_picture(const int id)
{
    if (DeleteFacePicture && faces_data[id].imgs_faces.size() > 0)
    {
        if (!ShowFacePicture)
            num_face_screen = faces_data[id].imgs_faces.size() - 1;

        if (num_face_screen >= 0 && num_face_screen < faces_data[id].imgs_faces.size())
        {
            faces_data[id].imgs_faces.erase(faces_data[id].imgs_faces.begin() + num_face_screen); // Delete first element
            DeleteFacePicture = false;
        }
    }
}

void MainWindow::show_flash(const int id)
{
    if (faces_data[id].added_new_face_image)
    {
        int mid_frame = COUNT_FLASH_FRAMES / 2;
        int max_val = 256;
        int step = max_val / mid_frame;
        int color = (faces_data[id].num_flash + 1) * step;
        if (color >= max_val)
            color = max_val - (color - max_val);

        Mat img_flash(img_show.size(), img_show.type(), Scalar(color,color,color));
        img_show += img_flash;
        faces_data[id].num_flash++;

        if (faces_data[id].num_flash >= COUNT_FLASH_FRAMES)
        {
            faces_data[id].added_new_face_image = false;
            faces_data[id].num_flash = 0;
            faces_data[id].shown_new_face_image = true;
            faces_data[id].time_face_in_screen = time(0);
        }

        faces_data[id].time_counter = false;
    }
}

void MainWindow::show_face_few_seconds(const int id)
{
    if (!faces_data[id].time_counter && faces_data[id].shown_new_face_image)
    {
        int time_dif = difftime(time(0), faces_data[id].time_face_in_screen);

        if (time_dif < COUNT_SECONDS_SHOW_FACE)
            show_faces_in_screen(id);
        else
        {
            faces_data[id].shown_new_face_image = false;
            faces_data[id].time_face_in_screen = 0;
        }
    }
}

void MainWindow::ProcessingGUI()
{  
    if (video_file_exist)
        usleep(30000);

  //  #define SHOW_TIME_CONSUMPTION_IN_FRAME
    #ifdef SHOW_TIME_CONSUMPTION_IN_FRAME
        double t_all = (double)getTickCount();
    #endif

    // Reading frame from camera, video or stream
    Mat img_src;
    capture.read(img_src);    // Or: capture >> img_src;

    frame_number++;

    if (img_src.empty() == true)
        return;

    frame = img_src;

    frame.copyTo(img_show);

    int count_faces;
    std::vector<Rect2f> rects_faces;
    std::vector<face_box> faces_info;

    // Face detection and tracking
    if (face_detect_algo == MTCNN_FD)
    {
        mtcnn.Detect(frame, faces_info);
        for (size_t i = 0; i < faces_info.size(); i++)
            rects_faces.push_back(Rect2f(faces_info[i].x0, faces_info[i].y0,
                                         faces_info[i].x1 - faces_info[i].x0, faces_info[i].y1 - faces_info[i].y0));
    }

    ft.tracking(rects_faces, frame_number, img_show);

    count_faces = ft.faces_data.size();

    // If count faces in the screen more then null
    if (count_faces > 0)
    {
        user_id_status = "";
        for (size_t i = 0; i < count_faces; i++)
        {
            int id = ft.faces_data[i].id;   // Person Id in frame
            faces_data[id].lastTime = getTickCount() / getTickFrequency();
            faces_data[id].face_rect = ft.faces_data[i].rect_src;
            faces_data[id].rects_faces.push_back(ft.faces_data[i].rect_src);

            if (face_detect_algo == MTCNN_FD)
                faces_data[id].face_info = faces_info[i];

            features_extraction_comparison(id);

            draw_id(i, id);

            if (faces_data[id].defined_id > 0)
                user_id_status = to_string(faces_data[id].defined_id);

            // New registration process
            if (RegistrationCheckBox)
            {
                show_time_counter(id, count_faces);
                show_flash(id);
                show_face_few_seconds(id);
                if (ShowFacePicture)
                    show_faces_in_screen(id);
                erase_face_picture(id);
                show_faces_in_strip(id);
            }
        }

        // If buttons "RegisterUser" is pressed and only one person in the screen
        if (RegisterUser && count_faces == 1)
            register_new_user(true);

        if (user_id_status.empty())
            user_id_status = "Unrecognized";
        face_detected_status = to_string(count_faces);
    }
    else
    {
        user_id_status = "No users in view";
        face_detected_status = to_string(0);
    }

    check_person_absence();

    // If button "UnregisterUser" is pressed and if there is at least one person in gallery, unregister user
    if (UnregisterUser && gallery_size > 0)
        unregister_user();

    // Display some status information to UI
    ui->UserID->setText(QString::fromStdString(user_id_status));
    ui->FaceDetected->setText(QString::fromStdString(face_detected_status));
    if (!RegistrationCheckBox)
        ui->checkBox_2->setChecked(false);

    // Changing size of the result image to size of the form window
    Mat img_show_window(ui->DstWindow->height(),ui->DstWindow->width(), img_show.type());
    cv::resize(img_show, img_show_window, img_show_window.size());

    // OpenCV to QImage datatype to display on labels
    cv::cvtColor(img_show_window,img_show_window, COLOR_BGR2RGB);
    QImage qimg((uchar*) img_show_window.data,img_show_window.cols,img_show_window.rows,img_show_window.step,QImage::Format_RGB888); // For color images

    // Update the labels on the form
    ui->DstWindow->setPixmap(QPixmap::fromImage(qimg));

    #ifdef SHOW_TIME_CONSUMPTION_IN_FRAME
        t_all = ((double)getTickCount() - t_all)/getTickFrequency();
        cout << "Time all [s]: " << t_all/1.0 << endl;
        count_time++;
        if (count_time != 1)
        {
            sum_time += t_all/1.0;
            mean_time = sum_time / (count_time - 1);
        }
        cout << "Mean time: " << mean_time << endl;
        cout << endl;
    #endif
}

void MainWindow::on_Exit_clicked()
{
    usleep(1000000);    // 1 sec delay
    exit(0);
    //QApplication::quit();  // Exit the application
}

void MainWindow::on_RegisterUser_clicked()
{
    RegisterUser = true;
}

void MainWindow::on_UnregisterUser_clicked()
{
    UnregisterUser = true;
}

void MainWindow::on_checkBox_2_clicked()
{
    if (ui->checkBox_2->isChecked())
        RegistrationCheckBox = true;
    else
        RegistrationCheckBox = false;
}

void MainWindow::on_TakeFacePicture_clicked()
{
    if (RegistrationCheckBox)
        TakeFacePicture = true;
}

void MainWindow::on_ShowFacePicture_clicked()
{
    if (RegistrationCheckBox)
    {
        ShowFacePicture = true;
        count_face_picture++;
    }
}

void MainWindow::on_DeleteFacePicture_clicked()
{
    if (RegistrationCheckBox)
        DeleteFacePicture = true;
}

