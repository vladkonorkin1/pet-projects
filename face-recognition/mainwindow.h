﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/opencv.hpp>

#include "fr_core.h"
#include <thread>

#include "face_tracking.h"

#include <mxnet/c_predict_api.h>
#include <math.h>
#include "mxnet_mtcnn.hpp"
#include "mxnet_extract.hpp"
#include "comm_lib.hpp"
#include "insightface_algorithm.hpp"
#include "insightface_preprocess.hpp"

struct FaceImageAngles
{
    cv::Mat img;
    cv::Vec3f angles;
};

struct Face_Data
{
    std::vector<FaceImageAngles> imgs_faces;
    std::vector<int> similarity_person;
    std::vector<cv::Rect2f> rects_faces;
    cv::Rect2f face_rect;
    double lastTime;
    bool new_thread;
    std::thread thr;
    double old_time;
    time_t time_start;
    int time_dif;
    int recog_id;
    int id;
    bool staff;
    bool person_defined;
    int defined_id;
    cv::Vec3f face_angles;
    bool added_new_face_image;
    int num_flash;
    int zero_counter;
    bool time_counter;
    time_t time_face_in_screen;
    bool shown_new_face_image;
    face_box face_info;

    Face_Data() : id(0), lastTime(0), old_time(0), recog_id(-1), time_start(-1),
                  new_thread(true), staff(true), person_defined(false), time_dif(0),
                  defined_id(0), num_flash(0), added_new_face_image(false),
                  zero_counter(0), time_counter(false), time_face_in_screen(0),
                  shown_new_face_image(false)
    {
    }
};

typedef enum FeatureAlgorithm
{
    INSIGHTFACE = 1     // Insightface (https://github.com/deepinsight/insightface and https://github.com/njvisionpower/mxnet-insightface-cpp)

} FeatureAlgorithm;

typedef enum FaceDetectAlgo
{
    MTCNN_FD = 1        // Multi-task Cascaded Convolutional Networks (https://kpzhang93.github.io/MTCNN_face_detection_alignment)

} FaceDetectAlgo;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(std::string video_file, QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    cv::VideoCapture capture;
    cv::Mat cvimg;
    std::vector<cv::Rect> faces_rect;
    cv::Mat gray, frame, img_show;
    std::vector<cv::Point2f> points_rect;
    std::map<int, Face_Data> faces_data;
    std::string face_detected_status;
    std::string user_id_status;
    std::vector<InsightfacePrepr> pre_inight_vec;
    int n_unit_pre;
    int feat_algo;
    int face_detect_algo;
    int TIME_DISAP;
    int MAX_COLLECT_BASE;
    int MAX_GALLERY_BASE;
    int MIN_COUNT_VOTES;
    int COUNT_TRIGGER_ALERT;
    int BACK_TIME_COUNTER;
    int COUNT_FLASH_FRAMES;
    int COUNT_SECONDS_SHOW_FACE;
    int gallery_size;
    int last_registered_user_id;
    bool TakeFacePicture;
    bool RegisterUser;
    bool UnregisterUser;
    bool ShowFacePicture;
    bool DeleteFacePicture;
    bool video_file_exist;
    FaceRecognitionCore fr_core;
    int frame_number;
    bool RegistrationCheckBox;
    cv::Mat img_icon_dark, img_icon_bright, img_bg;
    int count_face_picture, num_face_screen;
    int last_id;
    bool saved_last_id;
    int MAX_COUNT_USERS_IN_FRAME;
    float sum_time;
    float mean_time;
    int count_time;
    MxNetMtcnn mtcnn;
    FaceTracking ft;

    void collecting_faces(const cv::Mat& img_face, std::vector<FaceImageAngles>& imgs_faces, double& old_time, const cv::Vec3f& face_angles);
    void check_person_absence();
    void register_new_user(const bool staff);
    bool features_extraction_comparison(const int id);
    void check_voting(const int id);
    void draw_id(const int i, const int id);
    void unregister_user();
    void add_img_angle(const cv::Mat& img_face, const cv::Vec3f& face_angles, std::vector<FaceImageAngles>& imgs_faces);
    void add_all_faces(const std::vector<FaceImageAngles>& all_imgs_faces, std::vector<cv::Mat>& faces_for_database);
    void read_icon_img(const std::string name_icon_img, cv::Mat& img_icon_dark, cv::Mat& img_icon_bright);
    void show_time_counter(const int id, const int count_faces_in_screen);
    void show_flash(const int id);
    void show_faces_in_strip(const int id);
    void show_faces_in_screen(const int id);
    void erase_face_picture(const int id);
    void show_face_few_seconds(const int id);

    QImage qimg;
    QTimer *tmrTimer;

public slots:
    void ProcessingGUI();
private slots:
    void on_Exit_clicked();
    void on_RegisterUser_clicked();
    void on_UnregisterUser_clicked();
    void on_checkBox_2_clicked();
    void on_TakeFacePicture_clicked();
    void on_ShowFacePicture_clicked();
    void on_DeleteFacePicture_clicked();
};

#endif // MAINWINDOW_H

