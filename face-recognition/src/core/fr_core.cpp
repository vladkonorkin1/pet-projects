﻿#include <stdio.h>
#include <iostream>
#include <sys/stat.h>

#ifdef _MSC_VER
#include <direct.h>
#endif

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "fr_core.h"

using namespace std;
using namespace cv;

FaceRecognitionCore::FaceRecognitionCore()
{
    ID_last = 0;
    n_unit = -1;
    FAR_MAX = 0.5;
    TAU_INSIGHTFACE = 0.5;  // Threshold for Insightface algorithm
}

FaceRecognitionCore::~FaceRecognitionCore()
{
}

int FaceRecognitionCore::search_in_database(const Mat& face_prepr)
{
  //  #define SHOW_ENCODE_TIME
    #ifdef SHOW_ENCODE_TIME
        double t1 = (double)getTickCount();
    #endif

    Mat descriptor;
    int dist_type;
    double tau = 0;
    if (face_prepr.rows == 112 && face_prepr.cols == 112)  // INSIGHTFACE
    {
        descriptor = inface.get_insightface_descriptor(face_prepr);

        tau = TAU_INSIGHTFACE;
        dist_type = 3;
    }

    #ifdef SHOW_ENCODE_TIME
        t1 = ((double)getTickCount() - t1)/getTickFrequency();
        cout << "ENCODE_TIME [s]: " << t1/1.0 << endl;
    #endif

    int id_database = 0;    // Id of user in gallery

    // Comparison user with gallery
    if (gallery.size() > 0)
    {
        double min_score_gallery = DBL_MAX;
        int id_win = -1;
        bool member = true;

        for (int i = 0; i < gallery.size(); i++)
        {
            double min_score = DBL_MAX;
            double score;

            for (int j = 0; j < gallery[i].features.size(); j++)
            {
                score = get_score(gallery[i].features[j], descriptor, dist_type);
                if (score < min_score)
                    min_score = score;
            }

            if (min_score < min_score_gallery)
            {
                min_score_gallery = min_score;
                id_win = gallery[i].ID;
                member = gallery[i].staff;
            }
        }

        if (min_score_gallery < tau)
        {
            id_database = id_win;
        }
        else
        {
            id_database = 0;            // Unknown
        }

        #define SHOW_EXTRACTION
        #ifdef SHOW_EXTRACTION
        float similarity = (float)1 / (1 + min_score_gallery) * 100;
         //   cout << "min_score_gallery = " << min_score_gallery << endl;
            cout << "min_score_gallery = " << min_score_gallery << "  " << similarity << "%" << endl;
         //        "   id_win = " << id_win << endl;

            if (min_score_gallery < tau)
                cout << "WIN: " << id_win << endl;
            else
                cout << "Unrecognized user" << endl;
        #endif
    }
    else
        id_database = 0;            // Unknown

    return id_database;
}

int FaceRecognitionCore::add_new_user(const std::vector<Mat>& user_faces, const string user_name, const bool staff)
{
    Gallery new_person;

    // Get descriptors features from face images
    for (int i = 0; i < user_faces.size(); i++)
    {
        Mat descriptor;

        if (user_faces[i].rows == 112 && user_faces[i].cols == 112)  // INSIGHTFACE
        {
            descriptor = inface.get_insightface_descriptor(user_faces[i]);
        }

        if (!descriptor.empty())
            new_person.features.push_back(descriptor);
    }

    // Creation Id for new user
    ID_last++;
    new_person.ID = ID_last;
    new_person.staff = staff;

    if (user_name.empty())
    {
        string user_name_dst= "User" + to_string(ID_last);
        new_person.name = user_name_dst;
    }
    else
        new_person.name = user_name;

    gallery.push_back(new_person);

    return ID_last;
}

int FaceRecognitionCore::get_gallery_size()
{
    return gallery.size();
}

double FaceRecognitionCore::get_score(const Mat& samp_gallery, const Mat& samp_query, const int dist_type)
{
    double score;

    if (samp_gallery.rows != samp_query.rows || samp_gallery.cols != samp_query.cols)
    {
        cout << "Mismatch in size of database descriptor and query" << endl;
        return DBL_MAX;
    }

    if (dist_type == 0)      // Euclidean distance
    {
        score = norm(samp_gallery, samp_query, NORM_L2);
    }
    else if (dist_type == 1) // Sum of absolute differences
    {
        score = norm(samp_gallery, samp_query, NORM_L1);
    }
    else if (dist_type == 2) // Cosine distance 1
    {
        Mat gal = samp_gallery(Rect(0,0,samp_gallery.cols/2,1));
        Mat query = samp_query(Rect(0,0,samp_query.cols/2,1));
        Scalar mul_sum = sum(gal.mul(query));
        score = -mul_sum[0] / (norm(query)*norm(gal)) + 1;
    }
    else if (dist_type == 3) // Cosine distance 2
    {
        Scalar mul_sum = sum(samp_gallery.mul(samp_query));
        score = -mul_sum[0] / (norm(samp_query)*norm(samp_gallery)) + 1;
    }

    return score;
}

string FaceRecognitionCore::get_time()
{
    time_t now = time(0);
    struct tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);

    return buf;
}

int FaceRecognitionCore::unregister_user_id(const int id_unregist)
{
    int num = -1;
    for (int i = 0; i < gallery.size(); i++)
        if (gallery[i].ID == id_unregist)
        {
            num = i;
            break;
        }

    if (num >= 0)
    {
        gallery.erase(gallery.begin() + num);       // Delete user with id_unregist
    }

    return gallery.size();
}

