﻿/*
    Face tracking

    Author Alexandr Shusharin
    Date   12.08.2022
*/

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include <fstream>
#include <sstream>
#include <iostream>

#include "face_tracking.h"

using namespace cv;
using namespace std;

FaceTracking::FaceTracking()
{
    // Parameters initialization
    TIME_DISAP = 1.0;   // 1.0            // Time in seconds after disappearance of the certain object
    MIN_DETECTION_COUNT = 2;        // Assignment ID to object after MIN_DETECTION_COUNT detection
    DIST_COEF_HEIGHT = 0.7;         // Distance coefficient between rectangles belonging to the same object
    ID_obj = 1;
}

FaceTracking::~FaceTracking()
{
}

void FaceTracking::tracking(const vector<Rect2f>& rects_faces, const unsigned int num_frame, Mat& img_show)
{
    // Create or update tracked objects
    for (int i = 0; i < rects_faces.size(); i++)
        check_objects(rects_faces[i], num_frame);

    // Check if object disappeared
    //check_objects_absence();

    // Predict center position of the rect based on Kalman filter
    kalman_filter();

    // Check if face detected
    check_detection(num_frame);

    add_faces_data();

  //  #define SHOW_TRACKING_FACE

    #ifdef SHOW_TRACKING_FACE
        //for (int i = 0; i < rects_faces.size(); i++)
        //    rectangle(img_show, rects_faces[i], Scalar(0,255,255), 2, 1);

        for (int i = 0; i < objects.size(); i++)
        {
            if (objects[i].id > 0 && objects[i].detected)
            {
                rectangle(img_show, objects[i].rect_src, Scalar(0,255,0), 2, 1);

                string id = to_string(objects[i].id);
                string data_str = "id: " + id;
                putText(img_show, data_str, Point(objects[i].rect_src.x, objects[i].rect_src.y - 5),
                        FONT_HERSHEY_DUPLEX, 0.8, Scalar(255,255,255), 2, LINE_AA);
            }
        }

        //imshow("FaceTrackingn", img_show);
        //if (waitKey(1) >= 0)
        //    exit(0);
    #endif
}

void FaceTracking::add_object(const Rect2f rect_src, const unsigned int num_frame)
{
    ObjectTracking obj;
    obj.rect_src = rect_src;
    obj.all_rects.push_back(rect_src);
    obj.center_src = get_rect_center(rect_src);
    obj.count_detection++;
    obj.last_time_detect = getTickCount() / getTickFrequency();
    obj.last_detected_frame = num_frame;
    objects.push_back(obj);
}

void FaceTracking::update_object(ObjectTracking& obj, const Rect2f rect_src, const unsigned int num_frame)
{
    obj.all_rects.push_back(rect_src);
    obj.center_src = get_rect_center(rect_src);
    obj.last_time_detect = getTickCount() / getTickFrequency();
    obj.rect_src = get_mean_rect(obj.all_rects, obj.center_src, 7); // Or: get_median_rect
    obj.last_detected_frame = num_frame;
    obj.count_detection++;

    if (obj.count_detection == MIN_DETECTION_COUNT)
    {
        obj.id = ID_obj;
        ID_obj++;
    }
}

void FaceTracking::check_objects_absence()
{
    double current_time = getTickCount() / getTickFrequency();

    if (objects.size() > 0)
    {
        vector<int> num_objs_erase;

        for (int i = 0; i < objects.size(); i++)
        {
            // In absence of a object more than a certain (TIME_DISAP) time
            if (current_time - objects[i].last_time_detect >= TIME_DISAP)
            {
                num_objs_erase.push_back(i);
            }
        }

        if (num_objs_erase.size() > 0)
        {
            std::sort(num_objs_erase.begin(), num_objs_erase.end());
            for (int idx = num_objs_erase.size()-1; idx >= 0; idx--)
                objects.erase(objects.begin() + num_objs_erase[idx]);

        }
    }
}

void FaceTracking::check_objects(const Rect2f rect_face, const unsigned int num_frame)
{
    if (objects.size() == 0)
        add_object(rect_face, num_frame);
    else
    {
        Point2f center_rect = get_rect_center(rect_face);

        float dif_centers;
        bool new_object = true;
        int num_obj = -1;
        float min_dist = FLT_MAX;

        // Saved objects
        for (int t = 0; t < objects.size(); t++)
        {
            // Euclidean distance between the centers of objects
            dif_centers = norm(center_rect - objects[t].center_src);

            if (dif_centers < min_dist)
            {
                min_dist = dif_centers;
                num_obj = t;
            }
        }

        if (min_dist < DIST_COEF_HEIGHT * rect_face.height)
            new_object = false;

        if (new_object)
            add_object(rect_face, num_frame);
        else
            update_object(objects[num_obj], rect_face, num_frame);
    }
}

void FaceTracking::kalman_filter()
{
    for (int i = 0; i < objects.size(); i++)
    {
        if (objects[i].id > 0)
        {
            Point center_kf = objects[i].kf.predict_kalman(get_rect_center(objects[i].rect_src));
            objects[i].rect_src.x = center_kf.x - objects[i].rect_src.width / 2;
            objects[i].rect_src.y = center_kf.y - objects[i].rect_src.height / 2;
        }
    }
}

void FaceTracking::check_detection(const unsigned int num_frame)
{
    for (int i = 0; i < objects.size(); i++)
    {
        if (objects[i].id > 0)
        {
            if (objects[i].last_detected_frame == num_frame)
                objects[i].detected = true;
            else
                objects[i].detected = false;
        }
    }
}

Point2f FaceTracking::get_rect_center(const Rect2f rect_src)
{
    Point2f center;
    center.x = rect_src.x + rect_src.width / 2;
    center.y = rect_src.y + rect_src.height / 2;

    return center;
}

Rect2f FaceTracking::get_mean_rect(const vector<Rect2f>& rects, const Point2f center, const int frames_num)
{
    Rect2f rect_dst;

    if (rects.size() > 0)
    {
        int start_num = 0;
        if (rects.size() > frames_num)
            start_num = rects.size() - frames_num;

        int mean_width = 0;
        int mean_height = 0;
        int n = 0;
        for (int i = start_num; i < rects.size(); i++)
        {
            mean_width += rects[i].width;
            mean_height += rects[i].height;
            n++;
        }
        if (n > 0)
        {
            mean_width /= n;
            mean_height /= n;
        }

        rect_dst.x = center.x - mean_width / 2;
        rect_dst.y = center.y - mean_height / 2;
        rect_dst.width = mean_width;
        rect_dst.height = mean_height;
    }

    return rect_dst;
}

Rect2f FaceTracking::get_median_rect(const vector<Rect2f>& rects, const Point2f center, const int frames_num)
{
    int start_num = 0;
    if (rects.size() > frames_num)
        start_num = rects.size() - frames_num;

    vector<float> width;
    vector<float> height;
    for (int i = start_num; i < rects.size(); i++)
    {
        width.push_back(rects[i].width);
        height.push_back(rects[i].height);
    }

    std::sort(width.begin(), width.end());
    std::sort(height.begin(), height.end());

    float median_width = width[width.size() / 2];
    float median_height = height[height.size() / 2];

    Rect2f rect_dst;
    rect_dst.x = center.x - median_width / 2;
    rect_dst.y = center.y - median_height / 2;
    rect_dst.width = median_width;
    rect_dst.height = median_height;

    return rect_dst;
}

void FaceTracking::add_faces_data()
{
    faces_data.clear();
    for (int i = 0; i < objects.size(); i++)
    {
        if (objects[i].id > 0 && objects[i].detected)
        {
            FaceTrackData fd;
            fd.rect_src = objects[i].rect_src;
            fd.id = objects[i].id;
            faces_data.push_back(fd);
        }
    }
}
