﻿/*
    Kalman Filter class

    Authors Alexandr Shusharin
    Date   9.08.2022
*/

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <stdio.h>

#include "kalman_filter.h"

using namespace cv;
using namespace std;

KalmanObject::KalmanObject()
{
    stateSize = 4;      // [x, y, v_x, v_y]
    measSize = 2;       // [z_x, z_y] // Measure x and y
    contrSize = 0;      // No control input
    F_type = CV_32F;
    ticks = 0;
    call_num = 0;
    FRAME_COV_MAT = 7;

    // Initiation of OpenCV Kalman Filter
    KF.init(stateSize, measSize, contrSize, F_type);

    // Creating state vector
    state.create(stateSize, 1, F_type); // [x, y, v_x, v_y] // Column Matrix

    // Creating measurement vector
    meas.create(measSize, 1, F_type);   // [z_x, z_y] // Column matrix

    // Transition state matrix A
    // Note: set dT at each processing step!
    // X_k = A*X_k-1
    // X_k = current state := x_k, y_k, v_x_k
    // X_k-1 = previous state
    // A =
    // [1 0 dT 0]
    // [0 1 0 dT]
    // [0 0 1  0]
    // [0 0 0  1]
    // Observe it is an identity matrix with dT inputs that we will provide later
    cv::setIdentity(KF.transitionMatrix);

    // Measurement Matrix (This is C or H matrix)
    // size of C is measSize x stateSize
    // only those values will set which we can get as measurement in a state vector
    // here out of [x, y, v_x and v_y] we can only measure x, y of the coordianates
    // so we set only element "0" and "5".
    // [1 0 0 0]
    // [0 1 0 0]
    KF.measurementMatrix = cv::Mat::zeros(measSize, stateSize, F_type);
    KF.measurementMatrix.at<float>(0) = 1.0f;
    KF.measurementMatrix.at<float>(5) = 1.0f;

    // Process Noise Covariance Matrix = stateSize x stateSize
    //  [Ex 0  0    0]
    //  [0 Ey  0    0]
    //  [0 0 E_v_x  0]
    //  [0 0  0  E_v_y]
    KF.processNoiseCov.at<float>(0) = 1e-1;    // 1e-2 1e-1
    KF.processNoiseCov.at<float>(5) = 1e-1;    // 1e-2 1e-1
    KF.processNoiseCov.at<float>(10) = 1e-2;  // 1e-2 5.0;
    KF.processNoiseCov.at<float>(15) = 1e-2;  // 1e-2 5.0;

    // Measure Noise Covariance Matrix
    cv::setIdentity(KF.measurementNoiseCov, cv::Scalar(1e-7));  // 1e-7 1e-1
}

KalmanObject::~KalmanObject()
{
}

Point KalmanObject::predict_kalman(const Point& point_src)
{
    if (call_num == FRAME_COV_MAT)
        cv::setIdentity(KF.measurementNoiseCov, cv::Scalar(1e-1)); // 5e-2 1e-1
    call_num++;

    double precTick = ticks;
    ticks = (double)cv::getTickCount();

    double dT = (ticks - precTick) / cv::getTickFrequency(); // Seconds

    // >>> Kalman Prediction
    // >>> Matrix A
    KF.transitionMatrix.at<float>(2) = dT;
    KF.transitionMatrix.at<float>(7) = dT;
    // <<< Matrix A

    //std::cout << "dt: " << dT << std::endl;

    state = KF.predict(); // First predict, to update the internal statePre variable
    //std::cout << "State post: " << state << std::endl;

    Point predictPt(state.at<float>(0), state.at<float>(1));
    // <<< Kalman Prediction

    // >>> Passing the measured values to the measurement vector
    meas.at<float>(0) = point_src.x;
    meas.at<float>(1) = point_src.y;
    // <<< Passing the measured values to the measurement vector

    // >>> Kalman Update Phase
    Mat estimated = KF.correct(meas);

    Point statePt(estimated.at<float>(0),estimated.at<float>(1));
    // <<< Kalman Update Phase

    return statePt;
}


