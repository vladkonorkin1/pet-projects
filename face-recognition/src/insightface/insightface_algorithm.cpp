﻿#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>
#include <mxnet/c_predict_api.h>
#include <math.h>

#include "insightface_algorithm.hpp"

using namespace std;
using namespace cv;

InsightfaceAlgo::InsightfaceAlgo()
{
    //cout << "Loading Insightface model" << endl;
    string params = "data/insightface/model-0000.params";
    string json = "data/insightface/model-symbol.json";

    // Load feature extraction module
    extract.LoadExtractModule(params, json, 1, 3, 112, 112);
}

InsightfaceAlgo::~InsightfaceAlgo()
{
}

Mat InsightfaceAlgo::get_insightface_descriptor(const Mat& img_src)
{
    #define SHOW_TIME_CALCULATE
    #ifdef SHOW_TIME_CALCULATE
        double t1 = (double)getTickCount();
    #endif

    Mat descriptor = extract.extractFeature(img_src);

    #ifdef SHOW_TIME_CALCULATE
        t1 = ((double)getTickCount() - t1)/getTickFrequency();
        cout << "Time insightface descriptor extraction [s]: " << t1/1.0 << endl;
    #endif

  //  #define SHOW_DESCRIPTOR_INSIGHTFACE
    #ifdef SHOW_DESCRIPTOR_INSIGHTFACE
        cout << "Descriptor: " << descriptor << endl;
        cout << "Descriptor size: " << descriptor.rows << "x" << descriptor.cols << "\n" << endl;
    #endif

    return descriptor;
}
