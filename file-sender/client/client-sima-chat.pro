TARGET = file-sender

CONFIG -= flat
TEMPLATE = app

release: DESTDIR = tmp/release
debug:   DESTDIR = tmp/debug
OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.ui

QT += widgets gui network concurrent sql
QMAKE_CXXFLAGS += /MP
Debug:CONFIG   += console
#Release:CONFIG += console
Debug:DEFINES += _DEBUG

INCLUDEPATH += src
INCLUDEPATH += ../shared

CONFIG += precompile_header
PRECOMPILED_HEADER = ../shared/base/precomp.h

Release:QMAKE_POST_LINK = copy tmp\release\*.exe bin\release

win32 {
	INCLUDEPATH += $$(QT_INSTALL_DIR)/Tools/OpenSSL/Win_x86/include/
	LIBS += -L$$(QT_INSTALL_DIR)/Tools/OpenSSL/Win_x86/lib/ -llibcrypto
}


RESOURCES = src/ui/resource/project.qrc
RC_FILE = src/ui/resource/myapp.rc

HEADERS =  	src/main_window.h                                               \
			../shared/base/precomp.h                                        \
			../shared/base/config.h                                         \
			../shared/base/timer.h                                          \
			../shared/base/bin_stream.h                                     \
			../shared/base/utility.h                                        \
			../shared/base/log.h                                            \
			../shared/base/single_run.h                                     \
			../shared/base/convert_utility.h                                \
			../shared/base/cryptography.h                                   \
			../shared/network/packet.h                                      \
			../shared/network/packet_reader.h                               \
			../shared/network/net_message.h                                 \
			../shared/network/message_dispatcher.h                          \
			../shared/network/base_client_connection.h                      \
			../shared/network/net_messages.h                                \
			../shared/network/handler_messages.h                            \
			../shared/network/types.h                                       \
			../shared/thread_task.h                                         \
			../shared/thread_task_manager.h                                 \
			../shared/updater/start_updater.h                               \
			src/message_connection.h                                        \
			src/connection.h                                                \
			src/user.h                                                      \
			src/users.h                                                     \
			src/chats.h                                                     \
			src/chat.h                                                      \
			src/chats_widgets/base_move_contacts_widget.h                   \
			src/chats_widgets/recents_widget.h                              \
			src/chats_widgets/favorites_widget.h                            \
			src/chats_widgets/contact_separator.h                           \
			src/chats_widgets/chat_item_widget.h                            \
			src/chats_widgets/chat_item_widgets.h                           \
			src/chats_widgets/chat_item_widgets_manager.h                   \
			src/chats_widgets/chat_item_context_menu.h                      \
			src/chats_widgets/chats_widget.h                                \
			src/chats_widgets/search_contacts_widget.h                      \
			src/chats_widgets/current_chat.h                                \
			src/chats_widgets/chat_item_renamer.h                           \
			src/chats_widgets/chat_elided_text.h                            \
			src/modify_chat_users/dialog_modify_chat_users.h                \
			src/modify_chat_users/chat_user_flow_item.h                     \
			src/modify_chat_users/flow_content_widget.h                     \
			src/modify_chat_users/modify_chat_users.h                       \
			src/modify_chat_users/group_chat_users_actions.h                \
			src/gui_messages/item_message.h                                 \
			src/gui_messages/item_text_message_myself.h                     \
			src/gui_messages/item_text_message_another.h                    \
			src/gui_messages/item_file_message.h                            \
			src/gui_messages/item_file_message_myself.h                     \
			src/gui_messages/item_file_message_another.h                    \
			src/gui_messages/item_notification_error.h                      \
			src/gui_messages/messages_widget.h                              \
			src/gui_messages/messages_widget_manager.h                      \
			src/gui_messages/write_message_widget.h                         \
			src/gui_messages/write_text_edit.h                              \
			src/gui_messages/hover_label.h                                  \
			src/gui_messages/hover_button.h                                 \
			src/gui_messages/item_file_message_buttons.h                    \
			src/gui_messages/chat_top/base_chat_top.h                       \
			src/gui_messages/chat_top/contact_chat_top.h                    \
			src/gui_messages/chat_top/animate_expand_participants.h         \
			src/gui_messages/chat_top/group_chat_top.h                      \
			src/gui_messages/chat_top/group_chat_top_user_context_menu.h    \
			src/notification_message/notification_message.h                 \
			src/notification_message/notification_message_widget.h          \
			src/notification_message/notification_message_smooth_show.h     \
			src/notification_message/notification_message_smooth_expand.h   \
			src/notification_message/notification_message_edge.h            \
			src/notification_message/notification_message_button_close.h    \
			src/common_gui/flow_layout.h                                    \
			src/test.h                                                      \
			src/check_readed_new_messages.h                                 \
			src/chat_message.h                                              \
			src/database/database.h                                         \
			src/database/database_utility.h                                 \
			src/database/database_properties.h                              \
			src/database_update_manager.h                                   \
			src/shell.h                                                     \
			src/application.h                                               \
			src/tray_icon.h                                                 \
			src/loader_chat_messages.h                                      \
			src/search_users.h                                              \
			src/search_user_worker.h                                        \
			src/user_info_for_search.h                                      \
			src/sending_messages.h                                          \
			src/file_transfer/file_connection.h                             \
			src/file_transfer/file_dialog.h                                 \
			src/file_transfer/file_transfer.h                               \
			src/file_transfer/file_transfer_manager.h                       \
			src/file_transfer/file_transfer_thread.h                        \
			src/file_transfer/task_scan_folder.h                            \
			src/login_dialog.h                                              \
			src/connection_manager.h                                        \

SOURCES = 	src/main.cpp                                                    \
			src/main_window.cpp                                             \
			../shared/base/config.cpp                                       \
			../shared/base/timer.cpp                                        \
			../shared/base/utility.cpp                                      \
			../shared/base/log.cpp                                          \
			../shared/base/single_run.cpp                                   \
			../shared/base/convert_utility.cpp                              \
			../shared/base/cryptography.cpp                                 \
			../shared/network/packet.cpp                                    \
			../shared/network/packet_reader.cpp                             \
			../shared/network/net_message.cpp                               \
			../shared/network/message_dispatcher.cpp                        \
			../shared/network/base_client_connection.cpp                    \
			../shared/network/net_messages.cpp                              \
			../shared/thread_task_manager.cpp                               \
			../shared/updater/start_updater.cpp                             \
			src/message_connection.cpp                                      \
			src/connection.cpp                                              \
			src/users.cpp                                                   \
			src/chats.cpp                                                   \
			src/chat.cpp                                                    \
			src/chats_widgets/base_move_contacts_widget.cpp                 \
			src/chats_widgets/recents_widget.cpp                            \
			src/chats_widgets/favorites_widget.cpp                          \
			src/chats_widgets/contact_separator.cpp                         \
			src/chats_widgets/chat_item_widget.cpp                          \
			src/chats_widgets/chat_item_widgets.cpp                         \
			src/chats_widgets/chat_item_widgets_manager.cpp                 \
			src/chats_widgets/chat_item_context_menu.cpp                    \
			src/chats_widgets/chats_widget.cpp                              \
			src/chats_widgets/search_contacts_widget.cpp                    \
			src/chats_widgets/current_chat.cpp                              \
			src/chats_widgets/chat_item_renamer.cpp                         \
			src/chats_widgets/chat_elided_text.cpp                          \
			src/modify_chat_users/dialog_modify_chat_users.cpp              \
			src/modify_chat_users/chat_user_flow_item.cpp                   \
			src/modify_chat_users/flow_content_widget.cpp                   \
			src/modify_chat_users/modify_chat_users.cpp                     \
			src/modify_chat_users/group_chat_users_actions.cpp              \
			src/gui_messages/item_message.cpp                               \
			src/gui_messages/item_text_message_myself.cpp                   \
			src/gui_messages/item_text_message_another.cpp                  \
			src/gui_messages/item_file_message.cpp                          \
			src/gui_messages/item_file_message_myself.cpp                   \
			src/gui_messages/item_file_message_another.cpp                  \
			src/gui_messages/item_notification_error.cpp                    \
			src/gui_messages/messages_widget.cpp                            \
			src/gui_messages/messages_widget_manager.cpp                    \
			src/gui_messages/write_message_widget.cpp                       \
			src/gui_messages/write_text_edit.cpp                            \
			src/gui_messages/hover_label.cpp                                \
			src/gui_messages/hover_button.cpp                               \
			src/gui_messages/item_file_message_buttons.cpp                  \
			src/gui_messages/chat_top/base_chat_top.cpp                     \
			src/gui_messages/chat_top/contact_chat_top.cpp                  \
			src/gui_messages/chat_top/animate_expand_participants.cpp       \
			src/gui_messages/chat_top/group_chat_top.cpp                    \
			src/gui_messages/chat_top/group_chat_top_user_context_menu.cpp  \
			src/notification_message/notification_message.cpp               \
			src/notification_message/notification_message_widget.cpp        \
			src/notification_message/notification_message_smooth_show.cpp   \
			src/notification_message/notification_message_smooth_expand.cpp \
			src/notification_message/notification_message_edge.cpp          \
			src/notification_message/notification_message_button_close.cpp  \
			src/common_gui/flow_layout.cpp                                  \
			src/test.cpp                                                    \
			src/check_readed_new_messages.cpp                               \
			src/database/database.cpp                                       \
			src/database/database_utility.cpp                               \
			src/database/database_properties.cpp                            \
			src/database_update_manager.cpp                                 \
			src/shell.cpp                                                   \
			src/application.cpp                                             \
			src/tray_icon.cpp                                               \
			src/loader_chat_messages.cpp                                    \
			src/search_users.cpp                                            \
			src/search_user_worker.cpp                                      \
			src/user_info_for_search.cpp                                    \
			src/sending_messages.cpp                                        \
			src/file_transfer/file_connection.cpp                           \
			src/file_transfer/file_dialog.cpp                               \
			src/file_transfer/file_transfer.cpp                             \
			src/file_transfer/file_transfer_manager.cpp                     \
			src/file_transfer/file_transfer_thread.cpp                      \
			src/file_transfer/task_scan_folder.cpp                          \
			src/login_dialog.cpp                                            \
			src/connection_manager.cpp                                      \

FORMS += 	src/ui/main_window.ui                                           \
			src/ui/contact_item_widget.ui                                   \
			src/ui/contact_separator.ui                                     \
			src/ui/item_file_message_another.ui                             \
			src/ui/item_file_message_myself.ui                              \
			src/ui/item_text_message_another.ui                             \
			src/ui/item_text_message_myself.ui                              \
			src/ui/messages_widget.ui                                       \
			src/ui/item_notification_error.ui                               \
			src/ui/modify_chat_users.ui                                     \
			src/ui/contact_chat_top.ui                                      \
			src/ui/group_chat_top.ui                                        \
			src/ui/notification_message.ui                                  \
			src/ui/notification_message_widget.ui                           \
			src/ui/test.ui                                                  \
			src/ui/login_dialog.ui                                          \
