//=====================================================================================//
//   Author: open
//   Date:   15.02.2019
//=====================================================================================//
#include "base/precomp.h"
#include "application.h"
#include "updater/start_updater.h"
#include "base/cryptography.h"
#include "base/single_run.h"
#include "base/utility.h"
#include "base/config.h"
#include <QCryptographicHash>
#include <QStandardPaths>
#include <QMessageBox>

Application::Application(Config& config, SingleRun& single_run, bool show_window, bool is_dev)
: m_config(config)
, m_is_dev(is_dev)
, m_app_name(qApp->applicationName())
//, m_app_visible_name(tr("file-sender"))
, m_app_visible_name(tr("Передача файлов"))
, m_app_data_path(create_app_data_folder())
, m_main_window("razrabotka++", m_app_name)
, m_login_dialog(&m_main_window)
, m_connection_manager(m_config.value("connection/host", "127.0.0.1"), NetProtocolVersion)
, m_single_run(single_run)
, m_once_wrong_password(false)
{
	connect(&m_single_run, SIGNAL(activate()), SLOT(slot_show_window()));
	connect(&m_main_window, SIGNAL(need_change_login()), SLOT(slot_change_login()));
	connect(&m_login_dialog, SIGNAL(cancel_clicked()), SLOT(slot_login_dialog_close()));
	connect(&m_login_dialog, SIGNAL(closed()), SLOT(slot_login_dialog_close()));
	connect(&m_login_dialog, SIGNAL(login_clicked(const QString&, const QString&)), SLOT(slot_login_ok(const QString&, const QString&)));
	connect(&m_connection_manager, SIGNAL(connection_autorized(MessageConnection*, const QString&, const QByteArray&, Timestamp)), SLOT(slot_connection_autorized(MessageConnection*, const QString&, const QByteArray&, Timestamp)));
	connect(&m_connection_manager, SIGNAL(connection_disconnected()), SLOT(slot_connection_disconnected()));
	connect(&m_connection_manager, SIGNAL(wrong_password()), SLOT(slot_wrong_password()));
	connect(&m_connection_manager, SIGNAL(server_not_available()), SLOT(slot_server_not_available()));
	connect(&m_connection_manager, SIGNAL(wrong_version()), SLOT(slot_wrong_version()));
	if (show_window)
		m_main_window.show();

	login_on_startup();
}
void Application::show_login_window()
{
	m_main_window.show();
	m_login_dialog.show_default();
	m_login_dialog.open();
}
void Application::slot_change_login()
{
	change_login();
}
void Application::change_login()
{
	m_connection_manager.set_auto_reconnect(false);
	m_connection_manager.close_connection();
	remove_startup_authorization();
	m_shell.reset();

	show_login_window();
}
void Application::slot_show_window()
{
	m_main_window.show_window();
}
void Application::slot_login_dialog_close()
{
	qApp->quit();
}
void Application::slot_login_ok(const QString& login, const QString& password)
{
	m_connection_manager.open_connection(login, password);
}
void Application::slot_connection_autorized(MessageConnection* message_connection, const QString& login_name, const QByteArray& encrypted_password, Timestamp database_timestamp)
{
	m_once_wrong_password = false;
	m_connection_manager.set_auto_reconnect(true);

	// скрываем диалог
	m_login_dialog.hide();

	// записываем на диск файл с авторизацией для старта
	write_startup_authorization(login_name, encrypted_password);

	// если таймштамп базы не совпадает, то удаляем базу
	//check_database_timestamp(login_name, database_timestamp);
	{
		QString database_path = get_database_folder_path(get_profile_folder_path(get_profiles_folder_path(), login_name));

		DatabaseUpdateManager db_update_manager(database_path);
		if (!db_update_manager.check_database_timestamp(database_timestamp))
		{
			// сбрасываем оболочку
			m_shell.reset();

			// удаляем базу
			db_update_manager.remove_database(database_timestamp);
		}
	}

	if (!m_shell)
	{
		// создаём новую оболочку
		m_shell = create_shell(login_name);
	}

	// устанавливаем соединение в оболочку
	m_shell->set_message_connection(message_connection);
}
// создать оболочку
std::unique_ptr<Shell> Application::create_shell(const QString& login)
{
	QString profiles_folder_path = get_profiles_folder_path();
	Utility::create_folder(profiles_folder_path);

	QString profile_folder_path = get_profile_folder_path(profiles_folder_path, login);
	Utility::create_folder(profile_folder_path);

	QString database_folder_path = get_database_folder_path(profile_folder_path);
	Utility::create_folder(database_folder_path);

	DatabaseUpdateManager db_update_manager(database_folder_path);
	if (!db_update_manager.migrate())
	{
		QMessageBox::information(&m_main_window, tr("database migrate error"), tr("Please call technical support 8888"), QMessageBox::StandardButton::Ok);
		exit(3);
		return std::unique_ptr<Shell>();
	}
	return std::unique_ptr<Shell>(new Shell(m_main_window, m_app_visible_name, login, database_folder_path, m_config.value("connection/host", "127.0.0.1")));
}
// дроп конекшена
void Application::slot_connection_disconnected()
{
	if (m_shell)
		m_shell->set_message_connection(nullptr);

	if (m_once_wrong_password) m_once_wrong_password = false;
	else m_login_dialog.server_not_available();
}

void Application::slot_wrong_password()
{
	// отображаем сообщение о неправильном пароле
	m_main_window.show();
	m_login_dialog.show();
	m_login_dialog.wrong_password();

	// удаляем ключ авторизации
	remove_startup_authorization();

	// отключаем автоматическое переподключение
	m_connection_manager.set_auto_reconnect(false);

	// если во время автоматического подключения пришёл сигнал о неправильном пароле... то выводим окно авторизации
	if (m_shell)
		change_login();

	m_once_wrong_password = true;
}

void Application::slot_server_not_available()
{
	m_login_dialog.server_not_available();
	m_once_wrong_password = false;
}

void Application::slot_wrong_version()
{
	m_connection_manager.set_auto_reconnect(false);
	m_once_wrong_password = false;
	m_login_dialog.hide();

	if (QMessageBox::information(&m_main_window, m_app_name, tr("Please update the application"), QMessageBox::StandardButton::Ok, QMessageBox::StandardButton::Close) == QMessageBox::Ok)
		start_updater();	// стартуем обновление

	qApp->exit(-1);
}
QString Application::create_app_data_folder() const
{
	QString path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + '/';
	Utility::create_folder(path);
	return path;
}
QString Application::get_profiles_folder_path() const
{
	return m_is_dev ? "profiles/" : m_app_data_path + "profiles/";
}
QString Application::get_profile_folder_path(const QString& profiles_path, const QString& login) const
{
	return profiles_path + login + '/';
}
QString Application::get_database_folder_path(const QString& profile_path)
{
	return profile_path + "database/";
}
// получить данные последней авторизации
bool Application::what_last_startup_authorization(QString& login_name, QByteArray& encrypted_password)
{
	QByteArray aes_encrypted_password;
	if (read_startup_authorization(login_name, aes_encrypted_password))
	{
		QByteArray aes_key = unique_machine_login_key256();
		if (!aes_key.isEmpty())
		{
			QByteArray aes_decrypted_password;
			if (Cryptography::aes_decrypt(aes_key, aes_encrypted_password, aes_decrypted_password))
			{
				encrypted_password = aes_decrypted_password;
				return true;
			}
		}
	}
	return false;
}
void Application::login_on_startup()
{
	// устанавливаем имя логина по умолчанию
	m_login_dialog.set_login_name(Utility::get_user_name());

	QString login_name;
	QByteArray encrypted_password;
	if (what_last_startup_authorization(login_name, encrypted_password))
	{
		// загружаем оболочку
		m_shell = create_shell(login_name);

		// устанавливаем загруженный логин в окно авторизации
		m_login_dialog.set_login_name(login_name);

		// подключаемся к серверу
		m_connection_manager.open_connection_with_encrypted_password(login_name, encrypted_password);

		m_connection_manager.set_auto_reconnect(true);
		return;
	}

	// иначе отображаем окно ввода логина
	change_login();
}
// путь до файла авторизации при старте
QString Application::startup_authorization_path() const
{
	return m_app_data_path + "/startup_authorization.key";
}
// записать файл с авторизацией для старта
void Application::write_startup_authorization(const QString& login_name, const QByteArray& encrypted_password)
{
	QByteArray aes_key = unique_machine_login_key256();
	if (!aes_key.isEmpty())
	{
		QByteArray aes_encrypted_password;
		if (Cryptography::aes_encrypt(aes_key, encrypted_password, aes_encrypted_password))
		{
			QFile file(startup_authorization_path());
			if (file.open(QIODevice::WriteOnly))
			{
				file.write(login_name.toUtf8());
				file.write("\0", 1);
				file.write(aes_encrypted_password);
			}
		}
	}
}
// прочитать файл с авторизацией для старта
bool Application::read_startup_authorization(QString& login_name, QByteArray& encrypted_password) const
{
	QFile file(startup_authorization_path());
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray login_and_password = file.readAll();
		int separator = login_and_password.indexOf('\0');
		if (separator == -1) return false;

		login_name = login_and_password.left(separator);
		encrypted_password = login_and_password.mid(separator + 1);
		return true;
	}
	return false;
}
// удаление файла с авторизацией для старта
void Application::remove_startup_authorization()
{
	QFile file(startup_authorization_path());
	if (file.exists())
		file.remove();
}
// уникальный 256-битный ключ PC
QByteArray Application::unique_machine_login_key256() const
{
	QByteArray id = Utility::unique_machine_login_id();
	return QCryptographicHash::hash(id, QCryptographicHash::Algorithm::Keccak_256);
}