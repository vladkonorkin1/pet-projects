//=====================================================================================//
//   Author: open
//   Date:   15.02.2019
//=====================================================================================//
#pragma once
#include "main_window.h"
#include "shell.h"
#include "login_dialog.h"
#include "connection_manager.h"

class Config;
class SingleRun;

class Application : public QObject
{
	Q_OBJECT
private:
	Config& m_config;
	bool m_is_dev;
	QString m_app_name;
	QString m_app_visible_name;
	QString m_app_data_path;
	MainWindow m_main_window;
	LoginDialog m_login_dialog;
	ConnectionManager m_connection_manager;
	SingleRun& m_single_run;
	std::unique_ptr<Shell> m_shell;
	bool m_once_wrong_password;

public:
	Application(Config& config, SingleRun& single_run, bool show_window, bool is_dev);

private slots:
	void slot_change_login();
	void slot_show_window();
	void slot_login_dialog_close();
	void slot_login_ok(const QString& login, const QString& password);
	void slot_connection_autorized(MessageConnection* message_connection, const QString& login_name, const QByteArray& encrypted_password, Timestamp database_timestamp);
	void slot_connection_disconnected();
	void slot_wrong_password();
	void slot_server_not_available();
	void slot_wrong_version();

private:
	void show_login_window();
	// создать папку данных приложения
	QString create_app_data_folder() const;
	// авторизация при старте
	void login_on_startup();
	// путь до файла авторизации при старте
	QString startup_authorization_path() const;
	// прочитать файл с авторизацией для старта
	bool read_startup_authorization(QString& login_name, QByteArray& encrypted_password) const;
	// записать файл с авторизацией для старта
	void write_startup_authorization(const QString& login_name, const QByteArray& encrypted_password);
	// уникальный 256-битный ключ PC
	QByteArray unique_machine_login_key256() const;
	// удаление файла с авторизацией для старта
	void remove_startup_authorization();
	// сменить логин
	void change_login();
	// создать оболочку
	std::unique_ptr<Shell> create_shell(const QString& login);
	// получить данные последней авторизации
	bool what_last_startup_authorization(QString& login_name, QByteArray& encrypted_password);
	// пути к папкам
	QString get_profiles_folder_path() const;
	QString get_profile_folder_path(const QString& profiles_path, const QString& login) const;
	QString get_database_folder_path(const QString& profile_path);
};