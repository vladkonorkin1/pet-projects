//=====================================================================================//
//   Author: open
//   Date:   10.02.2018
//=====================================================================================//
#include "base/precomp.h"
#include "chat.h"

Chat::Chat(ChatId id)
: m_id(id)
, m_contact(nullptr)
, m_timestamp(0)
, m_favorite(false)
, m_enabled(false)
, m_participant(false)
, m_admin(false)
, m_last_time(0)
, m_last_message_id(0)
, m_last_message_stamp(0)
, m_historical_message_stamp(0)
, m_last_readed_message_id(0)
{
}
Chat::~Chat()
{
	clear_users();

	// удаляем сообщения
	for (auto& pair : m_local_id_messages)
		delete pair.second;
}
void Chat::init(ChatMessageId last_message_id, ChatMessageStamp last_message_stamp, ChatMessageStamp historical_message_stamp, ChatMessageId last_readed_message_id)
{
	m_last_message_id = last_message_id;
	m_last_message_stamp = last_message_stamp;
	m_historical_message_stamp = historical_message_stamp;
	m_last_readed_message_id = last_readed_message_id;
}
void Chat::set_contact(const User* contact) {
	m_contact = contact;
	m_name = contact->name;
}
void Chat::set_favorite(bool favorite) {
	m_favorite = favorite;
}
void Chat::set_name(const QString& name) {
	assert( !m_contact );
	m_name = name;
}
void Chat::set_timestamp(Timestamp timestamp) {
	m_timestamp = timestamp;
}
bool Chat::favorite() const {
	return m_favorite;
}
const QString& Chat::name() const {
	return m_name;
}
const User* Chat::contact() const {
	return m_contact;
}
ChatId Chat::id() const {
	return m_id;
}
const Chat::users_t& Chat::users() const
{
	return m_users;
}
void Chat::clear_users()
{
	for (auto& it : m_users)
		delete it.second;
	m_users.clear();
}
ChatUser* Chat::add_user(const User* user, bool admin)
{
	if (m_users.end() == m_users.find(user->id))
	{
		ChatUser* chat_user = new ChatUser{ user, admin };
		m_users.insert(std::make_pair(user->id, chat_user));
		return chat_user;
	}
	return nullptr;
}
void Chat::set_participant(bool participant)
{
	m_participant = participant;
}
bool Chat::is_participant() const {
	return m_participant;
}
void Chat::set_admin(bool admin) {
	m_admin = admin;
}
bool Chat::is_admin() const {
	return m_admin;
}
bool Chat::is_user_admin(const User* user) const
{
	auto it = m_users.find(user->id);
	if (it != m_users.end())
		return it->second->admin;
	return false;
}
void Chat::set_last_time(int last_time) {
	m_last_time = last_time;
}
ChatMessageId Chat::last_message_id() const {
	return m_last_message_id;
}
// найти сообщение или создать
ChatMessage* Chat::find_or_create_message(const ChatMessage& source_message)
{
	ChatMessageLocalId local_id = source_message.local_id;
	assert( !local_id.isNull() );

	ChatMessage* msg = nullptr;
	{
		auto it = m_local_id_messages.find(local_id);
		if (m_local_id_messages.end() != it)
		{
			msg = it->second;
			*msg = source_message;
		}
		else
		{
			msg = new ChatMessage(source_message);
			m_local_id_messages[local_id] = msg;
		}
	}
	if (msg->id != 0)
	{
		if (m_id_messages.end() == m_id_messages.find(msg->id))
			m_id_messages[msg->id] = msg;
	}
	return msg;
}
// добавить сообщение
ChatMessage* Chat::add_message(const ChatMessage& message)
{
	if (ChatMessage* msg = find_or_create_message(message))
	{
		prepare_message(msg);
		return msg;
	}
	return nullptr;
}
// подготовить сообщение
void Chat::prepare_message(ChatMessage* msg)
{
	// если новый id-ник больше предыдущего, то считаем его последним добавленным
	if (msg->id > m_last_message_id)
		m_last_message_id = msg->id;

	if (msg->stamp > m_last_message_stamp)
		m_last_message_stamp = msg->stamp;

	// если чужое сообщение
	if (!msg->myself)
	{
		if (msg->readed())
		{
			if (msg->id > m_last_readed_message_id)
			{
				m_last_readed_message_id = msg->id;
			}
		}

		if (!msg->readed())
		{
			add_unreaded_message(msg->id);
		}
	}
}

// отметить сообщение, как прочитанное
bool Chat::set_messages_readed(ChatMessageId message_id, const QDateTime& datetime_readed)
{
	if (m_last_readed_message_id < message_id)
	{
		m_last_readed_message_id = message_id;
		
		// выставляем прочтение для загруженных сообщений
		{
			auto it = m_id_messages.find(message_id);
			if (m_id_messages.end() != it)
			{
				ChatMessage* chat_message = it->second;
				chat_message->datetime_readed = datetime_readed;
				chat_message->status = ChatMessageStatus::Readed;
			}
		}

		// удаляем из списка непрочитанных сообщений
		{
			auto it = m_unreaded_messages.find(message_id);
			if (m_unreaded_messages.end() != it)
				m_unreaded_messages.erase(m_unreaded_messages.begin(), ++it);
		}
		return true;
	}
	return false;
}
const ChatMessage* Chat::first_message() const
{
	auto it = m_id_messages.begin();
	if (it != m_id_messages.end()) return it->second;
	return nullptr;
}
// следующее сообщение для запроса из истории
ChatMessageId Chat::next_history_message_id() const
{
	if (const ChatMessage* message = first_message()) return message->id;
	//return m_historical_message_id + 1;
	
	if (m_last_message_id == 0)
		return std::numeric_limits<qint64>::max();	// this is hack!!!
	
	return m_last_message_id + 1;
}
// установить список непрочитанных сообщений
void Chat::set_unreaded_messages(const unreaded_messages_t& unreaded_messages)
{
	m_unreaded_messages = unreaded_messages;
}
// добавить непрочитанное сообщение
void Chat::add_unreaded_message(ChatMessageId message_id)
{
	m_unreaded_messages.insert(message_id);
}
// удалить сообщение
bool Chat::remove_message(const ChatMessageLocalId& local_id)
{
	auto it = m_local_id_messages.find(local_id);
	if (m_local_id_messages.end() != it)
	{
		ChatMessage* msg = it->second;

		if (msg->id != 0)
			m_id_messages.erase(msg->id);

		delete msg;
		m_local_id_messages.erase(it);
		return true;
	}
	return false;
}
// получить все сообщения (отсортированными)
std::vector<const ChatMessage*> Chat::sorted_messages() const
{
	std::vector<const ChatMessage*> result;
	result.reserve(m_local_id_messages.size());

	// копируем все полученные сервером сообщения (они уже в порядке возрастания)
	for (auto& pair : m_id_messages)
		result.push_back(pair.second);

	// ищем сообщения, которые не были доставлены на сервер
	std::vector<ChatMessage*> unsent_messages;
	for (auto& pair : m_local_id_messages)
	{
		ChatMessage* msg = pair.second;
		if (msg->id == 0)
			unsent_messages.push_back(msg);
	}
	// сортируем их по дате
	std::sort(unsent_messages.begin(), unsent_messages.end(), [](const ChatMessage* l, const ChatMessage* r) { return l->datetime < r->datetime; });

	// в конец добавляем сообщения ещё недоставленные на сервер
	for (ChatMessage* msg : unsent_messages)
		result.push_back(msg);

	return result;
}
// найти сообщение по id
const ChatMessage* Chat::find_message(ChatMessageId message_id) const
{
	auto it = m_id_messages.find(message_id);
	if (m_id_messages.end() != it)
		return it->second;
	return nullptr;
}
// найти сообщение по local_id
const ChatMessage* Chat::find_message(const ChatMessageLocalId& message_local_id) const
{
	auto it = m_local_id_messages.find(message_local_id);
	if (m_local_id_messages.end() != it)
		return it->second;
	return nullptr;
}