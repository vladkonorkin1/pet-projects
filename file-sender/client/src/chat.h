//=====================================================================================//
//   Author: open
//   Date:   10.02.2018
//=====================================================================================//
#pragma once
#include "user.h"
#include "chat_message.h"

struct ChatUser
{
	const User* user;
	bool admin;
};

class Chat : public QObject
{
	Q_OBJECT
public:
	using users_t = std::map<UserId, ChatUser*>;
	using unreaded_messages_t = std::set<ChatMessageId>;

private:
	ChatId m_id;
	const User* m_contact;
	// group chat properties
	QString m_name;
	users_t m_users;
	Timestamp m_timestamp;	// таймштамп изменения данных чата

	bool m_favorite;
	bool m_enabled;			// флаг присутствия чата в chat_list
	bool m_participant;		// является ли текущий контакт участником данного чата
	bool m_admin;			// является ли текущий контакт админом данного чата
	int m_last_time;		// счётчик сортировки в чате

	ChatMessageId m_last_message_id;			// последнее сообщение в чате
	ChatMessageStamp m_last_message_stamp;		// последний штамп сообщений
	ChatMessageStamp m_historical_message_stamp;	// последний штамп исторических сообщений (условное деление сообщений на новые и старые)
	ChatMessageId m_last_readed_message_id;		// последнее прочитанное сообщение		//ChatMessageId m_first_unreaded_message_id;// первое непрочитанное сообщение
	
	// сообщения
	std::map<ChatMessageId, ChatMessage*> m_id_messages;
	std::map<ChatMessageLocalId, ChatMessage*> m_local_id_messages;

	// список id-ников непрочитанных сообщений
	unreaded_messages_t m_unreaded_messages;

public:
	Chat(ChatId id);
	virtual ~Chat();

	void init(ChatMessageId last_message_id, ChatMessageStamp last_message_stamp, ChatMessageStamp historical_message_stamp, ChatMessageId last_readed_message_id);

	void set_contact(const User* contact);
	void set_name(const QString& name);
	void set_favorite(bool favorite);
	void set_participant(bool participant);
	void set_admin(bool admin);
	void set_last_time(int last_time);
	// установить таймштамп изменения данных чата
	void set_timestamp(Timestamp timestamp);
	// установить список непрочитанных сообщений
	void set_unreaded_messages(const unreaded_messages_t& unreaded_messages);
	// добавить непрочитанное сообщение
	void add_unreaded_message(ChatMessageId message_id);
	void clear_users();
	ChatUser* add_user(const User* user, bool admin);
	const users_t& users() const;
	const User* contact() const;
	const QString& name() const;
	bool favorite() const;
	ChatId id() const;
	Timestamp timestamp() const;
	bool is_participant() const;
	bool is_admin() const;
	bool is_user_admin(const User* user) const;
	int last_time() const;
	ChatMessageId last_message_id() const;
	ChatMessageId last_readed_message_id() const;
	ChatMessageStamp last_message_stamp() const;
	void set_enabled(bool enabled) { m_enabled = enabled; }
	bool is_enabled() const { return m_enabled; }
	// добавить сообщение
	ChatMessage* add_message(const ChatMessage& msg);
	// удалить сообщение
	bool remove_message(const ChatMessageLocalId& local_id);
	// отметить сообщение, как прочитанное
	bool set_messages_readed(ChatMessageId message_id, const QDateTime& datetime_readed);
	// получить кол-во непрочитанных сообщений
	int count_unreaded_messages() const;
	// следующее сообщение для запроса из истории
	ChatMessageId next_history_message_id() const;
	// получить все сообщения (отсортированными - потому как неотправленные сообщения никак не отсортированы)
	std::vector<const ChatMessage*> sorted_messages() const;
	// найти сообщение по id
	const ChatMessage* find_message(ChatMessageId message_id) const;
	// найти сообщение по local_id
	const ChatMessage* find_message(const ChatMessageLocalId& message_local_id) const;

private:
	const ChatMessage* first_message() const;
	// найти сообщение или создать
	ChatMessage* find_or_create_message(const ChatMessage& source_message);
	// подготовить сообщение
	void prepare_message(ChatMessage* msg);
	// создать сообщение
	ChatMessage* create_message(const ChatMessage& source_message);
};


inline int Chat::last_time() const {
	return m_last_time;
}
inline Timestamp Chat::timestamp() const {
	return m_timestamp;
}
inline ChatMessageId Chat::last_readed_message_id() const {
	return m_last_readed_message_id;
}
inline ChatMessageStamp Chat::last_message_stamp() const {
	return m_last_message_stamp;
}
// получить кол-во непрочитанных сообщений
inline int Chat::count_unreaded_messages() const {
	return m_unreaded_messages.size();
}