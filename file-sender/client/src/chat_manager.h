//=====================================================================================//
//   Author: open
//   Date:   27.02.2019
//=====================================================================================//
#pragma once
#include "user.h"
#include "chat.h"

class Database;

class ChatManager : public QObject
{
	Q_OBJECT
public:
	using users_t = std::vector<const User*>;
	using chats_t = std::map<ChatId, Chat*>;

private:
	Database& m_database;


public:
	ChatManager();
	virtual ~ChatManager();
	// поиск чата по id
	Chat* find_chat(ChatId id) const;
	// поиск чата по id контакта
	Chat* find_chat_by_contact(UserId id) const;
	// удалить все чаты из чат листа
	void remove_all_chats();
	// обновить чат с контактом
	void update_contact_chat();
	// обновить групповой чат
	void update_group_chat();
	// обновить чат лист
	void update_chat_list(const std::vector<Chat*>& recents, const std::vector<Chat*>& favorites);
	// обновить имя группового чата
	void update_group_chat_name(Chat* chat, const QString& name);
	// добавить чат в чат-лист
	void add_chat(Chat* chat, bool favorite);
	// убрать чат из списка
	void remove_chat(ChatId id);
	// получить чат-лист
	const chats_t& chat_list() const;
	// получить все чаты
	//const chats_t& chats() const;
};