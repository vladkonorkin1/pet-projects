//=====================================================================================//
//   Author: open
//   Date:   27.04.2017
//=====================================================================================//
#pragma once
#include <QDateTime>

class User;
class Chat;

class ChatMessage
{
public:
	ChatMessageId id;
	const Chat* chat;
	const User* sender;
	ChatMessageType type;
	ChatMessageStamp stamp;
	ChatMessageStatus status;
	HistoryStatus history_status;
	QDateTime datetime;
	QDateTime datetime_readed;

	bool myself;
	bool historical;

	QString text;
	ChatMessageLocalId local_id;	// GUID

	QString file_local_path;
	QString file_server_path;
	quint64 file_size;
	bool file_is_dir;
	FileTransferResult file_status;

	const User* target_contact;	// кого пригласили в чат (или удалили)

	ChatMessage(ChatMessageId id = 0, const Chat* chat = nullptr, const User* sender = nullptr, ChatMessageStamp stamp = 0, ChatMessageType type = ChatMessageType::Text, ChatMessageStatus status = ChatMessageStatus::Sending, HistoryStatus history_status = HistoryStatus::Created, const QDateTime& datetime = QDateTime(), const QDateTime& datetime_readed = QDateTime(), bool historical = false)
	: id(id)
	, chat(chat)
	, sender(sender)
	, type(type)
	, stamp(stamp)
	, status(status)
	, history_status(history_status)
	, datetime(datetime)
	, datetime_readed(datetime_readed)
	, myself(false)
	, historical(historical)
	, file_is_dir(false)
	, file_size(0)
	, file_status(FileTransferResult::Default)
	, target_contact(nullptr)
	{
	}

	bool readed() const { return status == ChatMessageStatus::Readed; }
};

using UChatMessage = std::unique_ptr<ChatMessage>;