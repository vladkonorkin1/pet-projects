//=====================================================================================//
//   Author: open
//   Date:   20.06.2017
//=====================================================================================//
#include "base/precomp.h"
#include "chats.h"
#include "database/database.h"
#include "users.h"

Chats::Chats(Database& database, const Users& users)
: m_database(database)
, m_users(users)
, m_last_time(0)
, m_login_user(nullptr)
, m_have_unreaded_messages(false)
{
}
Chats::~Chats()
{
	for (auto pair : m_chats)
		delete pair.second;
}
// поиск чата по id
Chat* Chats::find_chat(ChatId id) const
{
	chats_t::const_iterator it = m_chats.find(id);
	if (it != m_chats.end()) return it->second;
	return nullptr;
}
// поиск чата по id контакта
Chat* Chats::find_chat_by_contact(UserId id) const
{
	contact_chats_t::const_iterator it = m_contact_chats.find(id);
	if (it != m_contact_chats.end()) return it->second;
	return nullptr;
}
// загрузить из базы данных
void Chats::load_from_database(const User* self_user)
{
	// загружаем чаты
	std::vector<DescChat> chats = m_database.select_chats();
	for (const DescChat& desc : chats)
	{
		if (desc.contact_id != 0)
		{
			if (const User* contact = m_users.find_user(desc.contact_id))
				create_contact_chat(desc.id, contact, desc.timestamp, desc.last_message_id, desc.last_message_stamp, desc.historical_message_stamp, desc.last_readed_message_id);
		}
		else
		{
			if (Chat* chat = create_group_chat(desc.id, desc.name, desc.timestamp, desc.last_message_id, desc.last_message_stamp, desc.historical_message_stamp, desc.last_readed_message_id))
			{
				std::vector<const User*> users;
				users.reserve(desc.chat_users.size());
				std::vector<const User*> admins;
				for (const DescChatUser& chat_user : desc.chat_users)
				{
					if (const User* contact = m_users.find_user(chat_user.user_id))
					{
						users.push_back(contact);
						if (chat_user.admin) admins.push_back(contact);
					}
				}
				set_group_chat_users(chat, self_user, users, admins);
			}
		}
	}

	// загружаем чат-лист и создаем его
	std::vector<DescChatListContact> chat_list = m_database.select_chat_list();
	for (const DescChatListContact& desc : chat_list)
	{
		if (Chat* chat = find_chat(desc.chat_id))
			internal_add_chat(chat, desc.favorite);
	}

	// загружаем id непрочитанных сообщений
	auto unreaded_chat_messages = m_database.select_unreaded_messages(self_user);
	std::set<Chat*> unreaded_chats;
	for (const auto& pair : unreaded_chat_messages)
	{
		ChatId chat_id = pair.first;
		const Database::unreaded_messages_t& unreaded_messages = pair.second;

		if (Chat* chat = find_chat(chat_id))
		{
			unreaded_chats.insert(chat);
			chat->set_unreaded_messages(unreaded_messages);
		}
	}

	// оповещаем о появлении непрочитанных сообщений
	notify_unreaded_messages(unreaded_chats);
}
// удалить все чаты из чат листа
void Chats::remove_all_chats()
{
	// сбрасываем счётчик поднятия чата в чат-листе
	m_last_time = 0;

	for (auto& pair : m_chats)
	{
		Chat* chat = pair.second;
		if (chat->is_enabled())
			internal_remove_chat(chat);
	}
}
// обновить чат с контактом
void Chats::update_contact_chat(ChatId id, const User* contact, Timestamp timestamp)
{
	if (Chat* chat = create_contact_chat(id, contact, timestamp, 0, 0, 0, 0))
		m_database.update_chat(chat);
}
// обновить групповой чат
void Chats::update_group_chat(ChatId id, const QString& name, Timestamp timestamp)
{
	if (Chat* chat = create_group_chat(id, name, timestamp, 0, 0, 0, 0))
		m_database.update_chat(chat);
}
// обновить участников группового чата
void Chats::update_group_chat_users(Chat* chat, const User* self_user, const users_t& users, const users_t& admins)
{
	set_group_chat_users(chat, self_user, users, admins);
	m_database.update_chat_users(chat);
}
// обновить чат лист
void Chats::update_chat_list(const std::vector<ChatId>& favorites, const std::vector<ChatId>& recents)
{
	emit start_update_chat_list();

	remove_all_chats();

	std::vector<const Chat*> chat_list;
	for (auto it = recents.begin(); it != recents.end(); it++)
	{
		if (Chat* chat = find_chat(*it))
		{
			add_chat(chat, false);
			chat_list.push_back(chat);
		}
	}
	for (auto it = favorites.begin(); it != favorites.end(); it++)
	{
		if (Chat* chat = find_chat(*it))
		{
			add_chat(chat, true);
			chat_list.push_back(chat);
		}
	}
	m_database.update_chat_list(chat_list);

	emit finish_update_chat_list();
}
/*// обновить чат лист
void Chats::update_chat_list(const std::vector<Chat*>& favorite_chats, const std::vector<Chat*>& recent_chats)
{
	// выбираем чаты для поднятия вверх
	std::vector<Chat*> to_top_chats = select_to_top_chats(chats); 
	// выбираем чаты которые были удалены для их скрытия
	std::vector<Chat*> deleted_chats = select_deleted_chats_from_new(chats);
	// обновляем отображение чатов
	update_chats_display(to_top_chats, deleted_chats);
	// обновляем чат-лист в локальной базе
	m_database.update_chat_list(chats);*/

	/*std::vector<Chat*> chats;
	for (Chat* chat : favorite_chats)
		chats.push_back(chat);

	for (Chat* chat : recent_chats)
		chats.push_back(chat);

	// выбираем чаты которые были удалены для их скрытия
	std::vector<Chat*> favorite_deleted_chats = select_deleted_chats_from_new(favorite_chats);
	// удаляем их с отображения
	for (Chat* chat : favorite_deleted_chats)
	{
		if (chat->is_enabled())
			internal_remove_chat(chat);
	}

	// выбираем чаты которые были удалены для их скрытия
	std::vector<Chat*> recent_deleted_chats = select_deleted_chats_from_new(recent_chats);
	// удаляем их с отображения
	for (Chat* chat : recent_deleted_chats)
	{
		if (chat->is_enabled())
			internal_remove_chat(chat);
	}

	// выбираем чаты для поднятия вверх
	std::vector<Chat*> favorite_top_chats = select_to_top_chats(favorite_chats);
	// обновляем отображение чатов
	for (Chat* chat : favorite_top_chats)
	{
		if (chat->is_enabled())
			internal_remove_chat(chat);
	}

	// выбираем чаты для поднятия вверх
	std::vector<Chat*> recent_top_chats = select_to_top_chats(recent_chats);
	// обновляем отображение чатов
	for (Chat* chat : recent_top_chats)
	{
		if (chat->is_enabled())
			internal_remove_chat(chat);
	}

	// обновляем флаг favorite
	for (Chat* chat : favorite_chats)
		chat->set_favorite(true);

	// обновляем флаг recent
	for (Chat* chat : recent_chats)
		chat->set_favorite(false);


	for (Chat* chat : recent_top_chats)
		add_chat(chat, chat->favorite());

	for (Chat* chat : favorite_top_chats)
		add_chat(chat, chat->favorite());

	//std::vector<Chat*> recent_top_chats = select_to_top_chats(chats);


	// обновляем чат-лист в локальной базе
	m_database.update_chat_list(chats);
}*/
// выбираем чаты которые были удалены для их скрытия
std::vector<Chat*> Chats::select_deleted_chats_from_new(const std::vector<Chat*>& new_chats)
{
	std::vector<Chat*> deleted_chats;

	std::set<Chat*> set_new_chats;
	std::transform(new_chats.begin(), new_chats.end(), std::inserter(set_new_chats, set_new_chats.end()), [](Chat* chat) { return chat; });
	for (auto& it : m_chat_list)
	{
		Chat* chat = it.second;
		if (set_new_chats.find(chat) == set_new_chats.end())
			deleted_chats.push_back(chat);
	}
	return deleted_chats;
}
// выбрать чаты для поднятия наверх
std::vector<Chat*> Chats::select_to_top_chats(const std::vector<Chat*>& new_chats)
{
	std::vector<Chat*> top_chats = select_top_chats_from_new(new_chats);
	return remove_doubles_top_chats(top_chats);
}
//выбираем чаты которые поднялись наверх в списке
std::vector<Chat*> Chats::select_top_chats_from_new(const std::vector<Chat*>& new_chats)
{
	std::list<Chat*> old_chats; // лист старых чатов
	const chats_t& map_old_chats = m_chat_list;
	// копируем чаты в лист для сортировки
	std::transform(map_old_chats.begin(), map_old_chats.end(), std::back_inserter(old_chats), [](const std::pair<ChatId, Chat*>& item) { return item.second; });
	// сортируем чаты по времени изменения
	old_chats.sort([](Chat* x, Chat* y) { return x->last_time() < y->last_time(); });
	std::vector<Chat*> top_chats; // вектор чатов с измененной позицией в чат листе
	int reverse_it = -1; // обратный итератор обхода чат листа

	// проверяем есть ли изменения чатов
	for (int i = new_chats.size(); i > 0; i--)
	{
		Chat* new_chat = new_chats.at(i - 1);
		// если чата нет в чат-листе - добавляем его в чаты
		if (map_old_chats.find(new_chat->id()) == map_old_chats.end())
			top_chats.push_back(new_chat);
		// если чат есть - проверяем на изменения
		else
		{
			for (int index = old_chats.size() - 1; index > 0; index--)
			{

				std::list<Chat*>::iterator iter = std::next(old_chats.end(), reverse_it);
				// если чат изменился - добавляем в вектор
				if (new_chat->id() != (*iter)->id())
				{
					// переносим чат наверх
					old_chats.splice(old_chats.begin(), old_chats, iter);
					top_chats.push_back(new_chat);
				}
				// проверяем есть ли чат в векторе измененных если есть = сдвигаем регистр но не пишем в вектор
				//else 
				//	if (std::find(top_chats.begin(), top_chats.end(), new_chat) == top_chats.end())
				//{
				//	reverse_it--;
				//	break;
				//}
				else
				{
					reverse_it--;
					break;
				}
			}
		}
	}
	return top_chats; // вектор чатов с измененной позицией в чат листе
}
// отсекаем дубли новых чатов для перемещения их в топ
std::vector<Chat*> Chats::remove_doubles_top_chats(const std::vector<Chat*>& top_chats)
{
	std::vector<Chat*> to_top_chats; // вектор чатов для перемещения наверх
	std::set<Chat*> top_set;
	for (int i = top_chats.size() - 1; i >= 0; i--)
	{
		Chat* chat = top_chats.at(i);
		if (top_set.find(chat) == top_set.end())
		{
			top_set.insert(chat);
			to_top_chats.push_back(chat);
		}
	}
	return to_top_chats;
}
/*void Chats::update_chats_display(const std::vector<Chat*>& top_chats, const std::vector<Chat*>& deleted_chats)
{
	for (Chat* chat : deleted_chats)
	{
		if (chat->is_enabled())
			internal_remove_chat(chat);
	}
	for (Chat* chat : top_chats)
	{
		if (chat->is_enabled())
			internal_remove_chat(chat);
		add_chat(chat, chat->favorite());
	}
}*/
// добавить чат в чат-лист
void Chats::add_chat(Chat* chat, bool favorite)
{
	internal_add_chat(chat, favorite);
	m_database.add_chat_to_chat_list(chat);
}
// добавить в чат лист
void Chats::internal_add_chat(Chat* chat, bool favorite)
{
	chat->set_enabled(true);
	chat->set_favorite(favorite);
	set_chat_top(chat);

	m_chat_list[chat->id()] = chat;
	emit chat_added(chat);
}
// убрать чат из списка
void Chats::remove_chat(ChatId id)
{
	chats_t::iterator it = m_chats.find(id);
	if (it != m_chats.end())
	{
		assert( it->second->is_enabled() );
		internal_remove_chat(it->second);
	}
	m_database.remove_chat_from_chat_list(id);
}
// удалить чат
void Chats::internal_remove_chat(Chat* chat)
{
	chat->set_enabled(false);
	m_chat_list.erase(chat->id());
	emit chat_removed(chat);
}
/*// проверяет наличие непрочитанных сообщения во всех чатах
bool Chats::check_for_unreaded_messages()
{
	for (auto& it : m_chats)
	{
		Chat* chat = it.second;
		if (chat->is_enabled())
		{
			if (chat->count_unreaded_messages() > 0)
				return true;
		}
	}
	return false;
}*/
void Chats::set_chat_top(Chat* chat)
{
	chat->set_last_time(++m_last_time);
}
Chat* Chats::create_contact_chat(	ChatId id,
									const User* contact,
									Timestamp timestamp,
									ChatMessageId last_message_id,
									ChatMessageStamp last_message_stamp,
									ChatMessageStamp historical_message_stamp,
									ChatMessageId last_readed_message_id)
{
	Chat* chat = nullptr;
	chats_t::iterator it = m_chats.find(id);
	bool need_create = it == m_chats.end();
	if (need_create)
	{
		chat = new Chat(id);
		chat->init(last_message_id, last_message_stamp, historical_message_stamp, last_readed_message_id);
		m_chats.insert(std::make_pair(id, chat)).first;

		if (m_contact_chats.find(contact->id) == m_contact_chats.end())
		{
			m_contact_chats.insert(std::make_pair(contact->id, chat));
			m_request_contact_chats.insert(contact->id);
		}	
	}
	else chat = it->second;

	chat->set_contact(contact);
	chat->set_timestamp(timestamp);

	emit chat_created(chat);
	return chat;
}
// можем ли запросить информацию по контакт-чату
bool Chats::may_request_contact_chat(const User* contact)
{
	/*if (m_request_contact_chats.find(contact->id) == m_request_contact_chats.end())
	{
		m_request_contact_chats.insert(contact->id);
		return true;
	}
	return false;*/

	return true;
}
Chat* Chats::create_group_chat(ChatId id, const QString& name, Timestamp timestamp, ChatMessageId last_message_id, ChatMessageStamp last_message_stamp, ChatMessageStamp historical_message_stamp, ChatMessageId last_readed_message_id)
{
	Chat* chat = nullptr;
	chats_t::iterator it = m_chats.find(id);
	bool need_create = it == m_chats.end();
	if (need_create)
	{
		chat = new Chat(id);
		chat->init(last_message_id, last_message_stamp, historical_message_stamp, last_readed_message_id);
		m_chats.insert(std::make_pair(id, chat)).first;
	}
	else chat = it->second;

	chat->set_name(name);
	chat->set_timestamp(timestamp);

	emit chat_created(chat);
	return chat;
}
// установить участников группового чата
void Chats::set_group_chat_users(Chat* chat, const User* this_contact, const users_t& users, const users_t& admins)
{
	chat->clear_users();

	std::map<UserId, ChatUser*> added_users;
	for (auto contact : users)
	{
		ChatUser* user = chat->add_user(contact, false);
		added_users[contact->id] = user;
	}

	for (auto contact : admins)
	{
		if (ChatUser* user = added_users[contact->id])
			user->admin = true;
	}

	ChatUser* this_user = added_users[this_contact->id];
	chat->set_participant(this_user);
	chat->set_admin(this_user && this_user->admin);

	emit chat_change_users(chat);
}
// установить сообщения прочитанными
void Chats::set_messages_readed(Chat* chat, ChatMessageId message_id, const QDateTime& datetime_readed)
{
	ChatMessageId old_last_readed_message_id = chat->last_readed_message_id();
	if (chat->set_messages_readed(message_id, datetime_readed))
	{
		m_database.set_messages_readed(chat, old_last_readed_message_id, message_id, datetime_readed, m_login_user->id);

		if (const ChatMessage* msg = chat->find_message(message_id))
			emit chat_message_readed(chat, msg);

		notify_unreaded_messages(chat);
		//emit chat_change_count_new_messages(chat, chat->count_unreaded_messages());
	}
}
// установить пользователя, под которым мы авторизовались
void Chats::set_login_user(const User* user)
{
	m_login_user = user;
}
// добавить сообщения в чаты
void Chats::add_messages(const std::vector<DescChatMessage>& messages, bool store_database)
{
	// список поднятия чатов наверх
	std::set<const Chat*> to_top_chats;
	// список чатов с непрочитанными сообщениями
	std::set<Chat*> unreaded_chats;

	for (const DescChatMessage& desc : messages)
	{
		Chat* chat = find_chat(desc.chat_id);
		const User* sender = m_users.find_user(desc.sender_id);
		if (!chat || !sender)
			continue;
		ChatMessage msg(
			desc.id, 
			chat,
			sender,
			desc.stamp,
			desc.type,
			desc.status,
			desc.history_status,
			desc.datetime,
			desc.datetime_readed,
			desc.historical
		);
		msg.myself = m_login_user == sender;
		msg.text = desc.text;
		msg.local_id = desc.local_id;
		msg.file_local_path = desc.file_src_path;
		if (msg.type == ChatMessageType::File)
		{
			if (!msg.myself)
			{
				static QString s_folder_mover_name = QString::fromUtf8("/!Для перемещений");
				msg.file_server_path = m_login_user->folder + s_folder_mover_name + '/' + desc.file_dest_name;

				//msg.file_server_path = m_login_user->full_folder_path() + '/' + desc.file_dest_name;
				//msg.file_server_path = m_users.full_folder_path(m_login_user, desc.file_dest_name);
			}
		}
		msg.file_is_dir = desc.file_is_dir;
		msg.file_size = desc.file_size;
		msg.file_status = desc.file_status;

		ChatMessage* pmessage = chat->add_message(msg);
		if (!pmessage)
			continue;
		emit chat_message_added(chat, pmessage);

		// если сообщение не историческое, то подымаем чат наверх
		if (!pmessage->historical)
		{	
			set_chat_top(chat);
			to_top_chats.insert(chat);
		}

		if (!pmessage->myself)
			if (!pmessage->readed())
				unreaded_chats.insert(chat);
	}

	if (store_database)
		m_database.update_chat_messages(messages, to_top_chats);

	// оповещаем о появлении непрочитанных сообщений
	notify_unreaded_messages(unreaded_chats);
}
// оповещаем о непрочитанных сообщениях несколько чатов
void Chats::notify_unreaded_messages(const std::set<Chat*>& unreaded_chats)
{
	// оповещаем о появлении непрочитанных сообщений
	if (!unreaded_chats.empty())
	{
		for (Chat* chat : unreaded_chats)
			emit chat_change_count_new_messages(chat, chat->count_unreaded_messages());

		emit change_count_unreaded_messages(count_unreaded_messages());
	}
}
// оповещаем о непрочитанных сообщениях один чат
void Chats::notify_unreaded_messages(Chat* chat)
{
	emit chat_change_count_new_messages(chat, chat->count_unreaded_messages());
	emit change_count_unreaded_messages(count_unreaded_messages());
}
// кол-во непрочитанных сообщений во всех чатах
int Chats::count_unreaded_messages() const
{
	int count = 0;
	for (const auto& pair : m_chats)
	{
		const Chat* chat = pair.second;
		if (chat->is_enabled())
			count += chat->count_unreaded_messages();
	}
	return count;
}
// удалить сообщение
void Chats::remove_message(ChatId chat_id, const ChatMessageLocalId& local_id)
{
	if (Chat* chat = find_chat(chat_id))
	{
		if (const ChatMessage* msg = chat->find_message(local_id))
		{
			emit chat_message_removed(chat, local_id);
			chat->remove_message(local_id);
		}
	}
}