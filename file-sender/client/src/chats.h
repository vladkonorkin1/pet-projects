//=====================================================================================//
//   Author: open
//   Date:   28.02.2019
//=====================================================================================//
#pragma once
#include "user.h"
#include "chat.h"

class Database;
class Users;

class Chats : public QObject
{
	Q_OBJECT
public:
	using users_t = std::vector<const User*>;
	using chats_t = std::map<ChatId, Chat*>;

private:
	Database& m_database;
	const Users& m_users;
	// все чаты
	chats_t m_chats;
	// чат лист
	chats_t m_chat_list;
	// счётчик поднятия вверх
	int m_last_time;
	// чаты на контакт
	using contact_chats_t = std::map<UserId, Chat*>;
	contact_chats_t m_contact_chats;
	// пользователь под которым авторизовались
	const User* m_login_user;
	// контейнер пометок запроса контакт-чата
	std::set<UserId> m_request_contact_chats;
	// наличие непрочитанных сообщений
	bool m_have_unreaded_messages;

public:
	Chats(Database& database, const Users& users);
	virtual ~Chats();
	// поиск чата по id
	Chat* find_chat(ChatId id) const;
	// поиск чата по id контакта
	Chat* find_chat_by_contact(UserId id) const;
	// загрузить из базы данных
	void load_from_database(const User* self_user);
	// обновить чат с контактом
	void update_contact_chat(ChatId id, const User* contact, Timestamp timestamp);
	// обновить групповой чат
	void update_group_chat(ChatId id, const QString& name, Timestamp timestamp);
	// обновить участников группового чата
	void update_group_chat_users(Chat* chat, const User* self_user, const users_t& users, const users_t& admins);
	// обновить чат лист
	//void update_chat_list(const std::vector<Chat*>& favorite_chats, const std::vector<Chat*>& recent_chats);
	void update_chat_list(const std::vector<ChatId>& favorites, const std::vector<ChatId>& recents);
	// добавить чат в чат-лист
	void add_chat(Chat* chat, bool favorite);
	// убрать чат из списка
	void remove_chat(ChatId id);
	// получить чат-лист
	const chats_t& chat_list() const;
	// установить пользователя, под которым мы авторизовались
	void set_login_user(const User* user);
	// проверить на непрочитанные сообщения
	//bool check_for_unreaded_messages();
	// добавить сообщения в чаты
	void add_messages(const std::vector<DescChatMessage>& messages, bool store_database);
	// удалить сообщение
	void remove_message(ChatId chat_id, const ChatMessageLocalId& local_id);
	// установить сообщения прочитанными
	void set_messages_readed(Chat* chat, ChatMessageId message_id, const QDateTime& datetime_readed);
	// можем ли запросить информацию по контакт-чату
	bool may_request_contact_chat(const User* contact);

signals:
	void chat_created(Chat* chat);				// создание чата (единожды)
	void chat_added(Chat* chat);				// добавление в список чатов (если чат уже есть, то добавление чата наверх списка)
	void chat_removed(Chat* chat);				// удаление чата из чат листа
	void chat_change_users(const Chat* chat);	// изменение участников
	void chat_change_count_new_messages(Chat* chat, int count);
	void change_count_unreaded_messages(int count);
	
	void chat_message_added(Chat* chat, const ChatMessage* msg);					// добавлено сообщение
	void chat_message_removed(const Chat* chat, const ChatMessageLocalId& local_id);	// сообщение удалено
	void chat_message_readed(const Chat* chat, const ChatMessage* msg);					// сообщение прочтено

	void start_update_chat_list();
	void finish_update_chat_list();

private:
	// удалить все чаты из чат листа
	void remove_all_chats();
	// создать contact-чат
	Chat* create_contact_chat(ChatId id, const User* contact, Timestamp timestamp, ChatMessageId last_message_id, ChatMessageStamp last_message_stamp, ChatMessageStamp historical_message_stamp, ChatMessageId last_readed_message_id);
	// создать групповой чат
	Chat* create_group_chat(ChatId id, const QString& name, Timestamp timestamp, ChatMessageId last_message_id, ChatMessageStamp last_message_stamp, ChatMessageStamp historical_message_stamp, ChatMessageId last_readed_message_id);
	// установить участников группового чата
	void set_group_chat_users(Chat* chat, const User* this_contact, const users_t& users, const users_t& admins);
	// изменить имя группового чата
	//void set_group_chat_name(Chat* chat, const QString& name);
	// выбрать чаты для поднятия наверх
	std::vector<Chat*> select_to_top_chats(const std::vector<Chat*>& new_chats);
	// выбираем чаты которые были удалены для их скрытия
	std::vector<Chat*> select_deleted_chats_from_new(const std::vector<Chat*>& new_chats);
	// выбираем чаты которые поднялись наверх в списке
	std::vector<Chat*> select_top_chats_from_new(const std::vector<Chat*>& new_chats);
	// отсекаем дубли новых чатов для перемещения их в топ
	std::vector<Chat*> remove_doubles_top_chats(const std::vector<Chat*>& top_chats);
	// обновляем отображение чатов
	//void update_chats_display(const std::vector<Chat*>& top_chats, const std::vector<Chat*>& deleted_chats);
	// поднять чат наверх
	void set_chat_top(Chat* chat);
	// удалить чат
	void internal_remove_chat(Chat* chat);
	// добавить в чат лист
	void internal_add_chat(Chat* chat, bool favorite);
	// оповещаем о непрочитанных сообщениях несколько чатов
	void notify_unreaded_messages(const std::set<Chat*>& unreaded_chats);
	// оповещаем о непрочитанных сообщениях один чат
	void notify_unreaded_messages(Chat* chat);
	// кол-во непрочитанных сообщений во всех чатах
	int count_unreaded_messages() const;
};

// получить чат-лист
inline const Chats::chats_t& Chats::chat_list() const {
	return m_chat_list;
}