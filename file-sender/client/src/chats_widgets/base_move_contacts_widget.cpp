//=====================================================================================//
//   Author: open
//   Date:   02.06.2017
//=====================================================================================//
#include "base/precomp.h"
#include "base_move_contacts_widget.h"
#include "chat_item_widgets_manager.h"
#include "chat_item_widget.h"
#include "chat.h"

BaseMoveContactsWidget::BaseMoveContactsWidget(QListWidget* list_widget, QListWidgetItem* separator, ChatItemWidgetsManager& chat_item_widgets_manager)
: m_chat_item_widgets(chat_item_widgets_manager.create_chat_item_widgets())
, m_list_widget(list_widget)
, m_separator(separator)
, m_top(nullptr)
{
}
BaseMoveContactsWidget::~BaseMoveContactsWidget()
{

}
void BaseMoveContactsWidget::remove_chats()
{
	/*for (auto& pair : m_items)
	{
		QListWidgetItem* item = pair.second.get();
		QWidget* widget = m_list_widget->itemWidget(item);
		//m_list_widget->setItemWidget(item, nullptr);
		//QWidget* widget1 = m_list_widget->itemWidget(item);
		m_list_widget->removeItemWidget(item);
		//QWidget* widget2 = m_list_widget->itemWidget(item);
		//removeItemWidget

		//delete widget;
		__nop();
	}*/
	m_items.clear();
	m_top = nullptr;
	m_separator->setHidden(true);
}
void BaseMoveContactsWidget::remove_chat(const Chat* chat)
{
	items_t::iterator it = m_items.find(chat->id());
	if (it != m_items.end())
	{
		//m_order_items.erase(find_order_item(it->second.get()));

		if (it->second.get() == m_top)
			m_top = nullptr;

		m_items.erase(it);

		if (m_items.empty())
			m_separator->setHidden(true);
	}
}
void BaseMoveContactsWidget::set_chat_top(Chat* chat)
{
	items_t::iterator it = m_items.find(chat->id());
	if (it == m_items.end())	// если item не создан
	{
		m_separator->setHidden(false);
	
		ChatItemWidget* widget = m_chat_item_widgets->create_chat_item_widget(chat, true);
		UItem item = create_item(widget);
		insert_item(item.get(), widget);
		m_items.insert(std::make_pair(chat->id(), std::move(item)));
	}
	else
	{
		Item* item = it->second.get();
		//if (add_top ? item != top_item() : item != last_item())
		if (item != m_top)
		{
			if (ChatItemWidget* widget = get_widget(item))
			{
				QSignalBlocker blocker(m_list_widget);
				bool selected = item->isSelected();
	
				ChatItemWidget* new_widget = m_chat_item_widgets->clone_chat_item_widget(widget);
				// widget - удаляется на след. цикле выполнения
				m_list_widget->takeItem(m_list_widget->row(item));
				// удаляем из порядка
				//m_order_items.erase(find_order_item(item));
	
				insert_item(item, new_widget);
				item->setSelected(selected);
			}
		}
	}
}
BaseMoveContactsWidget::Item* BaseMoveContactsWidget::find_item(const Chat* chat) {
	items_t::iterator it = m_items.find(chat->id());
	if (it != m_items.end()) return it->second.get();
	return nullptr;
}
//BaseMoveContactsWidget::order_items_t::iterator BaseMoveContactsWidget::find_order_item(Item* item) {
//	return std::find(m_order_items.begin(), m_order_items.end(), item);
//}
BaseMoveContactsWidget::UItem BaseMoveContactsWidget::create_item(ChatItemWidget* widget)
{
	UItem item(new ChatListItem());
	item->setSizeHint(QSize(item->sizeHint().width(), widget->height()));
	return item;
}
void BaseMoveContactsWidget::insert_item(QListWidgetItem* item, ChatItemWidget* widget)
{
	int row = m_list_widget->row(m_separator) + 1;
	m_list_widget->insertItem(row, item);
	m_list_widget->setItemWidget(item, widget);
	item->setData(Qt::UserRole, QVariant::fromValue<ChatItemWidget*>(widget));
	m_top = item;
	//m_order_items.insert(add_top ? m_order_items.begin() : m_order_items.end(), item);
}
/*void BaseMoveContactsWidget::add_chat(Chat* chat)
{
	if (m_chat_items.find(chat->id()) == m_chat_items.end())
	{
		ChatItemWidget* widget = m_chat_item_widgets->create_chat_item_widget(chat, true);
		m_chat_items.insert(std::make_pair(chat->id(), create_item(widget)));
	}
	m_separator->setHidden(false);
}*/
//void BaseMoveContactsWidget::set_chat_order(Chat* chat, bool add_top)
//{
//	items_t::iterator it = m_items.find(chat->id());
//	if (it == m_items.end())	// если item не создан
//	{
//		m_separator->setHidden(false);
//
//		ChatItemWidget* widget = m_chat_item_widgets->create_chat_item_widget(chat, true);
//		UItem item = create_item(widget);
//		insert_item(item.get(), widget, add_top);
//		it = m_items.insert(std::make_pair(chat->id(), std::move(item))).first;
//	}
//	else
//	{
//		Item* item = it->second.get();
//		if (add_top ? item != top_item() : item != last_item())
//		{
//			if (ChatItemWidget* widget = get_widget(item))
//			{
//				QSignalBlocker blocker(m_list_widget);
//				bool selected = item->isSelected();
//
//				ChatItemWidget* new_widget = m_chat_item_widgets->clone_chat_item_widget(widget);
//				// widget - удаляется на след. цикле выполнения
//				m_list_widget->takeItem(m_list_widget->row(item));
//				// удаляем из порядка
//				m_order_items.erase(find_order_item(item));
//
//				insert_item(item, new_widget, add_top);
//				item->setSelected(selected);
//			}
//		}
//	}
//}
//void BaseMoveContactsWidget::insert_item(QListWidgetItem* item, ChatItemWidget* widget, bool add_top)
//{
//	int row = get_new_index_row(add_top);
//	m_list_widget->insertItem(row, item);
//	m_list_widget->setItemWidget(item, widget);
//	item->setData(Qt::UserRole, QVariant::fromValue<ChatItemWidget*>(widget));
//	m_order_items.insert(add_top ? m_order_items.begin() : m_order_items.end(), item);
//}
//int BaseMoveContactsWidget::get_new_index_row(bool add_top) const {
//	if (add_top || m_order_items.empty()) {
//		return m_list_widget->row(m_separator) + 1;
//	}
//	return m_list_widget->row(last_item()) + 1;
//}
//BaseMoveContactsWidget::Item* BaseMoveContactsWidget::top_item() const {
//	return m_order_items.empty() ? nullptr : m_order_items[0];
//}
//BaseMoveContactsWidget::Item* BaseMoveContactsWidget::last_item() const {
//	return m_order_items.empty() ? nullptr : m_order_items[m_order_items.size()-1];
//}
//void BaseMoveContactsWidget::set_chat_bottom(Chat* chat)
//{
//}
void BaseMoveContactsWidget::set_chat_selected(const Chat* chat, bool select)
{
	if (Item* item = find_item(chat))
	{
		if (select) {
			m_list_widget->setCurrentRow(m_list_widget->row(item));
			m_list_widget->scrollToItem(item);
		}
		else item->setSelected(select);
	}
}
/*void BaseMoveContactsWidget::set_scroll_to_chat(const Chat* chat)
{
	if (Item* item = find_item(chat))
		m_list_widget->scrollToItem(item);
}*/
void BaseMoveContactsWidget::set_chat_new_messages(const Chat* chat, bool new_messages)
{
	if (Item* item = find_item(chat))
		if (ChatItemWidget* widget = get_widget(item))
			widget->set_new_messages(new_messages);
}
ChatItemWidget* BaseMoveContactsWidget::get_widget(Item* item) {
	return item->data(Qt::UserRole).value<ChatItemWidget*>();
}