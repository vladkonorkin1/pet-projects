//=====================================================================================//
//   Author: open
//   Date:   02.06.2017
//=====================================================================================//
#pragma once
#include <QListWidget>
#include "chats_widgets/chat_item_widgets.h"

class ChatItemWidgetsManager;
class Chat;

class BaseMoveContactsWidget : public QObject
{
	Q_OBJECT
private:
	QListWidget* m_list_widget;
	QListWidgetItem* m_separator;
	UChatItemWidgets m_chat_item_widgets;

	using Item = QListWidgetItem;
	using UItem = std::unique_ptr<Item>;
	using items_t = std::map<ChatId, UItem>;
	//using order_items_t = std::vector<QListWidgetItem*>;
	
	QListWidgetItem* m_top;
	items_t m_items;
	//order_items_t m_order_items;

public:
	BaseMoveContactsWidget(QListWidget* list_widget, QListWidgetItem* separator, ChatItemWidgetsManager& chat_item_widgets_manager);
	virtual ~BaseMoveContactsWidget();

	void remove_chats();
	void remove_chat(const Chat* chat);
	void set_chat_top(Chat* chat);
	void set_chat_selected(const Chat* chat, bool select);
	//void set_scroll_to_chat(const Chat* chat);
	void set_chat_new_messages(const Chat* chat, bool new_messages);

private:
	UItem create_item(ChatItemWidget* widget);
	//order_items_t::iterator find_order_item(Item* item);
	Item* find_item(const Chat* chat);
	ChatItemWidget* get_widget(Item* item);
	void insert_item(QListWidgetItem* item, ChatItemWidget* widget);
	//Item* top_item() const;
	//Item* last_item() const;
	//void insert_item(QListWidgetItem* item, ChatItemWidget* widget, bool add_top);
	//int get_new_index_row(bool add_top) const;
};