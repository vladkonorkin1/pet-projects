//=====================================================================================//
//   Author: open
//   Date:   27.01.2018
//=====================================================================================//
#include "base/precomp.h"
#include "chat_elided_text.h"

ChatElidedText::ChatElidedText(const QFont& font, const QFont& font_bold)
: m_font_metrics_bold(font_bold)
, m_font_metrics(font)
, m_font_bold(font_bold)
, m_font(font)
, m_width(5000)
, m_bold(false)
{
}
/*ChatElidedText::ChatElidedText(const ChatElidedText& src)
: m_text(src.m_text)
, m_elided(src.m_elided)
, m_bold(m_bold)
, m_width(m_width)
, m_font_metrics(m_font_metrics)
, m_font_metrics_bold(m_font_metrics_bold)
{
}*/
void ChatElidedText::resize(int width)
{
	if (m_width != width)
	{
		m_width = width;
		update_elide();
	}
}
void ChatElidedText::set_text(const QString& text)
{
	if (m_text != text)
	{
		m_text = text;
		update_elide();
	}
}
void ChatElidedText::set_bold(bool bold)
{
	if (m_bold != bold)
	{
		m_bold = bold;
		update_elide();
	}
}
void ChatElidedText::update_elide()
{
	m_elided = font_metrics().elidedText(m_text, Qt::TextElideMode::ElideRight, m_width);
}
QFontMetrics& ChatElidedText::font_metrics()
{
	return m_bold ? m_font_metrics_bold : m_font_metrics;
}
const QFont& ChatElidedText::font() const
{
	return m_bold ? m_font_bold : m_font;
}
const QString& ChatElidedText::elided() const
{
	return m_elided;
}
const QString& ChatElidedText::text() const
{
	return m_text;
}