//=====================================================================================//
//   Author: open
//   Date:   27.01.2018
//=====================================================================================//
#pragma once
#include <QFontMetrics>

class ChatElidedText
{
private:
	QString m_text;
	QString m_elided;

	bool m_bold;
	int m_width;

	QFontMetrics m_font_metrics;
	QFontMetrics m_font_metrics_bold;

	const QFont& m_font;
	const QFont& m_font_bold;

public:
	ChatElidedText(const QFont& font, const QFont& font_bold);
	//ChatElidedText(const ChatElidedText& src);

	const QString& elided() const;
	const QString& text() const;
	void resize(int width);
	void set_text(const QString& text);
	void update_elide();
	void set_bold(bool bold);

	const QFont& font() const;

private:
	QFontMetrics& font_metrics();
};