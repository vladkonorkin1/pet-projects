//=====================================================================================//
//   Author: open
//   Date:   30.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "chat_item_context_menu.h"
#include "chat.h"
#include "chat_item_widget.h"

ChatItemContextMenu::ChatItemContextMenu(QWidget* parent)
: m_menu(parent)
, m_parent(parent)
, m_chat_id(0)
, m_chat_item_widget(nullptr)
{
	m_action_rename = new QAction(tr("Rename"), this);
	connect(m_action_rename, SIGNAL(triggered()), SLOT(slot_rename_chat()));
	m_menu.addAction(m_action_rename);

	m_action_remove_favorites = new QAction(tr("Remove from favorites"), this);
	connect(m_action_remove_favorites, SIGNAL(triggered()), SLOT(slot_remove_favorites()));
	m_menu.addAction(m_action_remove_favorites);

	m_action_add_favorites = new QAction(tr("Add to favorites"), this);
	connect(m_action_add_favorites, SIGNAL(triggered()), SLOT(slot_add_favorites()));
	m_menu.addAction(m_action_add_favorites);

	m_action_leave_group = new QAction(tr("Leave group"), this);
	connect(m_action_leave_group, SIGNAL(triggered()), SLOT(slot_leave_group_chat()));
	m_menu.addAction(m_action_leave_group);

	m_action_remove = new QAction(tr("Remove"), this);
	connect(m_action_remove, SIGNAL(triggered()), SLOT(slot_remove_chat()));
	m_menu.addAction(m_action_remove);
}
void ChatItemContextMenu::exec(ChatItemWidget* chat_item_widget, const Chat* chat, const QPoint& pos)
{
	m_chat_item_widget = chat_item_widget;
	m_chat_id = chat ? chat->id() : 0;
	if (chat && chat->is_enabled())
	{
		m_action_add_favorites->setVisible(!chat->favorite());
		m_action_remove_favorites->setVisible(chat->favorite());
		m_action_rename->setVisible(!chat->contact() && chat->is_admin());
		m_action_leave_group->setVisible(!chat->contact() && chat->is_participant());
		m_menu.popup(pos);
	}
}
void ChatItemContextMenu::slot_remove_chat()
{
	if (m_chat_id > 0) emit chat_remove(m_chat_id);
}
void ChatItemContextMenu::slot_add_favorites()
{
	if (m_chat_id > 0) emit chat_add_favorite(m_chat_id);
}
void ChatItemContextMenu::slot_remove_favorites()
{
	if (m_chat_id > 0) emit chat_remove_favorite(m_chat_id);
}
void ChatItemContextMenu::slot_rename_chat()
{
	if (m_chat_item_widget) m_chat_item_widget->open_renamer();
}
void ChatItemContextMenu::rename(ChatId chat_id, const QString& name)
{
	if (chat_id > 0) emit chat_rename(chat_id, name);
}
void ChatItemContextMenu::slot_leave_group_chat()
{
	if (m_chat_id > 0) emit group_chat_leave(m_chat_id);
}