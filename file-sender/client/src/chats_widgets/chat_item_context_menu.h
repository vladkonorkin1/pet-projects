//=====================================================================================//
//   Author: open
//   Date:   30.04.2017
//=====================================================================================//
#pragma once
#include <QMenu>

class Chat;
class ChatItemWidget;

class ChatItemContextMenu : public QObject
{
	Q_OBJECT
private:
	QWidget* m_parent;
	QMenu m_menu;
	QAction* m_action_rename;
	QAction* m_action_add_favorites;
	QAction* m_action_remove_favorites;
	QAction* m_action_leave_group;
	QAction* m_action_remove;

	ChatId m_chat_id;
	ChatItemWidget* m_chat_item_widget;

public:
	ChatItemContextMenu(QWidget* parent);

	void exec(ChatItemWidget* chat_item_widget, const Chat* chat, const QPoint& pos);
	void rename(ChatId chat_id, const QString& name);

signals:
	void chat_remove(ChatId chat_id);
	void chat_add_favorite(ChatId chat_id);
	void chat_remove_favorite(ChatId chat_id);
	void chat_rename(ChatId chat_id, const QString& name);
	void group_chat_leave(ChatId chat_id);

private slots:
	void slot_remove_chat();
	void slot_add_favorites();
	void slot_remove_favorites();
	void slot_rename_chat();
	void slot_leave_group_chat();
};