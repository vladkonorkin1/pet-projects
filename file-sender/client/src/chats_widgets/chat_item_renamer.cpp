//=====================================================================================//
//   Author: open
//   Date:   04.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "chat_item_renamer.h"
#include <QFocusEvent>
#include <array>

ChatItemRenamer::ChatItemRenamer(QWidget* parent)
: QLineEdit(parent)
, m_escape(false)
{
	//setContextMenuPolicy(Qt::ContextMenuPolicy::NoContextMenu);
	connect(this, SIGNAL(editingFinished()), SLOT(slot_finish_edit()));
	//setPlaceholderText(tr("You must to think up a good name :)"));
	setPlaceholderText(get_random_placeholder_text());
	setStyleSheet("QLineEdit{background: white; border: 1px solid #d8e5ef; padding: 0 1 0 1;}");

	static QFont font10("MS shell Dlg 2", 10);
	setFont(font10);
}
void ChatItemRenamer::show(const QString& name, int width)
{
	m_escape = false;
	set_width(width);
	setText(name);
	setSelection(0, name.size());
	
	QLineEdit::show();
	setFocus();
}
void ChatItemRenamer::set_width(int width)
{
	setGeometry(QRect(30, 8, width - 38, 24));
}
void ChatItemRenamer::slot_finish_edit()
{
	emit finish(true, false);
}
void ChatItemRenamer::focusOutEvent(QFocusEvent* event)
{
	if (event->reason() == Qt::FocusReason::PopupFocusReason)
	{
		event->accept();
	}
	else emit finish(false, !m_escape);
}
void ChatItemRenamer::keyPressEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Escape)
	{
		m_escape = true;
		clearFocus();
	}
	QLineEdit::keyPressEvent(event);
}
QString ChatItemRenamer::get_random_placeholder_text() const
{
	static const int size = 10;
	static std::array<QString, size> placeholders = {
		tr("You must to think up a good name :)"),
		tr("You don't have time to think up there"),
		tr("When did you start to think up this plan?"),
		tr("How long did it take to think up that brilliant move?"),
		tr("This time I think up something for myself"),
		tr("Think up the best story he's ever heard"),
		tr("It's never too late, sir, you could always think up some new turns"),
		tr("You can think up something, can't you?"),
		tr("We must think up a reason why the police weren't informed before now"),
		tr("Always ask for time to think"),
	};
	return placeholders[rand() % size];
}