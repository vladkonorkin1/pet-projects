//=====================================================================================//
//   Author: open
//   Date:   04.08.2017
//=====================================================================================//
#pragma once
#include <QLineEdit>

class ChatItemRenamer : public QLineEdit
{
	Q_OBJECT
private:
	bool m_escape;

public:
	ChatItemRenamer(QWidget* parent = nullptr);
	void show(const QString& name, int width);
	void set_width(int width);

signals:
	void finish(bool ok, bool focus_changed);

private slots:
	void slot_finish_edit();

protected:
	virtual void focusOutEvent(QFocusEvent* event);
	virtual void keyPressEvent(QKeyEvent* event);

private:
	QString get_random_placeholder_text() const;
};