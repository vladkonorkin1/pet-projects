//=====================================================================================//
//   Author: open
//   Date:   21.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "chat_item_widget.h"
#include "chat_item_context_menu.h"
#include "chat_item_widgets.h"
#include "chat_item_renamer.h"
#include <QContextMenuEvent>
#include <QPainter>
#include "chat.h"
#include <array>

ChatListItem::~ChatListItem()
{
	if (ChatItemWidget* widget = data(Qt::UserRole).value<ChatItemWidget*>()) {
		// удаляем виджет принудительно, иначе удаляется только на следующем цикле выполнения
		delete widget;
		// при удалении элементов постоянно меняется 'текущий-выделенный' и он натыкается на битую ссылку... поэтому data подчищаем
		setData(Qt::UserRole, QVariant::fromValue<ChatItemWidget*>(nullptr));
	}
}

ChatItemWidget::ChatItemWidget(Chat* chat, ChatItemContextMenu* menu, ChatItemWidgets* manager)
: m_contact(chat->contact())
, m_manager(manager)
, m_chat(chat)
, m_menu(menu)
//, m_status(User::Status::Group)
, m_name(create_elide_text_name())
, m_new_messages(chat->count_unreaded_messages() > 0)
{
	m_name.set_bold(chat->count_unreaded_messages() > 0);

	setFixedHeight(40);

	if (const User* contact = chat->contact())
	{
		setup_user(contact);
	}
	else
	{
		m_name.set_text(chat->name());
		
		
		
		/*//srand(2);
		//if (rand() % 100 < 50) set_new_messages(true);
		static int first = 0;
		++first;
		if (first == 1 || first == 1)
		{
			set_new_messages(true);
		}*/
	}
	//setFocusPolicy(Qt::FocusPolicy::NoFocus);
}
ChatItemWidget::ChatItemWidget(const User* contact, ChatItemContextMenu* menu, ChatItemWidgets* manager)
: m_contact(contact)
, m_manager(manager)
, m_chat(nullptr)
, m_menu(menu)
, m_name(create_elide_text_name())
, m_new_messages(false)
{
	setFixedHeight(40);
	setup_user(contact);

	//m_name.set_bold(chat->count_unreaded_messages() > 0);

	//setFocusPolicy(Qt::FocusPolicy::NoFocus);
}
ChatItemWidget::ChatItemWidget(ChatItemWidget& src)
: m_contact(src.m_contact)
, m_manager(src.m_manager)
, m_chat(src.m_chat)
, m_menu(src.m_menu)
, m_name(src.m_name)
, m_department(std::move(src.m_department))
//, m_status(src.m_status)
, m_new_messages(src.m_new_messages)
{
	setFixedHeight(40);
	//setFocusPolicy(Qt::FocusPolicy::NoFocus);
}
ChatItemWidget::~ChatItemWidget()
{
	if (m_manager) m_manager->unregister_chat_item_widget(this);
}
void ChatItemWidget::set_chat(Chat* chat)
{
	if (chat)
	{
		if (const User* user = chat->contact())
			set_name(user->name);
		else
			set_name(chat->name());

		set_new_messages(chat->count_unreaded_messages() > 0);
	}
	if (m_chat != chat)
	{
		emit chat_changed(this, m_chat, chat);
		m_chat = chat;

		
	}
}
void ChatItemWidget::setup_user(const User* user)
{
	m_name.set_text(user->name);
	m_department = create_elide_text_department();
	m_department->set_text(user->department->name);
	internal_set_online(user->online);
}
ChatElidedText ChatItemWidget::create_elide_text_name() const
{
	return ChatElidedText(m_manager->font_name, m_manager->font_name_bold);
}
ChatItemWidget::UChatElidedText ChatItemWidget::create_elide_text_department()
{
	return UChatElidedText(new ChatElidedText(m_manager->font_Department, m_manager->font_Department));
}
void ChatItemWidget::internal_set_online(bool online) {
	//m_status = online ? User::Status::Online : User::Status::Offline;

}
void ChatItemWidget::set_online(bool online) {
	internal_set_online(online);
	repaint();
}
void ChatItemWidget::set_name(const QString& name)
{
	m_name.set_text(name);
	repaint();
}
void ChatItemWidget::set_manager(ChatItemWidgets* manager) {
	m_manager = manager;
}

void ChatItemWidget::paintEvent(QPaintEvent*)
{
	QPainter painter(this);

	const QPixmap& status_pixmap = get_status_pixmaps();
	painter.drawPixmap(9, 12, status_pixmap);

	if (!m_renamer || (m_renamer && !m_renamer->isVisible()))
	{
		painter.setFont(m_name.font());
		painter.drawText(33, m_department.get() ? 1 : 8, 800, 20, Qt::AlignVCenter, m_name.elided());
	}
	if (m_department.get())
	{
		painter.setFont(m_department->font());
		painter.setPen(QPen(QColor(63, 63, 63)));
		painter.drawText(33, 17, 800, 20, Qt::AlignVCenter, m_department->elided());
		//opt.palette.setColor()
		//style()->drawItemText(&painter, QRect(33, 18, 800, 20), Qt::AlignVCenter, opt.palette, true, *m_Department, foregroundRole());
	}

	if (m_new_messages)
	{
		painter.setRenderHint(QPainter::Antialiasing);
		painter.setPen(QPen(QColor(255, 140, 0)));
		painter.setBrush(QBrush(QColor(255, 140, 0)));
		painter.drawEllipse(size().width() - (16 + 3), 16, 7, 7);
	}
}
const QPixmap& ChatItemWidget::get_status_pixmaps() const
{
	static QPixmap s_pixmap_status_offline(QString::fromUtf8(":/contact_status_offline.png"));
	static QPixmap s_pixmap_status_online(QString::fromUtf8(":/contact_status_online.png"));
	static QPixmap s_pixmap_group(QString::fromUtf8(":/contact_group.png"));

	if (m_contact)
		return m_contact->online ? s_pixmap_status_online : s_pixmap_status_offline;

	if (m_chat)
	{
		if (const User* contact = m_chat->contact())
			return contact->online ? s_pixmap_status_online : s_pixmap_status_offline;
	}
	return s_pixmap_group;
	
}
void ChatItemWidget::contextMenuEvent(QContextMenuEvent* event)
{
	if (m_menu) m_menu->exec(this, m_chat, event->globalPos());
}
void ChatItemWidget::open_renamer()
{
	if (m_chat && !m_chat->contact())
	{
		if (!m_renamer)
		{
			m_renamer.reset(new ChatItemRenamer(this));
			connect(m_renamer.get(), SIGNAL(finish(bool, bool)), SLOT(slot_renamer_finish_edit(bool, bool)));
		}
		m_renamer->show(m_name.text(), width());
	}
}
void ChatItemWidget::slot_renamer_finish_edit(bool ok, bool focus_changed)
{
	if (m_chat && !m_chat->contact())
		if (ok) m_menu->rename(m_chat->id(), m_renamer->text());
	m_renamer->hide();
	
	if (!focus_changed)
		setFocus();
}
void ChatItemWidget::keyPressEvent(QKeyEvent* event)
{
	if (m_renamer && m_renamer->isVisible())
		event->accept();
	//else if (event->key() == Qt::Key_F2)
	//	open_renamer();
	else QWidget::keyPressEvent(event);
}
void ChatItemWidget::resizeEvent(QResizeEvent* event)
{
	QWidget::resizeEvent(event);
	int width = event->size().width();
	if (m_renamer) m_renamer->set_width(width);

	update_text_width();
}
/*void ChatItemWidget::mouseReleaseEvent(QMouseEvent* event)
{
	//std::cout << m_name.toStdString() << std::endl;
}*/
void ChatItemWidget::set_new_messages(bool new_messages)
{
	if (new_messages != m_new_messages)
	{
		m_new_messages = new_messages;
		m_name.set_bold(new_messages);
		update_text_width();
		repaint();
	}
}
void ChatItemWidget::update_text_width()
{
	int width = size().width();
	int offset = 33 + (m_new_messages ? 23 : 9);
	m_name.resize(width - offset);
	if (m_department.get())
		m_department->resize(width - offset);
}