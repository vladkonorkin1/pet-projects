//=====================================================================================//
//   Author: open
//   Date:   21.04.2017
//=====================================================================================//
#pragma once
#include "user.h"
#include "chats_widgets/chat_elided_text.h"

#include <QListWidgetItem>
class ChatListItem : public QListWidgetItem
{
public:
	virtual ~ChatListItem();
};

class ChatItemContextMenu;
class ChatItemWidgets;
class ChatItemRenamer;
class Chat;

class ChatItemWidget : public QWidget
{
	Q_OBJECT
private:
	const User* m_contact;
	Chat* m_chat;
	//User::Status m_status;

	ChatItemContextMenu* m_menu;
	ChatItemWidgets* m_manager;


	ChatElidedText m_name;
	using UChatElidedText = std::auto_ptr<ChatElidedText>;
	UChatElidedText m_department;
	bool m_new_messages;


	using UChatItemRenamer = std::unique_ptr<ChatItemRenamer>;
	UChatItemRenamer m_renamer;
	
public:
	ChatItemWidget(Chat* chat, ChatItemContextMenu* menu, ChatItemWidgets* manager);
	ChatItemWidget(const User* contact, ChatItemContextMenu* menu, ChatItemWidgets* manager);
	ChatItemWidget(ChatItemWidget& src);
	virtual ~ChatItemWidget();

	const User* contact() const;
	const Chat* chat() const;
	void set_chat(Chat* chat);

	void set_online(bool online);
	void set_name(const QString& name);

	void set_manager(ChatItemWidgets* manager);

	// открыть переименование группового чата
	void open_renamer();
	
	void set_new_messages(bool new_messages);

signals:
	void chat_changed(ChatItemWidget* widget, Chat* old_chat, Chat* new_chat);

private slots:
	void slot_renamer_finish_edit(bool ok, bool focus_changed);

protected:
	virtual void contextMenuEvent(QContextMenuEvent* event);
	virtual void paintEvent(QPaintEvent* event);
	virtual void keyPressEvent(QKeyEvent* event);
	virtual void resizeEvent(QResizeEvent* event);
	//virtual void mouseReleaseEvent(QMouseEvent* event);

private:
	void setup_user(const User* user);
	void internal_set_online(bool online);
	ChatElidedText create_elide_text_name() const;
	UChatElidedText create_elide_text_department();
	void update_text_width();
	const QPixmap& get_status_pixmaps() const;
};


inline const Chat* ChatItemWidget::chat() const {
	return m_chat;
}
inline const User* ChatItemWidget::contact() const {
	return m_contact;
}