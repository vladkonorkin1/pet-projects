//=====================================================================================//
//   Author: open
//   Date:   11.07.2017
//=====================================================================================//
#include "base/precomp.h"
#include "chat_item_widgets.h"
#include "chat_item_widget.h"
#include "chats.h"
#include "users.h"

ChatItemWidgets::ChatItemWidgets(Users& users, Chats& chats, ChatItemContextMenu* context_menu)
: m_context_menu(context_menu)
, m_chats(chats)
, font_name("Segoe UI", 10)	//MS shell Dlg 2
, font_name_bold("Segoe UI", 10, 68)
, font_Department("Segoe UI", 8)
{
	//font_name_bold.setBold(true);
	connect(&users, SIGNAL(changed_online(const User*)), SLOT(slot_user_change_online(const User*)));
	//connect(&chats, SIGNAL(chat_change_name(Chat*, const QString&)), SLOT(slot_chat_change_name(Chat*, const QString&)));
	connect(&chats, SIGNAL(chat_created(Chat*)), SLOT(slot_chat_created(Chat*)));
}
ChatItemWidgets::~ChatItemWidgets()
{
	for (auto& pair : m_contact_chat_items)
		pair.second->set_manager(nullptr);

	for (auto& pair : m_chat_items)
		pair.second->set_manager(nullptr);
}
void ChatItemWidgets::set_contact_chat_item(const User* user, ChatItemWidget* widget)
{
	if (ChatItemWidget* old_widget = m_contact_chat_items[user->id])
		old_widget->set_manager(nullptr);
	m_contact_chat_items[user->id] = widget;
}
ChatItemWidget* ChatItemWidgets::create_chat_item_widget(const User* contact, bool add_context_menu)
{
	if (Chat* chat = m_chats.find_chat_by_contact(contact->id)) return create_chat_item_widget(chat, add_context_menu);

	ChatItemWidget* widget = new ChatItemWidget(contact, add_context_menu ? m_context_menu : nullptr, this);
	connect(widget, SIGNAL(chat_changed(ChatItemWidget*, Chat*, Chat*)), SLOT(slot_widget_chat_changed(ChatItemWidget*, Chat*, Chat*)));

	set_contact_chat_item(contact, widget);
	return widget;
}
ChatItemWidget* ChatItemWidgets::create_chat_item_widget(Chat* chat, bool add_context_menu)
{
	ChatItemWidget* widget = new ChatItemWidget(chat, add_context_menu ? m_context_menu : nullptr, this);
	connect(widget, SIGNAL(chat_changed(ChatItemWidget*, Chat*, Chat*)), SLOT(slot_widget_chat_changed(ChatItemWidget*, Chat*, Chat*)));

	set_chat_item(chat, widget);
	if (const User* contact = chat->contact())
		set_contact_chat_item(contact, widget);
	return widget;
}
ChatItemWidget* ChatItemWidgets::clone_chat_item_widget(ChatItemWidget* old_widget)
{
	ChatItemWidget* new_widget = new ChatItemWidget(*old_widget);
	connect(new_widget, SIGNAL(chat_changed(ChatItemWidget*, Chat*, Chat*)), SLOT(slot_widget_chat_changed(ChatItemWidget*, Chat*, Chat*)));
	old_widget->set_manager(nullptr);
	if (const User* user = new_widget->contact())
		set_contact_chat_item(user, new_widget);
	
	if (new_widget->chat())
		set_chat_item(new_widget->chat(), new_widget);
	return new_widget;
}
void ChatItemWidgets::unregister_chat_item_widget(ChatItemWidget* widget)
{
	if (const User* contact = widget->contact())
	{
		contact_chat_items_t::iterator it = m_contact_chat_items.find(contact->id);
		if (it != m_contact_chat_items.end())
			m_contact_chat_items.erase(it);
	}
	if (widget->chat())
		set_chat_item(widget->chat(), nullptr);
}
void ChatItemWidgets::slot_user_change_online(const User* user)
{
	contact_chat_items_t::iterator it = m_contact_chat_items.find(user->id);
	if (it != m_contact_chat_items.end())
		it->second->set_online(user->online);
}
void ChatItemWidgets::set_chat_item(const Chat* chat, ChatItemWidget* widget)
{
	assert( chat );
	if (widget) m_chat_items[chat->id()] = widget;
	else
	{
		chat_items_t::iterator it = m_chat_items.find(chat->id());
		if (it != m_chat_items.end())
			m_chat_items.erase(it);
		else assert(0);
	}
}
/*void ChatItemWidgets::slot_chat_change_name(Chat* chat, const QString& name)
{
	chat_items_t::iterator it = m_chat_items.find(chat->id());
	if (it != m_chat_items.end())
		it->second->set_name(name);
}*/
void ChatItemWidgets::slot_chat_created(Chat* chat)
{
	// для виджетов чата обновляем запись о чате
	if (const User* contact = chat->contact())
	{
		contact_chat_items_t::iterator it = m_contact_chat_items.find(contact->id);
		if (it != m_contact_chat_items.end())
		{
			ChatItemWidget* item_widget = it->second;
			// если чата ещё нет
			if (!item_widget->chat())
				item_widget->set_chat(chat);
			else assert( item_widget->chat() == chat );
		}
	}

	// обновляем имя группового чата
	if (!chat->contact())
	{
		auto it = m_chat_items.find(chat->id());
		if (m_chat_items.end() != it)
		{
			ChatItemWidget* item = it->second;
			item->set_name(chat->name());
		}
	}
}
void ChatItemWidgets::slot_widget_chat_changed(ChatItemWidget* widget, Chat* old_chat, Chat* new_chat)
{
	if (old_chat)
		set_chat_item(old_chat, nullptr);
	if (new_chat)
		set_chat_item(new_chat, widget);
}