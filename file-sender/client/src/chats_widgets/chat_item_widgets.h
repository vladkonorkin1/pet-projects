//=====================================================================================//
//   Author: open
//   Date:   11.07.2017
//=====================================================================================//
#pragma once
#include <QFont>

class ChatItemContextMenu;
class ChatItemWidget;
class Users;
class User;
class Chats;
class Chat;

class ChatItemWidgets : public QObject
{
	Q_OBJECT
public:
	QFont font_name;
	QFont font_name_bold;
	QFont font_Department;

private:
	ChatItemContextMenu* m_context_menu;
	Chats& m_chats;

	using chat_items_t = std::map<ChatId, ChatItemWidget*>;
	chat_items_t m_chat_items;

	using contact_chat_items_t = std::map<UserId, ChatItemWidget*>;
	contact_chat_items_t m_contact_chat_items;

public:
	ChatItemWidgets(Users& users, Chats& chats, ChatItemContextMenu* context_menu);
	virtual ~ChatItemWidgets();

	ChatItemWidget* create_chat_item_widget(const User* user, bool add_context_menu);
	ChatItemWidget* create_chat_item_widget(Chat* chat, bool add_context_menu);
	ChatItemWidget* clone_chat_item_widget(ChatItemWidget* widget);
	void unregister_chat_item_widget(ChatItemWidget* widget);

private slots:
	void slot_user_change_online(const User* user);
	void slot_widget_chat_changed(ChatItemWidget* widget, Chat* old_chat, Chat* new_chat);
	//void slot_chat_change_name(Chat* chat, const QString& name);
	void slot_chat_created(Chat* chat);

private:
	void set_contact_chat_item(const User* user, ChatItemWidget* widget);
	void set_chat_item(const Chat* chat, ChatItemWidget* widget);
};
using UChatItemWidgets = std::unique_ptr<ChatItemWidgets>;