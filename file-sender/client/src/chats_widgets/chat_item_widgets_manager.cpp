//=====================================================================================//
//   Author: open
//   Date:   11.07.2017
//=====================================================================================//
#include "base/precomp.h"
#include "chat_item_widgets_manager.h"
#include "chats.h"

ChatItemWidgetsManager::ChatItemWidgetsManager(Users& users, Chats& chats, QWidget* context_menu_parent)
: m_users(users)
, m_chats(chats)
, m_context_menu(context_menu_parent)
{
	connect(&m_context_menu, SIGNAL(chat_remove(ChatId)), SLOT(slot_need_chat_remove(ChatId)));
	connect(&m_context_menu, SIGNAL(chat_add_favorite(ChatId)), SLOT(slot_need_chat_add_favorite(ChatId)));
	connect(&m_context_menu, SIGNAL(chat_remove_favorite(ChatId)), SLOT(slot_need_chat_remove_favorite(ChatId)));
	connect(&m_context_menu, SIGNAL(chat_rename(ChatId, const QString&)), SLOT(slot_need_chat_rename(ChatId, const QString&)));
	connect(&m_context_menu, SIGNAL(group_chat_leave(ChatId)), SLOT(slot_need_group_chat_leave(ChatId)));
}
UChatItemWidgets ChatItemWidgetsManager::create_chat_item_widgets()
{
	return UChatItemWidgets(new ChatItemWidgets(m_users, m_chats, &m_context_menu));
}
void ChatItemWidgetsManager::slot_need_chat_remove(ChatId chat_id)
{
	if (const Chat* chat = m_chats.find_chat(chat_id))
	{
		emit need_chat_remove(chat);
	}
}
void ChatItemWidgetsManager::slot_need_group_chat_leave(ChatId chat_id)
{
	if (const Chat* chat = m_chats.find_chat(chat_id))
	{
		assert( !chat->contact() );
		emit need_group_chat_leave(chat);
	}
}
void ChatItemWidgetsManager::slot_need_chat_add_favorite(ChatId chat_id)
{
	if (const Chat* chat = m_chats.find_chat(chat_id))
		emit need_chat_add_favorite(chat);
}
void ChatItemWidgetsManager::slot_need_chat_remove_favorite(ChatId chat_id)
{
	if (const Chat* chat = m_chats.find_chat(chat_id))
		emit need_chat_remove_favorite(chat);
}
void ChatItemWidgetsManager::slot_need_chat_rename(ChatId chat_id, const QString& name)
{
	if (const Chat* chat = m_chats.find_chat(chat_id))
		emit need_chat_rename(chat, name);
}