//=====================================================================================//
//   Author: open
//   Date:   11.07.2017
//=====================================================================================//
#pragma once
#include "chats_widgets/chat_item_context_menu.h"
#include "chats_widgets/chat_item_widgets.h"

class ChatItemWidgetsManager : public QObject
{
	Q_OBJECT
private:
	Users& m_users;
	Chats& m_chats;
	ChatItemContextMenu m_context_menu;
	
public:
	ChatItemWidgetsManager(Users& users, Chats& chats, QWidget* context_menu_parent);
	UChatItemWidgets create_chat_item_widgets();

signals:
	void need_chat_remove(const Chat* chat);
	void need_chat_add_favorite(const Chat* chat);
	void need_chat_remove_favorite(const Chat* chat);
	void need_chat_rename(const Chat* chat, const QString&);
	void need_group_chat_leave(const Chat* chat);

private slots:
	void slot_need_chat_remove(ChatId chat_id);
	void slot_need_chat_add_favorite(ChatId chat_id);
	void slot_need_chat_remove_favorite(ChatId chat_id);
	void slot_need_chat_rename(ChatId chat_id, const QString& name);
	void slot_need_group_chat_leave(ChatId chat_id);
};