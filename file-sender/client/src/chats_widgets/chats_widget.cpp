//=====================================================================================//
//   Author: open
//   Date:   20.06.2017
//=====================================================================================//
#include "base/precomp.h"
#include "chats_widget.h"
#include "gui_messages/messages_widget_manager.h"
#include "contact_separator.h"
#include "chat_item_widget.h"
#include "chats.h"

ChatsWidget::ChatsWidget(Chats* chats, QListWidget* list_widget, MessagesWidgetManager* messages_widget_manager, const SearchContactsWidgetGerm& search_germ)
: m_chats_list(list_widget)
, m_search_list(search_germ.list)
, m_favorites_separator(add_separator(tr("Favorites")))
, m_favorites_widget(list_widget, m_favorites_separator.get(), search_germ.chat_item_widgets_manager)
, m_recents_separator(add_separator(tr("Recents")))
, m_recents_widget(list_widget, m_recents_separator.get(), search_germ.chat_item_widgets_manager)
, m_current_chat(messages_widget_manager)
, m_search_contacts_widget(search_germ, m_current_chat)
, m_chats(chats)
, m_initialization(false)
{
	connect(chats, SIGNAL(chat_added(Chat*)), SLOT(slot_chat_added(Chat*)));
	connect(chats, SIGNAL(chat_removed(Chat*)), SLOT(slot_chat_removed(Chat*)));
	connect(chats, SIGNAL(chat_change_count_new_messages(Chat*, int)), SLOT(slot_chat_change_count_new_messages(Chat*, int)));

	connect(m_chats_list, SIGNAL(itemSelectionChanged()), SLOT(slot_item_selection_changed()));

	connect(&m_search_contacts_widget, SIGNAL(show_search_list(bool)), SLOT(slot_show_search_list(bool)));
	connect(&m_search_contacts_widget, SIGNAL(select_contact(const User*)), SLOT(slot_search_contacts_widget_select_contact(const User*)));
	connect(&m_search_contacts_widget, SIGNAL(select_chat(const Chat*)), SLOT(slot_search_contacts_widget_select_chat(const Chat*)));
	connect(&m_search_contacts_widget, SIGNAL(request_users_online_statuses(const std::vector<const User*>&)), SIGNAL(request_users_online_statuses(const std::vector<const User*>&)));
}
ChatsWidget::~ChatsWidget()
{
	disconnect(m_chats_list, SIGNAL(itemSelectionChanged()), this, SLOT(slot_item_selection_changed()));
}
void ChatsWidget::slot_item_selection_changed()
{
	m_current_chat.process_items_selected(m_chats_list);
}
QListWidgetItem* ChatsWidget::add_separator(const QString& name)
{
	ContactSeparator* separator = new ContactSeparator(name);
	QListWidgetItem* item = new QListWidgetItem();
	item->setSizeHint(QSize(item->sizeHint().width(), separator->height()));
	//item->setFlags(Qt::ItemFlag::ItemIsEnabled);
	item->setFlags(Qt::ItemFlag::NoItemFlags);
	item->setData(Qt::UserRole, QVariant::fromValue<ContactSeparator*>(separator));
	m_chats_list->addItem(item);
	m_chats_list->setItemWidget(item, separator);
	item->setHidden(true);
	return item;
}
void ChatsWidget::slot_chat_removed(Chat* chat)
{
	// при удалении текущего чата сбрасываем выделение
	//if (m_chats_list->isVisible() && m_current_chat.chat() == chat)
	//	m_current_chat.set_current_chat(nullptr);
	
	if (m_initialization) return;
	m_favorites_widget.remove_chat(chat);
	m_recents_widget.remove_chat(chat);
}
void ChatsWidget::slot_show_search_list(bool show_search_list)
{
	m_current_chat.select_current_chat_item(show_search_list ? m_search_list : m_chats_list);
}
const CurrentChat* ChatsWidget::current() const {
	return &m_current_chat;
}
void ChatsWidget::set_chat_selected(const Chat* chat, bool select)//, bool scroll)
{
	if (chat->is_enabled()) {
		m_favorites_widget.set_chat_selected(chat, select);
		m_recents_widget.set_chat_selected(chat, select);
	}
	else
	{
		// после реконнекта снимаем select, и указываем что текущий чат пустой (такая ситуация возникает из-за удаления всех элементов при блокировке сигналов)
		m_chats_list->clearSelection();
		m_current_chat.set_current_chat(nullptr);
	}
}
void ChatsWidget::slot_search_contacts_widget_select_chat(const Chat* chat)
{
	set_chat_selected(chat, true);
}
void ChatsWidget::slot_search_contacts_widget_select_contact(const User* contact)
{
	emit need_add_chat_to_chat_list(contact);
}
void ChatsWidget::slot_chat_change_count_new_messages(Chat* chat, int count)
{
	m_favorites_widget.set_chat_new_messages(chat, count > 0);
	m_recents_widget.set_chat_new_messages(chat, count > 0);
}
void ChatsWidget::start_update()
{
	m_initialization = true;
	m_futures_chats.clear();

}
#include <QScrollBar>
void ChatsWidget::finish_update()
{
	// запоминаем позицию скролла
	int scroll_pos = m_chats_list->verticalScrollBar()->value();
	{
		// не оповещаем об изменении выделенного
		QSignalBlocker blocker(m_chats_list);
		// удаляем все чаты
		m_favorites_widget.remove_chats();
		m_recents_widget.remove_chats();
	}

	m_initialization = false;
	for (Chat* chat : m_futures_chats)
		add_chat(chat);

	if (const Chat* chat = m_current_chat.chat())
		set_chat_selected(chat, true);

	m_chats_list->verticalScrollBar()->setValue(scroll_pos);
}
void ChatsWidget::slot_chat_added(Chat* chat)//, bool add_top, bool favorite_changed)
{
	if (m_initialization) m_futures_chats.push_back(chat);
	else set_chat_top(chat);
}
void ChatsWidget::add_chat(Chat* chat)
{
	if (chat->favorite()) m_favorites_widget.set_chat_top(chat);
	else m_recents_widget.set_chat_top(chat);
}
void ChatsWidget::set_chat_top(Chat* chat)
{
	// если чат текущий, то обязательно его выделяем (актуально при переносе в избранное/recents или при поднятии чата в top)
	bool select = m_current_chat.chat() == chat;
	{
		// не оповещаем об изменении текущего
		QSignalBlocker blocker(m_chats_list);
		// проверка... чат мог уже существовать в другом списке, удаляем его оттуда
		if (chat->favorite())
			m_recents_widget.remove_chat(chat);
		else
			m_favorites_widget.remove_chat(chat);
	}
	add_chat(chat);
	if (select) set_chat_selected(chat, true);
}