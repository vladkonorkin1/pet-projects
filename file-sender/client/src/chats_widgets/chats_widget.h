//=====================================================================================//
//   Author: open
//   Date:   20.06.2017
//=====================================================================================//
#pragma once
#include "chats_widgets/chat_item_widgets_manager.h"
#include "chats_widgets/search_contacts_widget.h"
#include "chats_widgets/favorites_widget.h"
#include "chats_widgets/recents_widget.h"
#include "chats_widgets/current_chat.h"

class MessagesWidgetManager;
class QListWidgetItem;
class QListWidget;
class Chats;
class Chat;

class ChatsWidget : public QObject
{
	Q_OBJECT
private:
	QListWidget* m_chats_list;
	QListWidget* m_search_list;
	CurrentChat m_current_chat;
	SearchContactsWidget m_search_contacts_widget;
	Chats* m_chats;

	std::unique_ptr<QListWidgetItem> m_favorites_separator;
	FavoritesWidget m_favorites_widget;

	std::unique_ptr<QListWidgetItem> m_recents_separator;
	RecentsWidget m_recents_widget;

	std::vector<Chat*> m_futures_chats;
	bool m_initialization;

public:
	ChatsWidget(Chats* chats, QListWidget* list_widget, MessagesWidgetManager* messages_widget_manager, const SearchContactsWidgetGerm& search_germ);
	virtual ~ChatsWidget();

	const CurrentChat* current() const;
	SearchContactsWidget* search_contacts_widget() { return &m_search_contacts_widget; }
	void set_chat_selected(const Chat* chat, bool select);

	void start_update();
	void finish_update();
	void set_chat_top(Chat* chat);

signals:
	// необходимо добавить чат в chat-list
	void need_add_chat_to_chat_list(const User* contact);
	// запрос необходимости обновления онлайн-статусов
	void request_users_online_statuses(const std::vector<const User*>& users);

private slots:
	void slot_item_selection_changed();
	void slot_show_search_list(bool show_search_list);
	void slot_chat_removed(Chat* chat);
	void slot_chat_added(Chat* chat);
	void slot_chat_change_count_new_messages(Chat* chat, int count);
	void slot_search_contacts_widget_select_chat(const Chat* chat);
	void slot_search_contacts_widget_select_contact(const User* contact);

private:
	void add_chat(Chat* chat);
	QListWidgetItem* add_separator(const QString& name);
};