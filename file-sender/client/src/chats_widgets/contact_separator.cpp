//=====================================================================================//
//   Author: open
//   Date:   02.06.2017
//=====================================================================================//
#include "base/precomp.h"
#include "contact_separator.h"

ContactSeparator::ContactSeparator(const QString& name)
: m_ui(new Ui::ContactSeparator())
{
	m_ui->setupUi(this);
	m_ui->label_name->setText(name);
}