//=====================================================================================//
//   Author: open
//   Date:   02.06.2017
//=====================================================================================//
#pragma once
#include "ui_contact_separator.h"

namespace Ui { class ContactSeparator; }

class ContactSeparator : public QWidget
{
	Q_OBJECT
private:
	std::auto_ptr<Ui::ContactSeparator> m_ui;

public:
	ContactSeparator(const QString& name);
	~ContactSeparator()
	{
	}
};