//=====================================================================================//
//   Author: open
//   Date:   24.06.2017
//=====================================================================================//
#include "base/precomp.h"
#include "current_chat.h"
#include "gui_messages/messages_widget_manager.h"
#include "chat_item_widget.h"
#include <QListWidget>
#include "chat.h"
#include "contact_separator.h"

CurrentChat::CurrentChat(MessagesWidgetManager* messages_widget_manager)
: m_messages_widget_manager(messages_widget_manager)
, m_contact(nullptr)
, m_chat(nullptr)
{
}
void CurrentChat::process_items_selected(QListWidget* list_widget)
{
	QList<QListWidgetItem*> items = list_widget->selectedItems();

	//std::cout << QDateTime::currentDateTime().time().toString().toStdString() << " " << qPrintable(list_widget->objectName()) << ": " << items.size() << "  ";
	
	if (!items.empty())
	{
		for (auto item : items)
		{
			if (ChatItemWidget* widget = item->data(Qt::UserRole).value<ChatItemWidget*>())
			{
				//std::cout << qPrintable(widget->chat() ? widget->chat()->name() : widget->contact()->name) << " ";
				//std::cout << std::endl;

				if (widget->chat())
					set_current_chat(widget->chat());
				else
					set_current_contact(widget->contact());
				return;
			}
			else if (ContactSeparator* separator = item->data(Qt::UserRole).value<ContactSeparator*>())
			{
				assert( 0 );
			}
		}
		//std::cout << std::endl;
	}
	else
	{
		if (auto item = list_widget->itemAt(list_widget->mapFromGlobal(QCursor::pos())))
		{
			if (ContactSeparator* separator = item->data(Qt::UserRole).value<ContactSeparator*>())
			{
				// мы выделили разделитель... выделяем текущий активный чат
				select_current_chat_item(list_widget);
				return;
			}
		}
		// каким то образом сняли выделение... оповещаем об этом
		set_current_chat(nullptr);
	}
}
void CurrentChat::select_current_chat_item(QListWidget* list)
{
	QSignalBlocker block(list);
	list->clearSelection();
	
	for (int i = 0; i < list->count(); ++i)
	{
		QListWidgetItem* item = list->item(i);
		if (m_chat)
		{
			if (ChatItemWidget* widget = item->data(Qt::UserRole).value<ChatItemWidget*>())
			{
				if (widget->chat() == m_chat)
				{
					list->setItemSelected(item, true);
					//item->setSelected(true);
					break;
				}
			}
		}
		//else
		//	item->setSelected(false);
	}
}
void CurrentChat::set_current_chat(const Chat* chat)
{
	if (m_chat != chat)
	{
		m_contact = nullptr;
		m_chat = chat;
		emit chat_selected(m_chat);
		m_messages_widget_manager->activate_message_widget(m_chat);
	}
}
void CurrentChat::set_current_contact(const User* contact)
{
	if (m_contact != contact)
	{
		m_chat = nullptr;
		m_contact = contact;
		emit contact_selected(m_contact);
		m_messages_widget_manager->activate_default_contact_widget();
	}
}