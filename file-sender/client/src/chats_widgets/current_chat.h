//=====================================================================================//
//   Author: open
//   Date:   24.06.2017
//=====================================================================================//
#pragma once

class MessagesWidgetManager;
class QListWidgetItem;
class QListWidget;
class User;
class Chat;

class CurrentChat : public QObject
{
	Q_OBJECT
private:
	MessagesWidgetManager* m_messages_widget_manager;	// менеджер виджетов сообщений
	const Chat* m_chat;
	const User* m_contact;

public:
	CurrentChat(MessagesWidgetManager* messages_widget_manager);

	const Chat* chat() const;
	const User* contact() const;
	// выделить текущий чат в указанном списке
	void select_current_chat_item(QListWidget* list);
	void process_items_selected(QListWidget* list_widget);
	//void process_chat_destroyed(Chat* chat);

	void set_current_chat(const Chat* chat);//, const Contact* contact
	void set_current_contact(const User* contact);

signals:
	void contact_selected(const User* contact);
	void chat_selected(const Chat* chat);

private:
	void process_items_selected(const QList<QListWidgetItem*>& items);
};



inline const Chat* CurrentChat::chat() const {
	return m_chat;
}
inline const User* CurrentChat::contact() const {
	return m_contact;
}