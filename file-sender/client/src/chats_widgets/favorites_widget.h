//=====================================================================================//
//   Author: open
//   Date:   06.06.2017
//=====================================================================================//
#pragma once
#include "chats_widgets/base_move_contacts_widget.h"

class FavoritesWidget : public BaseMoveContactsWidget
{
	Q_OBJECT
public:
	FavoritesWidget(QListWidget* list_widget, QListWidgetItem* separator, ChatItemWidgetsManager& chat_item_widgets_manager);
};