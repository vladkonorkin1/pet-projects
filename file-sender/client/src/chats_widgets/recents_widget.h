//=====================================================================================//
//   Author: open
//   Date:   06.06.2017
//=====================================================================================//
#pragma once
#include "chats_widgets/base_move_contacts_widget.h"

class RecentsWidget : public BaseMoveContactsWidget
{
	Q_OBJECT
public:
	RecentsWidget(QListWidget* list_widget, QListWidgetItem* separator, ChatItemWidgetsManager& chat_item_widgets_manager);
};