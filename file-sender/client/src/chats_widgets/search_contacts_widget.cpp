//=====================================================================================//
//   Author: open
//   Date:   06.06.2017
//=====================================================================================//
#include "base/precomp.h"
#include "search_contacts_widget.h"
#include "chat_item_widgets_manager.h"
#include "chat_item_widget.h"
#include "current_chat.h"
#include <QListWidget>
#include <QLineEdit>
#include <QScrollBar>
#include <QEvent>
#include "chat.h"


const int SearchContactsWidget::LOAD_ITEMS_COUNT = 30;


bool SearchListWidgetEventFilter::eventFilter(QObject* obj, QEvent* event)
{
	if (event->type() == QEvent::MouseButtonRelease)
		emit mouse_release();
	return QObject::eventFilter(obj, event);
}

SearchContactsWidget::SearchContactsWidget(const SearchContactsWidgetGerm& germ, CurrentChat& current_chat)
: m_chat_item_widgets(germ.chat_item_widgets_manager.create_chat_item_widgets())
, m_favorite_recent_container(germ.favorite_recent_container)
, m_search_container(germ.search_container)
, m_current_chat(current_chat)
, m_lineedit(germ.lineedit)
, m_list(germ.list)
, m_users(germ.users)
, m_search_users(&germ.users, this)
, m_loaded_count(0)
{
	SearchListWidgetEventFilter* event_filter = new SearchListWidgetEventFilter();
	connect(event_filter, SIGNAL(mouse_release()), SLOT(slot_mouse_release()));
	m_list->viewport()->installEventFilter(event_filter);

	connect(m_lineedit, SIGNAL(textChanged(const QString&)), SLOT(slot_text_changed(const QString&)));
	connect(m_list, SIGNAL(itemSelectionChanged()), SLOT(slot_item_selection_changed()));
	connect(m_list, SIGNAL(itemActivated(QListWidgetItem*)), SLOT(slot_item_activated(QListWidgetItem*)));
	connect(&m_search_users, SIGNAL(search_finished(std::vector<const User*>*)), SLOT(slot_search_users_finished(std::vector<const User*>*)));
	connect(m_list->verticalScrollBar(), SIGNAL(valueChanged(int)), SLOT(slot_scroll_changed(int)));

	m_lineedit->setText("");
	m_search_container->setVisible(false);
}
void SearchContactsWidget::slot_text_changed(const QString& mask)
{
	//qDebug() << "SearchContactsWidget::slot_text_changed";
	bool show = mask.size() > 0;

	if (!m_favorite_recent_container->isVisible())
	{
		if (!show) emit show_search_list(false);
	}
	else
	{
		if (show) emit show_search_list(true);
	}

	m_favorite_recent_container->setVisible(!show);
	m_search_container->setVisible(show);

	if (mask.size() > 1)
	{
		//m_connection.send_message(ClientMessageChatSearchQuery(mask, ChatSearchType::TypeSearchContacts));
		//m_list->clear();
		m_search_users.search(mask);
	}
	else
	{
		hide_items();
		m_lineedit->setToolTip("");
	}
}
void SearchContactsWidget::hide_items()
{
	for(int i=0; i<m_list->count(); ++i)
		m_list->item(i)->setHidden(true);
}
const Chat* SearchContactsWidget::is_chat_in_chat_list(ChatItemWidget* widget) const
{
	if (const Chat* chat = widget->chat())
		if (chat->is_enabled())
			return chat;
	return nullptr;
}
void SearchContactsWidget::slot_item_selection_changed()
{
	m_current_chat.process_items_selected(m_list);
}
void SearchContactsWidget::slot_item_activated(QListWidgetItem* item)
{
	process_select_item(item);
}
void SearchContactsWidget::process_select_item(QListWidgetItem* item)
{
	if (!item || item->isHidden())
		return;
	if (ChatItemWidget* widget = item->data(Qt::UserRole).value<ChatItemWidget*>())
	{
		// закрываем окно поиска
		m_lineedit->setText(QString());

		if (const Chat* chat = is_chat_in_chat_list(widget))
			emit select_chat(chat);
		else if (const User* contact = widget->contact())
			emit select_contact(contact);
	}
}
void SearchContactsWidget::slot_mouse_release()
{
	for (auto item : m_list->selectedItems())
	{
		process_select_item(item);
		break;
	}
}

void SearchContactsWidget::slot_search_users_finished(std::vector<const User*>* contacts)
{
	m_current_chat.select_current_chat_item(m_list);
	m_lineedit->setToolTip(tr("Found %1 contacts").arg(contacts->size()));

	m_search_users_result = *contacts;
	m_loaded_count = 0;

	m_items.clear();
	//
	//const int COUNT = result->size();
	//loaded_search_widgets.resize(COUNT);
	emit show_search_list(true);
	load_new_items_portions();
}

/*void SearchContactsWidget::slot_list_verticalScrollBar_changed(int value)
{
	qDebug() << "scroll pos " << value;
	if (m_list->verticalScrollBar()->value() > static_cast<int>(static_cast<float>(m_list->verticalScrollBar()->maximum()) * 0.7f))
		load_new_items_portions();
}*/
void SearchContactsWidget::slot_scroll_changed(int value)
{
	int limit = static_cast<int>(static_cast<float>(m_list->verticalScrollBar()->maximum()) * 0.95f);
	if (value > limit)
		load_new_items_portions();
}

void SearchContactsWidget::load_new_items_portions()
{
	std::vector<const User*> created_users;

	const int count = std::fmin(m_search_users_result.size() - m_loaded_count, m_loaded_count == 0 ? LOAD_ITEMS_COUNT*2 : LOAD_ITEMS_COUNT);
	for (int i = m_loaded_count; i < m_loaded_count + count; ++i)
	{
		const User* user = m_search_users_result.at(i);
		ChatItemWidget* widget = m_chat_item_widgets->create_chat_item_widget(user, false);

		UItem item(new ChatListItem());
		item->setSizeHint(QSize(item->sizeHint().width(), widget->height()));
		item->setData(Qt::UserRole, QVariant::fromValue<ChatItemWidget*>(widget));

		m_list->addItem(item.get());
		m_list->setItemWidget(item.get(), widget);
		m_items[user->id] = ChatItem{ std::move(item), widget };

		created_users.push_back(user);
	}
	m_loaded_count += count;

	if (!created_users.empty())
		emit request_users_online_statuses(created_users);
}