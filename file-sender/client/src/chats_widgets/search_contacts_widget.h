//=====================================================================================//
//   Author: open
//   Date:   06.06.2017
//=====================================================================================//
#pragma once
#include <QListWidgetItem>
#include "chats_widgets/chat_item_widgets.h"
#include "search_users.h"
#include "users.h"

class ChatItemWidgetsManager;
class QListWidgetItem;
class ChatItemWidget;
class QListWidget;
class CurrentChat;
class QLineEdit;
class Chat;

class SearchContactsWidgetGerm
{
public:
	Users& users;
	QLineEdit* lineedit;
	QListWidget* list;
	QWidget* search_container;
	QWidget* favorite_recent_container;
	ChatItemWidgetsManager& chat_item_widgets_manager;
};

class SearchListWidgetEventFilter : public QObject
{
	Q_OBJECT
signals:
	  void mouse_release();

protected:
	virtual bool eventFilter(QObject* obj, QEvent* event);
};

class SearchContactsWidget : public QObject
{
	Q_OBJECT
private:
	static const int LOAD_ITEMS_COUNT;
	CurrentChat& m_current_chat;

	QWidget* m_search_container;
	QWidget* m_favorite_recent_container;
	QLineEdit* m_lineedit;
	QListWidget* m_list;
	Users& m_users;
	SearchUsers m_search_users;

	using UItem = std::unique_ptr<QListWidgetItem>;
	struct ChatItem
	{
		UItem item;
		ChatItemWidget* widget;
	};
	using items_t = std::map<UserId, ChatItem>;
	items_t m_items;

	UChatItemWidgets m_chat_item_widgets;
	std::vector<const User*> m_search_users_result;
	int m_loaded_count;

public:
	SearchContactsWidget(const SearchContactsWidgetGerm& germ, CurrentChat& current_chat);
	
signals:
	void show_search_list(bool show);
	void select_contact(const User* contact);
	void select_chat(const Chat* chat);
	void request_users_online_statuses(const std::vector<const User*>& users);

private slots:
	void slot_text_changed(const QString& mask);
	void slot_item_selection_changed();
	void slot_item_activated(QListWidgetItem* item);
	void slot_mouse_release();
	void slot_search_users_finished(std::vector<const User*>* result);
	
	void slot_scroll_changed(int value);

private:
	void hide_items();
	const Chat* is_chat_in_chat_list(ChatItemWidget* widget) const;
	void process_select_item(QListWidgetItem* item);
	void load_new_items_portions();
};