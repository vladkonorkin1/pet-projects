//=====================================================================================//
//   Author: open
//   Date:   20.09.2017
//=====================================================================================//
#include "base/precomp.h"
#include "check_readed_new_messages.h"
#include "gui_messages/messages_widget_manager.h"
#include "chats_widgets/current_chat.h"
#include "connection.h"
#include "chats.h"

CheckReadedNewMessages::CheckReadedNewMessages(QWidget* main_window, Chats* chats, const CurrentChat* current_chat, Connection* connection, MessagesWidgetManager* messages_widget_manager)
: m_messages_widget_manager(messages_widget_manager)
, m_main_window(main_window)
, m_current_chat(current_chat)
, m_connection(connection)
, m_emitted(false)
, m_chat(nullptr)
, m_chats(chats)
{
	connect(m_current_chat, SIGNAL(chat_selected(const Chat*)), SLOT(slot_chat_selected(const Chat*)));
	connect(this, SIGNAL(emit_check()), SLOT(slot_check()), Qt::ConnectionType::QueuedConnection);
	connect(this, SIGNAL(emit_activate_app()), SLOT(slot_activate_app()), Qt::ConnectionType::QueuedConnection);

	connect(m_messages_widget_manager, SIGNAL(read_message(const Chat*, ChatMessageId, bool&)), SLOT(slot_read_message(const Chat*, ChatMessageId, bool&)));
}
void CheckReadedNewMessages::check(const Chat* chat)
{
	//std::cout << "check(const Chat* chat)" << std::endl;
	if (!m_emitted)
	{
		if (m_main_window->isActiveWindow())
		{
			if (const Chat* selected_chat = m_current_chat->chat())
			{
				if (!chat || selected_chat == chat)
				{
					//if (selected_chat->count_new_messages() > 0)
					if (selected_chat->count_unreaded_messages() > 0)
					{
						m_chat = selected_chat;
						//if (!m_emitted)
						{
							//std::cout << "check(const Chat* chat) emited" << std::endl;
							m_emitted = true;
							emit emit_check();
						}
					}
				}
			}
		}
	}
}
void CheckReadedNewMessages::slot_check()
{
	//std::cout << "slot_check()" << std::endl;
	m_emitted = false;
	m_messages_widget_manager->check_unreaded_messages(m_chat);
}
// + выбор чата
// - при активации приложения + чат выбран
// - при добавлении сообщения (если приложение активно + чат выбран)
void CheckReadedNewMessages::slot_chat_selected(const Chat* chat)
{
	check(chat);
}
/*void CheckReadedNewMessages::add_new_message(Chat* chat)
{
	//m_chats->set_chat_count_new_messages(chat, chat->count_new_messages() + 1);
}*/
void CheckReadedNewMessages::slot_read_message(const Chat* chat, ChatMessageId message_id, bool& send_readed)
{
	send_readed = m_main_window->isActiveWindow();
	if (send_readed)
	{
		//logs(QString("client read visible message: %1").arg(message_id));

		// мы уже отправляли собщение серверу о прочтении?
		ChatMessageId sended_msg_id = get_sended_msg_id(chat);
		if (sended_msg_id < message_id)
		{
			m_sended_readed[chat->id()] = message_id;
			
			// сообщаем серверу, что прочитали сообщение
			m_connection->send_message(ClientMessageChatNewMessageRead(chat->id(), message_id));
			
			//logs(QString("client send read visible message: %1").arg(message_id));
		}
	}
}
// отправляли ли сообщение серверу о прочтении
ChatMessageId CheckReadedNewMessages::get_sended_msg_id(const Chat* chat) const
{
	auto it = m_sended_readed.find(chat->id());
	if (it != m_sended_readed.end())
		return it->second;
	return 0;
}
// отметить сообщения, как прочитанные
void CheckReadedNewMessages::set_message_readed(bool success, Chat* chat, ChatMessageId message_id)
{
	if (success)
	{
		ChatMessageId sended_msg_id = get_sended_msg_id(chat);
		if (sended_msg_id == message_id)
			m_sended_readed.erase(chat->id());
	}
}
// обработать активацию приложения
void CheckReadedNewMessages::process_activate_app()
{
	//emit emit_activate_app();
	slot_activate_app();
}
void CheckReadedNewMessages::slot_activate_app()
{
	if (m_main_window->isActiveWindow())
		check(nullptr);
}
// переотправить сетевые запросы
void CheckReadedNewMessages::resend()
{
	for (const auto& pair : m_sended_readed)
	{
		ChatId chat_id = pair.first;
		ChatMessageId msg_id = pair.second;
		m_connection->send_message(ClientMessageChatNewMessageRead(chat_id, msg_id));
	}
}