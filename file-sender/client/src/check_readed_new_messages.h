//=====================================================================================//
//   Author: open
//   Date:   20.09.2017
//=====================================================================================//
#pragma once
#include <QWidget>

class MessagesWidgetManager;
class CurrentChat;
class Connection;
class Chats;
class Chat;

class CheckReadedNewMessages : public QObject
{
	Q_OBJECT
private:
	MessagesWidgetManager* m_messages_widget_manager;
	const CurrentChat* m_current_chat;
	Connection* m_connection;
	QWidget* m_main_window;
	const Chat* m_chat;
	Chats* m_chats;
	bool m_emitted;

	std::map<ChatId, ChatMessageId> m_sended_readed;	// отправленные прочтенные сообщения

public:
	CheckReadedNewMessages(QWidget* main_window, Chats* chats, const CurrentChat* current_chat, Connection* connection, MessagesWidgetManager* messages_widget_manager);
	// запустить проверку на непрочитанные сообщения
	void check(const Chat* chat = nullptr);
	// добавить сообщение
	//void add_new_message(Chat* chat);
	// отметить сообщения, как прочитанные
	void set_message_readed(bool success, Chat* chat, ChatMessageId message_id);
	// обработать активацию приложения
	void process_activate_app();
	// переотправить сетевые запросы
	void resend();

signals:
	void emit_check();
	void emit_activate_app();

private slots:
	void slot_check();
	void slot_chat_selected(const Chat* chat);
	void slot_read_message(const Chat* chat, ChatMessageId message_id, bool& send_readed);
	void slot_activate_app();

private:
	// отправляли ли сообщение серверу о прочтении
	ChatMessageId get_sended_msg_id(const Chat* chat) const;
};