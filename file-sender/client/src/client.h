#pragma once
#include "network/base_client_connection.h"
#include "network/message_dispatcher.h"
#include "network/handler_messages.h"
#include "file_connection.h"

class Client : public QObject
{
	Q_OBJECT
private:
	QTcpSocket m_socket;
	BaseClientConnection m_message_connection;
	QString m_host_adress;
	MessageDispatcher<ClientHandlerMessages> m_message_dispatcher;
	std::unique_ptr<FileConnection> m_file_connection;

public:
	Client();
	void send_message(const Message& message);
	void set_message_listener(ClientHandlerMessages* handler_messages);
	void connect_host();
	void set_connection_id(MessageConnectionId id);
	bool send_file(const QString& file_path);

signals:
	void client_connected();
	void client_disconnected();

	void file_start_transferred();
	void file_finish_transferred();
	void file_cancel_transferred(quint64 size);

private slots:
	void slot_connected();
	void slot_disconnect();
	void slot_data_recieved(const char* data, unsigned int size);

	void slot_file_start_transferred();
	void slot_file_finish_transferred();
	void slot_file_cancel_transferred(quint64 size);
};