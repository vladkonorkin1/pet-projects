#include "base/precomp.h"
#include "connection.h"
#include "database/database.h"
#include "users.h"
#include "chats.h"

Connection::Connection(Database& database, Users& users, Chats& chats)
: m_message_connection(nullptr)
, m_login_user(nullptr)
, m_database(database)
, m_users(users)
, m_chats(chats)
{
}
Connection::~Connection()
{
	if (m_message_connection)
		m_message_connection->set_message_listener(nullptr);
}
void Connection::set_message_connection(MessageConnection* message_connection)
{
	if (m_message_connection)
		m_message_connection->set_message_listener(nullptr);

	m_message_connection = message_connection;

	if (m_message_connection)
		m_message_connection->set_message_listener(this);

	// если дроп конекшена
	if (!m_message_connection)
		m_login_user = nullptr;
}
bool Connection::is_connected() const
{
	return m_message_connection;
}
void Connection::send_message(const NetMessage& message)
{
	if (m_message_connection)
		m_message_connection->send_message(message);
}

const User* Connection::find_user(UserId id) const
{
	if (const User* user = m_users.find_user(id)) return user;
	logs(QString("couldn't to find user: %1").arg(id));
	return nullptr;
}
Chat* Connection::find_chat(ChatId id) const
{
	if (Chat* chat = m_chats.find_chat(id)) return chat;
	logs(QString("couldn't to find chat: %1").arg(id));
	return nullptr;
}

void Connection::on_message(const ServerMessageAliveResponse& msg)
{
	Q_UNUSED( msg );
	m_message_connection->set_alive();
}

void Connection::on_message(const ServerMessageInitializationFinish& msg)
{
	Q_UNUSED( msg );
	emit connection_initialized();
	start_sync();
}
// запускаем синхронизацию
void Connection::start_sync()
{
	// просим синхронизации
	ClientMessageSync msg_sync;

	// таймштамп изменения департаментов
	msg_sync.timestamp_departments = m_users.timestamp_departments();

	// отправляем максимальный таймштамп пользователей
	msg_sync.timestamp_users = m_users.timestamp_users();

	// собираем таймштампы чатов
	for (const auto& pair : m_chats.chat_list())
	{
		const Chat* chat = pair.second;

		if (msg_sync.timestamp_chats < chat->timestamp())
			msg_sync.timestamp_chats = chat->timestamp();

		if (msg_sync.last_chat_message_stamp < chat->last_message_stamp())
			msg_sync.last_chat_message_stamp = chat->last_message_stamp();

		if (msg_sync.last_readed_chat_message_id < chat->last_readed_message_id())
			msg_sync.last_readed_chat_message_id = chat->last_readed_message_id();
	}
	send_message(msg_sync);
}
void Connection::on_message(const ServerMessageDepartments& msg)
{
	m_users.update_departments(msg.departments);
}
void Connection::on_message(const ServerMessageUsers& msg)
{
	m_users.update_users(msg.users);
}
void Connection::on_message(const ServerMessageSetLoginUser& msg)
{
	assert( !m_login_user );
	m_login_user = find_user(msg.user_id);
	assert( m_login_user );
	emit login_user_updated(m_login_user);
}
void Connection::on_message(const ServerMessageContactChat& msg)
{
	logs(QString("add contact chat chat_id=%1 contact_id=%2").arg(msg.chat_id).arg(msg.contact_id));
	if (const User* contact = find_user(msg.contact_id))
		m_chats.update_contact_chat(msg.chat_id, contact, msg.timestamp);// , msg.last_message_id);
}
void Connection::on_message(const ServerMessageGroupChat& msg)
{
	logs(QString("add group chat chat_id=%1 name=%2").arg(msg.chat_id).arg(msg.name));
	m_chats.update_group_chat(msg.chat_id, msg.name, msg.timestamp);// , msg.last_message_id);
}
void Connection::on_message(const ServerMessageGroupChatSetUsers& msg)
{
	if (Chat* chat = find_chat(msg.chat_id))
		m_chats.update_group_chat_users(chat, m_login_user, get_users(msg.users), get_users(msg.admins));
}
void Connection::on_message(const ServerMessageChatList& msg)
{
	m_chats.update_chat_list(msg.favorites, msg.recents);
}
void Connection::on_message(const ServerMessageSyncResponse& msg)
{
	Q_UNUSED( msg );
	//m_chats.finish_update();
	emit sync_finished();
}
// сетевое событие получения новых сообщений в чаты
void Connection::on_message(const ServerMessageChatMessages& msg)
{
	emit new_chat_messages(msg.messages);
}
// сетевое событие получения исторических сообщений
void Connection::on_message(const ServerMessageChatHistoryMessages& msg)
{
	if (const Chat* chat = find_chat(msg.chat_id))
		emit chat_history_messages_loaded(chat, msg.messages, msg.is_finish_upload);
}
void Connection::on_message(const ServerMessageContactChatCreateResponse& msg)
{
	const Chat* chat = find_chat(msg.chat_id);
	const User* contact = find_user(msg.user_id);
	// проверку на chat исключаем (означает что чат мог быть не создан из-за какой то ошибки)
	// chat == nullptr допустимо!
	if (contact) emit contact_chat_created(contact, chat);
}
std::vector<const User*> Connection::get_users(const std::vector<UserId>& user_ids)
{
	std::vector<const User*> users;
	users.reserve(user_ids.size());
	for (UserId user_id : user_ids)
	{
		if (const User* user = find_user(user_id))
			users.push_back(user);
	}
	return users;
}
std::vector<Chat*> Connection::get_chats(const std::vector<ChatId>& chat_ids)
{
	std::vector<Chat*> chats;
	chats.reserve(chat_ids.size());
	for (ChatId chat_id : chat_ids)
	{
		if (Chat* chat = find_chat(chat_id))
			chats.push_back(chat);
	}
	return chats;
}
void Connection::on_message(const ServerMessageChatAddFavorite& msg)
{
	if (Chat* chat = find_chat(msg.chat_id)) m_chats.add_chat(chat, true);
}
void Connection::on_message(const ServerMessageChatAddRecent& msg)
{
	if (Chat* chat = find_chat(msg.chat_id)) m_chats.add_chat(chat, false);
}
void Connection::on_message(const ServerMessageChatRemoveResponse& msg)
{
	if (Chat* chat = find_chat(msg.chat_id)) m_chats.remove_chat(chat->id());
}
void Connection::on_message(const ServerMessageContactStatus& msg)
{
	if (const User* user = find_user(msg.user_id)) m_users.set_online(user->id, msg.status);
}
void Connection::on_message(const ServerMessageUsersStatus& msg)
{
	for (const UserStatus& user_status : msg.user_statuses)
	{
		if (const User* user = find_user(user_status.user_id))
			m_users.set_online(user->id, user_status.status);
	}
}
void Connection::on_message(const ServerMessageGroupChatCreateResponse& msg)
{
	if (msg.chat_id > 0)
	{
		if (Chat* chat = find_chat(msg.chat_id))
			emit group_chat_created(chat);
	}
}
void Connection::on_message(const ServerMessageChatMessageRead& msg)
{
	if (Chat* chat = find_chat(msg.chat_id))
		emit message_readed(chat, msg.message_id, msg.datetime);
}
void Connection::on_message(const ServerMessageChatNewMessageReadResponse& msg)
{
	if (Chat* chat = find_chat(msg.chat_id))
		emit new_message_readed_response(msg.success, chat, msg.message_id);
}
void Connection::on_message(const ServerMessageFileMessageProgress& msg)
{
	if (Chat* chat = find_chat(msg.chat_id))
		emit file_message_progress(msg.message_id, chat, msg.progress);
}
void Connection::on_message(const ServerMessageFileTransferFinish& msg)
{
	emit file_transfer_finished(msg.result, msg.dest_name);
}
void Connection::on_message(const ServerMessageFileTransferWriteFileError&)
{
	emit file_transfer_write_file_error();
}
void Connection::on_message(const ServerMessageFileMessageCancelResponse& msg)
{
	emit file_message_cancel_confirm(msg.message_id);
}
void Connection::on_message(const ServerMessageIsAccessClosedFolderResponse& msg)
{
	emit have_access_closed_folder(msg.is_have_access);
}