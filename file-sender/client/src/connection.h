#pragma once
#include "network/handler_messages.h"
#include "message_connection.h"
#include "chat_message.h"

class Database;
class Users;
class Chats;
class User;
class Chat;

class Connection : public QObject, public ClientHandlerMessages
{
	Q_OBJECT
private:
	MessageConnection* m_message_connection;
	const User* m_login_user;
	Database& m_database;
	Users& m_users;
	Chats& m_chats;

public:
	Connection(Database& database, Users& users, Chats& chats);
	virtual ~Connection();
	// установить подключение
	void set_message_connection(MessageConnection* message_connection);
	// отправка сообщения
	void send_message(const NetMessage& message);
	// под каким пользователем подключились
	const User* login_user() const;
	// подключен ли
	bool is_connected() const;

signals:
	void connection_initialized();
	// сигнал окончания синхронизации
	void sync_finished();
	/// сигнал обновления пользователя под которым мы залогинились
	void login_user_updated(const User* login_user);
	// событие окончания отправки контактов
	void contact_chat_created(const User* contact, const Chat* chat);
	// групповой чат создан
	void group_chat_created(Chat* chat);
	// сетевое сообщение о прочтении сообщения чата
	void message_readed(Chat* chat, ChatMessageId message_id, const QDateTime& datetime);
	// сетевое сообщение о прочтении сообщения чата (получаемое только отправителем)
	void new_message_readed_response(bool success, Chat* chat, ChatMessageId message_id);
	// получены новые сообщения
	void new_chat_messages(const std::vector<DescChatMessage>& messages);
	// получены исторические сообщения
	void chat_history_messages_loaded(const Chat* chat, const std::vector<DescChatMessage>& messages, bool is_finish_upload);

	void file_message_progress(ChatMessageId message_id, const Chat* chat, unsigned char progress);
	void file_message_cancel_confirm(ChatMessageId message_id);
	void file_transfer_finished(FileTransferResult result, const QString& dest_name);
	void file_transfer_write_file_error();

	// сигнал есть ли доступ в закрытую папку
	void have_access_closed_folder(bool is_have_access);

public:
	virtual void on_message(const ServerMessageInitializationFinish& msg);
	virtual void on_message(const ServerMessageDepartments& msg);
	virtual void on_message(const ServerMessageUsers& msg);
	virtual void on_message(const ServerMessageSetLoginUser& msg);
	virtual void on_message(const ServerMessageContactChat& msg);
	virtual void on_message(const ServerMessageGroupChat& msg);
	virtual void on_message(const ServerMessageGroupChatSetUsers& msg);
	virtual void on_message(const ServerMessageChatList& msg);
	virtual void on_message(const ServerMessageSyncResponse& msg);

	virtual void on_message(const ServerMessageAliveResponse& msg);

	virtual void on_message(const ServerMessageChatAddFavorite& msg);
	virtual void on_message(const ServerMessageChatAddRecent& msg);
	virtual void on_message(const ServerMessageChatRemoveResponse& msg);
	virtual void on_message(const ServerMessageContactChatCreateResponse& msg);
	virtual void on_message(const ServerMessageGroupChatCreateResponse& msg);

	virtual void on_message(const ServerMessageContactStatus& msg);
	virtual void on_message(const ServerMessageUsersStatus& msg);

	virtual void on_message(const ServerMessageChatMessages& msg);
	virtual void on_message(const ServerMessageChatHistoryMessages& msg);

	virtual void on_message(const ServerMessageFileMessageProgress& msg);
	virtual void on_message(const ServerMessageFileTransferFinish& msg);
	virtual void on_message(const ServerMessageFileTransferWriteFileError& msg);
	virtual void on_message(const ServerMessageFileMessageCancelResponse& msg);

	virtual void on_message(const ServerMessageChatMessageRead& msg);
	virtual void on_message(const ServerMessageChatNewMessageReadResponse& msg);

	virtual void on_message(const ServerMessageIsAccessClosedFolderResponse& msg);

private:
	std::vector<const User*> get_users(const std::vector<UserId>& user_ids);
	const User* find_user(UserId id) const;
	Chat* find_chat(ChatId id) const;
	std::vector<Chat*> get_chats(const std::vector<ChatId>& chat_ids);
	// запускаем синхронизацию
	void start_sync();
};

inline const User* Connection::login_user() const {
	return m_login_user;
}