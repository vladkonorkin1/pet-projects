#include "base/precomp.h"
#include "connection_manager.h"
#include "base/cryptography.h"

ConnectionManager::ConnectionManager(const QString& host, int protocol_version)
: m_last_auth_result(AutorizateResult::Null)
, m_host_adress(host)
, m_protocol_version(protocol_version)
, m_reconnect_time(3.f)
, m_database_timestamp(0)
, m_auto_reconnect(false)
{
}
// открыть подключение
void ConnectionManager::open_connection(const QString& login, const QString& password)
{
	m_login_name = login;
	Cryptography::rsa_encrypt("authorization_public_key.pem", password, m_encrypted_password);
	reconnect();
}
// закрыть подключение (даже если уже уничтожено)
void ConnectionManager::close_connection()
{
	m_last_auth_result = AutorizateResult::Null;
	if (m_message_connection)
		m_message_connection->close();
}
// открыть соединение зашифрованным паролем
void ConnectionManager::open_connection_with_encrypted_password(const QString& login, const QByteArray& encrypted_password)
{
	m_login_name = login;
	m_encrypted_password = encrypted_password;
	reconnect();
}
void ConnectionManager::slot_message_connection_connected()
{
	m_reconnect_time = 3.f;
	send_message(ClientMessageCheckProtocolVersion(m_protocol_version));
}
void ConnectionManager::slot_message_connection_disconnect()
{
	emit connection_disconnected();

	/*switch (m_last_auth_result)
	{
		case AutorizateResult::Success:
			// увеличиваем экспоненциально время задержки при каждом переподключении
			m_reconnect_time = next_reconnect_limit_time(m_reconnect_time);
			logs(QString("reconnect after %1 seconds").arg(m_reconnect_time));
			QTimer::singleShot(static_cast<int>(m_reconnect_time * 1000.f), this, [this]() { reconnect(); });
			break;
		case AutorizateResult::WrongLogin:
			break;
		case AutorizateResult::Null:
			emit server_not_available();
			break;
	}*/

	if (m_auto_reconnect)
	{
		// увеличиваем экспоненциально время задержки при каждом переподключении
		m_reconnect_time = next_reconnect_limit_time(m_reconnect_time);
		logs(QString("reconnect after %1 seconds").arg(m_reconnect_time));
		QTimer::singleShot(static_cast<int>(m_reconnect_time * 1000.f), this, [this]() { reconnect(); });
	}

	// удаляем соединение "вручную" с задержкой
	//m_message_connection.reset();
	MessageConnection* con = m_message_connection.release();
	assert( con );
	con->deleteLater();
}
void ConnectionManager::send_message(const NetMessage& message)
{
	if (m_message_connection)
		m_message_connection->send_message(message);
}
void ConnectionManager::on_message(const ServerMessageCheckProtocolVersionResponse& msg)
{
	if (!msg.success)
	{
		// номера версий не совпадают!
		logs(QString("wrong net protocol version %1").arg(m_protocol_version));
		emit wrong_version();
		return;
	}
	m_database_timestamp = msg.database_timestamp;
	send_message(ClientMessageAutorizate(m_login_name, m_encrypted_password));
}
void ConnectionManager::on_message(const ServerMessageAutorizateResponse& msg)
{
	m_last_auth_result = msg.auth_result;
	switch (m_last_auth_result)
	{
		case AutorizateResult::Success:
		{
			m_message_connection->set_id(msg.connection_id);
			emit connection_autorized(m_message_connection.get(), m_login_name, m_encrypted_password, m_database_timestamp);
			break;
		}
		case AutorizateResult::WrongLogin:
			emit wrong_password();
			break;
		case AutorizateResult::Null:
			emit server_not_available();
			break;
	}
}
// переподключиться
void ConnectionManager::reconnect()
{
	m_message_connection.reset(new MessageConnection());
	m_message_connection->set_message_listener(this);
	connect(m_message_connection.get(), SIGNAL(connected()), SLOT(slot_message_connection_connected()));
	connect(m_message_connection.get(), SIGNAL(disconnected()), SLOT(slot_message_connection_disconnect()));
	m_message_connection->connect_host(m_host_adress);
	logs(QString("start reconnect..."));
}
// увеличение времени задержки переподключения
float ConnectionManager::next_reconnect_limit_time(float current_reconnect_time) const
{
	const float factor = 1.3f;
	const float jitter = 0.1f;

	float reconnect_time = std::min(current_reconnect_time * factor, 36.f);
	reconnect_time += (jitter * reconnect_time * (rand() % 1000) / 1000.f);
	return reconnect_time;
}
// установить режим автоматического переподключения
void ConnectionManager::set_auto_reconnect(bool enable)
{
	m_auto_reconnect = enable;
}