#pragma once
#include "network/handler_messages.h"
#include "message_connection.h"

class ConnectionManager : public QObject, public ClientHandlerMessages
{
	Q_OBJECT
private:
	using UMessageConnection = std::unique_ptr<MessageConnection>;
	UMessageConnection m_message_connection;
	AutorizateResult m_last_auth_result;
	QString m_host_adress;
	float m_reconnect_time;	// ограничение по времени на попытку подключения
	int m_protocol_version;
	QString m_login_name;
	QByteArray m_encrypted_password;
	Timestamp m_database_timestamp;
	bool m_auto_reconnect;

public:
	ConnectionManager(const QString& host, int protocol_version);
	// открыть подключение
	void open_connection(const QString& login, const QString& password);
	// открыть соединение зашифрованным паролем
	void open_connection_with_encrypted_password(const QString& login, const QByteArray& encrypted_password);
	// закрыть подключение (даже если уже уничтожено)
	void close_connection();
	// установить режим автоматического переподключения
	void set_auto_reconnect(bool enable);

public:
	virtual void on_message(const ServerMessageCheckProtocolVersionResponse& msg);
	virtual void on_message(const ServerMessageAutorizateResponse& msg);

signals:
	void connection_autorized(MessageConnection* message_connection, const QString& login_name, const QByteArray& encrypted_password, Timestamp database_timestamp);
	void connection_disconnected();
	void wrong_password();
	void server_not_available();
	void wrong_version();

private slots:
	void slot_message_connection_connected();
	void slot_message_connection_disconnect();

private:
	// отправка сообщения
	void send_message(const NetMessage& message);
	// переподключиться
	void reconnect();
	// увеличение времени задержки переподключения
	float next_reconnect_limit_time(float current_reconnect_time) const;
};