//=====================================================================================//
//   Author: open
//   Date:   05.02.2019
//=====================================================================================//
#include "base/precomp.h"
#include "database.h"
#include "base/convert_utility.h"
#include "database_utility.h"
#include <QTextStream>
#include <QSqlQuery>
#include <QSqlError>
#include <QFile>
#include "user.h"
#include "chat.h"

Database::Database(const QString& profile_path)
: m_database(connect_database(profile_path + "data.db"))
, m_properties(&m_database)
{
}
Database::~Database()
{
	m_database.close();
	QSqlDatabase::removeDatabase("QSQLITE");
}
QSqlDatabase Database::connect_database(const QString& filename) const
{
	QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE");
	database.setDatabaseName(filename);
	if (!database.open())
	{
		QString error("Cannot open database: " + database.databaseName());
		logs(error);
		throw std::runtime_error(qPrintable(error));
	}
	return database;
}
// обновить департаменты
void Database::update_departaments(const std::vector<const Department*>& departments)
{
	if (m_database.transaction())
	{
		QSqlQuery query(m_database);
		query.prepare("insert or replace into departments(id, name, timestamp) values(:id, :name, :timestamp)");
		for (const Department* department : departments)
		{
			query.bindValue(":id", department->id);
			query.bindValue(":name", department->name);
			query.bindValue(":timestamp", timestamp_to_str(department->timestamp));
			exec(query);
		}
		m_database.commit();
	}
}
// обновить контакты
void Database::update_users(const std::vector<const User*>& users)
{
	if (m_database.transaction())
	{
		QSqlQuery query(m_database);
		query.prepare("insert or replace into users(id, login, name, folder, timestamp, department_id, deleted) values(:id, :login, :name, :folder, :timestamp, :department_id, :deleted)");
		for (const User* user : users)
		{
			query.bindValue(":id", user->id);
			query.bindValue(":login", user->login);
			query.bindValue(":name", user->name);
			query.bindValue(":folder", user->folder);
			query.bindValue(":timestamp", timestamp_to_str(user->timestamp));
			query.bindValue(":department_id", user->department->id);
			query.bindValue(":deleted", user->deleted);
			exec(query);
		}
		m_database.commit();
	}
}
// выбрать все департаменты
std::vector<DescDepartment> Database::select_departments()
{
	std::vector<DescDepartment> results;
	QSqlQuery query(m_database);
	query.prepare("select id, name, timestamp from departments");
	if (exec(query))
	{
		while (query.next())
		{
			results.emplace_back(DescDepartment {
				query.value(0).toUInt(),
				query.value(1).toString(),
				str_to_timestamp(query.value(2).toString())
			});
		}
	}
	return results;
}
// выбрать всех пользователей
std::vector<DescUser> Database::select_users()
{
	std::vector<DescUser> results;
	QSqlQuery query(m_database);
	query.prepare("select id, login, name, folder, timestamp, department_id, deleted from users");
	if (exec(query))
	{
		while (query.next())
		{
			results.emplace_back(DescUser
			{
				query.value(0).toUInt(),
				str_to_timestamp(query.value(4).toString()),	// timestamp
				query.value(1).toString(),
				query.value(2).toString(),
				query.value(3).toString(),
				query.value(5).toUInt(),	// department_id
				query.value(6).toBool()		// deleted
			});
		}
	}
	return results;
}
// выбрать чаты
static QString sql_select_chats =
"select chats.id, chats.timestamp, chats.name, chats.contact_id, last_message_id, last_message_stamp, last_readed_message_id from chats \
left join(select chat_id, MAX(id) as last_message_id, MAX(stamp) as last_message_stamp from messages group by chat_id) m1 on m1.chat_id = chats.id";

std::vector<DescChat> Database::select_chats()
{
	std::vector<DescChat> results;
	std::map<ChatId, DescChat*> chats;

	{
		QSqlQuery query(m_database);
		query.prepare(sql_select_chats);
		if (exec(query))
		{
			while (query.next())
			{
				/*results.emplace_back(DescChat
				{
					query.value(0).toULongLong(),
					str_to_timestamp(query.value(1).toString()),
					query.value(2).toString(),
					query.value(3).toUInt(),
					query.value(4).toULongLong(),
					query.value(5).toULongLong(),
					query.value(6).toULongLong()
				});*/

				DescChat desc_chat;
				desc_chat.id = query.value("id").toULongLong();
				desc_chat.timestamp = str_to_timestamp(query.value("timestamp").toString());
				desc_chat.name = query.value("name").toString();
				desc_chat.contact_id = query.value("contact_id").toUInt();
				desc_chat.last_message_id = query.value("last_message_id").toULongLong();
				desc_chat.last_message_stamp = query.value("last_message_stamp").toULongLong();
				desc_chat.historical_message_stamp = desc_chat.last_message_stamp;//query.value(6).toULongLong();
				desc_chat.last_readed_message_id = query.value("last_readed_message_id").toULongLong();
				results.push_back(desc_chat);
			}
		}

		for (DescChat& desc_chat : results)
			chats[desc_chat.id] = &desc_chat;
	}
	{
		QSqlQuery query(m_database);
		query.prepare("select chat_users.chat_id, chat_users.user_id, chat_users.admin from chat_users");
		if (exec(query))
		{
			while (query.next())
			{
				ChatId chat_id = query.value(0).toULongLong();
				auto it = chats.find(chat_id);
				if (chats.end() != it)
				{
					it->second->chat_users.emplace_back(DescChatUser
					{
						query.value(1).toUInt(),
						query.value(2).toBool()
					});
				}
			}
		}
	}
	return results;
}
// выбрать чат-лист
std::vector<DescChatListContact> Database::select_chat_list()
{
	std::vector<DescChatListContact> results;
	QSqlQuery query(m_database);
	query.prepare("SELECT chat_id, favorite FROM chat_list ORDER BY last_time ASC");
	if (exec(query))
	{
		while (query.next())
		{
			results.emplace_back(DescChatListContact
			{
				query.value(0).toULongLong(),
				query.value(1).toBool()
				//query.value(2).toInt()
			});
		}
	}
	return results;
}
// обновить чат
void Database::update_chat(const Chat* chat)
{
	QSqlQuery query(m_database);
	query.prepare("insert or replace into chats(id, timestamp, name, contact_id) values(:id, :timestamp, :name, :contact_id)");
	query.bindValue(":id", chat->id());
	query.bindValue(":timestamp", timestamp_to_str(chat->timestamp()));
	if (chat->contact()) query.bindValue(":contact_id", chat->contact()->id);
	else query.bindValue(":name", chat->name());
	exec(query);
}
// обновить участников чата
void Database::update_chat_users(const Chat* chat)
{
	if (m_database.transaction())
	{
		QSqlQuery query(m_database);
		query.prepare("insert or replace into chat_users(chat_id, user_id, admin) values(:chat_id, :user_id, :admin)");
		query.bindValue(":chat_id", chat->id());
		//for (const ChatUser* chat_user : chat->users())
		for (const auto& it : chat->users())
		{
			const ChatUser* chat_user = it.second;
			query.bindValue(":user_id", chat_user->user->id);
			query.bindValue(":admin", chat_user->admin);
			exec(query);
		}
		m_database.commit();
	}
}
// обновить чат-лист
void Database::update_chat_list(const std::vector<const Chat*>& chat_list)
{
	if (m_database.transaction())
	{
		{
			QSqlQuery query(m_database);
			query.prepare("delete from chat_list");
			exec(query);
		}
		{
			QSqlQuery query(m_database);
			query.prepare("insert into chat_list(chat_id, favorite, last_time) values(:chat_id, :favorite, :last_time)");
			for (const Chat* chat : chat_list)
			{
				query.bindValue(":chat_id", chat->id());
				query.bindValue(":favorite", chat->favorite());
				query.bindValue(":last_time", chat->last_time());
				exec(query);
			}
		}
		m_database.commit();
	}
}
// добавить чат в чат лист
bool Database::add_chat_to_chat_list(const Chat* chat)
{
	logs(QString("db: add_chat_to_chat_list chat_id=%1 name=%2").arg(chat->id()).arg(chat->name()));
	QSqlQuery query(m_database);
	query.prepare("insert or replace into chat_list(chat_id, favorite, last_time) values(:chat_id, :favorite, :last_time)");
	query.bindValue(":chat_id", chat->id());
	query.bindValue(":favorite", chat->favorite());
	query.bindValue(":last_time", chat->last_time());
	return exec(query);
}
// удалить чат из чат листа
void Database::remove_chat_from_chat_list(ChatId chat_id)
{
	QSqlQuery query(m_database);
	query.prepare("delete from chat_list where chat_id=:chat_id");
	query.bindValue(":chat_id", chat_id);
	exec(query);
}
// выбрать сообщения чата
std::vector<DescChatMessage> Database::select_chat_messages(ChatId chat_id, ChatMessageId message_id, int num)
{
	std::vector<DescChatMessage> messages;
	QSqlQuery query(m_database);
	query.prepare("select * from messages where chat_id=:chat_id and id<:id order by id desc limit :num");
	query.bindValue(":chat_id", chat_id);
	query.bindValue(":id", message_id);
	query.bindValue(":num", num);
	if (exec(query))
	{
		while (query.next())
		{
			DescChatMessage msg(
				query.value("id").toULongLong(),
				query.value("local_id").toUuid(),
				//QUuid("{F0E16276-95D0-48FD-99C2-2F6503DC4F84}"),
				query.value("chat_id").toULongLong(),
				query.value("sender_id").toUInt(),
				query.value("stamp").toULongLong(),
				static_cast<ChatMessageType>(query.value("type").toInt()),
				static_cast<ChatMessageStatus>(query.value("status").toInt()),
				static_cast<HistoryStatus>(query.value("history_status").toInt()),
				str_to_datetime(query.value("datetime").toString()),
				str_to_datetime(query.value("datetime_readed").toString()),
				true
			);
			msg.text = query.value("txt").toString();
			msg.file_src_path = query.value("file_src_path").toString();
			msg.file_dest_name = query.value("file_dest_name").toString();
			msg.file_is_dir = query.value("file_is_dir").toBool();
			msg.file_size = query.value("file_size").toULongLong();
			msg.file_status = static_cast<FileTransferResult>(query.value("file_status").toInt());
			msg.target_contact_id = query.value("target_contact_id").toUInt();
			messages.push_back(msg);
		}
	}
	return std::move(messages);
}
// обновить сообщения
void Database::update_chat_messages(const std::vector<DescChatMessage>& messages, const std::set<const Chat*> to_top_chats)
{
	if (m_database.transaction())
	{
		if (insert_messages(messages) && update_chat_last_time(to_top_chats))
			m_database.commit();
		else m_database.rollback();
	}
}
// внести сообщения
bool Database::insert_messages(const std::vector<DescChatMessage>& messages)
{
	QSqlQuery query(m_database);
	query.prepare("insert or replace into messages(id, local_id, chat_id, sender_id, type, status, history_status, stamp, datetime, datetime_readed, txt, file_src_path, file_dest_name, file_is_dir, file_size, file_status, target_contact_id) values(:id, :local_id, :chat_id, :sender_id, :type, :status, :history_status, :stamp, :datetime, :datetime_readed, :txt, :file_src_path, :file_dest_name, :file_is_dir, :file_size, :file_status, :target_contact_id)");

	for (const DescChatMessage& msg : messages)
	{
		query.bindValue(":id", msg.id);
		query.bindValue(":local_id", msg.local_id);
		query.bindValue(":chat_id", msg.chat_id);
		query.bindValue(":sender_id", msg.sender_id);
		query.bindValue(":stamp", msg.stamp);
		query.bindValue(":type", static_cast<int>(msg.type));
		query.bindValue(":status", static_cast<int>(msg.status));
		query.bindValue(":history_status", static_cast<int>(msg.history_status));
		query.bindValue(":datetime", datetime_to_str(msg.datetime));
		query.bindValue(":datetime_readed", datetime_to_str(msg.datetime_readed));
		
		// null
		QVariant null;
		query.bindValue(":txt", null);
		query.bindValue(":file_src_path", null);
		query.bindValue(":file_dest_name", null);
		query.bindValue(":file_is_dir", null);

		query.bindValue(":file_size", null);
		query.bindValue(":file_status", null);
		query.bindValue(":target_contact_id", null);

		switch (msg.type)
		{
			case ChatMessageType::Text:
				query.bindValue(":txt", msg.text);
				break;
			case ChatMessageType::File:
				if (!msg.file_src_path.isEmpty())
					query.bindValue(":file_src_path", msg.file_src_path);
				if (!msg.file_dest_name.isEmpty())
					query.bindValue(":file_dest_name", msg.file_dest_name);
				query.bindValue(":file_is_dir", msg.file_is_dir);

				query.bindValue(":file_size", msg.file_size);
				query.bindValue(":file_status", static_cast<int>(msg.file_status));
				break;
			default:
				if (msg.target_contact_id)
					query.bindValue(":target_contact_id", msg.target_contact_id);
		}
		if (!exec(query))
			return false;
	}
	return true;
}
// обновить у чатов last_time
bool Database::update_chat_last_time(const std::set<const Chat*> chats)
{
	for (const Chat* chat : chats)
	{
		if (!add_chat_to_chat_list(chat))
			return false;
	}
	return true;
}
// записать прочтение сообщения чата
void Database::set_messages_readed(const Chat* chat, ChatMessageId last_message_id, ChatMessageId new_message_id, const QDateTime& datetime_readed, UserId my_user_id)
{
	if (m_database.transaction())
	{
		if (update_last_readed_message_id(chat, new_message_id) && set_readed_status_message(last_message_id, new_message_id, datetime_readed, my_user_id))
			m_database.commit();
		else m_database.rollback();
	}
}
// обновить id последнего прочитанного сообщения
bool Database::update_last_readed_message_id(const Chat* chat, ChatMessageId message_id)
{
	QSqlQuery query(m_database);
	query.prepare("update chats set last_readed_message_id=? where id=?");
	query.addBindValue(message_id);
	query.addBindValue(chat->id());
	return exec(query);
}
// установить статус сообщения на прочитанное
bool Database::set_readed_status_message(ChatMessageId last_message_id, ChatMessageId new_message_id, const QDateTime& datetime_readed, UserId my_user_id)
{
	QSqlQuery query(m_database);
	query.prepare("update messages set status=?, datetime_readed=? where id between ? and ? and sender_id<>?");
	query.addBindValue(static_cast<int>(ChatMessageStatus::Readed));
	query.addBindValue(datetime_to_str(datetime_readed));
	query.addBindValue(last_message_id + 1);
	query.addBindValue(new_message_id);
	query.addBindValue(my_user_id);
	return exec(query);
}
static QString sql_select_unreaded_messages =
"SELECT messages.chat_id, messages.id				\
FROM messages										\
JOIN												\
	(SELECT chats.id, chats.last_readed_message_id	\
	FROM chats										\
	JOIN chat_list									\
	ON	chats.id = chat_list.chat_id) AS chat		\
ON	chat.id = messages.chat_id						\
	AND chat.last_readed_message_id < messages.id	\
	AND messages.datetime_readed IS NULL			\
	AND messages.sender_id <> ?";

// выбрать непрочитанные сообщения в чатах
std::map<ChatMessageId, Database::unreaded_messages_t> Database::select_unreaded_messages(const User* my_user)
{
	std::map<ChatMessageId, Database::unreaded_messages_t> results;

	QSqlQuery query(m_database);
	query.prepare(sql_select_unreaded_messages);
	query.addBindValue(my_user->id);
	if (exec(query))
	{
		while (query.next())
		{
			ChatId chat_id = query.value(0).toULongLong();
			ChatId message_id = query.value(1).toULongLong();
			
			auto it_res = results.find(chat_id);
			if (it_res == results.end())
				it_res = results.insert(std::make_pair(chat_id, unreaded_messages_t())).first;

			it_res->second.insert(message_id);
		}
	}
	return results;
}
// записать пока ещё не отправленное сообщение в БД
void Database::add_unsent_message(const DescChatMessage& msg)
{
	QSqlQuery query(m_database);
	query.prepare("INSERT INTO unsent_messages (local_id, chat_id, sender_id, type, txt, datetime) VALUES (:local_id, :chat_id, :sender_id, :type, :txt, :datetime)");
	query.bindValue(":local_id", msg.local_id);
	query.bindValue(":chat_id", msg.chat_id);
	query.bindValue(":sender_id", msg.sender_id);
	query.bindValue(":type", static_cast<int>(msg.type));
	query.bindValue(":txt", msg.text);
	query.bindValue(":datetime", datetime_to_str(msg.datetime));	//msg.datetime.toString(Qt::DateFormat::ISODate));
	exec(query);
}
// прочитать пока ещё не отправленные сообщения из БД
std::vector<DescChatMessage> Database::read_unsent_messages()
{
	std::vector<DescChatMessage> unsent_messages;
	QSqlQuery query(m_database);
	query.prepare("SELECT * FROM unsent_messages");
	if (exec(query))
	{
		while (query.next())
		{
			DescChatMessage msg;
			msg.local_id = query.value("local_id").toUuid();
			msg.chat_id = query.value("chat_id").toULongLong();
			msg.sender_id = query.value("sender_id").toUInt();
			msg.type = static_cast<ChatMessageType>(query.value("type").toInt());
			msg.text = query.value("txt").toString();
			msg.datetime = str_to_datetime(query.value("datetime").toString());	//QDateTime::fromString(query.value("datetime").toString(), Qt::DateFormat::ISODate);
			assert( msg.datetime.timeSpec() == Qt::TimeSpec::UTC );
			unsent_messages.push_back(msg);
		}
	}
	return unsent_messages;
}
// удаление из базы не отправленного сообщения (сообщение получено сервером)
void Database::delete_unsent_message(const ChatMessageLocalId& local_id)
{
	QSqlQuery query(m_database);
	query.prepare("DELETE FROM unsent_messages WHERE local_id=?");
	query.addBindValue(local_id);
	exec(query);
}