//=====================================================================================//
//   Author: open
//   Date:   05.02.2019
//=====================================================================================//
#pragma once
#include "database/database_properties.h"
#include "chat_message.h"
#include <QSqlDatabase>

class Department;
class User;
class Chat;

class Database 
{
private:
	QSqlDatabase m_database;
	DatabaseProperties m_properties;

public:
	Database(const QString& profile_path);
	virtual ~Database();
	// таблица свойств
	DatabaseProperties& properties();
	// выбрать все департаменты
	std::vector<DescDepartment> select_departments();
	// обновить департаменты
	void update_departaments(const std::vector<const Department*>& departments);
	// выбрать всех пользователей
	std::vector<DescUser> select_users();	
	// обновить контакты
	void update_users(const std::vector<const User*>& users);
	// выбрать чаты
	std::vector<DescChat> select_chats();
	// выбрать чат-лист
	std::vector<DescChatListContact> select_chat_list();
	// выбрать непрочитанные сообщения в чатах
	using unreaded_messages_t = std::set<ChatMessageId>;
	std::map<ChatMessageId, unreaded_messages_t> select_unreaded_messages(const User* my_user);
	// обновить чат
	void update_chat(const Chat* chat);
	// обновить участников чата
	void update_chat_users(const Chat* chat);
	// обновить чат-лист
	void update_chat_list(const std::vector<const Chat*>& chat_list);
	// добавить чат в чат лист
	bool add_chat_to_chat_list(const Chat* chat);
	// удалить чат из чат листа
	void remove_chat_from_chat_list(ChatId chat_id);
	// обновить сообщения
	void update_chat_messages(const std::vector<DescChatMessage>& messages, const std::set<const Chat*> to_top_chats);
	// обновить сообщение
	//void update_chat_message(const ChatMessage& chat_message);
	// выбрать сообщения чата
	std::vector<DescChatMessage> select_chat_messages(ChatId chat_id, ChatMessageId message_id, int num);
	// записать прочтение сообщения чата
	void set_messages_readed(const Chat* chat, ChatMessageId last_message_id, ChatMessageId new_message_id, const QDateTime& datetime_readed, UserId my_user_id);
	// записать пока ещё не отправленное сообщение в БД
	void add_unsent_message(const DescChatMessage& msg);
	// прочитать пока ещё не отправленные сообщения из БД
	std::vector<DescChatMessage> read_unsent_messages();
	// удаление из базы не отправленного сообщения (сообщение получено сервером)
	void delete_unsent_message(const ChatMessageLocalId& local_id);

private:
	QSqlDatabase connect_database(const QString& filename) const;
	// внести сообщения
	bool insert_messages(const std::vector<DescChatMessage>& messages);
	// обновить у чатов last_time
	bool update_chat_last_time(const std::set<const Chat*> chats);
	// обновить id последнего прочитанного сообщения
	bool update_last_readed_message_id(const Chat* chat, ChatMessageId message_id);
	// установить статус сообщения на прочитанное
	bool set_readed_status_message(ChatMessageId last_message_id, ChatMessageId new_message_id, const QDateTime& datetime_readed, UserId my_user_id);
};

inline DatabaseProperties& Database::properties() {
	return m_properties;
}