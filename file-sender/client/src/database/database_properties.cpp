//=====================================================================================//
//   Author: open
//   Date:   03.05.2018
//=====================================================================================//
#include "base/precomp.h"
#include "database/database_properties.h"
#include "database_utility.h"
#include <QSqlQuery>
#include <QVariant>

DatabaseProperties::DatabaseProperties(QSqlDatabase* database)
: m_database(database)
{
	QSqlQuery query;
	query.prepare("select key, value from properties");
	if (exec(query))
	{
		while (query.next())
			m_values[query.value(0).toString()] = query.value(1).toString();
	}
}
const QString& DatabaseProperties::get(const QString& key) const
{
	values_t::const_iterator it = m_values.find(key);
	if (m_values.end() != it) return it->second;
	static QString temp;
	return temp;
}
void DatabaseProperties::set(const QString& key, const QString& value)
{
	m_values[key] = value;

	QSqlQuery query;
	query.prepare("insert or replace into properties(key, value) values(?, ?)");
	query.addBindValue(key);
	query.addBindValue(value);
	exec(query);
}