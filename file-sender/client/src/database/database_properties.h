//=====================================================================================//
//   Author: open
//   Date:   03.05.2018
//=====================================================================================//
#pragma once

class QSqlDatabase;

class DatabaseProperties
{
private:
	QSqlDatabase* m_database;
	using values_t = std::map<QString, QString>;
	values_t m_values;

public:
	DatabaseProperties(QSqlDatabase* database);

	const QString& get(const QString& key) const;
	void set(const QString& key, const QString& value);
};