//=====================================================================================//
//   Author: open
//   Date:   15.02.2019
//=====================================================================================//
#include "base/precomp.h"
#include "database_utility.h"
#include <QSqlError>
#include <QSqlQuery>

bool exec(QSqlQuery& query)
{
	if (!query.exec()) {
		logs("error exec sqlquery: " + query.lastError().text() + " query: " + query.lastQuery());
		return false;
	}
	return true;
}