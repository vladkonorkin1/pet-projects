#include "base/precomp.h"
#include "database_update_manager.h"
#include <QSqlDatabase>
#include <QTextStream>
#include <QSqlError>
#include <QSqlQuery>
#include <QFile>
#include <QDir>

DatabaseUpdateManager::DatabaseUpdateManager(const QString& database_path)
: m_database_folder_path(database_path)
{
}
// проверка таймштампа базы данных
bool DatabaseUpdateManager::check_database_timestamp(Timestamp server_database_timestamp)
{
	Timestamp client_database_timestamp = read_from_file(path_database_timestamp()).toLongLong();
	bool is_compare = client_database_timestamp == server_database_timestamp;
	if (!is_compare)
	{
		// таймштампы базы сервера и клиента не совпадают.. удаляем локальную базу
		logs(QString("client and server database timestamp different %1 != %2").arg(client_database_timestamp).arg(server_database_timestamp));
	}
	return is_compare;
}

bool DatabaseUpdateManager::write_database_timestamp(Timestamp server_database_timestamp) const
{
	QFile file(path_database_timestamp());
	if (file.open(QFile::WriteOnly | QFile::Truncate | QFile::Text))
	{
		QTextStream stream(&file);
		stream.setCodec("UTF-8");
		stream << server_database_timestamp;
		return true;
	}
	return false;
}
// удаление базы данных
void DatabaseUpdateManager::remove_database(Timestamp server_database_timestamp)
{
	// delete database
	if (!remove_file(path_database()))
	{
		logs(QString("error to delete database %1").arg(path_database()));
		return;
	}

	// delete version
	if (!remove_file(path_database_version()))
	{
		logs(QString("error to delete version database %1").arg(path_database_version()));
		return;
	}

	// write server database timestamp
	if (!write_database_timestamp(server_database_timestamp))
	{
		logs(QString("error write server database timestamp %1").arg(server_database_timestamp));
		return;
	}
}
// путь до файла с таймштампом
QString DatabaseUpdateManager::path_database_timestamp() const
{
	return m_database_folder_path + "timestamp";
}
// путь до базы данных
QString DatabaseUpdateManager::path_database() const
{
	return m_database_folder_path + "data.db";
}
// миграция базы данных
bool DatabaseUpdateManager::migrate()
{
	// получаем список необходимых миграций
	int old_version = read_database_version();

	if (old_version == 0 && QFile::exists(path_database()))
	{
		logs(QString("migration: update database version to 1"));
		old_version = 1;
		write_database_version(old_version);

		remove_file(m_database_folder_path + "database_timestamp");
	}


	std::vector<MigrationFile> migration_files = select_migration_files(old_version);
	if (migration_files.empty()) return true;

	// сортируем в порядке возрастания
	std::sort(migration_files.begin(), migration_files.end(), [](const MigrationFile& left, const MigrationFile& right) { return left.number < right.number; });
	int version = migration_files[migration_files.size() - 1].number;

	{
		// подключаемся к базе данных
		QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE", "update_database_sima_chat");
		database.setDatabaseName(path_database());
		if (!database.open())
		{
			QString error("cannot open database: " + database.databaseName());
			logs(error);
			throw std::runtime_error(qPrintable(error));
			return false;
		}

		if (!database.transaction() ||
			!migrate_scripts(database, migration_files) ||
			!write_database_version(version))
		{
			database.rollback();
			database.close();
			QSqlDatabase::removeDatabase("update_database_sima_chat");
			return false;
		}

		database.commit();
	}
	QSqlDatabase::removeDatabase("update_database_sima_chat");
	return true;
}
// выбор файлов для миграции
std::vector<DatabaseUpdateManager::MigrationFile> DatabaseUpdateManager::select_migration_files(int old_version) const
{
	std::vector<MigrationFile> files;
	QFileInfoList file_infos = QDir("://database_migrations").entryInfoList();
	for (const QFileInfo& file_info : file_infos)
	{
		// разделяем номер и имя миграции
		QStringList name_parts = file_info.fileName().split("-", QString::SplitBehavior::SkipEmptyParts);
		int number = name_parts.at(0).toInt();
		if (number > old_version)
			files.push_back(MigrationFile{ number, file_info.filePath() });
	}
	return files;
}
bool DatabaseUpdateManager::migrate_scripts(QSqlDatabase& database, const std::vector<MigrationFile>& migration_files)
{
	// исполняем миграции
	for (const MigrationFile& file : migration_files)
	{
		if (!migrate_script(database, file.path))
		{
			logs(QString("migration: sql script migration error %1").arg(file.path));
			return false;
		}
		logs(QString("migration: database migrate to version: %1 script: %2").arg(file.number).arg(file.path));
	}
	return true;
}
// исполнение миграции sql-скрипта
bool DatabaseUpdateManager::migrate_script(QSqlDatabase& database, const QString& file_path)
{
	QFile file(file_path);
	if (file.open(QFile::ReadOnly | QFile::Text))
	{
		QTextStream stream(&file);
		stream.setCodec("UTF-8");
		QString text = stream.readAll();
		auto str_queries = text.split(';', QString::SplitBehavior::SkipEmptyParts);
		QSqlQuery query(database);

		for (const QString& str_query : str_queries)
		{
			if (!query.exec(str_query))
			{
				logs("error to update database: " + str_query + "  error: " + query.lastError().text());
				return false;
			}
		}
	}
	return true;
}
// путь до файла с номером текущей версии базы данных
QString DatabaseUpdateManager::path_database_version() const
{
	return m_database_folder_path + "version";
}
// чтение версии базы данных
int DatabaseUpdateManager::read_database_version()
{
	QString buffer = read_from_file(path_database_version());
	if (!buffer.isEmpty())
		return buffer.toUInt();
	return -1;
}
// запись версии базы данных
bool DatabaseUpdateManager::write_database_version(int version)
{
	QFile file(path_database_version());
	if (file.open(QFile::WriteOnly))
	{
		QTextStream stream(&file);
		stream.setCodec("UTF-8");
		stream << version;
		return true;
	}
	return false;
}
QString DatabaseUpdateManager::read_from_file(const QString& path) const
{
	QFile file(path);
	if (file.open(QFile::ReadOnly | QFile::Text))
	{
		QTextStream stream(&file);
		stream.setCodec("UTF-8");
		return stream.readAll();
	}
	return QString();
}
bool DatabaseUpdateManager::remove_file(const QString& path) const
{
	if (QFile::exists(path))
		return QFile::remove(path);
	return true;
}