#pragma once

class QSqlDatabase;

class DatabaseUpdateManager
{
private:
	QString m_database_folder_path;

	struct MigrationFile
	{
		int number;
		QString path;
	};

public:
	DatabaseUpdateManager(const QString& database_path);
	// проверка таймштампа базы данных
	bool check_database_timestamp(Timestamp server_database_timestamp);
	// удаление базы данных
	void remove_database(Timestamp server_database_timestamp);
	// миграция базы данных
	bool migrate();
	
private:
	// выбор файлов для миграции
	std::vector<MigrationFile> select_migration_files(int old_version) const;
	// запись версии базы данных
	bool write_database_version(int version);
	// чтение версии базы данных
	int read_database_version();
	// путь до файла с номером текущей версии базы данных
	QString path_database_version() const;
	// исполнение миграции sql-скрипта
	bool migrate_script(QSqlDatabase& database, const QString& file_path);
	bool migrate_scripts(QSqlDatabase& database, const std::vector<MigrationFile>& migration_files);
	// прочитать из файла текст
	QString read_from_file(const QString& path) const;
	// удалить файл
	bool remove_file(const QString& path) const;
	// путь до файла с таймштампом
	QString path_database_timestamp() const;
	// путь до базы данных
	QString path_database() const;
	bool write_database_timestamp(Timestamp server_database_timestamp) const;
};

