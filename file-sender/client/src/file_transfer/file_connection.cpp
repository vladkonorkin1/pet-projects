//=====================================================================================//
//   Author: open
//   Date:   14.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "file_connection.h"

static const quint64 NET_BLOCK_SIZE		= 64 * 1024;		// 64 KB
static const quint64 FILE_BLOCK_SIZE	=  5 * 1024 * 1024;	// 5 MB

FileConnection::FileConnection(ConnectionId id, const QString& host_address)
: m_id(id)
, m_id_sended_bytes(0)
, m_buffer_cursor(0)
, m_file_cursor(0)
, m_sended_bytes(0)
, m_cancel(false)
, m_timer(new Base::Timer(1.f))
, m_alive_time(0.f)
{
	connect(&m_socket, SIGNAL(connected()), SLOT(slot_start_connection()));
	connect(&m_socket, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(slot_close_connection(QAbstractSocket::SocketError)));
	connect(&m_socket, SIGNAL(bytesWritten(qint64)), SLOT(slot_bytes_written(qint64)));
	m_socket.connectToHost(dns_lookup(host_address), FileServerPort);

	connect(m_timer.get(), SIGNAL(updated(float)), SLOT(slot_timer_updated(float)));
	logs(QString("dbg >> FileConnection::FileConnection()"));
}
QHostAddress FileConnection::dns_lookup(const QString& host_address) const
{
	QHostInfo info = QHostInfo::fromName(host_address);
	if (!info.addresses().isEmpty())
		return info.addresses().first();
	return QHostAddress();
}
void FileConnection::slot_close_connection(QAbstractSocket::SocketError error)
{
	logs(QString("dbg >> FileConnection::slot_close_connection()"));
	Q_UNUSED(error);
	emit disconnected();
}
void FileConnection::slot_start_connection()
{
	logs(QString("dbg >> FileConnection::slot_start_connection()"));

	// first of all we send connection id
	// copy to buffer
	int size = sizeof(ConnectionId);
	m_buffer.resize(size);
	memcpy(m_buffer.data(), reinterpret_cast<const char*>(&m_id), size);

	m_cancel = false;
	m_sended_bytes = 0;
	m_buffer_cursor = 0;
	write_file_buffer_to_socket();
}
void FileConnection::slot_bytes_written(qint64 bytes)
{
	set_alive();

	if (m_id_sended_bytes == sizeof(ConnectionId))
	{
		write_file_buffer_to_socket();
		return;
	}

	logs(QString("dbg >> FileConnection::slot_bytes_written() 2 bytes=%1").arg(bytes));

	m_id_sended_bytes += bytes;

	if (m_id_sended_bytes == sizeof(ConnectionId))
	{
		logs(QString("dbg >> FileConnection::slot_bytes_written() connected"));
		emit connected();
	}
}
bool FileConnection::send_file(const QString& file_path, quint64& size)
{
	logs(QString("dbg >> FileConnection::send_file() file_path=%1 size=%2").arg(file_path).arg(size));

	m_cancel = false;
	m_sended_bytes = 0;

	assert( !m_file.isOpen() );
	m_file.setFileName(file_path);
	if (m_file.open(QIODevice::ReadOnly))
	{
		size = m_file.size();
		logs(QString("dbg >> FileConnection::send_file() 2 file_path=%1 size=%2").arg(file_path).arg(size));

		m_file_cursor = 0;
		emit file_start_transfer(size);
		read_portion();
		return true;
	}

	logs(QString("dbg >> FileConnection::send_file() 3 file_path=%1 size=%2").arg(file_path).arg(size));
	return false;
}
void FileConnection::read_portion()
{
	logs(QString("dbg >> FileConnection::read_portion()"));

	if (m_cancel)
	{
		m_file.close();
		//emit file_cancel_transfer(m_sended_bytes);
		emit file_finish_transfer(m_sended_bytes);
		return;
	}

	m_buffer_cursor = 0;
	m_buffer.resize(std::min<quint64>(m_file.size() - m_file_cursor, FILE_BLOCK_SIZE));

	logs(QString("dbg >> FileConnection::read_portion() 2 buffer.size=%1").arg(m_buffer.size()));

	if (m_buffer.empty())
	{
		logs(QString("dbg >> FileConnection::read_portion() 3 m_buffer.empty()"));

		m_file.close();
		emit file_finish_transfer(m_sended_bytes);
		return;
	}

	quint64 bytes_readed = m_file.read(m_buffer.data(), m_buffer.size());
	assert( bytes_readed == m_buffer.size() );
	if (bytes_readed != m_buffer.size())
		m_buffer.resize(bytes_readed);
	m_file_cursor += m_buffer.size();
	write_file_buffer_to_socket();
}
void FileConnection::write_file_buffer_to_socket()
{
	if (m_buffer_cursor < m_buffer.size() && !m_cancel)
	{
		qint64 written = m_socket.write(m_buffer.data() + m_buffer_cursor, qMin<quint64>(m_buffer.size() - m_buffer_cursor, NET_BLOCK_SIZE));
		m_buffer_cursor += written;
		m_sended_bytes += written;
		//logs(QString("sended bytes = %1").arg(m_sended_bytes));
	}
	else
		read_portion();
}
void FileConnection::cancel()
{
	m_cancel = true;
}
quint64 FileConnection::sended_bytes() const
{
	return m_sended_bytes;
}
void FileConnection::slot_timer_updated(float dt)
{
	if (!m_socket.isValid())
		return;

	m_alive_time += dt;
	if (m_alive_time > 10.f)
	{
		m_timer.reset();
		logs(QString("dbg >> FileConnection::slot_timer_updated() close socket"));
		m_socket.abort();
		slot_close_connection(QAbstractSocket::SocketError::NetworkError);
		return;
	}
}
void FileConnection::set_alive()
{
	m_alive_time = 0.f;
}