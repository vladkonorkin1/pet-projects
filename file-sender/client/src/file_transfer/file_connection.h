//=====================================================================================//
//   Author: open
//   Date:   14.04.2017
//=====================================================================================//
#pragma once
#include "network/types.h"
#include <QTcpSocket>
#include <QFile>
#include <QHostInfo>
#include <QHostAddress>
#include "base/timer.h"

class FileConnection : public QObject
{
	Q_OBJECT
private:
	QTcpSocket m_socket;
	ConnectionId m_id;
	int m_id_sended_bytes;

	QFile m_file;
	quint64 m_file_cursor;
	std::vector<char> m_buffer;
	unsigned int m_buffer_cursor;
	quint64 m_sended_bytes;
	bool m_cancel;

	std::unique_ptr<Base::Timer> m_timer;
	float m_alive_time;
	
public:
	FileConnection(ConnectionId id, const QString& host_address);

	ConnectionId id() const { return m_id; }
	bool send_file(const QString& file_path, quint64& size);
	void cancel();

	quint64 sended_bytes() const;

signals:
	void connected();
	void disconnected();
	void file_start_transfer(quint64 size);
	void file_finish_transfer(quint64 size);

private slots:
	void slot_start_connection();
	void slot_close_connection(QAbstractSocket::SocketError error);
	void slot_bytes_written(qint64 bytes);

	void slot_timer_updated(float dt);

private:
	void read_portion();
	void write_file_buffer_to_socket();
	QHostAddress dns_lookup(const QString& host_address) const; //����� ip �� ��������� �����
	void set_alive();
};