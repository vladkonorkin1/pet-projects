//=====================================================================================//
//   Author: open
//   Date:   26.05.2017
//=====================================================================================//
#include "base/precomp.h"
#include "file_dialog.h"
#include <QPushButton>
/*
FileDialog::FileDialog(QWidget* parent)
: QFileDialog(parent)
{
	setOption(QFileDialog::DontUseNativeDialog);
	setFileMode(QFileDialog::ExistingFiles);
	setReadOnly(true);
	for (auto *pushButton : findChildren<QPushButton*>()) {
		//qDebug() << pushButton->text();
		if (pushButton->text() == "&Open" || pushButton->text() == "&Choose") {
			m_button_open = pushButton;
			break;
		}
	}
	//disconnect(m_button_open, SIGNAL(clicked()));
	disconnect(m_button_open, SIGNAL(clicked(bool)));
	disconnect(m_button_open, SIGNAL(pressed()));
	disconnect(m_button_open, SIGNAL(released()));
	connect(m_button_open, &QPushButton::clicked, this, &FileDialog::slot_open_clicked);
	connect(this, SIGNAL(filesSelected(const QStringList&)), SLOT(slot_files_selected(const QStringList&)));
	//connect(this, SIGNAL(currentChanged(const QString&)), SLOT(slot_current_changed(const QString&)));
}

const QStringList& FileDialog::exec_dialog()
{
	exec();
	QDir dir = directory();
	if (!m_selected.isEmpty() && dir.absolutePath() == m_selected[0]) {
		QDir dir = directory();
		dir.cdUp();
		setDirectory(dir);
	}
	return m_selected;
}

void FileDialog::slot_files_selected(const QStringList& files)
{
	m_selected = files;
}
void FileDialog::slot_open_clicked()
{
	QStringList list = this->selectedFiles();
	if (list.empty()) m_selected.append(directory().absolutePath());
	else m_selected = list;
	close();
}*/