//=====================================================================================//
//   Author: open
//   Date:   13.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "file_transfer.h"
#include "file_connection.h"

FileTransfer::FileTransfer(const FileTransferDesc& desc, FileConnection* file_connection)
: m_file_connection(file_connection)
, m_desc(desc)
, m_folder_path(m_desc.folder_path)
, m_files(m_desc.files)
, m_file_index(0)
, m_cancel(false)//m_desc.cancel)
, m_transfer(false)
, m_send_finish(false)
, m_user_cancel(false)
, m_cancel_file_index(0)
, m_cancel_size(0)
{
}
FileTransfer::~FileTransfer()
{
	// мьютекс защищает выполнение функций во втором потоке, когда объект удаляют из главного
	QMutexLocker locker(&m_mutex);
}
void FileTransfer::send()
{
	logs(QString("dbg >> FileTransfer::send()"));

	QMutexLocker locker(&m_mutex);

	m_transfer = true;

	// запускаем передачу файлов
	connect(m_file_connection, SIGNAL(file_start_transfer(quint64)), SLOT(slot_file_connection_start_transfer(quint64)));
	connect(m_file_connection, SIGNAL(file_finish_transfer(quint64)), SLOT(slot_file_connection_finish_transfer(quint64)));

	emit send_message_connection_transfer_start(m_desc);
	next();
}
void FileTransfer::next()
{
	logs(QString("dbg >> FileTransfer::next() m_file_index=%1 size=%2").arg(m_file_index).arg(m_files.size()));

	// переходим к следующему файлу
	//for (; m_file_index < m_files.size() && !m_cancel; ++m_file_index)
	while (m_file_index < m_files.size() && !m_cancel)
	{
		quint64 size = 0;
		if (m_file_connection->send_file(m_folder_path + '/' + m_files[m_file_index], size))
		{
			break;
		}
		m_file_index++;
	}

	logs(QString("dbg >> FileTransfer::next() 2  m_file_index=%1 size=%2").arg(m_file_index).arg(m_files.size()));

	// проверяем завершение
	if (m_file_index >= m_files.size() || m_cancel)
	{
		logs(QString("dbg >> FileTransfer::next() 3"));
		if (!m_send_finish)
		{
			m_send_finish = true;
			
			logs(QString("dbg >> FileTransfer::next() 4"));

			if (m_cancel)
				emit send_message_connection_transfer_cancel(m_cancel_file_index, m_cancel_size, m_user_cancel);
			emit send_message_connection_transfer_finish();
		}
	}
}
void FileTransfer::slot_file_connection_start_transfer(quint64 size)
{
	logs(QString("dbg >> FileTransfer::slot_file_connection_start_transfer() m_file_index=%1 size=%2").arg(m_file_index).arg(size));

	emit send_message_connection_transfer_read_file(m_file_index, size);
}
void FileTransfer::slot_file_connection_finish_transfer(quint64 size)
{
	logs(QString("dbg >> FileTransfer::slot_file_connection_finish_transfer() m_file_index=%1 size=%2").arg(m_file_index).arg(size));

	Q_UNUSED( size );
	// сообщаем что файл отправлен и переходим к следующему
	++m_file_index;
	next();
}
//#include <chrono>
//#include <thread>
void FileTransfer::cancel(bool user_cancel)
{
	logs(QString("dbg >> FileTransfer::cancel()"));

	QMutexLocker locker(&m_mutex);

	if (m_cancel) return;

	//logs(QString("FileTransfer::cancel %1").arg(m_desc.message_local_id.toString()));
	//std::this_thread::sleep_for(std::chrono::seconds(10));

	m_cancel = true;
	m_user_cancel = user_cancel;
	m_file_connection->cancel();
	m_cancel_file_index = m_file_index;
	m_cancel_size = m_file_connection->sended_bytes();
}