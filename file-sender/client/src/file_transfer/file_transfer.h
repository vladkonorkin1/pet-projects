//=====================================================================================//
//   Author: open
//   Date:   13.04.2017
//=====================================================================================//
#pragma once

struct FileTransferDesc
{
	ChatMessageLocalId message_local_id;
	ChatMessageId message_id = 0;
	ChatId chat_id = 0;
	QString path;
	QString folder_path;
	QString name;
	bool is_dir = false;
	quint64 size = 0;
	std::vector<QString> files;
	bool cancel = false;

	FileTransferDesc() {}
	FileTransferDesc(const ChatMessageLocalId& message_local_id, ChatMessageId message_id, ChatId chat_id, const QString& path, const QString& folder_path, const QString& name, bool is_dir, quint64 size, const std::vector<QString>& files, bool cancel)
		: message_id(message_id), message_local_id(message_local_id), chat_id(chat_id), path(path), folder_path(folder_path), name(name), is_dir(is_dir), size(size), files(files), cancel(cancel) {}
};
using SFileTransferDesc = std::shared_ptr<FileTransferDesc>;


class FileConnection;

class FileTransfer : public QObject
{
	Q_OBJECT
private:
	FileConnection* m_file_connection;
	FileTransferDesc m_desc;
	const QString& m_folder_path;
	const std::vector<QString>& m_files;
	unsigned int m_file_index;
	bool m_cancel;
	bool m_transfer;
	bool m_send_finish;
	bool m_user_cancel;
	unsigned int m_cancel_file_index;
	quint64 m_cancel_size;
	QMutex m_mutex;	// мьютекс защищает выполнение функций во втором потоке, когда объект удаляют из главного

public:
	FileTransfer(const FileTransferDesc& desc, FileConnection* file_connection);
	virtual ~FileTransfer();

public slots:
	void send();
	void cancel(bool user_cancel);

signals:
	void send_message_connection_transfer_start(const FileTransferDesc& desc);
	void send_message_connection_transfer_read_file(unsigned int file_index, quint64 size);
	void send_message_connection_transfer_finish();
	void send_message_connection_transfer_cancel(unsigned int file_index, quint64 transfer_bytes, bool user_cancel);

private slots:
	void slot_file_connection_start_transfer(quint64 size);
	void slot_file_connection_finish_transfer(quint64 size);

private:
	void next();
};