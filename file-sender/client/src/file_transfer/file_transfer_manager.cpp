//=====================================================================================//
//   Author: open
//   Date:   13.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "file_transfer_manager.h"
#include "sending_messages.h"
#include "thread_task_manager.h"
#include "message_connection.h"
#include "chats.h"
#include "user.h"

FileTransferManager::FileTransferManager(ThreadTaskManager& thread_task_manager, Chats& chats, SendingMessages& sending_messages, const QString& host_address)
: m_thread_task_manager(thread_task_manager)
, m_chats(chats)
, m_sending_messages(sending_messages)
, m_login_user(nullptr)
, m_message_connection(nullptr)
, m_host_address(host_address)
, m_current_parse_folder(nullptr)
{
	connect(&m_sending_messages, SIGNAL(chat_message_sended(const ChatMessage*)), SLOT(slot_chat_message_sended(const ChatMessage*)));
}
// установить пользователя, под которым залогинились
void FileTransferManager::set_login_user(const User* user)
{
	m_login_user = user;
}
// установить подключение
void FileTransferManager::set_message_connection(MessageConnection* message_connection)
{
	m_message_connection = message_connection;
	if (message_connection)
	{
		check_connecting();
	}
	else
	{
		destroy();
	}
}
// переподключение
void FileTransferManager::check_connecting()
{
	logs(QString("dbg >> FileTransferManager::check_connecting()"));

	if (m_transfers.empty()) return;

	if (!m_file_transfer_thread)
	{
		if (!m_message_connection) return;

		logs(QString("dbg >> create FileTransferThread"));

		m_file_transfer_thread.reset(new FileTransferThread(m_message_connection, m_host_address));
		connect(m_file_transfer_thread.get(), SIGNAL(file_connection_created()), SLOT(slot_file_connection_created()));
		connect(m_file_transfer_thread.get(), SIGNAL(file_connection_destroyed()), SLOT(slot_file_connection_destroyed()));
	}
}
// подключение файлового соединения
void FileTransferManager::slot_file_connection_created()
{
	logs(QString("dbg >> FileTransferManager::slot_file_connection_created()"));
	next_run_transfer();
}
void FileTransferManager::destroy()
{
	logs(QString("dbg >> FileTransferManager::destroy()"));

	if (m_file_transfer_thread)
		m_file_transfer_thread.reset();

	if (m_current_transfer)
		m_current_transfer.reset();
}
#include <QTimer>
// отключение файлового соединения
void FileTransferManager::slot_file_connection_destroyed()
{
	logs(QString("dbg >> FileTransferManager::slot_file_connection_destroyed()"));
	destroy();
	QTimer::singleShot(2000, this, SLOT(slot_reconnect()));
}
void FileTransferManager::slot_reconnect()
{
	logs(QString("dbg >> FileTransferManager::slot_reconnect()"));
	check_connecting();
}
// добавить передачу файла/папки
void FileTransferManager::add_file_transfer(const ChatMessageLocalId& message_local_id, ChatId chat_id, const QString& path)
{
	logs(QString("dbg >> FileTransferManager::add_file_transfer() message_local_id=%1 chat_id=%2 path=%3").arg(message_local_id.toString()).arg(chat_id).arg(path));

	UTaskScanFolder task(new TaskScanFolder(path));
	TaskScanFolder* ptask = task.get();
	connect(ptask, SIGNAL(finished(const std::vector<QString>&, const QString&, const QString&, const QString&, bool, quint64)), SLOT(slot_parse_folder_finished(const std::vector<QString>&, const QString&, const QString&, const QString&, bool, quint64)));
	
	add_message_to_ui(message_local_id, chat_id, task->path(), task->is_dir(), 0);

	// добавить файловое сообщение на парсинг папки
	m_parse_folders.push_back(UParseFolder(new ParseFolder{ message_local_id, chat_id, path, std::move(task), ptask, false }));
	next_parse_folder();
}
// запустить следующее передаваемое сообщение на парсинг файлов и подсчета размера файлов
void FileTransferManager::next_parse_folder()
{
	logs(QString("dbg >> FileTransferManager::next_parse_folder()"));

	if (m_current_parse_folder) return;
	if (m_parse_folders.empty()) return;

	m_current_parse_folder = m_parse_folders.front().get();
	// ставим в другой поток на парсинг файлов
	m_thread_task_manager.post_task(m_current_parse_folder->task_scan.release());
}
// событие завершения парсинга папки/файла
void FileTransferManager::slot_parse_folder_finished(const std::vector<QString>& files, const QString& path, const QString& folder_path, const QString& name, bool is_dir, quint64 size)
{
	logs(QString("dbg >> FileTransferManager::slot_parse_folder_finished() %1").arg((int64_t)m_current_parse_folder));

	if (!m_current_parse_folder) return;

	ChatMessageLocalId message_local_id = m_current_parse_folder->message_local_id;
	ChatId chat_id = m_current_parse_folder->chat_id;
	add_message_to_ui(message_local_id, chat_id, path, is_dir, size);
	
	// добавляем сообщение в очередь на регистрацию
	m_registers[message_local_id] = SFileTransferDesc(new FileTransferDesc(
		message_local_id,
		0,
		chat_id,
		path,
		folder_path,
		name,
		is_dir,
		size,
		files,
		m_current_parse_folder->cancel));

	// ставим на отправку
	m_sending_messages.add_file_message(m_login_user, message_local_id, chat_id, path, is_dir, size);

	m_parse_folders.pop_front();
	m_current_parse_folder = nullptr;
	next_parse_folder();
}
// обработать регистрацию сообщения
void FileTransferManager::slot_chat_message_sended(const ChatMessage* msg)
{
	logs(QString("dbg >> FileTransferManager::slot_chat_message_sended()"));

	auto it = m_registers.find(msg->local_id);
	if (m_registers.end() != it)
	{
		// файловое сообщение зарегистрировано
		SFileTransferDesc file_transfer_desc = it->second;
		file_transfer_desc->message_id = msg->id;
		
		if (file_transfer_desc->cancel)
		{
			// если отмена
			add_cancel(msg->id);
		}
		else
		{
			// ставим в очередь на отправку
			m_transfers.push_back(file_transfer_desc);
			next_run_transfer();
		}
		m_registers.erase(it);
	}
}
// завершить передачу
void FileTransferManager::finish_transfer(FileTransferResult result, const QString& dest_name)
{
	logs(QString("dbg >> FileTransferManager::finish_transfer()"));

	if (!m_current_transfer || !m_file_transfer_thread)
		return;

	logs(QString("dbg >> FileTransferManager::finish_transfer() 2"));
	
	// передача завершена
	m_file_transfer_thread->finish_transfer();

	/*add_message_to_server(
		m_current_transfer->message_local_id,
		m_current_transfer->chat_id,
		m_current_transfer->path,
		dest_name,
		m_current_transfer->is_dir,
		m_current_transfer->size,
		result
	);*/

	m_current_transfer.reset();

	if (!m_transfers.empty())
		m_transfers.pop_front();

	// запускаем следующую передачу
	next_run_transfer();
}
// прервать передачу
void FileTransferManager::stop_transfer()
{
	logs(QString("dbg >> FileTransferManager::stop_transfer()"));

	if (m_current_transfer && m_file_transfer_thread)
	{
		m_file_transfer_thread->stop_transfer();
	}
}
// получить текущий передаваемый id чата
ChatId FileTransferManager::get_current_trasfer_chat_id() const
{
	if (m_current_transfer) return m_current_transfer->chat_id;
	return 0;
}
void FileTransferManager::next_run_transfer()
{
	logs(QString("dbg >> FileTransferManager::next_run_transfer()"));

	if (m_current_transfer) return;
	if (!m_file_transfer_thread)
	{
		check_connecting();
		return;
	}
	if (m_transfers.empty()) return;

	logs(QString("dbg >> FileTransferManager::next_run_transfer() 2"));

	m_current_transfer = m_transfers.front();
	m_file_transfer_thread->start_transfer(*m_current_transfer);
}
// снять с передачи файловое сообщение
void FileTransferManager::cancel_file_message(const ChatMessageLocalId& message_local_id)
{
	logs(QString("dbg >> FileTransferManager::cancel_file_message() m_transfers.size=%1 m_parse_folders=%2").arg(m_transfers.size()).arg(m_parse_folders.size()));

	for (auto it = m_transfers.begin(); it != m_transfers.end(); it++)
	{
		FileTransferDesc* desc = it->get();
		if (desc->message_local_id == message_local_id)
		{
			if (m_current_transfer &&
				m_current_transfer->message_local_id == desc->message_local_id)
			{
				// сейчас идёт передача файла
				m_file_transfer_thread->cancel_transfer();
			}
			else
			{
				// передача файла ещё не начата
				
				/*// удаляем с интерфейса
				m_chats.remove_message(desc->chat_id, message_local_id);
				// удаляем из очереди
				m_transfers.erase(it);*/

				//desc.cancel = true;

				// сообщаем об отмене
				add_cancel(desc->message_id);

				// удаляем из очереди
				m_transfers.erase(it);
			}
			return;
		}
	}

	// смотрим в очереди на парсинге папок (составление списка файлов и подсчёта их размера)
	for (auto it = m_parse_folders.begin(); it != m_parse_folders.end(); it++)
	{
		ParseFolder* parse_folder = it->get();
		if (message_local_id == parse_folder->message_local_id)
		{
			parse_folder->cancel = true;
			parse_folder->ptask_scan->cancel();

			// удаляем с интерфейса
			//m_chats.remove_message(parse_folder->chat_id, message_local_id);
			return;
		}
	}
}
// добавления файлового сообщения
void FileTransferManager::add_message_to_ui(const ChatMessageLocalId& message_local_id, ChatId chat_id, const QString& path, bool is_dir, quint64 size)
{
	logs(QString("dbg >> FileTransferManager::add_message_to_ui()"));

	// добавляем новое файловое сообщение только в отображение
	DescChatMessage msg(
		0,
		message_local_id,
		chat_id,
		m_login_user->id,
		0,
		ChatMessageType::File,
		ChatMessageStatus::Sending,
		HistoryStatus::Created);
	msg.file_src_path = path;
	msg.file_is_dir = is_dir;
	msg.file_size = size;

	std::vector<DescChatMessage> messages;
	messages.push_back(msg);
	m_chats.add_messages(messages, false);
}
// добавить в очередь отмены
void FileTransferManager::add_cancel(ChatMessageId message_id)
{
	logs(QString("dbg >> FileTransferManager::add_cancel()"));

	m_cancels.insert(message_id);
	send_cancel(message_id);
}
// отправить сообщение об отмене передачи
void FileTransferManager::send_cancel(ChatMessageId message_id)
{
	logs(QString("dbg >> FileTransferManager::send_cancel()"));

	if (m_message_connection)
		m_message_connection->send_message(ClientMessageFileMessageCancel(message_id));
}
// переотправить сообщения
void FileTransferManager::resend()
{
	logs(QString("dbg >> FileTransferManager::resend()"));

	// заново отсылаем список на отмену
	for (ChatMessageId message_id : m_cancels)
		send_cancel(message_id);

	// заново регистрируем файлы на передачу
	for (const SFileTransferDesc& desc : m_transfers)
	{
		if (!desc->cancel)
			if (m_message_connection)
				m_message_connection->send_message(ClientMessageFileMessageRegister(desc->chat_id, desc->message_id));
	}
}
// обработать подтверждение отмены
void FileTransferManager::process_confirm_cancel(ChatMessageId message_id)
{
	logs(QString("dbg >> FileTransferManager::process_confirm_cancel()"));

	m_cancels.erase(message_id);
}