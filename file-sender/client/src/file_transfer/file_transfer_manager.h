//=====================================================================================//
//   Author: open
//   Date:   13.04.2017
//=====================================================================================//
#pragma once
#include "file_transfer/file_transfer_thread.h"
#include "task_scan_folder.h"

class SendingMessages;
class MessageConnection;
class ThreadTaskManager;
class ChatMessage;
class Chats;
class User;

class FileTransferManager : public QObject
{
	Q_OBJECT
private:
	ThreadTaskManager& m_thread_task_manager;
	Chats& m_chats;
	SendingMessages& m_sending_messages;
	const User* m_login_user;
	MessageConnection* m_message_connection;
	std::unique_ptr<FileTransferThread> m_file_transfer_thread;
	QString m_host_address;

	// 1) парсинг файлов и подсчёт размера файлов
	using UTaskScanFolder = std::unique_ptr<TaskScanFolder>;
	struct ParseFolder
	{
		ChatMessageLocalId message_local_id;
		ChatId chat_id;
		QString path;
		UTaskScanFolder task_scan;
		TaskScanFolder* ptask_scan;
		bool cancel;
	};
	using UParseFolder = std::unique_ptr<ParseFolder>;
	std::list<UParseFolder> m_parse_folders;
	ParseFolder* m_current_parse_folder;

	// 2) очередь на регистрацию
	std::map<ChatMessageLocalId, SFileTransferDesc> m_registers;

	// 3) очередь на передачу
	std::list<SFileTransferDesc> m_transfers;
	SFileTransferDesc m_current_transfer;

	// очередь на отмену (чтобы гарантировать получение сервером сигнала отмены)
	std::set<ChatMessageId> m_cancels;

public:
	FileTransferManager(ThreadTaskManager& thread_task_manager, Chats& chats, SendingMessages& sending_messages_manager, const QString& host_address);
	// установить подключение
	void set_message_connection(MessageConnection* message_connection);
	// добавить передачу файла/папки
	void add_file_transfer(const ChatMessageLocalId& message_local_id, ChatId chat_id, const QString& path);
	// завершить передачу
	void finish_transfer(FileTransferResult result, const QString& dest_name);
	// прервать передачу
	void stop_transfer();
	// снять с передачи файловое сообщение
	void cancel_file_message(const ChatMessageLocalId& message_local_id);
	// установить пользователя, под которым залогинились
	void set_login_user(const User* user);
	// переотправить сообщения
	void resend();
	// обработать подтверждение отмены
	void process_confirm_cancel(ChatMessageId message_id);
	// получить текущий передаваемый id чата
	ChatId get_current_trasfer_chat_id() const;

private slots:
	// подключение файлового соединения
	void slot_file_connection_created();
	// отключение файлового соединения
	void slot_file_connection_destroyed();
	// событие завершения парсинга папки/файла
	void slot_parse_folder_finished(const std::vector<QString>& files, const QString& path, const QString& folder_path, const QString& name, bool is_dir, quint64 size);
	// обработать регистрацию сообщения
	void slot_chat_message_sended(const ChatMessage* msg);

	void slot_reconnect();

private:
	// запустить следующее передаваемое сообщение на парсинг файлов и подсчета размера файлов
	void next_parse_folder();
	// подключение по необходимости
	void check_connecting();
	// запустить следующую передачу файла
	void next_run_transfer();
	// очистка 
	void destroy();
	// добавления файлового сообщения
	void add_message_to_ui(const ChatMessageLocalId& message_local_id, ChatId chat_id, const QString& path, bool is_dir, quint64 size);
	// регистрация файлового сообщения после передачи файла
	//void add_message_to_server(const ChatMessageLocalId& message_local_id, ChatId chat_id, const QString& src_path, const QString& dest_name, bool is_dir, quint64 size, FileTransferResult result);
	// добавить в очередь отмены
	void add_cancel(ChatMessageId message_id);
	// отправить сообщение об отмене передачи
	void send_cancel(ChatMessageId message_id);
};