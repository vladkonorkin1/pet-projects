//=====================================================================================//
//   Author: open
//   Date:   07.07.2019
//=====================================================================================//
#include "base/precomp.h"
#include "file_transfer_thread.h"
#include "message_connection.h"

FileTransferThread::FileTransferThread(MessageConnection* message_connection, const QString& host_address)
: m_message_connection(message_connection)
, m_message_connection_id(m_message_connection->id())
{
	connect(&m_thread, &QThread::started, this, [this, host_address]() { slot_thread_started(host_address); }, Qt::DirectConnection);
	m_thread.setObjectName(QString("thread file transfer %1").arg(m_message_connection_id));
	m_thread.start();
}
void FileTransferThread::slot_thread_started(const QString& host_address)
{
	logs("dbg >> FileTransferThread::slot_thread_started(const QString& host_address)");
	m_file_connection.reset(new FileConnection(m_message_connection_id, host_address));
	connect(m_file_connection.get(), SIGNAL(connected()), SLOT(slot_file_connection_connected()), Qt::QueuedConnection);
	connect(m_file_connection.get(), SIGNAL(disconnected()), SLOT(slot_file_connection_disconnected()), Qt::QueuedConnection);
}
FileTransferThread::~FileTransferThread()
{
	logs("dbg >> FileTransferThread::~FileTransferThread()");
	m_thread.quit();
	m_thread.wait();
}
void FileTransferThread::start_transfer(const FileTransferDesc& desc)
{
	// создаем новую передачу
	m_file_transfer.reset(new FileTransfer(desc, m_file_connection.get()));
	m_file_transfer->moveToThread(&m_thread);
	connect(this, SIGNAL(emit_start_transfer()), m_file_transfer.get(), SLOT(send()));
	connect(this, SIGNAL(emit_cancel_transfer(bool)), m_file_transfer.get(), SLOT(cancel(bool)));
	//connect(this, SIGNAL(emit_stop_transfer()), m_file_transfer.get(), SLOT(stop()));
	connect(m_file_transfer.get(), SIGNAL(send_message_connection_transfer_start(const FileTransferDesc&)), SLOT(slot_send_message_connection_transfer_start(const FileTransferDesc&)), Qt::QueuedConnection);
	connect(m_file_transfer.get(), SIGNAL(send_message_connection_transfer_read_file(unsigned int, quint64)), SLOT(slot_send_message_connection_transfer_read_file(unsigned int, quint64)), Qt::QueuedConnection);
	connect(m_file_transfer.get(), SIGNAL(send_message_connection_transfer_finish()), SLOT(slot_send_message_connection_transfer_finish()), Qt::QueuedConnection);
	connect(m_file_transfer.get(), SIGNAL(send_message_connection_transfer_cancel(unsigned int, quint64, bool)), SLOT(slot_send_message_connection_transfer_cancel(unsigned int, quint64, bool)), Qt::QueuedConnection);
	emit emit_start_transfer();
}
void FileTransferThread::finish_transfer()
{
	m_file_transfer.reset();
}
void FileTransferThread::stop_transfer()
{
	emit emit_cancel_transfer(false);
}
void FileTransferThread::cancel_transfer()
{
	emit emit_cancel_transfer(true);
}
void FileTransferThread::slot_file_connection_connected()
{
	emit file_connection_created();
}
void FileTransferThread::slot_file_connection_disconnected()
{
	emit file_connection_destroyed();
}
void FileTransferThread::slot_send_message_connection_transfer_start(const FileTransferDesc& file_transfer_desc)
{
	if (!m_message_connection) return;

	ClientMessageFileTransferStart msg(
		file_transfer_desc.message_id,
		file_transfer_desc.chat_id,
		file_transfer_desc.name,
		file_transfer_desc.is_dir,
		file_transfer_desc.size,
		file_transfer_desc.files
	);
	m_message_connection->send_message(msg);
}
void FileTransferThread::slot_send_message_connection_transfer_read_file(unsigned int file_index, quint64 size)
{
	if (m_message_connection)
		m_message_connection->send_message(ClientMessageFileTransferFileBodyStart(file_index, size));
}
void FileTransferThread::slot_send_message_connection_transfer_finish()
{
	if (m_message_connection)
		m_message_connection->send_message(ClientMessageFileTransferFinish());
}
void FileTransferThread::slot_send_message_connection_transfer_cancel(unsigned int file_index, quint64 transfer_bytes, bool user_cancel)
{
	if (m_message_connection)
		m_message_connection->send_message(ClientMessageFileTransferCancel(file_index, transfer_bytes, user_cancel));
}