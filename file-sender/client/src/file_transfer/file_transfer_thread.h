//=====================================================================================//
//   Author: open
//   Date:   07.07.2019
//=====================================================================================//
#pragma once
#include "file_transfer/file_connection.h"
#include "file_transfer/file_transfer.h"
#include <QThread>

class MessageConnection;

class FileTransferThread : public QObject
{
	Q_OBJECT
private:
	MessageConnection* m_message_connection;
	ConnectionId m_message_connection_id;
	std::unique_ptr<FileConnection> m_file_connection;
	std::unique_ptr<FileTransfer> m_file_transfer;
	QThread m_thread;
	
public:
	FileTransferThread(MessageConnection* message_connection, const QString& host_address);
	virtual ~FileTransferThread();

	void start_transfer(const FileTransferDesc& desc);
	void finish_transfer();
	void cancel_transfer();
	void stop_transfer();

signals:
	void file_connection_created();
	void file_connection_destroyed();

// internal:
	void emit_start_transfer();
	void emit_cancel_transfer(bool user_cancel);
	//void emit_stop_transfer();

private slots:
	void slot_file_connection_connected();
	void slot_file_connection_disconnected();

	void slot_send_message_connection_transfer_start(const FileTransferDesc& file_transfer_desc);
	void slot_send_message_connection_transfer_read_file(unsigned int file_index, quint64 size);
	void slot_send_message_connection_transfer_finish();
	void slot_send_message_connection_transfer_cancel(unsigned int file_index, quint64 transfer_bytes, bool user_cancel);

	void slot_thread_started(const QString& host_address);
};