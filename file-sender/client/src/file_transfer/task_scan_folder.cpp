//=====================================================================================//
//   Author: open
//   Date:   07.07.2019
//=====================================================================================//
#include "base/precomp.h"
#include "task_scan_folder.h"
#include <QFileInfo>
#include <QDir>

TaskScanFolder::TaskScanFolder(const QString& path)
: m_path(path)
, m_size(0)
, m_is_dir(false)
, m_cancel(false)
{
	QFileInfo path_info(m_path);
	m_is_dir = path_info.isDir();

	m_folder_path = m_is_dir ? m_path : path_info.path();
	m_name = path_info.fileName();
}
void TaskScanFolder::on_process()
{
	QFileInfo path_info(m_path);
	if (m_is_dir) scan_folder(m_folder_path, QString());
	else add_file(path_info.fileName(), path_info);
}
void TaskScanFolder::on_result()
{
	emit finished(m_files, m_path, m_folder_path, m_name, m_is_dir, m_size);
}

void TaskScanFolder::add_file(const QString& name, const QFileInfo& info)
{
	quint64 size = 0;
	if (info.isSymLink()) size = get_file_size(info.filePath());
	else size = static_cast<quint64>(info.size());
	m_files.emplace_back(name);
	m_size += size;
}
void TaskScanFolder::scan_folder(const QString& path, const QString& folder_path)
{
	QDir dir_folder(path);
	dir_folder.setFilter(QDir::Dirs | QDir::Hidden | QDir::NoSymLinks | QDir::NoDot | QDir::NoDotAndDotDot);
	QFileInfoList folders = dir_folder.entryInfoList();
	for (const QFileInfo& folder : folders)
	{
		if (m_cancel) return;
		QString current_folder_path = folder_path.isEmpty() ? folder.fileName() : folder_path + "/" + folder.fileName();
		scan_folder(folder.filePath(), current_folder_path);
	}

	if (m_cancel) return;

	QDir dir_files(path);
	dir_files.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
	QFileInfoList files = dir_files.entryInfoList();
	for (const QFileInfo& file_info : files)
	{
		if (m_cancel) return;
		add_file(folder_path.isEmpty() ? file_info.fileName() : (folder_path + "/" + file_info.fileName()), file_info);
	}
}
quint64 TaskScanFolder::get_file_size(const QString& path) const
{
	quint64 size = 0;
	QFile file(path);
	if (file.open(QIODevice::ReadOnly))
	{
		size = file.size();  // when file does open
		file.close();
	}
	return size;
}