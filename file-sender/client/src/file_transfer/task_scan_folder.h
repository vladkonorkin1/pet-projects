//=====================================================================================//
//   Author: open
//   Date:   07.07.2019
//=====================================================================================//
#pragma once
#include "thread_task.h"

class QFileInfo;

class TaskScanFolder : public ThreadTask
{
	Q_OBJECT
private:
	QString m_path;
	QString m_folder_path;
	QString m_name;
	std::vector<QString> m_files;
	quint64 m_size;
	bool m_is_dir;
	bool m_cancel;

public:
	TaskScanFolder(const QString& path);
	virtual void on_process();
	virtual void on_result();

	void cancel();
	bool is_dir() const;
	const QString& path() const { return m_path; }

signals:
	void finished(const std::vector<QString>& files, const QString& path,  const QString& folder_path, const QString& name, bool is_dir, quint64 size);

private:
	void add_file(const QString& name, const QFileInfo& info);
	void scan_folder(const QString& path, const QString& folder_path);
	quint64 get_file_size(const QString& path) const;
};

inline bool TaskScanFolder::is_dir() const
{
	return m_is_dir;
}
inline void TaskScanFolder::cancel()
{
	m_cancel = true;
}