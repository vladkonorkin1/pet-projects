#include "base/precomp.h"
#include "animate_expand_participants.h"
#include <QSplitter>
#include <QCheckBox>

float AnimateExpandParticipants::m_default_duration = 0.25f;

AnimateExpandParticipants::AnimateExpandParticipants(QSplitter* splitter_top, QCheckBox* checkbox, QWidget* contents, int default_expand_flow_content_height)
: m_splitter_top(splitter_top)
, m_checkbox(checkbox)
, m_contents(contents)
, m_default_expand_flow_content_height(default_expand_flow_content_height)
{
	update_splitter_pos(0);
	connect(m_splitter_top, SIGNAL(splitterMoved(int, int)), SLOT(slot_splitter_moved(int, int)));
	connect(m_checkbox, SIGNAL(stateChanged(int)), SLOT(slot_checkbox(int)));
}
void AnimateExpandParticipants::slot_splitter_moved(int pos, int index)
{
	Q_UNUSED(index);
	QSignalBlocker block(m_checkbox);
	m_checkbox->setChecked(pos > 0);
}
void AnimateExpandParticipants::slot_checkbox(int checked)
{
	if (checked > 0)
		run_timer(flow_content_height());
	else
		run_timer(0);
}
void AnimateExpandParticipants::run_timer(int target_value)
{
	m_duration = m_default_duration;

	m_timer.reset(new Base::Timer(1.f/60.f));
	connect(m_timer.get(), SIGNAL(updated(float)), SLOT(slot_animate_timer(float)));
	m_time = 0.f;
	m_start_value = current_value();
	m_end_value = target_value;
}

int clamp_pos(int pos, int start_value, int end_value)
{
	if (start_value > end_value)
		std::swap(start_value, end_value);

	return std::min<int>(std::max<int>(pos, start_value), end_value);
}
void AnimateExpandParticipants::slot_animate_timer(float dt)
{
	m_time += dt;
	float length = m_end_value - m_start_value;
	int pos = static_cast<int>(m_start_value + (m_time/ m_duration)*length);
	
	pos = clamp_pos(pos, m_start_value, m_end_value);

	//std::cout << " pos: " << pos << std::endl;

	if (m_time > m_duration)
		m_timer.reset();

	update_splitter_pos(pos);
}
int AnimateExpandParticipants::current_value() const
{
	auto sizes = m_splitter_top->sizes();
	if (!sizes.empty())
		return sizes[0];
	return 0;
}
void AnimateExpandParticipants::update_splitter_pos(int pos)
{
	QList<int> sizes;
	sizes.append(pos);
	int h = m_splitter_top->height();
	int handleWidth = m_splitter_top->handleWidth();
	sizes.append(h - pos - handleWidth);
	m_splitter_top->setSizes(sizes);
}
void AnimateExpandParticipants::grow_expand()
{
	if (m_checkbox->isChecked())
		QTimer::singleShot(100, this, SLOT(slot_grow_expand()));
}
void AnimateExpandParticipants::slot_grow_expand()
{
	if (current_value() < flow_content_height())
		run_timer(flow_content_height());
}
int AnimateExpandParticipants::flow_content_height() const
{
	return std::min(m_contents->size().height(), m_default_expand_flow_content_height) + 1;
}