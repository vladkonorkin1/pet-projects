#pragma once
#include "base/timer.h"

class QSplitter;
class QCheckBox;

class AnimateExpandParticipants : public QObject
{
	Q_OBJECT
private:
	QSplitter* m_splitter_top;
	QCheckBox* m_checkbox;
	QWidget* m_contents;

	std::unique_ptr<Base::Timer> m_timer;
	float m_time;

	float m_start_value;
	float m_end_value;

	float m_duration;
	static float m_default_duration;
	int m_default_expand_flow_content_height;

public:
	AnimateExpandParticipants(QSplitter* splitter_top, QCheckBox* checkbox, QWidget* contents, int default_expand_flow_content_height);
	void grow_expand();

private slots:
	void slot_splitter_moved(int pos, int index);
	void slot_animate_timer(float dt);
	void slot_checkbox(int checked);
	void slot_grow_expand();

private:
	void run_timer(int target_value);
	void update_splitter_pos(int pos);
	int current_value() const;
	int flow_content_height() const;
};