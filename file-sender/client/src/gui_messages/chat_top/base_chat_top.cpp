#include "base/precomp.h"
#include "base_chat_top.h"
#include <QAbstractButton>

BaseChatTop::BaseChatTop(QWidget* parent)
: QWidget(parent)
, m_add_users_button(nullptr)
{
}
void BaseChatTop::set_add_users_button(QAbstractButton* button)
{
	m_add_users_button = button;
}
void BaseChatTop::set_enable_add_users_button(bool enable)
{
	if(m_add_users_button) m_add_users_button->setEnabled(enable);
}
QPoint BaseChatTop::calc_modify_dialog_position(QAbstractButton* button) const {
	QSize size = button->size();
	return button->mapToGlobal(QPoint(size.width(), size.height()));
}
void BaseChatTop::connect_modify_chat_users_button(QAbstractButton* button)
{
	connect(button, &QAbstractButton::clicked, [this, button](){ emit need_modify_chat_users(calc_modify_dialog_position(button)); });
}