#pragma once
#include <QWidget>

class QAbstractButton;
class Chat;

class BaseChatTop : public QWidget
{
	Q_OBJECT
private:
	QAbstractButton* m_add_users_button;

public:
	BaseChatTop(QWidget* parent);
	virtual void add_messages_widget(QWidget* widget) = 0;
	virtual void process_changed_users() {}
	virtual void update_name() = 0;
	void set_enable_add_users_button(bool hide_or_show);

signals:
	void need_modify_chat_users(const QPoint& position);

protected:
	void connect_modify_chat_users_button(QAbstractButton* button);
	void set_add_users_button(QAbstractButton* button);

private:
	QPoint calc_modify_dialog_position(QAbstractButton* button) const;
};