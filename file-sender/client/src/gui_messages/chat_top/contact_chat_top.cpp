#include "base/precomp.h"
#include "contact_chat_top.h"
#include "chat.h"

ContactChatTop::ContactChatTop(QWidget* parent, const Chat* chat)
: BaseChatTop(parent)
, m_ui(new Ui::ContactChatTop())
, m_chat(chat)
{
	m_ui->setupUi(this);
	update_name();
	update_Department();
	connect_modify_chat_users_button(m_ui->button_create_group_chat);
}
void ContactChatTop::add_messages_widget(QWidget* widget)
{
	widget->setParent(m_ui->messages_container);
	m_ui->messages_layout->addWidget(widget);
}
void ContactChatTop::update_name()
{
	m_ui->label_name->setText(m_chat->name());
}
void ContactChatTop::update_Department()
{
	m_ui->label_Department->setText(m_chat->contact()->department->name);
}