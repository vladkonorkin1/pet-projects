#pragma once
#include "ui_contact_chat_top.h"
#include "gui_messages/chat_top/base_chat_top.h"

namespace Ui { class ContactChatTop; }

class ContactChatTop : public BaseChatTop
{
	Q_OBJECT
private:
	std::auto_ptr<Ui::ContactChatTop> m_ui;
	const Chat* m_chat;

public:
	ContactChatTop(QWidget* parent, const Chat* chat);
	virtual void add_messages_widget(QWidget* widget);
	virtual void update_name();

	void update_Department();
};