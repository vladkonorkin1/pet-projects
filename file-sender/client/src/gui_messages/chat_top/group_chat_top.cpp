#include "base/precomp.h"
#include "group_chat_top.h"
#include "modify_chat_users/chat_user_flow_item.h"
#include "group_chat_top_user_context_menu.h"
//#include "chat.h"
#include <QScrollBar>

GroupChatTop::GroupChatTop(QWidget* parent, const Chat* chat, GroupChatTopUserContextMenu* user_context_menu)
: BaseChatTop(parent)
, m_ui(create_ui())
, m_chat(chat)
, m_flow_layout(m_ui->flow_content_widget, 6, 8, 6)
, m_animate_expand_participants(m_ui->splitter_top, m_ui->checkbox, m_ui->flow_content_widget, calc_expand_flow_content_height(4))
, m_user_context_menu(user_context_menu)
{
	m_ui->scroll_area_participants->verticalScrollBar()->setSingleStep(9);
	update_name();
	update_users();
	set_add_users_button(m_ui->button_group_chat_add);
	connect_modify_chat_users_button(m_ui->button_group_chat_add);
}
GroupChatTop::UGroupChatTop GroupChatTop::create_ui()
{
	UGroupChatTop ui(new Ui::GroupChatTop());
	ui->setupUi(this);
	return ui;
}
void GroupChatTop::set_enabled_add_users_button(bool hide_or_show)
{
	m_ui->button_group_chat_add->setEnabled(hide_or_show);
}
void GroupChatTop::add_messages_widget(QWidget* widget)
{
	widget->setParent(m_ui->messages_container);
	m_ui->messages_layout->addWidget(widget);
}
void GroupChatTop::update_name()
{
	m_ui->label_name->setText(m_chat->name());
}
void GroupChatTop::update_users()
{
	m_users.clear();
	for (const ChatUser* chat_user : get_sorted_chat_users())
		create_chat_user_flow_item(chat_user->user, chat_user->admin);
	m_ui->checkbox->setText(tr("Participants: %1").arg(m_chat->users().size()));
	m_animate_expand_participants.grow_expand();
}
// �������� ��������������� ���������� ����
std::vector<ChatUser*> GroupChatTop::get_sorted_chat_users() const
{
	const std::map<UserId, ChatUser*>& users = m_chat->users();
	std::vector<ChatUser*> sorted_users;
	// �������� ������������� �� ��� � ������
	std::transform(users.begin(), users.end(), std::back_inserter(sorted_users), [](const std::pair<UserId, ChatUser*>& user) { return user.second; });
	// ��������� ������������� ������ �������
	std::sort(sorted_users.begin(), sorted_users.end(), [](ChatUser* left_user, ChatUser* right_user)
	{
		if (left_user->admin != right_user->admin)
			return left_user->admin;	
		return left_user->user->name < right_user->user->name;
	});
	return sorted_users;
}
ChatUserFlowItem* GroupChatTop::create_chat_user_flow_item(const User* contact, bool admin)
{
	UChatUserFlowItem item(new ChatUserFlowItem(m_ui->flow_content_widget, contact, admin));
	ChatUserFlowItem* pitem = item.get();
	connect(pitem, &ChatUserFlowItem::exec_menu, [this, pitem](const QPoint& pos) { slot_exec_menu_contact_flow_item(pitem, pos); });
	connect(pitem, &ChatUserFlowItem::itemDoubleClicked, [this, pitem] {slot_contact_add_to_chat(pitem); });
	m_flow_layout.addWidget(pitem);
	m_users.push_back(std::move(item));
	return pitem;
}
void GroupChatTop::process_changed_users()
{
	update_users();
}
void GroupChatTop::slot_exec_menu_contact_flow_item(ChatUserFlowItem* item, const QPoint& pos)
{
	m_user_context_menu->exec(m_chat, item->contact(), pos);
}
void GroupChatTop::slot_contact_add_to_chat(ChatUserFlowItem* item)
{
	emit add_contact_to_chat_list(item->contact());
}
int GroupChatTop::calc_expand_flow_content_height(int num_rows) const
{
	return num_rows* ChatUserFlowItem::height() + 2*m_flow_layout.margin() + (num_rows-1)*m_flow_layout.verticalSpacing();
}