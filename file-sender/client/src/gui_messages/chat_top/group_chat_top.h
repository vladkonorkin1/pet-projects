#pragma once
#include "ui_group_chat_top.h"
#include "gui_messages/chat_top/base_chat_top.h"
#include "gui_messages/chat_top/animate_expand_participants.h"
#include "common_gui/flow_layout.h"
#include "chat.h"
namespace Ui { class GroupChatTop; }
class GroupChatTopUserContextMenu;
class ChatUserFlowItem;
class User;

class GroupChatTop : public BaseChatTop
{
	Q_OBJECT
private:
	using UGroupChatTop = std::auto_ptr<Ui::GroupChatTop>;
	UGroupChatTop m_ui;
	const Chat* m_chat;
	FlowLayout m_flow_layout;
	using UChatUserFlowItem = std::unique_ptr<ChatUserFlowItem>;
	using users_t = std::vector<UChatUserFlowItem>;
	users_t m_users;
	AnimateExpandParticipants m_animate_expand_participants;
	GroupChatTopUserContextMenu* m_user_context_menu;

public:
	GroupChatTop(QWidget* parent, const Chat* chat, GroupChatTopUserContextMenu* user_context_menu);
	virtual void add_messages_widget(QWidget* widget);
	virtual void process_changed_users();
	virtual void update_name();
	void set_enabled_add_users_button(bool hide_or_show);

signals:
	void add_contact_to_chat_list(const User* contact);

private:
	void slot_exec_menu_contact_flow_item(ChatUserFlowItem* item, const QPoint& pos);
	void slot_contact_add_to_chat(ChatUserFlowItem* item);

private:
	UGroupChatTop create_ui();
	void update_users();
	ChatUserFlowItem* create_chat_user_flow_item(const User* user, bool admin);
	int calc_expand_flow_content_height(int num_rows) const;
	// получить отсортированных участников чата
	std::vector<ChatUser*> get_sorted_chat_users() const;
};