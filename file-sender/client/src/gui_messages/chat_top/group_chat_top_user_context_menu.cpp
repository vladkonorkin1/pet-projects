//=====================================================================================//
//   Author: open
//   Date:   22.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "group_chat_top_user_context_menu.h"
#include "chats.h"

GroupChatTopUserContextMenu::GroupChatTopUserContextMenu(QWidget* parent, const Chats* chats)
: m_menu(parent)
, m_chat(nullptr)
, m_user(nullptr)
, m_chats(chats)
{
	m_action_add_contact_to_chat_list = new QAction(tr("Add to contacts"), this);
	connect(m_action_add_contact_to_chat_list, SIGNAL(triggered()), SLOT(slot_add_contact_to_chat_list()));
	m_menu.addAction(m_action_add_contact_to_chat_list);

	m_action_remove_user = new QAction(tr("Remove from this group"), this);
	connect(m_action_remove_user, SIGNAL(triggered()), SLOT(slot_remove_user()));
	m_menu.addAction(m_action_remove_user);

	m_action_set_admin = new QAction(tr("Set as Administrator"), this);
	connect(m_action_set_admin, SIGNAL(triggered()), SLOT(slot_set_admin()));
	m_menu.addAction(m_action_set_admin);

	m_action_remove_admin = new QAction(tr("Remove from Admininstartors"), this);
	connect(m_action_remove_admin, SIGNAL(triggered()), SLOT(slot_remove_admin()));
	m_menu.addAction(m_action_remove_admin);
}
void GroupChatTopUserContextMenu::exec(const Chat* chat, const User* user, const QPoint& pos)
{
	assert( !chat->contact() );

	m_chat = chat;
	m_user = user;

	m_action_add_contact_to_chat_list->setVisible(!is_user_in_chat_list(user));
	m_action_remove_user->setVisible(chat->is_participant() && chat->is_admin());//chat->is_user_admin(contact));
	m_action_set_admin->setVisible(chat->is_admin() && !chat->is_user_admin(user));
	m_action_remove_admin->setVisible(chat->is_admin() && chat->is_user_admin(user));

	m_menu.popup(pos);
}
void GroupChatTopUserContextMenu::slot_add_contact_to_chat_list()
{
	if (m_chat->is_enabled()) emit add_contact_to_chat_list(m_user);
}
void GroupChatTopUserContextMenu::slot_remove_user()
{
	if (m_chat->is_enabled()) emit chat_remove_user(m_chat, m_user);
}
bool GroupChatTopUserContextMenu::is_user_in_chat_list(const User* user) const
{
	if (const Chat* chat = m_chats->find_chat_by_contact(user->id))
		return chat->is_enabled();
	return false;
}
void GroupChatTopUserContextMenu::slot_set_admin()
{
	if (m_chat->is_enabled()) emit set_chat_admin(m_chat, m_user);
}
void GroupChatTopUserContextMenu::slot_remove_admin()
{
	if (m_chat->is_enabled()) emit remove_chat_admin(m_chat, m_user);
}
