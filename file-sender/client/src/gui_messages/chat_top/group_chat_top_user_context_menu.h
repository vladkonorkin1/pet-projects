//=====================================================================================//
//   Author: open
//   Date:   22.08.2017
//=====================================================================================//
#pragma once
#include <QMenu>

class User;
class Chats;
class Chat;

class GroupChatTopUserContextMenu : public QObject
{
	Q_OBJECT
private:
	QMenu m_menu;
	QAction* m_action_add_contact_to_chat_list;
	QAction* m_action_remove_user;
	QAction* m_action_set_admin;
	QAction* m_action_remove_admin;

	const Chat* m_chat;
	const User* m_user;
	const Chats* m_chats;

public:
	GroupChatTopUserContextMenu(QWidget* parent, const Chats* chats);
	void exec(const Chat* chat, const User* user, const QPoint& pos);

signals:
	void add_contact_to_chat_list(const User* contact);
	void chat_remove_user(const Chat* chat, const User* contact);
	void set_chat_admin(const Chat* chat, const User* contact);
	void remove_chat_admin(const Chat* chat, const User* contact);

private slots:
	void slot_add_contact_to_chat_list();
	void slot_remove_user();
	void slot_set_admin();
	void slot_remove_admin();

private:
	bool is_user_in_chat_list(const User* user) const;
};