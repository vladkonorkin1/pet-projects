//=====================================================================================//
//   Author: open
//   Date:   08.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "hover_button.h"
#include <QDebug>

HoverButton::HoverButton(QWidget* parent)
: QPushButton(parent)
{
}
void HoverButton::enterEvent(QEvent* event)
{
	//qDebug() << "HoverButton: enter event" << this;
	emit hover_in();
	QWidget::enterEvent(event);
}

void HoverButton::leaveEvent(QEvent* event)
{
	//qDebug() << "HoverButton: leave event" << this;
	emit hover_out();
	QWidget::leaveEvent(event);
}