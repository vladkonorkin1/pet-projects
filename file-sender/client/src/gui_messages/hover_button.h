//=====================================================================================//
//   Author: open
//   Date:   08.08.2017
//=====================================================================================//
#pragma once
#include <QPushButton>

class HoverButton : public QPushButton
{
	Q_OBJECT
private:

public:
	HoverButton(QWidget* parent);

signals:
	void hover_in();
	void hover_out();

protected:
	void enterEvent(QEvent* event);
	void leaveEvent(QEvent* event);
};