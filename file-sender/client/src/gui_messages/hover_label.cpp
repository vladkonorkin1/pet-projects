//=====================================================================================//
//   Author: open
//   Date:   08.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "hover_label.h"
#include <QDebug>

HoverLabel::HoverLabel(QWidget* parent)
: QLabel(parent)
{
}
void HoverLabel::enterEvent(QEvent* event)
{
	//qDebug() << "HoverLabel: enter event" << this;
	emit hover_in();
	QWidget::enterEvent(event);
}

void HoverLabel::leaveEvent(QEvent* event)
{
	//qDebug() << "HoverLabel: leave event" << this;
	emit hover_out();
	QWidget::leaveEvent(event);
}