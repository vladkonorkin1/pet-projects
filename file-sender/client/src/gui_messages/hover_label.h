//=====================================================================================//
//   Author: open
//   Date:   08.08.2017
//=====================================================================================//
#pragma once
#include <QLabel>

class HoverLabel : public QLabel
{
	Q_OBJECT
private:

public:
	HoverLabel(QWidget* parent);

signals:
	void hover_in();
	void hover_out();

protected:
	void enterEvent(QEvent* event);
	void leaveEvent(QEvent* event);
};