//=====================================================================================//
//   Author: open
//   Date:   16.05.2017
//=====================================================================================//
#include "base/precomp.h"
#include "item_file_message.h"
#include "chat_message.h"
#include "base/utility.h"
#include <QAbstractButton>
#include <QLabel>
#include <QProgressBar>
#include <QProcess>
#include "user.h"
#include "chat.h"
#include <QDir>
#include <array>

ItemFileMessage::ItemFileMessage(QWidget* parent, const ChatMessage* chat_message)
: ItemMessage(parent, chat_message)
{
}
void ItemFileMessage::setup_file_ui(QLabel* label_icon, QAbstractButton* button_open_file, QAbstractButton* button_open_folder, QLabel* label_sender_name)
{
	m_buttons.reset(new ItemFileMessageButtons(label_icon, button_open_file, button_open_folder));
	if (label_sender_name)
	{
		if (chat_message()->chat->contact()) label_sender_name->setVisible(false);
		else label_sender_name->setText(chat_message()->sender->name);
	}
	connect(button_open_file, SIGNAL(clicked()), SLOT(slot_button_open_file_clicked()));
	connect(button_open_folder, SIGNAL(clicked()), SLOT(slot_button_open_folder_clicked()));
	//set_progress(0);
}
quint64 ItemFileMessage::file_size() const
{
	return chat_message()->file_size;
}
static quint64 s_1gb = 1024 * 1024 * 1024;
static quint64 s_10gb = s_1gb * 10;
QString ItemFileMessage::format_size() const
{
	if (s_1gb < file_size() && file_size() < s_10gb)
		return QString().sprintf("%.1f ", convert_unit()) + format_unit();
	return QString("%1 %2").arg(static_cast<quint64>(convert_unit())).arg(format_unit());
}
double ItemFileMessage::convert_unit() const
{
	quint64 size = file_size();
	if (size > s_1gb) return (static_cast<double>(size) / (1073741824.));
	else if (size > 1024 * 1024) return (static_cast<double>(size) / (1048576.));
	else if (size > 1024) return (static_cast<double>(size) / (1024.));
	else return size;
}
QString ItemFileMessage::format_unit() const
{
	quint64 size = file_size();
	if (size > s_1gb) return tr("GB");
	else if (size > 1024 * 1024) return tr("MB");
	else if (size > 1024) return tr("KB");
	else return tr("B");
}
QString ItemFileMessage::trim_name(const QString& name) const
{
	const int max_chars = 68;
	if (name.length() > max_chars) return name.left(max_chars).remove(QRegExp("[ \t]+$")).append("...");
	return name;
}
const QString& ItemFileMessage::fullpath() const
{
	const ChatMessage* msg = chat_message();
	return msg->myself ? msg->file_local_path : msg->file_server_path;
}
#include <QMessageBox>
void ItemFileMessage::open_folder()
{
	QString path = QDir::toNativeSeparators(fullpath());

	// ���� ���� �����������
	if (!QFile::exists(path))
	{
		QMessageBox::information(this, tr("Warning"), tr("Could not to open a folder:\n%1").arg(path));
		return;
	}
	QProcess::startDetached(QString("explorer.exe /select, \"%1\"").arg(path));
}
void ItemFileMessage::open_url()
{
	QString path = QDir::toNativeSeparators(fullpath());

	if (chat_message()->file_is_dir)
	{
		// ���� ����� �����������
		if (!QDir(path).exists())
		{
			QMessageBox::information(this, tr("Warning"), tr("Could not to open a folder:\n%1").arg(path));
			return;
		}
	}
	else
	{
		// ���� ���� �����������
		if (!QFile::exists(path))
		{
			QMessageBox::information(this, tr("Warning"), tr("Could not to open a file:\n%1").arg(path));
			return;
		}
	}

	QProcess::startDetached(QString("explorer.exe \"%1\"").arg(path));
}
void ItemFileMessage::slot_button_open_file_clicked()
{
	open_url();
}
void ItemFileMessage::slot_button_open_folder_clicked()
{
	open_folder();
}
void ItemFileMessage::update_file_data(QLabel* label_name, QLabel* label_size, QLabel* label_status, QLabel* label_icon, QProgressBar* progress_bar)
{
	const ChatMessage* msg = chat_message();

	label_name->setText(trim_name(Utility::filename_from_path(fullpath())));
	label_size->setText(format_size());
	
	bool status_default = msg->file_status == FileTransferResult::Default;
	bool status_ok = msg->file_status <= FileTransferResult::Success;

	progress_bar->setVisible(status_default);
	
	static QPixmap pixmap_file(":/file_transfer_file.png");
	static QPixmap pixmap_folder(":/file_transfer_folder.png");
	static QPixmap pixmap_error(":/file_transfer_cancel.png");
	label_icon->setPixmap(status_ok ? (msg->file_is_dir ? pixmap_folder : pixmap_file) : pixmap_error);

	if (status_ok)
	{
		label_icon->setStyleSheet("");
		label_name->setStyleSheet("");
		label_size->setStyleSheet("color: #696969;");
	}
	else
	{
		label_icon->setStyleSheet("background-color: #8d8d9a; border-radius: 5px;");
		label_name->setStyleSheet("color: #696969;");
		label_size->setStyleSheet("color: #696969;");
	}

	static std::array<QString, size_t(FileTransferResult::Max)> status_texts =
	{
		//msg->myself ? QString() : tr("transferring..."),
		QString(),
		QString(),
		tr("read error"),
		tr("write error"),
		tr("transfer error"),
		tr("cancelled")
	};
	const QString& status_text = status_texts[static_cast<size_t>(msg->file_status)];
	label_status->setText(status_text);
	//label_status->setVisible(!status_text.isEmpty());
}