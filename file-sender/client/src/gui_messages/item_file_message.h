//=====================================================================================//
//   Author: open
//   Date:   16.05.2017
//=====================================================================================//
#pragma once
#include "gui_messages/item_message.h"
#include "item_file_message_buttons.h"

class QAbstractButton;
class QProgressBar;
class QLabel;

class ItemFileMessage : public ItemMessage
{
	Q_OBJECT
private:
	std::unique_ptr<ItemFileMessageButtons> m_buttons;

public:
	ItemFileMessage(QWidget* parent, const ChatMessage* chat_message);
	virtual void set_progress(unsigned char progress) = 0;

protected:
	void setup_file_ui(QLabel* label_icon, QAbstractButton* button_open_file, QAbstractButton* button_open_folder, QLabel* label_sender_name);
	void update_file_data(QLabel* label_name, QLabel* label_size, QLabel* label_status, QLabel* label_icon, QProgressBar* progress_bar);
	QString format_size() const;
	double convert_unit() const;
	QString format_unit() const;
	void open_folder();
	void open_url();
	const QString& fullpath() const;

private slots:
	void slot_button_open_file_clicked();
	void slot_button_open_folder_clicked();

private:
	quint64 file_size() const;
	QString trim_name(const QString& name) const;
};