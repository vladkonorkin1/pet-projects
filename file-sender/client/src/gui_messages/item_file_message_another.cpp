//=====================================================================================//
//   Author: open
//   Date:   05.05.2017
//=====================================================================================//
#include "base/precomp.h"
#include "item_file_message_another.h"
#include "item_file_message_buttons.h"
#include "chat_message.h"
#include "chat.h"
#include "user.h"

ItemFileMessageAnother::ItemFileMessageAnother(QWidget* parent, const ChatMessage* chat_message)
: ItemFileMessage(parent, chat_message)
, m_ui(new Ui::ItemFileMessageAnother())
{
	m_ui->setupUi(this);
	setup_file_ui(m_ui->label_icon, m_ui->button_open_file, m_ui->button_open_folder, m_ui->label_user_name);
}
void ItemFileMessageAnother::set_progress(unsigned char progress)
{
	m_ui->progress_bar->setValue(progress);
}
void ItemFileMessageAnother::update()
{
	update_datetime(m_ui->label_time, m_ui->label_date);
	update_file_data(m_ui->label_name, m_ui->label_size, m_ui->label_status, m_ui->label_icon, m_ui->progress_bar);
}