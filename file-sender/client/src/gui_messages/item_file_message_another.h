//=====================================================================================//
//   Author: open
//   Date:  05.05.2017
//=====================================================================================//
#pragma once
#include "ui_item_file_message_another.h"
#include "gui_messages/item_file_message.h"

class User;

class ItemFileMessageAnother : public ItemFileMessage
{
	Q_OBJECT
private:
	std::auto_ptr<Ui::ItemFileMessageAnother> m_ui;

public:
	ItemFileMessageAnother(QWidget* parent, const ChatMessage* chat_message);
	virtual void set_progress(unsigned char progress);
	virtual void update();

private slots:
};