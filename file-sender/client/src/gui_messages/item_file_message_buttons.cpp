//=====================================================================================//
//   Author: open
//   Date:   08.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "item_file_message_buttons.h"
#include <QLabel>
#include <QAbstractButton>
//#include "hover_button.h"
//#include "hover_label.h"

ItemFileMessageButtons::ItemFileMessageButtons(QLabel* label, QAbstractButton* button_open_file, QAbstractButton* button_open_folder)
: m_label(label)
, m_button_open_file(button_open_file)
, m_button_open_folder(button_open_folder)
, m_registered(false)
, m_visible(false)
, m_enabled(true)
{
	connect(m_label, SIGNAL(hover_in()), SLOT(slot_hover_in()), Qt::ConnectionType::AutoConnection);
	connect(m_button_open_file, SIGNAL(hover_in()), SLOT(slot_hover_in()), Qt::ConnectionType::AutoConnection);
	connect(m_button_open_folder, SIGNAL(hover_in()), SLOT(slot_hover_in()), Qt::ConnectionType::AutoConnection);
	connect(m_label, SIGNAL(hover_out()), SLOT(slot_hover_out()), Qt::ConnectionType::AutoConnection);
	connect(m_button_open_file, SIGNAL(hover_out()), SLOT(slot_hover_out()), Qt::ConnectionType::AutoConnection);
	connect(m_button_open_folder, SIGNAL(hover_out()), SLOT(slot_hover_out()), Qt::ConnectionType::AutoConnection);
	connect(this, SIGNAL(need_change_visible()), SLOT(slot_need_change_visible()), Qt::ConnectionType::QueuedConnection);
	set_visible_buttons(false);
}
void ItemFileMessageButtons::slot_hover_in()
{
	//set_visible_buttons(true);
	//std::cout << "slot_hover_in " << std::endl;
	if (m_enabled) register_change_visible(true);
}
void ItemFileMessageButtons::slot_hover_out()
{
	//std::cout << "slot_hover_out " << std::endl;
	//set_visible_buttons(false);
	if (m_enabled) register_change_visible(false);
}
void ItemFileMessageButtons::register_change_visible(bool visible)
{
	//std::cout << "register_change_visible 1 " << visible << std::endl;
	m_visible = visible;
	if (!m_registered)
	{
		//std::cout << "register_change_visible 2 " << visible << std::endl;
		m_registered = true;
		emit need_change_visible();
	}
}
void ItemFileMessageButtons::slot_need_change_visible()
{
	//std::cout << "slot_need_change_visible " << m_visible << std::endl;
	m_registered = false;
	set_visible_buttons(m_visible);
}
void ItemFileMessageButtons::set_visible_buttons(bool visible)
{
	m_button_open_folder->setVisible(visible);
	m_button_open_file->setVisible(visible);
	

	//m_button_open_file->setEnabled(visible);
	//m_button_open_folder->setEnabled(visible);
}
void ItemFileMessageButtons::set_enabled(bool enable)
{
	m_enabled = enable;
}
void ItemFileMessageButtons::set_visible(bool visible)
{
	set_visible_buttons(visible);
}