//=====================================================================================//
//   Author: open
//   Date:   08.08.2017
//=====================================================================================//
#pragma once
#include <QObject>

class QAbstractButton;
class QLabel;

class ItemFileMessageButtons : public QObject
{
	Q_OBJECT
private:
	QLabel* m_label;
	QAbstractButton* m_button_open_file;
	QAbstractButton* m_button_open_folder;
	bool m_registered;
	bool m_visible;
	bool m_enabled;

public:
	ItemFileMessageButtons(QLabel* label, QAbstractButton* button_open_file, QAbstractButton* button_open_folder);
	void set_enabled(bool enable);
	void set_visible(bool visible);

signals:
	void need_change_visible();

private slots:
	void slot_hover_in();
	void slot_hover_out();
	void slot_need_change_visible();

private:
	void set_visible_buttons(bool visible);
	void register_change_visible(bool visible);
};