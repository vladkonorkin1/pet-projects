//=====================================================================================//
//   Author: open
//   Date:   16.05.2017
//=====================================================================================//
#include "base/precomp.h"
#include "item_file_message_myself.h"
#include "item_file_message_buttons.h"
#include "chat_message.h"

ItemFileMessageMyself::ItemFileMessageMyself(QWidget* parent, const ChatMessage* chat_message)
: ItemFileMessage(parent, chat_message)
, m_ui(new Ui::ItemFileMessageMyself())
{
	m_ui->setupUi(this);
	setup_file_ui(m_ui->label_icon, m_ui->button_open_file, m_ui->button_open_folder, nullptr);
	connect(m_ui->label_cancel, SIGNAL(linkActivated(const QString&)), SLOT(slot_button_cancel_clicked()));
}
void ItemFileMessageMyself::set_progress(unsigned char progress)
{
	m_ui->progress_bar->setValue(progress);
}
void ItemFileMessageMyself::update()
{
	update_datetime(m_ui->label_time, m_ui->label_date);
	update_file_data(m_ui->label_name, m_ui->label_size, m_ui->label_status, m_ui->label_icon, m_ui->progress_bar);
	m_ui->label_cancel->setVisible(chat_message()->file_status == FileTransferResult::Default);
}
void ItemFileMessageMyself::slot_button_cancel_clicked()
{
	emit need_cancel(chat_message()->local_id);
}