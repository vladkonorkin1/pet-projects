//=====================================================================================//
//   Author: open
//   Date:   16.05.2017
//=====================================================================================//
#pragma once
#include "ui_item_file_message_myself.h"
#include "gui_messages/item_file_message.h"

class ItemFileMessageButtons;

class ItemFileMessageMyself : public ItemFileMessage
{
	Q_OBJECT
private:
	std::auto_ptr<Ui::ItemFileMessageMyself> m_ui;

public:
	ItemFileMessageMyself(QWidget* parent, const ChatMessage* chat_message);
	virtual void set_progress(unsigned char progress);
	virtual void update();

signals:
	void need_cancel(const ChatMessageLocalId& local_id);

private slots:
	void slot_button_cancel_clicked();
};