//=====================================================================================//
//   Author: open
//   Date:   27.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "item_message.h"
#include "chat_message.h"
#include <QLabel>

int ItemMessage::m_max_append_chars = 3800;

ItemMessage::ItemMessage(QWidget* parent, const ChatMessage* chat_message)
: QWidget(parent)
, m_chat_message(chat_message)
{
}
QString ItemMessage::format_time(const QDateTime& datetime) const
{
	return datetime.toLocalTime().time().toString("h:mm");
}
QString ItemMessage::format_date(const QDateTime& datetime) const
{
	return datetime.toLocalTime().date().toString("dd.MM.yyyy");
}
ChatMessageId ItemMessage::id() const
{
	return m_chat_message->id;
}
const User* ItemMessage::sender() const
{
	return m_chat_message->sender;
}
void ItemMessage::update_datetime(QLabel* label_time, QLabel* label_date)
{
	label_time->setText(format_time(m_chat_message->datetime));
	label_date->setText(format_date(m_chat_message->datetime));
}