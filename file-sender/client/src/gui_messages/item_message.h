//=====================================================================================//
//   Author: open
//   Date:   27.04.2017
//=====================================================================================//
#pragma once
#include <QWidget>
#include <QDateTime>


class ChatMessage;
class QLabel;
class User;

class ItemMessage : public QWidget
{
	Q_OBJECT
private:
	const ChatMessage* m_chat_message;

protected:
	static int m_max_append_chars;

public:
	ItemMessage(QWidget* parent, const ChatMessage* chat_message);
	// id-сообщения
	ChatMessageId id() const;
	// получить данные сообщения
	const ChatMessage* chat_message() const { return m_chat_message; }
	// отправитель сообщения (владелец)
	const User* sender() const;
	// обновить отображение
	virtual void update() = 0;

protected:
	void update_datetime(QLabel* label_time, QLabel* label_date);

	QString format_time(const QDateTime& datetime) const;
	QString format_date(const QDateTime& datetime) const;
};