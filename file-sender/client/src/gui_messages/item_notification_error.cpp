//=====================================================================================//
//   Author: open
//   Date:   10.07.2017
//=====================================================================================//
#include "base/precomp.h"
#include "item_notification_error.h"
#include <QLabel>

ItemNotificationError::ItemNotificationError(QWidget* parent)
: QWidget(parent)
, m_ui(new Ui::ItemNotificationError())
, m_label(nullptr)
, m_num_push_text(0)
{
	m_ui->setupUi(this);
}
void ItemNotificationError::add_text(const QString& notification)
{
	if (m_label == nullptr || m_num_push_text > 30) {
		m_label = new QLabel(this);
		QFont font;
		font.setPointSize(10);
		m_label->setFont(font);
		//m_label->setWordWrap(true);
		//m_label->setTextInteractionFlags(Qt::LinksAccessibleByMouse | Qt::TextSelectableByMouse);
		m_ui->verticalLayout->addWidget(m_label);
	}
	++m_num_push_text;

	QString text = m_label->text();
	m_label->setText(text.isEmpty() ? notification : text + '\n' + notification);
}