//=====================================================================================//
//   Author: open
//   Date:   10.07.2017
//=====================================================================================//
#pragma once
#include "ui_item_notification_error.h"

class QLabel;

class ItemNotificationError : public QWidget
{
	Q_OBJECT
private:
	std::auto_ptr<Ui::ItemNotificationError> m_ui;
	QLabel* m_label;
	int m_num_push_text;
	
public:
	ItemNotificationError(QWidget* parent);
	void add_text(const QString& text);
};