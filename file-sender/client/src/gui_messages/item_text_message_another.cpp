//=====================================================================================//
//   Author: open
//   Date:   27.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "item_text_message_another.h"
#include "user.h"
#include "chat_message.h"
#include "chat.h"

ItemTextMessageAnother::ItemTextMessageAnother(QWidget* parent, const ChatMessage* msg)
: ItemMessage(parent, msg)
, m_ui(new Ui::ItemSendMessageAnother())
{
	m_ui->setupUi(this);
	show_contact_name(msg->sender, !msg->chat->contact());
}
void ItemTextMessageAnother::update()
{
	update_datetime(m_ui->label_time, m_ui->label_date);
	m_ui->label_message->setText(chat_message()->text);
}
void ItemTextMessageAnother::show_contact_name(const User* contact, bool is_group)
{
	if (is_group) m_ui->label_name->setText(contact->name);
	else m_ui->label_name->setVisible(false);
}