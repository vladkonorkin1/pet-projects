//=====================================================================================//
//   Author: open
//   Date:   27.04.2017
//=====================================================================================//
#pragma once
#include "ui_item_text_message_another.h"
#include "gui_messages/item_message.h"

class ItemTextMessageAnother : public ItemMessage
{
	Q_OBJECT
private:
	std::auto_ptr<Ui::ItemSendMessageAnother> m_ui;
	
public:
	ItemTextMessageAnother(QWidget* parent, const ChatMessage* msg);
	virtual void update();

private:
	void show_contact_name(const User* contact, bool is_group);
};