//=====================================================================================//
//   Author: open
//   Date:   27.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include <QStyle>
#include "item_text_message_myself.h"
#include "chat_message.h"


ItemTextMessageMyself::ItemTextMessageMyself(QWidget* parent, const ChatMessage* msg)
: ItemMessage(parent, msg)
, m_ui(new Ui::ItemSendMessageMyself())
{
	m_ui->setupUi(this);
}

void ItemTextMessageMyself::update()
{
	update_datetime(m_ui->label_time, m_ui->label_date);
	m_ui->label_message->setText(chat_message()->text);
	m_ui->label_message->setProperty("sending", chat_message()->status == ChatMessageStatus::Sending);
	m_ui->label_message->style()->unpolish(m_ui->label_message);
	m_ui->label_message->style()->polish(m_ui->label_message);
}

bool ItemTextMessageMyself::append_text(const QString& text, bool to_begin)
{
	if ((m_ui->label_message->text().size() + text.size()) >= m_max_append_chars)
		return false;
	if (to_begin)
		set_text(text + "\n" + m_ui->label_message->text());
	else
		set_text(m_ui->label_message->text() + "\n" + text);
	return true;
}

void ItemTextMessageMyself::set_datetime(const QDateTime& datetime)
{
	m_ui->label_time->setText(format_time(datetime));
	m_ui->label_date->setText(format_date(datetime));
	//m_ui->label_date->setText("");
}

void ItemTextMessageMyself::set_text(const QString& text)
{
	m_ui->label_message->setText(text);
}



