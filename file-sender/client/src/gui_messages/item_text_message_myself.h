//=====================================================================================//
//   Author: open
//   Date:   27.04.2017
//=====================================================================================//
#pragma once
#include "ui_item_text_message_myself.h"
#include "gui_messages/item_message.h"

class ItemTextMessageMyself : public ItemMessage
{
	Q_OBJECT
private:
	std::auto_ptr<Ui::ItemSendMessageMyself> m_ui;
	
public:
	ItemTextMessageMyself(QWidget* parent, const ChatMessage* msg);
	virtual void update();
	bool append_text(const QString& text, bool to_begin);
	void set_datetime(const QDateTime& datetime);
private:
	void set_text(const QString& text);
};