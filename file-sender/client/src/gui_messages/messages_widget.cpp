//=====================================================================================//
//   Author: open
//   Date:   27.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "messages_widget.h"
#include "item_text_message_myself.h"
#include "item_text_message_another.h"
#include "item_file_message_myself.h"
#include "item_file_message_another.h"
#include "file_transfer/file_transfer.h"
#include <QVBoxLayout>
#include <QScrollBar>
#include "chat.h"

MessagesWidget::MessagesWidget(QWidget* parent, const Chat* chat)
: QWidget(parent)
, m_chat(chat)
, m_ui(new Ui::MessagesWidget())
, m_last_scroll_range_load_messages(0)
, m_counter_uploaded_messages(0)
, m_scroll_slider_pressed(false)
, m_need_upload_messages(true)
, m_scroll_range(0)
, m_scroll_snap_bottom(true)
, m_may_scroll_range_load_messages(false)
, m_once_history_uploaded(0)
, m_unreaded_history_item(nullptr)
, m_may_upload_messages(true)
, m_once_after_load_messages(false)
{
	m_ui->setupUi(this);
	connect(m_ui->scroll_area->verticalScrollBar(), SIGNAL(rangeChanged(int, int)), this, SLOT(slot_scroll_change_range(int, int)));
	connect(m_ui->scroll_area->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(slot_scroll_changed(int)));
	connect(m_ui->scroll_area->verticalScrollBar(), SIGNAL(sliderPressed()), this, SLOT(slot_scroll_slider_pressed()));
	connect(m_ui->scroll_area->verticalScrollBar(), SIGNAL(sliderReleased()), this, SLOT(slot_scroll_slider_released()));

	connect(this, SIGNAL(signal_update_after_change_scroll_range()), SLOT(slot_update_after_change_scroll_range()), Qt::ConnectionType::QueuedConnection);
	connect(this, SIGNAL(signal_check_unreaded_messages()), SLOT(slot_check_unreaded_messages()), Qt::ConnectionType::QueuedConnection);
}
bool MessagesWidget::is_group() const
{
	return !m_chat->contact();
}
void MessagesWidget::set_max_width(int width)
{
	m_ui->scroll_area_content->setMaximumWidth(width);
}
void MessagesWidget::set_visible(bool visible)
{
	setVisible(visible);
	if (isVisible()) emit_update_after_change_scroll_range();
}
// добавить сообщение
void MessagesWidget::add_message(const ChatMessage* msg)
{
	const ChatMessageLocalId& local_id = msg->local_id;
	assert( !local_id.isNull() );

	bool is_removed = true;
	ItemMessage* item = nullptr;
	{
		auto it = m_local_id_items.find(local_id);
		if (m_local_id_items.end() == it)
		{
			if (item = create_item(msg))
				m_local_id_items[local_id] = item;
		}
		else
		{
			item = it->second;
			is_removed = false;
		}
	}

	if (item)
	{
		if (msg->id != 0)
		{
			auto it = m_id_items.find(msg->id);
			if (m_id_items.end() == it)
				m_id_items[msg->id] = item;
		}

		

		// вставляем виджет в новую позицию
		if (is_removed)
			insert_item(item);

		if (msg->status == ChatMessageStatus::Sending)
		{
			/*auto it = std::find(m_sending_items.begin(), m_sending_items.end(), item);
			if (m_sending_items.end() == it)
			{
				auto it_insert = std::upper_bound(m_sending_items.begin(), m_sending_items.end(), item, [](const ItemMessage* left, const ItemMessage* item) { return left->chat_message()->datetime < item->chat_message()->datetime; });
				m_sending_items.insert(it_insert, item);
			}*/

			m_sending_items[msg->local_id] = item;
		}
		else
		{
			m_sending_items.erase(msg->local_id);
		}

		// подготавливаем виджет отображения сообщений
		prepare_messages_widget(item);
		// обновляем его отображение
		item->update();
	}
}
// создать виджет
ItemMessage* MessagesWidget::create_item(const ChatMessage* msg)
{
	ItemMessage* item = nullptr;
	switch (msg->type)
	{
		case ChatMessageType::Text:
		{
			if (msg->myself)
				item = new ItemTextMessageMyself(m_ui->scroll_area_content, msg);
			else
				item = new ItemTextMessageAnother(m_ui->scroll_area_content, msg);
			break;
		}
		case ChatMessageType::File:
		{
			if (msg->myself)
			{
				ItemFileMessageMyself* file_item = new ItemFileMessageMyself(m_ui->scroll_area_content, msg);
				connect(file_item, SIGNAL(need_cancel(const ChatMessageLocalId&)), SIGNAL(need_file_message_cancel(const ChatMessageLocalId&)));
				m_myself_item_file_messages[msg->local_id] = file_item;
				item = file_item;
			}
			else
				item = new ItemFileMessageAnother(m_ui->scroll_area_content, msg);
			break;
		}
	}
	return item;
}
// удалить item из layout
void MessagesWidget::remove_item(ItemMessage* item)
{
	QVBoxLayout* layout = m_ui->verticalLayout_2;
	layout->removeWidget(item);
}
// подготовить виджет отображения сообщений под поступившее сообщение
void MessagesWidget::prepare_messages_widget(ItemMessage* item)
{
	const ChatMessage* msg = item->chat_message();
	if (!msg->myself && !msg->readed())
	{
		// заносим item в список непрочитанных сообщений
		m_unreaded_messages[msg->id] = item;
	}

	if (msg->historical)
	{
		// если первое получение исторических сообщений
		if (m_once_history_uploaded == 0)
		{
			// ищем первое непрочитанное сообщение
			if (!msg->readed())
			{
				if (m_unreaded_history_item)
				{
					if (const ChatMessage* chat_message = m_unreaded_history_item->chat_message())
					{
						if (msg->id < chat_message->id)
							m_unreaded_history_item = item;
					}
				}
				else m_unreaded_history_item = item;
			}
		}
	}
	else
	{
		adjust_content();
	}
}
void MessagesWidget::insert_item(ItemMessage* item)
{
	QVBoxLayout* layout = m_ui->verticalLayout_2;
	const ChatMessage* msg = item->chat_message();

	if (msg->status == ChatMessageStatus::Sending)
	{
		// вставляем в конец
		layout->insertWidget(layout->count() - 1, item);
	}
	else
	{
		auto it = std::upper_bound(m_id_items.begin(), m_id_items.end(), msg->id, [](ChatMessageId message_id, const std::pair<ChatMessageId, ItemMessage*>& pair) { return message_id < pair.first; });
		if (m_id_items.end() != it)
		{
			QWidget* pos_item = it->second;
			int index = layout->indexOf(pos_item);
			layout->insertWidget(index, item);
		}
		else
		{
			// вставляем в конец
			layout->insertWidget(layout->count() - 1 - m_sending_items.size(), item);
		}
	}
}
void MessagesWidget::slot_scroll_change_range(int min, int max)
{
	m_scroll_range = max - min;
	//logs(QString("slot_scroll_change_range  %1").arg(m_scroll_range));

	if (m_scroll_snap_bottom)
		set_scroll_value(max);

	internal_check_unreaded_messages();
}
// проверить, нужно ли загрузить историю сообщений
void MessagesWidget::check_history_messages()
{
	//logs(QString("check_history_messages"));

	// если виджет сообщений скрыт... то сообщения всё загружаются и загружаются
	if (isVisible())
	{
		// если есть возможность + необходимость + и не конец
		if (m_may_upload_messages && m_need_upload_messages)// && !m_finish_upload_messages)
		{
			//logs(QString("emit request_history_messages"));

			m_may_upload_messages = false;	// загружать историю пока не может
			m_need_upload_messages = false;	// снимаем необходимость загрузки
			// запрашиваем исторические сообщения по наименьшему id
			emit request_history_messages(m_chat->next_history_message_id());
		}
	}
}
void MessagesWidget::emit_update_after_change_scroll_range()
{
	//logs(QString("emit_update_after_change_scroll_range"));

	//std::cout << "emit_update_after_change_scroll_range()" << std::endl;
	emit signal_update_after_change_scroll_range();
}
// событие после изменения размера занимаемой высоты всех сообщений
void MessagesWidget::slot_update_after_change_scroll_range()
{
	//logs(QString("slot_update_after_change_scroll_range"));

	if (m_may_scroll_range_load_messages && m_last_scroll_range_load_messages > 0 && m_last_scroll_range_load_messages != m_scroll_range)
	{
		int new_range = m_scroll_range - m_last_scroll_range_load_messages;
		set_scroll_value(new_range);
		m_last_scroll_range_load_messages = 0;
		m_may_scroll_range_load_messages = false;
	}

	//if ((!m_items.empty() || after_load_messages) && !m_once_history_uploaded)
	if (m_once_history_uploaded == 1 && isVisible())
	{
		++m_once_history_uploaded;// = true;
		// если есть непрочитанное сообщение, то ставим курсор на первое непрочитанное сообщение
		if (m_unreaded_history_item)
		{
			int y = m_unreaded_history_item->pos().y();
			int mid = y - scroll_window_height() / 2;
			int scroll = std::max(std::min(mid, m_scroll_range), 0);

			if (scroll_value() != scroll)
				set_scroll_value(scroll);
			else
				slot_scroll_changed(scroll_value());

			m_unreaded_history_item = nullptr;
		}
		else
		{
			// если нет новых сообщений, то скроллим вниз
			set_scroll_value(m_scroll_range);
		}
	}

	// вызываем после выставления set_scroll_value
	check_loaded_nesessary_history_messages();

	// проверяем на непрочитанные сообщения
	//internal_check_unreaded_messages();
}
// проверить, нужно ли загружать историю сообщение в размер qscrollarea
void MessagesWidget::check_loaded_nesessary_history_messages()
{
	//logs(QString("check_loaded_nesessary_history_messages"));

	if (isVisible())
	{
		/*// только после загрузки сообщений
		if (m_once_after_load_messages)
		{
			m_once_after_load_messages = false;

			if (m_counter_uploaded_messages > 0)
				m_finish_upload_messages = true;
			else
			{
				m_may_upload_messages = true;
				m_need_upload_messages = false;
			}
		}*/

		if (m_scroll_range <= 0)
			m_need_upload_messages = true;
		//else if (scroll_value() < 1)
		//	m_need_upload_messages = true;

		check_history_messages();
	}
}
void MessagesWidget::slot_scroll_changed(int value)
{
	//logs(QString("slot_scroll_changed value=%1").arg(value));

	//std::cout << "scroll changed: value = " << value << std::endl;
	// если слайдер не двигаем мышкой
	if (!m_scroll_slider_pressed)
		check_scroll_zero();

	// включаем привязку к низу
	m_scroll_snap_bottom = (m_scroll_range - value) < 48;

	// отключение привязки
	if (m_scroll_snap_bottom)
	{
		unreaded_messages_t::iterator it = m_unreaded_messages.begin();
		if (it != m_unreaded_messages.end())
		{
			ItemMessage* item = it->second;
			int pos_y = item->pos().y();
			if (pos_y < scroll_value())
			{
				// если новые!!! непрочитанные!!! сообщения не помещаются целиком в видимую часть экрана... то отключаем привязку книзу
				m_scroll_snap_bottom = false;
			}
		}
	}
	// проверяем на непрочитанные сообщения
	internal_check_unreaded_messages();
}
void MessagesWidget::slot_scroll_slider_pressed()
{
	m_scroll_slider_pressed = true;
}
// при отпускании сладйера подгружаем сообщения
void MessagesWidget::slot_scroll_slider_released()
{
	check_scroll_zero();
	m_scroll_slider_pressed = false;
}
// проверить, нужно ли подгрузить новую порцию сообщений
void MessagesWidget::check_scroll_zero()
{
	if (scroll_value() < 1)
	{
		//logs(QString(" check_scroll_zero need_upload"));

		m_need_upload_messages = true;
		check_history_messages();
	}
}
// проверяем на непрочитанные сообщения
void MessagesWidget::check_unreaded_messages()
{
	//if (isVisible()) emit_update_after_change_scroll_range();
	if (isVisible())
	{
		emit signal_check_unreaded_messages();
	}
}
void MessagesWidget::slot_check_unreaded_messages()
{
	internal_check_unreaded_messages();
}
void MessagesWidget::internal_check_unreaded_messages()
{
	if (isVisible())
	{
		int end_scroll = scroll_window_height() + scroll_value();
		//logs(QString("end_scroll = %1").arg(end_scroll));

		unreaded_messages_t::iterator last_it = m_unreaded_messages.end();
		for (unreaded_messages_t::iterator it = m_unreaded_messages.begin(); it != m_unreaded_messages.end(); ++it)
		{
			ItemMessage* item = it->second;
			//if (!(it->second.readed_send))
			{
				int end_pos = item->pos().y() + item->height();
				//logs(QString("  m-> %1 %2").arg(item->chat_message()->id).arg(end_pos));
				if ((end_pos - 11) < end_scroll)
					last_it = it;
				else break;
			}
		}
		if (last_it != m_unreaded_messages.end())
		{
			//if (!(last_it->second.readed_send))
			{
				bool send_readed = false;
				emit read_message(last_it->first, send_readed);
				if (send_readed)
				{
					unreaded_messages_t::iterator it_end = last_it;
					++it_end;
					//for (unreaded_messages_t::iterator it = m_unreaded_messages.begin(); it != it_end; ++it)
					//	it->second.readed_send = true;
				}
			}
		}
	}
}
// высота окна той видимой области, в которую помещаются сообщения
int MessagesWidget::scroll_window_height() const
{
	//int scroll_area_height = m_ui->scroll_area_content->height();
	//return scroll_area_height - m_scroll_range;

	int scroll_area_height = m_ui->scroll_area_content->height();
	int v = scroll_area_height - m_scroll_range;

	//logs(QString("scroll_window_height = %1").arg(v));
	return v;
}
int MessagesWidget::scroll_value() const
{
	return m_ui->scroll_area->verticalScrollBar()->value();
}
// установить значение скроллинга
void MessagesWidget::set_scroll_value(int value)
{
	//logs(QString("set_scroll_value value=%1").arg(value));

	m_ui->scroll_area->verticalScrollBar()->setValue(value);
}
// подогнать содержимое под размер
void MessagesWidget::adjust_content()
{
	m_ui->scroll_area_content->adjustSize();

	//logs(QString("adjust_content"));

	/*m_ui->scroll_area_content->resize(100, 10000);
	
	m_ui->scroll_area_content->updateGeometry();


	for (unreaded_messages_t::iterator it = m_unreaded_messages.begin(); it != m_unreaded_messages.end(); ++it)
	{
		ItemMessage* item = it->second.item;
		int end_pos = item->pos().y() + item->height();
		logs(QString("   ++++  %1 %2").arg(item->chat_message()->id).arg(end_pos));
		//if ((end_pos - 11) < end_scroll)
		//	last_it = it;
		//else break;
	}*/
}


// начало загрузки исторических сообщений
void MessagesWidget::start_load_history_messages()
{
	/*if (!m_may_scroll_range_load_messages)
		m_last_scroll_range_load_messages = m_scroll_range;
	else
		m_last_scroll_range_load_messages = 0;*/

	m_last_scroll_range_load_messages = m_scroll_range;
}
// окончание загрузки исторических сообщений
void MessagesWidget::finish_load_history_messages()
{
	// подгоняем размер виджетов (без этого видны рывки "разжатия" содержимого)
	adjust_content();

	if (m_last_scroll_range_load_messages > 0)
		m_may_scroll_range_load_messages = true;

	if (m_once_history_uploaded == 0) ++m_once_history_uploaded;
	m_once_after_load_messages = true;
	emit_update_after_change_scroll_range();

	m_may_upload_messages = true;
	m_need_upload_messages = false;
}
// отметить, что сообщение прочитано (и предыдущие тоже)
void MessagesWidget::set_message_readed(const ChatMessage* message)
{
	unreaded_messages_t::iterator it = m_unreaded_messages.find(message->id);
	if (m_unreaded_messages.end() != it) m_unreaded_messages.erase(m_unreaded_messages.begin(), ++it);
}
// установить прогресс передачи файлового сообщения
void MessagesWidget::set_file_message_progress(const ChatMessageLocalId& message_local_id, unsigned char progress)
{
	auto it = m_myself_item_file_messages.find(message_local_id);
	if (m_myself_item_file_messages.end() != it)
		it->second->set_progress(progress);
}
// удалить сообщение
void MessagesWidget::remove_message(const ChatMessageLocalId& message_local_id)
{
	m_myself_item_file_messages.erase(message_local_id);

	auto it = m_local_id_items.find(message_local_id);
	if (m_local_id_items.end() != it)
	{
		ItemMessage* item = it->second;
		const ChatMessage* msg = item->chat_message();
		
		if (msg->id != 0)
		{
			m_id_items.erase(msg->id);
			m_unreaded_messages.erase(msg->id);
		}

		if (m_unreaded_history_item == item)
			m_unreaded_history_item = nullptr;

		remove_item(item);
		delete item;
		m_local_id_items.erase(it);
	}
}