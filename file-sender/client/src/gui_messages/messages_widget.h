//=====================================================================================//
//   Author: open
//   Date:   27.04.2017
//=====================================================================================//
#pragma once
#include "gui_messages/item_notification_error.h"
#include "ui_messages_widget.h"
#include <QDateTime>

class ItemTextMessageAnother;
class ItemFileMessageAnother;
class ItemTextMessageMyself;
class ItemFileMessageMyself;
class ItemFileMessage;
class FileTransfer;
class ItemMessage;
class QVBoxLayout;
class ChatMessage;
class User;
class Chat;

class MessagesWidget : public QWidget
{
	Q_OBJECT
private:
	std::unique_ptr<Ui::MessagesWidget> m_ui;
	const Chat* m_chat;
	
	bool m_may_scroll_range_load_messages;
	int m_last_scroll_range_load_messages;	// значение рейнджа перед подгрузкой сообщений скроллингом
	int m_counter_uploaded_messages;		// кол-во зарезервированных сообщений в порции при подгрузке

	bool m_need_upload_messages;			// флаг необходимости загрузки сообщений
	bool m_may_upload_messages;				// флаг возможности загрузки сообщений
	bool m_once_after_load_messages;		// флаг обработки события после загрузки сообщений (вынесен отдельно, потому что не всегда виджет виден)

	bool m_scroll_slider_pressed;			// флаг нажатия мышкой на слайдер

	bool m_scroll_snap_bottom;				// привязка "к низу" (скроллинг в конец)
	int m_scroll_range;						// тот размер, который не влазит в qscrollarea

	int m_once_history_uploaded;			// первое получение исторических сообщений
	ItemMessage* m_unreaded_history_item;

	// виджеты сообщений
	std::map<ChatMessageId, ItemMessage*> m_id_items;
	std::map<ChatMessageLocalId, ItemMessage*> m_local_id_items;

	// отправляемые сообщения
	std::map<ChatMessageLocalId, ItemMessage*> m_sending_items;

	// непрочитанные сообщения
	using unreaded_messages_t = std::map<ChatMessageId, ItemMessage*>;
	unreaded_messages_t m_unreaded_messages;	

	// мои файловые сообщения
	std::map<ChatMessageLocalId, ItemFileMessage*> m_myself_item_file_messages;

public:
	MessagesWidget(QWidget* parent, const Chat* chat);
	// установить видимость виджета сообщений
	void set_visible(bool visible);
	// установить максимальную ширину
	void set_max_width(int width);
	// добавить сообщение
	void add_message(const ChatMessage* msg);
	// удалить сообщение
	void remove_message(const ChatMessageLocalId& message_local_id);
	// начало загрузки исторических сообщений
	void start_load_history_messages();
	// окончание загрузки исторических сообщений
	void finish_load_history_messages();
	// проверяем на непрочитанные сообщения
	void check_unreaded_messages();
	// отметить, что сообщение прочитано (и предыдущие тоже)
	void set_message_readed(const ChatMessage* message);
	// установить прогресс передачи файлового сообщения
	void set_file_message_progress(const ChatMessageLocalId& message_local_id, unsigned char progress);
	//void set_file_message_progress(ChatMessageId message_id, unsigned char progress);

signals:
	// сигнал о запросе исторических сообщений
	void request_history_messages(ChatMessageId message_id);
	// сигнал о детекции прочтения сообщения - оно целиком было видно -> считаем, что оно прочитано
	void read_message(ChatMessageId message_id, bool& send_readed);
	// нажатие на отмену файлового сообщения
	void need_file_message_cancel(const ChatMessageLocalId& message_local_id);

// internal
	void signal_update_after_change_scroll_range();
	void signal_check_unreaded_messages();

//public slots:
	//void slot_make_message_received_on_server(const ChatMessageLocalId& message_local_id);

private slots:
	void slot_scroll_change_range(int min, int max);
	void slot_scroll_changed(int value);
	void slot_update_after_change_scroll_range();
	void slot_scroll_slider_pressed();
	void slot_scroll_slider_released();
	void slot_check_unreaded_messages();

private:
	void check_history_messages();
	void check_scroll_zero();
	void check_loaded_nesessary_history_messages();

	void emit_update_after_change_scroll_range();
	// это групповой чат?
	bool is_group() const;
	// высота окна той видимой области, в которую помещаются сообщения
	int scroll_window_height() const;
	// значение скроллинга
	int scroll_value() const;
	// установить значение скроллинга
	void set_scroll_value(int value);
	// подогнать содержимое под размер
	void adjust_content();

	// найти item и переместить его в правильный контейнер
	//ItemMessage* get_item_and_correct_container(const ChatMessage* msg);
	// создать виджет
	ItemMessage* create_item(const ChatMessage* msg);
	// вставка виджета сообщения
	void insert_item(ItemMessage* item);
	// удалить item из layout
	void remove_item(ItemMessage* item);
	// подготовить виджет отображения сообщений под поступившее сообщение
	void prepare_messages_widget(ItemMessage* item);
	// внутренний метод проверки на непрочитанные сообщения
	void internal_check_unreaded_messages();
};