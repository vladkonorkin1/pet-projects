//=====================================================================================//
//   Author: open
//   Date:   27.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "messages_widget_manager.h"
#include "user.h"
#include "messages_widget.h"
#include "item_file_message_another.h"
#include "item_file_message_myself.h"
#include "chat_top/contact_chat_top.h"
#include "chat_top/group_chat_top.h"
#include "chats.h"

MessagesWidgetManager::MessagesWidgetManager(Chats* chats, QWidget* content_top, QBoxLayout* layout_top, QWidget* content_write, QBoxLayout* layout_write)
: m_content_top(content_top)
, m_layout_top(layout_top)
, m_content_write(content_write)
, m_layout_write(layout_write)
, m_current_widget(nullptr)
, m_contact_widget(create_messages_widget(nullptr))
, m_group_chat_top_user_context_menu(content_top, chats)
, m_chats(chats)
, m_max_width(0)
{
	connect(m_chats, SIGNAL(chat_change_users(const Chat*)), SLOT(slot_chat_change_users(const Chat*)));
	connect(m_chats, SIGNAL(chat_message_readed(const Chat*, const ChatMessage*)), SLOT(slot_chat_message_readed(const Chat*, const ChatMessage*)));
	connect(m_chats, SIGNAL(chat_created(Chat*)), SLOT(slot_chat_created(Chat*)));
	connect(m_chats, SIGNAL(chat_message_removed(const Chat*, const ChatMessageLocalId&)), SLOT(slot_chat_message_removed(const Chat*, const ChatMessageLocalId&)));
	connect(m_contact_widget->write_message_widget.get(), SIGNAL(new_text_message(const QString&)), SIGNAL(contact_new_text_message(const QString&)));
	connect(&m_group_chat_top_user_context_menu, SIGNAL(add_contact_to_chat_list(const User*)), SIGNAL(need_add_chat_to_chat_list(const User*)));
	connect(&m_group_chat_top_user_context_menu, SIGNAL(chat_remove_user(const Chat*, const User*)), SIGNAL(need_group_chat_remove_user(const Chat*, const User*)));
	connect(&m_group_chat_top_user_context_menu, SIGNAL(set_chat_admin(const Chat*, const User*)), SIGNAL(need_group_chat_set_admin(const Chat*, const User*)));
	connect(&m_group_chat_top_user_context_menu, SIGNAL(remove_chat_admin(const Chat*, const User*)), SIGNAL(need_group_chat_remove_admin(const Chat*, const User*)));
}
void MessagesWidgetManager::set_current_messages_widget(UWidget widget)
{
	if (widget != m_current_widget)
	{
		if (m_current_widget) m_current_widget->set_visible(false);
		m_current_widget = widget;
		if (m_current_widget) m_current_widget->set_visible(true);
	}
}
MessagesWidgetManager::UWidget MessagesWidgetManager::get_widget(const Chat* chat)
{
	if (!chat) return nullptr;

	chat_widgets_t::iterator it = m_chat_widgets.find(chat->id());
	if (it == m_chat_widgets.end())
	{
		UWidget widget = create_messages_widget(chat);
		connect(widget->messages_widget.get(), &MessagesWidget::request_history_messages, [this, chat](ChatMessageId message_id) { emit request_history_messages(chat, message_id); });
		connect(widget->messages_widget.get(), &MessagesWidget::read_message, [this, chat](ChatMessageId message_id, bool& send_readed) { emit read_message(chat, message_id, send_readed); });
		connect(widget->messages_widget.get(), SIGNAL(need_file_message_cancel(const ChatMessageLocalId&)), SIGNAL(need_file_message_cancel(const ChatMessageLocalId&)));
		connect(widget->write_message_widget.get(), &WriteMessageWidget::new_text_message, [this, chat](const QString& text) { emit new_text_message(chat, text); });
		connect(widget->write_message_widget.get(), &WriteMessageWidget::new_file_message, [this, chat](const QList<QUrl>& urls) { emit new_file_message( urls); });
		connect(widget->top_widget.get(), SIGNAL(need_modify_chat_users(const QPoint&)), SIGNAL(need_modify_chat_users(const QPoint&)));
		it = m_chat_widgets.insert(std::make_pair(chat->id(), widget)).first;

		update_enable_messages_widget(widget.get(), chat);

		widget->messages_widget->start_load_history_messages();
		// добавляем сообщения
		for (const ChatMessage* msg : chat->sorted_messages())
			add_message(chat, msg);
		widget->messages_widget->finish_load_history_messages();
	}
	return it->second;
}
MessagesWidgetManager::UBaseChatTop MessagesWidgetManager::create_chat_top_widget(const Chat* chat)
{
	if (const User* contact = chat->contact())
	{
		ContactChatTop* top = new ContactChatTop(m_content_top, chat);
		m_layout_top->addWidget(top);
		return UBaseChatTop(top);
	}
	
	GroupChatTop* top = new GroupChatTop(m_content_top, chat, &m_group_chat_top_user_context_menu);
	m_layout_top->addWidget(top);
	connect(top, SIGNAL(add_contact_to_chat_list(const User*)), SIGNAL(need_add_chat_to_chat_list(const User*)));
	top->set_enabled_add_users_button(chat->is_admin());
	return UBaseChatTop(top);
}
MessagesWidgetManager::UWidget MessagesWidgetManager::create_messages_widget(const Chat* chat)
{
	UWidget w(new Widget(chat));
	if (chat)
	{
		UBaseChatTop top = create_chat_top_widget(chat);
		
		UMessagesWidget messages_widget(new MessagesWidget(nullptr, chat));
		top->add_messages_widget(messages_widget.get());

		w->top_widget = top;
		w->messages_widget = messages_widget;
		w->messages_widget->set_max_width(m_max_width);
	}

	UWriteMessageWidget wmw(new WriteMessageWidget(m_content_write));
	m_layout_write->addWidget(wmw.get());
	w->write_message_widget = wmw;
	
	w->set_visible(false);
	return w;
}
MessagesWidget* MessagesWidgetManager::find_messages_widget(const Chat* chat)
{
	assert( chat );
	if (Widget* widget = find_widget(chat))
		return widget->messages_widget.get();
	return nullptr;
}
void MessagesWidgetManager::activate_message_widget(const Chat* chat)
{
	set_current_messages_widget(get_widget(chat));
}
void MessagesWidgetManager::activate_default_contact_widget()
{
	m_contact_widget->write_message_widget->clear();
	set_current_messages_widget(m_contact_widget);
}

void MessagesWidgetManager::Widget::set_visible(bool visible)
{
	if (top_widget.get()) top_widget->setVisible(visible);
	if (messages_widget.get()) messages_widget->set_visible(visible);
	write_message_widget->setVisible(visible);
}
void MessagesWidgetManager::send_current_text_message()
{
	if (m_current_widget) {
		m_current_widget->write_message_widget->send_text_message();
	}
}
void MessagesWidgetManager::resize_scroll_area(const QSize& size)
{
	m_max_width = size.width();
	for (auto& pair : m_chat_widgets)
	{
		Widget* widget = pair.second.get();
		if (widget) widget->messages_widget->set_max_width(m_max_width);
	}
}
// можно ли писать в чат
bool MessagesWidgetManager::is_chat_enable_write(const Chat* chat) const
{
	if (!chat->contact())
		return chat->is_participant();
	return chat->is_enabled();
}
void MessagesWidgetManager::update_enable_messages_widget(Widget* widget, const Chat* chat)
{
	widget->write_message_widget->set_enabled(is_chat_enable_write(chat));
}
MessagesWidgetManager::Widget* MessagesWidgetManager::find_widget(const Chat* chat)
{
	chat_widgets_t::iterator it = m_chat_widgets.find(chat->id());
	if (it != m_chat_widgets.end())
		return it->second.get();
	return nullptr;
}
void MessagesWidgetManager::slot_chat_change_users(const Chat* chat)
{
	if (!chat->contact())
	{
		if (Widget* widget = find_widget(chat))
		{
			// если не являешься участником сообщения не напишешь
			update_enable_messages_widget(widget, chat);
			// обрабатываем изменение участников
			widget->top_widget->process_changed_users();
			widget->top_widget->set_enable_add_users_button(chat->is_admin());
		}
	}
}
void MessagesWidgetManager::slot_chat_created(Chat* chat)
{
	if (chat->is_enabled() && !chat->contact())
	{
		if (Widget* widget = find_widget(chat))
			widget->top_widget->update_name();
	}
}
// добавить сообщение
void MessagesWidgetManager::add_message(const Chat* chat, const ChatMessage* msg)
{
	if (MessagesWidget* messages_widget = find_messages_widget(chat))
		messages_widget->add_message(msg);
}
// проверить на непрочитанные сообщения
void MessagesWidgetManager::check_unreaded_messages(const Chat* chat)
{
	if (MessagesWidget* messages_widget = find_messages_widget(chat))
		messages_widget->check_unreaded_messages();
}
// начало загрузки исторических сообщений
void MessagesWidgetManager::start_load_history_messages(const Chat* chat)
{
	if (MessagesWidget* messages_widget = find_messages_widget(chat))
		messages_widget->start_load_history_messages();
}
// окончание загрузки исторических сообщений
void MessagesWidgetManager::finish_load_history_messages(const Chat* chat)
{
	if (MessagesWidget* messages_widget = find_messages_widget(chat))
		messages_widget->finish_load_history_messages();
}
// событие прочтения сообщения
void MessagesWidgetManager::slot_chat_message_readed(const Chat* chat, const ChatMessage* message)
{
	if (MessagesWidget* messages_widget = find_messages_widget(chat))
		messages_widget->set_message_readed(message);
}
void MessagesWidgetManager::set_file_message_progress(const ChatMessageLocalId& message_local_id, const Chat* chat, unsigned char progress)
{
	if (MessagesWidget* messages_widget = find_messages_widget(chat))
		messages_widget->set_file_message_progress(message_local_id, progress);
}
//void MessagesWidgetManager::set_file_message_progress(ChatMessageId message_id, const Chat* chat, unsigned char progress)
//{
//	if (MessagesWidget* messages_widget = find_messages_widget(chat))
//		messages_widget->set_file_message_progress(message_id, progress);
//}
// событие удаления сообщения чата
void MessagesWidgetManager::slot_chat_message_removed(const Chat* chat, const ChatMessageLocalId& message_local_id)
{
	if (MessagesWidget* messages_widget = find_messages_widget(chat))
		messages_widget->remove_message(message_local_id);
}