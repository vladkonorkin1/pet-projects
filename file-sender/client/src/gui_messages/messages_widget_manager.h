//=====================================================================================//
//   Author: open
//   Date:   27.04.2017
//=====================================================================================//
#pragma once
#include "gui_messages/chat_top/group_chat_top_user_context_menu.h"
#include "gui_messages/chat_top/base_chat_top.h"
#include "gui_messages/write_message_widget.h"
#include "gui_messages/messages_widget.h"
#include <QScrollArea>

class ItemFileMessage;
class FileTransfer;
class ChatMessage;
class QBoxLayout;
class User;
class Chats;
class Chat;

class MessagesWidgetManager : public QObject
{
	Q_OBJECT
private:
	using UBaseChatTop = std::auto_ptr<BaseChatTop>;
	using UMessagesWidget = std::auto_ptr<MessagesWidget>;
	using UWriteMessageWidget = std::auto_ptr<WriteMessageWidget>;
	struct Widget
	{
		Widget(const Chat* chat) : chat(chat) {}
		const Chat* chat;
		UBaseChatTop top_widget;
		UMessagesWidget messages_widget;
		UWriteMessageWidget write_message_widget;
		void set_visible(bool visible);
	};
	
	Chats* m_chats;
	QWidget* m_content_top;
	QBoxLayout* m_layout_top;
	QWidget* m_content_write;
	QBoxLayout* m_layout_write;

	using UWidget = std::shared_ptr<Widget>;
	UWidget m_current_widget;
	UWidget m_contact_widget;

	using chat_widgets_t = std::map<ChatId, UWidget>;
	chat_widgets_t m_chat_widgets;

	GroupChatTopUserContextMenu m_group_chat_top_user_context_menu;

	int m_max_width;

public:
	MessagesWidgetManager(Chats* chats, QWidget* content_top, QBoxLayout* layout_top, QWidget* content_write, QBoxLayout* layout_write);

	void activate_message_widget(const Chat* chat);
	void activate_default_contact_widget();
	void send_current_text_message();
	// проверить на непрочитанные сообщения
	void check_unreaded_messages(const Chat* chat);
	// обновить прогресс передачи
	//void set_file_message_progress(ChatMessageId message_id, const Chat* chat, unsigned char progress);
	void set_file_message_progress(const ChatMessageLocalId& message_local_id, const Chat* chat, unsigned char progress);
	// добавить сообщение
	void add_message(const Chat* chat, const ChatMessage* msg);

signals:
	void new_text_message(const Chat* chat, const QString& text);
	void new_file_message(const QList<QUrl>& urls);
	void contact_new_text_message(const QString& text);
	
	void request_history_messages(const Chat* chat, ChatMessageId message_id);
	void need_add_chat_to_chat_list(const User* contact);
	void need_group_chat_remove_user(const Chat* chat, const User* user);
	void need_group_chat_set_admin(const Chat* chat, const User* user);
	void need_group_chat_remove_admin(const Chat* chat, const User* user);
	void read_message(const Chat* chat, ChatMessageId message_id, bool& send_readed);
	void need_modify_chat_users(const QPoint& position);
	void need_file_message_cancel(const ChatMessageLocalId& message_local_id);

public slots:
	void resize_scroll_area(const QSize& size);
	void slot_chat_change_users(const Chat* chat);
	void slot_chat_created(Chat* chat);
	// событие удаления сообщения чата
	void slot_chat_message_removed(const Chat* chat, const ChatMessageLocalId& message_local_id);
	// событие прочтения сообщения
	void slot_chat_message_readed(const Chat* chat, const ChatMessage* message);
	// начало загрузки исторических сообщений
	void start_load_history_messages(const Chat* chat);
	// окончание загрузки исторических сообщений
	void finish_load_history_messages(const Chat* chat);

private:
	// получить виджет (или создать его)
	UWidget get_widget(const Chat* chat);
	// найти виджет (его может не быть)
	Widget* find_widget(const Chat* chat);
	MessagesWidget* find_messages_widget(const Chat* chat);
	void set_current_messages_widget(UWidget widget);
	UWidget create_messages_widget(const Chat* chat);
	UBaseChatTop create_chat_top_widget(const Chat* chat);
	void update_enable_messages_widget(Widget* widget, const Chat* chat);
	// можно ли писать в чат
	bool is_chat_enable_write(const Chat* chat) const;
};