//=====================================================================================//
//   Author: open
//   Date:   24.05.2017
//=====================================================================================//
#include "base/precomp.h"
#include "write_message_widget.h"
#include <QKeyEvent>

static int s_max_chars = 2000;

WriteMessageWidget::WriteMessageWidget(QWidget* parent)
: QPlainTextEdit(parent)
{
	QFont font;
	font.setPointSize(10);
	setFont(font);
	setStyleSheet(QLatin1String("QPlainTextEdit { background-color: rgb(244, 245, 245); border-radius: 10px; padding: 10px; } QPlainTextEdit:disabled{background:white;}"));
	setFrameShape(QFrame::NoFrame);
}
void WriteMessageWidget::send_text_message()
{
	QString text = toPlainText();
	QString modificate = text.remove(QRegExp("[ \t\r\n]+$"));	// remove empty characters at end
	if (!modificate.isEmpty())
	{
		emit new_text_message(modificate);
		clear();
	}
}
void WriteMessageWidget::keyPressEvent(QKeyEvent* event)
{
	const int key = event->key();
	if ((key == Qt::Key_Return || key == Qt::Key_Enter) && !(event->modifiers() & Qt::ControlModifier))
	{
		send_text_message();
	}
	else
	{
		QString text = toPlainText();
		if (text.length() >= s_max_chars)
		{
			bool delete_char = (key == Qt::Key_Delete) || (key == Qt::Key_Backspace) || (key == Qt::Key_Cancel);
			bool asciiCtrl = false;
			QString txt = event->text();
			if (txt.length() == 1)
			{
				int sym = txt.toUtf8().at(0);
				if ((sym >= 0 && sym <= 31) || sym == 127)
					asciiCtrl = true;
			}

			QTextCursor cursor = textCursor();
			bool select = cursor.selectionStart() != cursor.selectionEnd();

			if (txt.isEmpty() || delete_char || asciiCtrl || select)
				QPlainTextEdit::keyPressEvent(event);
		}
		else
		{
			if ((key == Qt::Key_Return || key == Qt::Key_Enter) && (event->modifiers() & Qt::ControlModifier))
				insertPlainText("\n");

			QPlainTextEdit::keyPressEvent(event);
		}
	}
}
#include <QMimeData>
void WriteMessageWidget::insertFromMimeData(const QMimeData* source)
{
	QMimeData scopy;
	if (source->hasColor())
	{
		scopy.setColorData(source->colorData());
	}
	if (source->hasHtml())
	{
		scopy.setHtml(source->html());
	}
	if (source->hasImage())
	{
		scopy.setImageData(source->imageData());
	}
	if (source->hasText())
	{
		scopy.setText(source->text());
	}
	if (source->hasUrls())
	{
		emit new_file_message(source->urls());
		return;
	}

	if (s_max_chars > 0 && source->hasText())
	{
		QString paste_text = source->text();
		QString text = toPlainText();
		QTextCursor cursor = textCursor();
		int num_selected = cursor.selectionEnd() - cursor.selectionStart();
		//int new_length = text.length() + paste_text.length() - num_selected;

		int num_ost = text.length() - num_selected;
		int num = s_max_chars - num_ost;

		QString new_text = scopy.text().left(num);
		scopy.setText(new_text);

		QPlainTextEdit::insertFromMimeData(&scopy);
	}
}
void WriteMessageWidget::set_enabled(bool enable)
{
	setReadOnly(!enable);
	if (!enable)
		clear();
	setPlaceholderText(enable ? tr("Type a message here") : tr("You don't enter into this group"));
}