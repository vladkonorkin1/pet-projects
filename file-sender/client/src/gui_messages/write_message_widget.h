//=====================================================================================//
//   Author: open
//   Date:   24.05.2017
//=====================================================================================//
#pragma once
#include <QPlainTextEdit>
//#include "ui_write_message_widget.h"

class WriteMessageWidget : public QPlainTextEdit
{
	Q_OBJECT
//private:
	//std::unique_ptr<Ui::WriteMessageWidget> m_ui;

public:
	WriteMessageWidget(QWidget* parent);
	void send_text_message();
	void set_enabled(bool enable);
	//void clear();

signals:
	void new_text_message(const QString& text);
	void new_file_message(const QList<QUrl>& urls);

//private slots:
	//void slot_text_changed();

protected:
	virtual void keyPressEvent(QKeyEvent* event);
	virtual void insertFromMimeData(const QMimeData* source);
	//virtual void keyPressEvent(QKeyEvent* event);
	//virtual bool eventFilter(QObject* obj, QEvent* event);
};