//=====================================================================================//
//   Author: open
//   Date:   04.06.2019
//=====================================================================================//
#include "base/precomp.h"
#include "loader_chat_messages.h"
#include "database/database.h"
#include "connection.h"
#include "chats.h"

LoaderChatMessages::LoaderChatMessages(const Chat* chat, Database& database, Connection& connection, Chats& chats)
: m_chat(chat)
, m_database(database)
, m_connection(connection)
, m_chats(chats)
, m_local(true)
, m_sending_remote_request(false)
, m_may_remote_request(true)
{
}
// загрузить сообщения
bool LoaderChatMessages::request_messages()
{
	ChatMessageId message_id = m_chat->next_history_message_id();
	logs(QString("LoaderChatMessages::request_messages() local=%1 chat_id=%2 msg_id=%3").arg(m_local).arg(m_chat->id()).arg(message_id));
	if (m_local)
	{
		// локальный запрос
		unsigned int limit = 34;
		std::vector<DescChatMessage> messages = m_database.select_chat_messages(m_chat->id(), message_id, limit);

		if (messages.size() < limit)
			m_local = false;

		if (!messages.empty())
		{
			emit local_chat_history_messages_loaded(m_chat, messages);
			return true;
		}
	}
	
	// удаленнный запрос
	if (!m_may_remote_request) return false;

	send_remote_request();
	return true;
}
// переотправить запросы
void LoaderChatMessages::resend()
{
	// если отправляли запрос, то отправим его ещё раз
	if (m_sending_remote_request)
		send_remote_request();
}
// отправить запрос на получение данных
void LoaderChatMessages::send_remote_request()
{
	m_sending_remote_request = true;
	m_connection.send_message(ClientMessageChatRequestHistoryMessages(m_chat->id(), m_chat->next_history_message_id()));
}
// обработать получение исторических сообщений по сети
void LoaderChatMessages::remote_messages_received(bool finish_upload_history_messages)
{
	m_sending_remote_request = false;
	m_may_remote_request = !finish_upload_history_messages;
}



LoaderChatMessagesManager::LoaderChatMessagesManager(Database& database, Connection& connection, Chats& chats)
: m_database(database)
, m_connection(connection)
, m_chats(chats)
{
}
bool LoaderChatMessagesManager::request_messages(const Chat* chat)
{
	auto it = m_loader_chats.find(chat->id());
	if (m_loader_chats.end() == it)
	{
		it = m_loader_chats.insert(std::make_pair(chat->id(), ULoaderChatMessages(new LoaderChatMessages(chat, m_database, m_connection, m_chats)))).first;
		connect(it->second.get(), SIGNAL(local_chat_history_messages_loaded(const Chat*, const std::vector<DescChatMessage>&)), SIGNAL(local_chat_history_messages_loaded(const Chat*, const std::vector<DescChatMessage>&)));
	}
	it->second->request_messages();
	return false;
}
// переотправить зависшие запросы на получение исторических сообщений
void LoaderChatMessagesManager::resend()
{
	for (auto& pair : m_loader_chats)
		pair.second->resend();
}
// обработать получение исторических сообщений по сети
void LoaderChatMessagesManager::remote_messages_received(const Chat* chat, bool finish_upload_history_messages)
{
	auto it = m_loader_chats.find(chat->id());
	if (m_loader_chats.end() != it)
		it->second->remote_messages_received(finish_upload_history_messages);
}