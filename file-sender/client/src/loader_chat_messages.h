//=====================================================================================//
//   Author: open
//   Date:   04.06.2019
//=====================================================================================//
#pragma once

class Connection;
class Database;
class Chats;
class Chat;

class LoaderChatMessages : public QObject
{
	Q_OBJECT
private:
	const Chat* m_chat;
	Database& m_database;
	Connection& m_connection;		// сетевое подключение
	Chats& m_chats;
	bool m_local;					// локальная выборка из базы или удалённый запрос
	bool m_sending_remote_request;	// удаленный запрос
	bool m_may_remote_request;		// можно ли ещё выполнять удаленные запросы

public:
	LoaderChatMessages(const Chat* chat, Database& database, Connection& connection, Chats& chats);
	// загрузить сообщения
	bool request_messages();
	// переотправить запросы
	void resend();
	// обработать получение исторических сообщений по сети
	void remote_messages_received(bool finish_upload_history_messages);

signals:
	void local_chat_history_messages_loaded(const Chat* chat, const std::vector<DescChatMessage>& messages);

private:
	// отправить запрос на получение данных
	void send_remote_request();
};

class LoaderChatMessagesManager : public QObject
{
	Q_OBJECT
public:
	

private:
	Database& m_database;
	Connection& m_connection;
	Chats& m_chats;
	using ULoaderChatMessages = std::unique_ptr<LoaderChatMessages>;
	using loader_chats_t = std::map<ChatId, ULoaderChatMessages>;
	loader_chats_t m_loader_chats;

public:
	LoaderChatMessagesManager(Database& database, Connection& connection, Chats& chats);
	// запрос сообщений
	bool request_messages(const Chat* chat);
	// переотправить зависшие запросы на получение исторических сообщений
	void resend();
	// обработать получение исторических сообщений по сети
	void remote_messages_received(const Chat* chat, bool finish_upload_history_messages);

signals:
	void local_chat_history_messages_loaded(const Chat* chat, const std::vector<DescChatMessage>& messages);
};