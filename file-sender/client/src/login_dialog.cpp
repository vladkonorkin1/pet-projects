#include "base/precomp.h"
#include "login_dialog.h"
#include "base/utility.h"
#include "ui_login_dialog.h"
#include <QKeyEvent>

LoginDialog::LoginDialog(QWidget* parent)
: QDialog(parent)
, ui(new Ui::LoginDialog)
, is_login_clicked(false)
{
	ui->setupUi(this);
	connect(ui->button_login,  SIGNAL(clicked()), SLOT(slot_button_login_clicked()));
	connect(ui->button_cancel, SIGNAL(clicked()), SLOT(slot_button_cancel_clicked()));

	//setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint & ~Qt::WindowCloseButtonHint);
	setModal(true);
}

LoginDialog::~LoginDialog()
{
	delete ui;
}

void LoginDialog::set_login_name(const QString& login_name)
{
	ui->lineEdit_login->setText(login_name);
}

void LoginDialog::show_default()
{
	is_login_clicked = false;
	ui->label_info->setText(QString());

	ui->lineEdit_password->setFocus();
}

void LoginDialog::login_on_startup()
{
	is_login_clicked = true;
	trying_login();
}

void LoginDialog::wrong_password()
{
	is_login_clicked = false;
	ui->label_info->setText(tr("<font color='red'>Wrong password</font>"));
}

void LoginDialog::server_not_available()
{
	is_login_clicked = false;
	ui->label_info->setText(tr("<font color='red'>Server not available</font>"));
}

void LoginDialog::slot_button_login_clicked()
{
	is_login_clicked = true;
	trying_login();
	emit login_clicked(ui->lineEdit_login->text(), ui->lineEdit_password->text());
	ui->lineEdit_password->setText(QString());
}

void LoginDialog::slot_button_cancel_clicked()
{
	emit cancel_clicked();
}

void LoginDialog::closeEvent(QCloseEvent* e)
{
	emit closed();
}

void LoginDialog::keyPressEvent(QKeyEvent* e)
{
	int key = e->key();
	switch (key)
	{
		case Qt::Key_Return:
		case Qt::Key_Enter:
			slot_button_login_clicked();
			e->accept();
			break;
		case Qt::Key_Escape:
			slot_button_cancel_clicked();
			e->accept();
			break;
		default:
			break;
	}
}

void LoginDialog::trying_login()
{
	ui->label_info->setText(tr("Checking login and password..."));
}