#pragma once
#include <QDialog>

namespace Ui {
	class LoginDialog;
}

class LoginDialog :	public QDialog
{
	Q_OBJECT
private:
	Ui::LoginDialog* ui;
	bool is_login_clicked;

public:
	LoginDialog(QWidget* parent = nullptr);
	virtual ~LoginDialog();
	void show_default();
	void login_on_startup();
	void set_login_name(const QString& login_name);

signals:
	void login_clicked(const QString& login, const QString& password);
	void cancel_clicked();
	void closed();

public slots:
	void wrong_password();
	void server_not_available();

private slots:
	void slot_button_login_clicked();
	void slot_button_cancel_clicked();

protected:
	virtual void closeEvent(QCloseEvent* e) override;
	virtual void keyPressEvent(QKeyEvent* e) override;

private:
	void trying_login();
};