//=====================================================================================//
//   Author: open
//   Date:   15.03.2017
//   Делай хорошо!!! Плохо... само получится ))
//=====================================================================================//
#include "application.h"
#include <QApplication>
#include <QMessageBox>
#include <QTranslator>
#include "base/single_run.h"
#include "base/config.h"

Q_DECLARE_METATYPE(FileTransferDesc);

void register_metatypes()
{
	qRegisterMetaType<FileTransferDesc>();
}

//#include <Windows.h>
//#include <DSRole.h>

//#pragma comment(lib, "netapi32.lib")


/*#include <fstream>
#include <bitset>
#include <iostream>
#include <filesystem>
namespace fs = std::experimental::filesystem;

void demo_perms(fs::perms p)
{
	std::cout << ((p & fs::perms::owner_read) != fs::perms::none ? "r" : "-")
		<< ((p & fs::perms::owner_write) != fs::perms::none ? "w" : "-")
		<< ((p & fs::perms::owner_exec) != fs::perms::none ? "x" : "-")
		<< ((p & fs::perms::group_read) != fs::perms::none ? "r" : "-")
		<< ((p & fs::perms::group_write) != fs::perms::none ? "w" : "-")
		<< ((p & fs::perms::group_exec) != fs::perms::none ? "x" : "-")
		<< ((p & fs::perms::others_read) != fs::perms::none ? "r" : "-")
		<< ((p & fs::perms::others_write) != fs::perms::none ? "w" : "-")
		<< ((p & fs::perms::others_exec) != fs::perms::none ? "x" : "-")
		<< '\n';
}*/


int main(int argc, char *argv[])
{
	/*DSROLE_PRIMARY_DOMAIN_INFO_BASIC * info;
	DWORD dw;

	dw = DsRoleGetPrimaryDomainInformation(NULL,
		DsRolePrimaryDomainInfoBasic,
		(PBYTE *)&info);
	if (dw != ERROR_SUCCESS)
	{
		wprintf(L"DsRoleGetPrimaryDomainInformation: %u\n", dw);
		return dw;
	}

	if (info->DomainNameDns == NULL)
	{
		wprintf(L"DomainNameDns is NULL\n");
	}
	else
	{
		wprintf(L"DomainNameDns: %s\n", info->DomainNameDns);
	}*/

	///demo_perms(fs::status("test.txt").permissions());

	QApplication::addLibraryPath(".");

	Q_INIT_RESOURCE(project);
	QApplication qapp(argc, argv);
	qapp.setQuitOnLastWindowClosed(false);

	logs("start");

	//QTranslator translator;
	//if (translator.load(QLocale(), QLatin1String("sima-chat"), QLatin1String("_"), QLatin1String(":/translations")))
	//	qapp.installTranslator(&translator);

	register_metatypes();

	Config config;
	SingleRun single_run;
	bool is_dev = config.value("app_conf/dev_mode", "false") == "true";
	// если включен режим разработчика - отключаем одиночный запуск
	bool is_single_run = !is_dev;
	// если одиночный запуск и приложение уже запущено
	if (is_single_run && single_run.is_run())
		return 0;
	try
	{
		// parse arguments
		QStringList args = qapp.arguments();
		bool show_window = true;
		for (const QString& arg : args)
		{
			if (arg == "-silent")
			{
				show_window = false;
				continue;
			}
		}
		Application app(config, single_run, show_window, is_dev);
		return qapp.exec();
	}
	catch (const std::exception& e)
	{
		QMessageBox::critical(0, "Exception", e.what());
	}
	return -1;
}