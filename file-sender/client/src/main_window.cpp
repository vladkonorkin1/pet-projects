//=====================================================================================//
//   Author: open
//   Date:   15.02.2019
//=====================================================================================//
#include "base/precomp.h"
#include "main_window.h"
#include <QResizeEvent>
#include <QProxyStyle>
#include <QMimeData>
#include <QSettings>
#include <QAction>


class MyProxyStyle : public QProxyStyle
{
public:
	virtual void drawPrimitive(PrimitiveElement element, const QStyleOption* option, QPainter* painter, const QWidget* widget = nullptr) const
	{
		/// do not draw focus rectangle
		if (PE_FrameFocusRect != element)
			QProxyStyle::drawPrimitive(element, option, painter, widget);
	}
};

bool ResizeEventHandler::eventFilter(QObject* obj, QEvent* event)
{
	QResizeEvent* resize_event = dynamic_cast<QResizeEvent*>(event);
	if (resize_event)
	{
		emit resized(resize_event->size());
		return false;
	}
	return QObject::eventFilter(obj, event);
}


MainWindow::MainWindow(const QString& company_name, const QString& app_name)
: m_ui(new Ui::MainWindow())
, m_company_name(company_name)
, m_app_name(app_name)
, m_version(0)
{
	m_ui->setupUi(this);
	MyProxyStyle* style = new MyProxyStyle();
	setStyle(style);
	m_ui->list_favorite_recent->setStyle(style);
	m_ui->list_search->setStyle(style);

	
	connect(&m_tray_icon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), SLOT(slot_tray_icon_activated(QSystemTrayIcon::ActivationReason)));
	connect(&m_tray_icon, SIGNAL(messageClicked()), SLOT(slot_tray_icon_clicked()));
	QWidget* dock_widget_bar = new QWidget();
	dock_widget_bar->setVisible(false);
	m_ui->dockWidget->setTitleBarWidget(dock_widget_bar);

	connect(m_ui->button_send_msg, SIGNAL(clicked()), SIGNAL(need_send_message()));
	connect(m_ui->button_send_file, SIGNAL(clicked()), SIGNAL(need_send_file()));
	connect(m_ui->button_send_folder, SIGNAL(clicked()), SIGNAL(need_send_folder()));
	//connect(m_ui->button_test, SIGNAL(clicked()), SIGNAL(need_test()));

//#ifdef _DEBUG
	QAction* action_login = new QAction(this);
	action_login->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_L));
	connect(action_login, SIGNAL(triggered()), SIGNAL(need_change_login()));
	addAction(action_login);
//#endif
	connect(&m_resize_event_handler, SIGNAL(resized(const QSize&)), SLOT(slot_main_window_resized(const QSize&)));
	m_ui->widget_limit_width800->installEventFilter(&m_resize_event_handler);

	load_layout();
}
MainWindow::~MainWindow()
{
	save_layout();
}
void MainWindow::update_view()
{
	slot_main_window_resized(QSize());
}
void MainWindow::slot_main_window_resized(const QSize&)
{
	QSize sz = m_ui->widget_limit_width800->size();
	emit resized(QSize(sz.width() - 28, sz.height()));
}
void MainWindow::save_layout()
{
	QSettings settings(QSettings::UserScope, m_company_name, m_app_name);
	settings.setValue("version", QVariant::fromValue<int>(m_version));
	settings.setValue("state", saveState());
	settings.setValue("geometry", saveGeometry());
	settings.setValue("splitter_state", m_ui->splitter_messages->saveState());
}
void MainWindow::load_layout()
{
	QSettings settings(QSettings::UserScope, m_company_name, m_app_name);
	bool ok = false;
	int version = settings.value("version").toInt(&ok);
	if (ok && version == m_version)
	{
		restoreState(settings.value("state").toByteArray());
//#ifdef _DEBUG
		restoreGeometry(settings.value("geometry").toByteArray());
//#endif
		m_ui->splitter_messages->restoreState(settings.value("splitter_state").toByteArray());
	}
}
void MainWindow::closeEvent(QCloseEvent* event)
{
	if (isVisible())
	{
		//save_layout();
		event->ignore();
		hide();
	}
	else
		qApp->exit(0);
}
void MainWindow::slot_tray_icon_clicked()
{
	//show_window();
}
void MainWindow::slot_tray_icon_activated(QSystemTrayIcon::ActivationReason reason)
{
	logs("tray icon activated -> show window");
	if (reason == QSystemTrayIcon::Trigger)
		//emit tray_icon_open();
		show_window();
}
void MainWindow::show_window()
{
	setWindowState(this->windowState() & ~Qt::WindowMinimized);
	show();
	raise();
	activateWindow();
}
void MainWindow::set_send_file_button_visible(bool visible)
{
	m_ui->button_send_file->setVisible(visible);
	m_ui->button_send_folder->setVisible(visible);
}
void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
	event->setAccepted(true);
}
void MainWindow::dropEvent(QDropEvent* event)
{
	event->accept();
	emit dropped(event->mimeData()->urls());
}
void MainWindow::set_unreaded_messages(bool unreaded) 
{
	m_tray_icon.set_unreaded_messages(unreaded);
}
void MainWindow::set_connection_status(bool on)
{
	m_tray_icon.set_connection_status(on);
}
void MainWindow::changeEvent(QEvent* event)
{
	if (event->type() == QEvent::ActivationChange && isActiveWindow())
	{
		emit activated();
	}
	QMainWindow::changeEvent(event);
}
QWidget* MainWindow::widget_chat_top()				{ return m_ui->widget_chat_top; }
QBoxLayout* MainWindow::layout_chat_top()			{ return m_ui->layout_chat_top; }
QWidget* MainWindow::content_write_messages()		{ return m_ui->write_messages; }
QBoxLayout* MainWindow::layout_write_messages()		{ return m_ui->horizontalLayout_8; }
QListWidget* MainWindow::list_favorite_recent()		{ return m_ui->list_favorite_recent; }
QLineEdit* MainWindow::lineedit_search_contacts()	{ return m_ui->lineedit_search_contacts; }
QListWidget* MainWindow::list_search()				{ return m_ui->list_search; }
QWidget* MainWindow::search_contacts_container()	{ return m_ui->search_contacts_container; }
QWidget* MainWindow::favorite_recent_container()	{ return m_ui->favorite_recent_container; }