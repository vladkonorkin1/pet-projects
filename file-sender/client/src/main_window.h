//=====================================================================================//
//   Author: open
//   Date:   15.02.2019
//=====================================================================================//
#pragma once
#include "ui_main_window.h"
#include <QUrl>
#include "tray_icon.h"

namespace Ui { class MainWindow; }

class ResizeEventHandler : public QObject
{
	Q_OBJECT
signals:
	void resized(const QSize& size);

protected:
	virtual bool eventFilter(QObject* obj, QEvent* event);
};

class MainWindow : public QMainWindow
{
	Q_OBJECT
private:
	std::auto_ptr<Ui::MainWindow> m_ui;
	ResizeEventHandler m_resize_event_handler;
	QString m_company_name;
	QString m_app_name;
	int m_version;	// версия интерфейса
	TrayIcon m_tray_icon;

public:
	MainWindow(const QString& company_name, const QString& app_name);
	virtual ~MainWindow();

	QWidget* widget_chat_top();
	QBoxLayout* layout_chat_top();
	QWidget* content_write_messages();
	QBoxLayout* layout_write_messages();
	QListWidget* list_favorite_recent();
	QLineEdit* lineedit_search_contacts();
	QListWidget* list_search();
	QWidget* search_contacts_container();
	QWidget* favorite_recent_container();
	void set_send_file_button_visible(bool visible);
	void set_connection_status(bool on);
	void update_view();
	void show_window();
	void set_unreaded_messages(bool readed);

signals:
	void need_send_message();
	void need_send_file();
	void need_send_folder();
	void need_test();
	void need_change_login();
	//void tray_icon_open();

	// window
	void activated();
	void resized(const QSize&);
	void dropped(const QList<QUrl>& urls);

private:
	void load_layout();
	void save_layout();

private slots:
	void slot_tray_icon_clicked();
	void slot_tray_icon_activated(QSystemTrayIcon::ActivationReason reason);
	void slot_main_window_resized(const QSize& size);

protected:
	virtual void dragEnterEvent(QDragEnterEvent *event);
	virtual void dropEvent(QDropEvent* event);
	virtual void closeEvent(QCloseEvent* event);
	virtual void changeEvent(QEvent* event);
};