#include "base/precomp.h"
#include "message_connection.h"
#include <QHostInfo>
#include <QHostAddress>

MessageConnection::MessageConnection()
: m_message_connection(&m_socket)
, m_id(0)
, m_timer(4.f)
, m_alive_time(0.f)
{
	connect(&m_message_connection, SIGNAL(connected()), SLOT(slot_connected()));
	connect(&m_message_connection, SIGNAL(disconnected()), SLOT(slot_disconnect()));
	connect(&m_message_connection, SIGNAL(data_received(const char*, unsigned int)), SLOT(slot_data_recieved(const char*, unsigned int)));
	connect(&m_timer, SIGNAL(updated(float)), SLOT(slot_timer_updated(float)));

	m_message_dispatcher.register_message<ServerMessageCheckProtocolVersionResponse>();
	m_message_dispatcher.register_message<ServerMessageAutorizateResponse>();
	m_message_dispatcher.register_message<ServerMessageInitializationFinish>();
	m_message_dispatcher.register_message<ServerMessageDepartments>();
	m_message_dispatcher.register_message<ServerMessageUsers>();
	m_message_dispatcher.register_message<ServerMessageSetLoginUser>();
	m_message_dispatcher.register_message<ServerMessageContactChat>();
	m_message_dispatcher.register_message<ServerMessageGroupChat>();
	m_message_dispatcher.register_message<ServerMessageGroupChatSetUsers>();
	m_message_dispatcher.register_message<ServerMessageChatList>();
	m_message_dispatcher.register_message<ServerMessageSyncResponse>();
	m_message_dispatcher.register_message<ServerMessageChatAddRecent>();
	m_message_dispatcher.register_message<ServerMessageChatAddFavorite>();
	m_message_dispatcher.register_message<ServerMessageChatRemoveResponse>();
	m_message_dispatcher.register_message<ServerMessageContactChatCreateResponse>();
	m_message_dispatcher.register_message<ServerMessageGroupChatCreateResponse>();
	m_message_dispatcher.register_message<ServerMessageAliveResponse>();
	m_message_dispatcher.register_message<ServerMessageContactStatus>();
	m_message_dispatcher.register_message<ServerMessageUsersStatus>();
	m_message_dispatcher.register_message<ServerMessageChatMessages>();
	m_message_dispatcher.register_message<ServerMessageChatHistoryMessages>();
	m_message_dispatcher.register_message<ServerMessageFileMessageProgress>();
	m_message_dispatcher.register_message<ServerMessageFileMessageCancelResponse>();
	m_message_dispatcher.register_message<ServerMessageFileTransferFinish>();
	m_message_dispatcher.register_message<ServerMessageFileTransferWriteFileError>();
	m_message_dispatcher.register_message<ServerMessageChatMessageRead>();
	m_message_dispatcher.register_message<ServerMessageChatNewMessageReadResponse>();
	m_message_dispatcher.register_message<ServerMessageIsAccessClosedFolderResponse>();
}
void MessageConnection::connect_host(const QString& host_address)
 {
	m_socket.connectToHost(dns_lookup(host_address), MessagesServerPort);
}

QHostAddress MessageConnection::dns_lookup(const QString& host_address) const
{
	QHostInfo info = QHostInfo::fromName(host_address);
	if (!info.addresses().isEmpty())
		return info.addresses().first();
	return QHostAddress();
}

void MessageConnection::set_message_listener(ClientHandlerMessages* handler_messages)
{
	m_message_dispatcher.set_observer(handler_messages);
}
void MessageConnection::slot_connected()
{
	emit connected();
}
void MessageConnection::send_message(const NetMessage& message)
{
	m_message_connection.send_message(message);
}
void MessageConnection::slot_disconnect()
{
	emit disconnected();
}
void MessageConnection::slot_data_recieved(const char* data, unsigned int size)
{
	m_message_dispatcher.process(data, size);
}
void MessageConnection::close()
{
	m_message_connection.close();
}
void MessageConnection::slot_timer_updated(float dt)
{
//#ifndef _DEBUG
	m_alive_time += dt;
	if (m_alive_time > 10.f)
	{
		if (m_socket.isValid())
			close();
		return;
	}
	if (m_socket.isValid())
		send_message(ClientMessageAlive());
//#endif
}

void MessageConnection::set_alive()
{
	m_alive_time = 0.f;
}