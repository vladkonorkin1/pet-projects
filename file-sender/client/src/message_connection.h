#pragma once
#include "network/base_client_connection.h"
#include "network/message_dispatcher.h"
#include "network/handler_messages.h"
#include "base/timer.h"
#include <QHostInfo>
#include <QHostAddress>

class MessageConnection : public QObject
{
	Q_OBJECT
private:
	ConnectionId m_id;
	QTcpSocket m_socket;

	BaseClientConnection m_message_connection;
	MessageDispatcher<ClientHandlerMessages> m_message_dispatcher;
	QString m_host_address;

	Base::Timer m_timer;
	float m_alive_time;

public:
	MessageConnection();
	void connect_host(const QString& host_address);
	void send_message(const NetMessage& message);
	void set_message_listener(ClientHandlerMessages* handler_messages);
	void set_id(ConnectionId id);
	ConnectionId id() const;
	void close();
	void set_alive();

signals:
	void connected();
	void disconnected();

private slots:
	void slot_connected();
	void slot_disconnect();
	void slot_data_recieved(const char* data, unsigned int size);
	void slot_timer_updated(float dt);

private:
	QHostAddress dns_lookup(const QString& host_address) const; //����� ip �� ��������� �����
};


inline void MessageConnection::set_id(ConnectionId id) {
	m_id = id;
}
inline ConnectionId MessageConnection::id() const {
	return m_id;
}