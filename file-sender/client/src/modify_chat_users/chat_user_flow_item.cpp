//=====================================================================================//
//   Author: open
//   Date:   02.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "chat_user_flow_item.h"
#include "user.h"

ChatUserFlowItem::ChatUserFlowItem(QWidget* parent, const User* contact, bool admin)
: QPushButton(parent)
, m_contact(contact)
, m_admin(false)
{
	setMinimumHeight(height());
	setMaximumHeight(height());
	setCursor(QCursor(Qt::ArrowCursor));
	static const int max_chars = 56;
	if (contact->name.size() > max_chars) setText(contact->name.left(max_chars).append("..."));
	else setText(contact->name);
	set_admin(admin);
}
#include <QKeyEvent>
void ChatUserFlowItem::keyPressEvent(QKeyEvent* event)
{
	switch (event->key())
	{
		case Qt::Key_Backspace:
		{
			emit need_delete(true);
			break;
		}
		case Qt::Key_Delete:
		{
			emit need_delete(false);
			break;
		}
	}
	event->accept();
}
void ChatUserFlowItem::mouseDoubleClickEvent(QMouseEvent *event)
{
	if(event->buttons() == Qt::LeftButton)
		emit itemDoubleClicked();
}
void ChatUserFlowItem::set_admin(bool admin)
{
	m_admin = admin;

	QFont font;
	font.setPointSize(9);
	//font.setBold(m_admin);
	setFont(font);

	if (m_admin)
	{
		setStyleSheet("QPushButton{ background-color: #fdfdf4; }");
		//setStyleSheet("QPushButton{ background-color: #8ad1ed; }");
	}
}
#include <QContextMenuEvent>
void ChatUserFlowItem::contextMenuEvent(QContextMenuEvent* event)
{
	emit exec_menu(event->globalPos());
}
int ChatUserFlowItem::height() { return 21; }