//=====================================================================================//
//   Author: open
//   Date:   02.08.2017
//=====================================================================================//
#pragma once
#include <QPushButton>

class QWidget;
class User;

class ChatUserFlowItem : public QPushButton
{
	Q_OBJECT
private:
	const User* m_contact;
	bool m_admin;

public:
	ChatUserFlowItem(QWidget* parent, const User* contact, bool admin);
	const User* contact() const { return m_contact; }
	bool admin() const { return m_admin;  }

	static int height();

signals:
	void need_delete(bool backspace);
	void exec_menu(const QPoint& pos);
	void itemDoubleClicked();

protected:
	virtual void keyPressEvent(QKeyEvent* event);
	virtual void mouseDoubleClickEvent(QMouseEvent *event);
	void contextMenuEvent(QContextMenuEvent* event);

private:
	void set_admin(bool admin);
};