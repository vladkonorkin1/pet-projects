//=====================================================================================//
//   Author: open
//   Date:   01.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "dialog_modify_chat_users.h"
#include "chats_widgets/chat_item_widgets_manager.h"
#include "chats_widgets/chat_item_widget.h"
#include "group_chat_users_actions.h"
#include "chat_user_flow_item.h"
#include "connection.h"
#include <QScrollBar>
#include <QSplitter>

#include "search_users.h"
#include "user.h"
#include "user_info_for_search.h"

const int DialogModifyChatUsers::LOAD_ITEMS_COUNT = 30;


DialogModifyChatUsers::DialogModifyChatUsers(const Users& users, QWidget* parent, ChatItemWidgetsManager* chat_item_widgets_manager)
: QDialog(parent)
, m_ui(new Ui::AddChatUsers())
, m_chat_item_widgets(chat_item_widgets_manager->create_chat_item_widgets())
, m_scroll_snap_bottom(true)
, m_scroll_range(0)
, m_users(users)
, m_search_users(&users, this)
, m_loaded_count(0)
, m_excluded_count(0)
, m_is_search(false)
{
	m_ui->setupUi(this);
	setWindowFlags(Qt::SplashScreen);
	
	m_flow_layout.reset(new FlowLayout(m_ui->flow_content_widget, 3, 4, 4));

	connect(m_ui->button_ok, SIGNAL(clicked()), SLOT(slot_button_ok_pressed()));
	connect(m_ui->button_cancel, SIGNAL(clicked()), SLOT(slot_button_cancel_pressed()));
	connect(m_ui->lineedit, SIGNAL(textChanged(const QString&)), SLOT(slot_text_changed(const QString&)));
	connect(m_ui->lineedit, SIGNAL(need_delete()), SLOT(slot_need_delete_last_contact_flow_item()));
	connect(m_ui->list, SIGNAL(itemActivated(QListWidgetItem*)), SLOT(slot_item_activated(QListWidgetItem*)));
	connect(m_ui->list, SIGNAL(itemClicked(QListWidgetItem*)), SLOT(slot_item_selection_changed(QListWidgetItem*)));
	connect(m_ui->flow_content_widget, SIGNAL(clicked()), SLOT(slot_flow_content_widget_clicked()));
	connect(m_ui->scrollArea->verticalScrollBar(), SIGNAL(rangeChanged(int, int)), SLOT(slot_scroll_change_range(int, int)));
	connect(m_ui->scrollArea->verticalScrollBar(), SIGNAL(valueChanged(int)), SLOT(slot_scroll_changed(int)));
	connect(&m_search_users, SIGNAL(search_finished(std::vector<const User*>*)), SLOT(slot_search_users_finished(std::vector<const User*>*)));
	connect(m_ui->list->verticalScrollBar(), SIGNAL(valueChanged(int)), SLOT(slot_scroll_changed(int)));

	m_ui->splitter->setSizes(QList<int>() << 60 << 300);
	m_select_time.start();
}
void DialogModifyChatUsers::changeEvent(QEvent* e)
{
	if (e->type() == QEvent::ActivationChange && !this->isActiveWindow())
		close();

	QDialog::changeEvent(e);
}
void DialogModifyChatUsers::slot_button_ok_pressed()
{
	if (m_action)
	{
		BaseGroupChatUsersAction::chat_users_t chat_users;
		chat_users.reserve(m_selected.size());
		for (selected_t::iterator it = m_selected.begin(); it != m_selected.end(); ++it)
		{
			ChatUserFlowItem* item = it->get();
			chat_users.push_back(ChatUser{ item->contact(), item->admin() });
		}
		m_action->accept(chat_users);
	}

	close();
}
void DialogModifyChatUsers::slot_button_cancel_pressed()
{
	close();
}
void DialogModifyChatUsers::show(const QPoint& point, UAction action)
{
	m_action = std::move(action);
	
	m_selected.clear();

	QSignalBlocker blocker(m_ui->lineedit);
	m_ui->lineedit->setText(QString());

	m_ui->lineedit->setFocus();
	m_ui->scrollArea->adjustSize();

	m_search_users_result.clear();
	m_items.clear();

	update_list_contact();

	bool is_create = m_action->is_create_group_chat();
	m_ui->button_ok->setText(is_create ? tr("Create group") : tr("Add"));
	m_ui->button_ok->setMinimumWidth(is_create ? 115 : m_ui->button_cancel->minimumWidth());

	m_ui->list->verticalScrollBar()->setValue(0);

	m_search_users.search("");

	move(point);
	QDialog::show();
}
//=====================================================================================//
void DialogModifyChatUsers::slot_item_activated(QListWidgetItem* item)
{
	slot_item_selection_changed(item);
}
void DialogModifyChatUsers::slot_item_selection_changed(QListWidgetItem* item)
{
	if (m_select_time.elapsed() > 300)
	{
		m_select_time.restart();

		if (item && !item->isHidden())
		{
			//set_changed(true);
			if (ChatItemWidget* widget = item->data(Qt::UserRole).value<ChatItemWidget*>())
			{
				create_chat_user_flow_item(widget->contact(), false);
				show_contact_list(widget->contact(), false);
				scroll_flow_bottom();
				update_button_ok();
			}
		}
	}
}
bool DialogModifyChatUsers::focus_contact_item(QListWidgetItem* item)
{
	if (item && !item->isHidden())
	{
		if (QWidget* widget = m_ui->list->itemWidget(item))
		{
			widget->setFocus();
			return true;
		}
	}
	return false;
}
void DialogModifyChatUsers::slot_text_changed(const QString& mask)
{
	scroll_flow_bottom();

	m_search_users.search(mask);

	/*m_is_search = mask.size() > 1;
	if (m_is_search)
	{
		m_search_users.search(mask);
	}
	else
	{
		m_search_users_result.clear();
		m_items.clear();
		update_list_contact();
	}*/
}
void DialogModifyChatUsers::slot_search_users_finished(std::vector<const User*>* contacts)
{
	//if (!m_is_search || !isVisible()) return;
	//if (!isVisible()) return;

	m_search_users_result = *contacts;
	m_loaded_count = 0;
	m_excluded_count = 0;
	m_items.clear();
	update_list_contact();
	load_new_items_portions();
}
void DialogModifyChatUsers::update_list_contact()
{
	update_button_ok();
	show_list_contacts(true);
	exclude_selected_list_contacts();
}
void DialogModifyChatUsers::show_list_contacts(bool show)
{
	for (int i = 0; i < m_ui->list->count(); ++i)
		m_ui->list->item(i)->setHidden(!show);
}
void DialogModifyChatUsers::exclude_selected_list_contacts()
{
	// исключаем новых добавленных пользователей
	for (const auto& it : m_selected)
		show_contact_list(it->contact(), false);

	// исключим текущих участников чата
	for (auto& it : m_action->get_chat_users())
		show_contact_list(it.second->user, false);
}
void DialogModifyChatUsers::show_contact_list(const User* contact, bool show)
{
	items_t::iterator it = m_items.find(contact->id);
	if (it != m_items.end())
	{
		it->second.item->setHidden(!show);
		//it->second.widget->setVisible(show);
		//it->second.widget->setFixedHeight(show ? 40 : 8);
		//it->second.item->setSizeHint(QSize(100, show ? 40 : 8));
		//it->second.item->setFlags(show ? Qt::ItemIsEnabled | Qt::ItemIsSelectable : Qt::ItemIsEnabled);
	}
}
void DialogModifyChatUsers::create_contact_item_widget(const User* user)
{
	ChatItemWidget* widget = m_chat_item_widgets->create_chat_item_widget(user, false);
	UItem item(new ChatListItem());
	item->setSizeHint(QSize(item->sizeHint().width(), widget->height()));
	item->setData(Qt::UserRole, QVariant::fromValue<ChatItemWidget*>(widget));

	m_ui->list->addItem(item.get());
	m_ui->list->setItemWidget(item.get(), widget);
	m_items[user->id] = ChatItem{ std::move(item), widget };
}
void DialogModifyChatUsers::slot_flow_content_widget_clicked()
{
	m_ui->lineedit->setFocus();
	scroll_flow_bottom();
}
ChatUserFlowItem* DialogModifyChatUsers::create_chat_user_flow_item(const User* contact, bool admin)
{
	UChatUserFlowItem item(new ChatUserFlowItem(m_ui->flow_content_widget, contact, admin));
	ChatUserFlowItem* pitem = item.get();
	connect(pitem, &ChatUserFlowItem::need_delete, [this, pitem](bool backspace) { slot_delete_chat_user_flow_item(pitem, backspace); });
	m_flow_layout->addWidget(pitem);
	m_selected.push_back(std::move(item));
	return pitem;
}
void DialogModifyChatUsers::slot_delete_chat_user_flow_item(ChatUserFlowItem* item, bool backspace)
{
	delete_chat_user_flow_item(item, backspace);
}
void DialogModifyChatUsers::slot_need_delete_last_contact_flow_item()
{
	if (!m_selected.empty()) delete_chat_user_flow_item((--m_selected.end())->get(), true);
}
void DialogModifyChatUsers::delete_chat_user_flow_item(ChatUserFlowItem* item, bool backspace)
{
	//set_changed(true);
	for (selected_t::iterator it_sel = m_selected.begin(); it_sel != m_selected.end(); ++it_sel)
	{
		if (it_sel->get() == item)
		{
			// какой flow-элемент выделить после удаления?
			selected_t::iterator it_previous = it_sel;
			selected_t::iterator it_next = it_sel;
			++it_next;

			if (backspace)
			{
				if (it_previous != m_selected.begin())
					(--it_previous)->get()->setFocus();
				else if (it_next != m_selected.end())
					it_next->get()->setFocus();
				else {
					m_ui->lineedit->setFocus();
				}
			}
			else
			{
				if (it_next != m_selected.end())
					it_next->get()->setFocus();
				else if (it_previous != m_selected.begin())
					(--it_previous)->get()->setFocus();
				else
					m_ui->lineedit->setFocus();
			}

			// удаляем
			m_selected.erase(it_sel);

			// обновляем список контактов
			update_list_contact();
			break;
		}
	}
}
void DialogModifyChatUsers::slot_scroll_changed(int value)
{
	// включаем привязку к низу
	int limit = static_cast<int>(static_cast<float>(m_ui->list->verticalScrollBar()->maximum()) * 0.95f);
	if (value > limit)
		load_new_items_portions();
}
void DialogModifyChatUsers::slot_scroll_change_range(int min, int max)
{
	m_scroll_range = max - min;
	if (m_scroll_snap_bottom)
		m_ui->scrollArea->verticalScrollBar()->setValue(max);
}

void DialogModifyChatUsers::scroll_flow_bottom()
{
	m_scroll_snap_bottom = true;
	m_ui->scrollArea->verticalScrollBar()->setValue(m_scroll_range);
}
void DialogModifyChatUsers::update_button_ok()
{
	m_ui->button_ok->setEnabled(!m_selected.empty());
}

void DialogModifyChatUsers::load_new_items_portions()
{
	std::vector<const User*> created_users;

	const unsigned int count = (m_loaded_count == 0) ? (LOAD_ITEMS_COUNT * 2) : LOAD_ITEMS_COUNT;
	int loaded_in_this_portion = 0;
	for (size_t i = m_loaded_count + m_excluded_count; i < m_loaded_count + count + m_excluded_count; i++)
	{
		if (i >= m_search_users_result.size())
			break;

		const User* user = m_search_users_result[i];
		create_contact_item_widget(user);
		loaded_in_this_portion++;

		// исключаем новых добавленных пользователей
		if (is_exclude_from_selected(user) || is_exclude_from_chat_users(user))
		{
			m_excluded_count++;
			show_contact_list(user, false);
		}
		created_users.push_back(user);
	}
	m_loaded_count += loaded_in_this_portion;

	// запросить онлайн статус найденных пользователей
	if (!created_users.empty())
		emit request_users_online_statuses(created_users);
}
// нужно ли скрыть из списка выделенных
bool DialogModifyChatUsers::is_exclude_from_selected(const User* user) const
{
	for (const auto& it : m_selected)
	{
		if (it->contact() == user)
			return true;
	}
	return false;
}
// нужно ли скрыть из списка пользователей чата
bool DialogModifyChatUsers::is_exclude_from_chat_users(const User* user) const
{
	const auto& chat_users = m_action->get_chat_users();
	return chat_users.end() != chat_users.find(user->id);
}