//=====================================================================================//
//   Author: open
//   Date:   01.08.2017
//=====================================================================================//
#pragma once
#include "ui_modify_chat_users.h"
#include "chats_widgets/chat_item_widgets.h"
#include "common_gui/flow_layout.h"
#include "search_users.h"
#include <QListWidgetItem>
#include <QWidget>
#include <QTime>

namespace Ui { class DialogModifyChatUsers; }
class BaseGroupChatUsersAction;
class ChatItemWidgetsManager;
class ChatUserFlowItem;
class User;

class DialogModifyChatUsers : public QDialog
{
	Q_OBJECT
public:
	using contacts_t = std::vector<const User*>;
	using UAction = std::unique_ptr<BaseGroupChatUsersAction>;

private:
	static const int LOAD_ITEMS_COUNT;
	std::auto_ptr<Ui::AddChatUsers> m_ui;
	std::unique_ptr<FlowLayout> m_flow_layout;

	using UChatUserFlowItem = std::unique_ptr<ChatUserFlowItem>;
	using selected_t = std::vector<UChatUserFlowItem>;
	selected_t m_selected;

	using UItem = std::unique_ptr<QListWidgetItem>;
	struct ChatItem
	{
		UItem item;
		ChatItemWidget* widget;
	};
	using items_t = std::map<UserId, ChatItem>;
	items_t m_items;

	UChatItemWidgets m_chat_item_widgets;
	UAction m_action;

	bool m_scroll_snap_bottom;		// привязка "к низу" (скроллинг в конец)
	int m_scroll_range;				// тот размер, который не влазит в qscrollarea

	QTime m_select_time;			// время выбора контакта

	bool m_is_search;
	const Users& m_users;
	SearchUsers m_search_users;
	std::vector<const User*> m_search_users_result;
	unsigned int m_loaded_count;
	unsigned int m_excluded_count;

public:
	DialogModifyChatUsers(const Users& users, QWidget* parent, ChatItemWidgetsManager* chat_item_widgets_manager);
	void show(const QPoint& point, UAction action);

signals:
	void request_users_online_statuses(const std::vector<const User*>& users);

public slots:
	void create_contact_item_widget(const User* user);

protected:
	void changeEvent(QEvent* e);

private slots:
	void slot_button_ok_pressed();
	void slot_button_cancel_pressed();
	void slot_text_changed(const QString& mask);
	void slot_flow_content_widget_clicked();
	void slot_item_activated(QListWidgetItem* item);
	void slot_item_selection_changed(QListWidgetItem* item);
	void slot_need_delete_last_contact_flow_item();
	void slot_scroll_changed(int value);
	void slot_scroll_change_range(int min, int max);
	void slot_search_users_finished(std::vector<const User*>* contacts);

private: // slots
	void slot_delete_chat_user_flow_item(ChatUserFlowItem* item, bool backspace);

private:
	void show_list_contacts(bool show);
	void exclude_selected_list_contacts();
	void update_list_contact();
	ChatUserFlowItem* create_chat_user_flow_item(const User* contact, bool admin);
	void scroll_flow_bottom();
	void delete_chat_user_flow_item(ChatUserFlowItem* item, bool backspace);
	void show_contact_list(const User* user, bool show);
	bool focus_contact_item(QListWidgetItem* item);
	void update_button_ok();
	void load_new_items_portions();
	// нужно ли скрыть из списка выделенных
	bool is_exclude_from_selected(const User* user) const;
	// нужно ли скрыть из списка пользователей чата
	bool is_exclude_from_chat_users(const User* user) const;
};