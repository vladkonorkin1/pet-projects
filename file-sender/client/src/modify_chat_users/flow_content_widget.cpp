//=====================================================================================//
//   Author: open
//   Date:   01.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "flow_content_widget.h"

FlowContentWidget::FlowContentWidget(QWidget* parent)
: QWidget(parent)
{
}
void FlowContentWidget::mousePressEvent(QMouseEvent*)
{
	emit clicked();
}


/*ScrollAreaFlowContentWidget::ScrollAreaFlowContentWidget(QWidget* parent)
: QWidget(parent)
{
}
void ScrollAreaFlowContentWidget::mousePressEvent(QMouseEvent* event)
{
	emit clicked();
}*/


ModifyChatUsersFlowLineEdit::ModifyChatUsersFlowLineEdit(QWidget* parent)
: QLineEdit(parent)
{
}
#include <QKeyEvent>
void ModifyChatUsersFlowLineEdit::keyPressEvent(QKeyEvent* event)
{
	switch (event->key())
	{
		case Qt::Key_Backspace:
		{
			if (text().isEmpty())
				emit need_delete();
			break;
		}
	}
	QLineEdit::keyPressEvent(event);
}