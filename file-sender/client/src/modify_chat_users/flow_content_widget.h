//=====================================================================================//
//   Author: open
//   Date:   01.08.2017
//=====================================================================================//
#pragma once
#include <QWidget>
#include <QLineEdit>

class FlowContentWidget : public QWidget
{
	Q_OBJECT
public:
	FlowContentWidget(QWidget* parent = nullptr);

signals:
	void clicked();

protected:
	virtual void mousePressEvent(QMouseEvent* event);
};

/*class ScrollAreaFlowContentWidget : public QWidget
{
	Q_OBJECT
public:
	ScrollAreaFlowContentWidget(QWidget* parent = nullptr);

signals:
	void clicked();

protected:
	virtual void mousePressEvent(QMouseEvent* event);
};*/

class ModifyChatUsersFlowLineEdit : public QLineEdit
{
	Q_OBJECT
public:
	ModifyChatUsersFlowLineEdit(QWidget* parent = nullptr);

signals:
	void need_delete();

protected:
	virtual void keyPressEvent(QKeyEvent* event);
};