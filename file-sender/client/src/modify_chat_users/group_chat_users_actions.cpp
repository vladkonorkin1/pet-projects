//=====================================================================================//
//   Author: open
//   Date:   02.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "group_chat_users_actions.h"
#include "connection.h"

CreateGroupChatAction::CreateGroupChatAction(Connection* connection, const User* contact)
: m_connection(connection)
, m_contact(contact)
{
}
const Chat::users_t& CreateGroupChatAction::get_chat_users() const
{
	static Chat::users_t temp;
	return temp;
}
void CreateGroupChatAction::accept(const chat_users_t& chat_users)
{
	if (!chat_users.empty())
	{
		ClientMessageGroupChatCreate msg;
		msg.user_ids.push_back(m_connection->login_user()->id);
		msg.user_ids.push_back(m_contact->id);
		for (auto& chat_user : chat_users)
			msg.user_ids.push_back(chat_user.user->id);
		m_connection->send_message(msg);
	}
}
bool CreateGroupChatAction::is_create_group_chat() const { return true; }


ModifyGroupChatUsersAction::ModifyGroupChatUsersAction(Connection* connection, const Chat* chat)
: m_connection(connection)
, m_chat(chat)
{
}
const Chat::users_t& ModifyGroupChatUsersAction::get_chat_users() const
{
	return m_chat->users();
}
void ModifyGroupChatUsersAction::accept(const chat_users_t& chat_users)
{
	std::vector<UserId> new_users;
	for (auto& chat_user : chat_users)
		new_users.push_back(chat_user.user->id);

	if (!new_users.empty()) m_connection->send_message(ClientMessageGroupChatAddUsers(m_chat->id(), new_users));
}
bool ModifyGroupChatUsersAction::is_create_group_chat() const { return false; }