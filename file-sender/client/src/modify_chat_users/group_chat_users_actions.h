//=====================================================================================//
//   Author: open
//   Date:   02.08.2017
//=====================================================================================//
#pragma once
#include "chat.h"

class Connection;
class User;

class BaseGroupChatUsersAction
{
public:
	using chat_users_t = std::vector<ChatUser>;
	virtual ~BaseGroupChatUsersAction() {}
	virtual void accept(const chat_users_t& chat_users) = 0;
	virtual const Chat::users_t& get_chat_users() const = 0;
	virtual bool is_create_group_chat() const = 0;
};


class CreateGroupChatAction : public BaseGroupChatUsersAction
{
private:
	Connection* m_connection;
	const User* m_contact;

public:
	CreateGroupChatAction(Connection* connection, const User* contact);
	virtual void accept(const chat_users_t& chat_users);
	virtual const Chat::users_t& get_chat_users() const;
	virtual bool is_create_group_chat() const;
};
using UCreateGroupChatAction = std::unique_ptr<CreateGroupChatAction>;


class ModifyGroupChatUsersAction : public BaseGroupChatUsersAction
{
private:
	Connection* m_connection;
	const Chat* m_chat;

public:
	ModifyGroupChatUsersAction(Connection* connection, const Chat* chat);
	virtual void accept(const chat_users_t& chat_users);
	virtual const Chat::users_t& get_chat_users() const;
	virtual bool is_create_group_chat() const;
};
using UModifyGroupChatUsersAction = std::unique_ptr<ModifyGroupChatUsersAction>;