//=====================================================================================//
//   Author: open
//   Date:   02.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "modify_chat_users.h"
#include "chats_widgets/current_chat.h"
#include "group_chat_users_actions.h"
#include "connection.h"

ModifyChatUsers::ModifyChatUsers(Users& users, const CurrentChat* current_chat, Connection* connection, QWidget* parent, ChatItemWidgetsManager* chat_item_widgets_manager)
: m_current_chat(current_chat)
, m_connection(connection)
, m_users(users)
, m_dialog_modify_chat_users(new DialogModifyChatUsers(users, parent, chat_item_widgets_manager))
{
	connect(m_dialog_modify_chat_users.get(), SIGNAL(request_users_online_statuses(const std::vector<const User*>&)), SIGNAL(request_users_online_statuses(const std::vector<const User*>&)));
}
void ModifyChatUsers::show_dialog(const QPoint& point)
{
	QPoint coord(point.x() - m_dialog_modify_chat_users->width(), point.y() + 4);
	/*if (const Contact* contact = m_current_chat->contact())
	{
		show_create_group_chat(coord, contact);
	}
	else */
	if (const Chat* chat = m_current_chat->chat())
	{
		if (const User* contact = chat->contact())
			show_create_group_chat(coord, contact);
		else
			show_modify_group_chat(coord, chat);
	}
}
void ModifyChatUsers::show_create_group_chat(const QPoint& point, const User* contact)
{
	UCreateGroupChatAction action(new CreateGroupChatAction(m_connection, contact));
	m_dialog_modify_chat_users->show(point, std::move(action));
}
void ModifyChatUsers::show_modify_group_chat(const QPoint& point, const Chat* chat)
{
	UModifyGroupChatUsersAction action(new ModifyGroupChatUsersAction(m_connection, chat));
	m_dialog_modify_chat_users->show(point, std::move(action));
}