//=====================================================================================//
//   Author: open
//   Date:   02.08.2017
//=====================================================================================//
#pragma once
#include "modify_chat_users/dialog_modify_chat_users.h"
#include "users.h"

class CurrentChat;
class Connection;

class ModifyChatUsers : public QObject
{
	Q_OBJECT
public:
	using users_t = std::vector<const User*>;

private:
	const CurrentChat* m_current_chat;
	Connection* m_connection;
	Users& m_users;
	std::unique_ptr<DialogModifyChatUsers> m_dialog_modify_chat_users;

public:
	ModifyChatUsers(Users& users, const CurrentChat* current_chat, Connection* connection, QWidget* parent, ChatItemWidgetsManager* chat_item_widgets_manager);

signals:
	void request_users_online_statuses(const std::vector<const User*>& users);

public slots:
	void show_dialog(const QPoint& point);

private:
	void show_create_group_chat(const QPoint& point, const User* contact);
	void show_modify_group_chat(const QPoint& point, const Chat* chat);
};