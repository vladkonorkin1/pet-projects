//=====================================================================================//
//   Author: open
//   Date:   22.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "notification_message.h"
#include "notification_message_widget.h"
#include "chat.h"
#include <QWindow>
#include <QScreen>
#include <QDesktopWidget>

NotificationMessage::NotificationMessage(QWidget* parent)
: m_ui(create_ui())
, m_parent(parent)
, m_shooth_show(this)
, m_shooth_expand(this)
{
	setWindowFlags(Qt::ToolTip | Qt::WindowStaysOnTopHint);
	setAttribute(Qt::WA_ShowWithoutActivating);
	connect(m_ui->button_close, SIGNAL(clicked()), SLOT(slot_button_close()));
	connect(m_ui->widget_top, SIGNAL(pressed()), SLOT(slot_edge_pressed()));
	connect(m_ui->widget_bottom, SIGNAL(pressed()), SLOT(slot_edge_pressed()));
	//connect(m_ui->list_widget, SIGNAL(itemPressed(QListWidgetItem*)), SLOT(slot_list_widget_item_pressed(QListWidgetItem*)));
	connect(m_ui->list_widget, SIGNAL(itemClicked(QListWidgetItem*)), SLOT(slot_list_widget_item_clicked(QListWidgetItem*)));
}
NotificationMessage::UNotificationMessage NotificationMessage::create_ui()
{
	UNotificationMessage ui(new Ui::NotificationMessage());
	ui->setupUi(this);
	return ui;
}
void NotificationMessage::show_right_corner(int off_y)
{
	{
		QRect rect = QApplication::desktop()->screenGeometry(-1);
		//QRect rect = QApplication::desktop()->availableGeometry();
		m_widget_size = m_widget_size + off_y;
		QPoint pos(rect.x() + rect.width() - size().width() - 5, rect.y() + rect.height() - m_widget_size - 70);
		move(pos);
		if (isHidden())
			m_shooth_show.show(true);
	}
}

void NotificationMessage::show_notification(const Chat* chat, const QString& text)
{
	if (!m_parent->isHidden() && m_parent->isActiveWindow())
		return;

	QApplication::alert(m_parent, 0);

	if (!m_hide_timer.get())
		m_items.clear();
	items_t::iterator it = m_items.find(chat->id());
	if (it == m_items.end())
	{
		NotificationMessageWidget* widget = new NotificationMessageWidget(chat);
		UItem item(new QListWidgetItem());
		
		item->setSizeHint(QSize(item->sizeHint().width(), widget->height()));
		m_ui->list_widget->insertItem(m_ui->list_widget->count(), item.get());
		m_ui->list_widget->setItemWidget(item.get(), widget);
		
		item->setData(Qt::UserRole, QVariant::fromValue<NotificationMessageWidget*>(widget));
		
		it = m_items.insert(std::make_pair(chat->id(), WidgetItem{widget, std::move(item)})).first;
		show_right_corner(widget->height());
	}
	m_shooth_expand.expand(static_cast<int>(m_items.size()));

	m_hide_timer.reset(new Base::Timer(1.f / 60.f));
	//connect(m_hide_timer.get(), SIGNAL(updated(float)), SLOT(slot_hide_timer(float)));
	m_hide_time = 0.f;
	it->second.widget->set_text(text);
}
void NotificationMessage::slot_button_close()
{
	hide_notification();

}
void NotificationMessage::hide_notification()
{
	if (m_hide_timer.get())
	{
		m_hide_timer.reset();
		m_shooth_show.show(false);
	}
	m_widget_size = 0;
}
#include <QCloseEvent>
void NotificationMessage::closeEvent(QCloseEvent* event)
{
	hide_notification();
	event->ignore();
}
void NotificationMessage::slot_hide_timer(float dt)
{
	bool over = geometry().contains(QCursor::pos());
	if (over) m_hide_time = 0;

	if (!over)
	{
		m_hide_time += dt;
		if (m_hide_time > 10.f)	// по истечении времени -> скрываем
			hide_notification();
	}
}
void NotificationMessage::slot_edge_pressed()
{
	activate_main_window();
}
void NotificationMessage::activate_main_window()
{
	m_parent->showNormal();
	m_parent->activateWindow();
	hide_notification();
}
/*void NotificationMessage::slot_list_widget_item_pressed(QListWidgetItem* item)
{
	m_list_item_pressed = true;
}*/
void NotificationMessage::slot_list_widget_item_clicked(QListWidgetItem* item)
{
	activate_main_window();
	if (NotificationMessageWidget* widget = item->data(Qt::UserRole).value<NotificationMessageWidget*>())
		emit activate_chat(widget->chat());
}