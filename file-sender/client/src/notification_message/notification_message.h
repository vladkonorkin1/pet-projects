//=====================================================================================//
//   Author: open
//   Date:   22.08.2017
//=====================================================================================//
#pragma once
#include "ui_notification_message.h"
#include "notification_message/notification_message_smooth_show.h"
#include "notification_message/notification_message_smooth_expand.h"

namespace Ui { class NotificationMessage; }
class NotificationMessageWidget;
class Chat;

class NotificationMessage : public QWidget
{
	Q_OBJECT
private:
	using UNotificationMessage = std::auto_ptr<Ui::NotificationMessage>;
	UNotificationMessage m_ui;
	QWidget* m_parent;

	using UItem = std::unique_ptr<QListWidgetItem>;
	struct WidgetItem
	{
		NotificationMessageWidget* widget;
		UItem item;
	};
	using items_t = std::map<ChatId, WidgetItem>;
	items_t m_items;
	NotificationMessageSmoothShow m_shooth_show;
	NotificationMessageSmoothExpand m_shooth_expand;

	std::unique_ptr<Base::Timer> m_hide_timer;
	float m_hide_time;
	int m_widget_size = 0;

public:
	NotificationMessage(QWidget* parent);
	void show_notification(const Chat* chat, const QString& text);
	void hide_notification();

signals:
	void activate_chat(const Chat* chat);

private slots:
	void slot_button_close();
	void slot_hide_timer(float dt);
	void slot_edge_pressed();
	//void slot_list_widget_item_pressed(QListWidgetItem* item);
	void slot_list_widget_item_clicked(QListWidgetItem* item);

protected:
	void closeEvent(QCloseEvent* event);

private:
	UNotificationMessage create_ui();
	void show_right_corner(int off_y);
	void activate_main_window();
};