//=====================================================================================//
//   Author: open
//   Date:   23.08.2017
//=====================================================================================//
#pragma once
#include <QToolButton>

class NotificationMessageButtonClose : public QToolButton
{
	Q_OBJECT
private:

public:
	NotificationMessageButtonClose(QWidget* parent);

//signals:
	//void over(bool over);

protected:
	//void enterEvent(QEvent* event);
	//void leaveEvent(QEvent* event);
};