//=====================================================================================//
//   Author: open
//   Date:   22.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "notification_message_edge.h"
#include <QMouseEvent>

NotificationMessageEdge::NotificationMessageEdge(QWidget* parent)
: QWidget(parent)
, m_pressed(false)
{
}
void NotificationMessageEdge::mousePressEvent(QMouseEvent* event)
{
	Q_UNUSED( event );
	m_pressed = true;
}
void NotificationMessageEdge::mouseReleaseEvent(QMouseEvent* event)
{
	if (m_pressed && rect().contains(event->pos()))
		emit pressed();

	m_pressed = false;
}