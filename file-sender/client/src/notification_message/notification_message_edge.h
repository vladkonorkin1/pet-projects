//=====================================================================================//
//   Author: open
//   Date:   23.08.2017
//=====================================================================================//
#pragma once
#include <QWidget>

class NotificationMessageEdge : public QWidget
{
	Q_OBJECT
private:
	bool m_pressed;

public:
	NotificationMessageEdge(QWidget* parent);

signals:
	void pressed();

protected:
	virtual void mousePressEvent(QMouseEvent* event);
	virtual void mouseReleaseEvent(QMouseEvent* event);
};