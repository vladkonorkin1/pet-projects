//=====================================================================================//
//   Author: open
//   Date:   23.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "notification_message_smooth_expand.h"
#include <QWidget>

float NotificationMessageSmoothExpand::s_duration = 0.15f;

NotificationMessageSmoothExpand::NotificationMessageSmoothExpand(QWidget* widget)
: m_widget(widget)
{
}
void NotificationMessageSmoothExpand::expand(int num)
{
	if (num == 1) reset();
	else grow(num);
}
void NotificationMessageSmoothExpand::grow(int num)
{
	m_expand_height = get_widget_height(num);

	if (!m_expand_timer.get())
	{
		m_expand_timer.reset(new Base::Timer(1.f/60.f));
		connect(m_expand_timer.get(), SIGNAL(updated(float)), SLOT(slot_expand_timer(float)));
	}
}
void NotificationMessageSmoothExpand::slot_expand_timer(float dt)
{
	int new_height = m_widget->size().height() + 48 * dt*(1.f / s_duration);
	if (new_height >= m_expand_height)
	{
		new_height = m_expand_height;
		m_expand_timer.reset();
	}
	set_widget_height(new_height);
}
void NotificationMessageSmoothExpand::reset()
{
	m_expand_timer.reset();
	set_widget_height(get_widget_height(1));
}
int NotificationMessageSmoothExpand::get_widget_height(int num) const
{
	return 30 + 48 * num;
}
void NotificationMessageSmoothExpand::set_widget_height(int height)
{
	m_widget->setFixedSize(238, height);
}