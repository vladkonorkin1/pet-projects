//=====================================================================================//
//   Author: open
//   Date:   23.08.2017
//=====================================================================================//
#pragma once
#include "base/timer.h"

class NotificationMessageSmoothExpand : public QObject
{
	Q_OBJECT
private:
	std::unique_ptr<Base::Timer> m_expand_timer;
	float m_expand_height;	// высота виджета? к которой стремится расшириться
	float m_expand_time;
	QWidget* m_widget;
	static float s_duration;

public:
	NotificationMessageSmoothExpand(QWidget* widget);
	void expand(int num);

private slots:
	void slot_expand_timer(float dt);

private:
	void reset();
	void grow(int num);
	int get_widget_height(int num) const;
	void set_widget_height(int height);
};