//=====================================================================================//
//   Author: open
//   Date:   23.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "notification_message_smooth_show.h"
#include <QWidget>

float NotificationMessageSmoothShow::s_duration_show = 0.18f;
float NotificationMessageSmoothShow::s_duration_hide = 0.25f;

NotificationMessageSmoothShow::NotificationMessageSmoothShow(QWidget* widget)
: m_widget(widget)
{
}
void NotificationMessageSmoothShow::show(bool show)
{
	//m_widget->show();
	m_widget->setVisible(true);

	m_duration = show ? s_duration_show : s_duration_hide;
	m_end_opacity = show ? 1.f : 0.f;
	m_time = 0.f;

	if (!m_smooth_show_timer.get())
	{
		m_smooth_show_timer.reset(new Base::Timer(1.f/60.f));
		connect(m_smooth_show_timer.get(), SIGNAL(updated(float)), SLOT(slot_smooth_show_timer(float)));
		
		m_start_opacity = show ? 0.f : 1.f;
		m_widget->setWindowOpacity(m_start_opacity);
	}
	else m_start_opacity = m_widget->windowOpacity();
}
void NotificationMessageSmoothShow::slot_smooth_show_timer(float dt)
{
	m_time += dt;
	float id = m_time / m_duration;

	float opacity = m_start_opacity + (m_end_opacity - m_start_opacity) * id;
	opacity = std::min(std::max(opacity, 0.f), 1.f);
	m_widget->setWindowOpacity(opacity);

	if (m_time > m_duration)
	{
		m_smooth_show_timer.reset();
		if (opacity < 0.001f)
			m_widget->hide();
	}
}