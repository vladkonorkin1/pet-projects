//=====================================================================================//
//   Author: open
//   Date:   23.08.2017
//=====================================================================================//
#pragma once
#include "base/timer.h"

class NotificationMessageSmoothShow : public QObject
{
	Q_OBJECT
private:
	QWidget* m_widget;
	std::unique_ptr<Base::Timer> m_smooth_show_timer;
	float m_time;
	float m_duration;
	static float s_duration_show;
	static float s_duration_hide;
	float m_start_opacity;
	float m_end_opacity;

public:
	NotificationMessageSmoothShow(QWidget* widget);
	void show(bool show);

private slots:
	void slot_smooth_show_timer(float dt);
};