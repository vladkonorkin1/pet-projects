//=====================================================================================//
//   Author: open
//   Date:   22.08.2017
//=====================================================================================//
#include "base/precomp.h"
#include "notification_message_widget.h"
#include "chat.h"

NotificationMessageWidget::NotificationMessageWidget(const Chat* chat)
: m_ui(create_ui())
, m_chat(chat)
{
	m_ui->label_name->setText(chat->name());
	
	//m_ui->button_close->setIcon(QCommonStyle().standardIcon(QStyle::SP_LineEditClearButton));
}
NotificationMessageWidget::UNotificationMessageWidget NotificationMessageWidget::create_ui()
{
	UNotificationMessageWidget ui(new Ui::NotificationMessageWidget());
	ui->setupUi(this);
	return ui;
}
void NotificationMessageWidget::set_text(const QString& text)
{
	m_ui->label_msg->setText(text);
}
void NotificationMessageWidget::enterEvent(QEvent* event)
{
	emit over(true);
	QWidget::enterEvent(event);
}
void NotificationMessageWidget::leaveEvent(QEvent* event)
{
	emit over(false);
	QWidget::leaveEvent(event);
}
const Chat* NotificationMessageWidget::chat() const
{
	return m_chat;
}