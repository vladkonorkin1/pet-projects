//=====================================================================================//
//   Author: open
//   Date:   22.08.2017
//=====================================================================================//
#pragma once
#include "ui_notification_message_widget.h"

namespace Ui { class NotificationMessageWidget; }
class Chat;

class NotificationMessageWidget : public QWidget
{
	Q_OBJECT
private:
	using UNotificationMessageWidget = std::auto_ptr<Ui::NotificationMessageWidget>;
	UNotificationMessageWidget m_ui;
	const Chat* m_chat;

public:
	NotificationMessageWidget(const Chat* chat);
	void set_text(const QString& text);
	const Chat* chat() const;

signals:
	void over(bool over);

protected:
	void enterEvent(QEvent* event);
	void leaveEvent(QEvent* event);
	
private:
	UNotificationMessageWidget create_ui();
};