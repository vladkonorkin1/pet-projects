#include "base/precomp.h"
#include "search_user_worker.h"
#include "user.h"


 bool compare_user(const User* a, const User* b)
{
	return a->name.compare(b->name) < 0;
}


SearchUserWorker::SearchUserWorker(bool* cancel, QObject* parent) : QObject(parent)
, m_cancel(cancel)
{
}

SearchUserWorker::~SearchUserWorker()
{
}

void SearchUserWorker::on_search(const QString what, const std::vector<UserInfoForSearch>& users_info)
{
	m_result.clear();

	QStringList mask_list = what.split(" ", QString::SkipEmptyParts);

	for (const UserInfoForSearch& user_info : users_info)
	{
		if (*m_cancel)
		{
			logs(QString("SearchUserWorker cancelled"));
			emit cancelled();
			return;
		}
		const User* user = user_info.user;
		// игнорируем уволенных
		if (user_info.user->deleted) continue;

		int num_compare = 0;
		for (const QString& mask : mask_list)
		{
			if (user_info.name.contains(mask, Qt::CaseInsensitive))
				num_compare++;
		}

		if (num_compare == mask_list.size())
		{
			m_result.push_back(user);
			continue;
		}

		if (!user_info.login.contains(what, Qt::CaseInsensitive) &&
			!user_info.departement_name.contains(what, Qt::CaseInsensitive))
			continue;

		m_result.push_back(user_info.user);
	}
	std::sort(m_result.begin(), m_result.end(), compare_user);
	//logs(QString("SearchUserWorker m_result.size()=%1").arg(m_result.size()));
	emit search_finished(&m_result);
}