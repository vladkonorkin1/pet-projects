#pragma once
#include "user_info_for_search.h"


class SearchUserWorker : public QObject
{
	Q_OBJECT
private:
	bool* m_cancel;
	std::vector<const User*> m_result;
public:
	explicit SearchUserWorker(bool* cancel, QObject* parent = nullptr);
	virtual ~SearchUserWorker();
signals:
	void cancelled();
	void search_finished(std::vector<const User*>*);
public slots:
	void on_search(const QString what, const std::vector<UserInfoForSearch>& users_info);
	friend class SearchUsers;
};

