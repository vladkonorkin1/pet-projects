#include "base/precomp.h"
#include "search_users.h"
#include "user.h"
#include "user_info_for_search.h"
#include <QEventloop>

SearchUsers::SearchUsers(const Users* users, QObject* parent) : QObject(parent)
, m_users(users)
, m_cancel(false)
, m_running(false)
, worker(&m_cancel, this)
{
	worker.moveToThread(&thread_worker);
	connect(this, SIGNAL(worker_search(const QString&, const std::vector<UserInfoForSearch>&)), &worker, SLOT(on_search(const QString&, const std::vector<UserInfoForSearch>&)));
	connect(&worker, SIGNAL(search_finished(std::vector<const User*>*)), this, SLOT(on_worker_finished(std::vector<const User*>*)));
	thread_worker.start();
}

SearchUsers::~SearchUsers()
{
	thread_worker.quit();
	thread_worker.wait();
}

void SearchUsers::search(const QString& mask)
{
	std::vector<UserInfoForSearch> users_info;
	std::vector<const User*> users = m_users->users();
	for (size_t i = 0; i < users.size(); i++)
	{
		const User* user = users.at(i);
		users_info.push_back(UserInfoForSearch{ user, user->name, user->login, user->department->name });
	}
	if (m_running)
	{
		QEventLoop loop;
		connect(&worker, SIGNAL(cancelled()), &loop, SLOT(quit()));
		connect(&worker, SIGNAL(search_finished(std::vector<const User*>*)), &loop, SLOT(quit()));
		m_cancel = true;
		loop.exec();
		disconnect(&worker, SIGNAL(search_finished(std::vector<const User*>*)), &loop, SLOT(quit()));
	}
	m_cancel = false;
	m_running = true;
	emit worker_search(mask, users_info);
}

void SearchUsers::on_worker_finished(std::vector<const User*>* result)
{
	m_running = false;
	if (m_cancel)
		return;
	emit search_finished(result);
}



