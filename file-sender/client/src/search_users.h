//=====================================================================================//
//   Author: open
//   Date:   06.06.2017
//=====================================================================================//
#pragma once
#include <QThread.h>
#include "search_user_worker.h"
#include "users.h"

class SearchUsers : public QObject
{
	Q_OBJECT
private:
	const Users* m_users;
	QString m_mask;
	bool m_cancel;
	bool m_running;
	SearchUserWorker worker;
	QThread thread_worker;
public:
	SearchUsers(const Users* users, QObject* parent = nullptr);
	virtual ~SearchUsers();
	void search(const QString& mask);
signals:
	void worker_search(const QString& mask, const std::vector<UserInfoForSearch>&);
	void search_finished(std::vector<const User*>*);

private slots:
	void on_worker_finished(std::vector<const User*>* result);

	friend class SearchUserWorker;
};


