#include "base/precomp.h"
#include "sending_messages.h"
#include "database/database.h"
#include "connection.h"
#include "chats.h"

SendingMessages::SendingMessages(Database& database, Chats& chats, Connection& connection)
: m_database(database)
, m_chats(chats)
, m_connection(connection)
, m_sending(false)
{
}
// загрузить неотправленные сообщения из базы
void SendingMessages::load_unsent_messages_from_database()
{
	// загрузили
	std::vector<DescChatMessage> unsent_messages = m_database.read_unsent_messages();

	// переносим в m_messages
	std::transform(unsent_messages.begin(), unsent_messages.end(), std::back_inserter(m_messages), [](const DescChatMessage& msg) { return msg; });

	// добавляем в чаты
	m_chats.add_messages(unsent_messages, false);
}
// добавить в очередь отправки текстовое сообщение
void SendingMessages::add_text_message(const User* user, const Chat* chat, const QString& text)
{
	DescChatMessage msg;
	msg.local_id = QUuid::createUuid();
	msg.chat_id = chat->id();
	msg.sender_id = user->id;
	msg.datetime = QDateTime::currentDateTimeUtc();
	msg.type = ChatMessageType::Text;
	msg.text = text;
	add_message(msg, true);
}
// добавить в очередь файловое сообщение
void SendingMessages::add_file_message(const User* user, const ChatMessageLocalId& message_local_id, ChatId chat_id, const QString& path, bool is_dir, quint64 size)
{
	DescChatMessage msg;
	msg.local_id = message_local_id;
	msg.chat_id = chat_id;
	msg.sender_id = user->id;
	msg.datetime = QDateTime::currentDateTimeUtc();
	msg.type = ChatMessageType::File;
	msg.file_src_path = path;
	msg.file_is_dir = is_dir;
	msg.file_size = size;
	add_message(msg, false);
}
// добавить сообщение в очередь
void SendingMessages::add_message(const DescChatMessage& msg, bool store_database)
{
	// ставим сообщение в очередь отправки
	m_messages.push_back(msg);

	// записываем в базу
	if (store_database)
		m_database.add_unsent_message(msg);

	// отправляем в ui
	std::vector<DescChatMessage> messages;
	messages.push_back(msg);
	m_chats.add_messages(messages, false);

	// отправляем в сеть
	send_next_message();
}
// обработать доставку сообщения на сервер
void SendingMessages::process_message_received_on_server(const ChatMessage* msg)
{
	if (m_messages.empty())
		return;
	if (m_messages.front().local_id != msg->local_id)
		return;
	if (msg->id == 0 || msg->historical || msg->status == ChatMessageStatus::Sending)
		return;
	m_database.delete_unsent_message(msg->local_id);
	m_messages.pop_front();
	emit chat_message_sended(msg);

	// отправляем следующее сообщение
	m_sending = false;
	send_next_message();
}
// переотправить сообщения
void SendingMessages::resend()
{
	m_sending = false;
	send_next_message();
}
// отправить следующее сообщение (отправляем по одному)
void SendingMessages::send_next_message()
{
	if (m_sending)
		return;
	if (m_messages.empty())
		return;
	const DescChatMessage& msg = m_messages.front();

	m_sending = true;
	switch (msg.type)
	{
		case ChatMessageType::Text:
			m_connection.send_message(ClientMessageTextMessageSend(msg.chat_id, msg.local_id, msg.text));
			break;
		case ChatMessageType::File:
			m_connection.send_message(ClientMessageFileMessageSend(
				msg.chat_id,
				msg.local_id,
				msg.file_src_path,
				msg.file_is_dir,
				msg.file_size));
			break;
		default:
			break;
	}
}
