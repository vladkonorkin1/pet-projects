#pragma once

class ChatMessage;
class Connection;
class Database;
class Chats;
class Chat;
class User;

class SendingMessages : public QObject
{
	Q_OBJECT
private:
	Database& m_database;
	Chats& m_chats;
	Connection& m_connection;
	std::deque<DescChatMessage> m_messages;
	bool m_sending;

public:
	SendingMessages(Database& database, Chats& chats, Connection& connection);
	// загрузить неотправленные сообщения из базы
	void load_unsent_messages_from_database();
	// переотправить сообщения
	void resend();
	// добавить в очередь отправки текстовое сообщение
	void add_text_message(const User* user, const Chat* chat, const QString& text);
	// добавить в очередь файловое сообщение
	void add_file_message(const User* user, const ChatMessageLocalId& message_local_id, ChatId chat_id, const QString& path, bool is_dir, quint64 size);
	// обработать доставку сообщения на сервер
	void process_message_received_on_server(const ChatMessage* msg);

signals:
	void chat_message_sended(const ChatMessage* msg);
	
private:
	// отправить следующее сообщение (отправляем по одному)
	void send_next_message();
	// добавить сообщение в очередь
	void add_message(const DescChatMessage& msg, bool store_database);
};