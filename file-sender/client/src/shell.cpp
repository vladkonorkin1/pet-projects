//=====================================================================================//
//   Author: open
//   Date:   15.02.2019
//=====================================================================================//
#include "base/precomp.h"
#include "shell.h"
#include "notification_message/notification_message.h"
#include "file_transfer/file_transfer.h"
#include "file_transfer/file_dialog.h"
#include "base/utility.h"
#include <QMessageBox>
#include <QTimer>

Shell::Shell(MainWindow& main_window, const QString& app_name, const QString& login_name, const QString& database_path, const QString& host)
: m_app_name(app_name) 
, m_main_window(main_window)
, m_login_name(login_name)
, m_thread_task_manager(1)
, m_database(database_path)
, m_users(m_database)
, m_chats(m_database, m_users)
, m_connection(m_database, m_users, m_chats)
, m_login_user(nullptr)
, m_sending_messages(m_database, m_chats, m_connection)
, m_file_transfer_manager(m_thread_task_manager, m_chats, m_sending_messages, host)
, m_chat_item_widgets_manager(m_users, m_chats, &m_main_window)
, m_messages_widget_manager(&m_chats, m_main_window.widget_chat_top(), m_main_window.layout_chat_top(), m_main_window.content_write_messages(), m_main_window.layout_write_messages())
, m_chats_widget(&m_chats, m_main_window.list_favorite_recent(), &m_messages_widget_manager, make_search_germ())	// m_ui->list_favorite_recent
, m_modify_chat_users(m_users, m_chats_widget.current(), &m_connection, &m_main_window, &m_chat_item_widgets_manager)
, m_notification_message(&m_main_window)
, m_check_readed_new_messages(&m_main_window, &m_chats, m_chats_widget.current(), &m_connection, &m_messages_widget_manager)
, m_loader_chat_messages_manager(m_database, m_connection, m_chats)
, m_once_request_access_to_folder(false)
{
	connect(&m_chats, SIGNAL(chat_message_added(Chat*, const ChatMessage*)), SLOT(slot_chat_message_added(Chat*, const ChatMessage*)));
	connect(&m_chats, SIGNAL(start_update_chat_list()), SLOT(slot_start_update_chat_list()));
	connect(&m_chats, SIGNAL(finish_update_chat_list()), SLOT(slot_finish_update_chat_list()));
	connect(&m_chats, SIGNAL(change_count_unreaded_messages(int)), SLOT(slot_change_count_unreaded_messages(int)));

	connect(&m_connection, SIGNAL(file_message_progress(ChatMessageId, const Chat*, unsigned char)), SLOT(slot_file_message_progress(ChatMessageId, const Chat*, unsigned char)));
	connect(&m_connection, SIGNAL(file_message_cancel_confirm(ChatMessageId)), SLOT(slot_file_message_cancel_confirm(ChatMessageId)));
	connect(&m_connection, SIGNAL(file_transfer_write_file_error()), SLOT(slot_file_transfer_write_file_error()));
	connect(&m_connection, SIGNAL(file_transfer_finished(FileTransferResult, const QString&)), SLOT(slot_file_transfer_finished(FileTransferResult, const QString&)));

	connect(&m_chat_item_widgets_manager, SIGNAL(need_chat_remove(const Chat*)), SLOT(slot_need_chat_remove(const Chat*)));
	connect(&m_chat_item_widgets_manager, SIGNAL(need_chat_add_favorite(const Chat*)), SLOT(slot_need_chat_add_favorite(const Chat*)));
	connect(&m_chat_item_widgets_manager, SIGNAL(need_chat_remove_favorite(const Chat*)), SLOT(slot_need_chat_remove_favorite(const Chat*)));
	connect(&m_chat_item_widgets_manager, SIGNAL(need_chat_rename(const Chat*, const QString&)), SLOT(slot_need_chat_rename(const Chat*, const QString&)));
	connect(&m_chat_item_widgets_manager, SIGNAL(need_group_chat_leave(const Chat*)), SLOT(slot_need_group_chat_leave(const Chat*)));

	connect(&m_messages_widget_manager, SIGNAL(new_text_message(const Chat*, const QString&)), SLOT(slot_new_text_message(const Chat*, const QString&)));
	connect(&m_messages_widget_manager, SIGNAL(new_file_message(const QList<QUrl>&)), SLOT(slot_dropped(const QList<QUrl>&)));

	connect(&m_messages_widget_manager, SIGNAL(request_history_messages(const Chat*, ChatMessageId)), SLOT(slot_request_history_messages(const Chat*, ChatMessageId)));
	connect(&m_messages_widget_manager, SIGNAL(need_add_chat_to_chat_list(const User*)), SLOT(slot_need_add_chat_to_chat_list(const User*)));
	connect(&m_messages_widget_manager, SIGNAL(need_group_chat_remove_user(const Chat*, const User*)), SLOT(slot_need_group_chat_remove_user(const Chat*, const User*)));
	connect(&m_messages_widget_manager, SIGNAL(need_group_chat_set_admin(const Chat*, const User*)), SLOT(slot_need_group_chat_set_admin(const Chat*, const User*)));
	connect(&m_messages_widget_manager, SIGNAL(need_group_chat_remove_admin(const Chat*, const User*)), SLOT(slot_need_group_chat_remove_admin(const Chat*, const User*)));
	connect(&m_messages_widget_manager, SIGNAL(need_file_message_cancel(const ChatMessageLocalId&)), SLOT(slot_need_file_message_cancel(const ChatMessageLocalId&)));
	connect(&m_main_window, SIGNAL(resized(const QSize&)), &m_messages_widget_manager, SLOT(resize_scroll_area(const QSize&)));

	connect(&m_connection, SIGNAL(connection_initialized()), SLOT(slot_connection_initialized()));
	connect(&m_connection, SIGNAL(sync_finished()), SLOT(slot_connection_sync_finished()));
	connect(&m_connection, SIGNAL(login_user_updated(const User*)), SLOT(slot_login_user_updated(const User*)));
	connect(&m_connection, SIGNAL(new_chat_messages(const std::vector<DescChatMessage>&)), SLOT(slot_new_chat_messages(const std::vector<DescChatMessage>&)));
	connect(&m_connection, SIGNAL(chat_history_messages_loaded(const Chat*, const std::vector<DescChatMessage>&, bool)), SLOT(slot_remote_chat_history_messages_loaded(const Chat*, const std::vector<DescChatMessage>&, bool)));

	connect(&m_chats_widget, SIGNAL(need_add_chat_to_chat_list(const User*)), SLOT(slot_need_add_chat_to_chat_list(const User*)));
	connect(&m_chats_widget, SIGNAL(request_users_online_statuses(const std::vector<const User*>&)), SLOT(slot_request_users_online_statuses(const std::vector<const User*>&)));
	
	connect(m_chats_widget.current(), SIGNAL(contact_selected(const User*)), SLOT(slot_contact_selected(const User*)));

	connect(m_chats_widget.current(), SIGNAL(chat_selected(const Chat*)), SLOT(slot_chat_selected(const Chat*)));
	connect(&m_connection, SIGNAL(group_chat_created(Chat*)), SLOT(slot_group_chat_created(Chat*)));

	connect(&m_connection, SIGNAL(contact_chat_created(const User*, const Chat*)), SLOT(slot_contact_chat_created(const User*, const Chat*)));
	connect(&m_connection, SIGNAL(message_readed(Chat*, ChatMessageId, const QDateTime&)), SLOT(slot_message_readed(Chat*, ChatMessageId, const QDateTime&)));
	connect(&m_connection, SIGNAL(new_message_readed_response(bool, Chat*, ChatMessageId)), SLOT(slot_new_message_readed_response(bool, Chat*, ChatMessageId)));

	connect(&m_connection, SIGNAL(have_access_closed_folder(bool)), SLOT(slot_have_access_closed_folder(bool)));

	connect(&m_messages_widget_manager, SIGNAL(need_modify_chat_users(const QPoint&)), &m_modify_chat_users, SLOT(show_dialog(const QPoint&)));
	connect(&m_notification_message, SIGNAL(activate_chat(const Chat*)), SLOT(slot_notification_select_chat(const Chat*)));

	connect(&m_loader_chat_messages_manager, SIGNAL(local_chat_history_messages_loaded(const Chat*, const std::vector<DescChatMessage>&)), SLOT(slot_local_chat_history_messages_loaded(const Chat*, const std::vector<DescChatMessage>&)));

	connect(&m_modify_chat_users, SIGNAL(request_users_online_statuses(const std::vector<const User*>&)), SLOT(slot_request_users_online_statuses(const std::vector<const User*>&)));

	connect(&m_main_window, SIGNAL(need_send_message()), SLOT(slot_need_send_message()));
	connect(&m_main_window, SIGNAL(need_send_file()), SLOT(slot_need_send_file()));
	connect(&m_main_window, SIGNAL(need_send_folder()), SLOT(slot_need_send_folder()));
	connect(&m_main_window, SIGNAL(need_test()), SLOT(slot_need_test()));
	connect(&m_main_window, SIGNAL(activated()), SLOT(slot_main_window_activated()));
	connect(&m_main_window, SIGNAL(dropped(const QList<QUrl>&)), SLOT(slot_dropped(const QList<QUrl>&)));
	//connect(this, SIGNAL(non_readed_message(bool)), &m_main_window, SLOT(slot_non_readed_message(bool)));

	connect(this, SIGNAL(show_warning_no_access_closed_folder()), this, SLOT(slot_show_warning_no_access_closed_folder()), Qt::ConnectionType::QueuedConnection);
	connect(this, SIGNAL(show_warning_no_access_closed_folder_another_user(QString)), this, SLOT(slot_show_warning_no_access_closed_folder_another_user(QString)), Qt::ConnectionType::QueuedConnection);

	m_users.load_from_database();
	if (const User* user = m_users.find_user(m_login_name))
	{
		update_login_user(user);
		m_chats.load_from_database(user);
		m_sending_messages.load_unsent_messages_from_database();
	}

	m_main_window.update_view();
	slot_need_test();
}
// установить соединение
void Shell::set_message_connection(MessageConnection* message_connection)
{
	m_connection.set_message_connection(message_connection);
	m_file_transfer_manager.set_message_connection(message_connection);

	m_main_window.set_connection_status(m_connection.is_connected());
	update_window_title();
}
SearchContactsWidgetGerm Shell::make_search_germ()
{
	return SearchContactsWidgetGerm {
		m_users,
		m_main_window.lineedit_search_contacts(),
		m_main_window.list_search(),
		m_main_window.search_contacts_container(),
		m_main_window.favorite_recent_container(),
		m_chat_item_widgets_manager
	};
}
// событие инициализации подключения
void Shell::slot_connection_initialized()
{
	if (!m_once_request_access_to_folder)
		m_connection.send_message(ClientMessageIsAccessClosedFolder());
}
// слот результата есть ли доступ в закрытую папку
void Shell::slot_have_access_closed_folder(bool is_have_access)
{
	m_once_request_access_to_folder = true;
	if (is_have_access) return;

	emit show_warning_no_access_closed_folder();
}
// слот мессадж бокса отсутствия доступа к закрытой папке
void Shell::slot_show_warning_no_access_closed_folder()
{
	//QMessageBox::warning(&m_main_window, tr("Warning"), tr("The private folder for moving files is not configured.\nContact technical support (tel. 8888)"), QMessageBox::StandardButton::Ok);
	QMessageBox::warning(&m_main_window, tr("Warning"), tr("Закрытая папка для перемещения файлов не настроена.\nСвяжитесь со службой технической поддержки (тел. 8888)"), QMessageBox::StandardButton::Ok);
}
// слот мессадж бокса отсутствия доступа к закрытой папке другого пользователя
void Shell::slot_show_warning_no_access_closed_folder_another_user(QString user_name)
{
	QMessageBox::warning(&m_main_window, tr("Warning"), tr("Невозможно передать папку/файл пользователю '%1'.\nСвяжитесь со службой технической поддержки (тел. 8888)").arg(user_name), QMessageBox::StandardButton::Ok);
}
void Shell::slot_connection_sync_finished()
{
	// после восстановления подключение отправляем то, что не отправилось
	m_loader_chat_messages_manager.resend();
	m_check_readed_new_messages.resend();
	m_sending_messages.resend();
	m_file_transfer_manager.resend();
}
void Shell::update_window_title()
{
	QString title = m_app_name + " - " + m_login_name;
	if (!m_connection.is_connected())
		title += " (no connection...)";

	m_main_window.setWindowTitle(title);
}
void Shell::slot_new_text_message(const Chat* chat, const QString& text)
{
	m_sending_messages.add_text_message(m_login_user, chat, text);
}
void Shell::slot_need_send_message()
{
	m_messages_widget_manager.send_current_text_message();
}
void Shell::slot_need_chat_remove(const Chat* chat)
{
	if (!chat->contact())
	{
		if (QMessageBox::information(&m_main_window, tr("Remove group"), tr("Are you sure you want remove and leave this group?"), QMessageBox::StandardButton::Ok, QMessageBox::StandardButton::Cancel) == QMessageBox::Ok)
			m_connection.send_message(ClientMessageChatRemove(chat->id()));
	}
	else m_connection.send_message(ClientMessageChatRemove(chat->id()));
} 
void Shell::slot_need_chat_add_favorite(const Chat* chat)
{
	m_connection.send_message(ClientMessageChatMoveFavorite(chat->id(), true));
}
void Shell::slot_need_chat_remove_favorite(const Chat* chat)
{
	m_connection.send_message(ClientMessageChatMoveFavorite(chat->id(), false));
}
void Shell::slot_need_chat_rename(const Chat* chat, const QString& name)
{
	m_connection.send_message(ClientMessageGroupChatRename(chat->id(), name));
}
void Shell::slot_need_group_chat_leave(const Chat* chat)
{
	slot_need_group_chat_remove_user(chat, m_login_user);
}
void Shell::slot_request_history_messages(const Chat* chat, ChatMessageId message_id)
{
	m_loader_chat_messages_manager.request_messages(chat);
}
void Shell::slot_contact_selected(const User* contact)
{
	if (m_chats.may_request_contact_chat(contact))
		m_connection.send_message(ClientMessageContactChatRequest(contact->id));
}
void Shell::slot_chat_selected(const Chat* chat)
{
	m_main_window.set_send_file_button_visible(chat ? chat->contact() : false);
}
void Shell::slot_group_chat_created(Chat* chat)
{
	// скрываем search list
	m_main_window.lineedit_search_contacts()->setText(QString());
	// устанавливаем текущим только что созданный групповой чат
	m_chats_widget.set_chat_selected(chat, true);
}
void Shell::slot_need_add_chat_to_chat_list(const User* contact)
{
	m_connection.send_message(ClientMessageContactChatCreate(contact->id));
}
void Shell::slot_contact_chat_created(const User* contact, const Chat* chat)
{
	Q_UNUSED(contact);
	if (chat)	// нужно учитывать, что чат может быть не создан
	{
		// выделяем только что созданный чат
		m_chats_widget.set_chat_selected(chat, true);
	}
}
void Shell::slot_notification_select_chat(const Chat* chat)
{
	m_chats_widget.set_chat_selected(chat, true);
}
void Shell::slot_need_group_chat_remove_user(const Chat* chat, const User* user)
{
	if (user != m_login_user ||
		(user == m_login_user && QMessageBox::information(&m_main_window, tr("Leave group"), tr("Are you sure you want leave this group?"), QMessageBox::StandardButton::Ok, QMessageBox::StandardButton::Cancel) == QMessageBox::Ok))
	{
		m_connection.send_message(ClientMessageGroupChatRemoveUser(chat->id(), user->id));
	}
}
// установка администратора на чат
void Shell::slot_need_group_chat_set_admin(const Chat* chat, const User* user)
{
	if (user == m_login_user ||
		(user != m_login_user && QMessageBox::information(&m_main_window, tr("Set administrator"), tr("Are you sure you want set %1 as administrator?").arg(user->name), QMessageBox::StandardButton::Ok, QMessageBox::StandardButton::Cancel) == QMessageBox::Ok))
	{
		m_connection.send_message(ClientMessageGroupSetOrRemoveAdmin(chat->id(), user->id, true));
	}
}
// удаление прав админа
void Shell::slot_need_group_chat_remove_admin(const Chat* chat, const User* user)
{
	if ((user == m_login_user && QMessageBox::information(&m_main_window, tr("Remove administrator"), tr("Are you sure you want to leave the admins?").arg(user->name), QMessageBox::StandardButton::Ok, QMessageBox::StandardButton::Cancel) == QMessageBox::Ok) || (user != m_login_user && QMessageBox::information(&m_main_window, tr("Remove administrator"), tr("Are you sure you want remove %1 from admins?").arg(user->name), QMessageBox::StandardButton::Ok, QMessageBox::StandardButton::Cancel) == QMessageBox::Ok))
	{
		m_connection.send_message(ClientMessageGroupSetOrRemoveAdmin(chat->id(), user->id, false));
	}
}
void Shell::slot_need_test()
{
	QTimer::singleShot(3000, this, SLOT(slot_test()));
}
void Shell::slot_test()
{
	/*if (Chat* chat = m_chats->find_chat(1))
	{
		if (rand() % 10 > 5) m_notification_message->show_notification(chat, "Drop box!!!");
		if (rand() % 10 > 5) m_notification_message->show_notification(chat, "Drop box!!! 60");
		if (rand() % 10 > 5) m_notification_message->show_notification(m_chats->find_chat(29), "29");
		if (rand() % 10 > 5) m_notification_message->show_notification(m_chats->find_chat(24), "24");
	}*/
}
// - выбор чата
// + при активации приложения + чат выбран
// + при добавлении сообщения (если приложение активно + чат выбран)
void Shell::slot_chat_message_added(Chat* chat, const ChatMessage* msg)
{
	// 1) снимаем с отправки
	m_sending_messages.process_message_received_on_server(msg);

	// если не историческое
	if (!msg->historical)
	{
		if (chat->is_enabled())
		{
			m_chats_widget.set_chat_top(chat);
			// если отправлено от другого
			if (!msg->myself)
			{
				// и не прочитано
				if (!msg->readed())
				{
					if (msg->type == ChatMessageType::File)
					{
						QString mask = msg->file_is_dir ? tr("folder: %1") : tr("file: %1");
						m_notification_message.show_notification(chat, mask.arg(Utility::filename_from_path(msg->file_server_path)));
					}
					else if (msg->type == ChatMessageType::Text)
					{
						m_notification_message.show_notification(chat, msg->text);
					}
				}
			}
		}
	}
	// если непрочитанное сообщение, то отправляем на проверку прочтения
	if (!msg->readed())
		m_check_readed_new_messages.check(chat);
	
	// добавляем в gui
	m_messages_widget_manager.add_message(chat, msg);
}
void Shell::slot_message_readed(Chat* chat, ChatMessageId message_id, const QDateTime& datetime)
{
	logs("message readed");
	m_chats.set_messages_readed(chat, message_id, datetime);
}
void Shell::slot_new_message_readed_response(bool success, Chat* chat, ChatMessageId message_id)
{
	m_check_readed_new_messages.set_message_readed(success, chat, message_id);
}
// событие изменения кол-ва непрочитанных сообщений
void Shell::slot_change_count_unreaded_messages(int unreaded_count)
{
	m_main_window.set_unreaded_messages(unreaded_count > 0);
}
void Shell::slot_main_window_activated()
{
	// при активации приложения скрываем оповещение
	m_notification_message.hide_notification();
	// при активации приложения проверяем, есть ли непрочитанные сообщения
	m_check_readed_new_messages.process_activate_app();
}
void Shell::slot_login_user_updated(const User* login_user)
{
	update_login_user(login_user);
}
// обновить пользователя, под которым в чате
void Shell::update_login_user(const User* user)
{
	m_login_user = user;
	m_chats.set_login_user(user);
	m_file_transfer_manager.set_login_user(user);
}
// событие локальной загрузки сообщений
void Shell::slot_local_chat_history_messages_loaded(const Chat* chat, const std::vector<DescChatMessage>& messages)
{
	m_messages_widget_manager.start_load_history_messages(chat);
	m_chats.add_messages(messages, false);
	m_messages_widget_manager.finish_load_history_messages(chat);
}
// событие сетевой (удалённой) загрузки сообщений с сервера
void Shell::slot_remote_chat_history_messages_loaded(const Chat* chat, const std::vector<DescChatMessage>& messages, bool is_finish_upload)
{
	m_loader_chat_messages_manager.remote_messages_received(chat, is_finish_upload);

	m_messages_widget_manager.start_load_history_messages(chat);
	m_chats.add_messages(messages, true);
	m_messages_widget_manager.finish_load_history_messages(chat);
}
// получение новых сообщений
void Shell::slot_new_chat_messages(const std::vector<DescChatMessage>& messages)
{
	for (const DescChatMessage& msg : messages)
		logs(QString("new chat message: msg_id=%1 chat_id=%2 file_name=%3 file_status=%4").arg(msg.id).arg(msg.chat_id).arg(msg.file_dest_name).arg((int)msg.file_status));

	// получение сообщений в чаты
	m_chats.add_messages(messages, true);
}
// получение онлайн статусов пользователей
void Shell::slot_request_users_online_status(const std::vector<UserId>& users)
{
	m_connection.send_message(ClientMessageUsersStatus(users));
}
void Shell::slot_dropped(const QList<QUrl>& urls)
{
	QStringList files;
	for (const QUrl& url : urls)
	{
		QString ideal_url = url.toString().remove("file:///").remove("file:");
		files << ideal_url;
	}
	send_files(files);
}
void Shell::slot_need_send_folder()
{
	QString dir = QFileDialog::getExistingDirectory(nullptr, QString());
	if (!dir.isEmpty())
		send_files(QStringList() << dir);
}
void Shell::slot_need_send_file()
{
	QStringList files = QFileDialog::getOpenFileNames(nullptr, QString());
	if (!files.isEmpty())
		send_files(files);
}
void Shell::send_files(const QStringList& files)
{
	static int limit_files = 10;
	if (files.size() > limit_files)
	{
		QMessageBox::warning(&m_main_window, tr("Warning"), tr("Cannot transfer more than %1 files").arg(limit_files), QMessageBox::StandardButton::Ok);
		return;
	}

	if (const CurrentChat* current_chat = m_chats_widget.current())
	{
		if (const Chat* chat = current_chat->chat())
		{
			if (chat->contact())
			{
				for (const QString& file : files)
					send_file(chat, file);
			}
		}
	}
}
void Shell::send_file(const Chat* chat, const QString& path)
{
	m_file_transfer_manager.add_file_transfer(QUuid::createUuid(), chat->id(), path);
}
void Shell::slot_file_transfer_finished(FileTransferResult result, const QString& dest_name)
{
	m_file_transfer_manager.finish_transfer(result, dest_name);
}
void Shell::slot_file_transfer_write_file_error()
{
	// в случае ошибки, останавливаем передачу
	m_file_transfer_manager.stop_transfer();

	// выведем окно с сообщением "Свяжитесь со службой технической поддержки"
	check_show_warning_no_access_closed_folder_another_user();
}
// выведем окно с сообщением "Свяжитесь со службой технической поддержки"
void Shell::check_show_warning_no_access_closed_folder_another_user()
{
	ChatId chat_id = m_file_transfer_manager.get_current_trasfer_chat_id();
	if (0 == chat_id) return;

	if (Chat* chat = m_chats.find_chat(chat_id))
	{
		if (m_file_transfer_write_error_chat_ids.find(chat_id) != m_file_transfer_write_error_chat_ids.end()) return;

		m_file_transfer_write_error_chat_ids.insert(chat_id);
		emit show_warning_no_access_closed_folder_another_user(chat->contact()->name);
	}
}
void Shell::slot_file_message_progress(ChatMessageId message_id, const Chat* chat, unsigned char progress)
{
	if (const ChatMessage* msg = chat->find_message(message_id))
		m_messages_widget_manager.set_file_message_progress(msg->local_id, chat, progress);
}
// событие отмены файловой передачи
void Shell::slot_need_file_message_cancel(const ChatMessageLocalId& message_local_id)
{
	m_file_transfer_manager.cancel_file_message(message_local_id);
}
// слот подтверждения отмены передачи файлового сообщения
void Shell::slot_file_message_cancel_confirm(ChatMessageId message_id)
{
	m_file_transfer_manager.process_confirm_cancel(message_id);
}
// слот запроса онлайн-статусов пользователей
void Shell::slot_request_users_online_statuses(const std::vector<const User*>& users)
{
	logs(QString("request users online statuses = %1").arg(users.size()));

	std::vector<UserId> user_ids;
	for (const User* user : users)
		user_ids.push_back(user->id);

	m_connection.send_message(ClientMessageUsersStatus(user_ids));
}
void Shell::slot_start_update_chat_list()
{
	m_chats_widget.start_update();
}
void Shell::slot_finish_update_chat_list()
{
	m_chats_widget.finish_update();
}