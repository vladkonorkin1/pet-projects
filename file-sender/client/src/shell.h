//=====================================================================================//
//   Author: open
//   Date:   15.02.2019
//=====================================================================================//
#pragma once
#include "notification_message/notification_message.h"
#include "chats_widgets/chat_item_widgets_manager.h"
#include "gui_messages/messages_widget_manager.h"
#include "modify_chat_users/modify_chat_users.h"
#include "file_transfer/file_transfer_manager.h"
#include "chats_widgets/chats_widget.h"
#include "check_readed_new_messages.h"
#include "database_update_manager.h"
#include "loader_chat_messages.h"
#include "thread_task_manager.h"
#include "database/database.h"
#include "sending_messages.h"
#include "main_window.h"
#include "connection.h"
#include "chats.h"

class Shell : public QObject
{
	Q_OBJECT
private:
	QString m_app_name;
	MainWindow& m_main_window;							// интерфейс главного окна
	QString m_login_name;								// под кем авторизовался
	ThreadTaskManager m_thread_task_manager;			// менеджер выполнения задач в фоне
	Database m_database;								// база данных
	Users m_users;										// пользователи
	Chats m_chats;										// чаты
	Connection m_connection;							// подключение
	const User* m_login_user;							// пользователь под кем мы авторизовались
	SendingMessages m_sending_messages;					// менеджер отправки сообщений
	FileTransferManager m_file_transfer_manager;		// менеджер передачи файлов

	ChatItemWidgetsManager m_chat_item_widgets_manager;	// менеджер менеджеров айтемов чата (или контакта)
	MessagesWidgetManager m_messages_widget_manager;	// менеджер окон сообщений
	ChatsWidget m_chats_widget;							// виджет чатов
	ModifyChatUsers m_modify_chat_users;				// окно добавления/удаления пользователей чата
	NotificationMessage m_notification_message;			// оповещение о сообщениях
	CheckReadedNewMessages m_check_readed_new_messages;	// проверка прочтения новых сообщений
	LoaderChatMessagesManager m_loader_chat_messages_manager;	// загрузчик сообщений чатов

	bool m_once_request_access_to_folder;
	std::set<ChatId> m_file_transfer_write_error_chat_ids;

public:
	Shell(MainWindow& main_window, const QString& app_name, const QString& login_name, const QString& database_path, const QString& host);
	// установить соединение
	void set_message_connection(MessageConnection* message_connection);

private slots:
	void slot_need_send_message();
	void slot_need_send_file();
	void slot_need_send_folder();

	// событие инициализации подключения
	void slot_connection_initialized();
	void slot_connection_sync_finished();

	void slot_login_user_updated(const User* login_user);

	void slot_new_text_message(const Chat* chat, const QString& text);

	void slot_need_chat_remove(const Chat* chat);
	void slot_need_chat_add_favorite(const Chat* chat);
	void slot_need_chat_remove_favorite(const Chat* chat);
	void slot_need_chat_rename(const Chat* chat, const QString& name);
	void slot_need_group_chat_leave(const Chat* chat);
	
	void slot_contact_selected(const User* contact);
	void slot_chat_selected(const Chat* chat);

	void slot_group_chat_created(Chat* chat);
	void slot_need_add_chat_to_chat_list(const User* contact);
	void slot_contact_chat_created(const User* contact, const Chat* chat);
	void slot_need_group_chat_remove_user(const Chat* chat, const User* user);
	void slot_need_group_chat_set_admin(const Chat* chat, const User* user);
	void slot_need_group_chat_remove_admin(const Chat* chat, const User* user);
	void slot_notification_select_chat(const Chat* chat);

	void slot_chat_message_added(Chat* chat, const ChatMessage* msg);
	void slot_message_readed(Chat* chat, ChatMessageId message_id, const QDateTime& datetime);
	void slot_new_message_readed_response(bool success, Chat* chat, ChatMessageId message_id);

	void slot_dropped(const QList<QUrl>& urls);
	void slot_main_window_activated();

	void slot_need_test();
	void slot_test();

	// событие запроса исторических сообщений
	void slot_request_history_messages(const Chat* chat, ChatMessageId message_id);
	// событие локальной загрузки сообщений
	void slot_local_chat_history_messages_loaded(const Chat* chat, const std::vector<DescChatMessage>& messages);
	// событие сетевой (удалённой) загрузки сообщений с сервера
	void slot_remote_chat_history_messages_loaded(const Chat* chat, const std::vector<DescChatMessage>& messages, bool is_finish_upload);
	// получение новых сообщений
	void slot_new_chat_messages(const std::vector<DescChatMessage>& messages);
	// получение онлайн статусов пользователей
	void slot_request_users_online_status(const std::vector<UserId>& users);

	void slot_file_transfer_finished(FileTransferResult result, const QString& dest_name);
	void slot_file_transfer_write_file_error();
	void slot_file_message_progress(ChatMessageId message_id, const Chat* chat, unsigned char progress);
	// событие отмены файловой передачи
	void slot_need_file_message_cancel(const ChatMessageLocalId& message_local_id);
	// слот подтверждения отмены передачи файлового сообщения
	void slot_file_message_cancel_confirm(ChatMessageId message_id);
	// слот запроса онлайн-статусов пользователей
	void slot_request_users_online_statuses(const std::vector<const User*>& users);
	// событие начала обновления чат-листа
	void slot_start_update_chat_list();
	// событие окончания обновления чат-листа
	void slot_finish_update_chat_list();
	// событие изменения кол-ва непрочитанных сообщений
	void slot_change_count_unreaded_messages(int unreaded_count);
	// слот результата есть ли доступ в закрытую папку
	void slot_have_access_closed_folder(bool is_have_access);
	// слот мессадж бокса отсутствия доступа к закрытой папке
	void slot_show_warning_no_access_closed_folder();
	// слот мессадж бокса отсутствия доступа к закрытой папке другого пользователя
	void slot_show_warning_no_access_closed_folder_another_user(QString user_name);
		
signals:
	void non_readed_message(bool readed);
	void show_warning_no_access_closed_folder();
	void show_warning_no_access_closed_folder_another_user(QString user_name);

private:
	void send_file(const Chat* chat, const QString& path);
	void update_window_title();
	SearchContactsWidgetGerm make_search_germ();
	// обновить пользователя, под которым в чате
	void update_login_user(const User* user);
	void send_files(const QStringList& files);

	// выведем окно с сообщением "Свяжитесь со службой технической поддержки"
	void check_show_warning_no_access_closed_folder_another_user();
};