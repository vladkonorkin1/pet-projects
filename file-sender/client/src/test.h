#pragma once
#include "ui_test.h"

namespace Ui { class Test; }

class Test : public QWidget
{
	Q_OBJECT
private:
	std::auto_ptr<Ui::Test> m_ui;

public:
	Test();
};