#include "base/precomp.h"
#include "tray_icon.h"
#include <QApplication>

TrayIcon::TrayIcon()
: m_tray_menu(new QMenu)
, m_unreaded_messages(false)
, m_connection_status(false)
{
	QAction* close(new QAction("Exit", this));
	connect(close, &QAction::triggered, []() { QApplication::quit(); });
	m_tray_menu.addAction(close);
	setContextMenu(&m_tray_menu);
	update_tray_icon();
	show();
}
// ���������� � ��������� ���� ���������
void TrayIcon::set_unreaded_messages(bool unreaded)
{
	m_unreaded_messages = unreaded;
	update_tray_icon();
}
// ���������� ������ �����������
void TrayIcon::set_connection_status(bool on)
{
	m_connection_status = on;
	update_tray_icon();
}
// �������� ������
void TrayIcon::update_tray_icon()
{
	static std::array<QIcon, 4> icons =
	{
		QIcon(":/tray_icon_connect_unreaded_messages.png"),
		QIcon(":/tray_icon_connect_readed_messages.png"),
		QIcon(":/tray_icon_no_connect_unreaded_messages.png"),
		QIcon(":/tray_icon_no_connect_readed_messages.png")
	};

	if (m_connection_status)
	{
		if (m_unreaded_messages) setIcon(icons[0]);
		else setIcon(icons[1]);
	}
	else
	{
		if (m_unreaded_messages) setIcon(icons[2]);
		else setIcon(icons[3]);
	}
}