#pragma once
#include <QSystemTrayIcon>
#include <QAction>
#include <QMenu>

class TrayIcon : public QSystemTrayIcon
{
	Q_OBJECT
private:
	QMenu m_tray_menu;
	bool m_unreaded_messages;
	bool m_connection_status;

public:
	TrayIcon();
	// ���������� ������������� ���������
	void set_unreaded_messages(bool unreaded);
	// ���������� ������ �����������
	void set_connection_status(bool on);

private:
	// �������� ������
	void update_tray_icon();
};
