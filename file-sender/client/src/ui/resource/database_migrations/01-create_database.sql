create table properties (
	key text primary key not null,
	value text not null
);

create table departments (
	id integer primary key not null,
	name text,
	timestamp text not null
);

create table users (
	id int primary key not null,
	login text,
	name text,
	folder text,
	timestamp text not null,
	department_id int not null,
	deleted tinyint not null
);

create table chats (
	id int primary key not null,
	timestamp text not null,
	name text,
	contact_id int,
	--server_history_message_id int not null default 0
	last_readed_message_id int not null default 0
);

create table chat_users (
	chat_id int not null,
	user_id int not null,
	admin int not null,
	primary key(chat_id, user_id)
);

create table chat_list (
	chat_id int primary key not null,
	favorite int not null,
	last_time int not null
);

create table messages (
	id int primary key not null,
	local_id text not null,
	chat_id int not null,
	sender_id int not null,
	stamp int not null,
	type int not null,
	status int not null,
	history_status int not null,
	datetime text not null,
	datetime_readed text null,
	txt text null,
	file_src_path text null,
	file_dest_name text null,
	file_is_dir int null,
	file_size int null,
	file_status int null,
	target_contact_id int null
);

create table unsent_messages (
	local_id text primary key not null,
	chat_id int not null,
	sender_id int not null,
	datetime text not null,
	type int not null,
	txt text null
);