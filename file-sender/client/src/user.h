//=====================================================================================//
//   Author: open
//   Date:   18.02.2018
//=====================================================================================//
#pragma once

class Department
{
public:
	DepartmentId id;
	QString name;
	Timestamp timestamp;
	//std::vector<const User*> users;

	Department(DepartmentId id) : id(id), timestamp(0) {}
};

class User
{
public:
	UserId id;						// уникальный номер
	QString login;					// логин
	QString name;					// ФИО
	QString folder;					// путь к папке для файлов
	Timestamp timestamp;			// временная отметка изменения
	Department* department;
	bool deleted;
	bool online;

	User(UserId id, const QString& login) : id(id), login(login), timestamp(0), department(nullptr), deleted(false), online(false) {}
};