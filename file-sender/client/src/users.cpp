//=====================================================================================//
//   Author: open
//   Date:   19.02.2019
//=====================================================================================//
#include "base/precomp.h"
#include "users.h"
#include "database/database.h"

Users::Users(Database& database)
: m_database(database)
, m_timestamp_departments(0)
, m_timestamp_users(0)
{
}
Department* Users::find_department(DepartmentId id) const
{
	auto it = m_departments.find(id);
	if (it != m_departments.end()) return it->second;
	return nullptr;
}
Department* Users::create_department(DepartmentId id)
{
	auto it = m_departments.find(id);
	if (it == m_departments.end())
	{
		Department* department = new Department(id);
		m_departments[id] = department;
		return department;
	}
	return nullptr;
}
void Users::update_department(Department* department, const DescDepartment& desc)
{
	department->name = desc.name;
	department->timestamp = desc.timestamp;

	// ��������� ��������� ������������
	if (m_timestamp_departments < department->timestamp)
		m_timestamp_departments = department->timestamp;
}
// ����� ������������ �� id
const User* Users::find_user(UserId id) const
{
	return find_user_non_const(id);
}
User* Users::find_user_non_const(UserId id) const
{
	auto it = m_id_users.find(id);
	if (it != m_id_users.end()) return it->second;
	return nullptr;
}
// ����� ������������ �� ������
User* Users::find_user(const QString& login) const
{
	auto it = m_login_users.find(login);
	if (it != m_login_users.end()) return it->second;
	return nullptr;
}
// ������ �������������
std::vector<const User*> Users::users() const
{
	std::vector<const User*> users;
	for (auto& pair : m_id_users)
		users.push_back(pair.second);
	return std::move(users);
}
User* Users::create_user(UserId id, const QString& login)
{
	if (m_id_users.find(id) == m_id_users.end())
	{
		User* user = new User(id, login);
		m_id_users[id] = user;
		if (!login.isEmpty())
			m_login_users[login] = user;
		return user;
	}
	return nullptr;
}
void Users::update_user(User* user, const DescUser& desc)
{
	// ���� ��������� �����
	if (user->login != desc.login)
	{
		if (!user->login.isEmpty())
			m_login_users.erase(user->login);
		if (!desc.login.isEmpty())
			m_login_users[desc.login] = user;
		user->login = desc.login;
	}

	user->name = desc.name;
	user->folder = desc.folder;
	user->deleted = desc.deleted;

	user->department = find_department(desc.department_id);
	assert( user->department );

	user->timestamp = desc.timestamp;
	if (m_timestamp_users < user->timestamp)
		m_timestamp_users = user->timestamp;
}
// ���������� �������������
void Users::update_departments(const std::vector<DescDepartment>& desc_departments)
{
	if (desc_departments.empty()) return;

	std::vector<const Department*> database_departments;
	for (const DescDepartment& desc : desc_departments)
	{
		Department* department = find_department(desc.id);
		if (!department)
			department = create_department(desc.id);
		update_department(department, desc);
		database_departments.push_back(department);
	}
	m_database.update_departaments(database_departments);
}
void Users::set_online(UserId id, bool online)
{
	if (User* user = find_user_non_const(id))
	{
		if (user->online != online)
		{
			user->online = online;
			emit changed_online(user);
		}
	}
}
// ���������� �������������
void Users::update_users(const std::vector<DescUser>& desc_users)
{
	if (desc_users.empty()) return;

	std::vector<const User*> database_users;
	for (const DescUser& desc : desc_users)
	{
		User* user = find_user_non_const(desc.id);
		if (!user)
			user = create_user(desc.id, desc.login);
		update_user(user, desc);
		database_users.push_back(user);
	}
	m_database.update_users(database_users);
}
// ��������� �� ���� ������
void Users::load_from_database()
{
	std::vector<DescDepartment> departments = m_database.select_departments();
	for (const DescDepartment& desc : departments)
	{
		if (Department* department = create_department(desc.id))
			update_department(department, desc);
	}

	std::vector<DescUser> users = m_database.select_users();
	for (const DescUser& desc : users)
	{
		if (User* user = create_user(desc.id, desc.login))
			update_user(user, desc);
	}
}
