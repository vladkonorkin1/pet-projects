//=====================================================================================//
//   Author: open
//   Date:   19.02.2019
//=====================================================================================//
#pragma once
#include "user.h"

class Database;

class Users : public QObject
{
	Q_OBJECT
private:
	Database& m_database;

	std::map<DepartmentId, Department*> m_departments;
	std::map<UserId, User*> m_id_users;
	std::map<QString, User*> m_login_users;

	Timestamp m_timestamp_departments;
	Timestamp m_timestamp_users;
	
public:
	Users(Database& database);
	// ����� ������������ �� id
	const User* find_user(UserId id) const;
	// ����� ������������ �� ������
	User* find_user(const QString& login) const;
	// ���������� �������������
	void update_departments(const std::vector<DescDepartment>& desc_departments);
	// ���������� �������������
	void update_users(const std::vector<DescUser>& desc_users);
	// ���������� ��� ������������ ������ ������
	void set_online(UserId id, bool online);
	// ������� ������������ ��������� �������������
	Timestamp timestamp_departments() const;
	// ������� ������������ ��������� �������������
	Timestamp timestamp_users() const;
	// ������ �������������
	std::vector<const User*> users() const;
	// ��������� �� ���� ������
	void load_from_database();

signals:
	void changed_online(const User* user);

private:
	Department* find_department(DepartmentId id) const;
	Department* create_department(DepartmentId id);
	void update_department(Department* department, const DescDepartment& desc);

	User* find_user_non_const(UserId id) const;
	User* create_user(UserId id, const QString& login);
	void update_user(User* user, const DescUser& desc);
};

// ������� ������������ ��������� �������������
inline Timestamp Users::timestamp_departments() const {
	return m_timestamp_departments;
}
// ������� ������������ ��������� �������������
inline Timestamp Users::timestamp_users() const {
	return m_timestamp_users;
}