#define MyAppName "file-sender"  ;file-sender
#define MyAppVersion "0.1"
#define MyAppExeName "launcher.exe"
#define MyAppPublisher "razrabotka++"
#define MyAppURL "http://www.razrabotka++.ru/"
#define MyTargetExeName "sima-chat.exe"
;#define SimaAppsPath "{drive:{sys}}simaapps"
#define SimaAppsPath "c:/simaapps"

[Files]
Source: application_icon.ico; DestDir: {app}
Source: "sima-chat-launcher-win32/*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

;vcredist
Source: vc_redist.x86.exe; DestDir: {tmp}

[Setup]
PrivilegesRequired=admin
AppId={{C236E241-1E02-4665-8AFB-033864F0CA08}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
;DefaultDirName={pf64}/{#MyAppName}
DefaultDirName={#SimaAppsPath}/{#MyAppName}
OutputBaseFilename={#MyAppName}
DefaultGroupName={#MyAppName}
OutputDir=./
;SetupIconFile=../src/src/resource/appicon.ico
InternalCompressLevel=max
SolidCompression=yes
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}

CloseApplications = force
CloseApplicationsFilter = *.exe

[Run]
Filename: {sys}\taskkill.exe; Parameters: "/f /im {#MyTargetExeName}"; Flags: skipifdoesntexist runhidden
Filename: {tmp}\vc_redist.x86.exe; Parameters: "/install /passive /norestart"; StatusMsg: Installing Microsoft Visual C++ 2017 Runtime Library...
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall

[Icons]
Name: "{commonprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; IconFilename: {app}\application_icon.ico

[Registry]
Root: HKLM; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Run"; ValueType: string; ValueName: {#MyAppName}; ValueData: """{app}\{#MyAppExeName}"""; Flags: uninsdeletevalue

[UninstallRun]
Filename: {sys}\taskkill.exe; Parameters: "/f /im {#MyTargetExeName}"; Flags: skipifdoesntexist runhidden

;[UninstallDelete] 
;Type: dirifempty; Name: {app};
;Type: dirifempty; Name: {#SimaAppsPath};

;[Tasks]
;Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons};
;Name: startup; Description: {cm:CreateQuickLaunchIcon}; GroupDescription: {cm:AdditionalIcons}
;Name: programs; Description: {cm:CreateGroupIcons}; GroupDescription: {cm:AdditionalIcons}
;[Icons]
;Name: {userstartup}\{#MyAppName}; Filename: {app}\{#MyAppExeName}; Tasks: startup;
;Name: {commonstartup}\{#MyAppName}; Filename: {app}\{#MyAppExeName}; Tasks: startup;
;Name: {userprograms}\{#MyAppName}; Filename: {app}\{#MyAppExeName}; Tasks: programs
;Name: {userdesktop}\{#MyAppName}x; Filename: {app}\{#MyAppExeName}; Tasks: desktopicon; IconFilename: {app}\application_icon.ico