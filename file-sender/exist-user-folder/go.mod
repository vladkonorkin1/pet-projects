module exist-user-folder

go 1.14

require (
	github.com/go-ini/ini v1.55.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lib/pq v1.5.1
	github.com/smartystreets/goconvey v1.6.4 // indirect
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79 // indirect
	gopkg.in/ini.v1 v1.55.0 // indirect
)
