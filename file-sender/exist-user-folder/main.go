package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"

	"database/sql"

	"github.com/go-ini/ini"
	"github.com/labstack/echo"
	_ "github.com/lib/pq"
)

type DatabaseConnectionParameters struct {
	database_name     string
	database_host     string
	database_port     string
	database_user     string
	database_password string
}

type Service struct {
	e   *echo.Echo
	dcp DatabaseConnectionParameters
}

func NewService(dcp DatabaseConnectionParameters) *Service {
	e := echo.New()
	e.HidePort = true
	e.HideBanner = true
	e.Debug = false

	s := &Service{
		e:   e,
		dcp: dcp,
	}
	gr_users := e.Group("/users")
	gr_users.GET("/no_access_folders", s.on_no_access_user_folders)
	e.GET("/num_goroutines", s.on_num_goroutines)
	return s
}

func (s *Service) Listen() {
	address := ":" + strconv.Itoa(8908)
	s.e.Logger.Fatal(s.e.Start(address))
}

func main() {
	cfg, err := ini.Load("config.ini")
	if err != nil {
		log.Fatal(err)
	}

	section := cfg.Section("service")
	dcp := DatabaseConnectionParameters{
		database_host:     section.Key("database-host-name").String(),
		database_port:     section.Key("database-port").String(),
		database_name:     section.Key("database-name").String(),
		database_user:     section.Key("database-user-name").String(),
		database_password: section.Key("database-password").String(),
	}

	s := NewService(dcp)
	go s.Listen()

	log.Println("start")

	//s.no_access_user_folders()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs
}

type User struct {
	Login       string `json:"login" pg:"login"`
	Name        string `json:"name" pg:"name"`
	Folder      string `json:"folder" pg:"folder"`
	Permissions string `json:"permissions"`
}

func (s *Service) no_access_user_folders() ([]User, error) {
	log.Println("start select database users")

	err, users := s.select_users()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	log.Printf("database users size=%d\n", len(users))

	no_access_users := []User{}
	for _, u := range users {
		fi, err := os.Lstat(u.Folder)
		//if _, err := os.Stat(u.Folder); err != nil {
		if err != nil {
			if !os.IsExist(err) {
				no_access_users = append(no_access_users, u)
				log.Printf("- login=%s folder=%s\n", u.Login, u.Folder)
			} else {
				log.Printf("+ %v login=%s folder=%s\n", fi.Mode().Perm(), u.Login, u.Folder)
			}
		} else {
			perm := fi.Mode().Perm()
			p := fmt.Sprintf("%v", perm)
			u.Permissions = p
			if len(p) == 10 && (p[1] != '-' && p[2] != '-' && p[4] != '-' && p[5] != '-' && p[7] != '-' && p[8] != '-') {
				//log.Printf("+ %v login=%s folder=%s\n", fi.Mode().Perm(), u.Login, u.Folder)
			} else {
				no_access_users = append(no_access_users, u)
				log.Printf("- %v login=%s folder=%s\n", fi.Mode().Perm(), u.Login, u.Folder)
			}
		}
	}

	log.Printf("no access folder users size=%d\n", len(no_access_users))
	return no_access_users, nil
}

func (s *Service) on_no_access_user_folders(c echo.Context) error {
	no_access_users, err := s.no_access_user_folders()
	if err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, no_access_users)
}
func (s *Service) select_users() (error, []User) {
	connStr := fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=disable",
		s.dcp.database_host, s.dcp.database_port, s.dcp.database_name, s.dcp.database_user, s.dcp.database_password)
	//connStr := "user=file-sender password=123 dbname=file-sender sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return err, nil
	}
	defer db.Close()

	rows, err := db.Query("select login, name, folder from users where deleted=false")
	if err != nil {
		return err, nil
	}
	defer rows.Close()
	users := []User{}

	for rows.Next() {
		u := User{}
		err := rows.Scan(&u.Login, &u.Name, &u.Folder)
		if err != nil {
			continue
		}
		users = append(users, u)
	}
	//for _, u := range users {
	//	fmt.Println(u.login, u.name, u.folder)
	//}
	return nil, users
}

func (s *Service) on_num_goroutines(c echo.Context) error {
	// Get the count of number of go routines running.
	count := runtime.NumGoroutine()
	c.JSON(http.StatusOK, count)
	return nil
}
