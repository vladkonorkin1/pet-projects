TARGET = server-file-sender
CONFIG -= flat
TEMPLATE = app

win32 {
    release: DESTDIR = tmp/release
    debug:   DESTDIR = tmp/debug
    OBJECTS_DIR = $$DESTDIR/.obj
    MOC_DIR = $$DESTDIR/.moc
    RCC_DIR = $$DESTDIR/.qrc
    UI_DIR = $$DESTDIR/.ui
}
unix {
    DESTDIR = .
    OBJECTS_DIR = $$DESTDIR/tmp/.obj
    MOC_DIR = $$DESTDIR/tmp/.moc
    RCC_DIR = $$DESTDIR/tmp/.qrc
    UI_DIR = $$DESTDIR/tmp/.ui
}

QT -= gui
QT += network sql
CONFIG += console
#Debug:CONFIG   += console
Debug:DEFINES += _DEBUG

INCLUDEPATH += src
INCLUDEPATH += ../shared
#INCLUDEPATH += src/qtservice

CONFIG += precompile_header
PRECOMPILED_HEADER = ../shared/base/precomp.h

HEADERS =  	../shared/base/precomp.h                                    \
			../shared/base/timer.h                                      \
			../shared/base/utility.h                                    \
			../shared/base/log.h                                        \
			../shared/base/config.h                                     \
			../shared/base/convert_utility.h                            \
			../shared/network/packet.h                                  \
			../shared/network/packet_reader.h                           \
			../shared/network/net_message.h                             \
			../shared/network/net_messages.h                            \
			../shared/network/message_dispatcher.h                      \
			../shared/network/base_client_connection.h                  \
			../shared/network/handler_messages.h                        \
			../shared/network/types.h                                   \
			../shared/http_client.h                                     \
			../shared/http_request.h                                    \
			../shared/thread_task_manager.h								\
			../shared/thread_task.h										\
			src/database_test_ddos.h                                    \
			src/message_connection.h                                    \
			src/message_server.h                                        \
			src/connection.h                                            \
			src/connection_germ.h                                       \
			src/connection_manager.h                                    \
			src/online_status_manager.h                                 \
			src/server.h                                                \
			src/session.h                                               \
			src/session_manager.h                                       \
			src/user.h                                                  \
			src/users.h                                                 \
			src/database/database.h                                     \
			src/database/database_create_connection.h                   \
			src/database/database_properties.h                          \
			src/database/database_chats.h                               \
			src/database/async/database_async.h                         \
			src/database/async/database_async_query.h                   \
			src/database/async/queries/database_async_query_chat.h      \
			src/database/async/queries/database_async_create_chat.h     \
			src/database/async/queries/database_async_chat_messages.h   \
			src/database/async/queries/database_async_users.h           \
			src/chat.h                                                  \
			src/chats.h                                                 \
			src/chat_list.h                                             \
			src/chat_lists.h                                            \
			src/send_user.h                                             \
			src/send_chat.h                                             \
			src/send_chat_messages.h                                    \
			src/history_messages.h                                      \
			src/chat_message_stamp_generator.h                          \
			src/file_transfer/file_connection.h                         \
			src/file_transfer/file_connection_server.h                  \
			src/file_transfer/file_thread_connection.h                  \
			src/file_transfer/file_transfer.h                           \
			src/file_transfer/file_message_manager.h                    \
			src/file_transfer/contact_folder_desc.h                     \
			src/file_transfer/file_transfer_current.h                   \
			src/progress_updater.h                                      \
			src/http_request_authorization.h                            \
			src/http_request_employers.h                                \
			src/http_request_departments.h                              \
			src/users_updater.h                                         \
			src/qtservice/qtservice.h									\
			src/qtservice/qtservice_p.h									\	
			src/check_exists_closed_folder_task.h						\

SOURCES = 	src/main.cpp                                                \
			../shared/base/precomp.cpp                                  \
			../shared/base/timer.cpp                                    \
			../shared/base/utility.cpp                                  \
			../shared/base/log.cpp                                      \
			../shared/base/config.cpp                                   \
			../shared/base/convert_utility.cpp                          \
			../shared/network/packet.cpp                                \
			../shared/network/packet_reader.cpp                         \
			../shared/network/net_message.cpp                           \
			../shared/network/net_messages.cpp                          \
			../shared/network/message_dispatcher.cpp                    \
			../shared/network/base_client_connection.cpp                \
			../shared/http_client.cpp                                   \
			../shared/http_request.cpp                                  \
			../shared/thread_task_manager.cpp							\
			src/database_test_ddos.cpp                                  \
			src/message_connection.cpp                                  \
			src/message_server.cpp                                      \
			src/connection.cpp                                          \
			src/connection_germ.cpp                                     \
			src/connection_manager.cpp                                  \
			src/online_status_manager.cpp                               \
			src/server.cpp                                              \
			src/session.cpp                                             \
			src/session_manager.cpp                                     \
			src/users.cpp                                               \
			src/database/database.cpp                                   \
			src/database/database_create_connection.cpp                 \
			src/database/database_properties.cpp                        \
			src/database/database_chats.cpp                             \
			src/database/async/database_async.cpp                       \
			src/database/async/database_async_query.cpp                 \
			src/database/async/queries/database_async_query_chat.cpp    \
			src/database/async/queries/database_async_create_chat.cpp   \
			src/database/async/queries/database_async_chat_messages.cpp \
			src/database/async/queries/database_async_users.cpp         \
			src/chat.cpp                                                \
			src/chats.cpp                                               \
			src/chat_list.cpp                                           \
			src/chat_lists.cpp                                          \
			src/send_user.cpp                                           \
			src/send_chat.cpp                                           \
			src/send_chat_messages.cpp                                  \
			src/history_messages.cpp                                    \
			src/chat_message_stamp_generator.cpp                        \
			src/file_transfer/file_connection.cpp                       \
			src/file_transfer/file_connection_server.cpp                \
			src/file_transfer/file_thread_connection.cpp                \
			src/file_transfer/file_transfer.cpp                         \
			src/file_transfer/file_message_manager.cpp                  \
			src/file_transfer/contact_folder_desc.cpp                   \
			src/file_transfer/file_transfer_current.cpp                 \
			src/progress_updater.cpp                                    \
			src/http_request_authorization.cpp                          \
			src/http_request_employers.cpp                              \
			src/http_request_departments.cpp                            \
			src/users_updater.cpp                                       \
			src/qtservice/qtservice.cpp									\
			src/qtservice/qtservice_win.cpp								\
