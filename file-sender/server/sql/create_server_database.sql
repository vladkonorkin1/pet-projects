drop table IF EXISTS last_read_messages;
drop table IF EXISTS receivers;
drop table IF EXISTS messages;
drop table IF EXISTS history_messages;
drop table IF EXISTS chat_lists;
drop table IF EXISTS chat_users;
drop table IF EXISTS chats;
drop table IF EXISTS users;
drop table IF EXISTS departments;
drop table IF EXISTS properties;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

create table if not exists properties (
	key text PRIMARY KEY not null,
	value text
);

CREATE TABLE IF NOT EXISTS departments (
	id SERIAL PRIMARY KEY,
	uuid UUID NOT NULL,
	name TEXT,
	timestamp TIMESTAMP with time zone NOT NULL,
	CONSTRAINT department_uuid_key UNIQUE (uuid)
);

CREATE TABLE IF NOT EXISTS users (
	id SERIAL PRIMARY KEY,
	uuid UUID NOT NULL,
	login TEXT,
	name TEXT,
	folder TEXT,
	timestamp TIMESTAMP with time zone NOT NULL,
	department_id INT NOT NULL,
	deleted BOOLEAN NOT NULL DEFAULT false,
	CONSTRAINT user_department_id_fkey FOREIGN KEY (department_id) REFERENCES departments(id),
	CONSTRAINT user_uuid_key UNIQUE (uuid)
);

CREATE TABLE IF NOT EXISTS chats (
	id BIGSERIAL PRIMARY KEY,
	name TEXT, 					-- Null - group chat
	creator_id INT NOT NULL,	-- создатель чата
	timestamp TIMESTAMP with time zone NOT NULL,
	CONSTRAINT chat_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS chat_users (
	chat_id BIGINT NOT NULL,
	user_id INT NOT NULL,			-- участник чата
	admin BOOLEAN NULL,
	CONSTRAINT chat_user_chat_id_fkey FOREIGN KEY (chat_id) REFERENCES chats(id),
	CONSTRAINT chat_user_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id),
	CONSTRAINT chat_user_chat_id_user_id_key PRIMARY KEY (chat_id, user_id)
);

CREATE TABLE IF NOT EXISTS chat_lists (
	user_id INT NOT NULL,
	chat_id BIGINT NOT NULL,
	favorite BOOLEAN NOT NULL,		-- добавил чат у себя в избранные
	last_time TIMESTAMP with time zone NOT NULL,
	CONSTRAINT chat_list_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id),
	CONSTRAINT chat_list_chat_id_fkey FOREIGN KEY (chat_id) REFERENCES chats(id),
	CONSTRAINT chat_list_user_id_chat_id_key PRIMARY KEY (user_id, chat_id)
);

create table if not exists history_messages (
	id BIGINT PRIMARY KEY,
	message_id BIGINT NOT NULL,
	datetime TIMESTAMP with time zone NOT NULL,
	type SMALLINT NOT NULL,		-- create, change, delete
	txt TEXT NULL
);

CREATE TABLE IF NOT EXISTS messages (
	id BIGSERIAL PRIMARY KEY,
	local_id UUID NOT NULL DEFAULT uuid_nil(),
	sender_id INT NOT NULL,
	chat_id BIGINT NOT NULL,
	stamp BIGINT NOT NULL,	-- отметка о правке (удалении) сообщения
	type INT NOT NULL,
	datetime TIMESTAMP with time zone NOT NULL,
	history_status SMALLINT NOT NULL,	-- 0 - обычное сообщение, 1 - сообщение изменялось, 2 - сообщение удалено
	txt TEXT NULL,
	file_src_path TEXT NULL,
	file_is_dir BOOLEAN NULL,
	file_size BIGINT NULL,
	CONSTRAINT message_sender_id_fkey FOREIGN KEY (sender_id) REFERENCES users(id),
	CONSTRAINT message_chat_id_fkey FOREIGN KEY (chat_id) REFERENCES chats(id)
--	CONSTRAINT message_history_messages_id_fkey FOREIGN KEY (stamp) REFERENCES history_messages(id)
);

CREATE UNIQUE INDEX messages_stamp_index ON messages (stamp);

-- DROP TYPE IF EXISTS FileTransferResult;
-- CREATE TYPE FileTransferResult AS ENUM ('Default', 'Success', 'Error', 'Cancel');

CREATE TABLE IF NOT EXISTS receivers (
	message_id BIGINT NOT NULL,
	chat_id BIGINT NOT NULL,	-- duplicate
	receiver_id INT NOT NULL,
	datetime_readed TIMESTAMP with time zone NULL,
	file_dest_name TEXT NULL,
	file_status SMALLINT NULL,
	CONSTRAINT receiver_message_id_fkey FOREIGN KEY (message_id) REFERENCES messages(id),
	CONSTRAINT receiver_chat_id_fkey FOREIGN KEY (chat_id) REFERENCES chats(id),
	CONSTRAINT receiver_receiver_id_fkey FOREIGN KEY (receiver_id) REFERENCES users(id),
	CONSTRAINT receiver_message_id_receiver_id_pkey PRIMARY KEY (message_id, receiver_id)
);


CREATE TABLE IF NOT EXISTS last_read_messages (
	chat_id BIGINT NOT NULL,
	user_id INT NOT NULL,
	last_read_message_id BIGINT NOT NULL default 0,
	CONSTRAINT last_read_message_chat_id_fkey FOREIGN KEY (chat_id) REFERENCES chats(id),
	CONSTRAINT last_read_message_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id),
	CONSTRAINT last_read_message_chat_id_user_id_key PRIMARY KEY (chat_id, user_id)
);

INSERT INTO properties (key, value) VALUES
	('database_timestamp', (select cast(extract(epoch FROM current_timestamp) as BIGINT)) );

/*
INSERT INTO departments (id, uuid, name) VALUES
	(DEFAULT, '{5AAF1AA0-79BA-4BEA-BB6E-FC6063995A50}', 'АХО'),
	(DEFAULT, '{5AAF1AA0-79BA-4BEA-BB6E-FC6063995A51}', 'Бухгалтерия'),
	(DEFAULT, '{5AAF1AA0-79BA-4BEA-BB6E-FC6063995A52}', 'ИТ отдел');
	
INSERT INTO users (id, uuid, login, name, timestamp, department_id, folder) VALUES
	(DEFAULT, '{5AAF1AA0-79BA-4BEA-BB6E-FC6063995A00}', 'kuz_ap', 'Кузьминых', '2019-01-01', 1, ''),
	(DEFAULT, '{5AAF1AA0-79BA-4BEA-BB6E-FC6063995A01}', 'bes', 'Бес', '2019-01-01', 1, ''),
	(DEFAULT, '{5AAF1AA0-79BA-4BEA-BB6E-FC6063995A02}', 'don', 'Дон', '2019-01-01', 1, ''),
	(DEFAULT, '{5AAF1AA0-79BA-4BEA-BB6E-FC6063995A03}', 'plashkevich', 'Плашкевич', '2019-01-01', 1, ''),
	(DEFAULT, '{5AAF1AA0-79BA-4BEA-BB6E-FC6063995A04}', 'open', 'Опен', '2019-01-01', 1, ''),
	(DEFAULT, '{5AAF1AA0-79BA-4BEA-BB6E-FC6063995A05}', 'locki', 'Локки', '2019-01-01', 2, '');
	
INSERT INTO chats (id, timestamp, creator_id, name) VALUES
	(DEFAULT, '2019-02-01', 1, NULL),
	(DEFAULT, '2019-02-01', 1, NULL),
	(DEFAULT, '2019-02-01', 2, NULL),
	(DEFAULT, '2019-02-01', 1, 'Чат системного администратора');
	
INSERT INTO chat_users (chat_id, user_id, admin) VALUES
	(1, 1, NULL),
	(1, 2, NULL),
	(2, 1, NULL),
	(2, 3, NULL),
	(3, 2, NULL),
	(3, 3, NULL),
	(4, 1, true),
	(4, 2, true),
	(4, 3, NULL),
	(4, 6, NULL);
	
INSERT INTO last_read_messages (chat_id, user_id, last_read_message_id) VALUES
	(1, 1, 0),
	(1, 2, 0),
	(2, 1, 1),
	(2, 3, 1),
	(3, 2, 1),
	(3, 3, 1),
	(4, 1, 1),
	--(4, 2, 1),
	(4, 3, 1),
	(4, 6, 2);
	
INSERT INTO chat_lists (user_id, chat_id, favorite, last_time) VALUES
	(1, 1, false, '2018-01-25 06:13:07'),
	-- (1, 2, false, '2017-06-01'), -- чат с Don отстутвует у kuz_ap
	(1, 4, false, '2017-05-01'),
	
	(2, 1, false, '2017-07-11 06:13:07'),
	(2, 3, false, '2017-05-01'),
	(2, 4, false, '2017-05-01'),
	
	(3, 2, false, '2017-06-01'),
	(3, 3, false, '2017-05-01'),
	(3, 4, false, '2017-05-01'),
	
	(6, 4, false, '2017-05-01');
	-- (12, 4, false, '2017-05-01');
	
INSERT INTO messages (id, local_id, sender_id, chat_id, stamp, type, datetime, history_status, txt) VALUES
	(DEFAULT, uuid_generate_v1(), 2, 1, 2, 0, '2016-01-01 00:00:00', 1, '1 (изменённое)'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 30, 0, '2016-01-01 00:00:01', 0, '2 сообщение'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 40, 0, '2016-01-01 00:02:00', 0, '3 сообщение'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 50, 0, '2016-01-01 00:02:00', 0, '4'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 60, 0, '2016-01-01 00:02:00', 0, '5'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 70, 0, '2016-01-01 00:02:00', 0, '6'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 80, 0, '2016-01-01 00:02:00', 0, '7'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 90, 0, '2016-01-01 00:02:00', 0, '8'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 100, 0, '2016-01-01 00:02:00', 0, '9'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 110, 0, '2016-01-01 00:02:00', 0, '10'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 120, 0, '2016-01-01 00:02:00', 0, '11'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 130, 0, '2016-01-01 00:02:00', 0, '12'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 140, 0, '2016-01-01 00:02:00', 0, '13'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 150, 0, '2016-01-01 00:02:00', 0, '14'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 160, 0, '2016-01-01 00:02:00', 0, '15'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 170, 0, '2016-01-01 00:02:00', 0, '16'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 180, 0, '2016-01-01 00:02:00', 0, '17'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 190, 0, '2016-01-01 00:02:00', 0, '18'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 200, 0, '2016-01-01 00:02:00', 0, '19'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 210, 0, '2016-01-01 00:02:00', 0, '20'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 220, 0, '2016-01-01 00:02:00', 0, '31'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 230, 0, '2016-01-01 00:02:00', 0, '32'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 240, 0, '2016-01-01 00:02:00', 0, '33'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 250, 0, '2016-01-01 00:02:00', 0, '34'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 260, 0, '2016-01-01 00:02:00', 0, '35'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 270, 0, '2016-01-01 00:02:00', 0, '36'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 280, 0, '2016-01-01 00:02:00', 0, '37'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 290, 0, '2016-01-01 00:02:00', 0, '38'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 300, 0, '2016-01-01 00:02:00', 0, '39'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 310, 0, '2016-01-01 00:02:00', 0, '40'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 320, 0, '2016-01-01 00:02:00', 0, '41'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 330, 0, '2016-01-01 00:02:00', 0, '42'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 340, 0, '2016-01-01 00:02:00', 0, '43'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 350, 0, '2016-01-01 00:02:00', 0, '44'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 360, 0, '2016-01-01 00:02:00', 0, '45'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 370, 0, '2016-01-01 00:02:00', 0, '46'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 380, 0, '2016-01-01 00:02:00', 0, '47'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 390, 0, '2016-01-01 00:02:00', 0, '48'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 400, 0, '2016-01-01 00:02:00', 0, '49'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 410, 0, '2016-01-01 00:02:00', 0, '50'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 420, 0, '2016-01-01 00:02:00', 0, '51'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 430, 0, '2016-01-01 00:02:00', 0, '52'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 440, 0, '2016-01-01 00:02:00', 0, '53'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 450, 0, '2016-01-01 00:02:00', 0, '54'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 460, 0, '2016-01-01 00:02:00', 0, '55'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 470, 0, '2016-01-01 00:02:00', 0, '56'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 480, 0, '2016-01-01 00:02:00', 0, '57'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 490, 0, '2016-01-01 00:02:00', 0, '58'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 500, 0, '2016-01-01 00:02:00', 0, '59'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 510, 0, '2016-01-01 00:02:00', 0, '60'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 520, 0, '2016-01-01 00:02:00', 0, '61'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 530, 0, '2016-01-01 00:02:00', 0, '62'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 540, 0, '2016-01-01 00:02:00', 0, '63'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 550, 0, '2016-01-01 00:02:00', 0, '64'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 560, 0, '2016-01-01 00:02:00', 0, '65'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 570, 0, '2016-01-01 00:02:00', 0, '66'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 580, 0, '2016-01-01 00:02:00', 0, '67'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 590, 0, '2016-01-01 00:02:00', 0, '68'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 600, 0, '2016-01-01 00:02:00', 0, '69'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 610, 0, '2016-01-01 00:02:00', 0, '70'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 620, 0, '2016-01-01 00:02:00', 0, '71'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 630, 0, '2016-01-01 00:02:00', 0, '72'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 640, 0, '2016-01-01 00:02:00', 0, '73'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 650, 0, '2016-01-01 00:02:00', 0, '74'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 660, 0, '2016-01-01 00:02:00', 0, '75'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 670, 0, '2016-01-01 00:02:00', 0, '76'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 680, 0, '2016-01-01 00:02:00', 0, '77'),
	(DEFAULT, uuid_generate_v1(), 1, 1, 690, 0, '2016-01-01 00:02:00', 0, '78'),
	(DEFAULT, uuid_generate_v1(), 2, 1, 700, 0, '2016-01-01 00:02:00', 0, '79');
	
INSERT INTO history_messages(id, message_id, datetime, type, txt) VALUES
	(1, 1, '2018-01-01 00:00:00', 0, 'Первое сообщение'),
	(20, 1, '2018-01-01 00:00:01', 1, 'Первое сообщение (изменённое)'),
	(30, 2, '2018-01-01 00:00:00', 0, 'Второе сообщение'),
	(40, 3, '2018-01-01 00:00:02', 0, 'Третье сообщение'),
	(50, 4, '2018-01-01 00:00:02', 0, 'no message'),
	(60, 5, '2018-01-01 00:00:02', 0, 'no message'),
	(70, 6, '2018-01-01 00:00:02', 0, 'no message'),
	(80, 7, '2018-01-01 00:00:02', 0, 'no message'),
	(90, 8, '2018-01-01 00:00:02', 0, 'no message'),
	(100, 9, '2018-01-01 00:00:02', 0, 'no message'),
	(110, 10, '2018-01-01 00:00:02', 0, 'no message'),
	(120, 11, '2018-01-01 00:00:02', 0, 'no message'),
	(130, 12, '2018-01-01 00:00:02', 0, 'no message'),
	(140, 13, '2018-01-01 00:00:02', 0, 'no message'),
	(150, 14, '2018-01-01 00:00:02', 0, 'no message'),
	(160, 15, '2018-01-01 00:00:02', 0, 'no message'),
	(170, 16, '2018-01-01 00:00:02', 0, 'no message'),
	(180, 17, '2018-01-01 00:00:02', 0, 'no message'),
	(190, 18, '2018-01-01 00:00:02', 0, 'no message'),
	(200, 19, '2018-01-01 00:00:02', 0, 'no message'),
	(210, 20, '2018-01-01 00:00:02', 0, 'no message'),
	(220, 21, '2018-01-01 00:00:02', 0, 'no message'),
	(230, 22, '2018-01-01 00:00:02', 0, 'no message'),
	(240, 23, '2018-01-01 00:00:02', 0, 'no message'),
	(250, 24, '2018-01-01 00:00:02', 0, 'no message'),
	(260, 25, '2018-01-01 00:00:02', 0, 'no message'),
	(270, 26, '2018-01-01 00:00:02', 0, 'no message'),
	(280, 27, '2018-01-01 00:00:02', 0, 'no message'),
	(290, 28, '2018-01-01 00:00:02', 0, 'no message'),
	(300, 29, '2018-01-01 00:00:02', 0, 'no message'),
	(310, 30, '2018-01-01 00:00:02', 0, 'no message'),
	(320, 31, '2018-01-01 00:00:02', 0, 'no message'),
	(330, 32, '2018-01-01 00:00:02', 0, 'no message'),
	(340, 33, '2018-01-01 00:00:02', 0, 'no message'),
	(350, 34, '2018-01-01 00:00:02', 0, 'no message'),
	(360, 35, '2018-01-01 00:00:02', 0, 'no message'),
	(370, 36, '2018-01-01 00:00:02', 0, 'no message'),
	(380, 37, '2018-01-01 00:00:02', 0, 'no message'),
	(390, 38, '2018-01-01 00:00:02', 0, 'no message'),
	(400, 39, '2018-01-01 00:00:02', 0, 'no message'),
	(410, 40, '2018-01-01 00:00:02', 0, 'no message'),
	(420, 41, '2018-01-01 00:00:02', 0, 'no message'),
	(430, 42, '2018-01-01 00:00:02', 0, 'no message'),
	(440, 43, '2018-01-01 00:00:02', 0, 'no message'),
	(450, 44, '2018-01-01 00:00:02', 0, 'no message'),
	(460, 45, '2018-01-01 00:00:02', 0, 'no message'),
	(470, 46, '2018-01-01 00:00:02', 0, 'no message'),
	(480, 47, '2018-01-01 00:00:02', 0, 'no message'),
	(490, 48, '2018-01-01 00:00:02', 0, 'no message'),
	(500, 49, '2018-01-01 00:00:02', 0, 'no message'),
	(510, 50, '2018-01-01 00:00:02', 0, 'no message'),
	(520, 51, '2018-01-01 00:00:02', 0, 'no message'),
	(530, 52, '2018-01-01 00:00:02', 0, 'no message'),
	(540, 53, '2018-01-01 00:00:02', 0, 'no message'),
	(550, 54, '2018-01-01 00:00:02', 0, 'no message'),
	(560, 55, '2018-01-01 00:00:02', 0, 'no message'),
	(570, 56, '2018-01-01 00:00:02', 0, 'no message'),
	(580, 57, '2018-01-01 00:00:02', 0, 'no message'),
	(590, 58, '2018-01-01 00:00:02', 0, 'no message'),
	(600, 59, '2018-01-01 00:00:02', 0, 'no message'),
	(610, 60, '2018-01-01 00:00:02', 0, 'no message'),
	(620, 61, '2018-01-01 00:00:02', 0, 'no message'),
	(630, 62, '2018-01-01 00:00:02', 0, 'no message'),
	(640, 63, '2018-01-01 00:00:02', 0, 'no message'),
	(650, 64, '2018-01-01 00:00:02', 0, 'no message'),
	(660, 65, '2018-01-01 00:00:02', 0, 'no message'),
	(670, 66, '2018-01-01 00:00:02', 0, 'no message'),
	(680, 67, '2018-01-01 00:00:02', 0, 'no message'),
	(690, 68, '2018-01-01 00:00:02', 0, 'no message'),
	(700, 69, '2018-01-01 00:00:02', 0, 'no message');
	
	
INSERT INTO receivers (message_id, chat_id, receiver_id, datetime_readed) VALUES
	(1, 1, 1, NULL),
	(1, 1, 2, NULL),
	(2, 1, 1, NULL),
	(2, 1, 2, NULL),
	(3, 1, 1, NULL),
	(3, 1, 2, NULL),
	(4, 1, 1, NULL),
	(4, 1, 2, NULL),
	(5, 1, 1, NULL),
	(5, 1, 2, NULL),
	(6, 1, 1, NULL),
	(6, 1, 2, NULL),
	(7, 1, 1, NULL),
	(7, 1, 2, NULL),
	(8, 1, 1, NULL),
	(8, 1, 2, NULL),
	(9, 1, 1, NULL),
	(9, 1, 2, NULL),
	(10, 1, 1, NULL),
	(10, 1, 2, NULL),
	(11, 1, 1, NULL),
	(11, 1, 2, NULL),
	(12, 1, 1, NULL),
	(12, 1, 2, NULL),
	(13, 1, 1, NULL),
	(13, 1, 2, NULL),
	(14, 1, 1, NULL),
	(14, 1, 2, NULL),
	(15, 1, 1, NULL),
	(15, 1, 2, NULL),
	(16, 1, 1, NULL),
	(16, 1, 2, NULL),
	(17, 1, 1, NULL),
	(17, 1, 2, NULL),
	(18, 1, 1, NULL),
	(18, 1, 2, NULL),
	(19, 1, 1, NULL),
	(19, 1, 2, NULL),
	(20, 1, 1, NULL),
	(20, 1, 2, NULL),
	(21, 1, 1, NULL),
	(21, 1, 2, NULL),
	(22, 1, 1, NULL),
	(22, 1, 2, NULL),
	(23, 1, 1, NULL),
	(23, 1, 2, NULL),
	(24, 1, 1, NULL),
	(24, 1, 2, NULL),
	(25, 1, 1, NULL),
	(25, 1, 2, NULL),
	(26, 1, 1, NULL),
	(26, 1, 2, NULL),
	(27, 1, 1, NULL),
	(27, 1, 2, NULL),
	(28, 1, 1, NULL),
	(28, 1, 2, NULL),
	(29, 1, 1, NULL),
	(29, 1, 2, NULL),
	(30, 1, 1, NULL),
	(30, 1, 2, NULL),
	(31, 1, 1, NULL),
	(31, 1, 2, NULL),
	(32, 1, 1, NULL),
	(32, 1, 2, NULL),
	(33, 1, 1, NULL),
	(33, 1, 2, NULL),
	(34, 1, 1, NULL),
	(34, 1, 2, NULL),
	(35, 1, 1, NULL),
	(35, 1, 2, NULL),
	(36, 1, 1, NULL),
	(36, 1, 2, NULL),
	(37, 1, 1, NULL),
	(37, 1, 2, NULL),
	(38, 1, 1, NULL),
	(38, 1, 2, NULL),
	(39, 1, 1, NULL),
	(39, 1, 2, NULL),
	(40, 1, 1, NULL),
	(40, 1, 2, NULL),
	(41, 1, 1, NULL),
	(41, 1, 2, NULL),
	(42, 1, 1, NULL),
	(42, 1, 2, NULL),
	(43, 1, 1, NULL),
	(43, 1, 2, NULL),
	(44, 1, 1, NULL),
	(44, 1, 2, NULL),
	(45, 1, 1, NULL),
	(45, 1, 2, NULL),
	(46, 1, 1, NULL),
	(46, 1, 2, NULL),
	(47, 1, 1, NULL),
	(47, 1, 2, NULL),
	(48, 1, 1, NULL),
	(48, 1, 2, NULL),
	(49, 1, 1, NULL),
	(49, 1, 2, NULL),
	(50, 1, 1, NULL),
	(50, 1, 2, NULL),
	(51, 1, 1, NULL),
	(51, 1, 2, NULL),
	(52, 1, 1, NULL),
	(52, 1, 2, NULL),
	(53, 1, 1, NULL),
	(53, 1, 2, NULL),
	(54, 1, 1, NULL),
	(54, 1, 2, NULL),
	(55, 1, 1, NULL),
	(55, 1, 2, NULL),
	(56, 1, 1, NULL),
	(56, 1, 2, NULL),
	(57, 1, 1, NULL),
	(57, 1, 2, NULL),
	(58, 1, 1, NULL),
	(58, 1, 2, NULL),
	(59, 1, 1, NULL),
	(59, 1, 2, NULL),
	(60, 1, 1, NULL),
	(60, 1, 2, NULL),
	(61, 1, 1, NULL),
	(61, 1, 2, NULL),
	(62, 1, 1, NULL),
	(62, 1, 2, NULL),
	(63, 1, 1, NULL),
	(63, 1, 2, NULL),
	(64, 1, 1, NULL),
	(64, 1, 2, NULL),
	(65, 1, 1, NULL),
	(65, 1, 2, NULL),
	(66, 1, 1, NULL),
	(66, 1, 2, NULL),
	(67, 1, 1, NULL),
	(67, 1, 2, NULL),
	(68, 1, 1, NULL),
	(68, 1, 2, NULL),
	(69, 1, 1, NULL),
	(69, 1, 2, NULL);
	
	
INSERT INTO messages (id, local_id, sender_id, chat_id, stamp, type, datetime, history_status, file_src_path, file_is_dir, file_size) VALUES
	(DEFAULT, uuid_generate_v1(), 1, 1, 701, 1, '2020-01-01 00:00:00', 1, 'd:/переданный файлик.txt', false, 100);
	
INSERT INTO receivers (message_id, chat_id, receiver_id, datetime_readed, file_dest_name, file_status) VALUES
	(70, 1, 1, '2021-01-01 00:00:00', 'переданный файлик (100).txt', 1),
	(70, 1, 2, NULL, NULL, NULL);*/