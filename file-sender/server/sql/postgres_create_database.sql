sudo -u postgres psql
postgres=# create database "sima-chat";
postgres=# create user "sima-user" with encrypted password 'darvin';
postgres=# ALTER ROLE "sima-user" SUPERUSER;
postgres=# grant all privileges on database "sima-chat" to "sima-user";