//=====================================================================================//
//   Author: open
//   Date:   26.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "chat.h"
#include "user.h"

Chat::Chat(ChatId id, const User* creator_user, bool is_group)
: m_id(id)
, m_creator_user(creator_user)
, m_last_message_id(0)
, m_last_message_stamp(0)
, m_timestamp(0)
//, m_is_users_sorted(false)
, m_is_group(is_group)
, m_database_loaded(false)
, m_prediction_add_message(0)
{
}
Chat::~Chat()
{
	for (auto pair : m_users)
		delete pair.second;
}
ChatUser* Chat::add_user(const User* user, bool admin)
{
	assert( !find_user(user->id) );
	//m_is_users_sorted = false;
	return m_users.insert(std::make_pair(user->id, new ChatUser{ user, admin })).first->second;
}
bool Chat::remove_user(UserId user_id)
{
	users_t::iterator it = m_users.find(user_id);
	if (it != m_users.end())
	{
		//m_is_users_sorted = false;
		delete it->second;
		m_users.erase(it);
		return true;
	}
	return false;
}
bool Chat::set_or_remove_admin(UserId user_id, bool add_or_remove)
{
	users_t::iterator it = m_users.find(user_id);
	if (it != m_users.end())
	{
		it->second->admin = add_or_remove;
		return true;
	}
	return false;
}
ChatUser* Chat::find_user(UserId user_id) const
{
	users_t::const_iterator it = m_users.find(user_id);
	if (it != m_users.end()) return it->second;
	return nullptr;
}
ChatUser* Chat::find_another_user(UserId user_id) const
{
	assert( !m_is_group );
	if (m_users.size() >= 2) {
		users_t::const_iterator it = m_users.begin();
		if (it->first == user_id) return (++it)->second;
		else return it->second;
	}
	return nullptr;
}
/*const Chat::sorted_users_t& Chat::sorted_users()
{
	if (!m_is_users_sorted)
	{
		m_is_users_sorted = true;
		m_sorted_users.clear();

		for (auto pair : m_users)
			m_sorted_users.push_back(pair.second);

		// сортировка по дате добавления
		//std::sort(m_sorted_users.begin(), m_sorted_users.end(), [](const ChatUser* x, const ChatUser* y) { return x->creation_time < y->creation_time; });

		// сортируем по фио
		std::sort(m_sorted_users.begin(), m_sorted_users.end(), [](const ChatUser* l, const ChatUser* r) { return l->user->name.toLower() < r->user->name.toLower(); });
	}
	return m_sorted_users;
}*/