//=====================================================================================//
//   Author: open
//   Date:   20.02.2019
//=====================================================================================//
#pragma once

class User;

struct ChatUser
{
	const User* user;
	bool admin;
};

class Chat
{
public:
	using users_t = std::map<UserId, ChatUser*>;
	//using sorted_users_t = std::vector<const ChatUser*>;

private:
	ChatId m_id;
	const User* m_creator_user;
	QString m_name;
	ChatMessageId m_last_message_id;
	ChatMessageStamp m_last_message_stamp;
	Timestamp m_timestamp;
	users_t m_users;
	bool m_is_group;
	bool m_database_loaded;
	int m_prediction_add_message;	// счетчик начала добавления сообщения

public:
	Chat(ChatId id, const User* creator_user, bool is_group);
	virtual ~Chat();

	ChatId id() const;
	const QString& name() const;
	bool is_group() const;
	ChatMessageId last_message_id() const;
	Timestamp timestamp() const;
	ChatMessageStamp last_message_stamp() const;

	void set_name(const QString& name);
	void set_last_message_id(ChatMessageId message_id);
	void set_timestamp(Timestamp timestamp);
	void set_last_message_stamp(ChatMessageStamp last_message_stamp);


	ChatUser* add_user(const User* user, bool admin);
	bool remove_user(UserId user_id);
	bool set_or_remove_admin(UserId user_id, bool add_or_remove);
	ChatUser* find_user(UserId user_id) const;
	ChatUser* find_another_user(UserId user_id) const;
	const users_t& users() const;
	//const sorted_users_t& sorted_users();

	bool is_database_loaded() const;
	void set_database_loaded();

	bool prediction_add_message() const { return m_prediction_add_message != 0; }
	void start_prediction_add_message();
	void finish_prediction_add_message();
};

inline void Chat::set_timestamp(Timestamp timestamp) {
	m_timestamp = timestamp;
}
inline Timestamp Chat::timestamp() const {
	return m_timestamp;
}
inline void Chat::set_last_message_id(ChatMessageId message_id) {
	m_last_message_id = message_id;
}
inline void Chat::set_last_message_stamp(ChatMessageStamp last_message_stamp) {
	m_last_message_stamp = last_message_stamp;
}
inline void Chat::set_name(const QString& name) {
	m_name = name;
}
inline ChatId Chat::id() const {
	return m_id;
}
inline const QString& Chat::name() const {
	return m_name;
}
inline bool Chat::is_group() const {
	return m_is_group;
}
inline const Chat::users_t& Chat::users() const {
	return m_users;
}
inline ChatMessageId Chat::last_message_id() const {
	return m_last_message_id;
}
inline ChatMessageId Chat::last_message_stamp() const {
	return m_last_message_stamp;
}
inline bool Chat::is_database_loaded() const {
	return m_database_loaded;
}
inline void Chat::set_database_loaded() {
	m_database_loaded = true;
}
inline void Chat::start_prediction_add_message()
{
	++m_prediction_add_message;
}
inline void Chat::finish_prediction_add_message()
{
	--m_prediction_add_message;
}