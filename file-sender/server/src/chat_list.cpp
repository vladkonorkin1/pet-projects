//=====================================================================================//
//   Author: open
//   Date:   22.07.2017
//=====================================================================================//
#include "base/precomp.h"
#include "chat_list.h"
#include "database/async/queries/database_async_query_chat.h"
#include "database/async/database_async.h"
#include "chat.h"
#include "user.h"

ChatList::ChatList(const User* user, DatabaseAsync& database_async)
: m_database_async(database_async)
, m_user(user)
, m_actual_timestamp_chats(false)
{
}
ChatList::~ChatList()
{
	for (auto pair : m_chats)
		delete pair.second;
}
ChatListContact* ChatList::add_chat(Chat* chat, bool favorite, const QDateTime& last_time)
{
	m_actual_timestamp_chats = false;
	assert( !find_chat(chat->id()) );
	ChatListContact* list_item = new ChatListContact{ chat, last_time, favorite };
	m_chats[chat->id()] = list_item;
	return list_item;
}
ChatListContact* ChatList::find_chat(ChatId chat_id) const
{
	chats_t::const_iterator it = m_chats.find(chat_id);
	if (it != m_chats.end()) return it->second;
	return nullptr;
}
bool ChatList::remove_chat(ChatId chat_id)
{
	m_actual_timestamp_chats = false;
	chats_t::const_iterator it = m_chats.find(chat_id);
	if (it != m_chats.end())
	{
		delete it->second;
		m_chats.erase(it);
		return true;
	}
	return false;
}
bool ChatList::set_chat_favorite(Chat* chat, bool favorite)
{
	if (ChatListContact* contact = find_chat(chat->id()))
	{
		if (contact->favorite != favorite)
		{
			contact->favorite = favorite;
			m_database_async.post_query(new SetChatListContactFavoriteQuery(m_user->id, chat->id(), favorite));
			return true;
		}
	}
	return false;
}
void ChatList::set_chat_last_time(Chat* chat, const QDateTime& last_time, bool store_database)
{
	if (ChatListContact* clc = find_chat(chat->id()))
	{
		clc->last_time = last_time;
		if (store_database) m_database_async.post_query(new SetChatListContactLastTimeQuery(m_user->id, chat->id(), last_time));
	}
}
bool ChatList::remove_chat(const Chat* chat)
{
	bool ok = remove_chat(chat->id());
	if (ok)
		m_database_async.post_query(new RemoveChatFromChatListQuery(m_user->id, chat->id()));
	return ok;
}
// получить список отсортированных чатов по таймштампу
const ChatList::timestamp_chats_t& ChatList::timestamp_chats()
{
	if (!m_actual_timestamp_chats)
	{
		m_actual_timestamp_chats = true;
		m_timestamp_chats.clear();
		for (const auto& pair : m_chats)
			m_timestamp_chats.push_back(pair.second->chat);
		std::sort(m_timestamp_chats.begin(), m_timestamp_chats.end(), [this](const Chat* left, const Chat* right) { return left->timestamp() < right->timestamp(); });
	}
	return m_timestamp_chats;
}