//=====================================================================================//
//   Author: open
//   Date:   22.07.2017
//=====================================================================================//
#pragma once
#include <QDateTime>

class DatabaseAsync;
class User;
class Chat;

struct ChatListContact
{
	Chat* chat;
	QDateTime last_time;
	bool favorite;
};

class ChatList
{
public:
	using chats_t = std::map<ChatId, ChatListContact*>;
	using timestamp_chats_t = std::vector<const Chat*>;	// отсортированы по timestamp'у

private:
	DatabaseAsync& m_database_async;
	const User* m_user;
	chats_t m_chats;

	bool m_actual_timestamp_chats;
	timestamp_chats_t m_timestamp_chats;

public:
	ChatList(const User* user, DatabaseAsync& database_async);
	virtual ~ChatList();

	ChatListContact* add_chat(Chat* chat, bool favorite, const QDateTime& last_time);
	bool remove_chat(const Chat* chat);
	const chats_t& chats() const;
	// получить список отсортированных чатов по таймштампу
	const timestamp_chats_t& timestamp_chats();

	bool set_chat_favorite(Chat* chat, bool favorite);
	void set_chat_last_time(Chat* chat, const QDateTime& last_time, bool store_database = true);

	ChatListContact* find_chat(ChatId chat_id) const;
	const User* user() const;

private:
	bool remove_chat(ChatId chat_id);
};


inline const User* ChatList::user() const {
	return m_user;
}
inline const ChatList::chats_t& ChatList::chats() const {
	return m_chats;
}