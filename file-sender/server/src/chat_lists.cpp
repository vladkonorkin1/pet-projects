//=====================================================================================//
//   Author: open
//   Date:   22.07.2017
//=====================================================================================//
#include "base/precomp.h"
#include "chat_lists.h"
#include "chat_list.h"
#include "database/async/queries/database_async_query_chat.h"
#include "database/async/database_async.h"
#include "database/database_chats.h"
#include "chats.h"
#include "users.h"

ChatLists::ChatLists(const Users& users, const Chats& chats, DatabaseAsync& database_async, DatabaseChats& database_chats)
: m_database_async(database_async)
, m_database_chats(database_chats)
, m_users(users)
, m_chats(chats)
{
	connect(&m_database_chats, SIGNAL(load_chat_list(UserId, ChatId, bool, const QDateTime&)), SLOT(slot_load_chat_list(UserId, ChatId, bool, const QDateTime&)));
}
ChatLists::~ChatLists()
{
	for (auto pair : m_chat_lists)
		delete pair.second;
}
void ChatLists::slot_load_chat_list(UserId user_id, ChatId chat_id, bool favorite, const QDateTime& last_time)
{
	if (const User* user = m_users.find_user(user_id))
	{
		if (Chat* chat = m_chats.find_chat(chat_id))
		{
			if (ChatList* chat_list = create_chat_list(user))
			{
				ChatListContact* chat_list_contact = chat_list->add_chat(chat, favorite, last_time);
				if (chat_list_contact)
					register_contact_chat_where_iam(chat, user);
			}
		}
	}
}
ChatList* ChatLists::create_chat_list(const User* user)
{
	auto it = m_chat_lists.find(user->id);
	if (it == m_chat_lists.end())
		it = m_chat_lists.insert(std::make_pair(user->id, new ChatList(user, m_database_async))).first;
	return it->second;
}
ChatList* ChatLists::get_chat_list(const User* user)
{
	auto it = m_chat_lists.find(user->id);
	if (it == m_chat_lists.end())
	{
		//if (m_database_load_chats->load_chats(contact))
		//	__nop();

		m_database_chats.load_chats(user);

		// вернет загруженный из базы, иначе создаст пустой список
		return create_chat_list(user);
	}
	return it->second;
}
void ChatLists::set_chat_last_time(Chat* chat, const QDateTime& last_time)
{
	const Chat::users_t& chat_users = chat->users();
	for (auto pair : chat_users)
	{
		const ChatUser* chat_user = pair.second;
		ChatList* chat_list = get_chat_list(chat_user->user);
		chat_list->set_chat_last_time(chat, last_time, false);
	}
	m_database_async.post_query(new SetChatListLastTimeQuery(chat->id(), last_time));
}
// добавить чат в чат-лист
const ChatListContact* ChatLists::add_chat(ChatList* chat_list, Chat* chat, bool favorite, const QDateTime& last_time)
{
	if (!chat_list->find_chat(chat->id()))
	{
		if (ChatListContact* chat_list_contact = chat_list->add_chat(chat, favorite, last_time))
		{
			register_contact_chat_where_iam(chat, chat_list->user());
			m_database_async.post_query(new AddChatToChatListQuery(chat_list->user()->id, chat->id(), favorite, last_time));
			return chat_list_contact;
		}
	}
	return nullptr;
}
// у кого я есть в чат-листах
const std::set<UserId>& ChatLists::find_contact_chat_where_iam(UserId user_id) const
{
	auto it = m_where_iam.find(user_id);
	if (m_where_iam.end() != it)
		return it->second;

	static contact_chats_t temp;
	return temp;
}
// зарегистрировать чат на контакт (для создания мапы у каких пользователей есть чат со мной)
void ChatLists::register_contact_chat_where_iam(Chat* chat, const User* user)
{
	if (!chat->is_group())
	{
		if (ChatUser* opponent_user = chat->find_another_user(user->id))
		{
			auto it = m_where_iam.find(opponent_user->user->id);
			if (it == m_where_iam.end())
				it = m_where_iam.insert(std::make_pair(opponent_user->user->id, contact_chats_t())).first;

			contact_chats_t& contact_chats = it->second;
			//contact_chats[user->id] = ContactChatList{ chat_list, chat_list_contact };
			contact_chats.insert(user->id);
		}
	}
}
// снять регистрацию чата с контакта
void ChatLists::unregister_contact_chat_where_iam(Chat* chat, const User* user)
{
	if (!chat->is_group())
	{
		if (ChatUser* opponent_user = chat->find_another_user(user->id))
		{
			UserId opponent_user_id = opponent_user->user->id;
			auto it = m_where_iam.find(opponent_user_id);
			if (m_where_iam.end() != it)
			{
				contact_chats_t& contact_chats = it->second;
				contact_chats.erase(user->id);

				if (contact_chats.empty())
					m_where_iam.erase(opponent_user_id);
			}
		}
	}
}
// удалить чат из чат-листа
bool ChatLists::remove_chat(ChatList* chat_list, Chat* chat)
{
	if (chat_list->remove_chat(chat))
	{
		unregister_contact_chat_where_iam(chat, chat_list->user());
		return true;
	}
	return false;
}