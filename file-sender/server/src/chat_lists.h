//=====================================================================================//
//   Author: open
//   Date:   22.07.2017
//=====================================================================================//
#pragma once

struct ChatListContact;
class DatabaseAsync;
class DatabaseChats;
class ChatList;
class Users;
class Chats;
class User;
class Chat;

class ChatLists : public QObject
{
	Q_OBJECT
private:
	DatabaseAsync& m_database_async;
	DatabaseChats& m_database_chats;
	const Users& m_users;
	const Chats& m_chats;

	std::map<UserId, ChatList*> m_chat_lists;		// чат-листы по пользователям


	//struct ContactChatList
	//{
	//	const ChatList* chat_list;
	//	const ChatListContact* chat_list_contact;
	//};
	//using contact_chats_t = std::map<UserId, ContactChatList>;
	using contact_chats_t = std::set<UserId>;
	// у кого я есть в чат-листах
	// у каких пользователей есть чат со мной
	std::map<UserId, contact_chats_t> m_where_iam;

public:
	ChatLists(const Users& users, const Chats& chats, DatabaseAsync& database_async, DatabaseChats& database_chats);
	virtual ~ChatLists();
	// запросить чат-лист для пользователя
	ChatList* get_chat_list(const User* user);
	// установить время последнего обновления чат-листа
	void set_chat_last_time(Chat* chat, const QDateTime& last_time);
	// добавить чат в чат-лист
	const ChatListContact* add_chat(ChatList* chat_list, Chat* chat, bool favorite, const QDateTime& last_time);
	// удалить чат из чат-листа
	bool remove_chat(ChatList* chat_list, Chat* chat);
	// у кого я есть в чат-листах
	const std::set<UserId>& find_contact_chat_where_iam(UserId user_id) const;

private slots:
	void slot_load_chat_list(UserId user_id, ChatId chat_id, bool favorite, const QDateTime& last_time);

private:
	// создать чат-лист
	ChatList* create_chat_list(const User* user);
	// зарегистрировать чат на контакт (для создания мапы у каких пользователей есть чат со мной)
	void register_contact_chat_where_iam(Chat* chat, const User* user);
	// снять регистрацию чата с контакта
	void unregister_contact_chat_where_iam(Chat* chat, const User* user);
};