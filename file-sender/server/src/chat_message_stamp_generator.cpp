//=====================================================================================//
//   Author: open
//   Date:   08.06.2019
//=====================================================================================//
#include "base/precomp.h"
#include "chat_message_stamp_generator.h"
#include "database/async/queries/database_async_chat_messages.h"
#include "database/async/database_async.h"

ChatMessageStampGenerator::ChatMessageStampGenerator(DatabaseAsync& database)
: m_stamp(0)
{
	DBQueryMaxChatMessageStamp* query = new DBQueryMaxChatMessageStamp();
	connect(query, SIGNAL(loaded(ChatMessageStamp)), SLOT(slot_max_stamp_database_loaded(ChatMessageStamp)));
	database.post_query(query);
}
// получить следующий уникальный штамп сообщения чата
ChatMessageStamp ChatMessageStampGenerator::next_stamp()
{
	return ++m_stamp;
}
void ChatMessageStampGenerator::slot_max_stamp_database_loaded(ChatMessageStamp stamp)
{
	m_stamp = stamp;
}