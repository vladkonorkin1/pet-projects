//=====================================================================================//
//   Author: open
//   Date:   08.06.2019
//=====================================================================================//
#pragma once

class DatabaseAsync;

class ChatMessageStampGenerator : public QObject
{
	Q_OBJECT
private:
	ChatMessageStamp m_stamp;

public:
	ChatMessageStampGenerator(DatabaseAsync& database);
	// получить следующий уникальный штамп сообщения чата
	ChatMessageStamp next_stamp();

private slots:
	void slot_max_stamp_database_loaded(ChatMessageStamp stamp);
};