//=====================================================================================//
//   Author: open
//   Date:   18.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "chats.h"
#include "database/async/queries/database_async_query_chat.h"
#include "database/async/database_async.h"
#include "database/database_chats.h"
#include "users.h"
#include <QVariant>

Chats::Chats(const Users& users, DatabaseChats& database_chats, DatabaseAsync& database_async)
: m_database_chats(database_chats)
, m_database_async(database_async)
, m_users(users)
{
	connect(&m_database_chats, SIGNAL(load_chat(ChatId, const QString&, bool, UserId, Timestamp, ChatMessageId, ChatMessageStamp)), SLOT(slot_load_chat(ChatId, const QString&, bool, UserId, Timestamp, ChatMessageId, ChatMessageStamp)));
	connect(&m_database_chats, SIGNAL(load_chat_user(ChatId, UserId, bool)), SLOT(slot_load_chat_user(ChatId, UserId, bool)));
	connect(&m_database_chats, SIGNAL(load_chat_finished(const std::vector<ChatId>&)), SLOT(slot_load_chat_finished(const std::vector<ChatId>&)));
}
Chats::~Chats()
{
	for (auto pair : m_chats)
		delete pair.second;
}
/*void Chats::set_database(DatabaseQueryChats* database_query_chats, AsyncDatabaseQuery* async_database_query)
{
	assert( !m_database_query_chats );
	m_database_query_chats = database_query_chats;
	m_async_database_query = async_database_query;
	
	connect(m_database_query_chats, SIGNAL(load_chat(ChatId, const QString&, bool, ContactId, bool, MessageId)), SLOT(slot_load_chat(ChatId, const QString&, bool, ContactId, bool, MessageId)));
	connect(m_database_query_chats, SIGNAL(load_chat_users(ChatId, ContactId, bool)), SLOT(slot_load_chat_users(ChatId, ContactId, bool)));
	connect(m_database_query_chats, SIGNAL(load_chat_finished(const DatabaseQueryChats::chats_t&)), SLOT(slot_load_chat_finished(const DatabaseQueryChats::chats_t&)));
}*/
/*void Chats::database_load_chats(const User* user)
{
	m_database_chats.query_chats(user);
}*/
Chat* Chats::find_chat(ChatId id) const
{
	chats_t::const_iterator it = m_chats.find(id);
	if (it != m_chats.end()) return it->second;
	return nullptr;
}
// создание чата по загрузке из базы (неизбежна подгрузка дубликатов)
void Chats::slot_load_chat(ChatId chat_id, const QString& name, bool is_group, UserId creator_id, Timestamp timestamp, ChatMessageId last_message_id, ChatMessageStamp last_message_stamp)
{
	create_chat(chat_id, name, is_group, m_users.find_user(creator_id), timestamp, last_message_id, last_message_stamp);
}
void Chats::slot_load_chat_user(ChatId chat_id, UserId user_id, bool admin)
{
	if (Chat* chat = find_chat(chat_id))
	{
		if (!chat->is_database_loaded())
		{
			assert( !chat->find_user(user_id) );
			add_chat_user(chat, user_id, admin);
		}
	}
}
void Chats::slot_load_chat_finished(const std::vector<ChatId>& chats)
{
	for (auto chat_id : chats)
	{
		if (Chat* chat = find_chat(chat_id))
			chat->set_database_loaded();
	}
}
Chat* Chats::create_chat(ChatId chat_id, const QString& name, bool is_group, const User* creator_contact, Timestamp timestamp, ChatMessageId last_message_id, ChatMessageStamp last_message_stamp)
{
	if (chat_id > 0 && !find_chat(chat_id))
	{
		Chat* chat = new Chat(chat_id, creator_contact, is_group);
		chat->set_name(name);
		chat->set_timestamp(timestamp);
		chat->set_last_message_id(last_message_id);
		chat->set_last_message_stamp(last_message_stamp);
		//m_chats.insert(std::make_pair(chat_id, chat));
		m_chats[chat_id] = chat;
		return chat;
	}
	return nullptr;
}
// добавление участника в чат
ChatUser* Chats::add_chat_user(Chat* chat, ChatId user_id, bool admin)
{
	ChatUser* user = chat->add_user(m_users.find_user(user_id), admin);
	register_chat_by_user(user_id, chat);
	return user;
}
void Chats::create_contact_chat(UCreateContactChat creator)
{
	// если желаем создать чат с самим собой, то вежливо отказываем
	if (creator->creator_user() == creator->target_contact()) creator->chat_created(nullptr);
	else
	{
		// ищем, есть ли такой чат с таким участником уже
		ChatUser* user = nullptr;
		if (Chat* chat = find_contact_chat(creator->creator_user(), creator->target_contact(), &user))
		{
			// чат уже с таким участником создан, просто оповещаем
			emit creator->chat_created(chat);
		}
		else
		{
			// чата с таким клиентом не существует
			CreateChat::chat_users_t chat_users;
			chat_users.reserve(2);
			chat_users.emplace_back(CreateChat::ChatUser{ creator->creator_user(), false });
			if (creator->creator_user() != creator->target_contact())
				chat_users.emplace_back(CreateChat::ChatUser{ creator->target_contact(), false });

			CreateChatQuery* query = new CreateChatQuery(creator, chat_users);
			connect(query, SIGNAL(chat_created(ChatId, CreateChat*, const CreateChat::chat_users_t&)), SLOT(slot_database_chat_created(ChatId, CreateChat*, const CreateChat::chat_users_t&)));
			m_database_async.post_query(query);
		}
	}
}
void Chats::slot_database_chat_created(ChatId chat_id, CreateChat* creator, const CreateChat::chat_users_t& chat_users)
{
	QVariant name = creator->name();
	Chat* chat = create_chat(chat_id, name.toString(), !name.isNull(), creator->creator_user(), creator->timestamp(), 0, 0);
	if (chat)
	{
		// считаем, что для нового чата нет необходимости подгружать из базы
		chat->set_database_loaded();

		for (const CreateChat::ChatUser& chat_user : chat_users)
			add_chat_user(chat, chat_user.user->id, chat_user.admin);
	}
	emit creator->chat_created(chat);
}
void Chats::create_group_chat(UCreateGroupChat creator)
{
	size_t user_size = creator->chat_users().size();
	if (user_size >= 3)	// требуется 3 и более участников
	{
		CreateChat::chat_users_t chat_users;
		chat_users.reserve(user_size);
		for (auto pair : creator->chat_users())
		{
			const User* chat_user = pair.second;
			bool admin = chat_user == creator->creator_user();
			chat_users.emplace_back(CreateChat::ChatUser{ chat_user, admin });
		}
		CreateChatQuery* query = new CreateChatQuery(creator, chat_users);
		connect(query, SIGNAL(chat_created(ChatId, CreateChat*, const CreateChat::chat_users_t&)), SLOT(slot_database_chat_created(ChatId, CreateChat*, const CreateChat::chat_users_t&)));
		m_database_async.post_query(query);
	}
	else emit creator->chat_created(nullptr);
}
// регистрируем чат для участника
void Chats::register_chat_by_user(UserId user_id, Chat* chat)
{
	contact_chats_t::iterator it = m_contact_chats.find(user_id);
	if (it == m_contact_chats.end()) it = m_contact_chats.insert(std::make_pair(user_id, chats_t())).first;
	chats_t& chats = it->second;

	assert( chats.find(chat->id()) == chats.end() );
	if (chats.find(chat->id()) == chats.end())
		chats.insert(std::make_pair(chat->id(), chat));
}
// снимаем регистрацию чата для участника
void Chats::unregister_chat_by_user(UserId user_id, Chat* chat)
{
	contact_chats_t::iterator it = m_contact_chats.find(user_id);
	if (it != m_contact_chats.end())
	{
		chats_t& chats = it->second;
		chats_t::iterator chat_it = chats.find(chat->id());

		assert( chat_it != chats.end() );
		if (chat_it != chats.end())
			chats.erase(chat_it);

		if (chats.empty()) m_contact_chats.erase(it);
	}
}
void Chats::set_chat_name(Chat* chat, const QString& name, Timestamp timestamp)
{
	chat->set_name(name);
	chat->set_timestamp(timestamp);
	m_database_async.post_query(new DBQuerySetChatName(chat, name, timestamp));
}
// найти контакт-чат (user - для какого пользователя ищем, target_contact - с кем чат)
// ищем, есть ли чат с этим участником
Chat* Chats::find_contact_chat(const User* user, const User* target_contact, ChatUser** out_user)
{
	contact_chats_t::iterator it = m_contact_chats.find(user->id);
	if (it != m_contact_chats.end())
	{
		chats_t& chats = it->second;
		if (user == target_contact)
		{
			// ищем чат с самим собой
			for (auto pair : chats)
			{
				Chat* chat = pair.second;
				if (!chat->is_group())
				{
					if (chat->users().size() == 1)
					{
						if (ChatUser* chat_user = chat->find_user(user->id))
						{
							if (out_user) *out_user = chat_user;
							return chat;
						}
					}
				}
			}
		}
		else
		{
			for (auto pair : chats)
			{
				Chat* chat = pair.second;
				if (!chat->is_group())
				{
					if (ChatUser* target_chat_user = chat->find_another_user(user->id))
					{
						if (target_chat_user->user == target_contact)
						{
							if (ChatUser* chat_user = chat->find_user(user->id))
							{
								if (out_user) *out_user = chat_user;
								return chat;
							}
						}
					}
				}
			}
		}
	}
	if (out_user) *out_user = nullptr;
	return nullptr;
}
bool Chats::add_group_chat_user(Chat* chat, const User* user, Timestamp timestamp)
{
	if (!chat->find_user(user->id))
	{
		if (ChatUser* chat_user = chat->add_user(user, false))
		{
			chat->set_timestamp(timestamp);
			register_chat_by_user(user->id, chat);
			m_database_async.post_query(new DBQueryAddChatUser(chat, user, chat_user->admin, timestamp));
			return true;
		}
	}
	return false;
}
bool Chats::remove_group_chat_user(Chat* chat, const User* user, Timestamp timestamp)
{
	if (chat->remove_user(user->id))
	{
		chat->set_timestamp(timestamp);
		unregister_chat_by_user(user->id, chat);
		m_database_async.post_query(new DBQueryRemoveChatUser(chat, user, timestamp));
		return true;
	}
	return false;
}
bool Chats::add_or_remove_group_chat_admin(Chat* chat, const User* user, bool add_or_remove)
{
	if (chat->set_or_remove_admin(user->id, add_or_remove))
	{
		m_database_async.post_query(new DBQueryAddOrRemoveChatAdmin(chat, user, add_or_remove));
		return true;
	}
	return false;
}
