//=====================================================================================//
//   Author: open
//   Date:   18.04.2017
//=====================================================================================//
#pragma once
#include "database/async/queries/database_async_create_chat.h"
#include "chat.h"

class DatabaseChats;
class DatabaseAsync;
class Users;

class Chats : public QObject
{
	Q_OBJECT
private:
	DatabaseChats& m_database_chats;
	DatabaseAsync& m_database_async;
	const Users& m_users;

	using chats_t = std::map<ChatId, Chat*>;
	chats_t m_chats;	// мапа всех чатов

	// какие чаты зарегистрированы на контакт 
	using contact_chats_t = std::map<UserId, chats_t>;
	contact_chats_t m_contact_chats;

public:
	Chats(const Users& users, DatabaseChats& database_chats, DatabaseAsync& database_async);
	virtual ~Chats();

	//void set_database(DatabaseQueryChats* database_query_chats, AsyncDatabaseQuery* async_database_query);
	//void database_load_chats(const User* user);

	Chat* find_chat(ChatId id) const;
	// найти контакт-чат (user - для какого пользователя ищем, target_contact - с кем чат)
	Chat* find_contact_chat(const User* user, const User* target_contact, ChatUser** out_user);
	void set_chat_name(Chat* chat, const QString& name, Timestamp timestamp);
	void create_contact_chat(UCreateContactChat creator);
	void create_group_chat(UCreateGroupChat creator);
	bool add_group_chat_user(Chat* chat, const User* user, Timestamp timestamp);
	bool remove_group_chat_user(Chat* chat, const User* user, Timestamp timestamp);
	bool add_or_remove_group_chat_admin(Chat* chat, const User* user, bool add_or_remove);

private slots:
	// создание чата по загрузке из базы (неизбежна подгрузка дубликатов)
	void slot_load_chat(ChatId chat_id, const QString& name, bool is_group, UserId creator_id, Timestamp timestamp, ChatMessageId last_message_id, ChatMessageStamp last_message_stamp);
	void slot_load_chat_user(ChatId chat_id, UserId user_id, bool admin);
	void slot_load_chat_finished(const std::vector<ChatId>& chats);
	void slot_database_chat_created(ChatId chat_id, CreateChat* creator, const CreateChat::chat_users_t& chat_users);

private:
	// создание чата (отсекает дубликаты)
	Chat* create_chat(ChatId chat_id, const QString& name, bool is_group, const User* creator_contact, Timestamp timestamp, ChatMessageId last_message_id, ChatMessageStamp last_message_stamp);
	// добавление участника в чат
	ChatUser* add_chat_user(Chat* chat, ChatId user_id, bool admin);
	// удаление участника из чата
	//void remove_chat_user(Chat* chat, ContactId user_id);
	// регистрируем чат для участника
	void register_chat_by_user(UserId user_id, Chat* chat);
	// снимаем регистрацию чата для участника
	void unregister_chat_by_user(UserId user_id, Chat* chat);
};
