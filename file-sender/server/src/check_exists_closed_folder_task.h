//=====================================================================================//
//   Author: open
//   Date:   28.01.2019
//=====================================================================================//
#pragma once
#include "thread_task.h"
#include <QDir>

class CheckExistsClosedFolderTask : public ThreadTask
{
	Q_OBJECT
private:
	QString m_path;
	bool m_exists = false;

public:
	CheckExistsClosedFolderTask(const QString& path)
	: m_path(path)
	{
	}
	// обработчик исполняемый в неглавном потоке приложения
	virtual void on_process()
	{
		m_exists = QDir(m_path).exists();
	}
	// обработчик исполняемый в главном потоке приложения
	virtual void on_result()
	{
		emit finished(m_exists, m_path);
	}

signals:
	void finished(bool exists, const QString& folder);
};