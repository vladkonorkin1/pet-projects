//=====================================================================================//
//   Author: open
//   Date:   14.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "connection.h"
#include "database/async/queries/database_async_chat_messages.h"
#include "file_transfer/file_thread_connection.h"
#include "file_transfer/file_message_manager.h"
#include "message_connection.h"
#include "base/utility.h"
#include "chat_list.h"
#include "server.h"
#include "chat.h"
#include <QThread>

Connection::Connection(Server& server, MessageConnection* message_connection, const User* user)
: m_server(server)
, m_message_connection(message_connection)
, m_user(user)
, m_session(nullptr)
, m_chat_list(load_chat_list())
, m_history_messages(server.database_async(), server.send_chat_messages(), user, message_connection)
, m_file_thread_connection(nullptr)
{
	connect(&m_history_messages, SIGNAL(restore_messages_finished()), SLOT(slot_restore_messages_finished()));
	connect(&m_history_messages, SIGNAL(history_messages_finished()), SLOT(slot_history_messages_finished()));
	check_initialization();
}
// логирование от имени connection
void Connection::logcon(const QString& str) const
{
	logs(QString("connection [%1:%2] %3").arg(m_user->login).arg(m_message_connection->address()).arg(str));
}
ChatList* Connection::load_chat_list()
{
	// загрузка первый раз чатов из базы
	m_server.database_chats().load_chats(m_user);
	// получение чат-листа
	return m_server.chat_lists().get_chat_list(m_user);
}
Connection::~Connection()
{
	m_message_connection->set_message_listener(nullptr);
}
ConnectionId Connection::id() const
{
	return m_message_connection->id();
}
void Connection::send_message(const NetMessage& msg)
{
	assert( thread() == QThread::currentThread() );
	m_message_connection->send_message(msg);
}
bool Connection::check_destroy(float dt)
{
	return m_message_connection->check_destroy(dt, true);
}
// проверка на завершение инициализации
void Connection::check_initialization()
{
	// инициализация закончена, теперь слушаем сообщения
	m_message_connection->set_message_listener(this);
	// сообщаем клиенту
	send_message(ServerMessageInitializationFinish());
}
// сетевое событие сообщения соединению "я жив"
void Connection::on_message(const ClientMessageAlive& msg)
{
	Q_UNUSED( msg );
	m_message_connection->set_alive();
	send_message(ServerMessageAliveResponse());
}
void Connection::close()
{
	m_message_connection->close();
}
// сетевое событие синхронизации данных
void Connection::on_message(const ClientMessageSync& msg)
{
	//logcon("client message sync");

	// синхронизируем пользователей
	m_server.send_user().sync_users(m_message_connection, msg.timestamp_departments, msg.timestamp_users);

	// высылаем id-контакта под которым мы зашли
	send_message(ServerMessageSetLoginUser(m_user->id));
	// синхронизация чатов
	m_server.send_chat().sync_chats(this, msg.timestamp_chats, m_chat_list);
	// синхронизация сообщений
	m_history_messages.request_restore_messages(msg.last_chat_message_stamp, msg.last_readed_chat_message_id);
}
void Connection::slot_restore_messages_finished()
{
	//logcon("restore messages finished");
	send_message(ServerMessageSyncResponse());
	emit sync_finished(this);
}
void Connection::slot_history_messages_finished()
{
}
const User* Connection::find_user(UserId user_id) const
{
	if (const User* user = m_server.users().find_user(user_id)) return user;
	logcon(QString("error to find user: %1").arg(user_id));
	return nullptr;
}
Chat* Connection::find_chat(ChatId chat_id) const
{
	if (Chat* chat = m_server.chats().find_chat(chat_id)) return chat;
	logcon(QString("error to find chat: %1").arg(chat_id));
	return nullptr;
}
// сетевое событие получения текстового сообщения чата
void Connection::on_message(const ClientMessageTextMessageSend& msg)
{
	logcon(QString("new text message chat=%1").arg(msg.chat_id));

	if (Chat* chat = find_chat(msg.chat_id))
	{
		if (chat->find_user(m_user->id))
		{
			DescChatMessage chat_message = build_new_message(msg.local_id, chat, m_user, ChatMessageType::Text);
			chat_message.text = msg.text;

			check_chat_in_chat_list(chat, chat_message.datetime);

			m_server.chat_lists().set_chat_last_time(chat, chat_message.datetime);

			DBQueryAddChatMessage* query = new DBQueryAddChatMessage(chat, chat_message);
			connect(query, &DBQueryAddChatMessage::storred, this, [this, chat](bool success, const DescChatMessage& chat_message) { slot_text_message_database_storred(success, chat, chat_message); });
			m_server.database_async().post_query(query);
		}
	}
}
// создать описатель нового сообщения чата
DescChatMessage Connection::build_new_message(const ChatMessageLocalId& local_id, Chat* chat, const User* sender, ChatMessageType type) const
{
	return DescChatMessage(
		0,
		local_id,
		chat->id(),
		sender->id,
		m_server.chat_message_stamp_generator().next_stamp(),
		type,
		ChatMessageStatus::Sended,
		HistoryStatus::Created,
		QDateTime::currentDateTimeUtc(),
		QDateTime(),
		false
	);
}
// событие добавления текстового сообщения в базу
void Connection::slot_text_message_database_storred(bool success, Chat* chat, const DescChatMessage& chat_message)
{
	if (!success)
	{
		logcon("database: error to add chat message");
		return;
	}
	chat->set_last_message_id(chat_message.id);
	chat->set_last_message_stamp(chat_message.stamp);
	m_server.send_chat_messages().send_message(chat, chat_message);
}
// проверить, что чат присутствует в чат-листе
void Connection::check_chat_in_chat_list(Chat* chat, const QDateTime& last_time)
{
	// если мы написали в чат вне chat_list'a, то самодобавляемся
	if (!m_chat_list->find_chat(chat->id()))
		add_chat_to_chat_list(m_chat_list, chat, last_time);

	if (!chat->is_group())
	{
		// проверяем, есть ли чат в chat_list'e у собеседника
		if (ChatUser* companion_user = chat->find_another_user(m_user->id))
		{
			ChatList* companion_chat_list = m_server.chat_lists().get_chat_list(companion_user->user);
			if (!companion_chat_list->find_chat(chat->id()))
			{
				add_chat_to_chat_list(companion_chat_list, chat, last_time);

				// обновляем собеседнику свой онлайн-статус
				if (Session* session = m_server.session_manager().get_session(companion_user->user->id))
					m_server.online_status_manager().send_user_status(session, m_user);
			}
		}
	}
}
// добавить чат в chat_list
void Connection::add_chat_to_chat_list(ChatList* chat_list, Chat* chat, const QDateTime& last_time)
{
	if (const ChatListContact* chat_list_contact = m_server.chat_lists().add_chat(chat_list, chat, false, last_time))
	{
		// отправить сетевое сообщение добавления чата в chat_list
		if (Session* session = m_server.session_manager().get_session(chat_list->user()->id))
		{
			m_server.send_chat().send_all_chat_info(session, chat);
			m_server.send_chat().send_all_add_chat_to_chat_list(session, chat_list_contact);
		}
	}
}
// сетевое событие запроса исторических сообщений
void Connection::on_message(const ClientMessageChatRequestHistoryMessages& msg)
{
	logcon(QString("request history messages chat=%1 msg=%2").arg(msg.chat_id).arg(msg.message_id));
	m_history_messages.request_history_messages(msg.chat_id, msg.message_id);
}
// сетевое событие создания контакт-чата
void Connection::on_message(const ClientMessageContactChatCreate& msg)
{
	logcon(QString("contact chat create user=%1").arg(msg.user_id));
	if (const User* target_contact = find_user(msg.user_id))
	{
		UCreateContactChat create_contact_chat(new CreateContactChat(m_user, Utility::current_timestamp(), target_contact));
		connect(create_contact_chat.get(), &CreateContactChat::chat_created, this, [this, target_contact](Chat* chat) { slot_contact_chat_created(chat, target_contact); });
		m_server.chats().create_contact_chat(create_contact_chat);
	}
}
// событие создания чата в базе
void Connection::slot_contact_chat_created(Chat* chat, const User* target_contact)
{
	// добавить чат в chat_list создателя
	if (chat) add_chat_to_chat_list(m_chat_list, chat, QDateTime::currentDateTimeUtc());
	// сообщаем ответ на создание
	send_message(ServerMessageContactChatCreateResponse(target_contact->id, chat ? chat->id() : 0));
}
// сетевое событие запроса контакт-чата
void Connection::on_message(const ClientMessageContactChatRequest& msg)
{
	logcon(QString("contact chat request user=%1").arg(msg.user_id));

	if (const User* target_contact = find_user(msg.user_id))
	{
		if (Chat* chat = m_server.chats().find_contact_chat(m_user, target_contact, nullptr))
		{
			// отправляем информацию о чате только запрашивающему соединению (нужно потестить добавление паралельно с другим исполняемым)
			m_server.send_chat().send_chat_info(this, chat);
		}
	}
}
// сетевое событие создания группового чата
void Connection::on_message(const ClientMessageGroupChatCreate& msg)
{
	logcon("group chat create");

	UCreateGroupChat creator(new CreateGroupChat(m_user, Utility::current_timestamp(), get_users(msg.user_ids)));
	connect(creator.get(), &CreateChat::chat_created, this, [this](Chat* chat) { slot_group_chat_created(chat); });
	m_server.chats().create_group_chat(creator);
}
// событие создания группового чата
void Connection::slot_group_chat_created(Chat* chat)
{
	if (chat)
	{
		// добавляем всех участников в chat_list'ы
		QDateTime last_time = QDateTime::currentDateTimeUtc();
		for (const auto& pair : chat->users())
		{
			const ChatUser* chat_user = pair.second;
			ChatList* chat_list = m_server.chat_lists().get_chat_list(chat_user->user);
			add_chat_to_chat_list(chat_list, chat, last_time);
		}
	}
	// сообщаем ответ на создание
	send_message(ServerMessageGroupChatCreateResponse(chat ? chat->id() : 0));
}
// конвертировать id-ники пользователей
std::vector<const User*> Connection::get_users(const std::vector<UserId>& user_ids) const
{
	std::vector<const User*> users;
	for (UserId user_id : user_ids)
		if (const User* user = find_user(user_id))
			users.push_back(user);
	return users;
}
// сетевое событие удаления чата
void Connection::on_message(const ClientMessageChatRemove& msg)
{
	logcon(QString("remove chat=%1").arg(msg.chat_id));

	if (Chat* chat = find_chat(msg.chat_id))
	{
		// участник чата решил сам выйти, поэтому удаляем его
		if (chat->is_group())
			remove_group_chat_user(chat, m_user);

		// удалить чат из chat_list'a
		if (m_server.chat_lists().remove_chat(m_chat_list, chat))
		{
			// оповещаем всех об удалении
			m_server.send_chat().send_all_remove_chat_from_chat_list(m_session, chat);
			//m_server->received_chats()->chat_remove(m_chat_list->contact(), chat);
		}
	}
}
// удалить участника группового чата
void Connection::remove_group_chat_user(Chat* chat, const User* user)
{
	assert( chat->is_group() );

	// сессии, которым посылаем событие удаления пользователя из группового чата
	std::set<Session*> sessions;
	sessions.insert(m_session);

	for (auto& pair : chat->users())
	{
		if (Session* session = m_server.session_manager().get_session(pair.second->user->id))
			sessions.insert(session);
	}

	// удаляем участника
	if (m_server.chats().remove_group_chat_user(chat, user, Utility::current_timestamp()))
	{
		// высылаем обновление пользователей чата во всех сессиях участников чата
		for (Session* session : sessions)
			m_server.send_chat().send_all_chat_users(session, chat);
	}
}
// назначить или удалить админа группового чата
void  Connection::add_or_remove_group_chat_admin(Chat* chat, const User* user, bool add_or_remove)
{
	assert(chat->is_group());
	// сессии, которым посылаем событие назначения/удаления админа группового чата
	std::set<Session*> sessions;
	sessions.insert(m_session);

	for (auto& pair : chat->users())
	{
		if (Session* session = m_server.session_manager().get_session(pair.second->user->id))
			sessions.insert(session);
	}

	// назначаем админом
	if (m_server.chats().add_or_remove_group_chat_admin(chat, user, add_or_remove))
	{
		// высылаем обновление пользователей чата во всех сессиях участников чата
		for (Session* session : sessions)
			m_server.send_chat().send_all_chat_users(session, chat);
	}
}
// сетевое событие добавление участника группового чата
void Connection::on_message(const ClientMessageGroupChatAddUsers& msg)
{
	logcon(QString("group chat add users chat=%1").arg(msg.chat_id));

	if (Chat* chat = find_chat(msg.chat_id))
	{
		if (!chat->is_group()) return;

		// новых участников чата могут добавлять только участники чата
		if (chat->find_user(m_user->id))
		{
			// добавляем новых участников
			std::vector<const User*> new_users;
			for (const User* user : get_users(msg.user_ids))
			{
				if (m_server.chats().add_group_chat_user(chat, user, Utility::current_timestamp()))
					new_users.push_back(user);
			}

			if (new_users.empty())
				return;

			// отправляем информацию по чату всем новеньким
			for (const User* user : new_users)
			{
				if (Session* session = m_server.session_manager().get_session(user->id))
					m_server.send_chat().send_all_chat_info(session, chat);
			}

			// отправляем участников чата всем участникам чата
			for (const auto& pair : chat->users())
			{
				const User* user = pair.second->user;
				if (Session* session = m_server.session_manager().get_session(user->id))
					m_server.send_chat().send_all_chat_users(session, chat);
			}

			// добавляем всем участникам в chat list'ы чат и отправляем в сеть
			QDateTime time = QDateTime::currentDateTimeUtc();
			for (const auto& pair : chat->users())
			{
				const User* user = pair.second->user;
				ChatList* chat_list = m_server.chat_lists().get_chat_list(user);
				add_chat_to_chat_list(chat_list, chat, time);
			}
		}
	}
}
// сетевое событие удаление участника группового чата
void Connection::on_message(const ClientMessageGroupChatRemoveUser& msg)
{
	logcon(QString("group chat=%1 remove user=%2").arg(msg.chat_id).arg(msg.user_id));
	if (Chat* chat = find_chat(msg.chat_id))
	{
		if (!chat->is_group())
			return;

		if (const User* user = find_user(msg.user_id))
		{
			// удалять могут только админы чата или только сам себя
			if (is_current_user_admin(chat) || m_user == user)
				remove_group_chat_user(chat, user);
		}
	}
}

void Connection::on_message(const ClientMessageGroupSetOrRemoveAdmin& msg)
{
	logcon(QString("group chat=%1 %2 set admin user=%3").arg(msg.chat_id).arg(msg.set_or_remove ? "set" : "remove").arg(msg.user_id));
	if (Chat* chat = find_chat(msg.chat_id))
	{
		if (const User* user = find_user(msg.user_id))
		{
			if (!chat->is_group())
				return;
			// изменять адмнистраторов могут только админы
			if (is_current_user_admin(chat))
			{
				add_or_remove_group_chat_admin(chat, user, msg.set_or_remove);
			}
		}
	}
}
// есть ли право у текущего пользователя в чате
bool Connection::is_current_user_admin(const Chat* chat) const
{
	if (ChatUser* user = chat->find_user(m_user->id))
		return user->admin;
	return false;
}
// сетевое событие переименования группового чата
void Connection::on_message(const ClientMessageGroupChatRename& msg)
{
	logcon(QString("group chat=%1 rename=%2").arg(msg.chat_id).arg(msg.name));
	if (Chat* chat = find_chat(msg.chat_id))
	{
		// переименовываем только групповой чат
		if (!chat->is_group())
			return;

		// если мы админы чата, то можем его переименовать
		if (is_current_user_admin(chat))
		{
			// подымаем все чаты после переименования
			//m_server.chat_lists().set_chat_last_time(chat, QDateTime::currentDateTimeUtc());
			// переименовываем чат и пишем в базу
			m_server.chats().set_chat_name(chat, msg.name, Utility::current_timestamp());

			// отправляем обновлённую информацию по чату всем его участникам
			for (const auto& pair : chat->users())
			{
				const User* user = pair.second->user;
				if (Session* session = m_server.session_manager().get_session(user->id))
					m_server.send_chat().send_all_chat_info(session, chat);
			}
		}
	}
}
// сетевое событие перемещения чата в избранное
void Connection::on_message(const ClientMessageChatMoveFavorite& msg)
{
	set_chat_favorite(msg.chat_id, msg.favorite);
}
// добавить чат в избранное
void Connection::set_chat_favorite(ChatId chat_id, bool favorite)
{
	logcon(QString("set chat favorite chat=%1 favorite=%2").arg(chat_id).arg(favorite));
	if (Chat* chat = find_chat(chat_id))
	{
		if (m_chat_list->set_chat_favorite(chat, favorite))
		{
			m_chat_list->set_chat_last_time(chat, QDateTime::currentDateTime());

			if (ChatListContact* chat_list_contact = m_chat_list->find_chat(chat_id))
				m_server.send_chat().send_all_add_chat_to_chat_list(m_session, chat_list_contact);
		}
	}
}
// сетевое событие прочтения новых сообщений
void Connection::on_message(const ClientMessageChatNewMessageRead& msg)
{
	logcon(QString("read chat message chat=%1 message=%2").arg(msg.chat_id).arg(msg.message_id));
	if (Chat* chat = find_chat(msg.chat_id))
	{
		DBQuerySetReadedMessages* query = new DBQuerySetReadedMessages(chat, m_user, msg.message_id, QDateTime::currentDateTimeUtc());
		connect(query, SIGNAL(finished(bool, const Chat*, ChatMessageId, const QDateTime&)), SLOT(slot_new_messages_readed_finished(bool, const Chat*, ChatMessageId, const QDateTime&)));
		m_server.database_async().post_query(query);
	}
}
// сетевое событие запроса статусов списка пользователей
void Connection::on_message(const ClientMessageUsersStatus& msg)
{
	m_server.online_status_manager().send_users_status(this, msg.users_ids);
};
// событие записи в базу прочтения сообщения
void Connection::slot_new_messages_readed_finished(bool success, const Chat* chat, ChatMessageId message_id, const QDateTime& datetime)
{
	// отправляем оповещение о прочтении сообщения своей сессии
	if (success) m_session->send_message(ServerMessageChatMessageRead(chat->id(), message_id, datetime));
	// ответ отправителю
	send_message(ServerMessageChatNewMessageReadResponse(success, chat->id(), message_id));
}
// установить подключение для передачи "тела" файлов
void Connection::open_file_thread_connection(FileThreadConnection* file_thread_connection)
{
	logcon(QString("open file thread connection=%1").arg(file_thread_connection->id()));

	if (m_file_thread_connection)
	{
		disconnect(m_file_thread_connection, SIGNAL(file_write_error()), this, SLOT(slot_file_transfer_write_error()));
		disconnect(m_file_thread_connection, SIGNAL(file_transfer_finish(quint64, FileTransferResult, const QString&)), this, SLOT(slot_file_transfer_finish(quint64, FileTransferResult, const QString&)));
		disconnect(m_file_thread_connection, SIGNAL(file_transfer_progress(quint64, quint64, unsigned char)), this, SLOT(slot_file_transfer_progress(quint64, quint64, unsigned char)));
	}

	m_file_thread_connection = file_thread_connection;
	
	connect(m_file_thread_connection, SIGNAL(file_write_error()), SLOT(slot_file_transfer_write_error()));
	connect(m_file_thread_connection, SIGNAL(file_transfer_finish(quint64, FileTransferResult, const QString&)), SLOT(slot_file_transfer_finish(quint64, FileTransferResult, const QString&)));
	connect(m_file_thread_connection, SIGNAL(file_transfer_progress(quint64, quint64, unsigned char)), SLOT(slot_file_transfer_progress(quint64, quint64, unsigned char)));
}
// удалить подключение по передачи тела файла
void Connection::close_file_thread_connection(FileThreadConnection* file_thread_connection)
{
	logcon(QString("close file thread connection=%1").arg(file_thread_connection->id()));

	if (m_file_thread_connection == file_thread_connection)
	{
		logcon(QString("close file thread connection=%1 closed").arg(file_thread_connection->id()));

		disconnect(m_file_thread_connection, SIGNAL(file_write_error()), this, SLOT(slot_file_transfer_write_error()));
		disconnect(m_file_thread_connection, SIGNAL(file_transfer_finish(quint64, FileTransferResult, const QString&)), this, SLOT(slot_file_transfer_finish(quint64, FileTransferResult, const QString&)));
		disconnect(m_file_thread_connection, SIGNAL(file_transfer_progress(quint64, quint64, unsigned char)), this, SLOT(slot_file_transfer_progress(quint64, quint64, unsigned char)));

		m_file_thread_connection = nullptr;
	}
	else
	{
		logcon(QString("close file thread connection=%1 ERROR closed").arg(file_thread_connection->id()));
	}
}
// событие ошибки при записи файла на диск
void Connection::slot_file_transfer_write_error()
{
	send_message(ServerMessageFileTransferWriteFileError());
}
// событие окончания передачи файла
void Connection::slot_file_transfer_finish(ChatMessageId message_id, FileTransferResult result, const QString& dest_name)
{
	send_message(ServerMessageFileTransferFinish(result, dest_name));
	m_server.file_message_manager().set_file_message_status(message_id, result, dest_name);
}
// событие прогресса передачи файла/папки
void Connection::slot_file_transfer_progress(ChatMessageId message_id, ChatId chat_id, unsigned char percent)
{
	m_session->send_message(ServerMessageFileMessageProgress(message_id, chat_id, percent));
}
// сетевое событие регистрации сообщения о передаче файла
void Connection::on_message(const ClientMessageFileMessageSend& msg)
{
	logcon(QString("send file message chat=%1 path=%2 size=%3").arg(msg.chat_id).arg(msg.src_path).arg(msg.size));
	if (Chat* chat = find_chat(msg.chat_id))
	{
		if (chat->is_group()) return;

		if (chat->find_user(m_user->id))
		{
			if (const ChatUser* chat_receiver = chat->find_another_user(m_user->id))
			{
				UserId receiver_id = chat_receiver->user->id;

				DescChatMessage chat_message = build_new_message(msg.local_id, chat, m_user, ChatMessageType::File);
				chat_message.file_src_path = msg.src_path;
				chat_message.file_size = msg.size;
				chat_message.file_is_dir = msg.is_dir;

				check_chat_in_chat_list(chat, chat_message.datetime);
				m_server.chat_lists().set_chat_last_time(chat, chat_message.datetime);

				DBQueryAddFileChatMessage* query = new DBQueryAddFileChatMessage(chat_message, 0);		// при создании сообщения записываем его только для отправителя (для получателя не пишем)
				connect(query, &DBQueryAddFileChatMessage::storred, this, [this, chat, receiver_id](bool success, const DescChatMessage& chat_message) { slot_file_message_database_storred(success, chat, chat_message, receiver_id); });
				m_server.database_async().post_query(query);
			}
		}
	}
}
// событие записи в базу файлового сообщения
void Connection::slot_file_message_database_storred(bool success, Chat* chat, const DescChatMessage& chat_message, UserId receiver_id)
{
	if (!success)
	{
		logcon("database: error to add file chat message");
		return;
	}
	chat->set_last_message_id(chat_message.id);
	chat->set_last_message_stamp(chat_message.stamp);

	// регистрируем сообщение на передачу
	m_server.file_message_manager().register_file_message(this, chat_message.id, receiver_id, false);

	// отправить сообщение
	m_server.send_chat_messages().send_message(chat_message.sender_id, chat_message);

	//if (receiver_id != 0)
	//	m_server.send_chat_messages().send_message(receiver_id, chat_message);
}
// сетевое событие регистрации передачи файлового сообщения
void Connection::on_message(const ClientMessageFileMessageRegister& msg)
{
	logcon(QString("file message register chat=%1 id=%2").arg(msg.chat_id).arg(msg.message_id));
	if (Chat* chat = find_chat(msg.chat_id))
	{
		if (const ChatUser* receiver_user = chat->find_another_user(m_user->id))
		{
			// регистрируем сообщение на передачу
			m_server.file_message_manager().register_file_message(this, msg.message_id, receiver_user->user->id, true);
		}
	}
}
// сетевое событие отмены передачи файлового сообщения
void Connection::on_message(const ClientMessageFileMessageCancel& msg)
{
	logcon(QString("file message cancel id=%1").arg(msg.message_id));
	m_server.file_message_manager().cancel_file_message(msg.message_id);
	send_message(ServerMessageFileMessageCancelResponse(msg.message_id));
}
// сетевое событие начала передачи файла
void Connection::on_message(const ClientMessageFileTransferStart& msg)
{
	logcon(QString("start file transfer id=%1 chat=%2 name=%3 file_thread_connection=%4").arg(msg.message_id).arg(msg.chat_id).arg(msg.name).arg(m_file_thread_connection ? m_file_thread_connection->id() : 0));
	
	if (!m_file_thread_connection) return;

	if (Chat* chat = find_chat(msg.chat_id))
	{
		if (const ChatUser* chat_user = chat->find_another_user(m_user->id))
		{
			m_file_thread_connection->start_file_transfer(
				FileTransferDesc {
				msg.message_id,
				msg.chat_id,
				m_user->name,
				m_server.users().full_folder_path(chat_user->user),
				msg.name,
				msg.is_dir,
				msg.size,
				msg.files
			});
		}
	}
}
// сетевое событие передачи номера "тела" файла
void Connection::on_message(const ClientMessageFileTransferFileBodyStart& msg)
{
	if (m_file_thread_connection) m_file_thread_connection->current_read_file(msg.file_index, msg.size);
}
// сетевое событие отмены передачи файла
void Connection::on_message(const ClientMessageFileTransferCancel& msg)
{
	logcon(QString("cancel file transfer file_id=%1 bytes=%2 file_thread_connection=%3").arg(msg.file_id).arg(msg.bytes).arg(m_file_thread_connection ? m_file_thread_connection->id() : 0));

	if (m_file_thread_connection) m_file_thread_connection->current_cancel(msg.file_id, msg.bytes, msg.user_cancel);
}
// сетевое событие окончания передачи файла
void Connection::on_message(const ClientMessageFileTransferFinish&)
{
	logcon("finish file transfer");
	if (m_file_thread_connection) m_file_thread_connection->current_prepare_finish();
}



#include "check_exists_closed_folder_task.h"
// сетевое событие запроса доступа к закрытой папке
void Connection::on_message(const ClientMessageIsAccessClosedFolder&)
{
	const QString& folder = m_user->folder;

	if (folder.isEmpty())
	{
		slot_check_exists_closed_folder_finished(false, folder);
		return;
	}
	SThreadTask task(new CheckExistsClosedFolderTask(folder));
	connect(task.get(), SIGNAL(finished(bool, const QString&)), SLOT(slot_check_exists_closed_folder_finished(bool, const QString&)));
	m_server.thread_task_manager().post_task(task);
}
// событие завершения работы таска проверки доступности папки
void Connection::slot_check_exists_closed_folder_finished(bool exists, const QString& folder)
{
	if (!exists)
		logcon(QString("!!! no access to folder=%1").arg(folder));

	send_message(ServerMessageIsAccessClosedFolderResponse(exists));
}