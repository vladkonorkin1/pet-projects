//=====================================================================================//
//   Author: open
//   Date:   28.01.2019
//=====================================================================================//
#pragma once
#include "network/handler_messages.h"
#include "history_messages.h"

class FileThreadConnection;
class MessageConnection;
class ChatList;
class Session;
class Server;
class User;
class Chat;

class Connection : public QObject, public ServerHandlerMessages
{
	Q_OBJECT
private:
	Server& m_server;
	MessageConnection* m_message_connection;
	const User* m_user;
	Session* m_session;
	ChatList* m_chat_list;
	HistoryMessages m_history_messages;
	FileThreadConnection* m_file_thread_connection;
	
public:
	Connection(Server& server, MessageConnection* message_connection, const User* user);
	virtual ~Connection();
	ConnectionId id() const;

	bool check_destroy(float dt);
	void close();

	void set_session(Session* session);
	Session* session();
	const User* user() const;
	ChatList* chat_list();
	void send_message(const NetMessage& msg);
	// установить подключение для передачи "тела" файлов
	void open_file_thread_connection(FileThreadConnection* file_thread_connection);
	// удалить подключение по передачи тела файла
	void close_file_thread_connection(FileThreadConnection* file_thread_connection);
	// получить файловой подключение
	FileThreadConnection* file_thread_connection();

public:
	// сетевое событие синхронизации данных
	virtual void on_message(const ClientMessageSync& msg);
	// сетевое событие сообщения соединению "я жив"
	virtual void on_message(const ClientMessageAlive& msg);
	// сетевое событие получения текстового сообщения чата
	virtual void on_message(const ClientMessageTextMessageSend& msg);
	// сетевое событие запроса исторических сообщений
	virtual void on_message(const ClientMessageChatRequestHistoryMessages& msg);
	// сетевое событие создания контакт-чата
	virtual void on_message(const ClientMessageContactChatCreate& msg);
	// сетевое событие запроса контакт-чата
	virtual void on_message(const ClientMessageContactChatRequest& msg);
	// сетевое событие создания группового чата
	virtual void on_message(const ClientMessageGroupChatCreate& msg);
	// сетевое событие удаления чата
	virtual void on_message(const ClientMessageChatRemove& msg);
	// сетевое событие добавление участника группового чата
	virtual void on_message(const ClientMessageGroupChatAddUsers& msg);
	// сетевое событие удаление участника группового чата
	virtual void on_message(const ClientMessageGroupChatRemoveUser& msg);
	// сетевое событие назначения или удаления администратора чата
	virtual void on_message(const ClientMessageGroupSetOrRemoveAdmin& msg);
	// сетевое событие переименования группового чата
	virtual void on_message(const ClientMessageGroupChatRename& msg);
	// сетевое событие перемещения чата в избранное
	virtual void on_message(const ClientMessageChatMoveFavorite& msg);
	// сетевое событие прочтения новых сообщений
	virtual void on_message(const ClientMessageChatNewMessageRead& msg);
	// сетевое событие запроса статусов списка пользователей
	virtual void on_message(const ClientMessageUsersStatus& msg);
	// сетевое событие регистрации сообщения о передаче файла
	virtual void on_message(const ClientMessageFileMessageSend&);
	// сетевое событие регистрации передачи файлового сообщения
	virtual void on_message(const ClientMessageFileMessageRegister& msg);
	// сетевое событие отмены передачи файлового сообщения
	virtual void on_message(const ClientMessageFileMessageCancel& msg);
	// сетевое событие начала передачи файла
	virtual void on_message(const ClientMessageFileTransferStart&);
	// сетевое событие передачи номера "тела" файла
	virtual void on_message(const ClientMessageFileTransferFileBodyStart&);
	// сетевое событие отмены передачи файла
	virtual void on_message(const ClientMessageFileTransferCancel&);
	// сетевое событие окончания передачи файла
	virtual void on_message(const ClientMessageFileTransferFinish&);
	// сетевое событие запроса доступа к закрытой папке
	virtual void on_message(const ClientMessageIsAccessClosedFolder&);

signals:
	void sync_finished(Connection* connection);
	
private slots:
	void slot_restore_messages_finished();
	void slot_history_messages_finished();

	// событие добавления текстового сообщения в базу
	void slot_text_message_database_storred(bool success, Chat* chat, const DescChatMessage& chat_message);
	// событие создания чата в базе
	void slot_contact_chat_created(Chat* chat, const User* target_contact);
	// событие создания группового чата
	void slot_group_chat_created(Chat* chat);
	// событие записи в базу прочтения сообщения
	void slot_new_messages_readed_finished(bool success, const Chat* chat, ChatMessageId message_id, const QDateTime& datetime);
	// событие ошибки при записи файла на диск
	void slot_file_transfer_write_error();
	// событие окончания передачи файла/папки
	void slot_file_transfer_finish(quint64 message_id, FileTransferResult result, const QString& dest_name);
	// событие прогресса передачи файла/папки
	void slot_file_transfer_progress(quint64 message_id, quint64 chat_id, unsigned char percent);
	// событие записи в базу файлового сообщения
	void slot_file_message_database_storred(bool success, Chat* chat, const DescChatMessage& chat_message, UserId receiver_id);
	// событие завершения работы таска проверки доступности папки
	void slot_check_exists_closed_folder_finished(bool exists, const QString& folder);

private:
	ChatList* load_chat_list();
	const User* find_user(UserId user_id) const;
	Chat* find_chat(ChatId chat_id) const;
	// проверить, что чат присутствует в чат-листе
	void check_chat_in_chat_list(Chat* chat, const QDateTime& last_time);
	// добавить чат в chat_list
	void add_chat_to_chat_list(ChatList* chat_list, Chat* chat, const QDateTime& last_time);
	// создать описатель нового сообщения чата
	DescChatMessage build_new_message(const ChatMessageLocalId& local_id, Chat* chat, const User* sender, ChatMessageType type) const;
	// конвертировать id-ники пользователей
	std::vector<const User*> get_users(const std::vector<UserId>& user_ids) const;
	// удалить участника группового чата
	void remove_group_chat_user(Chat* chat, const User* user);
	// назначить или удалить администратора группового чата
	void add_or_remove_group_chat_admin(Chat* chat, const User* user, bool add_or_remove);
	// есть ли право у текцщего пользователя в чате 
	bool is_current_user_admin(const Chat* chat) const;
	// добавить чат в избранное
	void set_chat_favorite(ChatId chat_id, bool favorite);
	// проверка на завершение инициализации
	void check_initialization();
	// логирование от имени connection
	void logcon(const QString& str) const;
};

inline void Connection::set_session(Session* session)				{ m_session = session; }
inline Session* Connection::session()								{ return m_session; }
inline const User* Connection::user() const							{ return m_user; }
inline ChatList* Connection::chat_list()							{ return m_chat_list; }
inline FileThreadConnection* Connection::file_thread_connection()	{ return m_file_thread_connection; }