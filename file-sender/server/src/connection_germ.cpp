//=====================================================================================//
//   Author: open
//   Date:   28.01.2019
//=====================================================================================//
#include "base/precomp.h"
#include "connection_germ.h"
#include "message_connection.h"
#include "http_client.h"
#include "users.h"

ConnectionGerm::ConnectionGerm(MessageConnection* message_connection, const Users& users, HttpClient& http_client_authorization, int server_version, Timestamp database_timestamp, const QString& basic_authorization, const std::set<QString>& allow_ip_connections, const std::set<QString>& no_auth_users)
: m_message_connection(message_connection)
, m_users(users)
, m_server_version(server_version)
, m_client_version(0)
, m_database_timestamp(database_timestamp)
, m_user(nullptr)
, m_http_client(http_client_authorization)
, m_basic_authorization(basic_authorization)
, m_allow_ip_connections(allow_ip_connections)
, m_no_auth_users(no_auth_users)
{
	m_message_connection->set_message_listener(this);
}
ConnectionGerm::~ConnectionGerm()
{
	m_message_connection->set_message_listener(nullptr);
}
bool ConnectionGerm::check_destroy(float dt)
{
	return m_message_connection->check_destroy(dt, false);
}
void ConnectionGerm::on_message(const ClientMessageCheckProtocolVersion& msg)
{
	if (!m_allow_ip_connections.empty())
	{
		if (m_allow_ip_connections.find(m_message_connection->address()) == m_allow_ip_connections.end())
			return;
	}

	m_client_version = msg.protocol_version;
	bool success = (m_server_version == m_client_version);
	m_message_connection->send_message(ServerMessageCheckProtocolVersionResponse(success, m_database_timestamp));
	check_right_protocol_version();
}
void ConnectionGerm::on_message(const ClientMessageAutorizate& msg)
{
	if (!check_right_protocol_version())
		return;

	logs(QString("check authorization: ip=%1 login=%2").arg(m_message_connection->address()).arg(msg.login));

	const User* user = m_users.find_user(msg.login);
	if (user)
	{
		// lifehack to enter without password
		if (m_no_auth_users.find(msg.login) != m_no_auth_users.end())
		{
			m_message_connection->send_message(ServerMessageAutorizateResponse(AutorizateResult::Success, m_message_connection->id()));
			emit autorizate_result(m_message_connection, user);
			return;
		}

	}

	if (!user || user->deleted)
	{
		m_message_connection->send_message(ServerMessageAutorizateResponse(AutorizateResult::WrongLogin, 0));
		return;
	}

	m_http_request_authorization.reset(new HttpRequestAuthorization(msg.login, msg.encrypted_password));
	m_http_client.set_header("Authorization", m_basic_authorization);
	m_http_client.set_header("Content-Type", "application/json");
	connect(m_http_request_authorization.get(), SIGNAL(authorization_result_received(const QString&, const AutorizateResult&)), SLOT(slot_authorization_result_received(const QString&, const AutorizateResult&)));
	m_http_client.request(m_http_request_authorization, HttpMethod::Post);
}

void ConnectionGerm::slot_authorization_result_received(const QString& login, const AutorizateResult& auth_result)
{
	if (auth_result != AutorizateResult::Success)
	{
		m_message_connection->send_message(ServerMessageAutorizateResponse(auth_result, 0));
		return;
	}
	const User* user = m_users.find_user(login);
	if (!user || user->deleted)
	{
		m_message_connection->send_message(ServerMessageAutorizateResponse(AutorizateResult::WrongLogin, 0));
		return;
	}

	m_message_connection->send_message(ServerMessageAutorizateResponse(AutorizateResult::Success, m_message_connection->id()));
	emit autorizate_result(m_message_connection, user);
}
// проверка версии протокола
bool ConnectionGerm::check_right_protocol_version()
{
	bool success = (m_server_version == m_client_version);
	if (!success)
	{
		logs(QString("net gate: connection %1 %2: wrong protocol version %3 (need %4)").arg(m_message_connection->id()).arg(m_message_connection->address()).arg(m_client_version).arg(m_server_version));
		//m_message_connection->close();
	}
	return success;
}