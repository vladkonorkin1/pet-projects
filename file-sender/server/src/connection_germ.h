//=====================================================================================//
//   Author: open
//   Date:   28.01.2019
//=====================================================================================//
#pragma once
#include "network/handler_messages.h"
#include "http_request_authorization.h"

class MessageConnection;
class HttpClient;
class Users;
class User;

class ConnectionGerm : public QObject, public ServerHandlerMessages
{
	Q_OBJECT
private:
	MessageConnection* m_message_connection;
	const Users& m_users;
	int m_server_version;
	int m_client_version;
	Timestamp m_database_timestamp;
	const User* m_user;

	HttpClient& m_http_client;
	const QString& m_basic_authorization;
	SHttpRequestAuthorization m_http_request_authorization;

	const std::set<QString>& m_allow_ip_connections;
	const std::set<QString>& m_no_auth_users;

public:
	ConnectionGerm(MessageConnection* message_connection, const Users& users, HttpClient& http_client_authorization, int server_version, Timestamp database_timestamp, const QString& basic_authorization, const std::set<QString>& allow_ip_connections, const std::set<QString>& no_auth_users);
	virtual ~ConnectionGerm();
	bool check_destroy(float dt);

public:
	virtual void on_message(const ClientMessageCheckProtocolVersion& msg);
	virtual void on_message(const ClientMessageAutorizate& msg);

signals:
	void autorizate_result(MessageConnection* message_connection, const User* user);

private slots:
	void slot_authorization_result_received(const QString& login, const AutorizateResult& auth_result);

private:
	// проверка версии протокола
	bool check_right_protocol_version();
};