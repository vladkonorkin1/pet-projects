//=====================================================================================//
//   Author: open
//   Date:   28.01.2019
//=====================================================================================//
#include "base/precomp.h"
#include "connection_manager.h"
#include "message_connection.h"
#include "server.h"

ConnectionManager::ConnectionManager(Server& server, Timestamp database_timestamp, const QString& service_login, const QString& service_password, const QStringList& allow_ip_connections, const QStringList& no_auth_users)
: m_protocol_version(NetProtocolVersion)
, m_server(server)
, m_database_timestamp(database_timestamp)
, m_basic_auth("Basic " + (service_login + ':' + service_password).toUtf8().toBase64())
{
	connect(&m_message_server, SIGNAL(connection_created(MessageConnection*)), SLOT(slot_message_connection_created(MessageConnection*)));
	connect(&m_message_server, SIGNAL(connection_destroyed(MessageConnection*)), SLOT(slot_message_connection_destroyed(MessageConnection*)));

	for (const QString& ip : allow_ip_connections)
		m_allow_ip_connections.insert(ip);

	for (const QString& user_name : no_auth_users)
		m_no_auth_users.insert(user_name);
}
void ConnectionManager::slot_message_connection_created(MessageConnection* message_connection)
{
	logs(QString("net: open connection=%1  count connections=%2").arg(message_connection->address()).arg(m_connections.size()));

	// при обнаружении подключения создаём "зародыш" соединения (пусть занимается проверками и авторизацией)
	UConnectionGerm connection_germ(new ConnectionGerm(message_connection, m_server.users(), m_http_client_authorization, m_protocol_version, m_database_timestamp, m_basic_auth, m_allow_ip_connections, m_no_auth_users));
	connect(connection_germ.get(), SIGNAL(autorizate_result(MessageConnection*, const User*)), SLOT(slot_autorizate_result(MessageConnection*, const User*)));
	m_connection_germs[message_connection->id()] = std::move(connection_germ);
}
void ConnectionManager::slot_message_connection_destroyed(MessageConnection* message_connection)
{
	logs(QString("net: close connection=%1 count connections=%3").arg(message_connection->address()).arg(m_connections.size()));

	remove_connection_germ(message_connection);

	connections_t::iterator it = m_connections.find(message_connection->id());
	if (m_connections.end() != it)
	{
		emit connection_destroyed(it->second.get());
		m_connections.erase(it);
	}
}
void ConnectionManager::remove_connection_germ(const MessageConnection* message_connection)
{
	connection_germs_t::iterator it = m_connection_germs.find(message_connection->id());
	if (m_connection_germs.end() != it)
	{
		//ConnectionGerm* connection_germ = it->second.release();
		//connection_germ->deleteLater();
		m_connection_germs.erase(it);
	}
}
void ConnectionManager::slot_autorizate_result(MessageConnection* message_connection, const User* user)
{
	// удаляем соединение проверки версии и авторизации
	remove_connection_germ(message_connection);

	// создаем основное соединение для синхронизации и обмена
	UConnection uconnection(new Connection(m_server, message_connection, user));
	Connection* connection = uconnection.get();
	connect(connection, SIGNAL(sync_finished(Connection*)), SIGNAL(connection_synced(Connection*)));
	m_connections[message_connection->id()] = std::move(uconnection);
	emit connection_created(connection);
}
void ConnectionManager::update(float dt)
{
	for (auto it = m_connections.begin(); it != m_connections.end(); )
	{
		Connection* connection = it->second.get();
		++it;
		connection->check_destroy(dt);
	}

	for (auto it = m_connection_germs.begin(); it != m_connection_germs.end(); )
	{
		ConnectionGerm* germ = it->second.get();
		++it;
		germ->check_destroy(dt);
	}
}
void ConnectionManager::send_message_all_connections(const NetMessage& msg)
{
	for (auto& pair : m_connections)
	{
		Connection* connection = pair.second.get();
		connection->send_message(msg);
	}
}
Connection* ConnectionManager::find_connection(ConnectionId id)
{
	connections_t::iterator it = m_connections.find(id);
	if (m_connections.end() != it)
		return it->second.get();
	return nullptr;
}