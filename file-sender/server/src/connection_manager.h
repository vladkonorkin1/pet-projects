//=====================================================================================//
//   Author: open
//   Date:   28.01.2019
//=====================================================================================//
#pragma once
#include "message_server.h"
#include "connection_germ.h"
#include "connection.h"
#include "http_client.h"

class Server;

class ConnectionManager : public QObject
{
	Q_OBJECT
private:
	quint16 m_protocol_version;
	Server& m_server;
	Timestamp m_database_timestamp;
	MessageServer m_message_server;
	QString m_basic_auth;

	std::set<QString> m_allow_ip_connections;
	std::set<QString> m_no_auth_users;

	using UConnectionGerm = std::unique_ptr<ConnectionGerm>;
	using connection_germs_t = std::map<ConnectionId, UConnectionGerm>;
	connection_germs_t m_connection_germs;

	using UConnection = std::unique_ptr<Connection>;
	using connections_t = std::map<ConnectionId, UConnection>;
	connections_t m_connections;
	HttpClient m_http_client_authorization;

public:
	ConnectionManager(Server& server, Timestamp database_timestamp, const QString& service_login, const QString& service_password, const QStringList& allow_ip_connections, const QStringList& no_auth_users);
	void update(float dt);
	void send_message_all_connections(const NetMessage& msg);
	Connection* find_connection(ConnectionId id);

signals:
	void connection_created(Connection* connection);
	void connection_destroyed(Connection* connection);
	void connection_synced(Connection* connection);

private slots:
	void slot_message_connection_created(MessageConnection* message_connection);
	void slot_message_connection_destroyed(MessageConnection* message_connection);
	void slot_autorizate_result(MessageConnection* message_connection, const User* user);

private:
	void remove_connection_germ(const MessageConnection* message_connection);
};