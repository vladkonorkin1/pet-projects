//=====================================================================================//
//   Author: open
//   Date:   16.03.2019
//=====================================================================================//
#include "base/precomp.h"
#include "database_async.h"
#include "database_async_query.h"
#include <QCoreApplication>
#include <QEvent>


DatabaseThread::DatabaseThread(const QString& name, const DatabaseParams& database_params)
: m_name(name)
, m_database_params(database_params)
{
	setObjectName("thread "+ name);
}
void DatabaseThread::run()
{
	QSqlDatabase database = create_database_connection(m_name, m_database_params);
	m_database = &database;
	exec();
	m_database = nullptr;
	database.close();
	QSqlDatabase::removeDatabase(m_name);
}
#include <QSqlError>
void DatabaseThread::restore_connection_to_database()
{
	if (m_database->lastError().type() != QSqlError::NoError)
	{
		logs(QString("error query - close database: %1").arg(m_database->connectionName()));
		m_database->close();
	}

	while (!m_database->isOpen())
	{
		logs(QString("reconnect to database: %1").arg(m_database->connectionName()));
		m_database->open();
	}
}


DatabaseConnection::DatabaseConnection(DatabaseAsync& database_async, const QString& name, const DatabaseParams& database_params)
: m_database_async(database_async)
, m_thread(name, database_params)
{
}
DatabaseConnection::~DatabaseConnection()
{
	close();
}
void DatabaseConnection::close()
{
	m_thread.quit();
	m_thread.wait();
}
void DatabaseConnection::start_thread()
{
	moveToThread(&m_thread);
	m_thread.start();
}
void DatabaseConnection::post()
{
	assert( QThread::currentThread() != &m_thread );
	QCoreApplication::postEvent(this, new QEvent(QEvent::User));
}
bool DatabaseConnection::event(QEvent* event)
{
	if (event->type() == QEvent::User)
	{
		assert( QThread::currentThread() == &m_thread );
		while (1)
		{
			SDatabaseAsyncQuery query = m_database_async.next_query(this);
			if (!query) break;
			
			m_thread.restore_connection_to_database();
			query->on_process(m_thread.database());
			emit query_finished(query);
		}
	}
	return true;
}


Q_DECLARE_METATYPE(SDatabaseAsyncQuery);

DatabaseAsync::DatabaseAsync(const DatabaseParams& database_params)
: m_database_params(database_params)
{
	int num_connections = database_params.num_connections;
	m_connections.reserve(num_connections);
	for (int i = 0; i < num_connections; ++i)
		m_connections.push_back(UDatabaseConnection(new DatabaseConnection(*this, QString("database %1").arg(i), m_database_params)));

	for (UDatabaseConnection& connection : m_connections)
	{
		connection->start_thread();
		m_free_connections.push(connection.get());
		connect(connection.get(), &DatabaseConnection::query_finished, this, &DatabaseAsync::slot_query_finished, Qt::QueuedConnection);
	}
}
void DatabaseAsync::slot_query_finished(SDatabaseAsyncQuery query)
{
	query->on_result();
}
void DatabaseAsync::post_query(DatabaseAsyncQuery* query)
{
	post_query(SDatabaseAsyncQuery(query));
}
void DatabaseAsync::post_query(const SDatabaseAsyncQuery& query)
{
	QMutexLocker locker(&m_mutex);
	m_queries.push(query);

	if (!m_free_connections.empty())
	{
		DatabaseConnection* connection = m_free_connections.top();
		m_free_connections.pop();
		connection->post();
	}
}
SDatabaseAsyncQuery DatabaseAsync::next_query(DatabaseConnection* connection)
{
	QMutexLocker locker(&m_mutex);
	if (!m_queries.empty())
	{
		SDatabaseAsyncQuery query = m_queries.front();
		m_queries.pop();
		return query;
	}
	m_free_connections.push(connection);
	static SDatabaseAsyncQuery temp;
	return temp;
}