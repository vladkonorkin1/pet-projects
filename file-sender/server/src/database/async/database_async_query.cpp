//=====================================================================================//
//   Author: open
//   Date:   09.06.2017
//=====================================================================================//
#include "base/precomp.h"
#include "database_async_query.h"
#include <QSqlQuery>
#include <QSqlError>

bool DatabaseAsyncQuery::safe_exec(QSqlQuery& query) const
{
	if (query.exec()) return true;
	QString error(query.lastError().text());
	logs(error);
	logs(query.lastQuery());
	return false;
}

/*bool safe_exec(QSqlQuery& query)
{
	if (query.exec()) return true;
	QString error(query.lastError().text());
	logs(error);
	logs(query.lastQuery());
	return false;
}*/