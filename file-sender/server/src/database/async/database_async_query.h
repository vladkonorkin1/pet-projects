//=====================================================================================//
//   Author: open
//   Date:   09.06.2017
//=====================================================================================//
#pragma once

class QSqlDatabase;
class QSqlQuery;

class DatabaseAsyncQuery : public QObject
{
	Q_OBJECT
public:
	// обработчик исполняемый в неглавном потоке приложения
	virtual void on_process(QSqlDatabase& database) = 0;
	// обработчик исполняемый в главном потоке приложения
	virtual void on_result() {}

	bool safe_exec(QSqlQuery& query) const;
};

using SDatabaseAsyncQuery = std::shared_ptr<DatabaseAsyncQuery>;

//extern bool safe_exec(QSqlQuery& query);