//=====================================================================================//
//   Author: open
//   Date:   16.03.2019
//=====================================================================================//
#pragma once
#include "database/async/database_async_query.h"
#include "database/database_create_connection.h"
#include <QThread>
#include <stack>

class DatabaseThread : public QThread
{
public:
	QSqlDatabase* m_database = nullptr;
	QString m_name;
	DatabaseParams m_database_params;

public:
	DatabaseThread(const QString& name, const DatabaseParams& database_params);
	QSqlDatabase& database() { return *m_database; }
	void restore_connection_to_database();

protected:
	virtual void run();
};

class DatabaseAsync;

class DatabaseConnection : public QObject
{
	Q_OBJECT
private:
	DatabaseAsync& m_database_async;
	DatabaseThread m_thread;

public:
	DatabaseConnection(DatabaseAsync& database_async, const QString& name, const DatabaseParams& database_params);
	virtual ~DatabaseConnection();
	void start_thread();
	void post();

signals:
	void query_finished(SDatabaseAsyncQuery query);

protected:
	virtual bool event(QEvent* event);

private:
	void close();
};

class DatabaseAsync : public QObject
{
	Q_OBJECT
private:
	DatabaseParams m_database_params;
	using UDatabaseConnection = std::unique_ptr<DatabaseConnection>;
	std::vector<UDatabaseConnection> m_connections;
	std::stack<DatabaseConnection*> m_free_connections;
	std::queue<SDatabaseAsyncQuery> m_queries;		// очередь запросов
	QMutex m_mutex;

public:
	DatabaseAsync(const DatabaseParams& database_params);
	void post_query(DatabaseAsyncQuery* query);

private slots:
	void slot_query_finished(SDatabaseAsyncQuery query);

private:
	void post_query(const SDatabaseAsyncQuery& query);
	SDatabaseAsyncQuery next_query(DatabaseConnection* connection);
	friend class DatabaseConnection;
};