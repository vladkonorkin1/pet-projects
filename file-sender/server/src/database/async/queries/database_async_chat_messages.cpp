//=====================================================================================//
//   Author: open
//   Date:   03.07.2017
//=====================================================================================//
#include "base/precomp.h"
#include "database_async_chat_messages.h"
#include "base/utility.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>
#include "chat.h"
#include "user.h"

bool insert_message(DatabaseAsyncQuery* db_query, QSqlDatabase& database, const DescChatMessage& msg, ChatMessageId& message_id)
{
	QSqlQuery query(database);
	query.prepare("INSERT INTO messages (id, local_id, sender_id, chat_id, stamp, type, datetime, txt, file_src_path, file_is_dir, file_size, history_status) VALUES (default, :local_id, :sender_id, :chat_id, :stamp, :type, :datetime, :txt, :file_src_path, :file_is_dir, :file_size, :history_status) RETURNING id");
	query.bindValue(":local_id", msg.local_id);
	query.bindValue(":sender_id", msg.sender_id);
	query.bindValue(":chat_id", msg.chat_id);
	query.bindValue(":stamp", msg.stamp);
	query.bindValue(":type", static_cast<int>(msg.type));
	query.bindValue(":datetime", QVariant::fromValue<QDateTime>(msg.datetime));
	query.bindValue(":history_status", static_cast<int>(msg.history_status));

	switch (msg.type)
	{
		case ChatMessageType::Text:
		{
			query.bindValue(":txt", msg.text);
			break;
		}
		case ChatMessageType::File:
		{
			query.bindValue(":file_src_path", msg.file_src_path);
			query.bindValue(":file_is_dir", msg.file_is_dir);
			query.bindValue(":file_size", msg.file_size);
			break;
		}
		default:
			assert(0);
	}
	if (db_query->safe_exec(query) && query.next())
	{
		message_id = query.value(0).toULongLong();
		return true;
	}
	return false;
}

DBQueryAddChatMessage::DBQueryAddChatMessage(const Chat* chat, const DescChatMessage& message)
: m_message(message)
{
	for (const auto& pair : chat->users())
		m_receivers.push_back(pair.first);
}
void DBQueryAddChatMessage::on_process(QSqlDatabase& database)
{
	if (database.transaction())
	{
		if (insert_message(this, database, m_message, m_message.id))
		{
			if (insert_history_stamp(database))
			{
				if (insert_receivers(database, m_message.id, m_message.chat_id))
				{
					database.commit();
					m_success = true;
					return;
				}
			}
		}
		database.rollback();
	}
}
void DBQueryAddChatMessage::on_result()
{
	emit storred(m_success, m_message);
}
bool DBQueryAddChatMessage::insert_receivers(QSqlDatabase& database, ChatMessageId message_id, ChatId chat_id) const
{
	for (UserId receiver_id : m_receivers)
	{
		if (!insert_receiver(database, message_id, chat_id, receiver_id))
			return false;
	}
	return true;
}
bool DBQueryAddChatMessage::insert_receiver(QSqlDatabase& database, ChatMessageId message_id, ChatId chat_id, UserId receiver_id) const
{
	QSqlQuery query(database);
	query.prepare("insert into receivers (message_id, chat_id, receiver_id) values (?, ?, ?)");
	query.addBindValue(message_id);
	query.addBindValue(chat_id);
	query.addBindValue(receiver_id);
	return safe_exec(query);
}
bool DBQueryAddChatMessage::insert_history_stamp(QSqlDatabase& database) const
{
	if (m_message.type == ChatMessageType::Text)
	{
		QSqlQuery query(database);
		query.prepare("insert into history_messages (id, message_id, datetime, type, txt) values (?, ?, ?, ?, ?)");
		query.addBindValue(m_message.stamp);
		query.addBindValue(m_message.id);
		query.addBindValue(m_message.datetime);
		query.addBindValue(static_cast<int>(m_message.history_status));
		query.addBindValue(m_message.text);
		return safe_exec(query);
	}
	return true;
}


DBQueryAddFileChatMessage::DBQueryAddFileChatMessage(const DescChatMessage& message, UserId receiver_id)
: m_message(message)
, m_receiver_id(receiver_id)
{
}
void DBQueryAddFileChatMessage::on_process(QSqlDatabase& database)
{
	if (!database.transaction()) return;

	if (insert_message(this, database, m_message, m_message.id) && insert_sender(database) && insert_receiver(database))
	{
		database.commit();
		m_success = true;
		return;
	}
	database.rollback();
}
void DBQueryAddFileChatMessage::on_result()
{
	emit storred(m_success, m_message);
}
bool DBQueryAddFileChatMessage::insert_sender(QSqlDatabase& database) const
{
	QSqlQuery query(database);
	query.prepare("INSERT INTO receivers (message_id, chat_id, receiver_id, file_status) VALUES (:message_id, :chat_id, :receiver_id, :file_status)");
	query.bindValue(":message_id",	m_message.id);
	query.bindValue(":chat_id",		m_message.chat_id);
	query.bindValue(":receiver_id",	m_message.sender_id);
	query.bindValue(":file_status",	static_cast<unsigned short>(m_message.file_status));
	return safe_exec(query);
}
bool DBQueryAddFileChatMessage::insert_receiver(QSqlDatabase& database) const
{
	if (m_receiver_id == 0) return true;

	QSqlQuery query(database);
	query.prepare("INSERT INTO receivers (message_id, chat_id, receiver_id, file_dest_name, file_status) VALUES (:message_id, :chat_id, :receiver_id, :file_dest_name, :file_status)");
	query.bindValue(":message_id",		m_message.id);
	query.bindValue(":chat_id",			m_message.chat_id);
	query.bindValue(":receiver_id",		m_receiver_id);
	query.bindValue(":file_dest_name",	m_message.file_dest_name);
	query.bindValue(":file_status",		static_cast<unsigned short>(m_message.file_status));
	return safe_exec(query);
}




void DBQueryMaxChatMessageStamp::on_process(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("select MAX(stamp) from messages");
	if (safe_exec(query) && query.next())
		m_result_stamp = query.value(0).toULongLong();
}
void DBQueryMaxChatMessageStamp::on_result()
{
	emit loaded(m_result_stamp);
}




DescChatMessage select_message(QSqlQuery& query, bool historical, UserId* receiver_id)
{
	QDateTime datetime_readed = query.value("datetime_readed").toDateTime().toUTC();
	DescChatMessage message(
		query.value("id").toULongLong(),
		query.value("local_id").toUuid(),
		query.value("chat_id").toULongLong(),
		query.value("sender_id").toUInt(),
		query.value("stamp").toULongLong(),
		static_cast<ChatMessageType>(query.value("type").toInt()),
		datetime_readed.isNull() ? ChatMessageStatus::Sended : ChatMessageStatus::Readed,	// status
		static_cast<HistoryStatus>(query.value("history_status").toInt()),
		query.value("datetime").toDateTime().toUTC(),
		datetime_readed,
		historical
	);

	switch (message.type)
	{
		case ChatMessageType::Text:
		{
			message.text = query.value("txt").toString();
			break;
		}
		case ChatMessageType::File:
		{
			message.file_src_path = query.value("file_src_path").toString();
			message.file_dest_name = query.value("file_dest_name").toString();
			message.file_is_dir = query.value("file_is_dir").toBool();
			message.file_size = query.value("file_size").toULongLong();
			message.file_status = static_cast<FileTransferResult>(query.value("file_status").toInt());
			break;
		}
		default:
			assert(0);
	}

	if (receiver_id)
		*receiver_id = query.value("receiver_id").toUInt();

	return message;
}
std::vector<DescChatMessage> select_messages(QSqlQuery& query, bool historical)
{
	std::vector<DescChatMessage> messages;
	while (query.next())
		messages.push_back(select_message(query, historical, nullptr));

	return std::move(messages);
}

SelectHistoryMessagesQuery::SelectHistoryMessagesQuery(ChatId chat_id, const User* receiver, ChatMessageId message_id, int num_requested_messages)
: m_chat_id(chat_id)
, m_receiver(receiver)
, m_message_id(message_id)
, m_num_requested_messages(num_requested_messages)
{
}
static QString sql_select_history =
	"SELECT m.id, m.local_id, m.chat_id, m.sender_id, m.stamp, m.type, m.datetime, r.datetime_readed, m.history_status, m.txt, m.file_src_path, m.file_is_dir, m.file_size, r.file_dest_name, r.file_status FROM messages m \
	join receivers r on m.id = r.message_id and r.receiver_id = :receiver_id and m.chat_id = :chat_id and m.id < :message_id order by m.id desc limit ";
void SelectHistoryMessagesQuery::on_process(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare(sql_select_history + QString::number(m_num_requested_messages));
	query.bindValue(":chat_id", m_chat_id);
	query.bindValue(":message_id", m_message_id);
	query.bindValue(":receiver_id", m_receiver->id);
	if (safe_exec(query))
		m_result_messages = select_messages(query, true);
}
void SelectHistoryMessagesQuery::on_result()
{
	emit messages_uploaded(m_chat_id, m_result_messages, m_result_messages.size() != m_num_requested_messages);
}


DBQuerySelectRestoreMessages::DBQuerySelectRestoreMessages(const User* receiver, ChatMessageStamp last_message_stamp, ChatMessageId last_readed_message_id)
: m_success(false)
, m_receiver(receiver)
, m_last_message_stamp(last_message_stamp)
, m_last_readed_message_id(last_readed_message_id)
, m_result_messages(0, 0)
{
}
void DBQuerySelectRestoreMessages::on_process(QSqlDatabase& database)
{
	// !!! m_success - добавить проверку

	if (m_last_message_stamp == 0)
	{
		// у клиента нет никаких сообщений, проверяем есть ли новые непрочитанные сообщения для отправки их клиенту
		select_unread_messages(database);
	}
	else
	{
		// запрашиваем все сообщения, что > last_stamp
		select_restore_messages(database);
	}
}

// выбираем сообщения только для чатов из чат-листа
static QString sql_select_restore_messages = "\
SELECT M.id, M.local_id, M.chat_id, M.sender_id, M.stamp, M.type, M.datetime, R.datetime_readed, M.history_status, M.txt, M.file_src_path, M.file_is_dir, M.file_size, R.file_dest_name, R.file_status \
FROM (SELECT chat_id FROM chat_lists WHERE user_id = :user_id) AS CL \
	JOIN receivers AS R \
		ON R.chat_id = CL.chat_id AND R.receiver_id = :user_id \
	JOIN messages AS M \
		ON M.id = R.message_id \
WHERE M.stamp > :last_stamp order by M.id ASC\
";
void DBQuerySelectRestoreMessages::select_restore_messages(QSqlDatabase& database)
{
	if (database.transaction())
	{
		QSqlQuery query(database);
		query.prepare(sql_select_restore_messages);
		query.bindValue(":user_id", m_receiver->id);
		query.bindValue(":last_stamp", m_last_message_stamp);
		if (safe_exec(query))
			m_result_messages = select_messages(query, false);

		select_readed_messages(database);
		database.commit();
	}
}

static QString sql_select_unread_messages = "\
SELECT M.id, M.local_id, M.chat_id, M.sender_id, M.stamp, M.type, M.datetime, R.datetime_readed, M.history_status, M.txt, M.file_src_path, M.file_is_dir, M.file_size, R.file_dest_name, R.file_status \
FROM receivers AS R \
	JOIN \
		(\
			SELECT CL.user_id, \
			 	CL.chat_id, \
				CASE when last_read_message_id IS NULL \
				THEN 0 ELSE \
					last_read_message_id \
				END \
			FROM chat_lists AS CL \
				LEFT JOIN last_read_messages AS LRM \
					ON CL.chat_id = LRM.chat_id AND CL.user_id = LRM.user_id \
			WHERE CL.user_id = :user_id \
		) AS VT \
		ON R.receiver_id = VT.user_id AND r.chat_id = VT.chat_id AND VT.last_read_message_id < R.message_id \
	JOIN messages AS M \
		ON M.id = R.message_id \
";
void DBQuerySelectRestoreMessages::select_unread_messages(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare(sql_select_unread_messages);
	query.bindValue(":user_id", m_receiver->id);
	if (safe_exec(query))
		m_result_messages = select_messages(query, false);
}



static QString sql_select_read_messages = "\
SELECT LRM.chat_id, LRM.last_read_message_id, R.datetime_readed last_read_datetime \
FROM( \
	SELECT LRM.chat_id, LRM.last_read_message_id \
	FROM last_read_messages AS LRM \
	JOIN(SELECT chat_id FROM chat_lists WHERE user_id = :user_id) AS CL \
	ON user_id = :user_id and CL.chat_id = LRM.chat_id \
	WHERE LRM.last_read_message_id > :last_readed_message_id \
) as LRM \
JOIN receivers AS R \
ON R.message_id = LRM.last_read_message_id AND R.receiver_id = :user_id";
// выбрать максимально прочитанные сообщения на чаты
void DBQuerySelectRestoreMessages::select_readed_messages(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare(sql_select_read_messages);
	query.bindValue(":user_id", m_receiver->id);
	query.bindValue(":last_readed_message_id", m_last_readed_message_id);
	if (!safe_exec(query)) return;
	
	while (query.next())
	{
		ChatId chat_id = query.value("chat_id").toULongLong();
		ChatMessageId last_read_message_id = query.value("last_read_message_id").toULongLong();
		QDateTime last_read_datetime = query.value("last_read_datetime").toDateTime().toUTC();
		m_result_readed_messages.emplace_back(LastReadMessage { chat_id, last_read_message_id, last_read_datetime });
	}
}
void DBQuerySelectRestoreMessages::on_result()
{
	emit messages_uploaded(m_success, m_result_messages, m_result_readed_messages);
}



DBQuerySetReadedMessages::DBQuerySetReadedMessages(const Chat* chat, const User* receiver, ChatMessageId new_read_message_id, const QDateTime& datetime)
: m_chat(chat)
, m_receiver(receiver)
, m_new_read_message_id(new_read_message_id)
, m_datetime(datetime)
, m_success(false)
{
}
bool DBQuerySetReadedMessages::select_last_read_messages(QSqlDatabase& database, ChatMessageId& last_read_message_id)
{
	QSqlQuery query(database);
	query.prepare("select last_read_message_id from last_read_messages where chat_id=:chat_id and user_id=:user_id");
	query.bindValue(":chat_id", m_chat->id());
	query.bindValue(":user_id", m_receiver->id);
	if (safe_exec(query))
	{
		last_read_message_id = 0;
		if (query.next())
			last_read_message_id = query.value(0).toULongLong();
		return true;
	}
	return false;
}
static QString sql_udpate_datetime_read_last_read_messages = "\
insert into last_read_messages AS LRM(chat_id, user_id, last_read_message_id) values(:chat_id, :user_id, :last_read_message_id) \
	on conflict(chat_id, user_id) DO update SET last_read_message_id=:last_read_message_id WHERE LRM.chat_id=:chat_id AND LRM.user_id=:user_id \
";
bool DBQuerySetReadedMessages::update_last_read_messages(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare(sql_udpate_datetime_read_last_read_messages);
	query.bindValue(":chat_id", m_chat->id());
	query.bindValue(":user_id", m_receiver->id);
	query.bindValue(":last_read_message_id", m_new_read_message_id);
	return safe_exec(query);
}
bool DBQuerySetReadedMessages::update_receivers(QSqlDatabase& database, ChatMessageId last_read_message_id)
{
	QSqlQuery query(database);
	query.prepare("update receivers set datetime_readed=? where chat_id=? and receiver_id=? and message_id in (select id from messages where id between ? and ? and sender_id<>?)");
	query.addBindValue(m_datetime);
	query.addBindValue(m_chat->id());
	query.addBindValue(m_receiver->id);
	query.addBindValue(last_read_message_id + 1);
	query.addBindValue(m_new_read_message_id);
	query.addBindValue(m_receiver->id);
	return safe_exec(query);
}
void DBQuerySetReadedMessages::on_process(QSqlDatabase& database)
{
	if (!database.transaction()) return;
	
	ChatMessageId last_read_message_id = 0;
	m_success = select_last_read_messages(database, last_read_message_id)
		&& (last_read_message_id < m_new_read_message_id)
		&& update_last_read_messages(database)
		&& update_receivers(database, last_read_message_id);

	//logs(QString(" message readed %1 %2 %3").arg(m_success).arg(last_read_message_id).arg(m_new_read_message_id));
	
	if (m_success) database.commit();
	else database.rollback();
}
void DBQuerySetReadedMessages::on_result()
{
	emit finished(m_success, m_chat, m_new_read_message_id, m_datetime);
}




DBQueryFileMessageSetStatus::DBQueryFileMessageSetStatus(ChatMessageId message_id, UserId sender_id, ChatMessageStamp stamp, FileTransferResult file_status, UserId receiver_id, const QString& file_dest_name)
: m_message_id(message_id)
, m_sender_id(sender_id)
, m_stamp(stamp)
, m_file_status(file_status)
, m_receiver_id(receiver_id)
, m_file_dest_name(file_dest_name)
, m_success(false)
{
}
void DBQueryFileMessageSetStatus::on_process(QSqlDatabase& database)
{
	if (!database.transaction()) return;

	if (update_message_stamp(database) && update_sender(database) && update_receiver(database) && select_chat_message(database))
	{
		database.commit();
		m_success = true;
		return;
	}
	database.rollback();
}
void DBQueryFileMessageSetStatus::on_result()
{
	emit finished(m_success, m_message_receivers);
}
bool DBQueryFileMessageSetStatus::update_message_stamp(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("update messages set stamp=? where id=?");
	query.addBindValue(m_stamp);
	query.addBindValue(m_message_id);
	return safe_exec(query);
}
bool DBQueryFileMessageSetStatus::update_sender(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("update receivers set file_status=? where message_id=? and receiver_id=?");
	query.addBindValue(static_cast<unsigned short>(m_file_status));
	query.addBindValue(m_message_id);
	query.addBindValue(m_sender_id);
	return safe_exec(query);
}
bool DBQueryFileMessageSetStatus::update_receiver(QSqlDatabase& database)
{
	if (m_receiver_id == 0) return true;

	QSqlQuery query(database);

	static QString sql_query = "\
INSERT INTO receivers (message_id, receiver_id, chat_id, file_status, file_dest_name) VALUES (\
	:message_id, \
	:receiver_id, \
	(SELECT chat_id FROM receivers WHERE message_id=:message_id AND receiver_id=:sender_id), \
	:file_status, \
	:file_dest_name) \
on conflict (message_id, receiver_id) DO UPDATE SET file_status=:file_status, file_dest_name=:file_dest_name WHERE receivers.message_id=:message_id and receivers.receiver_id=:receiver_id";

	query.prepare(sql_query);
	query.bindValue(":file_status", static_cast<unsigned short>(m_file_status));
	query.bindValue(":file_dest_name", m_file_dest_name);
	query.bindValue(":message_id", m_message_id);
	query.bindValue(":receiver_id", m_receiver_id);
	query.bindValue(":sender_id", m_sender_id);
	return safe_exec(query);
}
static QString sql_select_message =
"SELECT m.id, m.local_id, m.chat_id, m.sender_id, r.receiver_id, m.stamp, m.type, m.datetime, r.datetime_readed, m.history_status, m.txt, m.file_src_path, m.file_is_dir, m.file_size, r.file_dest_name, r.file_status FROM messages m \
	join receivers r on m.id = r.message_id and m.id = :message_id";
bool DBQueryFileMessageSetStatus::select_chat_message(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare(sql_select_message);
	query.bindValue(":message_id", m_message_id);
	//query.bindValue(":receiver_id", m_receiver_id != 0 ? m_receiver_id : m_sender_id);		// выбираем как для получателя
	if (safe_exec(query))		// && query.next())
	{
		while (query.next())
		{
			UserId receiver_id = 0;
			DescChatMessage msg = select_message(query, false, &receiver_id);
			m_message_receivers[receiver_id] = msg;
		}
		return true;
	}
	return false;
}