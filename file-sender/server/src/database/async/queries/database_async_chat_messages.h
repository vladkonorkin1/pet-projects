//=====================================================================================//
//   Author: open
//   Date:   03.07.2017
//=====================================================================================//
#pragma once
#include "database/async/database_async_query.h"
#include <QVariant>

class User;
class Chat;

class DBQueryAddChatMessage : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	std::vector<UserId> m_receivers;
	DescChatMessage m_message;
	bool m_success = false;

public:
	DBQueryAddChatMessage(const Chat* chat, const DescChatMessage& message);
	virtual void on_process(QSqlDatabase& database);
	virtual void on_result();

signals:
	void storred(bool success, const DescChatMessage& chat_message);

private:
	bool insert_receiver(QSqlDatabase& database, ChatMessageId message_id, ChatId chat_id, UserId receiver_id) const;
	bool insert_receivers(QSqlDatabase& database, ChatMessageId message_id, ChatId chat_id) const;
	bool insert_history_stamp(QSqlDatabase& database) const;
};

class DBQueryAddFileChatMessage : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	DescChatMessage m_message;
	UserId m_receiver_id;
	bool m_success = false;

public:
	DBQueryAddFileChatMessage(const DescChatMessage& message, UserId receiver_id);
	virtual void on_process(QSqlDatabase& database);
	virtual void on_result();

signals:
	void storred(bool success, const DescChatMessage& chat_message);

private:
	bool insert_sender(QSqlDatabase& database) const;
	bool insert_receiver(QSqlDatabase& database) const;
};

class DBQueryMaxChatMessageStamp : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	ChatMessageStamp m_result_stamp = 0;

public:
	virtual void on_process(QSqlDatabase& database);
	virtual void on_result();

signals:
	void loaded(ChatMessageStamp stamp);
};


// запросить порцию сообщений в обратном порядке начиная с message_id
class SelectHistoryMessagesQuery : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	ChatId m_chat_id;
	const User* m_receiver;
	ChatMessageId m_message_id;
	int m_num_requested_messages;

	// result
	std::vector<DescChatMessage> m_result_messages;

public:
	SelectHistoryMessagesQuery(ChatId chat_id, const User* receiver, ChatMessageId message_id, int num_requested_messages);
	virtual void on_process(QSqlDatabase& database);
	virtual void on_result();

signals:
	void messages_uploaded(ChatId chat_id, const std::vector<DescChatMessage>& messages, bool finish_upload);
};

// запросить все последние сообщения начиная с message_stamp
// и какие прочитанные сообщения
class DBQuerySelectRestoreMessages : public DatabaseAsyncQuery
{
	Q_OBJECT
public:
	struct LastReadMessage
	{
		ChatId chat_id;
		ChatMessageId message_id;
		QDateTime datetime;
	};

private:
	bool m_success;
	const User* m_receiver;
	ChatMessageStamp m_last_message_stamp;
	ChatMessageId m_last_readed_message_id;

	// result
	std::vector<DescChatMessage> m_result_messages;
	std::vector<LastReadMessage> m_result_readed_messages;	// последние прочитанные сообщения, отдаваемые клиенту

public:
	DBQuerySelectRestoreMessages(const User* receiver, ChatMessageStamp last_message_stamp, ChatMessageId last_readed_message_id);
	virtual void on_process(QSqlDatabase& database);
	virtual void on_result();

signals:
	void messages_uploaded(bool success, const std::vector<DescChatMessage>& messages, const std::vector<DBQuerySelectRestoreMessages::LastReadMessage>& chat_last_read_messages);

private:
	// выбрать максимально прочитанные сообщения на чаты
	void select_readed_messages(QSqlDatabase& database);
	// запрашиваем все сообщения, что > last_stamp
	void select_restore_messages(QSqlDatabase& database);
	// выбираем новые непрочитанные сообщения для отправки их клиенту
	void select_unread_messages(QSqlDatabase& database);
};



class DBQuerySetReadedMessages : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	const User* m_receiver;
	const Chat* m_chat;
	ChatMessageId m_new_read_message_id;
	QDateTime m_datetime;
	bool m_success;

public:
	DBQuerySetReadedMessages(const Chat* chat, const User* receiver, ChatMessageId new_read_message_id, const QDateTime& datetime);
	virtual void on_process(QSqlDatabase& database);
	virtual void on_result();

signals:
	void finished(bool success, const Chat* chat, ChatMessageId message_id, const QDateTime& datetime);

private:
	bool select_last_read_messages(QSqlDatabase& database, ChatMessageId& last_read_message_id);
	bool update_last_read_messages(QSqlDatabase& database);
	bool update_receivers(QSqlDatabase& database, ChatMessageId last_read_message_id);	
};


class DBQueryFileMessageSetStatus: public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	ChatMessageId m_message_id;
	UserId m_sender_id;
	UserId m_receiver_id;
	ChatMessageStamp m_stamp;
	FileTransferResult m_file_status;
	QString m_file_dest_name;
	bool m_success;
	
	std::map<UserId, DescChatMessage> m_message_receivers;		// сообщения по получателям

public:
	DBQueryFileMessageSetStatus(ChatMessageId message_id, UserId sender_id, ChatMessageStamp stamp, FileTransferResult file_status, UserId receiver_id, const QString& file_dest_name);
	virtual void on_process(QSqlDatabase& database);
	virtual void on_result();

signals:
	void finished(bool success, const std::map<UserId, DescChatMessage>& message_receivers);

private:
	bool update_message_stamp(QSqlDatabase& database);
	bool update_sender(QSqlDatabase& database);
	bool update_receiver(QSqlDatabase& database);
	bool select_chat_message(QSqlDatabase& database);
};