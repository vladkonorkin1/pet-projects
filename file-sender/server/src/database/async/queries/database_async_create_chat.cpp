//=====================================================================================//
//   Author: open
//   Date:   21.02.2019
//=====================================================================================//
#include "base/precomp.h"
#include "database_async_create_chat.h"
#include "user.h"

Q_DECLARE_METATYPE(CreateChat::chat_users_t);

CreateChat::CreateChat(const User* creator_user, Timestamp timestamp)
: m_creator_user(creator_user)
, m_timestamp(timestamp)
{
	qRegisterMetaType<CreateChat::chat_users_t>();
}


CreateContactChat::CreateContactChat(const User* creator_user, Timestamp timestamp, const User* target_contact)
: CreateChat(creator_user, timestamp)
, m_target_contact(target_contact)
{
}


CreateGroupChat::CreateGroupChat(const User* creator_user, Timestamp timestamp, const std::vector<const User*>& chat_users)
: CreateChat(creator_user, timestamp)
, m_chat_users(build_contacts(chat_users))
, m_name(build_name())
{
}
QVariant CreateGroupChat::name() const {
	return m_name;
}
QString CreateGroupChat::build_name() const
{
	QStringList list;
	for (auto pair : m_chat_users)
		list << pair.second->name;
	return list.join(", ");
}
CreateGroupChat::chat_users_t CreateGroupChat::build_contacts(const std::vector<const User*>& chat_users) const
{
	chat_users_t result;
	for (const User* user : chat_users)
		result[user->id] = user;	// устраняем дубликаты
	return result;
}