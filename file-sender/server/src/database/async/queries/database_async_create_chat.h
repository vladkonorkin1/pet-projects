//=====================================================================================//
//   Author: open
//   Date:   21.02.2019
//=====================================================================================//
#pragma once
#include <QVariant>

class User;
class Chat;

class CreateChat : public QObject
{
	Q_OBJECT
public:
	struct ChatUser
	{
		const User* user;
		bool admin;
	};
	using chat_users_t = std::vector<ChatUser>;

private:
	const User* m_creator_user;		// создатель чата
	Timestamp m_timestamp;

public:
	CreateChat(const User* creator_user, Timestamp timestamp);
	const User* creator_user() { return m_creator_user; }
	virtual Timestamp timestamp() const { return m_timestamp; }
	virtual QVariant name() const { return QVariant(); }
	//void notify_create_chat(Chat* chat) { emit chat_created(chat); }
signals:
	void chat_created(Chat* chat);
};
using UCreateChat = std::shared_ptr<CreateChat>;


class CreateContactChat : public CreateChat
{
	Q_OBJECT
private:
	const User* m_target_contact;	// тот, кого добавляют в чат
public:
	CreateContactChat(const User* creator_user, Timestamp timestamp, const User* target_contact);
	const User* target_contact() { return m_target_contact; }
};
using UCreateContactChat = std::shared_ptr<CreateContactChat>;


class CreateGroupChat : public CreateChat
{
	Q_OBJECT
public:
	using chat_users_t = std::map<UserId, const User*>;
private:
	chat_users_t m_chat_users;
	QString m_name;
public:
	CreateGroupChat(const User* creator_user, Timestamp timestamp, const std::vector<const User*>& chat_users);
	const chat_users_t& chat_users() const { return m_chat_users; }
	virtual QVariant name() const;
private:
	QString build_name() const;
	chat_users_t build_contacts(const std::vector<const User*>& chat_users) const;
};
using UCreateGroupChat = std::shared_ptr<CreateGroupChat>;