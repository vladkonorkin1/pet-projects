//=====================================================================================//
//   Author: open
//   Date:   26.06.2017
//=====================================================================================//
#include "base/precomp.h"
#include "database_async_query_chat.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include "chat.h"
#include "user.h"

CreateChatQuery::CreateChatQuery(UCreateChat creator, const CreateChat::chat_users_t& chat_users)
: m_creator(std::move(creator))
, m_chat_users(chat_users)
{
	
}
void CreateChatQuery::on_process(QSqlDatabase& database)
{
	if (database.transaction())
	{
		m_chat_id = insert_chats(database);
		if (m_chat_id > 0 && insert_members(database, m_chat_id))
		{
			database.commit();
			return;
		}
		database.rollback();
		m_chat_id = 0;
	}
}
void CreateChatQuery::on_result()
{
	emit chat_created(m_chat_id, m_creator.get(), m_chat_users);
}
ChatId CreateChatQuery::insert_chats(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("insert into chats(id, timestamp, creator_id, name) values(default, ?, ?, ?) returning id");
	query.addBindValue(QDateTime::fromMSecsSinceEpoch(m_creator->timestamp(), Qt::TimeSpec::UTC));
	query.addBindValue(m_creator->creator_user()->id);
	query.addBindValue(m_creator->name());
	if (safe_exec(query) && query.next())
		return query.value(0).toULongLong();
	return 0;
}
bool CreateChatQuery::insert_members(QSqlDatabase& database, ChatId chat_id)
{
	QSqlQuery query(database);
	query.prepare("insert into chat_users(chat_id, user_id, admin) values(?,?,?)");
	for (const CreateChat::ChatUser& chat_user : m_chat_users)
	{
		query.addBindValue(chat_id);
		query.addBindValue(chat_user.user->id);
		query.addBindValue(chat_user.admin ? chat_user.admin : QVariant());
		if (!safe_exec(query))
			return false;
	}
	return true;
}


SetChatListLastTimeQuery::SetChatListLastTimeQuery(ChatId chat_id, const QDateTime& time)
: m_last_time(time)
, m_chat_id(chat_id)
{
}
void SetChatListLastTimeQuery::on_process(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("update chat_lists set last_time=:last_time \
		from (select user_id from chat_users where chat_id=:chat_id) as c \
		where chat_lists.user_id=c.user_id and chat_lists.chat_id=:chat_id");
	query.bindValue(":chat_id", m_chat_id);
	query.bindValue(":last_time", m_last_time);
	safe_exec(query);
}

SetChatListContactLastTimeQuery::SetChatListContactLastTimeQuery(UserId user_id, ChatId chat_id, const QDateTime& time)
: m_user_id(user_id)
, m_chat_id(chat_id)
, m_last_time(time)
{
}
void SetChatListContactLastTimeQuery::on_process(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("update chat_lists set last_time=? where chat_id=? and user_id=?");
	query.addBindValue(m_last_time);
	query.addBindValue(m_chat_id);
	query.addBindValue(m_user_id);
	safe_exec(query);
}


// обновить таймштамп для чата
bool update_chat_timestamp(DatabaseAsyncQuery* db_query, QSqlDatabase& database, ChatId chat_id, Timestamp timestamp)
{
	QSqlQuery query(database);
	query.prepare("update chats set timestamp=? where id=?");
	query.addBindValue(QDateTime::fromMSecsSinceEpoch(timestamp, Qt::TimeSpec::UTC));
	query.addBindValue(chat_id);
	return db_query->safe_exec(query);
}

DBQueryRemoveChatUser::DBQueryRemoveChatUser(const Chat* chat, const User* target_user, Timestamp timestamp)
: m_target_user(target_user)
, m_chat(chat)
, m_timestamp(timestamp)
{
}
void DBQueryRemoveChatUser::on_process(QSqlDatabase& database)
{
	if (database.transaction())
	{
		if (delete_chat_user(database) && update_chat_timestamp(this, database, m_chat->id(), m_timestamp))
			database.commit();
		else
			database.rollback();
	}
}
bool DBQueryRemoveChatUser::delete_chat_user(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("delete from chat_users where chat_id=? and user_id=?");
	query.addBindValue(m_chat->id());
	query.addBindValue(m_target_user->id);
	return safe_exec(query);
}


DBQueryAddOrRemoveChatAdmin::DBQueryAddOrRemoveChatAdmin(const Chat* chat, const User* target_user, bool add_or_remove)
: m_chat(chat)
, m_target_user(target_user)
, m_add_or_remove(add_or_remove)
{
}

void DBQueryAddOrRemoveChatAdmin::on_process(QSqlDatabase& database)
{
	if (database.transaction())
	{
		if (add_or_remove_chat_admin(database))
			database.commit();
		else
			database.rollback();
	}
}

bool DBQueryAddOrRemoveChatAdmin::add_or_remove_chat_admin(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("update chat_users set admin = ? where chat_id = ? and user_id = ?");
	query.addBindValue(m_add_or_remove);
	query.addBindValue(m_chat->id());
	query.addBindValue(m_target_user->id);
	return safe_exec(query);
}


DBQueryAddChatUser::DBQueryAddChatUser(const Chat* chat, const User* target_user, bool admin, Timestamp timestamp)
: m_target_user(target_user)
, m_chat(chat)
, m_admin(admin)
, m_timestamp(timestamp)
{
}
void DBQueryAddChatUser::on_process(QSqlDatabase& database)
{
	if (database.transaction())
	{
		if (insert_chat_user(database) && update_chat_timestamp(this, database, m_chat->id(), m_timestamp))
			database.commit();
		else
			database.rollback();
	}
}
bool DBQueryAddChatUser::insert_chat_user(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("insert into chat_users(chat_id, user_id, admin) values(?,?,?)");
	query.addBindValue(m_chat->id());
	query.addBindValue(m_target_user->id);
	query.addBindValue(m_admin ? m_admin : QVariant());
	return safe_exec(query);
}



SetChatListContactFavoriteQuery::SetChatListContactFavoriteQuery(UserId user_id, ChatId chat_id, bool favorite)
: m_user_id(user_id)
, m_favorite(favorite)
, m_chat_id(chat_id)
{
}
void SetChatListContactFavoriteQuery::on_process(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("update chat_lists set favorite=? where chat_id=? and user_id=?");
	query.addBindValue(m_favorite);
	query.addBindValue(m_chat_id);
	query.addBindValue(m_user_id);
	safe_exec(query);
}


DBQuerySetChatName::DBQuerySetChatName(const Chat* chat, const QString& name, Timestamp timestamp)
: m_chat(chat)
, m_name(name)
, m_timestamp(timestamp)
{
	assert( m_chat->is_group() );
}
void DBQuerySetChatName::on_process(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("update chats set name=?, timestamp=? where id=?");
	query.addBindValue(m_name);
	query.addBindValue(QDateTime::fromMSecsSinceEpoch(m_timestamp, Qt::TimeSpec::UTC));
	query.addBindValue(m_chat->id());
	safe_exec(query);
}



AddChatToChatListQuery::AddChatToChatListQuery(UserId user_id, ChatId chat_id, bool favorite, const QDateTime& last_time)
: m_user_id(user_id)
, m_chat_id(chat_id)
, m_favorite(favorite)
, m_last_time(last_time)
{
}
void AddChatToChatListQuery::on_process(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("insert into chat_lists(user_id, chat_id, favorite, last_time) values(?,?,?,?)");
	query.addBindValue(m_user_id);
	query.addBindValue(m_chat_id);
	query.addBindValue(m_favorite);
	query.addBindValue(m_last_time);
	safe_exec(query);
}
void AddChatToChatListQuery::on_result()
{
	emit finished();
}



RemoveChatFromChatListQuery::RemoveChatFromChatListQuery(UserId user_id, ChatId chat_id)
: m_user_id(user_id)
, m_chat_id(chat_id)
{
}
void RemoveChatFromChatListQuery::on_process(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("delete from chat_lists where chat_id=? and user_id=?");
	query.addBindValue(m_chat_id);
	query.addBindValue(m_user_id);
	safe_exec(query);
}