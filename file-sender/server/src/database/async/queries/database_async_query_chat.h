//=====================================================================================//
//   Author: open
//   Date:   26.06.2017
//=====================================================================================//
#pragma once
#include "database/async/database_async_query.h"
//#include "database_async_delete_chat_user.h"
#include "database_async_create_chat.h"
#include <QDateTime>

class CreateChatQuery : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	UCreateChat m_creator;
	CreateChat::chat_users_t m_chat_users;
	ChatId m_chat_id = 0;

public:
	CreateChatQuery(UCreateChat creator, const CreateChat::chat_users_t& chat_users);
	virtual void on_process(QSqlDatabase& database);
	virtual void on_result();

signals:
	void chat_created(ChatId chat_id, CreateChat* creator, const CreateChat::chat_users_t& chat_users);
	
private:
	ChatId insert_chats(QSqlDatabase& database);
	bool insert_members(QSqlDatabase& database, ChatId chat_id);
};



/*class RemoveChatUserQuery : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	URemoveChatUser m_remove_chat_user;

public:
	RemoveChatUserQuery(URemoveChatUser remove_chat_user);
	virtual void exec(QSqlDatabase& database);

signals:
	void chat_user_removed(RemoveChatUser* remove_chat_user);
};*/
class DBQueryRemoveChatUser : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	const User* m_target_user;		// кого удаляем из чата
	const Chat* m_chat;
	Timestamp m_timestamp;

public:
	DBQueryRemoveChatUser(const Chat* chat, const User* target_user, Timestamp timestamp);
	virtual void on_process(QSqlDatabase& database);

private:
	bool delete_chat_user(QSqlDatabase& database);
};

class DBQueryAddOrRemoveChatAdmin : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	const User* m_target_user;
	const Chat* m_chat;
	bool m_add_or_remove;

public:
	DBQueryAddOrRemoveChatAdmin(const Chat* chat, const User* target_user, bool add_or_remove);
	virtual void on_process(QSqlDatabase& database);

private:
	bool add_or_remove_chat_admin(QSqlDatabase& database);
};


class SetChatListLastTimeQuery : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	ChatId m_chat_id;
	QDateTime m_last_time;

public:
	SetChatListLastTimeQuery(ChatId chat_id, const QDateTime& time);
	virtual void on_process(QSqlDatabase& db);
};
class SetChatListContactLastTimeQuery : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	UserId m_user_id;	// у какого пользователя поднять чат наверх
	ChatId m_chat_id;
	QDateTime m_last_time;

public:
	SetChatListContactLastTimeQuery(UserId user_id, ChatId chat_id, const QDateTime& time);
	virtual void on_process(QSqlDatabase& database);
};

class SetChatListContactFavoriteQuery : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	UserId m_user_id;
	ChatId m_chat_id;
	bool m_favorite;

public:
	SetChatListContactFavoriteQuery(UserId user_id, ChatId chat_id, bool favorite);
	virtual void on_process(QSqlDatabase& database);
};

class DBQuerySetChatName : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	const Chat* m_chat;
	QString m_name;
	Timestamp m_timestamp;

public:
	DBQuerySetChatName(const Chat* chat, const QString& name, Timestamp timestamp);
	virtual void on_process(QSqlDatabase& database);
};

class DBQueryAddChatUser : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	const User* m_target_user;		// кого добавляем в чат
	const Chat* m_chat;
	bool m_admin;
	Timestamp m_timestamp;

public:
	DBQueryAddChatUser(const Chat* chat, const User* target_user, bool admin, Timestamp timestamp);
	virtual void on_process(QSqlDatabase& database);

private:
	bool insert_chat_user(QSqlDatabase& database);
};

class AddChatToChatListQuery : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	UserId m_user_id;
	ChatId m_chat_id;
	QDateTime m_last_time;
	bool m_favorite;

public:
	AddChatToChatListQuery(UserId user_id, ChatId chat_id, bool favorite, const QDateTime& last_time);
	virtual void on_process(QSqlDatabase& database);
	virtual void on_result();

signals:
	void finished();
};

class RemoveChatFromChatListQuery : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	UserId m_user_id;	// у кого удаляем чат из чат-листа
	ChatId m_chat_id;

public:
	RemoveChatFromChatListQuery(UserId user_id, ChatId chat_id);
	virtual void on_process(QSqlDatabase& database);
};