//=====================================================================================//
//   Author: open
//   Date:   09.08.2019
//=====================================================================================//
#include "base/precomp.h"
#include "database_async_users.h"
#include <QSqlQuery>
#include <QVariant>

void DBQuerySelectUsers::on_process(QSqlDatabase& database)
{
	m_success = select_departments(database) && select_users(database);
}
bool DBQuerySelectUsers::select_departments(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("select * from departments");
	if (safe_exec(query))
	{
		while (query.next())
		{
			m_departments.push_back(DatabaseDepartment {
				query.value("id").toUInt(),
				query.value("uuid").toUuid(),
				query.value("name").toString(),
				query.value("timestamp").toDateTime().toUTC().toMSecsSinceEpoch()
			});
		}
		return true;
	}
	return false;
}
bool DBQuerySelectUsers::select_users(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("select * from users");
	if (safe_exec(query))
	{
		while (query.next())
		{
			DatabaseUser user;
			user.id = query.value("id").toUInt();
			user.uuid = query.value("uuid").toUuid();
			user.login = query.value("login").toString();
			user.name = query.value("name").toString();
			user.folder = query.value("folder").toString();
			QDateTime datetime_timestamp = query.value("timestamp").toDateTime();
			{
				auto t1 = datetime_timestamp.toMSecsSinceEpoch();
				auto t2 = datetime_timestamp.toUTC().toMSecsSinceEpoch();
				assert( t1 == t2 );
			}
			user.timestamp = datetime_timestamp.toUTC().toMSecsSinceEpoch();
			user.department_id = static_cast<DepartmentId>(query.value("department_id").toUInt());
			user.deleted = query.value("deleted").toBool();
			m_users.push_back(user);
		}
		return true;
	}
	return false;
}
void DBQuerySelectUsers::on_result()
{
	emit finished(m_success, m_departments, m_users);
}



DBQueryUpdateDepartments::DBQueryUpdateDepartments(const std::vector<Department>& departments)
: m_input_departments(departments)
{
}
void DBQueryUpdateDepartments::on_process(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("	INSERT INTO departments(id, uuid, name, timestamp) VALUES((SELECT coalesce(MAX(id), 0) + 1 FROM departments), :uuid, :name, :timestamp) \
					ON CONFLICT(uuid) DO UPDATE SET name=:name, timestamp=:timestamp \
					RETURNING id");
	
	if (!database.transaction()) return;

	for (const Department& department : m_input_departments)
	{
		query.bindValue(":uuid", department.uuid);
		query.bindValue(":name", department.name);
		query.bindValue(":timestamp", QDateTime::fromMSecsSinceEpoch(department.timestamp, Qt::UTC));
		if (safe_exec(query) && query.next())
		{
			m_result_departments.emplace_back(DatabaseDepartment {
				query.value("id").toUInt(),
				department.uuid,
				department.name,
				department.timestamp
			});
		}
		else
		{
			database.rollback();
			return;
		}
	}
	database.commit();
	m_success = true;
}
void DBQueryUpdateDepartments::on_result()
{
	emit finished(m_success, m_result_departments);
}



DBQueryUpdateUser::DBQueryUpdateUser(const std::vector<User>& users)
: m_input_users(users)
{
}
void DBQueryUpdateUser::on_process(QSqlDatabase& database)
{
	QSqlQuery query(database);
	query.prepare("	INSERT INTO users(id, uuid, timestamp, login, name, deleted, department_id, folder) \
					VALUES((SELECT coalesce(MAX(id), 0) + 1 FROM users), :uuid, :timestamp, :login, :name, :deleted, :department_id, :folder) \
					ON CONFLICT(uuid) DO UPDATE SET timestamp=:timestamp, login=:login, name=:name, deleted=:deleted, department_id=:department_id, folder=:folder \
					RETURNING id");

	if (!database.transaction()) return;

	for (const User& user : m_input_users)
	{
		query.bindValue(":uuid", user.uuid);
		query.bindValue(":timestamp", QDateTime::fromMSecsSinceEpoch(user.timestamp, Qt::UTC));
		query.bindValue(":login", user.login);
		query.bindValue(":name", user.name);
		query.bindValue(":deleted", user.deleted);
		query.bindValue(":department_id", user.department_id);
		query.bindValue(":folder", user.folder);
		if (safe_exec(query) && query.next())
		{
			DatabaseUser database_user;
			database_user.id = query.value("id").toUInt();
			database_user.uuid = user.uuid;
			database_user.timestamp = user.timestamp;
			database_user.login = user.login;
			database_user.name = user.name;
			database_user.deleted = user.deleted;
			database_user.folder = user.folder;
			database_user.department_id = user.department_id;
			m_result_users.push_back(database_user);
		}
		else
		{
			database.rollback();
			return;
		}
	}
	database.commit();
	m_success = true;
}
void DBQueryUpdateUser::on_result()
{
	emit finished(m_success, m_result_users);
}