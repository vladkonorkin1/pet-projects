//=====================================================================================//
//   Author: open
//   Date:   09.08.2019
//=====================================================================================//
#pragma once
#include "database/async/database_async_query.h"

struct DatabaseDepartment
{
	DepartmentId id;
	QUuid uuid;
	QString name;
	Timestamp timestamp;
};

struct DatabaseUser
{
	UserId id;
	QUuid uuid;
	QString login;
	QString name;
	QString folder;
	Timestamp timestamp;
	DepartmentId department_id;
	bool deleted;

	DatabaseUser() : id(0), timestamp(0), department_id(0), deleted(false) {}
};

class DBQuerySelectUsers : public DatabaseAsyncQuery
{
	Q_OBJECT
private:
	bool m_success = false;
	std::vector<DatabaseDepartment> m_departments;
	std::vector<DatabaseUser> m_users;

public:
	virtual void on_process(QSqlDatabase& database);
	virtual void on_result();

signals:
	void finished(bool success, const std::vector<DatabaseDepartment>& departments, const std::vector<DatabaseUser>& users);

private:
	bool select_departments(QSqlDatabase& database);
	bool select_users(QSqlDatabase& database);
};



class DBQueryUpdateDepartments : public DatabaseAsyncQuery
{
	Q_OBJECT
public:
	struct Department
	{
		QUuid uuid;
		QString name;
		Timestamp timestamp;
	};

private:
	bool m_success = false;
	std::vector<Department> m_input_departments;
	std::vector<DatabaseDepartment> m_result_departments;

public:
	DBQueryUpdateDepartments(const std::vector<Department>& departments);
	virtual void on_process(QSqlDatabase& database);
	virtual void on_result();

signals:
	void finished(bool success, const std::vector<DatabaseDepartment>& departments);
};



class DBQueryUpdateUser : public DatabaseAsyncQuery
{
	Q_OBJECT
public:
	struct User
	{
		QUuid uuid;
		QString login;
		QString name;
		QString folder;
		Timestamp timestamp = 0;
		DepartmentId department_id;
		bool deleted = false;
	};

private:
	bool m_success = false;
	std::vector<User> m_input_users;
	std::vector<DatabaseUser> m_result_users;

public:
	DBQueryUpdateUser(const std::vector<User>& users);
	virtual void on_process(QSqlDatabase& database);
	virtual void on_result();

signals:
	void finished(bool success, const std::vector<DatabaseUser>& users);
};