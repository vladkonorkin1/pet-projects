//=====================================================================================//
//   Author: open
//   Date:   19.06.2017
//=====================================================================================//
#include "base/precomp.h"
#include "database.h"
#include <QSqlError>

Database::Database(const DatabaseParams& database_params)
: m_database(create_database_connection("database-sync-connection", database_params))
{
}
Database::~Database()
{
	m_database.close();
	QSqlDatabase::removeDatabase(m_database.databaseName());
}
bool Database::exec(QSqlQuery& query) const
{
	if (query.exec()) return true;
		
	QString msg = query.lastError().text();
	logs("database: " + msg);
	return false;
}
QSqlQuery Database::create_query()
{
	if (!m_database.isOpen())
		m_database.open();

	QSqlQuery query(m_database);
	return std::move(query);
}
// транзакции
bool Database::transaction()
{
	return m_database.transaction();
}
bool Database::commit()
{
	return m_database.commit();
}
bool Database::rollback()
{
	return m_database.rollback();
}