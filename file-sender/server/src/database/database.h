//=====================================================================================//
//   Author: open
//   Date:   19.06.2017
//=====================================================================================//
#pragma once
#include <QSqlDatabase>
#include <QSqlQuery>
#include "database/database_create_connection.h"

class Database : public QObject
{
	Q_OBJECT
protected:
	QSqlDatabase m_database;

public:
	Database(const DatabaseParams& database_params);
	virtual ~Database();
	// выполнить запрос
	bool exec(QSqlQuery& query) const;
	// создать запрос
	QSqlQuery create_query();
	// транзакции
	bool transaction();
	bool commit();
	bool rollback();
};