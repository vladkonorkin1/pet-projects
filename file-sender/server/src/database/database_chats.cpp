//=====================================================================================//
//   Author: open
//   Date:   19.06.2017
//=====================================================================================//
#include "base/precomp.h"
#include "database_chats.h"
#include "database/database.h"
#include <QVariant>
#include "user.h"

DatabaseChats::DatabaseChats(Database& database)
: m_database(database)
{
}
// запросить чаты, в которых пользователь является участником
void DatabaseChats::query_chats(const User* user)
{
	if (m_database.transaction())
	{
		std::vector<ChatId> chats;
		if (select_chats(user, chats) && select_chat_users(user))
		{
			emit load_chat_finished(chats);
			m_database.commit();
		}
		else m_database.rollback();
	}
}
bool DatabaseChats::select_chats(const User* user, std::vector<ChatId>& chats)
{
	QSqlQuery query = m_database.create_query();
	query.prepare(
		"select chats.*, last_message_id, last_message_stamp FROM chats join "\
		"(select ch.id, MAX(m.id) as last_message_id, MAX(m.stamp) as last_message_stamp from "\
		"(select chat_id as id from chat_users where user_id=:user_id union select chat_id from chat_lists where user_id=:user_id) ch "\
		"left join messages as m on ch.id = m.chat_id group by ch.id) maxs "\
		"on chats.id = maxs.id");

	query.bindValue(":user_id", user->id);
	if (!m_database.exec(query)) return false;

	while (query.next())
	{
		ChatId chat_id = query.value("id").toULongLong();
		QVariant vname = query.value("name");
		QString name = vname.toString();
		bool is_group = !vname.isNull();
		UserId creator_id = query.value("creator_id").toUInt();
		//bool shared = query.value("shared").toBool();
		ChatMessageId last_message_id = query.value("last_message_id").toULongLong();
		ChatMessageId last_message_stamp = query.value("last_message_stamp").toULongLong();
		Timestamp timestamp = query.value("timestamp").toDateTime().toMSecsSinceEpoch();
		emit load_chat(chat_id, name, is_group, creator_id, timestamp, last_message_id, last_message_stamp);
		chats.push_back(chat_id);
	}
	return true;
}
bool DatabaseChats::select_chat_users(const User* user)
{
	QSqlQuery query = m_database.create_query();
	query.prepare(
		"select chat_users.* from chat_users join "\
		"(select chat_id as id from chat_users where user_id=:user_id union select chat_id from chat_lists where user_id=:user_id) ch "\
		"on chat_users.chat_id = ch.id");
	query.bindValue(":user_id", user->id);
	if (!m_database.exec(query)) return false;
	
	while (query.next())
	{
		ChatId chat_id = query.value("chat_id").toULongLong();
		UserId user_id = query.value("user_id").toUInt();
		bool admin = query.value("admin").toBool();
		emit load_chat_user(chat_id, user_id, admin);
	}
	return true;
}
// запросить список чатов для пользователя
void DatabaseChats::query_chat_list(const User* user)
{
	QSqlQuery query = m_database.create_query();
	query.prepare("select * from chat_lists where user_id=:user_id");
	query.bindValue(":user_id", user->id);
	if (m_database.exec(query))
	{
		while (query.next())
		{
			UserId user_id = query.value("user_id").toUInt();
			ChatId chat_id = query.value("chat_id").toULongLong();
			bool favorite = query.value("favorite").toBool();
			QDateTime last_time = query.value("last_time").toDateTime().toUTC();
			emit load_chat_list(user_id, chat_id, favorite, last_time);
		}
	}
}
// загрузить чаты и список чатов для пользователя
bool DatabaseChats::load_chats(const User* user)
{
	if (m_mark_loaded.find(user->id) == m_mark_loaded.end())
	{
		m_mark_loaded.insert(user->id);
		query_chats(user);
		query_chat_list(user);
		return true;
	}
	return false;
}