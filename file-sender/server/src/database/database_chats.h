//=====================================================================================//
//   Author: open
//   Date:   19.06.2017
//=====================================================================================//
#pragma once
#include <QDateTime>

class Database;
class User;

class DatabaseChats : public QObject
{
	Q_OBJECT
private:
	Database& m_database;
	std::set<UserId> m_mark_loaded;

public:
	DatabaseChats(Database& database);
	// загрузить чаты и список чатов для пользователя
	bool load_chats(const User* user);

signals:
	void load_chat(ChatId chat_id, const QString& name, bool is_group, UserId creator_id, Timestamp timestamp, ChatMessageId last_message_id, ChatMessageStamp last_message_stamp);
	void load_chat_user(ChatId chat_id, UserId user_id, bool admin);
	void load_chat_finished(const std::vector<ChatId>& chats);
	void load_chat_list(UserId user_id, ChatId chat_id, bool favorite, const QDateTime& last_time);

private:
	bool select_chats(const User* user, std::vector<ChatId>& chats);
	bool select_chat_users(const User* user);

	// запросить чаты, в которых пользователь является участником
	void query_chats(const User* user);
	// запросить список чатов для пользователя
	void query_chat_list(const User* user);
};