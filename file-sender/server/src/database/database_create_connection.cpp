//=====================================================================================//
//   Author: open
//   Date:   14.06.2017
//=====================================================================================//
#include "base/precomp.h"
#include "database_create_connection.h"
#include <QCoreApplication>

QSqlDatabase create_database_connection(const QString& connection_name, const DatabaseParams& database_params)
{
	QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL", connection_name);
	db.setHostName(database_params.host_name);
	db.setDatabaseName(database_params.database_name);
	db.setUserName(database_params.user_name);
	db.setPassword(database_params.password);
	db.setPort(database_params.port);
	if (!db.open()) {
		logs("database: can't open: " + db.databaseName());
		QCoreApplication::exit();
	}
	return std::move(db);
}
