//=====================================================================================//
//   Author: open
//   Date:   14.06.2017
//=====================================================================================//
#pragma once
#include <QSqlDatabase>

struct DatabaseParams
{
	int num_connections = 0;
	QString host_name;
	QString database_name;
	QString user_name;
	QString password;
	int port = 0;

	DatabaseParams(int num_connections, const QString& host_name, const QString& database_name, const QString& user_name, const QString& password, int port) : num_connections(num_connections), host_name(host_name), database_name(database_name), user_name(user_name), password(password), port(port) {}
};

extern QSqlDatabase create_database_connection(const QString& connection_name, const DatabaseParams& database_params);