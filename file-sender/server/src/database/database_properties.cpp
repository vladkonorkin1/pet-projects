//=====================================================================================//
//   Author: open
//   Date:   18.02.2019
//=====================================================================================//
#include "base/precomp.h"
#include "database_properties.h"
#include "database.h"
#include <QVariant>

DatabaseProperties::DatabaseProperties(Database& database)
: m_database(database)
{
	QSqlQuery query = m_database.create_query();
	query.prepare("select key, value from properties");
	if (m_database.exec(query))
	{
		while (query.next())
			m_values[query.value(0).toString()] = query.value(1).toString();
	}
}
const QString& DatabaseProperties::get(const QString& key) const
{
	values_t::const_iterator it = m_values.find(key);
	if (m_values.end() != it) return it->second;
	static QString temp;
	return temp;
}
void DatabaseProperties::set(const QString& key, const QString& value)
{
	m_values[key] = value;

	QSqlQuery query = m_database.create_query();
	query.prepare("insert into properties(key, value) values(:key, :value) on conflict(key) do update set value=:value");
	query.bindValue(":key", key);
	query.bindValue(":value", value);
	m_database.exec(query);
}