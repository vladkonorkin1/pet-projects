//=====================================================================================//
//   Author: open
//   Date:   18.02.2019
//=====================================================================================//
#pragma once
//#include "database/database.h"

class Database;

class DatabaseProperties : public QObject
{
	Q_OBJECT
private:
	Database& m_database;
	using values_t = std::map<QString, QString>;
	values_t m_values;

public:
	DatabaseProperties(Database& database);
	const QString& get(const QString& key) const;
	void set(const QString& key, const QString& value);
};