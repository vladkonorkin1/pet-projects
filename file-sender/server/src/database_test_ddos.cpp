#include "base/precomp.h"
#include <random>
#include "base/utility.h"
#include "database_test_ddos.h"
#include "chat_lists.h"
#include "chat_list.h"
#include "database/async/queries/database_async_query_chat.h"
#include "database/async/queries/database_async_chat_messages.h"
#include "server.h"
#include <QFile>

static int users_count = 10000;
static unsigned int departaments_count = 20;
static unsigned int chats_count = 100;
static unsigned int messages_max_count = 1000;

/*
DatabaseTestDDOS::DatabaseTestDDOS(Server& server)
: m_server(server)
{

}
void DatabaseTestDDOS::start()
{
	create_test_users(users_count, departaments_count);
	create_chats(chats_count);
}
Department* DatabaseTestDDOS::create_department(DepartmentId id, const QString& name)
{
	Department* department = m_server.users().create_department(id, name);
	return department;
}

void DatabaseTestDDOS::create_test_users(int count, unsigned int count_deps)
{
	Timestamp timestamp = QDateTime::currentDateTimeUtc().toMSecsSinceEpoch();

	unsigned int dep_id = 1;
	UniqueLogins unique_logins;
	for(int i = 1; i < 1+count; i++)
	{

		QString str = QString::number(i);
		QString login = "user" + str;
		QString folder = Utility::correct_path("//10.171.0.73/files/user" + str);
		QString name = str + str + str;

		if (!login.isEmpty() && unique_logins.add(login))
		{
			if (User* user = m_server.users().find_user(login))
			{
			   m_server.database_users().update_user(login, name, folder, timestamp, dep_id);
			}
			else
			{
				if(dep_id > count_deps)
				{
					dep_id = 1;
				}

				Department* department = m_server.users().find_department(dep_id);
				QString department_name = "TestDep" + QString::number(dep_id);
				if (!department)
				{
					DepartmentId dep_id = m_server.database_users().add_department(department_name);
					department = create_department(dep_id, department_name);
				}
				UserId id = m_server.database_users().add_user(login, name, folder, timestamp, dep_id);
				user = m_server.users().create_user(id, login);
				m_server.users().update_user(user, name, folder, timestamp, department, false);
				dep_id++;
				logs("Create user:" + QString::number(user->id));
			}
		}
		else logs("wrong login: " + login);
	}
}
void DatabaseTestDDOS::create_chats(unsigned int num_of_targets)
{
	UserId user_id;
	std::map<UserId, User*>::iterator it;
	std::map<UserId, User*> base_users_id = m_server.users().base_users().id_users();
	for(it = base_users_id.begin(); it != base_users_id.end(); it++)
	{
		for(UserId target = 1; target < num_of_targets; target++)
		{
			user_id = get_rand(base_users_id.begin()->first, base_users_id.end()->first);
			if (const User* target_contact = m_server.users().base_users().find_user(user_id))
			{
				UCreateContactChat create_contact_chat(new CreateContactChat(m_server.users().base_users().find_user(it->first), Utility::current_timestamp(), target_contact));
				connect(create_contact_chat.get(), &CreateContactChat::chat_created, this, [this, target_contact](Chat* chat) { slot_contact_chat_created(chat, target_contact); });
				m_server.chats().create_contact_chat(create_contact_chat);
			}
		}
	}
}
void DatabaseTestDDOS::slot_contact_chat_created(Chat* chat, const User* target_contact)
{
	Q_UNUSED(target_contact);
	create_message_to_chat(chat);
	std::map<UserId, ChatUser*> users = chat->users();
	std::map<UserId, ChatUser*>::iterator it;;
	for(it = users.begin(); it != users.end(); it++)
	{
		m_server.database_async().post_query(new AddChatToChatListQuery(it->first, chat->id(), false, QDateTime::currentDateTimeUtc()));
	}
}
void DatabaseTestDDOS::create_message_to_chat(Chat* chat)
{
	QStringList text_list = parse_test_text_file("test-text.txt");
	std::vector<UserId> users;
	std::transform(chat->users().begin(),chat->users().end(), std::back_inserter(users), [](const std::pair<UserId, ChatUser*>& item)
	{return item.first; });
	if(users.size() > 1)
	{
		for(int msg = 0; msg < get_rand(0, messages_max_count); msg++)
		{
			UserId user_id = users.at(get_rand(0,1));
			DescChatMessage message;
			message.id = 0;
			message.local_id = QUuid::createUuid();
			message.chat_id = chat->id();
			message.sender_id = chat->find_another_user(user_id)->user->id;
			message.stamp = m_server.chat_message_stamp_generator().next_stamp();
			message.type = ChatMessageType::Text;
			message.status = ChatMessageStatus::Sended;
			message.history_status = HistoryStatus::Created;
			message.datetime = QDateTime::currentDateTimeUtc();
			message.datetime_readed = QDateTime();
			message.historical = false;
			message.text = text_list.at(msg);
			DBQueryAddChatMessage* query = new DBQueryAddChatMessage(chat, message);
			connect(query, &DBQueryAddChatMessage::storred, this, [this, chat](bool success, const DescChatMessage& chat_message) { slot_text_message_database_storred(success, chat, chat_message); });
			m_server.database_async().post_query(query);
		}
	}
	logs("Create chat: " + QString::number(chat->id()) + " done");
}

void DatabaseTestDDOS::slot_text_message_database_storred(bool success, Chat* chat, const DescChatMessage& chat_message)
{
	if(!success)
	{
		logs("database: error to add chat message");
		return;
	}
	chat->set_last_message_id(chat_message.id);
	chat->set_last_message_stamp(chat_message.stamp);
	// отправить сообщение
	m_server.send_chat_messages().send_message(chat_message.sender_id, chat_message);
}
QStringList DatabaseTestDDOS::parse_test_text_file(const QString& file_name)
{
	QFile file_txt(file_name);
	QString text_string;
	QStringList text_list;
	if(file_txt.open(QIODevice::ReadOnly))
	{
		text_string = file_txt.readAll();
		text_list = text_string.split(".");
	}
return text_list;
}
unsigned int DatabaseTestDDOS::get_rand(unsigned int min, unsigned int max)
{
	unsigned int ms = static_cast<unsigned>(QDateTime::currentMSecsSinceEpoch());
	std::mt19937 gen(ms);
	std::uniform_int_distribution<> dis(static_cast<int>(min), static_cast<int>(max));
	return static_cast<unsigned int>(dis(gen));
}
*/