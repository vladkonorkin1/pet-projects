//=====================================================================================//
//   Author: open
//   Date:   21.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "contact_folder_desc.h"
#include "base/utility.h"
#include <QDir>

static int s_max_new_file_index = 1000;

ContactFolderDesc::ContactFolderDesc(const QString& contact_path, const QString& folder_name, const QString& sender_name)
: m_contact_path(contact_path)		// contact->full_folder_path()
, m_exist(false)
, m_sender_name(sender_name)
{
	//QDir().mkpath(m_contact_path);

	QDir dir(m_contact_path);
	if (dir.exists())
	{
		logs(QString("file transfer: folder exist=%1").arg(m_contact_path));
	}
	else
	{
		logs(QString("file transfer: folder not exist=%1").arg(m_contact_path));

		bool res_create_for_moving = dir.mkpath(".");
		if (res_create_for_moving)
		{
			logs(QString("file transfer: create folder=%1").arg(m_contact_path));
		}
		else
		{
			logs(QString("file transfer: error to create folder=%1").arg(m_contact_path));
		}
	}
	m_exist = (folder_name.isEmpty() ? true : create_unique_dir(folder_name, m_folder_name));

	if (!m_exist)
	{
		logs(QString("file transfer: error to create folder=%1").arg(m_folder_name));
	}
}
QString ContactFolderDesc::folder_path() const
{
	if (m_folder_name.isEmpty()) return m_contact_path;
	return m_contact_path + '/' + m_folder_name;
}
QString ContactFolderDesc::file_path(const QString& filepath) const
{
	return folder_path() + '/' + filepath;
}
bool ContactFolderDesc::is_exist() const
{
	return m_exist;
}
bool ContactFolderDesc::create_directories(const QString& filepath) const
{
	QStringList l = Utility::split_path(filepath);
	QDir dir(folder_path());
	for (int i = 0; i < l.size() - 1; ++i) {
		const QString& name = l[i];
		if (!dir.exists(name))
			if (!dir.mkdir(name)) return false;
		dir.cd(name);
	}
	return true;
}
bool ContactFolderDesc::create_unique_dir(const QString& name, QString& new_name) const
{
	const QString& path = m_contact_path;

	QDir dir_path(path);
	if (!dir_path.exists())
	{
		logs(QString("file transfer: create_unique_dir() error exist dir=%1").arg(path));
		return false;
	}

	QString name2 = /*m_sender_name + " - " +*/ name;
	new_name = name2;
	int index = 1;
	while (index < s_max_new_file_index)
	{
		if (!dir_path.exists(new_name))
		{
			logs(QString("file transfer: create_unique_dir() error exist folder=%1").arg(new_name));

			bool res = dir_path.mkdir(new_name);
			if (res)
			{
				logs(QString("file transfer: create_unique_dir() folder created=%1").arg(new_name));
				return true;
			}
			else
			{
				logs(QString("file transfer: create_unique_dir() error create folder=%1").arg(new_name));
			}
		}

		new_name = name2 + " (" + QString::number(index++) + ")";
	}
	return false;
}
bool ContactFolderDesc::create_unique_file(const QString& filename, QString& new_filename) const
{
	QString name, ext;

	QString filename2 = /*m_sender_name + " - " +*/ filename;
	file_extension(filename2, name, ext);
	const QString& path = m_contact_path;

	if (!QDir(path).exists())
	{
		logs(QString("file transfer: create_unique_file() error exist dir=%1").arg(path));
		return false;
	}

	new_filename = name + ext;
	int index = 1;
	while (index < s_max_new_file_index)
	{
		// !!! тормоза
		QFileInfo info(path + '/' + new_filename);
		if (!info.exists()) return true;
		new_filename = name + " (" + QString::number(index) + ")" + ext;
		++index;
	}
	return false;
}
void ContactFolderDesc::file_extension(const QString& filename, QString& name, QString& ext) const
{
	int index = filename.lastIndexOf('.');
	if (index > 0) 
	{
		name = filename.left(index);
		ext = filename.right(filename.size() - index);
	}
}
QString ContactFolderDesc::folder_name() const
{
	return m_folder_name;
}
