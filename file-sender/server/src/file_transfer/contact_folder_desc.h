//=====================================================================================//
//   Author: open
//   Date:   21.04.2017
//=====================================================================================//
#pragma once

class ContactFolderDesc
{
private:
	QString m_contact_path;
	QString m_folder_name;
	bool m_exist;
	QString m_sender_name;

public:
	ContactFolderDesc(const QString& contact_path, const QString& folder_name, const QString& sender_name);

	bool create_directories(const QString& filepath) const;
	QString file_path(const QString& filepath) const;
	bool is_exist() const;
	bool create_unique_file(const QString& name, QString& new_name) const;
	QString folder_name() const;

private:
	bool create_unique_dir(const QString& name, QString& new_name) const;
	void file_extension(const QString& filename, QString& name, QString& ext) const;
	QString folder_path() const;
};