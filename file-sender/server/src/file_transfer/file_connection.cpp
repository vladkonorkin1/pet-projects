//=====================================================================================//
//   Author: open
//   Date:   19.10.2017
//=====================================================================================//
#include "base/precomp.h"
#include "file_connection.h"
#include <QHostAddress>

FileConnection::FileConnection(int socket_descriptor)
: m_socket_descriptor(socket_descriptor)
, m_message_connection_id(0)
, m_may_read(false)
, m_readed_size(0)
, m_size(0)
, m_timer(new Base::Timer(1.f))
, m_alive_time(0.f)
{
	m_socket.setSocketDescriptor(socket_descriptor);
	connect(&m_socket, SIGNAL(disconnected()), SLOT(slot_socket_disconnected()), Qt::DirectConnection);
	connect(&m_socket, SIGNAL(readyRead()), SLOT(slot_socket_received_id()), Qt::DirectConnection);

	connect(m_timer.get(), SIGNAL(updated(float)), SLOT(slot_timer_updated(float)));
	logs(QString("file connection createad ip=%1").arg(address()));
}
QString FileConnection::address() const
{
	QString ip = m_socket.peerAddress().toString();
	ip.remove("::ffff:");
	return ip;
}
// слот получения message_connection_id
void FileConnection::slot_socket_received_id()
{
	m_alive_time = 0.f;

	if (m_message_connection_id == 0)
	{
		if (m_socket.bytesAvailable() >= sizeof(ConnectionId))
		{
			quint64 avail_bytes1 = m_socket.bytesAvailable();
			QByteArray buffer = m_socket.read(sizeof(ConnectionId));
			memcpy(reinterpret_cast<char*>(&m_message_connection_id), buffer.data(), buffer.size());
			quint64 avail_bytes2 = m_socket.bytesAvailable();

			logs(QString("file connection ip=%1 message_connection_id=%2 received %3 %4").arg(address()).arg(m_message_connection_id).arg(avail_bytes1).arg(avail_bytes2));
			emit message_connection_id_recieved(m_message_connection_id);
		}
	}

	if (m_message_connection_id != 0)
		read_data();
}
// слот получения данных
/*void FileConnection::slot_socket_received_data()
{
	read_data();
}*/
// слот отключения сокета
void FileConnection::slot_socket_disconnected()
{
	m_socket.close();
	emit disconnected(this);
}
void FileConnection::set_read(const QString& name, quint64 size)
{
	m_name = name;
	m_size = size;
	m_readed_size = 0;
	m_may_read = true;
	read_data();
}
void FileConnection::set_cancel_read(quint64 bytes)
{
	m_size = bytes;
	read_data();
}
void FileConnection::read_data()
{
	if (!m_may_read) return;

	// если в буфере есть остатки с предыдущей передачи
	if (m_buffer.size() > 0)
	{
		qint64 need = m_size - m_readed_size;
		assert( need >= 0 );
		qint64 overhead = need - m_buffer.size();
		if (overhead >= 0)
		{
			emit file_data_received(m_buffer.data(), m_buffer.size());
			m_readed_size += m_buffer.size();
			m_buffer.clear();
		}
		else
		{
			emit file_data_received(m_buffer.data(), need);
			m_readed_size += need;
			m_buffer.add_offset(need);
		}
	}
	
	// вычитываем все из сокета
	if (m_socket.bytesAvailable() > 0)
	{
		QByteArray buffer = m_socket.readAll();
		if (buffer.size() > 0)
		{
			if (m_buffer.size() > 0)
			{
				// во внутреннем буфере есть остатки
				m_buffer.add_data(buffer.data(), buffer.size());
				qint64 need = m_size - m_readed_size;
				assert( need == 0 );
			}
			else
			{
				qint64 need = m_size - m_readed_size;
				qint64 readed = std::min<qint64>(need, buffer.size());
				emit file_data_received(buffer.data(), readed);
				m_readed_size += readed;

				qint64 overhead = buffer.size() - need;
				if (overhead > 0)
					m_buffer.add_data(buffer.data() + readed, overhead);
			}
		}
	}

	if (m_readed_size == m_size)
	{
		m_may_read = false;
		emit file_downloaded();
	}
}
void FileConnection::slot_timer_updated(float dt)
{
	m_alive_time += dt;
	if (m_alive_time > 9.f)
	{
		logs(QString("file connection close alive: ip=%1 msg_con_id=%2").arg(address()).arg(m_message_connection_id));
		m_timer.reset();
		slot_socket_disconnected();
	}
}