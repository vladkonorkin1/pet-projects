//=====================================================================================//
//   Author: open
//   Date:   19.10.2017
//=====================================================================================//
#pragma once
#include <QTcpSocket>
#include <base/timer.h>

class FileConnectionBuffer
{
private:
	std::vector<char> m_buffer;
	size_t m_offset;

public:
	FileConnectionBuffer() : m_offset(0) {}

	size_t size() const { return m_buffer.size() - m_offset; }
	const char* data() const { return &(m_buffer[m_offset]); }
	/*void set_data(const char* data, size_t size)
	{
		m_buffer.resize(size);
		memcpy(m_buffer.data(), data, size);
		m_offset = 0;
	}*/
	void add_data(const char* data, size_t size)
	{
		size_t old_size = m_buffer.size();
		m_buffer.resize(old_size + size);
		memcpy(m_buffer.data() + old_size, data, size);
	}
	void add_offset(size_t offset)
	{
		m_offset += offset;
		assert( m_offset <= m_buffer.size() );
		/*if (m_offset >= m_buffer.size())
		{
			m_buffer.clear();
			m_offset = 0;
		}*/
	}
	void clear()
	{
		m_buffer.clear();
		m_offset = 0;
	}
};


class FileConnection : public QObject
{
	Q_OBJECT
private:
	ConnectionId m_message_connection_id;
	int m_socket_descriptor;
	QTcpSocket m_socket;
	
	quint64 m_size;			// размер скачиваемого файла
	quint64 m_readed_size;	// прочитано байт
	bool m_may_read;		// можно читать?

	QString m_name;
	FileConnectionBuffer m_buffer;

	std::unique_ptr<Base::Timer> m_timer;
	float m_alive_time;

public:
	FileConnection(int socket_descriptor);
	void set_read(const QString& name, quint64 size);
	void set_cancel_read(quint64 bytes);
	int id() const;
	QString address() const;

signals:
	void disconnected(FileConnection* connection);
	void message_connection_id_recieved(ConnectionId message_connection_id);
	void file_data_received(const char* data, quint64 size);
	void file_downloaded();

private slots:
	// слот получения message_connection_id
	void slot_socket_received_id();
	// слот получения данных
	//void slot_socket_received_data();
	// слот отключения сокета
	void slot_socket_disconnected();
	// таймер
	void slot_timer_updated(float dt);

private:
	void read_data();
	friend class FileServer;
};

inline int FileConnection::id() const {
	return m_socket_descriptor;
}