//=====================================================================================//
//   Author: open
//   Date:   04.07.2019
//=====================================================================================//
#include "base/precomp.h"
#include "file_connection_server.h"
#include "file_thread_connection.h"

FileConnectionServer::FileConnectionServer(DatabaseAsync& database_async, FileMessageManager& file_message_manager)
: QTcpServer()
, m_database_async(database_async)
, m_file_message_manager(file_message_manager)
, m_id_generator(0)
{
	listen(QHostAddress::Any, FileServerPort);
}
void FileConnectionServer::incomingConnection(qintptr socket_descriptor)
{
	FileThreadConnection* thread = new FileThreadConnection(++m_id_generator, socket_descriptor, m_database_async, m_file_message_manager);
	connect(thread, &FileThreadConnection::message_connection_id_received, this, &FileConnectionServer::slot_thread_message_connection_id_received);
	connect(thread, &QThread::finished, this, [=](){ thread_finished(thread); }, Qt::ConnectionType::QueuedConnection);
	thread->start();
	m_threads[thread->id()] = thread;
}
void FileConnectionServer::thread_finished(FileThreadConnection* thread)
{
	assert( this->thread() == QThread::currentThread() );
	auto it = m_threads.find(thread->id());
	if (it != m_threads.end())
	{
		emit thread_destroyed(thread);
		m_threads.erase(it);
		delete thread;
	}
}
void FileConnectionServer::slot_thread_message_connection_id_received(FileThreadConnection* thread)
{
	assert( this->thread() == QThread::currentThread() );
	emit thread_created(thread);
}