//=====================================================================================//
//   Author: open
//   Date:   04.07.2019
//=====================================================================================//
#include <QTcpServer>

class FileThreadConnection;
class FileMessageManager;
class DatabaseAsync;

class FileConnectionServer : public QTcpServer
{
	Q_OBJECT
private:
	using threads_t = std::map<ConnectionId, FileThreadConnection*>;
	threads_t m_threads;
	DatabaseAsync& m_database_async;
	FileMessageManager& m_file_message_manager;
	ConnectionId m_id_generator;

public:
	FileConnectionServer(DatabaseAsync& database_async, FileMessageManager& file_message_manager);

signals:
	void thread_created(FileThreadConnection* thread);
	void thread_destroyed(FileThreadConnection* thread);

private slots:
	void slot_thread_message_connection_id_received(FileThreadConnection* thread);

protected:
	// входящее подключение
	void incomingConnection(qintptr socket_descriptor) override;

private:
	void thread_finished(FileThreadConnection* thread);
};