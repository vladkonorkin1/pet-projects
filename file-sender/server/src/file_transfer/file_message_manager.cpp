//=====================================================================================//
//   Author: open
//   Date:   29.07.2019
//=====================================================================================//
#include "base/precomp.h"
#include "file_message_manager.h"
#include "database/async/queries/database_async_chat_messages.h"
#include "database/async/database_async.h"
#include "chat_message_stamp_generator.h"
#include "file_thread_connection.h"
#include "network/net_messages.h"
#include "send_chat_messages.h"
#include "session_manager.h"
#include "connection.h"
#include "chats.h"
#include "user.h"

FileMessageManager::FileMessageManager(DatabaseAsync& database_async, SessionManager& session_manager, ChatMessageStampGenerator& chat_message_stamp_generator, Chats& chats, SendChatMessages& send_chat_messages)
: m_database_async(database_async)
, m_session_manager(session_manager)
, m_chat_message_stamp_generator(chat_message_stamp_generator)
, m_chats(chats)
, m_send_chat_messages(send_chat_messages)
{
}
// регистрация файлового сообщения
void FileMessageManager::register_file_message(Connection* connection, ChatMessageId message_id, UserId receiver_id, bool need_update)
{
	ConnectionFileMessages& cfm = find_or_create_connection(connection);

	auto it = m_file_messages.find(message_id);
	if (it == m_file_messages.end())
	{
		cfm.file_message_ids.insert(message_id);
		//m_file_messages[message_id] = FileMessageData{ cfm, receiver_id };
		m_file_messages.insert(std::make_pair(message_id, FileMessageData{ cfm, receiver_id }));
		// обновляем статус передачи файлового сообщения
		if (need_update)
			update_file_message_status(message_id, cfm.connection->user()->id, FileTransferResult::Default);
	}
}
// установить статус передачи файлового сообщения
void FileMessageManager::set_file_message_status(ChatMessageId message_id, FileTransferResult result, const QString& dest_name)
{
	auto it = m_file_messages.find(message_id);
	if (m_file_messages.end() != it)
	{
		FileMessageData& data = it->second;
		UserId sender_id = data.cfm.connection->user()->id;
		UserId receiver_id = data.receiver_id;
		// обновляем статус
		update_file_message_status(message_id, sender_id, result, receiver_id, dest_name);

		// удаляем в соединении
		data.cfm.file_message_ids.erase(message_id);

		// удаляем данные по сообщениям
		m_file_messages.erase(it);
	}
}
// событие завершения передачи и обновления статуса
//void FileMessageManager::set_file_transfer_status(ChatMessageId message_id, UserId sender_id, UserId receiver_id, FileTransferResult result, const QString& dest_name)
//{
//	// вызов из другого потока!!! поэтому перенаправляем в главный поток!!!!!!!!!!!!!!!!!!!!
//	emit emit_set_file_transfer_status(message_id, sender_id, receiver_id, result, dest_name);
//}
// событие завершения передачи и обновления статуса
//void FileMessageManager::slot_set_file_transfer_status(ChatMessageId message_id, UserId sender_id, UserId receiver_id, FileTransferResult result, const QString& dest_name)
//{
//	auto it = m_file_messages.find(message_id);
//	if (m_file_messages.end() != it)
//	{
//		// обновляем статус
//		update_file_message_status(message_id, sender_id, result, receiver_id, dest_name);
//		// удаляем данные по сообщениям
//		m_file_messages.erase(it);
//	}
//}
// найти или создать соединения для хранения файловых сообщений
FileMessageManager::ConnectionFileMessages& FileMessageManager::find_or_create_connection(Connection* connection)
{
	ConnectionId connection_id = connection->id();
	auto it = m_connections.find(connection_id);
	if (it == m_connections.end())
		it = m_connections.insert(std::make_pair(connection_id, ConnectionFileMessages{ connection })).first;
	return it->second;
}
// обработать разрушение соединения
void FileMessageManager::process_connection_destroy(Connection* connection)
{
	ConnectionId connection_id = connection->id();
	auto it = m_connections.find(connection_id);
	if (it != m_connections.end())
	{
		ConnectionFileMessages& cfm = it->second;
		for (ChatMessageId message_id : cfm.file_message_ids)
		{
			update_file_message_status(message_id, connection->user()->id, FileTransferResult::ErrorTransfer);
			m_file_messages.erase(message_id);
		}
		m_connections.erase(it);
	}
}
// обновляем статус передачи файлого сообщения
void FileMessageManager::update_file_message_status(ChatMessageId message_id, UserId sender_id, FileTransferResult result, UserId receiver_id, const QString& dest_name)
{
	ChatMessageStamp stamp = m_chat_message_stamp_generator.next_stamp();

	// в случае успеха добавляем запись сообщения на получателя
	if (FileTransferResult::Success != result)
		receiver_id = 0;

	DBQueryFileMessageSetStatus* query = new DBQueryFileMessageSetStatus(message_id, sender_id, stamp, result, receiver_id, dest_name);
	connect(query, SIGNAL(finished(bool, const std::map<UserId, DescChatMessage>&)), SLOT(slot_file_message_status_updated(bool, const std::map<UserId, DescChatMessage>&)));
	m_database_async.post_query(query);
}
// событие записи в бд статуса передачи файла
void FileMessageManager::slot_file_message_status_updated(bool success, const std::map<UserId, DescChatMessage>& message_receivers)
{
	if (success)
	{
		for (auto pair : message_receivers)
		{
			UserId receiver_id = pair.first;
			DescChatMessage& chat_message = pair.second;
			m_send_chat_messages.send_message(receiver_id, chat_message);
		}
	}
}
// отмена передачи файлового сообщения
void FileMessageManager::cancel_file_message(ChatMessageId message_id)
{
	auto it = m_file_messages.find(message_id);
	if (m_file_messages.end() != it)
	{
		FileMessageData& data = it->second;
		update_file_message_status(message_id, data.cfm.connection->user()->id, FileTransferResult::Cancel);

		// удаляем в соединении
		data.cfm.file_message_ids.erase(message_id);

		// удаляем данные по сообщениям
		m_file_messages.erase(it);
	}
}

// отмена передачи файлового сообщения
//void FileTransferManager::cancel_file_message(ChatMessageId message_id, FileTransferResult cancel_reason, unsigned int file_index, quint64 bytes)
//{
	//auto it = m_file_messages.find(message_id);
	//if (m_file_messages.end() != it)
	//{
	//	ConnectionFileMessages* cfm = it->second;
	//	m_file_messages.erase(it);
	//	cfm->file_message_ids.erase(message_id);
	//	update_file_message_status(message_id, cancel_reason);
	//
	//	FileThreadConnection* file_thread_connection = cfm->connection->file_thread_connection();
	//	if (file_thread_connection)
	//	{
	//		if (file_thread_connection->current_transfer_file_message_id() == message_id)
	//		{
	//			file_thread_connection->current_cancel(file_index, bytes, false);
	//		}
	//	}
	//}
//}

/*// событие обновления прогресса передачи
void FileTransferManager::set_file_transfer_progress(ChatId chat_id, ChatMessageId message_id, UserId sender_id, unsigned char percent)
{
	//if (Chat* chat = m_chats.find_chat(chat_id))
	//{
	//	// отправляем прогресс только отправителю сообщения
	//	//if (Session* session = m_session_manager.get_session(sender_id))
	//	//	session->send_message(ServerMessageFileMessageProgress(message_id, percent));
	//}
}*/
// событие обновления прогресса передачи
//void FileMessageManager::set_file_transfer_progress(ChatMessageId message_id, unsigned char percent)
//{
//
//}