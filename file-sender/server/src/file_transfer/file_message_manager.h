//=====================================================================================//
//   Author: open
//   Date:   29.07.2019
//=====================================================================================//
#pragma once
//#include "file_transfer/file_transfer_desc.h"

class ChatMessageStampGenerator;
class SendChatMessages;
class SessionManager;
class DatabaseAsync;
class Connection;
class Chats;
class Chat;


class FileMessageManager : public QObject
{
	Q_OBJECT
private:
	DatabaseAsync& m_database_async;
	SessionManager& m_session_manager;
	ChatMessageStampGenerator& m_chat_message_stamp_generator;
	Chats& m_chats;
	SendChatMessages& m_send_chat_messages;

	struct ConnectionFileMessages
	{
		Connection* connection;
		std::set<ChatMessageId> file_message_ids;
	};	
	std::map<ConnectionId, ConnectionFileMessages> m_connections;

	struct FileMessageData
	{
		//Connection* connection;
		ConnectionFileMessages& cfm;
		UserId receiver_id;
	};
	std::map<ChatMessageId, FileMessageData> m_file_messages;

public:
	FileMessageManager(DatabaseAsync& database_async, SessionManager& session_manager, ChatMessageStampGenerator& chat_message_stamp_generator, Chats& chats, SendChatMessages& send_chat_messages);
	// регистрация файлового сообщения
	void register_file_message(Connection* connection, ChatMessageId message_id, UserId receiver_id, bool need_update);
	// отмена передачи файлового сообщения
	void cancel_file_message(ChatMessageId message_id);
	// установить статус передачи файлового сообщения
	void set_file_message_status(ChatMessageId message_id, FileTransferResult result, const QString& dest_name);
	// обработать разрушение соединения
	void process_connection_destroy(Connection* connection);

//public slots:
	// событие обновления прогресса передачи
	//void set_file_transfer_progress(ChatMessageId message_id, unsigned char percent);
	// событие завершения передачи и обновления статуса (вызов из другого потока)
	//void set_file_transfer_status(ChatMessageId message_id, UserId sender_id, UserId receiver_id, FileTransferResult result, const QString& dest_name);

//signals:
	//void emit_set_file_transfer_status(quint64 message_id, unsigned int sender_id, unsigned int receiver_id, FileTransferResult result, const QString& dest_name);

private slots:
	// событие завершения передачи и обновления статуса
	//void slot_set_file_transfer_status(quint64 message_id, unsigned int sender_id, unsigned int receiver_id, FileTransferResult result, const QString& dest_name);
	// событие записи в бд статуса передачи файла
	void slot_file_message_status_updated(bool success, const std::map<UserId, DescChatMessage>& message_receivers);

private:
	// найти или создать соединения для хранения файловых сообщений
	ConnectionFileMessages& find_or_create_connection(Connection* connection);
	// обновляем статус передачи файлого сообщения
	void update_file_message_status(ChatMessageId message_id, UserId sender_id, FileTransferResult result, UserId receiver_id = 0, const QString& dest_name = QString());
};