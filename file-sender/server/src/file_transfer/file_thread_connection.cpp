//=====================================================================================//
//   Author: open
//   Date:   20.10.2017
//=====================================================================================//
#include "base/precomp.h"
#include "file_thread_connection.h"
#include "file_transfer_current.h"
#include "file_message_manager.h"
#include "file_connection.h"

Q_DECLARE_METATYPE(FileTransferDesc);

FileThreadConnection::FileThreadConnection(ConnectionId id, int socket_descriptor, DatabaseAsync& database_async, FileMessageManager& file_message_manager)
: QThread()
, m_id(id)
, m_message_connection_id(0)
, m_socket_descriptor(socket_descriptor)
, m_database_async(database_async)
, m_file_message_manager(file_message_manager)
{
	qRegisterMetaType<FileTransferDesc>();
	setObjectName(QString("File Connection Thread %1").arg(id));
}
FileThreadConnection::~FileThreadConnection()
{
}
void FileThreadConnection::run()
{
	m_file_connection.reset(new FileConnection(m_socket_descriptor));
	connect(m_file_connection.get(), &FileConnection::disconnected, this, &FileThreadConnection::slot_file_connection_disconnected, Qt::ConnectionType::DirectConnection);
	connect(m_file_connection.get(), &FileConnection::message_connection_id_recieved, this, &FileThreadConnection::slot_message_connection_id_received, Qt::ConnectionType::DirectConnection);
	exec();
}
void FileThreadConnection::slot_message_connection_id_received(ConnectionId message_connection_id)
{
	m_message_connection_id = message_connection_id;

	m_file_transfer_current.reset(new FileTransferCurrent(m_file_connection.get()));
	connect(m_file_transfer_current.get(), SIGNAL(file_write_error()), SIGNAL(file_write_error()));
	//connect(m_file_transfer_current.get(), SIGNAL(file_transfer_finish(ChatMessageId, FileTransferResult, const QString&)), SLOT(slot_file_transfer_finish(ChatMessageId, FileTransferResult, const QString&)));
	//connect(m_file_transfer_current.get(), SIGNAL(file_transfer_progress(ChatMessageId, ChatId, unsigned char)), SLOT(slot_file_transfer_progress(ChatMessageId, ChatId, unsigned char)));
	connect(m_file_transfer_current.get(), &FileTransferCurrent::file_transfer_finish, this, &FileThreadConnection::slot_file_transfer_finish);
	connect(m_file_transfer_current.get(), &FileTransferCurrent::file_transfer_progress, this, &FileThreadConnection::slot_file_transfer_progress);
	//connect(m_file_transfer_current.get(), SIGNAL(file_transfer_finish(ChatMessageId, UserId, UserId, FileTransferResult, const QString&)), &m_file_message_manager, SLOT(set_file_transfer_status(ChatMessageId, UserId, UserId, FileTransferResult, const QString&)));
	//connect(m_file_transfer_current.get(), SIGNAL(file_transfer_progress(ChatMessageId, unsigned char)), &m_file_message_manager, SLOT(set_file_transfer_progress(ChatMessageId, unsigned char)));
	connect(this, &FileThreadConnection::emit_start_file_transfer, m_file_transfer_current.get(), &FileTransferCurrent::start_file_transfer, Qt::ConnectionType::QueuedConnection);
	connect(this, &FileThreadConnection::emit_current_read_file, m_file_transfer_current.get(), &FileTransferCurrent::current_read_file, Qt::ConnectionType::QueuedConnection);
	connect(this, &FileThreadConnection::emit_current_cancel, m_file_transfer_current.get(), &FileTransferCurrent::current_cancel, Qt::ConnectionType::QueuedConnection);
	connect(this, &FileThreadConnection::emit_current_prepare_finish, m_file_transfer_current.get(), &FileTransferCurrent::current_prepare_finish, Qt::ConnectionType::QueuedConnection);
	emit message_connection_id_received(this);
}
// начать передачу файлов
void FileThreadConnection::start_file_transfer(const FileTransferDesc& file_transfer_desc)
{
	emit emit_start_file_transfer(file_transfer_desc);
}
// прочитать файл
void FileThreadConnection::current_read_file(unsigned int file_id, quint64 size)
{
	emit emit_current_read_file(file_id, size);
}
// отменить передачу
void FileThreadConnection::current_cancel(unsigned int file_id, quint64 bytes, bool user_cancel)
{
	emit emit_current_cancel(file_id, bytes, user_cancel);
}
// подготовка к окончанию передачи
void FileThreadConnection::current_prepare_finish()
{
	emit emit_current_prepare_finish();
}
void FileThreadConnection::slot_file_connection_disconnected()
{
	quit();
}
void FileThreadConnection::slot_file_transfer_progress(ChatMessageId message_id, ChatId chat_id, unsigned char percent)
{
	emit file_transfer_progress(message_id, chat_id, percent);
}
void FileThreadConnection::slot_file_transfer_finish(ChatMessageId message_id, FileTransferResult result, const QString& dest_name)
{
	emit file_transfer_finish(message_id, result, dest_name);
}