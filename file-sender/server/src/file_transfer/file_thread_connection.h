//=====================================================================================//
//   Author: open
//   Date:   20.10.2017
//=====================================================================================//
#pragma once
#include "file_transfer/file_transfer_desc.h"
#include "file_transfer/file_transfer.h"
#include <QTcpSocket>
#include <QThread>

class FileTransferCurrent;
class FileMessageManager;
class FileConnection;
class DatabaseAsync;
class Chat;

struct FileMessageDesc
{
	ChatMessageId message_id;
	ChatMessageLocalId message_local_id;
	ChatId chat_id;
	UserId sender_id;
	UserId receiver_id;
	QDateTime datetime;
	QString local_path;
	quint64 size;
	bool is_dir;
};

class FileThreadConnection : public QThread
{
	Q_OBJECT
private:
	ConnectionId m_id;
	ConnectionId m_message_connection_id;
	int m_socket_descriptor;
	DatabaseAsync& m_database_async;
	FileMessageManager& m_file_message_manager;
	std::unique_ptr<FileConnection> m_file_connection;				// соединение для передачи файлов
	std::unique_ptr<FileTransferCurrent> m_file_transfer_current;

public:
	FileThreadConnection(ConnectionId id, int socket_descriptor, DatabaseAsync& database_async, FileMessageManager& file_message_manager);
	virtual ~FileThreadConnection();
	// id файлового соединения
	ConnectionId id() const;
	// к какому соединению относится
	ConnectionId message_connection_id() const;
	// начать передачу файлов
	void start_file_transfer(const FileTransferDesc& file_transfer_desc);
	// прочитать файл
	void current_read_file(unsigned int file_id, quint64 size);
	// отменить передачу
	void current_cancel(unsigned int file_id, quint64 bytes, bool user_cancel);
	// подготовка к окончанию передачи
	void current_prepare_finish();

signals:
	void message_connection_id_received(FileThreadConnection* thread);
	void file_write_error();
	void file_transfer_finish(quint64 message_id, FileTransferResult result, const QString& dest_name);
	void file_transfer_progress(quint64 message_id, quint64 chat_id, unsigned char percent);

// internal
	void emit_add_file_transfer(Chat* chat, bool dir, const QString& path, quint64 size, const QDateTime& datetime);
	void emit_add_file_transfer(const FileTransferDesc& desc);
	void emit_start_file_transfer(const FileTransferDesc& desc);
	void emit_current_add_file(const QString& name, quint64 size);
	void emit_current_read_file(unsigned int file_id, quint64 size);
	void emit_current_cancel(unsigned int file_id, quint64 bytes, bool user_cancel);
	void emit_current_prepare_finish();

private slots:
	void slot_file_connection_disconnected();
	void slot_message_connection_id_received(ConnectionId message_connection_id);
	void slot_file_transfer_progress(quint64 message_id, quint64 chat_id, unsigned char percent);
	void slot_file_transfer_finish(quint64 message_id, FileTransferResult result, const QString& dest_name);
	
protected:
	void run() override;
};

inline ConnectionId FileThreadConnection::id() const {
	return m_id;
}
// к какому соединению относится
inline ConnectionId FileThreadConnection::message_connection_id() const {
	return m_message_connection_id;
}