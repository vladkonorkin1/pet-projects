//=====================================================================================//
//   Author: open
//   Date:   17.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "file_transfer.h"
#include "file_connection.h"

FileTransfer::FileTransfer(FileConnection* file_connection, ChatMessageId message_id, ChatId chat_id, bool is_dir, quint64 files_size, const QString& contact_dest_path, const QString& folder_name, const QString& sender_name)
: m_file_connection(file_connection)
, m_message_id(message_id)
, m_chat_id(chat_id)
, m_dest_folder_desc(contact_dest_path, is_dir ? folder_name : QString(), sender_name)
, m_is_dir(is_dir)
, m_files_size(files_size)
, m_visible_name(is_dir ? m_dest_folder_desc.folder_name() : QString())
, m_downloaded(false)
, m_finish(false)
, m_id_index(0)
, m_cancel(false)
, m_wrong(false)
, m_cancel_user(false)
, m_cancel_file_index(0)
, m_cancel_size(0)
, m_cancelled(false)
, m_progress_updater(m_files_size)
{
	connect(m_file_connection, SIGNAL(file_downloaded()), SLOT(slot_file_downloaded()), Qt::QueuedConnection);
	connect(m_file_connection, SIGNAL(file_data_received(const char*, quint64)), SLOT(slot_file_data_received(const char*, quint64)));
	connect(&m_progress_updater, SIGNAL(update_progress(unsigned char)), SLOT(slot_update_progress(unsigned char)));
}
FileTransfer::~FileTransfer()
{
}
void FileTransfer::add_file(const QString& name)
{
	m_files.emplace_back(FileDesc{ name, 0 });
}
void FileTransfer::read_file(unsigned int file_id, quint64 size)
{
	m_files[file_id].size = size;
	m_ids.push_back(file_id);
	next();
}
void FileTransfer::next()
{
	if (m_downloaded) return;

	if (m_id_index >= m_ids.size())
	{
		internal_finish();
		return;
	}
	
	m_downloaded = true;
	const FileDesc& file_desc = m_files[m_ids[m_id_index]];

	QString new_file_name = file_desc.name;
	if (m_is_dir)
	{
		bool res = m_dest_folder_desc.create_directories(file_desc.name);
		if (!res)
		{
			logs(QString("file transfer: error to create internal folder=%1").arg(file_desc.name));
		}
	}
	else
	{
		bool res = m_dest_folder_desc.create_unique_file(file_desc.name, new_file_name);
		if (!res)
		{
			logs(QString("file transfer: error to create unique file=%1").arg(file_desc.name));
		}
		m_visible_name = new_file_name;
	}

	assert( !m_file.isOpen() );
	m_file.setFileName(m_dest_folder_desc.file_path(new_file_name));
	m_file.open(QIODevice::WriteOnly);
	
	m_file_connection->set_read(file_desc.name, file_desc.size);

	// проверка на отмену передачи
	check_cancel();
}
void FileTransfer::slot_file_data_received(const char* data, quint64 size)
{
	if (m_file.isOpen())
	{
		m_file.write(data, size);
	}
	else
	{
		if (!m_cancel)
		{
			if (!m_wrong)
			{
				logs(QString("file transfer: error write file=%1").arg(m_file.fileName()));

				m_wrong = true;
				emit write_error();
			}
		}
	}
	// update progress
	m_progress_updater.append_current_size(size);
}
void FileTransfer::slot_file_downloaded()
{
	if (m_file.isOpen()) m_file.close();
	m_downloaded = false;
	++m_id_index;
	next();
}
void FileTransfer::cancel(unsigned int file_id, quint64 bytes, bool cancel_user)
{
	if (m_cancel) return;

	m_cancel = true;
	m_cancel_user = cancel_user;
	m_cancel_file_index = file_id;
	m_cancel_size = bytes;

	// если сейчас идёт передача, то проверяем отмену
	if (m_downloaded)
		check_cancel();
}
// проверка на отмену
void FileTransfer::check_cancel()
{
	if (!m_cancelled)
	{
		if (m_cancel)
		{
			if (m_id_index < m_ids.size())
			{
				if (m_cancel_file_index == m_ids[m_id_index])
				{
					m_file_connection->set_cancel_read(m_cancel_size);
					m_cancelled = true;
					return;
				}
			}
		}
	}
}
void FileTransfer::prepare_finish()
{
	m_finish = true;
	next();
}
void FileTransfer::internal_finish()
{
	if (m_finish)
		emit received(m_message_id, get_transfer_result(), m_visible_name);
}
FileTransferResult FileTransfer::get_transfer_result() const
{
	if (!m_finish) return FileTransferResult::ErrorTransfer;
	if (m_cancel_user) return FileTransferResult::Cancel;
	if (m_wrong) return FileTransferResult::ErrorWrite;
	if (m_ids.size() != m_files.size()) return FileTransferResult::ErrorRead;
	return FileTransferResult::Success;
}
void FileTransfer::slot_update_progress(unsigned char percent)
{
	emit progress_updated(m_message_id, m_chat_id, percent);
}