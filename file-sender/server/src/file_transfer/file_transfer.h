//=====================================================================================//
//   Author: open
//   Date:   17.04.2017
//=====================================================================================//
#pragma once
#include "file_transfer/contact_folder_desc.h"
#include "file_transfer/file_transfer_desc.h"
#include "progress_updater.h"
#include <QFile>

class FileConnection;

class FileTransfer : public QObject
{
	Q_OBJECT	
private:
	FileConnection* m_file_connection;
	
	ChatMessageId m_message_id;
	ChatId m_chat_id;
	ContactFolderDesc m_dest_folder_desc;
	bool m_is_dir;
	struct FileDesc
	{
		QString name;
		quint64 size;
	};
	std::vector<FileDesc> m_files;
	std::vector<unsigned int> m_ids;

	quint64 m_files_size;
	QFile m_file;
	QString m_visible_name;
	
	size_t m_id_index;
	bool m_downloaded;
	bool m_finish;
	bool m_wrong;		// сообщение о какой-либо ошибке
	bool m_cancel;
	bool m_cancel_user;
	unsigned int m_cancel_file_index;
	quint64 m_cancel_size;
	bool m_cancelled;

	ProgressUpdater m_progress_updater;

public:
	FileTransfer(FileConnection* file_connection, ChatMessageId message_id, ChatId chat_id, bool is_dir, quint64 files_size, const QString& contact_dest_path, const QString& folder_name, const QString& sender_name);
	virtual ~FileTransfer();
	void add_file(const QString& name);
	void read_file(unsigned int file_id, quint64 size);
	void cancel(unsigned int file_id, quint64 bytes, bool cancel_user);
	void prepare_finish();

signals:
	void received(quint64 message_id, FileTransferResult result, const QString& dest_name);
	void write_error();
	void progress_updated(quint64 message_id, quint64 chat_id, unsigned char percent);

private slots:
	void slot_file_downloaded();
	void slot_file_data_received(const char* data, quint64 size);
	void slot_update_progress(unsigned char percent);

private:
	void next();
	void internal_finish();
	FileTransferResult get_transfer_result() const;
	// проверка на отмену
	void check_cancel();
};