//=====================================================================================//
//   Author: open
//   Date:   05.07.2019
//=====================================================================================//
#include "base/precomp.h"
#include "file_transfer_current.h"
#include "chats.h"

FileTransferCurrent::FileTransferCurrent(FileConnection* file_connection)
: m_file_connection(file_connection)
{
}
void FileTransferCurrent::start_file_transfer(const FileTransferDesc& desc)
{
	m_file_transfer.reset(new FileTransfer(m_file_connection, desc.message_id, desc.chat_id, desc.is_dir, desc.size, desc.dest_path, desc.name, desc.sender_name));
	for (const QString& file_name : desc.files)
		m_file_transfer->add_file(file_name);
	connect(m_file_transfer.get(), SIGNAL(received(quint64, FileTransferResult, const QString&)), SLOT(slot_current_received(quint64, FileTransferResult, const QString&)));
	connect(m_file_transfer.get(), SIGNAL(write_error()), SLOT(slot_current_write_error()));
	connect(m_file_transfer.get(), SIGNAL(progress_updated(quint64, quint64, unsigned char)), SLOT(slot_current_progress_updated(quint64, quint64, unsigned char)));
}
void FileTransferCurrent::current_read_file(unsigned int file_id, quint64 size)
{
	if (m_file_transfer) m_file_transfer->read_file(file_id, size);
}
void FileTransferCurrent::current_cancel(unsigned int file_id, quint64 bytes, bool user_cancel)
{
	if (m_file_transfer) m_file_transfer->cancel(file_id, bytes, user_cancel);
}
void FileTransferCurrent::current_prepare_finish()
{
	if (m_file_transfer) m_file_transfer->prepare_finish();
}
void FileTransferCurrent::slot_current_write_error()
{
	if (m_file_transfer) emit file_write_error();
}
void FileTransferCurrent::slot_current_received(ChatMessageId message_id, FileTransferResult result, const QString& dest_name)
{
	if (m_file_transfer)
	{
		emit file_transfer_finish(message_id, result, dest_name);
		m_file_transfer.reset();
	}
}
void FileTransferCurrent::slot_current_progress_updated(ChatMessageId message_id, ChatId chat_id, unsigned char percent)
{
	if (m_file_transfer)
		emit file_transfer_progress(message_id, chat_id, percent);
}