//=====================================================================================//
//   Author: open
//   Date:   05.07.2019
//=====================================================================================//
#pragma once
#include "file_transfer/file_transfer_desc.h"
#include "file_transfer/file_transfer.h"

class FileConnection;
class DatabaseAsync;

class FileTransferCurrent : public QObject
{
	Q_OBJECT
private:
	FileConnection* m_file_connection;

	using UFileTransfer = std::unique_ptr<FileTransfer>;
	UFileTransfer m_file_transfer;

public:
	FileTransferCurrent(FileConnection* file_connection);

signals:
	void file_transfer_progress(quint64 message_id, quint64 chat_id, unsigned char percent);
	void file_transfer_finish(quint64 message_id, FileTransferResult result, const QString& dest_name);
	void file_write_error();

public Q_SLOTS:
	void start_file_transfer(const FileTransferDesc& desc);
	void current_read_file(unsigned int file_id, quint64 size);
	void current_cancel(unsigned int file_id, quint64 bytes, bool user_cancel);
	void current_prepare_finish();
	
private slots:
	void slot_current_received(quint64 message_id, FileTransferResult result, const QString& dest_name);
	void slot_current_write_error();
	void slot_current_progress_updated(quint64 message_id, quint64 quint64, unsigned char percent);
};