//=====================================================================================//
//   Author: open
//   Date:   26.10.2017
//=====================================================================================//
#pragma once

struct FileTransferDesc
{
	ChatMessageId message_id = 0;
	ChatId chat_id = 0;
	QString sender_name;
	QString dest_path;
	QString name;
	bool is_dir = false;
	quint64 size = 0;
	std::vector<QString> files;
};