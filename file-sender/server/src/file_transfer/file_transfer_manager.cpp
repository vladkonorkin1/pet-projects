﻿//=====================================================================================//
//   Author: open
//   Date:   12.07.2019
//=====================================================================================//
#include "base/precomp.h"
#include "file_transfer_manager.h"
#include "database/async/queries/database_async_chat_messages.h"
#include "database/async/database_async.h"
#include "chat_message_stamp_generator.h"
#include "file_thread_connection.h"
#include "network/net_messages.h"
#include "send_chat_messages.h"
#include "session_manager.h"
#include "connection.h"
#include "chats.h"
#include "user.h"

FileTransferManager::FileTransferManager(DatabaseAsync& database_async, SessionManager& session_manager, ChatMessageStampGenerator& chat_message_stamp_generator, Chats& chats, SendChatMessages& send_chat_messages)
: m_database_async(database_async)
, m_session_manager(session_manager)
, m_chat_message_stamp_generator(chat_message_stamp_generator)
, m_chats(chats)
, m_send_chat_messages(send_chat_messages)
{
	connect(this, &FileTransferManager::emit_set_file_transfer_status, this, &FileTransferManager::slot_set_file_transfer_status);
}
// событие обновления прогресса передачи
void FileTransferManager::set_file_transfer_progress(ChatId chat_id, ChatMessageId message_id, UserId sender_id, unsigned char percent)
{
	if (Chat* chat = m_chats.find_chat(chat_id))
	{
		// отправляем прогресс только отправителю сообщения
		//if (Session* session = m_session_manager.get_session(sender_id))
		//	session->send_message(ServerMessageFileMessageProgress(message_id, percent));
	}

	//ServerMessageFileTransferProgress


	//if (Chat* chat = m_chats.find_chat(chat_id))
	//{
	//	// отправляем прогресс только отправителю сообщения
	//	if (Session* session = m_session_manager.get_session(sender_id))
	//		session->send_message(ServerMessageFileMessageProgress(message_id, percent));
	//
	//
	//	//// всем пользователям чата отправляем прогресс передачи файла
	//	//for (const auto& pair : chat->users())
	//	//{
	//	//	const ChatUser* chat_user = pair.second;
	//	//	if (Session* session = m_session_manager.get_session(chat_user->user->id))
	//	//		session->send_message(ServerMessageFileMessageProgress(message_id, percent));
	//	//}
	//}
}
// событие завершения передачи и обновления статуса
void FileTransferManager::set_file_transfer_status(ChatId chat_id, ChatMessageId message_id, UserId sender_id, FileTransferResult result, const QString& dest_name)
{
	// вызов из другого потока!!! поэтому перенаправляем в главный поток!!!!!!!!!!!!!!!!!!!!
	emit emit_set_file_transfer_status(chat_id, message_id, sender_id, result, dest_name);
}
// событие завершения передачи и обновления статуса
void FileTransferManager::slot_set_file_transfer_status(ChatId chat_id, ChatMessageId message_id, UserId sender_id, FileTransferResult result, const QString& dest_name)
{
	//ChatMessageStamp stamp = m_chat_message_stamp_generator.next_stamp();
	//if (Chat* chat = m_chats.find_chat(chat_id))
	//{
	//	if (const ChatUser* chat_receiver_user = chat->find_another_user(sender_id))
	//	{
	//		DBQueryFileMessageSetStatus* query = new DBQueryFileMessageSetStatus(chat->id(), message_id, sender_id, chat_receiver_user->user->id, stamp, result, dest_name);
	//		connect(query, &DBQueryFileMessageSetStatus::finished, this, [this, chat](bool success, const DescChatMessage& chat_message) { slot_file_message_status_updated(success, chat, chat_message); });
	//		m_database_async.post_query(query);
	//	}
	//}
}
// событие записи в бд статуса передачи файла
void FileTransferManager::slot_file_message_status_updated(bool success, const Chat* chat, const DescChatMessage& chat_message)
{
	if (success)
	{
		m_send_chat_messages.send_message(chat, chat_message);
	}
}
// регистрация файлового сообщения
void FileTransferManager::register_file_message(Connection* connection, ChatMessageId message_id)
{
	ConnectionFileMessages* cfm = find_or_create_connection(connection);
	//m_file_messages[message_id] = cfm;

	//auto it = m_file_messages.find(message_id);
	//if (it == m_file_messages.end())
	//{
	//	m_file_messages.insert();
	//}

	// обновляем статус передачи файлого сообщения
	//update_file_message_status(message_id, FileTransferResult::Default);
}
// найти или создать соединения для хранения файловых сообщений
FileTransferManager::ConnectionFileMessages* FileTransferManager::find_or_create_connection(Connection* connection)
{
	ConnectionId connection_id = connection->id();
	auto it = m_connections.find(connection_id);
	if (it == m_connections.end())
		it = m_connections.insert(std::make_pair(connection_id, ConnectionFileMessages{ connection })).first;
	return &(it->second);
}
// отмена передачи файлового сообщения
void FileTransferManager::cancel_file_message(ChatMessageId message_id, FileTransferResult cancel_reason, unsigned int file_index, quint64 bytes)
{
	//auto it = m_file_messages.find(message_id);
	//if (m_file_messages.end() != it)
	//{
	//	ConnectionFileMessages* cfm = it->second;
	//	m_file_messages.erase(it);
	//	cfm->file_message_ids.erase(message_id);
	//	update_file_message_status(message_id, cancel_reason);
	//
	//	FileThreadConnection* file_thread_connection = cfm->connection->file_thread_connection();
	//	if (file_thread_connection)
	//	{
	//		if (file_thread_connection->current_transfer_file_message_id() == message_id)
	//		{
	//			file_thread_connection->current_cancel(file_index, bytes, false);
	//		}
	//	}
	//}
}
// обработать разрушение соединения
void FileTransferManager::process_connection_destroy(Connection* connection)
{
	ConnectionId connection_id = connection->id();
	auto it = m_connections.find(connection_id);
	if (it != m_connections.end())
	{
		ConnectionFileMessages& cfm = it->second;
		for (ChatMessageId message_id : cfm.file_message_ids)
		{
			update_file_message_status(message_id, FileTransferResult::ErrorTransfer);
			m_file_messages.erase(message_id);
		}
		m_connections.erase(it);
	}
}
// обновляем статус передачи файлого сообщения
void FileTransferManager::update_file_message_status(ChatMessageId message_id, FileTransferResult result)
{
	
}
