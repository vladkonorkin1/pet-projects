//=====================================================================================//
//   Author: open
//   Date:   12.07.2017
//=====================================================================================//
#include "base/precomp.h"
#include "history_messages.h"
#include "database/async/database_async.h"
#include "send_chat_messages.h"
#include "message_connection.h"
#include "chats.h"
#include "user.h"

static int HistoryMessagesLimit = 34;

HistoryMessages::HistoryMessages(DatabaseAsync& database_async, SendChatMessages& send_chat_messages, const User* user, MessageConnection* message_connection)
: m_database_async(database_async)
, m_send_chat_messages(send_chat_messages)
, m_user(user)
, m_message_connection(message_connection)
{
}
// запросить порцию сообщений в порядке убывания id
void HistoryMessages::request_history_messages(ChatId chat_id, ChatMessageId message_id)
{
	SelectHistoryMessagesQuery* query = new SelectHistoryMessagesQuery(chat_id, m_user, message_id, HistoryMessagesLimit);// get_count_historical_messages(chat_id, message_id));
	connect(query, SIGNAL(messages_uploaded(ChatId, const std::vector<DescChatMessage>&, bool)), SLOT(slot_history_messages_uploaded(ChatId, const std::vector<DescChatMessage>&, bool)));
	m_database_async.post_query(query);
}
// событие запроса исторических сообщений
void HistoryMessages::slot_history_messages_uploaded(ChatId chat_id, const std::vector<DescChatMessage>& messages, bool finish_upload)
{
	// отправляем сообщения в сеть
	if (!messages.empty())
	{
		m_send_chat_messages.send_history_messages(m_message_connection, m_user, messages, chat_id, finish_upload);
	}
	emit history_messages_finished();
}
// запросить восстановление сообщений
void HistoryMessages::request_restore_messages(ChatMessageStamp last_chat_message_stamp, ChatMessageId last_readed_chat_message_id)
{
	DBQuerySelectRestoreMessages* query = new DBQuerySelectRestoreMessages(m_user, last_chat_message_stamp, last_readed_chat_message_id);
	connect(query, &DBQuerySelectRestoreMessages::messages_uploaded, this, &HistoryMessages::slot_restore_messages_uploaded);
	m_database_async.post_query(query);
}
// событие восстановления сообщений чата
void HistoryMessages::slot_restore_messages_uploaded(bool success, const std::vector<DescChatMessage>& messages, const std::vector<DBQuerySelectRestoreMessages::LastReadMessage>& chat_last_read_messages)
{
	/// отправляем сообщения в сеть
	if (!messages.empty())
		m_send_chat_messages.send_messages(m_message_connection, m_user, messages);

	// высылаем какие сообщения были прочитаны
	for (const DBQuerySelectRestoreMessages::LastReadMessage& last_read_message : chat_last_read_messages)
		m_message_connection->send_message(ServerMessageChatMessageRead(last_read_message.chat_id, last_read_message.message_id, last_read_message.datetime));
	
	emit restore_messages_finished();
}