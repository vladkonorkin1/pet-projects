//=====================================================================================//
//   Author: open
//   Date:   12.07.2017
//=====================================================================================//
#pragma once
#include "database/async/queries/database_async_chat_messages.h"

class MessageConnection;
class SendChatMessages;
class DatabaseAsync;
class ChatList;
class User;
class Chat;

// per connection
class HistoryMessages : public QObject
{
	Q_OBJECT
private:
	DatabaseAsync& m_database_async;
	SendChatMessages& m_send_chat_messages;
	const User* m_user;
	//ConnectionId m_connection_id;
	MessageConnection* m_message_connection;

public:
	HistoryMessages(DatabaseAsync& database_async, SendChatMessages& send_chat_messages, const User* user, MessageConnection* message_connection);
	// запросить порцию сообщений в порядке убывания id
	void request_history_messages(ChatId chat_id, ChatMessageId message_id);
	// запросить восстановление сообщений
	void request_restore_messages(ChatMessageStamp last_chat_message_stamp, ChatMessageId last_readed_chat_message_id);

signals:
	void history_messages_finished();
	void restore_messages_finished();

private slots:
	// событие запроса исторических сообщений
	void slot_history_messages_uploaded(ChatId chat_id, const std::vector<DescChatMessage>& messages, bool finish_upload);
	// событие восстановления сообщений чата
	void slot_restore_messages_uploaded(bool success, const std::vector<DescChatMessage>& messages, const std::vector<DBQuerySelectRestoreMessages::LastReadMessage>& chat_last_read_messages);
};
