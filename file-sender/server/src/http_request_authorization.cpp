//=====================================================================================//
//   Author: open
//   Date:   12.08.2019
//=====================================================================================//
#include "base/precomp.h"
#include "http_request_authorization.h"
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>

HttpRequestAuthorization::HttpRequestAuthorization(const QString& login, const QByteArray& encrypted_password)
: m_surl("https://fs-auth.sima-land.local:3443/authuser/check")
, m_login(login)
, m_encrypted_password(encrypted_password)
{
}
QString HttpRequestAuthorization::url() const
{
	return m_surl;
}
void HttpRequestAuthorization::process(QNetworkReply* reply, bool success)
{
	//emit authorization_result_received(m_login, AutorizateResult::Success);
	//return;

	if (!success)
	{
		int http_status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
		if (http_status == 452)
		{
			emit authorization_result_received(m_login, AutorizateResult::WrongLogin);
			return;
		}

		emit authorization_result_received(m_login, AutorizateResult::Null);
		return;
	}
	QJsonDocument json_doc = QJsonDocument::fromJson(reply->readAll());
	QJsonObject json_obj = json_doc.object();
	emit authorization_result_received(m_login, json_obj["Success"].toBool() ? AutorizateResult::Success : AutorizateResult::WrongLogin);
}
const QByteArray HttpRequestAuthorization::send_buffer() const
{
	QJsonDocument doc;
	QJsonObject obj;
	obj["username"] = m_login;
	obj["password"] = QString(m_encrypted_password.toBase64());
	doc.setObject(obj);
	return doc.toJson(QJsonDocument::Compact);
}