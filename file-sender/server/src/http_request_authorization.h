//=====================================================================================//
//   Author: open
//   Date:   12.08.2019
//=====================================================================================//
#pragma once
#include "http_request.h"

class HttpRequestAuthorization : public HttpRequest
{
	Q_OBJECT
private:
	QString m_surl;
	QString m_login;
	QByteArray m_encrypted_password;

public:
	HttpRequestAuthorization(const QString& login, const QByteArray& encrypted_password);
	virtual void process(QNetworkReply* reply, bool success);
	virtual QString url() const;
	virtual const QByteArray send_buffer() const;

signals:
	void authorization_result_received(const QString& login, const AutorizateResult& auth_result);
};

using SHttpRequestAuthorization = std::shared_ptr<HttpRequestAuthorization>;