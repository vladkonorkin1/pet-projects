//=====================================================================================//
//   Author: open
//   Date:   12.08.2019
//=====================================================================================//
#include "base/precomp.h"
#include "http_request_departments.h"
#include <QNetworkReply>
#include <QJsonDocument>

HttpRequestDepartments::HttpRequestDepartments(const QDateTime& datetime)
//: m_surl(QString("https://employees.sima-land.ru/api/v1/department?updated_at=%1").arg(datetime.toString("yyyy-MM-dd")))
: m_surl(QString("https://employees.sima-land.ru/api/v1/department?updated_at=%1").arg(datetime.toString(Qt::DateFormat::ISODate))) //toString("yyyy-MM-dd")))
{
}
QString HttpRequestDepartments::url() const
{
	return m_surl;
}
void HttpRequestDepartments::process(QNetworkReply* reply, bool success)
{
	QJsonDocument json_doc;
	if (success)
		json_doc = QJsonDocument::fromJson(reply->readAll());
	emit received(success, json_doc);
}

