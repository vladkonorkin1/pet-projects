//=====================================================================================//
//   Author: open
//   Date:   12.08.2019
//=====================================================================================//
#include "base/precomp.h"
#include "http_request_employers.h"
#include <QNetworkReply>
#include <QJsonDocument>

HttpRequestEmployers::HttpRequestEmployers(const QDateTime& datetime)
: m_surl(QString("https://employees.sima-land.ru/api/v1/employee?updated_at=%1").arg(datetime.toString(Qt::DateFormat::ISODate)))
{
}
QString HttpRequestEmployers::url() const
{
	// https://employees.sima-land.ru/api/v1/employee?updated_at=2019-08-09
	return m_surl;
}
void HttpRequestEmployers::process(QNetworkReply* reply, bool success)
{
	QJsonDocument json_doc;
	if (success)
		json_doc = QJsonDocument::fromJson(reply->readAll());
	emit received(success, json_doc);
}


