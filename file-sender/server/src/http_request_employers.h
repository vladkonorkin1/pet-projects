//=====================================================================================//
//   Author: open
//   Date:   12.08.2019
//=====================================================================================//
#pragma once
#include "http_request.h"

class HttpRequestEmployers : public HttpRequest
{
	Q_OBJECT
private:
	QString m_surl;

public:
	HttpRequestEmployers(const QDateTime& datetime);
	virtual void process(QNetworkReply* reply, bool success);
	virtual QString url() const;
signals:
	void received(bool success, const QJsonDocument& json_doc);
};

using SHttpRequestEmployers = std::shared_ptr<HttpRequestEmployers>;