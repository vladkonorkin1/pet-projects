//=====================================================================================//
//   Author: open
//   Date:   15.03.2017
//=====================================================================================//
#include <QCoreApplication>
#include "server.h"
#include "base/config.h"
#include "qtservice/qtservice.h"
#include <QDir>

#include <QProcess>

Q_DECLARE_METATYPE(ChatId);
Q_DECLARE_METATYPE(FileTransferResult);

void register_metatypes()
{
	qRegisterMetaType<ChatId>("quint64");
	qRegisterMetaType<FileTransferResult>();
}

class WinService : public QtService<QCoreApplication>
{
private:
	std::unique_ptr<Server> m_server;

public:
	WinService(int argc, char **argv)
	: QtService<QCoreApplication>(argc, argv, "file-sender")
	{
		setServiceDescription("File transfer service");
		setServiceFlags(QtServiceBase::CanBeSuspended);

		register_metatypes();
	}

protected:
	void start()
	{
		//QCoreApplication* app = application();
		//QDir::setCurrent("d:\\work\\svn\\sima-land\\projects\\sima-chat\\deploy\\server\\win64\\sima-chat-server-win64");
		QDir::setCurrent(QCoreApplication::applicationDirPath());
		logs("start");

		start_external_process();

		Config config;
		DatabaseParams database_params(
			config.value("service/database-num-connections", "4").toInt(),
			config.value("service/database-host-name", "127.0.0.1"),
			config.value("service/database-name", "sima-chat"),
			config.value("service/database-user-name", "postgres"),
			config.value("service/database-password", "postgres"),
			config.value("service/database-port", "5432").toInt());

		QString employees_service_login = config.value("employees-service/login", "file-transfer");
		QString employees_service_password = config.value("employees-service/password", "d29cy6wDjmQARbcZ");
		QString authorization_service_login = config.value("authorization-service/login", "fs-auth-svc");
		QString authorization_service_password = config.value("authorization-service/password", "oD4qjmgy$fgu");

		QStringList allow_ip_connections = config.value("service/allow-ip-connections", "").split(" ", QString::SplitBehavior::SkipEmptyParts);
		QStringList no_auth_users = config.value("service/no-auth-users", "").split(" ", QString::SplitBehavior::SkipEmptyParts);

		m_server.reset(new Server(
			database_params,
			employees_service_login,
			employees_service_password,
			authorization_service_login,
			authorization_service_password,
			allow_ip_connections,
			no_auth_users
		));
	}
	void stop()
	{
		stop_external_process();
		m_server.reset();
	}
	void pause()
	{
		stop();
	}
	void resume()
	{
		start();
	}

	void start_external_process()
	{
		QProcess process;
		process.setProgram("exist-user-folder.exe");
		process.setWorkingDirectory(QDir::currentPath());
		//process.setStandardOutputFile(QProcess::nullDevice());
		//process.setStandardErrorFile(QProcess::nullDevice());
		qint64 pid;
		process.startDetached(&pid);
	}
	void stop_external_process()
	{
		QProcess process;
		process.setProgram("kill exist-user-folder");
		qint64 pid;
		process.startDetached(&pid);
	}
};

int main(int argc, char *argv[])
{
	WinService service(argc, argv);
	return service.exec();
}