//=====================================================================================//
//   Author: open
//   Date:   16.03.2017
//=====================================================================================//
#include "base/precomp.h"
#include "message_connection.h"
#include <QHostAddress>
#include <QTcpSocket>

MessageConnection::MessageConnection(QTcpSocket* socket, ConnectionId id)
: m_connection(socket)
, m_socket(socket)
, m_id(id)
, m_time_alive(0.f)
, m_address(ip())
{
	connect(&m_connection, SIGNAL(disconnected()), SLOT(slot_disconnect()));
	connect(&m_connection, SIGNAL(data_received(const char*, unsigned int)), SLOT(slot_data_recieved(const char*, unsigned int)));

	m_message_dispatcher.register_message<ClientMessageCheckProtocolVersion>();
	m_message_dispatcher.register_message<ClientMessageAutorizate>();
	m_message_dispatcher.register_message<ClientMessageSync>();
	m_message_dispatcher.register_message<ClientMessageAlive>();

	m_message_dispatcher.register_message<ClientMessageUsersStatus>();

	m_message_dispatcher.register_message<ClientMessageTextMessageSend>();

	m_message_dispatcher.register_message<ClientMessageContactChatCreate>();
	m_message_dispatcher.register_message<ClientMessageChatRemove>();
	m_message_dispatcher.register_message<ClientMessageChatMoveFavorite>();
	m_message_dispatcher.register_message<ClientMessageChatRequestHistoryMessages>();
	m_message_dispatcher.register_message<ClientMessageContactChatRequest>();
	m_message_dispatcher.register_message<ClientMessageGroupChatCreate>();
	m_message_dispatcher.register_message<ClientMessageGroupChatAddUsers>();
	m_message_dispatcher.register_message<ClientMessageGroupChatRemoveUser>();
	m_message_dispatcher.register_message<ClientMessageGroupSetOrRemoveAdmin>();
	m_message_dispatcher.register_message<ClientMessageGroupChatRename>();
	m_message_dispatcher.register_message<ClientMessageChatNewMessageRead>();

	m_message_dispatcher.register_message<ClientMessageFileMessageSend>();
	m_message_dispatcher.register_message<ClientMessageFileMessageRegister>();
	m_message_dispatcher.register_message<ClientMessageFileMessageCancel>();
	m_message_dispatcher.register_message<ClientMessageFileTransferStart>();
	m_message_dispatcher.register_message<ClientMessageFileTransferFileBodyStart>();
	m_message_dispatcher.register_message<ClientMessageFileTransferCancel>();
	m_message_dispatcher.register_message<ClientMessageFileTransferFinish>();

	m_message_dispatcher.register_message<ClientMessageIsAccessClosedFolder>();
}
void MessageConnection::slot_disconnect()
{
	m_socket->deleteLater();
	emit disconnected(this);
}
void MessageConnection::send_message(const NetMessage& message)
{
	m_connection.send_message(message);
}
void MessageConnection::slot_data_recieved(const char* data, unsigned int size)
{
	m_message_dispatcher.process(data, size);
}
void MessageConnection::set_message_listener(ServerHandlerMessages* handler_messages)
{
	m_message_dispatcher.set_observer(handler_messages);
}
void MessageConnection::close()
{
	m_connection.close();
}
bool MessageConnection::check_destroy(float dt, bool is_authorized)
{
//#ifndef _DEBUG
	m_time_alive += dt;

	float stop_time = is_authorized ? 10.f : 3.f;
	if (m_time_alive > stop_time)
	{
		// считаем, что соединение потеряно
		close();
		return true;
	}
//#endif
	return false;
}
void MessageConnection::set_alive()
{
	m_time_alive = 0.f;
}
QString MessageConnection::address() const
{
	/*QString ip = m_socket->peerAddress().toString();
	ip.remove("::ffff:");
	return ip;*/
	return m_address;
}
QString MessageConnection::ip() const
{
	QString ip = m_socket->peerAddress().toString();
	ip.remove("::ffff:");
	return ip;
}