//=====================================================================================//
//   Author: open
//   Date:   16.03.2017
//=====================================================================================//
#pragma once
#include "network/base_client_connection.h"
#include "network/message_dispatcher.h"
#include "network/handler_messages.h"

class MessageConnection : public QObject
{
	Q_OBJECT
private:
	BaseClientConnection m_connection;
	QTcpSocket* m_socket;
	ConnectionId m_id;
	MessageDispatcher<ServerHandlerMessages> m_message_dispatcher;
	float m_time_alive;
	QString m_address;
	
public:
	MessageConnection(QTcpSocket* socket, ConnectionId id);
	ConnectionId id() const;
	void send_message(const NetMessage& message);
	void set_message_listener(ServerHandlerMessages* handler_messages);
	QString address() const;
	void close();
	bool check_destroy(float dt, bool is_authorized);
	void set_alive();

signals:
	void disconnected(MessageConnection* connection);

private slots:
	void slot_disconnect();
	void slot_data_recieved(const char* data, unsigned int size);

private:
	QString ip() const;
};

inline ConnectionId MessageConnection::id() const { return m_id; }