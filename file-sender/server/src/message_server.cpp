//=====================================================================================//
//   Author: open
//   Date:   16.03.2017
//=====================================================================================//
#include "base/precomp.h"
#include "message_server.h"
#include "message_connection.h"

MessageServer::MessageServer()
: m_id_generator(0)
{
	connect(&m_tcp_server, SIGNAL(newConnection()), this, SLOT(slot_accept_connection()));
	m_tcp_server.listen(QHostAddress::Any, MessagesServerPort);
}
void MessageServer::slot_accept_connection()
{
	MessageConnection* connection = new MessageConnection(m_tcp_server.nextPendingConnection(), ++m_id_generator);
	connect(connection, SIGNAL(disconnected(MessageConnection*)), SLOT(slot_close_connection(MessageConnection*)));
	m_connections[connection->id()] = connection;
	emit connection_created(connection);
}
void MessageServer::slot_close_connection(MessageConnection* connection)
{
	auto it = m_connections.find(connection->id());
	if (it != m_connections.end())
	{
		emit connection_destroyed(connection);
		m_connections.erase(it);
		//connection->deleteLater();
		delete connection;
	}
}