//=====================================================================================//
//   Author: open
//   Date:   16.03.2017
//=====================================================================================//
#include <QTcpServer>

class MessageConnection;

class MessageServer : public QObject
{
	Q_OBJECT
private:
	QTcpServer m_tcp_server;
	ConnectionId m_id_generator;
	std::map<ConnectionId, MessageConnection*> m_connections;
	
public:
	MessageServer();

signals:
	void connection_created(MessageConnection* connection);
	void connection_destroyed(MessageConnection* connection);

private slots:
	void slot_accept_connection();
	void slot_close_connection(MessageConnection* connection);
};