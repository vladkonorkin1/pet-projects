#include "base/precomp.h"
#include "online_status_manager.h"
#include "connection.h"
#include "chat_lists.h"
#include "chat_list.h"
#include "users.h"
#include "user.h"
#include "chat.h"

OnlineStatusManager::OnlineStatusManager(SessionManager& session_manager, const Users& users, const ChatLists& chat_lists)
: m_session_manager(session_manager)
, m_users(users)
, m_chat_lists(chat_lists)
{
}
// обработать разрушение подключения
void OnlineStatusManager::process_connection_destroy(Connection* connection)
{
	UserId user_id = connection->user()->id;
	if (Session* session = m_session_manager.get_session(user_id))
	{
		if (session->connections().size() == 1)
			set_online_status(user_id, false);
	}
}
// обработать синхронизацию подключения
void OnlineStatusManager::process_connection_synced(Connection* connection)
{
	UserId user_id = connection->user()->id;
	Session* session = m_session_manager.get_session(user_id);
	// если это первое подключение в сессии, то обновляем статус 
	if (session->connections().size() == 1)
	{
		// оповещаем других о появлении нас в сети
		set_online_status(user_id, true);
	}
	// отправляем статусы других самому себе
	send_users_status_myself(connection);
}
// установить онлайн статус
void OnlineStatusManager::set_online_status(UserId user_id, bool online)
{
	if (User* user = m_users.find_user(user_id))
	{
		user->online = online;

		// оповещаем всех тех, у кого я присутствую в чат-листе
		const std::set<UserId>& updating_users = m_chat_lists.find_contact_chat_where_iam(user_id);
		for (UserId updating_user_id : updating_users)
		{
			if (Session* session = m_session_manager.get_session(updating_user_id))
				session->send_message(ServerMessageContactStatus(user_id, online));
		}
	}
}
// отправить статусы других самому себе
void OnlineStatusManager::send_users_status_myself(Connection* connection)
{
	std::vector<const User*> updating_users;
	for (const auto& it : connection->chat_list()->chats())
	{
		const Chat* chat = it.second->chat;
		if (!chat->is_group())
		{
			if (const ChatUser* opponent_chat_user = chat->find_another_user(connection->user()->id))
				updating_users.push_back(opponent_chat_user->user);
		}
	}
	connection->send_message(ServerMessageUsersStatus(convert_users_to_status(updating_users)));
}
// преобразовать пользователей в структуру онлайн статусов
std::vector<UserStatus> OnlineStatusManager::convert_users_to_status(const std::vector<const User*>& users) const
{
	std::vector<UserStatus> statuses;
	for (const User* user : users)
		statuses.push_back(UserStatus{ user->id, user->online });
	return statuses;
}
// отправить статусы
void OnlineStatusManager::send_users_status(Connection* connection, const std::vector<UserId>& user_ids) const
{
	std::vector<const User*> updating_users;
	for (UserId user_id : user_ids)
	{
		if (const User* user = m_users.find_user(user_id))
			updating_users.push_back(user);
	}
	connection->send_message(ServerMessageUsersStatus(convert_users_to_status(updating_users)));
}
// отправить в сессию статус пользователя
void OnlineStatusManager::send_user_status(Session* session, const User* user)
{
	session->send_message(ServerMessageContactStatus(user->id, user->online));
}