#pragma once
#include "session_manager.h"

struct ChatListContact;
class SessionManager;
class ChatLists;
class Session;
class Users;

class OnlineStatusManager : public QObject
{
	Q_OBJECT
private:
	SessionManager& m_session_manager;
	const Users& m_users;
	const ChatLists& m_chat_lists;
	
public:
	OnlineStatusManager(SessionManager& session_manager, const Users& users, const ChatLists& chat_lists);
	// обработать разрушение подключения
	void process_connection_destroy(Connection* connection);
	// обработать синхронизацию подключения
	void process_connection_synced(Connection* connection);
	// отправить статусы
	void send_users_status(Connection* connection, const std::vector<UserId>& user_ids) const;
	// отправить в сессию статус пользователя
	void send_user_status(Session* session, const User* user);
	
private:
	// отправить статусы других самому себе
	void send_users_status_myself(Connection* connection);
	// установить онлайн статус
	void set_online_status(UserId user_id, bool online);
	// преобразовать пользователей в структуру онлайн статусов
	std::vector<UserStatus> convert_users_to_status(const std::vector<const User*>& users) const;
};
