//=====================================================================================//
//   Author: open
//   Date:   04.05.2017
//=====================================================================================//
#include "base/precomp.h"
#include "progress_updater.h"

ProgressUpdater::ProgressUpdater(quint64 size)
//: m_timer(1.f/30.f)
: m_size(size)
, m_last_current_size(0)
, m_current_size(0)
{
	//connect(&m_timer, SIGNAL(updated(float)), SLOT(slot_update(float)));
}
void ProgressUpdater::set_size(quint64 size)
{
	m_size = size;
	m_current_size = 0;
	m_last_current_size = 0;
}
void ProgressUpdater::append_current_size(quint64 size)
{
	m_current_size += size;

	// для отправки 100%
	//if (m_current_size == m_size)
	//	internal_emit();

	if (m_size == 0) return;

	if ((m_current_size - m_last_current_size > static_cast<quint64>(static_cast<float>(m_size)*0.01f)) || m_current_size == m_size)
	{
		internal_emit();
	}
}
/*void ProgressUpdater::slot_update(float)
{
	if (m_size == 0) return;

	if ((m_current_size-m_last_current_size > static_cast<quint64>(static_cast<float>(m_size)*0.01f)) || m_current_size == m_size)
	{
		internal_emit();
	}
}*/
void ProgressUpdater::internal_emit()
{
	//emit update_progress(m_current_size);
	unsigned char percent = static_cast<double>(m_current_size) / static_cast<double>(m_size)*100.;
	emit update_progress(percent);
	if (m_current_size == m_size) 
	{
		//disconnect(&m_timer, SIGNAL(updated(float)), this, SLOT(slot_update(float)));
		++m_current_size;
	}
	m_last_current_size = m_current_size;
}