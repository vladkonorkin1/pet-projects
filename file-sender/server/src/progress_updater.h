//=====================================================================================//
//   Author: open
//   Date:   04.05.2017
//=====================================================================================//
//#include "base/timer.h"

class ProgressUpdater : public QObject
{
	Q_OBJECT
private:
	//Base::Timer m_timer;

	quint64 m_last_current_size;
	quint64 m_current_size;
	quint64 m_size;

public:
	ProgressUpdater(quint64 size = 0);
	void set_size(quint64 size);
	void append_current_size(quint64 size);

signals:
	void update_progress(unsigned char percent);

//private slots:
	//void slot_update(float dt);

private:
	void internal_emit();
};