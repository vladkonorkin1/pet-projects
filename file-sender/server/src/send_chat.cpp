//=====================================================================================//
//   Author: open
//   Date:   21.02.2019
//=====================================================================================//
#include "base/precomp.h"
#include "send_chat.h"
#include "connection.h"
#include "chat_list.h"
#include "session.h"
#include "chat.h"
#include "user.h"

SendChat::SendChat()
{
}
// синхронизировать чаты
//void SendChat::sync_chats(Connection* connection, const std::vector<DescSyncChat>& desc_sync_chats, const ChatList* chat_list) const
void SendChat::sync_chats(Connection* connection, Timestamp timestamp_chats, const ChatList* chat_list) const
{
	// синхронизируем только чаты из чат-листа
	{
		//const User* user = connection->user();
		//std::vector<const Chat*> sorted_chats;
		//for (const auto& pair : chat_list->chats())
		//{
		//	const Chat* chat = pair.second->chat;
		//	//if (chat->find_user(user->id))	//!!!!!!!!!!, в которых мы участники
		//		sorted_chats.push_back(chat);
		//}

		// синхронизируем только чаты из чат-листа
		const ChatList::chats_t& all_chats = chat_list->chats();
		std::vector<const Chat*> sorted_chats;
		std::transform(all_chats.begin(), all_chats.end(), std::back_inserter(sorted_chats), [](const std::pair<ChatId, const ChatListContact*> &item) { return item.second->chat; });

		// сортируем их
		std::sort(sorted_chats.begin(), sorted_chats.end(), [](const Chat* x, const Chat* y) { return x->timestamp() < y->timestamp(); });
		// ищем все чаты, которые больше timestamp
		auto it = std::upper_bound(sorted_chats.begin(), sorted_chats.end(), timestamp_chats, [](Timestamp timestamp, const Chat* chat) { return timestamp < chat->timestamp(); });
		for (; it != sorted_chats.end(); it++)
		{
			const Chat* chat = *it;
			send_chat_info(connection, chat);
		}
	}

	// отправляем чат-лист
	{
		std::vector<const ChatListContact*> sorted;
		for (auto pair : chat_list->chats())
			sorted.push_back(pair.second);

		std::sort(sorted.begin(), sorted.end(), [](const ChatListContact* x, const ChatListContact* y) { return x->last_time < y->last_time; });

		ServerMessageChatList msg;
		for (const ChatListContact* contact : sorted)
		{
			auto& container = contact->favorite ? msg.favorites : msg.recents;
			container.push_back(contact->chat->id());
		}
		connection->send_message(msg);
	}
}
// отправить информацию о чате всем соединениям
void SendChat::send_all_chat_info(Session* session, const Chat* chat) const
{
	for (Connection* connection : session->connections())
		send_chat_info(connection, chat);
}
// отправить добавление чата в чат-лист всем соединениям
void SendChat::send_all_add_chat_to_chat_list(Session* session, const ChatListContact* contact) const
{
	for (Connection* connection : session->connections())
		send_add_chat_to_chat_list(connection, contact);
}
// отправить удаление чата из чат-листа всем соединениям
void SendChat::send_all_remove_chat_from_chat_list(Session* session, const Chat* chat) const
{
	for (Connection* connection : session->connections())
		send_remove_chat_from_chat_list(connection, chat);
}
// отправить участников чата всем соединениям
void SendChat::send_all_chat_users(Session* session, const Chat* chat) const
{
	for (Connection* connection : session->connections())
		send_chat_users(connection, chat);
}
// отправить информацию о чате
void SendChat::send_chat_info(Connection* connection, const Chat* chat) const
{
	if (chat->is_group())
	{
		connection->send_message(ServerMessageGroupChat(chat->id(), chat->name(), chat->timestamp(), chat->last_message_id(), chat->last_message_stamp()));
		send_chat_users(connection, chat);
	}
	else
	{
		if (ChatUser* target_user = chat->find_another_user(connection->user()->id))
		{
			connection->send_message(ServerMessageContactChat(chat->id(), target_user->user->id, chat->timestamp(), chat->last_message_id(), chat->last_message_stamp()));
		}
	}
}
// отправить участников чата
void SendChat::send_chat_users(Connection* connection, const Chat* chat) const
{
	assert( chat->is_group() );
	ServerMessageGroupChatSetUsers msg(chat->id(), chat->timestamp());
	for (const auto& pair : chat->users())
	{
		const ChatUser* chat_user = pair.second;
		msg.users.push_back(chat_user->user->id);

		if (chat_user->admin)
			msg.admins.push_back(chat_user->user->id);
	}
	connection->send_message(msg);
}
// отправить добавление чата в чат-лист
void SendChat::send_add_chat_to_chat_list(Connection* connection, const ChatListContact* contact) const
{
	if (contact->favorite) connection->send_message(ServerMessageChatAddFavorite(contact->chat->id()));
	else connection->send_message(ServerMessageChatAddRecent(contact->chat->id()));
}
// отправить удаление чата из чат-листа
void SendChat::send_remove_chat_from_chat_list(Connection* connection, const Chat* chat) const
{
	connection->send_message(ServerMessageChatRemoveResponse(chat->id()));
}