//=====================================================================================//
//   Author: open
//   Date:   21.02.2019
//=====================================================================================//
#pragma once

struct ChatListContact;
class Connection;
class ChatList;
class Session;
class User;
class Chat;

class SendChat
{
private:

public:
	SendChat();
	// синхронизировать чаты
	void sync_chats(Connection* connection, Timestamp timestamp_chats, const ChatList* chat_list) const;
	// отправить информацию о чате всем соединениям
	void send_all_chat_info(Session* session, const Chat* chat) const;
	// отправить добавление чата в чат-лист всем соединениям
	void send_all_add_chat_to_chat_list(Session* session, const ChatListContact* contact) const;
	// отправить удаление чата из чат-листа всем соединениям
	void send_all_remove_chat_from_chat_list(Session* session, const Chat* chat) const;
	// отправить участников чата всем соединениям
	void send_all_chat_users(Session* session, const Chat* chat) const;

	// отправить информацию о чате
	void send_chat_info(Connection* connection, const Chat* chat) const;

private:
	// отправить добавление чата в чат-лист
	void send_add_chat_to_chat_list(Connection* connection, const ChatListContact* contact) const;
	// отправить удаление чата из чат-листа
	void send_remove_chat_from_chat_list(Connection* connection, const Chat* chat) const;
	// отправить участников чата
	void send_chat_users(Connection* connection, const Chat* chat) const;
};