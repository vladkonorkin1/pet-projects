//=====================================================================================//
//   Author: open
//   Date:   12.07.2019
//=====================================================================================//
#include "base/precomp.h"
#include "send_chat_messages.h"
#include "network/net_messages.h"
#include "message_connection.h"
#include "session_manager.h"
#include "chat.h"
#include "user.h"

void add_modify_chat_message(const DescChatMessage& chat_message, bool myself, std::vector<DescChatMessage>& messages)
{
	if (ChatMessageType::File == chat_message.type)
	{
		DescChatMessage copy(chat_message);
		if (myself)
			copy.file_dest_name.clear();
		else
			copy.file_src_path.clear();

		messages.push_back(copy);
	}
	else
		messages.push_back(chat_message);
}
ServerMessageChatMessages build_net_msg(const DescChatMessage& chat_message, bool myself)
{
	ServerMessageChatMessages msg;
	add_modify_chat_message(chat_message, myself, msg.messages);
	return msg;
}

SendChatMessages::SendChatMessages(SessionManager& session_manager)
: m_session_manager(session_manager)
{
}
void SendChatMessages::send_message(const Chat* chat, const DescChatMessage& chat_message)
{
	ServerMessageChatMessages msg_myself = build_net_msg(chat_message, true);
	ServerMessageChatMessages msg_another = build_net_msg(chat_message, false);

	// отправляем по всем сессиям
	for (const auto& pair : chat->users())
	{
		const User* user = pair.second->user;
		if (Session* session = m_session_manager.get_session(user->id))
		{
			bool myself = session->user()->id == chat_message.sender_id;
			session->send_message(myself ? msg_myself : msg_another);
		}
	}
}
void SendChatMessages::send_message(UserId user_id, const DescChatMessage& chat_message)
{
	if (Session* session = m_session_manager.get_session(user_id))
	{
		bool myself = session->user()->id == chat_message.sender_id;
		session->send_message(build_net_msg(chat_message, myself));
	}
}
void SendChatMessages::send_messages(MessageConnection* message_connection, const User* user, const std::vector<DescChatMessage>& messages)
{
	ServerMessageChatMessages msg;
	for (const DescChatMessage& chat_message : messages)
	{
		bool myself = user->id == chat_message.sender_id;
		add_modify_chat_message(chat_message, myself, msg.messages);
	}
	message_connection->send_message(msg);
}
void SendChatMessages::send_history_messages(MessageConnection* message_connection, const User* user, const std::vector<DescChatMessage>& messages, ChatId chat_id, bool finish_upload)
{
	ServerMessageChatHistoryMessages msg(chat_id, finish_upload);
	for (const DescChatMessage& chat_message : messages)
	{
		bool myself = user->id == chat_message.sender_id;
		add_modify_chat_message(chat_message, myself, msg.messages);
	}
	message_connection->send_message(msg);
}