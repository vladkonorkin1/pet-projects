//=====================================================================================//
//   Author: open
//   Date:   12.07.2019
//=====================================================================================//
#pragma once

class MessageConnection;
class SessionManager;
class Chat;
class User;

class SendChatMessages
{
private:
	SessionManager& m_session_manager;

public:
	SendChatMessages(SessionManager& session_manager);
	void send_message(const Chat* chat, const DescChatMessage& chat_message);
	void send_message(UserId user_id, const DescChatMessage& chat_message);
	void send_messages(MessageConnection* message_connection, const User* user, const std::vector<DescChatMessage>& messages);
	void send_history_messages(MessageConnection* message_connection, const User* user, const std::vector<DescChatMessage>& messages, ChatId chat_id, bool finish_upload);
};