//=====================================================================================//
//   Author: open
//   Date:   14.08.2019
//=====================================================================================//
#include "base/precomp.h"
#include "send_user.h"
#include "message_connection.h"
#include "connection_manager.h"
#include "users.h"

SendUser::SendUser(Users& users, ConnectionManager& connection_manager)
: m_users(users)
, m_connection_manager(connection_manager)
, m_timestamp_departments(0)
, m_timestamp_users(0)
{
}
// ��������� ���� ������������
void SendUser::send_all_departments(const std::vector<const Department*>& departments)
{
	if (departments.empty()) return;

	ServerMessageDepartments msg;
	for (const Department* department : departments)
		msg.departments.push_back(convert_department(department));
	m_connection_manager.send_message_all_connections(msg);

	logs(QString("send all departments (size = %1)").arg(msg.departments.size()));
}
// ��������� ���� �������������
void SendUser::send_all_users(const std::vector<const User*>& users)
{
	if (users.empty()) return;

	ServerMessageUsers msg;
	for (const User* user : users)
		msg.users.push_back(convert_user(user));
	m_connection_manager.send_message_all_connections(msg);

	logs(QString("send all users (size = %1)").arg(msg.users.size()));
}
// �������� ������������
void SendUser::process_departments_updated(const std::vector<const Department*>& departments)
{
	if (departments.empty()) return;

	// ���� ���������� ��������� ����� ���������� �������������
	for (const Department* department : departments)
	{
		if (m_timestamp_departments < department->timestamp)
			m_timestamp_departments = department->timestamp;
	}

	// ������ ��������� ��� ������������ � ����������� ��
	m_sorted_departments_by_timestamp = m_users.departments();
	std::sort(m_sorted_departments_by_timestamp.begin(), m_sorted_departments_by_timestamp.end(), [](const Department* x, const Department* y) { return x->timestamp < y->timestamp; });
}
// �������� �������������
void SendUser::process_users_updated(const std::vector<const User*>& users)
{
	if (users.empty()) return;

	// ���� ���������� ��������� ����� ���������� �������������
	for (const User* user : users)
	{
		if (m_timestamp_users < user->timestamp)
			m_timestamp_users = user->timestamp;
	}

	// ������ ��������� ���� ������������� � ����������� ��
	m_sorted_users_by_timestamp = m_users.users();
	std::sort(m_sorted_users_by_timestamp.begin(), m_sorted_users_by_timestamp.end(), [](const User* x, const User* y) { return x->timestamp < y->timestamp; });
}
// ������������� ����������� �� ���������� ��������� � ���������
DescDepartment SendUser::convert_department(const Department* department) const
{
	return DescDepartment {
		department->id,
		department->name,
		department->timestamp
	};
}
// ������������� ������������ �� ���������� ��������� � ���������
DescUser SendUser::convert_user(const User* user) const
{
	return DescUser {
		user->id,
		user->timestamp,
		user->login,
		user->name,
		user->folder,
		user->department->id,
		user->deleted
	};
}
// ���������������� ������������ � ������������� ��� ����������� (��������� ������ ��, ��� > timestamp)
void SendUser::sync_users(MessageConnection* message_connection, Timestamp timestamp_departments, Timestamp timestamp_users)
{
	if (m_timestamp_departments != timestamp_departments)
	{
		// �������� ������������, ������� ��� � �������
		ServerMessageDepartments msg;

		// ������ ��
		auto it = std::upper_bound(m_sorted_departments_by_timestamp.begin(), m_sorted_departments_by_timestamp.end(), timestamp_departments, [this](Timestamp timestamp, const Department* department) { return timestamp < department->timestamp; });

		for (; it != m_sorted_departments_by_timestamp.end(); it++)
			msg.departments.push_back(convert_department(*it));

		if (!msg.departments.empty())
			message_connection->send_message(msg);
	}

	if (m_timestamp_users != timestamp_users)
	{
		// �������� �������������, ������� ��� � �������
		ServerMessageUsers msg;

		// ������ ��
		auto it = std::upper_bound(m_sorted_users_by_timestamp.begin(), m_sorted_users_by_timestamp.end(), timestamp_users, [this](Timestamp timestamp, const User* user) { return timestamp < user->timestamp; });

		for (; it != m_sorted_users_by_timestamp.end(); it++)
			msg.users.emplace_back(convert_user(*it));

		if (!msg.users.empty())
			message_connection->send_message(msg);
	}
}