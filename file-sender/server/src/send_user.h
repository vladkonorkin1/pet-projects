//=====================================================================================//
//   Author: open
//   Date:   14.08.2019
//=====================================================================================//
#pragma once

class MessageConnection;
class ConnectionManager;
class Department;
class Users;
class User;

class SendUser
{
private:
	Users& m_users;
	ConnectionManager& m_connection_manager;
	std::vector<const Department*> m_sorted_departments_by_timestamp;
	std::vector<const User*> m_sorted_users_by_timestamp;
	Timestamp m_timestamp_departments;
	Timestamp m_timestamp_users;

public:
	SendUser(Users& users, ConnectionManager& connection_manager);
	// �������� ������������
	void process_departments_updated(const std::vector<const Department*>& departments);
	// �������� �������������
	void process_users_updated(const std::vector<const User*>& users);
	
	// ��������� ���� ������������
	void send_all_departments(const std::vector<const Department*>& departments);
	// ��������� ���� �������������
	void send_all_users(const std::vector<const User*>& users);
	// ���������������� ������������ � ������������� ��� ����������� (��������� ������ ��, ��� ���� timestamp)
	void sync_users(MessageConnection* message_connection, Timestamp timestamp_departments, Timestamp timestamp_users);

private:
	// ������������� ����������� �� ���������� ��������� � ���������
	DescDepartment convert_department(const Department* department) const;
	// ������������� ������������ �� ���������� ��������� � ���������
	DescUser convert_user(const User* user) const;
};