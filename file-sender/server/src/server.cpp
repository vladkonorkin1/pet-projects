//=====================================================================================//
//   Author: open
//   Date:   15.04.2017
//=====================================================================================//
#include "base/precomp.h"
#include "server.h"
#include "file_transfer/file_thread_connection.h"

Server::Server(const DatabaseParams& database_params, const QString& service_employers_login, const QString& service_employers_password, const QString& authorization_service_login, const QString& authorization_service_password, const QStringList& allow_ip_connections, const QStringList& no_auth_users)
: m_timer(5.f)
, m_database(database_params)
, m_database_properties(m_database)
, m_database_async(database_params)
, m_database_chats(m_database)
, m_chat_message_stamp_generator(m_database_async)
, m_connection_manager(*this, m_database_properties.get("database_timestamp").toLongLong(), authorization_service_login, authorization_service_password, allow_ip_connections, no_auth_users)
, m_session_manager(m_database_async)
, m_users()
, m_chats(m_users, m_database_chats, m_database_async)
, m_chat_lists(m_users, m_chats, m_database_async, m_database_chats)
, m_send_user(m_users, m_connection_manager)
, m_send_chat()
, m_send_chat_messages(m_session_manager)
, m_file_message_manager(m_database_async, m_session_manager, m_chat_message_stamp_generator, m_chats, m_send_chat_messages)
, m_file_connection_server(m_database_async, m_file_message_manager)
, m_online_status_manager(m_session_manager, m_users, m_chat_lists)
//, m_database_ddos(*this)
, m_users_updater(m_database_async, m_database_properties, m_users, m_send_user, service_employers_login, service_employers_password)
{
	//m_database_ddos.start();

	connect(&m_timer, SIGNAL(updated(float)), SLOT(slot_timer_updated(float)));
	connect(&m_connection_manager, SIGNAL(connection_created(Connection*)), SLOT(slot_connection_created(Connection*)));
	connect(&m_connection_manager, SIGNAL(connection_destroyed(Connection*)), SLOT(slot_connection_destroyed(Connection*)));
	connect(&m_connection_manager, SIGNAL(connection_synced(Connection*)), SLOT(slot_connection_synced(Connection*)));
	connect(&m_file_connection_server, SIGNAL(thread_created(FileThreadConnection*)), SLOT(slot_file_connection_thread_created(FileThreadConnection*)));
	connect(&m_file_connection_server, SIGNAL(thread_destroyed(FileThreadConnection*)), SLOT(slot_file_connection_thread_destroyed(FileThreadConnection*)));
}
void Server::slot_connection_created(Connection* connection)
{
	m_session_manager.add_connection(connection);
}
void Server::slot_connection_destroyed(Connection* connection)
{
	m_online_status_manager.process_connection_destroy(connection);
	m_file_message_manager.process_connection_destroy(connection);
	m_session_manager.remove_connection(connection);
}
void Server::slot_connection_synced(Connection* connection)
{
	m_online_status_manager.process_connection_synced(connection);
}
void Server::slot_timer_updated(float dt)
{
	m_connection_manager.update(dt);
	m_session_manager.update(dt);
}
void Server::slot_file_connection_thread_created(FileThreadConnection* file_thread_connection)
{
	if (Connection* connection = m_connection_manager.find_connection(file_thread_connection->message_connection_id()))
		connection->open_file_thread_connection(file_thread_connection);
}
void Server::slot_file_connection_thread_destroyed(FileThreadConnection* file_thread_connection)
{
	if (Connection* connection = m_connection_manager.find_connection(file_thread_connection->message_connection_id()))
		connection->close_file_thread_connection(file_thread_connection);
}