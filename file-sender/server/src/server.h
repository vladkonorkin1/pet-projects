//=====================================================================================//
//   Author: open
//   Date:   15.04.2017
//=====================================================================================//
#pragma once
#include "file_transfer/file_connection_server.h"
#include "file_transfer/file_message_manager.h"
#include "database/async/database_async.h"
#include "database/database_chats.h"
#include "database/database_properties.h"
#include "chat_message_stamp_generator.h"
#include "online_status_manager.h"
#include "thread_task_manager.h"
#include "send_chat_messages.h"
#include "connection_manager.h"
#include "database/database.h"
#include "session_manager.h"
#include "users_updater.h"
#include "base/timer.h"
#include "chat_lists.h"
#include "send_chat.h"
#include "send_user.h"
#include "users.h"
#include "chats.h"

class Connection;

class Server : public QObject
{
	Q_OBJECT
private:
	Base::Timer m_timer;
	Database m_database;
	DatabaseProperties m_database_properties;	// запрос свойств из базы данных
	DatabaseAsync m_database_async;				// асинхронные запросы к базе
	DatabaseChats m_database_chats;				// запрос чатов из базы данных
	ChatMessageStampGenerator m_chat_message_stamp_generator;

	ConnectionManager m_connection_manager;
	SessionManager m_session_manager;
	Users m_users;
	Chats m_chats;
	ChatLists m_chat_lists;						// менеджер чат-листов

	SendUser m_send_user;
	SendChat m_send_chat;
	SendChatMessages m_send_chat_messages;

	FileMessageManager m_file_message_manager;
	FileConnectionServer m_file_connection_server;
	OnlineStatusManager m_online_status_manager;
	//DatabaseTestDDOS m_database_ddos;
	UsersUpdater m_users_updater;

	ThreadTaskManager m_thread_task_manager;

public:
	Server(const DatabaseParams& database_params, const QString& service_employers_login, const QString& service_employers_password, const QString& authorization_service_login, const QString& authorization_service_password, const QStringList& allow_ip_connections, const QStringList& no_auth_users);

	DatabaseAsync& database_async();
	Users& users();
	Chats& chats();
	ChatLists& chat_lists();
	SendUser& send_user();
	SendChat& send_chat();
	SendChatMessages& send_chat_messages();
	FileMessageManager& file_message_manager();
	DatabaseChats& database_chats();
	SessionManager& session_manager();
	ChatMessageStampGenerator& chat_message_stamp_generator();
	OnlineStatusManager& online_status_manager();
	ThreadTaskManager& thread_task_manager();

private slots:
	void slot_connection_created(Connection* connection);
	void slot_connection_destroyed(Connection* connection);
	void slot_connection_synced(Connection* connection);
	void slot_timer_updated(float dt);
	void slot_file_connection_thread_created(FileThreadConnection* file_thread_connection);
	void slot_file_connection_thread_destroyed(FileThreadConnection* file_thread_connection);
};


inline Users& Server::users() {
	return m_users;
}
inline Chats& Server::chats() {
	return m_chats;
}
inline DatabaseAsync& Server::database_async() {
	return m_database_async;
}
inline ChatLists& Server::chat_lists() {
	return m_chat_lists;
}
inline DatabaseChats& Server::database_chats() {
	return m_database_chats;
}
inline SessionManager& Server::session_manager() {
	return m_session_manager;
}
inline ChatMessageStampGenerator& Server::chat_message_stamp_generator() {
	return m_chat_message_stamp_generator;
}
inline FileMessageManager& Server::file_message_manager() {
	return m_file_message_manager;
}
inline SendChatMessages& Server::send_chat_messages() {
	return m_send_chat_messages;
}
inline OnlineStatusManager& Server::online_status_manager() {
	return m_online_status_manager;
}
inline SendChat& Server::send_chat() {
	return m_send_chat;
}
inline SendUser& Server::send_user() {
	return m_send_user;
}
inline ThreadTaskManager& Server::thread_task_manager() {
	return m_thread_task_manager;
}