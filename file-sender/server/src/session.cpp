//=====================================================================================//
//   Author: open
//   Date:   13.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "session.h"
#include "connection.h"

#ifdef _DEBUG
float Session::s_destroy_timeout = 2 * 60;
#else
float Session::s_destroy_timeout = 60;
#endif

Session::Session(const User* user, ChatList* chat_list)//, DatabaseAsync& database_async)
: m_time_alive(0.f)
, m_update_connections(true)
, m_user(user)
, m_chat_list(chat_list)
//, m_unread_messages(m_user, database_async)
{
}
void Session::add_connection(Connection* connection)
{
	m_update_connections = true;
	m_map_connections[connection->id()] = connection;
	connection->set_session(this);

	// инициализируем менеджер непрочитанных сообщений
	//connect(&m_unread_messages, &UnreadMessages::loaded, [this, connection_id = connection->id()]() { slot_unreaded_messages_loaded(connection_id); });
	//m_unread_messages.load_from_database();
}
/*// событие загрузки непрочитанных сообщений
void Session::slot_unreaded_messages_loaded(ConnectionId connection_id)
{
	// ищем connection, к этому моменту он может быть удалён
	auto it = m_map_connections.find(connection_id);
	if (m_map_connections.end() != it)
	{
		Connection* connection = it->second;
		connection->set_unreaded_messages(&m_unread_messages);
	}
}*/
void Session::remove_connection(Connection* connection)
{
	m_update_connections = true;
	m_map_connections.erase(connection->id());
}
bool Session::check_destroy(float dt)
{
	if (m_map_connections.empty())
	{
		m_time_alive += dt;
		if (m_time_alive > s_destroy_timeout)
			return true;
	}
	else m_time_alive = 0.f;
	return false;
}
void Session::send_message(const NetMessage& msg)
{
	for (auto& pair : m_map_connections)
	{
		Connection* connection = pair.second;
		connection->send_message(msg);
	}
}
const Session::connections_t& Session::connections()
{
	if (m_update_connections)
	{
		m_update_connections = false;
		m_connections.clear();
		std::transform(m_map_connections.begin(), m_map_connections.end(), std::back_inserter(m_connections), [](const std::pair<ConnectionId, Connection*> &item) { return item.second; });
	}
	return m_connections;
}