//=====================================================================================//
//   Author: open
//   Date:   13.09.2018
//=====================================================================================//
#pragma once
//#include "unread_messages.h"

struct NetMessage;
class Connection;
class ChatList;
class User;

class Session : public QObject
{
	Q_OBJECT
public:
	using connections_t = std::vector<Connection*>;

private:
	float m_time_alive;
	static float s_destroy_timeout;
	
	std::map<ConnectionId, Connection*> m_map_connections;
	bool m_update_connections;
	connections_t m_connections;
	const User* m_user;
	
	ChatList* m_chat_list;

	//UnreadMessages m_unread_messages;

public:
	Session(const User* user, ChatList* chat_list);// , DatabaseAsync& database_async);
	
	void add_connection(Connection* connection);
	void remove_connection(Connection* connection);
	const connections_t& connections();

	bool check_destroy(float dt);
	void send_message(const NetMessage& msg);

	const User* user() const { return m_user; }
	const ChatList* chat_list() const { return m_chat_list; }

//private slots:
	// событие загрузки непрочитанных сообщений
	//void slot_unreaded_messages_loaded(ConnectionId connection_id);
};

using USession = std::unique_ptr<Session>;