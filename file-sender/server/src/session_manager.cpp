//=====================================================================================//
//   Author: open
//   Date:   13.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "session_manager.h"
#include "connection.h"
#include "user.h"

SessionManager::SessionManager(DatabaseAsync& database_async)
: m_database_async(database_async)	// ???
{
}
void SessionManager::add_connection(Connection* connection)
{
	const User* user = connection->user();
	sessions_t::iterator it = m_sessions.find(user->id);
	if (m_sessions.end() == it)
	{
		USession usession(new Session(user, connection->chat_list()));// , m_database_async));
		Session* session = usession.get();
		m_sessions[user->id] = std::move(usession);
		//setup_session(session, connection);
		session->add_connection(connection);
		logs(QString("net: open session=%1  count sessions=%2").arg(user->login).arg(m_sessions.size()));
		emit session_opened(session);
	}
	else
	{
		Session* session = it->second.get();
		session->add_connection(connection);
		//setup_session(session, connection);
	}
}
void SessionManager::setup_session(Session* session, Connection* connection) const
{
	//session->add_connection(connection);
	//connection->set_session(session);


}
void SessionManager::remove_connection(Connection* connection)
{
 	const User* user = connection->user();
	sessions_t::iterator it = m_sessions.find(user->id);
	if (m_sessions.end() != it)
	{
		Session* session = it->second.get();
		session->remove_connection(connection);
		connection->set_session(nullptr);
	}
}
void SessionManager::update(float dt)
{
	for (sessions_t::iterator it = m_sessions.begin(); it != m_sessions.end();)
	{
		Session* session = it->second.get();
		sessions_t::iterator it_erase = it++;
 		if (session->check_destroy(dt))
			close_session(it_erase);
	}
}
void SessionManager::close_session(sessions_t::iterator it)
{
	Session* session = it->second.get();
	logs(QString("net: close session=%1  count sessions=%2").arg(session->user()->login).arg(m_sessions.size() - 1));
	emit session_closed(session);
	m_sessions.erase(it);
}
Session* SessionManager::get_session(UserId user_id)
{
	sessions_t::iterator it = m_sessions.find(user_id);
	if (m_sessions.end() != it)
		return it->second.get();
	return nullptr;
}