//=====================================================================================//
//   Author: open
//   Date:   13.09.2018
//=====================================================================================//
#pragma once
#include "session.h"

class DatabaseAsync;
class Connection;

class SessionManager : public QObject
{
	Q_OBJECT
private:
	DatabaseAsync& m_database_async;

	//using USession = std::unique_ptr<Session>;

public:
	using USession = std::unique_ptr<Session>;
	using sessions_t = std::map<UserId, USession>;
	sessions_t m_sessions;

public:
	SessionManager(DatabaseAsync& database_async);
	void update(float dt);
	void add_connection(Connection* connection);
	void remove_connection(Connection* connection);
	Session* get_session(UserId user_id);

	sessions_t& get_sessions();

signals:
	void session_opened(Session* session);
	void session_closed(Session* session);

private:
	void setup_session(Session* session, Connection* connection) const;
	void close_session(sessions_t::iterator it);
};

inline SessionManager::sessions_t& SessionManager::get_sessions() {
	return m_sessions;
}