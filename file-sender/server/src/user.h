//=====================================================================================//
//   Author: open
//   Date:   18.02.2018
//=====================================================================================//
#pragma once

class Department
{
public:
	DepartmentId id;
	QUuid uuid;
	QString name;
	Timestamp timestamp;

	Department(DepartmentId id, const QUuid& uuid) : id(id), uuid(uuid), timestamp(0) {}
};

class User
{
public:
	UserId id;						// уникальный номер
	QUuid uuid;
	QString login;					// логин
	QString name;					// ФИО
	QString folder;					// путь к папке для файлов
	Timestamp timestamp;			// временная отметка изменения
	Department* department;
	bool deleted;
	bool online;

	User(UserId id, const QUuid& uuid, const QString& login) : id(id), uuid(uuid), login(login), timestamp(0), department(nullptr), deleted(false), online(false) {}
};