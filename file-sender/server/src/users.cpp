//=====================================================================================//
//   Author: open
//   Date:   16.02.2019
//=====================================================================================//
#include "base/precomp.h"
#include "users.h"

Users::~Users()
{
	for (auto pair : m_id_users)
		delete pair.second;

	for (auto pair : m_id_departments)
		delete pair.second;
}
Department* Users::create_department(DepartmentId id, const QUuid& uuid)
{
	auto it_id = m_id_departments.find(id);
	//auto it_uuid = m_uuid_departments.find(uuid);

	if (it_id == m_id_departments.end())// && it_uuid == m_uuid_departments.end())
	{
		Department* department = new Department(id, uuid);
		m_id_departments[id] = department;
		m_uuid_departments[uuid] = department;
		return department;
	}
	return nullptr;
}
Department* Users::find_department(DepartmentId id) const
{
	auto it = m_id_departments.find(id);
	if (it != m_id_departments.end()) return it->second;
	return nullptr;
}
Department* Users::find_department(const QUuid& uuid) const
{
	auto it = m_uuid_departments.find(uuid);
	if (it != m_uuid_departments.end()) return it->second;
	return nullptr;
}
User* Users::create_user(UserId id, const QUuid& uuid, const QString& login)
{
	if (m_id_users.find(id) == m_id_users.end())
	{
		User* user = new User(id, uuid, login);
		m_uuid_users[uuid] = user;
		m_id_users[id] = user;
		if (!login.isEmpty())
			m_login_users[login] = user;
		return user;
	}
	return nullptr;
}
User* Users::find_user(UserId id) const
{
	auto it = m_id_users.find(id);
	if (it != m_id_users.end()) return it->second;
	return nullptr;
}
User* Users::find_user(const QString& login) const
{
	auto it = m_login_users.find(login);
	if (it != m_login_users.end()) return it->second;
	return nullptr;
}
User* Users::find_user(const QUuid& uuid) const
{
	auto it = m_uuid_users.find(uuid);
	if (it != m_uuid_users.end()) return it->second;
	return nullptr;
}
std::vector<const Department*> Users::departments() const
{
	std::vector<const Department*> departments;
	for (auto& pair : m_id_departments)
		departments.push_back(pair.second);
	return departments;
}
std::vector<const User*> Users::users() const
{
	std::vector<const User*> users;
	for (auto& pair : m_id_users)
		users.push_back(pair.second);
	return std::move(users);
}
// получить полный путь к папке перемещений
static QString s_folder_mover_name = QString::fromUtf8("/!Для перемещений");
QString Users::full_folder_path(const User* user) const
{
	//return m_file_server_folder_path + '/' + user->folder + s_folder_mover_name;
	return user->folder + s_folder_mover_name;
}
// обновить данные пользователя
void Users::update_user(User* user, const QString& new_login, const QString& name, Timestamp timestamp, bool deleted, const QString& folder, DepartmentId department_id)
{
	user->name = name;
	user->timestamp = timestamp;
	user->deleted = deleted;
	user->folder = folder;
	user->department = find_department(department_id);
	assert( user->department );

	// если изменился логин
	if (user->login != new_login)
	{
		// удаляем старый логин
		if (!user->login.isEmpty())
			m_login_users.erase(user->login);

		// заносим новый логин
		if (!new_login.isEmpty())
			m_login_users[new_login] = user;

		user->login = new_login;
	}
}