//=====================================================================================//
//   Author: open
//   Date:   16.02.2019
//=====================================================================================//
#pragma once
#include "user.h"

class Users
{
private:
	std::map<DepartmentId, Department*> m_id_departments;
	std::map<QUuid, Department*> m_uuid_departments;
	std::map<UserId, User*> m_id_users;
	std::map<QUuid, User*> m_uuid_users;
	std::map<QString, User*> m_login_users;

public:
	virtual ~Users();

	User* create_user(UserId id, const QUuid& uuid, const QString& login);
	User* find_user(UserId id) const;
	User* find_user(const QString& login) const;
	User* find_user(const QUuid& uuid) const;
	// обновить данные пользователя
	void update_user(User* user, const QString& login, const QString& name, Timestamp timestamp, bool deleted, const QString& folder, DepartmentId department_id);
	
	Department* create_department(DepartmentId id, const QUuid& uuid);
	Department* find_department(DepartmentId id) const;
	Department* find_department(const QUuid& uuid) const;

	std::vector<const Department*> departments() const;
	std::vector<const User*> users() const;

	// получить полный путь к папке перемещений
	QString full_folder_path(const User* user) const;
};