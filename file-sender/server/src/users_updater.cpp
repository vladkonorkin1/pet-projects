//=====================================================================================//
//   Author: open
//   Date:   09.08.2019
//=====================================================================================//
#include "base/precomp.h"
#include "users_updater.h"
#include "database/async/database_async.h"
#include "database/database_properties.h"
#include "base/convert_utility.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QTimer>
#include "send_user.h"
#include "users.h"
#include <array>

static QString s_last_update_users_timestamp = "last_update_users_timestamp";

UsersUpdater::UsersUpdater(DatabaseAsync& database_async, DatabaseProperties& database_properties, Users& users, SendUser& send_user, const QString& service_employers_login, const QString& service_employers_password)
: m_database_async(database_async)
, m_database_properties(database_properties)
, m_users(users)
, m_send_user(send_user)
, m_last_datetime(str_to_datetime(m_database_properties.get(s_last_update_users_timestamp)))
{
	QString auth = QString("Basic ") + QString("%1:%2").arg(service_employers_login).arg(service_employers_password).toUtf8().toBase64();
	m_http_client.set_header("authorization", auth);

	load_users_from_database();
}
// загрузить из базы
void UsersUpdater::load_users_from_database()
{
	DBQuerySelectUsers* query = new DBQuerySelectUsers();
	connect(query, &DBQuerySelectUsers::finished, this, &UsersUpdater::slot_database_load_users);
	m_database_async.post_query(query);
}
#include <QDir>
// слот загрузки пользователей из базы
void UsersUpdater::slot_database_load_users(bool success, const std::vector<DatabaseDepartment>& database_departments, const std::vector<DatabaseUser>& database_users)
{
	if (!success) return;

	std::vector<const Department*> departments;
	for (const DatabaseDepartment& desc : database_departments)
	{
		if (Department* department = m_users.create_department(desc.id, desc.uuid))
		{
			update_department(department, desc);
			departments.push_back(department);
		}
	}
	// оповещаем об обновлении департаментов
	m_send_user.process_departments_updated(departments);

	std::vector<const User*> users;
	for (const DatabaseUser& desc : database_users)
	{
		if (User* user = m_users.create_user(desc.id, desc.uuid, desc.login))
		{
			m_users.update_user(user, desc.login, desc.name, desc.timestamp, desc.deleted, desc.folder, desc.department_id);
			users.push_back(user);
		}
	}
	// оповещаем об обновлении пользователей
	m_send_user.process_users_updated(users);

	/*for (const DatabaseUser& desc : database_users)
	{
		if (!desc.deleted)
		{
			if (desc.folder.isEmpty())
			{
				//logs(QString("error user folder: user=%1 folder=%2").arg(desc.name).arg(desc.folder));
				continue;
			}

			QString folder = desc.folder + QString::fromUtf8("\\!Для перемещений");
			QDir dir(folder);
			bool is_exists = dir.exists();
			if (!is_exists)
			{
				logs(QString("error user folder: user=%1 folder=%2").arg(desc.name).arg(folder));
				continue;
			}
			//logs(QString("user folder: user=%1 folder=%2").arg(desc.name).arg(desc.folder));	
			//logs(QString("user folder: user=%1 exists=%2 folder=%4").arg(desc.name).arg(is_exists).arg(desc.folder));
		}
	}*/

	// запускаем синхронизацию пользователей чата и сотрудников
	restart_update();
}
// перезапуск обновления
void UsersUpdater::restart_update()
{
	update();
	QTimer::singleShot(1000*60*60, this, [this]() { restart_update(); });
}
// обновить данные департамента
void UsersUpdater::update_department(Department* department, const DatabaseDepartment& database_department)
{
	department->name = database_department.name;
	department->timestamp = database_department.timestamp;
}
// обновить пользователей
void UsersUpdater::update()
{
	logs("update users: start");
	logs("update users: request departments start");

	// запоминаем время начала обновления
	m_request_datetime = QDateTime::currentDateTimeUtc();

	/*qint64 days = m_last_datetime.daysTo(m_request_datetime);
	if (days > 0)
	{
		logs(QString("update users: passed days=%1 -> request all departments and employers").arg(days));

		m_last_datetime = QDateTime();
		m_last_datetime.setTimeSpec(Qt::TimeSpec::UTC);
	}*/

	// если наступил следующий день, то обновляем всё
	if (m_last_datetime.date() != m_request_datetime.date())
	{
		logs("update users: request all departments and employers");

		m_last_datetime = QDateTime();
		m_last_datetime.setTimeSpec(Qt::TimeSpec::UTC);
	}

	// запрос департаментов
	m_http_request_departments.reset(new HttpRequestDepartments(QDateTime(m_last_datetime.date())));
	connect(m_http_request_departments.get(), &HttpRequestDepartments::received, this, &UsersUpdater::slot_request_departments_received);
	m_http_client.request(m_http_request_departments, HttpMethod::Get);	
}
// событие получения департаментов
void UsersUpdater::slot_request_departments_received(bool success, const QJsonDocument& json_doc)
{
	logs("update users: request departments finish");

	m_http_request_departments.reset();
	if (!success) return;
	
	std::vector<DBQueryUpdateDepartments::Department> departments;
	QJsonArray json_departments = json_doc.array();
	for (const auto& json_department_ref : json_departments)
	{
		QJsonObject json_department = json_department_ref.toObject();
		departments.emplace_back(DBQueryUpdateDepartments::Department {
			json_department["id"].toVariant().toUuid(),
			json_department["name"].toString(),
			QDateTime::currentDateTimeUtc().toMSecsSinceEpoch()
		});
	}

	if (!departments.empty())
	{
		DBQueryUpdateDepartments* query = new DBQueryUpdateDepartments(departments);
		connect(query, &DBQueryUpdateDepartments::finished, this, &UsersUpdater::slot_database_departments_finished);
		m_database_async.post_query(query);
	}
	else request_employers();
}
// событие записи департаментов в базу
void UsersUpdater::slot_database_departments_finished(bool success, const std::vector<DatabaseDepartment>& database_departments)
{	
	if (!success) return;

	// обновляем департаменты и отправляем их в сеть
	std::vector<const Department*> departments;
	for (const DatabaseDepartment& desc : database_departments)
	{
		Department* department = m_users.find_department(desc.id);
		if (!department)
			department = m_users.create_department(desc.id, desc.uuid);
		update_department(department, desc);

		departments.push_back(department);
	}
	// оповещаем об обновлении департаментов
	m_send_user.process_departments_updated(departments);
	// отправляем в сеть департаменты
	m_send_user.send_all_departments(departments);

	// запрос сотрудников
	request_employers();
}
// запрос сотрудников
void UsersUpdater::request_employers()
{
	logs("update users: request employers start");
	//m_http_request_employers.reset(new HttpRequestEmployers(m_last_datetime.toUTC()));
	m_http_request_employers.reset(new HttpRequestEmployers(QDateTime(m_last_datetime.date())));
	connect(m_http_request_employers.get(), &HttpRequestEmployers::received, this, &UsersUpdater::slot_request_employers_received);
	m_http_client.request(m_http_request_employers, HttpMethod::Get);
}
// событие получения сотрудников
void UsersUpdater::slot_request_employers_received(bool success, const QJsonDocument& json_doc)
{
	logs("update users: request employers finish");

	m_http_request_employers.reset();
	if (!success) return;
	
	std::vector<DBQueryUpdateUser::User> users;

	QJsonArray json_employers = json_doc.array();
	for (const auto& json_employer_ref : json_employers)
	{
		QJsonObject json_employer = json_employer_ref.toObject();

		QUuid uuid = json_employer["id"].toVariant().toUuid();
		if (uuid.isNull())
			continue;

		QString login = json_employer["ad_login"].toString();
		if (login.isNull())
		{
			// и пользователь ещё не заведен
			if (!m_users.find_user(uuid))
				continue;
		}

		QJsonObject json_department = json_employer["department"].toObject();
		QUuid department_uuid = json_department["id"].toVariant().toUuid();
		if (department_uuid.isNull())
			continue;

		Department* department = m_users.find_department(department_uuid);
		if (!department)
			continue;

		int status = json_employer["status"].toObject()["id"].toInt();
		QString folder = json_employer["private_folder"].toString();

		logs(QString("update users: update user=%1 folder=%2").arg(login).arg(folder));

		/*QString old_folder = folder;
		static std::array<QString, 7> replaces = {
			"\\\\sima-land.local\\files_server",
			"\\\\10.12.5.119\\files_server",
			"\\\\vwfileserverelectro",
			"\\\\vwfstekstil",
			"\\\\vwFSRemote",
			"\\\\vwfsit",
			"\\\\10.12.101.65"
		};
		for (const QString& str : replaces)
		{
			folder.remove(str, Qt::CaseInsensitive);
			if (folder.length() != old_folder.length())
			{
				//folder = "\\\\PC85607X\\file-sender-downloads" + folder;	// тестовая передача файлов
				//folder = "\\\\sima-land.local\\files_server\\Департамент ИТ\\_ОБЩАЯ\\тестовая передача файлов" + folder;
				folder = "downloads" + folder;
				break;
			}
		}*/

		users.emplace_back(DBQueryUpdateUser::User {
			uuid,
			login,
			//"",
			json_employer["full_name"].toString(),
			folder,
			QDateTime::currentDateTimeUtc().toMSecsSinceEpoch(),
			department->id,
			status != 0
		});
	}

	if (!users.empty())
	{
		DBQueryUpdateUser* query = new DBQueryUpdateUser(users);
		connect(query, &DBQueryUpdateUser::finished, this, &UsersUpdater::slot_database_users_finished);
		m_database_async.post_query(query);
	}
	else finish_update();
}
// событие записи пользователей в базу
void UsersUpdater::slot_database_users_finished(bool success, const std::vector<DatabaseUser>& database_users)
{
	if (!success) return;
	
	// создаем пользователей и отправляем их в сеть
	std::vector<const User*> users;
	for (const DatabaseUser& database_user : database_users)
	{
		User* user = m_users.find_user(database_user.id);
		if (!user)
			user = m_users.create_user(database_user.id, database_user.uuid, database_user.login);

		m_users.update_user(user, database_user.login, database_user.name, database_user.timestamp, database_user.deleted, database_user.folder, database_user.department_id);
		users.push_back(user);
	}
	// оповещаем об обновлении пользователей
	m_send_user.process_users_updated(users);
	// отправляем в сеть пользователей
	m_send_user.send_all_users(users);

	finish_update();
}
// завершение обновления
void UsersUpdater::finish_update()
{
	// сохраняем время записи последнего обновления
	m_last_datetime = m_request_datetime;
	m_database_properties.set(s_last_update_users_timestamp, datetime_to_str(m_last_datetime));
	logs("update users: finish");
}