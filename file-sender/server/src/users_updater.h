//=====================================================================================//
//   Author: open
//   Date:   09.08.2019
//=====================================================================================//
#pragma once
#include "database/async/queries/database_async_users.h"
#include "http_request_departments.h"
#include "http_request_employers.h"
#include "http_client.h"

class DatabaseProperties;
class DatabaseAsync;
class Department;
class SendUser;
class Users;

class UsersUpdater : public QObject
{
	Q_OBJECT
private:
	DatabaseAsync& m_database_async;
	DatabaseProperties& m_database_properties;
	Users& m_users;
	SendUser& m_send_user;
	HttpClient m_http_client;
	SHttpRequestEmployers m_http_request_employers;
	SHttpRequestDepartments m_http_request_departments;
	QDateTime m_request_datetime;
	QDateTime m_last_datetime;

public:
	UsersUpdater(DatabaseAsync& database_async, DatabaseProperties& database_properties, Users& users, SendUser& send_user, const QString& service_employers_login, const QString& service_employers_password);
	// обновить пользователей
	void update();

private slots:
	// слот загрузки пользователей из базы
	void slot_database_load_users(bool success, const std::vector<DatabaseDepartment>& departments, const std::vector<DatabaseUser>& users);
	// событие получения департаментов
	void slot_request_departments_received(bool success, const QJsonDocument& json_doc);
	// событие записи департаментов в базу
	void slot_database_departments_finished(bool success, const std::vector<DatabaseDepartment>& database_departments);
	// событие получения сотрудников
	void slot_request_employers_received(bool success, const QJsonDocument& json_doc);
	// событие записи пользователей в базу
	void slot_database_users_finished(bool success, const std::vector<DatabaseUser>& database_users);

private:
	// обновить данные департамента
	void update_department(Department* department, const DatabaseDepartment& database_department);
	// запрос сотрудников
	void request_employers();
	// загрузить из базы
	void load_users_from_database();
	// завершение обновления
	void finish_update();
	// перезапуск обновления
	void restart_update();
};

/*
#include "database/database_users.h"
#include <QFileSystemWatcher>
#include "base_users.h"
#include "pugixml.hpp"

class DatabaseProperties;
class ConnectionManager;
class MessageConnection;

class UniqueLogins
{
private:
	using unique_names_t = std::set<QString>;
	unique_names_t m_unique_names;

public:
	bool add(const QString& name);
	bool find(const QString& name) const;
};

class Users : public QObject
{
	Q_OBJECT
private:
	DatabaseProperties& m_database_properties;
	DatabaseUsers& m_database_users;
	ConnectionManager& m_connection_manager;

	QString m_path;
	QFileSystemWatcher m_watcher;
	
	BaseUsers m_users;

	users_t m_sorted_users_by_name;
	users_t m_sorted_users_by_timestamp;
	Timestamp m_timestamp_departments;
	Timestamp m_timestamp_users;

public:
	Users(DatabaseProperties& database_properties, DatabaseUsers& database_users, ConnectionManager& connection_manager);
    	User* create_user(UserId id, const QString& login);
	User* find_user(UserId id) const; 
	User* find_user(const QString& login) const;
	void sync_users(MessageConnection* message_connection, Timestamp timestamp_departments, Timestamp timestamp_users);

	const users_t& sorted_users_by_name() const;
    	void update_user(User* user, const QString& name, const QString& folder, Timestamp timestamp, Department* department, bool deleted);

	// установить путь к папке файл-сервера
	void set_file_server_folder_path(const QString& path);
	// получить полный путь к папке перемещений
	QString full_folder_path(const User* user) const;
    	Department* create_department(DepartmentId id, const QString& name);
    	Department* find_department(DepartmentId id);
    		Department* find_department(const QString& name);
    	BaseUsers& base_users();

signals:
	void user_added(const User* user);

private slots:
	void slot_file_users_changed(const QString& filename);

private:

	void parse_users(pugi::xml_node& department_node, Department* department, UniqueLogins& unique_login);
    	void database_load();
	void reload();
	bool load();

	/// высылаем в сеть новый загруженный департамент
	void sync_new_department(const Department* department);
	void sort_users();
	// обновить таймштамп пользователей
	void update_timestamp_users();
};

inline User* Users::create_user(UserId id, const QString &login) {
    return m_users.add_user(id, login);
}
inline User* Users::find_user(UserId id) const {
	return m_users.find_user(id);
}
inline const users_t& Users::sorted_users_by_name() const {
	return m_sorted_users_by_name;
}
inline User* Users::find_user(const QString& login) const {
	return m_users.find_user(login);
}
inline Department* Users::find_department(DepartmentId id){
    return m_users.find_department(id);
}
inline Department* Users::find_department(const QString& name){
    return m_users.find_department(name);
}
inline BaseUsers& Users::base_users(){
    return m_users;
}
*/