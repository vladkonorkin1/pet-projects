//=====================================================================================//
//   Author: open
//   Date:   19.02.2019
//=====================================================================================//
#include "base/precomp.h"
#include "convert_utility.h"

static QString g_datetime_convert_format = "yyyy-MM-dd hh:mm:ss.zzz";

QDateTime str_to_datetime(const QString& str)
{
	QDateTime datetime = QDateTime::fromString(str, g_datetime_convert_format);// g_datetime_convert_format);
	datetime.setTimeSpec(Qt::UTC);
	//QString local = datetime.toLocalTime().toString(g_datetime_convert_format);
	//QString utc = datetime.toString(g_datetime_convert_format);

	return datetime;
}
QString datetime_to_str(const QDateTime& datetime)
{
	assert( datetime.timeSpec() == Qt::UTC );
	return datetime.toString(g_datetime_convert_format);
}
QString timestamp_to_str(Timestamp timestamp)
{
	return datetime_to_str(QDateTime::fromMSecsSinceEpoch(timestamp, Qt::UTC));
}