//=====================================================================================//
//   Author: open
//   Date:   19.02.2019
//=====================================================================================//
#pragma once
#include <QDateTime>

extern QDateTime str_to_datetime(const QString& str);
extern Timestamp str_to_timestamp(const QString& str);
extern QString timestamp_to_str(Timestamp timestamp);
extern QString datetime_to_str(const QDateTime& datetime);


inline Timestamp str_to_timestamp(const QString& str) {
	return str_to_datetime(str).toMSecsSinceEpoch();
}