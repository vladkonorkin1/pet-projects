#include "base/precomp.h"
#include "cryptography.h"
#include <QDebug>

#include <openssl/bio.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/aes.h>
#include <openssl/idea.h>

void rsa_free_mem(BIO** bio, RSA** rsa)
{
	RSA_free(*rsa);
	*rsa = nullptr;
	BIO_free(*bio);
	*bio = nullptr;
}

void fill_buffer_to_random(char* buffer, const int size)
{
	srand(static_cast<unsigned int>(time(nullptr)));
	int rnd_int = rand();
	for (int i = 0; i < size; i++)
	{
		while (rand() == rnd_int)
		{
		}
		rnd_int = rand();
		// rand() generate values between 0 and INT_MAX, so first bit is always 0. Thats why I use only one byte from middle of rnd_int.
		*(buffer + i) = *(reinterpret_cast<char*>(&rnd_int) + 1);
	}
}

void Cryptography::rsa_encrypt(const QString& keyfile, const QString& text, QByteArray& encrypted_text)
{
	BIO* bio = BIO_new_file(keyfile.toStdString().c_str(), "r");
	if (!bio)
	{
		qDebug() << "BIO_new_file return nullptr";
		return;
	}
	RSA* rsa = PEM_read_bio_RSA_PUBKEY(bio, nullptr, nullptr, nullptr);
	if (!rsa)
	{
		BIO_free(bio);
		bio = nullptr;
		qDebug() << "PEM_read_bio_RSA_PUBKEY return nullptr";
		return;
	}
	const int BUF_SIZE = RSA_size(rsa);
	encrypted_text.resize(BUF_SIZE);
	QByteArray text_utf8 = text.toUtf8();
	if (RSA_public_encrypt(static_cast<int>(text_utf8.size()), reinterpret_cast<const unsigned char*>(text_utf8.data()), reinterpret_cast<unsigned char*>(encrypted_text.data()), rsa, RSA_PKCS1_PADDING) == -1)
	{
		rsa_free_mem(&bio, &rsa);
		qDebug() << "RSA_public_encrypt return -1";
		return;
	}

	//QByteArray tt = encrypted_text.toBase64();

	rsa_free_mem(&bio, &rsa);
}

// data must be aligned by AES_BLOCK_SIZE.
bool Cryptography::aes_encrypt(const QByteArray& key, const QByteArray& data, QByteArray& encrypted_data)
{
	if (data.size() % AES_BLOCK_SIZE)
		return false;
	encrypted_data.resize(data.size() + AES_BLOCK_SIZE);

	unsigned char iv[AES_BLOCK_SIZE];
	fill_buffer_to_random(reinterpret_cast<char*>(iv), AES_BLOCK_SIZE);
	memcpy(reinterpret_cast<void*>(encrypted_data.data()), reinterpret_cast<const void*>(iv), AES_BLOCK_SIZE);

	AES_KEY enc_key;
	AES_set_encrypt_key(reinterpret_cast<const unsigned char*>(key.constData()), key.size() * 8, &enc_key);
	AES_cbc_encrypt(reinterpret_cast<const unsigned char*>(data.constData()), reinterpret_cast<unsigned char*>(encrypted_data.data() + AES_BLOCK_SIZE), static_cast<size_t>(data.size()), &enc_key, iv, AES_ENCRYPT);
	return true;
}

bool Cryptography::aes_decrypt(const QByteArray& key, const QByteArray& encrypted_data, QByteArray& data)
{
	if (encrypted_data.size() % AES_BLOCK_SIZE)
		return false;
	data.resize(encrypted_data.size() - AES_BLOCK_SIZE);
	unsigned char iv[AES_BLOCK_SIZE];
	memcpy(reinterpret_cast<void*>(iv), reinterpret_cast<const void*>(encrypted_data.data()), AES_BLOCK_SIZE);

	AES_KEY dec_key;
	AES_set_decrypt_key(reinterpret_cast<const unsigned char*>(key.constData()), key.size() * 8, &dec_key);
	AES_cbc_encrypt(reinterpret_cast<const unsigned char*>(encrypted_data.constData() + AES_BLOCK_SIZE), reinterpret_cast<unsigned char*>(data.data()), static_cast<size_t>(data.size()), &dec_key, iv, AES_DECRYPT);
	return true;
}