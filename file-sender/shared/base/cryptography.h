#pragma once

class Cryptography
{
public:
	static void rsa_encrypt(const QString& keyfile, const QString& text, QByteArray& encrypted_text);
	static bool aes_encrypt(const QByteArray& key, const QByteArray& data, QByteArray& encrypted_data);
	static bool aes_decrypt(const QByteArray& key, const QByteArray& encrypted_data, QByteArray& data);
};
