//=====================================================================================//
//   Author: open
//   Date:   11.02.2017
//=====================================================================================//
#pragma once

#include <exception>
#include <string>

namespace Base
{

//=====================================================================================//
//                                   class Exception                                   //
//=====================================================================================//
class Exception : public std::exception
{
private:

	std::string m_msg;			// строка, с сообщением об ошибке

public:
	Exception();
	Exception(const char* msg);
	Exception(const std::string& msg);
	Exception(const exception& another);
	virtual ~Exception();
public:
	///	получить строку с сообщением об ошибке
	virtual const char* what() const throw();

private:
	///	выводит сообщение об ошибке в отладочный поток
	void writeDebugOut() const;

};

inline Exception::Exception() : m_msg("Unknown error")
{
	writeDebugOut();
}

inline Exception::Exception(const char* msg) : m_msg(msg)
{
	writeDebugOut();
}

inline Exception::Exception(const std::string& msg) : m_msg(msg)
{
	writeDebugOut();
}

inline Exception::Exception(const std::exception& another) : m_msg(another.what())
{
}

inline Exception::~Exception()
{
}
//	получить строку с сообщением об ошибке
inline const char* Exception::what() const throw()
{
	return m_msg.c_str();
}
//	выводит сообщение об ошибке в отладочный поток
inline void Exception::writeDebugOut() const
{
}

}

/// —генерировать новый тип исключени¤.
#define BASE_MAKE_EXCEPTION(Name,Parent)									\
class Name : public Parent													\
{																			\
public:																		\
	Name() {}																\
	Name(const char* msg) : Parent(msg) {}									\
	Name(const std::string& msg) : Parent(msg) {}							\
	Name(const Name& another) : Parent(another) {}							\
}