//=====================================================================================//
//   Author: open
//   Date:   08.09.2017
//=====================================================================================//
#include "base/precomp.h"
#include "log.h"
#include <QCoreApplication>
#include <QTextStream>
#include <QFile>
#include <QDir>

#ifdef Q_OS_WIN
#include <Windows.h>
#endif

static std::string colors[16] =
{
	"05;232",				//BLACK 
	"05;18",				//NAVY
	"05;28",				//GREEN
	"05;30",				//TEAL
	"05;88",				//MAROON
	"05;90",				//PURPLE
	"05;100",				//OLIVE
	"05;251",				//SILVER
	"05;244",				//GRAY
	"05;21",				//BLUE
	"05;46",				//LIME
	"05;51",				//AQUA
	"05;196",				//RED
	"05;201",				//FUCHSIA
	"05;226",				//YELLOW
	"05;15"                                 //WHITE
};

class FileLogOutput : public LogOutput
{
private:
	QFile m_file;
	QTextStream m_stream;

public:
	FileLogOutput()
	: m_file(build_path())
	{
		if (m_file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
		{
			m_stream.setDevice(&m_file);
			m_stream.setCodec("UTF-8");
		}
	}
	virtual void msg(const QString& str, const QDateTime& datetime)
	{
		if (m_file.isOpen())
		{
			QString printed_str = datetime.toString("[dd.MM.yyyy hh:mm:ss] %1").arg(str);
			m_stream << printed_str.toUtf8();
			m_stream << '\n';
			m_stream.flush();
		}
	}

private:
	QString build_path() const
	{
		const QString log_folder("logs");
		QDir().mkdir(log_folder);
		return log_folder + '/' + QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss") + " " + QCoreApplication::applicationName() + ".log";
	}
};


/*class TestFileLogOutput : public LogOutput
{
private:
	QFile m_file;
	QTextStream m_stream;

public:
	TestFileLogOutput() : m_file(build_path())
	{
		if (m_file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
		{
			m_stream.setDevice(&m_file);
			m_stream.setCodec("UTF-8");
		}
	}
	virtual void msg(const QString& str, const QDateTime& datetime)
	{
		if (m_file.isOpen())
		{
			//QString printed_str = datetime.toString("[dd.MM.yyyy hh:mm:ss] %1").arg(str);
			m_stream << str.toUtf8();
			m_stream << '\n';
			m_stream.flush();
		}
	}

private:
	QString build_path() const
	{
		const QString log_folder("test_logs");
		QDir().mkdir(log_folder);
		return log_folder + '/' + QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss") + " " + QCoreApplication::applicationName() + ".log";
	}
};*/


class StdCoutLogOutput : public LogOutput
{
private:
	QDateTime m_start_time;
#ifdef Q_OS_WIN
	HANDLE m_hstdout;
#endif Q_OS_WIN

public:
	StdCoutLogOutput()
	: m_start_time(QDateTime::currentDateTime())
#ifdef Q_OS_WIN
	, m_hstdout(GetStdHandle(STD_OUTPUT_HANDLE))
#endif Q_OS_WIN
	{
		//QTextCodec::setCodecForLocale(QTextCodec::codecForName("IBM 866"));
	}

	virtual void msg(const QString& str, const QDateTime& datetime)
	{
		quint64 msec = m_start_time.msecsTo(datetime);

		//QString printed_str = QString().sprintf("[%1 %.2f] %2", msec/1000.).arg(datetime.toString("hh:mm:ss")).arg(str);
		//set_color(Log::Color::Black, Log::Color::Red);
		//std::cout << qPrintable(printed_str) << std::endl;



		QString stime = datetime.toString("hh:mm:ss ");
		set_color(Log::Color::Gray);
		std::cout << qPrintable(stime);

		set_color(Log::Color::Silver, Log::Color::Gray);
		QString smsec;
		smsec = smsec.sprintf("%.2f", msec / 1000.);
		std::cout << qPrintable(smsec);

        set_color(Log::Color::Black, Log::Color::Black);
		std::cout << " ";

		set_color(Log::Color::Red);

		std::cout << qPrintable(str) << std::endl;

        set_color(Log::Color::Lime);
	}
private:
    void set_color(Log::Color foreground_color, Log::Color background_color = Log::Color::Black)
	{
#ifdef Q_OS_WIN
		int back = static_cast<int>(background_color);
		int fore = static_cast<int>(foreground_color);
		WORD color = (back << 4) | fore;
		SetConsoleTextAttribute(m_hstdout, color);
#endif
#ifdef Q_OS_UNIX
		int back = static_cast<int>(background_color);
		int fore = static_cast<int>(foreground_color);
        if (background_color == Log::Color::Black)
            std::cout<<"\033[01;38;"<<colors[fore]<<";49m"; //49m - default background
        else
        {
            std::cout<<"\033[01;38;"<<colors[fore]<<";48;"<<colors[back]<<"m";
        }
#endif Q_OS_UNIX
	}
};

Log::Log(bool print_datetime)
: m_print_datetime(print_datetime)
{
//#ifdef Q_OS_WIN
	add_output(ULogOutput(new FileLogOutput()));
	//add_output(ULogOutput(new TestFileLogOutput()));
//#endif
	add_output(ULogOutput(new StdCoutLogOutput()));
}
void Log::msg(const QString& str)
{
	QMutexLocker locker(&m_mutex);
	QDateTime datetime = QDateTime::currentDateTime();
	for (auto& output : m_outputs)
		output->msg(str, datetime);
}
/*QString Log::format_message(const QString& str) const
{
	if (m_print_datetime) return QDateTime::currentDateTime().toString("[dd.MM.yyyy hh:mm:ss] %1").arg(str);
	else {
		quint64 msec = m_start_time.msecsTo(QDateTime::currentDateTime());
		return QString().sprintf("[%.2f] %1", msec / 1000.).arg(str);
	}
}*/
void Log::add_output(ULogOutput output) {
	m_outputs.push_back(std::move(output));
}
#include "base/console.h"
void Log::init()
{
#ifdef Q_OS_WIN
//#ifdef _DEBUG
	//SetConsoleWindow(150, 600);
//#endif
#endif

	//s_log.reset(new Log(true));
}


SingleLog::ULog SingleLog::s_log;

void SingleLog::msg(const QString& str)
{
	if (!s_log.get()) s_log.reset(new Log(true));
	s_log->msg(str);
}
