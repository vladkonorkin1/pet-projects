//=====================================================================================//
//   Author: open
//   Date:   08.09.2017
//=====================================================================================//
#pragma once
#include <QDateTime>
#include <QMutex>

class LogOutput
{
public:
	virtual void msg(const QString& str, const QDateTime& datetime) = 0;
};

class Log
{
public:
	// WIN: http://www.wideskills.com/html/html-color-names UNIX: http://terminal-color-builder.mudasobwa.ru/
	enum class Color : unsigned char
	{
		Black,	// WIN:0x0 	UNIX:05;232
		Navy,	// WIN:0x1	UNIX:05;18
		Green,	// WIN:0x2	UNIX:05;28
		Teal,	// WIN:0x3	UNIX:05;30
		Maroon,	// WIN:0x4	UNIX:05;88
		Purple,	// WIN:0x5	UNIX:05;90
		Olive,	// WIN:0x6	UNIX:05;100
		Silver,	// WIN:0x7	UNIX:05;251
		Gray,	// WIN:0x8	UNIX:05;244
		Blue,	// WIN:0x9	UNIX:05;21
		Lime,	// WIN:0xA	UNIX:05;46
		Aqua,	// WIN:0xB	UNIX:05;51
		Red,	// WIN:0xC	UNIX:05;196
		Fuchsia,// WIN:0xD	UNIX:05;201
		Yellow,	// WIN:0xE	UNIX:05;226
		White	// WIN:0xF	UNIX:05;15
	};

	enum class Level
	{
		Debug,
		Info,
		Warning,
		Critical
	};

private:
	using ULogOutput = std::unique_ptr<LogOutput>;
	std::vector<ULogOutput> m_outputs;
	QMutex m_mutex;
	bool m_print_datetime;

public:
	Log(bool print_datetime);
	virtual ~Log() {}
	void msg(const QString& str);
	static void init();

private:
	//QString format_message(const QString& str) const;
	void add_output(ULogOutput output);
};

class SingleLog
{
private:
	using ULog = std::unique_ptr<Log>;
	static ULog s_log;

public:
	static void msg(const QString& str);
};

#define logs(x) SingleLog::msg(x)