#include "single_run.h"
#include <QCoreApplication>
#include <QLocalSocket>
#include <QDataStream>
#include <QTime>
#include <QDir>

#if defined(Q_OS_WIN)
#include <QLibrary>
#include <qt_windows.h>
typedef BOOL(WINAPI*PProcessIdToSessionId)(DWORD, DWORD*);
static PProcessIdToSessionId pProcessIdToSessionId = 0;
#endif
#if defined(Q_OS_UNIX)
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#endif

SingleRun::SingleRun()
: m_id(build_id())
, m_lock_file(lock_file_path())
{
}
QString SingleRun::build_id() const
{
	QString app_name = QCoreApplication::applicationFilePath().section(QLatin1Char('/'), -1);
	QString id = app_name.section(QLatin1Char('.'), 0, 0);
	QByteArray idc = id.toUtf8();
	quint16 idNum = qChecksum(idc.constData(), idc.size());
	id += QLatin1Char('-') + QString::number(idNum, 16);
	return id;
}
QString SingleRun::lock_file_path() const {
	return QDir::tempPath() + QLatin1Char('/') + m_id + "-lock";
}
QString SingleRun::get_socket_name() const
{
#if defined(Q_OS_WIN)
	if (!pProcessIdToSessionId) {
		QLibrary lib("kernel32");
		pProcessIdToSessionId = (PProcessIdToSessionId)lib.resolve("ProcessIdToSessionId");
	}
	DWORD sessionId = 0;
	if (pProcessIdToSessionId) {
		pProcessIdToSessionId(GetCurrentProcessId(), &sessionId);
	}
	return m_id + QLatin1Char('-') + QString::number(sessionId, 16);
#else
	return m_id + QLatin1Char('-') + QString::number(::getuid(), 16);
#endif
}

QString SingleRun::s_msg_activate = "+";
bool SingleRun::is_run()
{
	if (m_lock_file.tryLock(1000))
	{
		// server
		m_server.reset(new QLocalServer());
		bool res = m_server->listen(get_socket_name());
		if (!res) qWarning("listen on local socket failed, %s", qPrintable(m_server->errorString()));
		QObject::connect(m_server.get(), SIGNAL(newConnection()), SLOT(slot_receive_connection()));
		return false;
	}

	// client
	return send_message(s_msg_activate, 5000);
}
bool SingleRun::send_message(const QString &message, int timeout)
{
	QLocalSocket socket;
	bool connOk = false;
	for (int i = 0; i < 2; i++) {
		// Try twice, in case the other instance is just starting up
		socket.connectToServer(get_socket_name());
		connOk = socket.waitForConnected(timeout / 2);
		if (connOk || i)
			break;
		int ms = 250;
#if defined(Q_OS_WIN)
		Sleep(DWORD(ms));
#else
		struct timespec ts = { ms / 1000, (ms % 1000) * 1000 * 1000 };
		nanosleep(&ts, NULL);
#endif
	}
	if (!connOk)
		return false;

	QByteArray uMsg(message.toUtf8());
	QDataStream ds(&socket);
	ds.writeBytes(uMsg.constData(), uMsg.size());
	bool res = socket.waitForBytesWritten(timeout);
	/*if (res) {
		res &= socket.waitForReadyRead(timeout);   // wait for ack
		if (res)
			res &= (socket.read(qstrlen(ack)) == ack);
	}*/
	socket.waitForDisconnected(1000);
	return res;
}


void SingleRun::slot_receive_connection()
{
	using ULocalSocket = std::unique_ptr<QLocalSocket>;
	ULocalSocket socket(m_server->nextPendingConnection());
	if (!socket.get())
		return;

	while (true)
	{
		if (socket->state() == QLocalSocket::UnconnectedState) {
			qWarning("Peer disconnected");
			return;
		}
		if (socket->bytesAvailable() >= qint64(sizeof(quint32)))
			break;
		socket->waitForReadyRead();
	}

	QDataStream ds(socket.get());
	QByteArray uMsg;
	quint32 remaining;
	ds >> remaining;
	uMsg.resize(remaining);
	int got = 0;
	char* uMsgBuf = uMsg.data();
	do {
		got = ds.readRawData(uMsgBuf, remaining);
		remaining -= got;
		uMsgBuf += got;
	} while (remaining && got >= 0 && socket->waitForReadyRead(3000));
	/*if (got < 0) {
		qWarning("QtLocalPeer: Message reception failed %s", socket->errorString().toLatin1().constData());
		delete socket;
		return;
	}*/
	QString message(QString::fromUtf8(uMsg));
	/*socket->write(ack, qstrlen(ack));
	socket->waitForBytesWritten(1000);
	socket->waitForDisconnected(1000); // make sure client reads ack
	delete socket;*/
	//emit messageReceived(message); //### (might take a long time to return)
	if (message == s_msg_activate)
		emit activate();
}

/*#include <QWidget>
void SingleRun::activate_window(QWidget* window)
{
	window->setWindowState(window->windowState() & ~Qt::WindowMinimized);
	window->show();
	window->raise();
	window->activateWindow();
}*/