#pragma once
#include <QLocalServer>
#include <QLockFile>

class SingleRun : public QObject
{
	Q_OBJECT
private:
	QString m_id;
	QLockFile m_lock_file;
	using ULocalServer = std::unique_ptr<QLocalServer>;
	ULocalServer m_server;
	static QString s_msg_activate;

public:
	SingleRun();
	bool is_run();
	//static void activate_window(QWidget* window);

signals:
	void activate();
	//void message_received(const QString &message);

private slots:
	void slot_receive_connection();

private:
	QString build_id() const;
	QString lock_file_path() const;
	QString get_socket_name() const;
	bool send_message(const QString &message, int timeout);
};