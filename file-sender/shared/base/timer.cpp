#include "base/precomp.h"
#include "timer.h"

namespace Base
{

//=====================================================================================//
//                                     class Timer                                     //
//=====================================================================================//
Timer::Timer(float period)
{
	connect(&m_timer, SIGNAL(timeout()), this, SLOT(on_timeout()));
	m_timer.start(static_cast<int>(period*1000.f));
	m_time.start();
}
/// событие тайминга
void Timer::on_timeout()
{
	emit updated(static_cast<float>(m_time.restart())/1000.f);
}
}	// namespace Utility