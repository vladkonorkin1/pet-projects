#pragma once
#include <QTimer>
#include <QTime>

namespace Base
{

//=====================================================================================//
//                                     class Timer                                     //
//=====================================================================================//
class Timer : public QObject
{
public:
	Q_OBJECT

private:
	QTimer m_timer;
	QTime m_time;

public:
	Timer(float period);

signals:
	/// обновление
	void updated(float dt);

private slots:
	/// событие тайминга
	void on_timeout();
};	

}	// namespace Utility