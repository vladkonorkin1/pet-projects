#include "base/precomp.h"
#include "utility.h"
#include <QRegExp>
#include <QDir>

namespace Utility
{

QString correct_path(const QString& path)
{
	QString p(path);
	p.replace('\\', '/');
	return p;
}
void create_folder(const QString& path)
{
	QDir dir(path);
	if (!dir.exists())
		dir.mkdir(".");
}

QStringList split_path(const QString& path)
{
	return correct_path(path).split('/', QString::SplitBehavior::SkipEmptyParts);
}

QString filename_from_path(const QString& path)
{
	//QStringList l = path.split(QRegExp("[/\\\\]"), QString::SplitBehavior::SkipEmptyParts);
	QStringList l = split_path(path);
	if (!l.isEmpty())
	{
		QString p(l.back());
		p.remove(':');
		return p;
	}
	return path;
}
/*QString folder_from_path(const QString& path)
{
	//QStringList l = split_path(path);
	//if (l.size() > 1)
	//{
	//	return l.join('/');
	//}
	//return path;

	int index = path.lastIndexOf('/');
	if (index > 0)
	{
		return path.left(index);
	}
	return path;
}*/

#ifdef WIN32
#include <windows.h>
#include <lmcons.h>
#elif defined Q_WS_X11
#include <pwd.h>
#endif

QString get_user_name()
{
#ifdef WIN32
	WCHAR* lpszSystemInfo;
	DWORD cchBuff = UNLEN;
	WCHAR tchBuffer[UNLEN + 1];
	lpszSystemInfo = tchBuffer;
	GetUserNameW(lpszSystemInfo, &cchBuff);
	return QString::fromWCharArray(lpszSystemInfo);
#elif defined Q_WS_X11
	register struct passwd *pw;
	pw = getpwuid(getuid());
	if (pw)
		return pw->pw_name;
	return QString();
#endif //#ifdef Q_WS_WIN
}

//QString get_user_name2()
//{
//	#if defined(Q_OS_WIN)
//		return QString::fromLocal8Bit(qgetenv("USERNAME").constData()).toUtf8();
//	#elif defined(Q_OS_UNIX)
//		return qgetenv("USER").constData();
//	#else
//		return "";
//	#endif
//}

Timestamp current_timestamp()
{
	return QDateTime::currentDateTimeUtc().toMSecsSinceEpoch();
}

QByteArray unique_machine_login_id()
{
	return QSysInfo::machineUniqueId() + Utility::get_user_name().toUtf8();
}


}	// namespace Utility