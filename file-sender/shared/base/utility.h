#pragma once

namespace Utility
{

extern void create_folder(const QString& path);
extern QStringList split_path(const QString& path);
extern QString filename_from_path(const QString& path);
extern QString correct_path(const QString& path);
//extern QString folder_from_path(const QString& path);
extern QString get_user_name();
//extern QString get_user_name2();
extern Timestamp current_timestamp();
extern QByteArray unique_machine_login_id();

}	// namespace Utility