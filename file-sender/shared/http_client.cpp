//=====================================================================================//
//   Author: open
//   Date:   12.08.2019
//=====================================================================================//
#include "base/precomp.h"
#include "http_client.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QAuthenticator>
#include <QNetworkReply>

HttpClient::HttpClient()
{
	connect(&m_network_access_manager, SIGNAL(finished(QNetworkReply*)), SLOT(slot_download_finished(QNetworkReply*)));
	connect(&m_network_access_manager, SIGNAL(sslErrors(QNetworkReply*, const QList<QSslError>&)), SLOT(slot_ssl_errors(QNetworkReply*, const QList<QSslError>&)));
}
void HttpClient::slot_ssl_errors(QNetworkReply *reply, const QList<QSslError> &errors)
{
	for (const QSslError& error : errors)
		logs(QString("http client: ssl error url='%1' error='%2'").arg(reply->url().toString()).arg(error.errorString()));

	reply->ignoreSslErrors();
}
// установить заголовок для всех отправляемых запросов
void HttpClient::set_header(const QString& header, const QString& value)
{
	m_headers[header.toUtf8()] = value.toUtf8();
}
// отправить http-запрос
void HttpClient::request(SHttpRequest http_request, HttpMethod method)
{
	HttpRequest* p_http_request = http_request.get();
	m_requests[p_http_request] = http_request;
	
	QString surl = p_http_request->url();
	QNetworkRequest request(surl);
	request.setAttribute(QNetworkRequest::User, QVariant::fromValue<HttpRequest*>(p_http_request));

	for (const auto& pair : m_headers)
		request.setRawHeader(pair.first, pair.second);

	logs(QString("http client: request url='%1'").arg(surl));

	QNetworkReply* reply = nullptr;
	switch (method)
	{
		case HttpMethod::Post:
			reply = m_network_access_manager.post(request, p_http_request->send_buffer());
			break;
		case HttpMethod::Put:
			reply = m_network_access_manager.put(request, p_http_request->send_buffer());
			break;
		case HttpMethod::Delete:
			reply = m_network_access_manager.deleteResource(request);
			break;
		case HttpMethod::Patch:
			reply = m_network_access_manager.sendCustomRequest(request, "PATCH", p_http_request->send_buffer());
			break;
		case HttpMethod::Get:
		default:
			reply = m_network_access_manager.get(request);
	}
	p_http_request->set_reply(reply);
}
// событие ответа http-сервера
void HttpClient::slot_download_finished(QNetworkReply* reply)
{
	HttpRequest* p_http_request = reply->request().attribute(QNetworkRequest::User).value<HttpRequest*>();
	auto it = m_requests.find(p_http_request);
	if (it != m_requests.end())
	{
		if (!it->second.expired())
		{
			SHttpRequest http_request = it->second.lock();
			bool success = (reply->error() == QNetworkReply::NetworkError::NoError);
			if (!success)
			{
				int http_status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
				logs(QString("http request error: %1  status: %2  error: %3").arg(http_request->url()).arg(http_status).arg(reply->errorString()));
			}
			http_request->process(reply, success);
			http_request->set_reply(nullptr);
		}
		m_requests.erase(it);
		reply->deleteLater();
	}
}


