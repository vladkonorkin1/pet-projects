//=====================================================================================//
//   Author: open
//   Date:   12.08.2019
//=====================================================================================//
#pragma once
#include <QNetworkAccessManager>
#include "http_request.h"

class HttpClient : public QObject
{
	Q_OBJECT
private:
	QNetworkAccessManager m_network_access_manager;
	std::map<HttpRequest*, WHttpRequest> m_requests;

	using headers_t = std::map<QByteArray, QByteArray>;
	headers_t m_headers;

public:
	HttpClient();
	// отправить http-запрос
	void request(SHttpRequest http_request, HttpMethod method);
	// установить заголовок для всех отправляемых запросов
	void set_header(const QString& header, const QString& value);

private slots:
	// событие ответа http-сервера
	void slot_download_finished(QNetworkReply* reply);

	void slot_ssl_errors(QNetworkReply *reply, const QList<QSslError> &errors);
};
