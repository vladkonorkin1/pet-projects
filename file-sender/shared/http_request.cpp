//=====================================================================================//
//   Author: open
//   Date:   12.08.2019
//=====================================================================================//
#include "base/precomp.h"
#include "http_request.h"
#include <QNetworkReply>

//=====================================================================================
HttpRequest::HttpRequest()
: m_reply(nullptr)
{
}
HttpRequest::~HttpRequest()
{
	abort();
}
void HttpRequest::abort()
{
	if (m_reply && m_reply->isRunning())
		m_reply->abort();
}
void HttpRequest::set_reply(QNetworkReply* reply)
{
	m_reply = reply;
}

const QByteArray HttpRequest::send_buffer() const
{
	static QByteArray temp;
	return temp;
}

