//=====================================================================================//
//   Author: open
//   Date:   12.08.2019
//=====================================================================================//
#pragma once

class QNetworkReply;

enum class HttpMethod
{
	Get,
	Post,
	Put,
	Delete,
	Patch,
	MaxMethod
};

class HttpRequest : public QObject
{
	Q_OBJECT
private:
	QNetworkReply* m_reply;

public:
	HttpRequest();
	virtual ~HttpRequest();

	virtual QString url() const = 0;
	virtual void process(QNetworkReply* reply, bool success) = 0;

	void set_reply(QNetworkReply* reply);
	virtual const QByteArray send_buffer() const;

private:
	void abort();
};

using SHttpRequest = std::shared_ptr<HttpRequest>;
using WHttpRequest = std::weak_ptr<HttpRequest>;