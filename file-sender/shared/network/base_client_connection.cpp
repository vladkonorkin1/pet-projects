#include "base/precomp.h"
#include "base_client_connection.h"

const quint64 BaseClientConnection::m_block_size = 64 * 1024; // 64 KB

BaseClientConnection::BaseClientConnection(QTcpSocket* socket)
: m_socket(socket)
, m_write(false)
{
	connect(m_socket, SIGNAL(connected()), SLOT(slot_start_connection()));
	connect(m_socket, SIGNAL(readyRead()), SLOT(slot_received()));
	connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(slot_close_connection(QAbstractSocket::SocketError)));
	connect(m_socket, SIGNAL(bytesWritten(qint64)), SLOT(slot_update_write_transfer(qint64)));
	connect(&m_packet_reader, SIGNAL(data_received(const char*, unsigned int)), SLOT(slot_data_recieved(const char*, unsigned int)));
}
void BaseClientConnection::slot_update_write_transfer(qint64 /*numBytes*/)
{
	update_write();
}
void BaseClientConnection::update_write()
{
	if (!m_buffer.empty())
	{
		qint64 written = m_socket->write(m_buffer.data(), qMin<quint64>(m_buffer.size(), m_block_size));
		if (written > 0)
		{
			m_buffer.erase(m_buffer.begin(), m_buffer.begin() + written);
			if (m_buffer.empty())
				m_write = false;
		}
	}
}
void BaseClientConnection::write()
{
	if (!m_write)
	{
		m_write = true;
		update_write();
	}
}
void BaseClientConnection::slot_received()
{
	m_packet_reader.read(m_socket->readAll());
	//std::cout << m_socket->bytesAvailable() << endl;
}
void BaseClientConnection::slot_close_connection(QAbstractSocket::SocketError error)
{
	//if (error != QAbstractSocket::SocketError::RemoteHostClosedError)
	{
	//	std::cout << "close connection: " << error << std::endl;
		//int a = 0;
	}
	/*m_socket->close();
	//m_socket->deleteLater();
	emit disconnected();
	//m_socket->close();*/
	Q_UNUSED( error );
	close();
}
void BaseClientConnection::slot_start_connection()
{
	emit connected();
}
//	отослать сообщение серверу
void BaseClientConnection::send_message(const NetMessage& message)
{
	/*std::string data = message.save();
	Packet packet(data);
	m_socket->write(packet.data());*/

	std::string data = message.save();
	//Packet packet(data);

	PacketHeader header(static_cast<unsigned int>(data.size()));
	size_t cur = m_buffer.size();
	int header_size = sizeof(PacketHeader);
	m_buffer.resize(m_buffer.size() + header_size + data.size());
	memcpy(&m_buffer[cur], &header, header_size);
	memcpy(&m_buffer[cur] + header_size, data.c_str(), data.size());

	write();
}
void BaseClientConnection::slot_data_recieved(const char* data, unsigned int size)
{
	emit data_received(data, size);
}
//  закрыть соединение
void BaseClientConnection::close()
{
	m_socket->close();
	// нельзя удалять сокет! мы им не владеем...
	//delete m_socket;	m_socket->deleteLater(); 
	emit disconnected();
}