//=====================================================================================//
//   Author: open
//   Date:   29.03.2017
//=====================================================================================//
#pragma once
#include "network/packet_reader.h"
//#include "network/message_dispatcher.h"
#include "network/net_message.h"
#include <QTcpSocket>

class BaseClientConnection : public QObject
{
public:
	Q_OBJECT
private:
	static const quint64 m_block_size;
	using buffer_t = std::vector<char>;
	buffer_t m_buffer;
	QTcpSocket* m_socket;
	PacketReader m_packet_reader;
	bool m_write;

	//MessageDispatcher<HandlerMessages> m_message_dispatcher;

public:
	BaseClientConnection(QTcpSocket* socket);
	//	отослать сообщение серверу
	void send_message(const NetMessage& message);
	//  закрыть соединение
	void close();

signals:
	// клиент подключился к серверу
	void connected();
	void disconnected();
	void data_received(const char* data, unsigned int size);

private slots:
	void slot_received();
	void slot_start_connection();
	void slot_close_connection(QAbstractSocket::SocketError error);
	void slot_update_write_transfer(qint64 numBytes);
	void slot_data_recieved(const char* data, unsigned int size);

private:
	// записать в сеть из буфера
	void write();
	void update_write();
};