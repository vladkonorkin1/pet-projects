#pragma once
#include "network/net_messages.h"

class ClientHandlerMessages
{
public:
	virtual ~ClientHandlerMessages() {}
	virtual void on_message(const ServerMessageCheckProtocolVersionResponse&) {}
	virtual void on_message(const ServerMessageAutorizateResponse&) {}
	virtual void on_message(const ServerMessageInitializationFinish&) {}
	virtual void on_message(const ServerMessageDepartments&) {}
	virtual void on_message(const ServerMessageUsers&) {}
	virtual void on_message(const ServerMessageSetLoginUser&) {}
	virtual void on_message(const ServerMessageContactChat&) {}
	virtual void on_message(const ServerMessageGroupChat&) {}
	virtual void on_message(const ServerMessageGroupChatSetUsers&) {}
	virtual void on_message(const ServerMessageChatList&) {}
	virtual void on_message(const ServerMessageSyncResponse&) {}
	
	virtual void on_message(const ServerMessageChatAddFavorite&) {}
	virtual void on_message(const ServerMessageChatAddRecent&) {}
	virtual void on_message(const ServerMessageChatRemoveResponse&) {}
	
	virtual void on_message(const ServerMessageAliveResponse&) {}
	virtual void on_message(const ServerMessageContactChatCreateResponse&) {}
	virtual void on_message(const ServerMessageGroupChatCreateResponse&) {}

	virtual void on_message(const ServerMessageChatMessages&) {}
	virtual void on_message(const ServerMessageChatHistoryMessages&) {}
	
	virtual void on_message(const ServerMessageContactStatus&) {}
	virtual void on_message(const ServerMessageUsersStatus&) {}

	virtual void on_message(const ServerMessageChatNewMessageReadResponse&) {}
	virtual void on_message(const ServerMessageChatMessageRead&) {}
	
	virtual void on_message(const ServerMessageFileMessageProgress&) {}
	virtual void on_message(const ServerMessageFileTransferFinish&) {}
	virtual void on_message(const ServerMessageFileTransferWriteFileError&) {}
	virtual void on_message(const ServerMessageFileMessageCancelResponse&) {}

	virtual void on_message(const ServerMessageIsAccessClosedFolderResponse&) {}
};

class ServerHandlerMessages 
{
public:
	virtual ~ServerHandlerMessages() {}
	virtual void on_message(const ClientMessageCheckProtocolVersion&) {}
	virtual void on_message(const ClientMessageAutorizate&) {}
	virtual void on_message(const ClientMessageSync&) {}
	virtual void on_message(const ClientMessageAlive&) {}

	virtual void on_message(const ClientMessageTextMessageSend&) {}
	virtual void on_message(const ClientMessageContactChatCreate&) {}
	virtual void on_message(const ClientMessageChatRemove&) {}
	virtual void on_message(const ClientMessageChatMoveFavorite&) {}
	virtual void on_message(const ClientMessageChatRequestHistoryMessages&) {}
	virtual void on_message(const ClientMessageContactChatRequest&) {}
	virtual void on_message(const ClientMessageGroupChatCreate&) {}
	virtual void on_message(const ClientMessageGroupChatAddUsers&) {}
	virtual void on_message(const ClientMessageGroupChatRemoveUser&) {}
	virtual void on_message(const ClientMessageGroupSetOrRemoveAdmin&){}
	virtual void on_message(const ClientMessageGroupChatRename&) {}
	virtual void on_message(const ClientMessageChatNewMessageRead&) {}

	virtual void on_message(const ClientMessageUsersStatus&) {}

	virtual void on_message(const ClientMessageFileMessageSend&) {}
	virtual void on_message(const ClientMessageFileMessageRegister&) {}
	virtual void on_message(const ClientMessageFileMessageCancel&) {}
	virtual void on_message(const ClientMessageFileTransferStart&) {}
	virtual void on_message(const ClientMessageFileTransferFileBodyStart&) {}
	virtual void on_message(const ClientMessageFileTransferCancel&) {}
	virtual void on_message(const ClientMessageFileTransferFinish&) {}

	virtual void on_message(const ClientMessageIsAccessClosedFolder&) {}
};