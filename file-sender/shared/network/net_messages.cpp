#include "base/precomp.h"
#include "net_messages.h"

NET_MESSAGE_IMPL(ClientMessageCheckProtocolVersion);
NET_MESSAGE_IMPL(ServerMessageCheckProtocolVersionResponse);

NET_MESSAGE_IMPL(ClientMessageAutorizate);
NET_MESSAGE_IMPL(ServerMessageAutorizateResponse);
NET_MESSAGE_IMPL(ServerMessageInitializationFinish);

NET_MESSAGE_IMPL(ClientMessageSync);
NET_MESSAGE_IMPL(ServerMessageSyncResponse);
NET_MESSAGE_IMPL(ServerMessageDepartments);
NET_MESSAGE_IMPL(ServerMessageUsers);
NET_MESSAGE_IMPL(ServerMessageSetLoginUser);
NET_MESSAGE_IMPL(ServerMessageContactChat);
NET_MESSAGE_IMPL(ServerMessageGroupChat);
NET_MESSAGE_IMPL(ServerMessageGroupChatSetUsers);
NET_MESSAGE_IMPL(ServerMessageChatList);

NET_MESSAGE_IMPL(ClientMessageAlive);
NET_MESSAGE_IMPL(ServerMessageAliveResponse);

NET_MESSAGE_IMPL(ServerMessageChatAddFavorite);
NET_MESSAGE_IMPL(ServerMessageChatAddRecent);
NET_MESSAGE_IMPL(ClientMessageChatRemove);
NET_MESSAGE_IMPL(ServerMessageChatRemoveResponse);
NET_MESSAGE_IMPL(ClientMessageChatMoveFavorite);

NET_MESSAGE_IMPL(ClientMessageContactChatRequest);
NET_MESSAGE_IMPL(ClientMessageContactChatCreate);
NET_MESSAGE_IMPL(ServerMessageContactChatCreateResponse);

NET_MESSAGE_IMPL(ClientMessageGroupChatCreate);
NET_MESSAGE_IMPL(ServerMessageGroupChatCreateResponse);
NET_MESSAGE_IMPL(ClientMessageGroupChatAddUsers);
NET_MESSAGE_IMPL(ClientMessageGroupChatRemoveUser);
NET_MESSAGE_IMPL(ClientMessageGroupSetOrRemoveAdmin);
NET_MESSAGE_IMPL(ClientMessageGroupChatRename);

NET_MESSAGE_IMPL(ServerMessageContactStatus);
NET_MESSAGE_IMPL(ClientMessageUsersStatus);
NET_MESSAGE_IMPL(ServerMessageUsersStatus);

NET_MESSAGE_IMPL(ClientMessageTextMessageSend);
NET_MESSAGE_IMPL(ServerMessageChatMessages);
NET_MESSAGE_IMPL(ClientMessageChatRequestHistoryMessages);
NET_MESSAGE_IMPL(ServerMessageChatHistoryMessages);

NET_MESSAGE_IMPL(ClientMessageChatNewMessageRead);
NET_MESSAGE_IMPL(ServerMessageChatNewMessageReadResponse);
NET_MESSAGE_IMPL(ServerMessageChatMessageRead);

NET_MESSAGE_IMPL(ClientMessageFileMessageSend);
NET_MESSAGE_IMPL(ClientMessageFileMessageRegister);
NET_MESSAGE_IMPL(ClientMessageFileMessageCancel);
NET_MESSAGE_IMPL(ServerMessageFileMessageCancelResponse);
NET_MESSAGE_IMPL(ServerMessageFileMessageProgress);

NET_MESSAGE_IMPL(ClientMessageFileTransferStart);
NET_MESSAGE_IMPL(ClientMessageFileTransferFileBodyStart);
NET_MESSAGE_IMPL(ClientMessageFileTransferCancel);
NET_MESSAGE_IMPL(ServerMessageFileTransferWriteFileError);
NET_MESSAGE_IMPL(ServerMessageFileTransferFinish);
NET_MESSAGE_IMPL(ClientMessageFileTransferFinish);

NET_MESSAGE_IMPL(ClientMessageIsAccessClosedFolder);
NET_MESSAGE_IMPL(ServerMessageIsAccessClosedFolderResponse);


Base::OBinStream& operator << (Base::OBinStream& stream, const std::vector<UserId>& users)
{
	stream << static_cast<UserId>(users.size());
	for (auto user_id : users)
		stream << user_id;
	return stream;
}
Base::IBinStream& operator >> (Base::IBinStream& stream, std::vector<UserId>& users)
{
	UserId size;
	stream >> size;
	for (UserId i = 0; i < size; ++i) {
		UserId id;
		stream >> id;
		users.push_back(id);
	}
	return stream;
}
Base::OBinStream& operator << (Base::OBinStream& stream, FileTransferResult result)
{
	stream << static_cast<unsigned char>(result);
	return stream;
}
Base::IBinStream& operator >> (Base::IBinStream& stream, FileTransferResult& result)
{
	unsigned char value;
	stream >> value;
	result = static_cast<FileTransferResult>(value);
	return stream;
}




void ClientMessageTextMessageSend::read(Base::IBinStream& stream) {
	stream >> chat_id;
	stream >> local_id;
	stream >> text;
}
void ClientMessageTextMessageSend::write(Base::OBinStream& stream) const {
	stream << chat_id;
	stream << local_id;
	stream << text;
}

void ClientMessageFileMessageSend::read(Base::IBinStream& stream) {
	stream >> chat_id;
	stream >> local_id;
	stream >> src_path;
	stream >> is_dir;
	stream >> size;
}
void ClientMessageFileMessageSend::write(Base::OBinStream& stream) const {
	stream << chat_id;
	stream << local_id;
	stream << src_path;
	stream << is_dir;
	stream << size;
}

/*void ServerMessageFileMessageStatus::read(Base::IBinStream& stream) {
	stream >> message_id;
	stream >> result;
	stream >> fullpath;
}
void ServerMessageFileMessageStatus::write(Base::OBinStream& stream) const {
	stream << message_id;
	stream << result;
	stream << fullpath;
}*/

Base::OBinStream& operator<<(Base::OBinStream& stream, ChatSearchType type)
{
	stream << static_cast<unsigned char>(type);
	return stream;
}
Base::IBinStream& operator>>(Base::IBinStream& stream, ChatSearchType& type)
{
	unsigned char temp;
	stream >> temp;
	type = static_cast<ChatSearchType>(temp);
	return stream;
}

void ClientMessageGroupChatCreate::read(Base::IBinStream& stream) {
	stream >> user_ids;
}
void ClientMessageGroupChatCreate::write(Base::OBinStream& stream) const {
	stream << user_ids;
}

void ServerMessageGroupChatSetUsers::read(Base::IBinStream& stream) {
	stream >> chat_id;
	stream >> timestamp;
	stream >> users;
	stream >> admins;
}
void ServerMessageGroupChatSetUsers::write(Base::OBinStream& stream) const {
	stream << chat_id;
	stream << timestamp;
	stream << users;
	stream << admins;
}
void ClientMessageGroupChatAddUsers::read(Base::IBinStream& stream) {
	stream >> chat_id;
	stream >> user_ids;
}
void ClientMessageGroupChatAddUsers::write(Base::OBinStream& stream) const {
	stream << chat_id;
	stream << user_ids;
}
void ClientMessageGroupChatRemoveUser::read(Base::IBinStream& stream) {
	stream >> chat_id;
	stream >> user_id;
}
void ClientMessageGroupChatRemoveUser::write(Base::OBinStream& stream) const {
	stream << chat_id;
	stream << user_id;
}

void ServerMessageContactChat::read(Base::IBinStream& stream) {
	stream >> chat_id;
	stream >> contact_id;
	stream >> timestamp;
	stream >> last_message_id;
	stream >> last_message_stamp;
}
void ServerMessageContactChat::write(Base::OBinStream& stream) const {
	stream << chat_id;
	stream << contact_id;
	stream << timestamp;
	stream << last_message_id;
	stream << last_message_stamp;
}
void ServerMessageGroupChat::read(Base::IBinStream& stream) {
	stream >> chat_id;
	stream >> name;
	stream >> timestamp;
	stream >> last_message_id;
	stream >> last_message_stamp;
}
void ServerMessageGroupChat::write(Base::OBinStream& stream) const {
	stream << chat_id;
	stream << name;
	stream << timestamp;
	stream << last_message_id;
	stream << last_message_stamp;
}

Base::OBinStream& operator<<(Base::OBinStream& stream, AutorizateResult result)
{
	stream << static_cast<unsigned char>(result);
	return stream;
}
Base::IBinStream& operator>>(Base::IBinStream& stream, AutorizateResult& result)
{
	unsigned char temp;
	stream >> temp;
	result = static_cast<AutorizateResult>(temp);
	return stream;
}
void ServerMessageAutorizateResponse::read(Base::IBinStream& stream) {
	stream >> auth_result;
	stream >> connection_id;
}
void ServerMessageAutorizateResponse::write(Base::OBinStream& stream) const {
	stream << auth_result;
	stream << connection_id;
}


Base::OBinStream& operator<<(Base::OBinStream& stream, HistoryStatus history_status)
{
	stream << static_cast<unsigned char>(history_status);
	return stream;
}
Base::IBinStream& operator>>(Base::IBinStream& stream, HistoryStatus& history_status)
{
	unsigned char temp;
	stream >> temp;
	history_status = static_cast<HistoryStatus>(temp);
	return stream;
}

Base::OBinStream& operator<<(Base::OBinStream& stream, ChatMessageType type)
{
	stream << static_cast<unsigned char>(type);
	return stream;
}
Base::IBinStream& operator>>(Base::IBinStream& stream, ChatMessageType& type)
{
	unsigned char temp;
	stream >> temp;
	type = static_cast<ChatMessageType>(temp);
	return stream;
}

Base::OBinStream& operator<<(Base::OBinStream& stream, ChatMessageStatus status)
{
	stream << static_cast<unsigned char>(status);
	return stream;
}
Base::IBinStream& operator>>(Base::IBinStream& stream, ChatMessageStatus& status)
{
	unsigned char temp;
	stream >> temp;
	status = static_cast<ChatMessageStatus>(temp);
	return stream;
}

Base::IBinStream& operator>>(Base::IBinStream& stream, std::vector<DescChatMessage>& messages)
//void read_messages(Base::IBinStream& stream, std::vector<DescChatMessage>& messages)
{
	unsigned int size = 0;
	stream >> size;
	messages.reserve(size);
	for (unsigned int i = 0; i < size; ++i)
	{
		DescChatMessage msg;
		stream >> msg.id;
		stream >> msg.chat_id;
		stream >> msg.sender_id;
		stream >> msg.stamp;
		stream >> msg.type;
		stream >> msg.status;
		stream >> msg.history_status;
		stream >> msg.datetime;
		stream >> msg.datetime_readed;
		stream >> msg.historical;
		stream >> msg.local_id;

		switch (msg.type)
		{
			case ChatMessageType::Text:
			{
				stream >> msg.text;
				break;
			}
			case ChatMessageType::File:
			{
				stream >> msg.file_src_path;
				stream >> msg.file_dest_name;
				stream >> msg.file_size;
				stream >> msg.file_is_dir;
				stream >> msg.file_status;
				break;
			}
			default:
				assert(0);
		}
		messages.push_back(msg);
	}
	return stream;
}
Base::OBinStream& operator<<(Base::OBinStream& stream, const std::vector<DescChatMessage>& messages)
//void write_messages(Base::OBinStream& stream, const std::vector<DescChatMessage>& messages)
{
	stream << static_cast<unsigned int>(messages.size());
	for (const DescChatMessage& msg : messages)
	{
		stream << msg.id;
		stream << msg.chat_id;
		stream << msg.sender_id;
		stream << msg.stamp;
		stream << msg.type;
		stream << msg.status;
		stream << msg.history_status;
		stream << msg.datetime;
		stream << msg.datetime_readed;
		stream << msg.historical;
		stream << msg.local_id;
		switch (msg.type)
		{
			case ChatMessageType::Text:
			{
				stream << msg.text;
				break;
			}
			case ChatMessageType::File:
			{
				stream << msg.file_src_path;
				stream << msg.file_dest_name;
				stream << msg.file_size;
				stream << msg.file_is_dir;
				stream << msg.file_status;
				break;
			}
			default:
				assert(0);
		}
	}
	return stream;
}

void ServerMessageChatMessages::read(Base::IBinStream& stream)
{
	stream >> messages;
}
void ServerMessageChatMessages::write(Base::OBinStream& stream) const
{
	stream << messages;
}

void ServerMessageChatHistoryMessages::read(Base::IBinStream& stream)
{
	stream >> chat_id;
	stream >> is_finish_upload;
	stream >> messages;
}
void ServerMessageChatHistoryMessages::write(Base::OBinStream& stream) const
{
	stream << chat_id;
	stream << is_finish_upload;
	stream << messages;
}

void ServerMessageFileTransferFinish::read(Base::IBinStream& stream)
{
	stream >> result;
	stream >> dest_name;
}
void ServerMessageFileTransferFinish::write(Base::OBinStream& stream) const
{
	stream << result;
	stream << dest_name;
}