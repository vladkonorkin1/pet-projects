#pragma once
#include "net_message.h"
#include "types.h"

enum class MessageProtocolId
{
	Empty = 0,

	// ===================================================================
	// проверка версии
	ClientMessageCheckProtocolVersion,
	ServerMessageCheckProtocolVersionResponse,

	// ===================================================================
	// авторизация
	ClientMessageAutorizate,
	ServerMessageAutorizateResponse,
	// завершение инициализации соединения (подгрузка данных из базы) и приём всех остальных сетевых сообщений
	ServerMessageInitializationFinish,

	// ===================================================================
	// синхронизация
	ClientMessageSync,
	ServerMessageSyncResponse,

	ServerMessageDepartments,					// обновление информации о департаментах
	ServerMessageUsers,							// обновление информации о пользователях
	ServerMessageSetLoginUser,					// установить под каким пользователем мы подключились
	ServerMessageContactChat,					// отправить контакт-чат клиенту
	ServerMessageGroupChat,						// отправить групповой чат клиенту
	ServerMessageGroupChatSetUsers,				// назначение участников чата
	ServerMessageChatList,						// отправить клиенту чат-лист

	// ===================================================================
	// обмен пингом между сервером и клиентом
	ClientMessageAlive,							// сообщить серверу "я жив!!!" (иначе дроп соединения)
	ServerMessageAliveResponse,					// и сервер отвечает "понял, принял, записал" (иначе дроп соединения)

	// чаты
	ServerMessageChatAddFavorite,				// добавить в избранное
	ServerMessageChatAddRecent,					// добавить в 'последние'
	ClientMessageChatRemove,					// клиент просит удалить чат из списка
	ServerMessageChatRemoveResponse,			// удалить чат из списка
	ClientMessageChatMoveFavorite,				// запрос на перенос чата в избранное (или в недавние)

	ClientMessageContactChatRequest,			// запросить отправку клиенту контакт-чата, если он когда то был создан (и даже если удален)
	ClientMessageContactChatCreate,				// запросить создание приватного клиентского чата
	ServerMessageContactChatCreateResponse,
	
	ClientMessageGroupChatCreate,				// клиент просит сервер создать групповой чат
	ServerMessageGroupChatCreateResponse,
	ClientMessageGroupChatAddUsers,				// клиент пожелал добавить к чату участников
	ClientMessageGroupChatRemoveUser,			// клиент пожелал удалить участника чата
	ClientMessageGroupSetOrRemoveAdmin,			// клиент пожелал удалить или назначит администратора чата
	ClientMessageGroupChatRename,				// клиент просит переименовать групповой чат

	// ===================================================================
	ServerMessageContactStatus,					// текущий статус человека (в сети или нет) из списка контактов
	ClientMessageUsersStatus,					// запрос на получение статусов списка людей
	ServerMessageUsersStatus,					// статус списка людей 

	// передача текстового сообщения
	ClientMessageTextMessageSend,				// отправка нового текстового сообщения
	ServerMessageChatMessages,					// получение сообщений чата
	ClientMessageChatRequestHistoryMessages,	// запросить сообщения из истории
	ServerMessageChatHistoryMessages,			// получение исторических сообщений
	

	ClientMessageChatNewMessageRead,			// клиент сообщает серверу, что увидел эти сообщения и они должны считаться прочитанными
	ServerMessageChatNewMessageReadResponse,
	ServerMessageChatMessageRead,				// сервер оповещает клиентов, что новые сообщения прочитаны

	// передача файлов
	ClientMessageFileMessageSend,				//
	ClientMessageFileMessageRegister,			// регистрация после переподключения (для сброса состояния передачи)
	ClientMessageFileMessageCancel,				// отмена файлового сообщения
	ServerMessageFileMessageCancelResponse,
	ServerMessageFileMessageProgress,			// прогресс передачи файлового сообщения

	ClientMessageFileTransferStart,				// начало передачи файла/папки
	ClientMessageFileTransferFileBodyStart,		// передача тела файла в файловый сокет
	ClientMessageFileTransferCancel,			// отмена передачи
	ServerMessageFileTransferWriteFileError,	// сообщение клиенту об ошибке записи файла при передаче
	ServerMessageFileTransferFinish,			// ответ от сервера о приёме файла
	ClientMessageFileTransferFinish,

	ClientMessageIsAccessClosedFolder,
	ServerMessageIsAccessClosedFolderResponse,
	
	MessageProtocolMax,
};


struct ClientMessageCheckProtocolVersion : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageCheckProtocolVersion);
	ClientMessageCheckProtocolVersion(int protocol_version) : protocol_version(protocol_version) {}
	int protocol_version = 0;

	void read(Base::IBinStream& stream) {
		stream >> protocol_version;
	}
	void write(Base::OBinStream& stream) const {
		stream << protocol_version;
	}
};
NET_MESSAGE_STREAM(ClientMessageCheckProtocolVersion);

struct ServerMessageCheckProtocolVersionResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageCheckProtocolVersionResponse);
	ServerMessageCheckProtocolVersionResponse(bool success, Timestamp database_timestamp) : success(success), database_timestamp(database_timestamp) {}
	bool success = false;
	Timestamp database_timestamp = 0;

	void read(Base::IBinStream& stream)
	{
		stream >> success;
		stream >> database_timestamp;
	}
	void write(Base::OBinStream& stream) const
	{
		stream << success;
		stream << database_timestamp;
	}
};
NET_MESSAGE_STREAM(ServerMessageCheckProtocolVersionResponse);


struct ClientMessageAutorizate : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageAutorizate);
	ClientMessageAutorizate(const QString& login, const QByteArray& encrypted_password) : login(login), encrypted_password(encrypted_password) {}

	QString login;
	QByteArray encrypted_password;

	void read(Base::IBinStream& stream) {
		stream >> login;
		stream >> encrypted_password;
	}
	void write(Base::OBinStream& stream) const {
		stream << login;
		stream << encrypted_password;
	}
};
NET_MESSAGE_STREAM(ClientMessageAutorizate);


struct ServerMessageAutorizateResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageAutorizateResponse);
	ServerMessageAutorizateResponse(AutorizateResult auth_result, ConnectionId connection_id) : auth_result(auth_result), connection_id(connection_id) {}

	AutorizateResult auth_result = AutorizateResult::Null;
	ConnectionId connection_id = 0;

	void read(Base::IBinStream& stream);
	void write(Base::OBinStream& stream) const;
};
NET_MESSAGE_STREAM(ServerMessageAutorizateResponse);


struct ServerMessageInitializationFinish : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageInitializationFinish);

	void read(Base::IBinStream&) {}
	void write(Base::OBinStream&) const {}
};
NET_MESSAGE_STREAM(ServerMessageInitializationFinish);


struct ClientMessageSync : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageSync);

	Timestamp timestamp_departments = 0;		// синхронизация департаментов
	Timestamp timestamp_users = 0;				// синхронизация пользователей
	Timestamp timestamp_chats = 0;				// синхронизация чатов

	ChatMessageStamp last_chat_message_stamp = 0;	// синхронизация сообщений по штампу
	ChatMessageId last_readed_chat_message_id = 0;	// поиск прочитанных сообщений

	void read(Base::IBinStream& stream)
	{
		stream >> timestamp_departments;
		stream >> timestamp_users;
		stream >> timestamp_chats;
		stream >> last_chat_message_stamp;
		stream >> last_readed_chat_message_id;
	}
	void write(Base::OBinStream& stream) const
	{
		stream << timestamp_departments;
		stream << timestamp_users;
		stream << timestamp_chats;
		stream << last_chat_message_stamp;
		stream << last_readed_chat_message_id;
	}
};
NET_MESSAGE_STREAM(ClientMessageSync);

struct ServerMessageSyncResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageSyncResponse);

	void read(Base::IBinStream&)
	{
	}
	void write(Base::OBinStream&) const
	{
	}
};
NET_MESSAGE_STREAM(ServerMessageSyncResponse);


struct ServerMessageDepartments : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageDepartments);

	std::vector<DescDepartment> departments;

	void read(Base::IBinStream& stream)
	{
		unsigned int size = 0;
		stream >> size;
		departments.reserve(size);
		for (unsigned int i = 0; i < size; ++i)
		{
			DescDepartment department;
			stream >> department.id;
			stream >> department.name;
			stream >> department.timestamp;
			departments.push_back(department);
		}
	}
	void write(Base::OBinStream& stream) const
	{
		stream << static_cast<unsigned int>(departments.size());
		for (const DescDepartment& department : departments)
		{
			stream << department.id;
			stream << department.name;
			stream << department.timestamp;
		}
	}
};
NET_MESSAGE_STREAM(ServerMessageDepartments);


struct ServerMessageUsers : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageUsers);

	std::vector<DescUser> users;

	void read(Base::IBinStream& stream)
	{
		unsigned int size = 0;
		stream >> size;
		users.reserve(size);
		for (unsigned int i = 0; i < size; ++i)
		{
			DescUser user;
			stream >> user.id;
			stream >> user.login;
			stream >> user.name;
			stream >> user.folder;
			stream >> user.timestamp;
			stream >> user.department_id;
			stream >> user.deleted;
			users.push_back(user);
		}
	}
	void write(Base::OBinStream& stream) const
	{
		stream << static_cast<unsigned int>(users.size());
		for (const DescUser& user : users)
		{
			stream << user.id;
			stream << user.login;
			stream << user.name;
			stream << user.folder;
			stream << user.timestamp;
			stream << user.department_id;
			stream << user.deleted;
		}
	}
};
NET_MESSAGE_STREAM(ServerMessageUsers);


struct ServerMessageChatList : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageChatList);

	std::vector<ChatId> favorites;
	std::vector<ChatId> recents;

	void read(Base::IBinStream& stream)
	{
		{
			unsigned int size = 0;
			stream >> size;
			favorites.reserve(size);
			for (unsigned int i = 0; i < size; ++i)
			{
				ChatId chat_id;
				stream >> chat_id;
				favorites.push_back(chat_id);
			}
		}
		{
			unsigned int size = 0;
			stream >> size;
			recents.reserve(size);
			for (unsigned int i = 0; i < size; ++i)
			{
				ChatId chat_id;
				stream >> chat_id;
				recents.push_back(chat_id);
			}
		}
	}
	void write(Base::OBinStream& stream) const
	{
		stream << static_cast<unsigned int>(favorites.size());
		for (auto chat_id : favorites)
			stream << chat_id;

		stream << static_cast<unsigned int>(recents.size());
		for (auto chat_id : recents)
			stream << chat_id;
	}
};
NET_MESSAGE_STREAM(ServerMessageChatList);


struct ClientMessageAlive : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageAlive);
	void read(Base::IBinStream& stream) { Q_UNUSED(stream); }
	void write(Base::OBinStream& stream) const { Q_UNUSED(stream); }
};
NET_MESSAGE_STREAM(ClientMessageAlive);

struct ServerMessageAliveResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageAliveResponse);
	void read(Base::IBinStream& stream) { Q_UNUSED(stream); }
	void write(Base::OBinStream& stream) const { Q_UNUSED(stream); }
};
NET_MESSAGE_STREAM(ServerMessageAliveResponse);



struct ServerMessageSetLoginUser : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageSetLoginUser);
	ServerMessageSetLoginUser(UserId user_id) : user_id(user_id) {}
	UserId user_id = 0;

	void read(Base::IBinStream& stream) {
		stream >> user_id;
	}
	void write(Base::OBinStream& stream) const {
		stream << user_id;
	}
};
NET_MESSAGE_STREAM(ServerMessageSetLoginUser);


struct ServerMessageContactStatus : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageContactStatus);
	ServerMessageContactStatus(UserId user_id, bool status) : user_id(user_id), status(status) {}
	UserId user_id;
	bool status;

	void read(Base::IBinStream& stream) {
		stream >> user_id;
		stream >> status;
	}
	void write(Base::OBinStream& stream) const {
		stream << user_id;
		stream << status;
	}
};
NET_MESSAGE_STREAM(ServerMessageContactStatus);

struct ClientMessageUsersStatus : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageUsersStatus);
	ClientMessageUsersStatus(const std::vector<UserId>& users_ids) : users_ids(users_ids) {}
	std::vector<UserId> users_ids;
	void read(Base::IBinStream& stream)
	{
		unsigned int size = 0;
		stream >> size;
		users_ids.reserve(size);
		for (unsigned int i = 0; i < size; ++i)
		{
			UserId user_id;
			stream >> user_id;
			users_ids.push_back(user_id);
		}
	}
	void write(Base::OBinStream& stream) const
	{
		stream << static_cast<unsigned int>(users_ids.size());
		for (UserId user_id : users_ids)
			stream << user_id;
	}
};
NET_MESSAGE_STREAM(ClientMessageUsersStatus);

struct ServerMessageUsersStatus : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageUsersStatus);
	ServerMessageUsersStatus(const std::vector<UserStatus>& user_statuses): user_statuses(user_statuses) {}
	std::vector<UserStatus> user_statuses;
	void read(Base::IBinStream& stream)
	{
		unsigned int size = 0;
		stream >> size;
		user_statuses.reserve(size);
		for (unsigned int i = 0; i < size; ++i)
		{
			UserStatus user_status;
			stream >> user_status.user_id;
			stream >> user_status.status;
			user_statuses.push_back(user_status);
		}
	}
	void write(Base::OBinStream& stream) const
	{
		stream << static_cast<unsigned int>(user_statuses.size());
		for (const UserStatus& user_status : user_statuses)
		{
			stream << user_status.user_id;
			stream << user_status.status;
		}
	}
};
NET_MESSAGE_STREAM(ServerMessageUsersStatus);

/*struct ServerMessageContactStatusOffline : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageContactStatusOffline);
	ServerMessageContactStatusOffline(unsigned int _id) : id(_id) {}
	UserId id = 0;

	void read(Base::IBinStream& stream) {
		stream >> id;
	}
	void write(Base::OBinStream& stream) const {
		stream << id;
	}
};
NET_MESSAGE_STREAM(ServerMessageContactStatusOffline);*/


struct ClientMessageTextMessageSend : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageTextMessageSend);
	ClientMessageTextMessageSend(ChatId chat_id, const ChatMessageLocalId& local_id, const QString& text) : chat_id(chat_id), local_id(local_id), text(text) {}

	ChatId chat_id = 0;
	ChatMessageLocalId local_id;
	QString text;

	void read(Base::IBinStream& stream);
	void write(Base::OBinStream& stream) const;
};
NET_MESSAGE_STREAM(ClientMessageTextMessageSend);


struct ServerMessageChatMessages : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageChatMessages);
	std::vector<DescChatMessage> messages;

	void read(Base::IBinStream& stream);
	void write(Base::OBinStream& stream) const;
};
NET_MESSAGE_STREAM(ServerMessageChatMessages);


struct ServerMessageChatHistoryMessages : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageChatHistoryMessages);
	ServerMessageChatHistoryMessages(ChatId chat_id, bool is_finish_upload) : chat_id(chat_id), is_finish_upload(is_finish_upload) {}
	ChatId chat_id = 0;
	bool is_finish_upload = false;
	std::vector<DescChatMessage> messages;

	void read(Base::IBinStream& stream);
	void write(Base::OBinStream& stream) const;
};
NET_MESSAGE_STREAM(ServerMessageChatHistoryMessages);



struct ClientMessageFileMessageSend : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageFileMessageSend);
	ClientMessageFileMessageSend(ChatId chat_id, const ChatMessageLocalId& local_id, const QString& src_path, bool is_dir, quint64 size) : chat_id(chat_id), local_id(local_id), src_path(src_path), is_dir(is_dir), size(size) {}
	
	ChatId chat_id = 0;
	ChatMessageLocalId local_id;
	QString src_path;
	bool is_dir = false;
	quint64 size = 0;

	void read(Base::IBinStream& stream);
	void write(Base::OBinStream& stream) const;
};
NET_MESSAGE_STREAM(ClientMessageFileMessageSend);


/*struct ServerMessageFileMessageSendResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageFileMessageSendResponse);
	ServerMessageFileMessageSendResponse(const ChatMessageLocalId& message_local_id, ChatMessageId message_id) : message_local_id(message_local_id), message_id(message_id) {}
	ChatMessageLocalId message_local_id;
	ChatMessageId message_id;

	void read(Base::IBinStream& stream) {
		stream >> message_local_id;
		stream >> message_id;
	}
	void write(Base::OBinStream& stream) const {
		stream << message_local_id;
		stream << message_id;
	}
};
NET_MESSAGE_STREAM(ServerMessageFileMessageSendResponse);
*/

struct ClientMessageFileTransferStart : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageFileTransferStart);
	ClientMessageFileTransferStart(ChatMessageId message_id, ChatId chat_id, const QString& name, bool is_dir, quint64 size, const std::vector<QString>& files) : message_id(message_id), chat_id(chat_id), name(name), is_dir(is_dir), size(size), files(files) {}

	ChatMessageId message_id = 0;
	ChatId chat_id = 0;
	QString name;	// имя папки или файла
	bool is_dir = false;
	quint64 size = 0;
	std::vector<QString> files;

	void read(Base::IBinStream& stream) {
		stream >> message_id;
		stream >> chat_id;
		stream >> name;
		stream >> is_dir;
		stream >> size;

		unsigned int num_files = 0;
		stream >> num_files;
		files.reserve(num_files);
		for (unsigned int i = 0; i < num_files; ++i)
		{
			QString file_name;
			stream >> file_name;
			files.push_back(file_name);
		}
	}
	void write(Base::OBinStream& stream) const {
		stream << message_id;
		stream << chat_id;
		stream << name;		
		stream << is_dir;
		stream << size;

		stream << static_cast<unsigned int>(files.size());
		for (const QString& file_name : files)
			stream << file_name;
	}
};
NET_MESSAGE_STREAM(ClientMessageFileTransferStart);


struct ClientMessageFileTransferFinish : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageFileTransferFinish);
	void read(Base::IBinStream&) {}
	void write(Base::OBinStream&) const {}
};
NET_MESSAGE_STREAM(ClientMessageFileTransferFinish);


struct ClientMessageFileTransferCancel : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageFileTransferCancel);
	ClientMessageFileTransferCancel(unsigned int file_id, quint64 bytes, bool user_cancel) : file_id(file_id), bytes(bytes), user_cancel(user_cancel) {}
	
	unsigned int file_id = 0;
	quint64 bytes = 0;
	bool user_cancel = false;

	void read(Base::IBinStream& stream) {
		stream >> file_id;
		stream >> bytes;
		stream >> user_cancel;
	}
	void write(Base::OBinStream& stream) const {
		stream << file_id;
		stream << bytes;
		stream << user_cancel;
	}
};
NET_MESSAGE_STREAM(ClientMessageFileTransferCancel);


struct ServerMessageFileTransferFinish : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageFileTransferFinish);
	ServerMessageFileTransferFinish(FileTransferResult result, const QString& dest_name) : result(result), dest_name(dest_name) {}

	FileTransferResult result = FileTransferResult::Default;
	QString dest_name;

	void read(Base::IBinStream& stream);
	void write(Base::OBinStream& stream) const;
};
NET_MESSAGE_STREAM(ServerMessageFileTransferFinish);


struct ServerMessageFileTransferWriteFileError : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageFileTransferWriteFileError);
	void read(Base::IBinStream&) {}
	void write(Base::OBinStream&) const {}
};
NET_MESSAGE_STREAM(ServerMessageFileTransferWriteFileError);



struct ClientMessageFileTransferFileBodyStart : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageFileTransferFileBodyStart);
	ClientMessageFileTransferFileBodyStart(unsigned int file_index, quint64 size) : file_index(file_index), size(size) {}
	unsigned int file_index = 0;
	quint64 size = 0;

	void read(Base::IBinStream& stream) {
		stream >> file_index;
		stream >> size;
	}
	void write(Base::OBinStream& stream) const {
		stream << file_index;
		stream << size;
	}
};
NET_MESSAGE_STREAM(ClientMessageFileTransferFileBodyStart);


struct ClientMessageFileMessageRegister : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageFileMessageRegister);
	ClientMessageFileMessageRegister(ChatId chat_id, ChatMessageId message_id) : chat_id(chat_id), message_id(message_id) {}
	ChatId chat_id = 0;
	ChatMessageId message_id = 0;

	void read(Base::IBinStream& stream) {
		stream >> chat_id;
		stream >> message_id;
	}
	void write(Base::OBinStream& stream) const {
		stream << chat_id;
		stream << message_id;
	}
};
NET_MESSAGE_STREAM(ClientMessageFileMessageRegister);


struct ServerMessageFileMessageProgress : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageFileMessageProgress);
	ServerMessageFileMessageProgress(ChatMessageId message_id, ChatId chat_id, unsigned char progress) : message_id(message_id), chat_id(chat_id), progress(progress) {}
	ChatMessageId message_id = 0;
	ChatId chat_id = 0;
	unsigned char progress = 0;

	void read(Base::IBinStream& stream) {
		stream >> message_id;
		stream >> chat_id;
		stream >> progress;
	}
	void write(Base::OBinStream& stream) const {
		stream << message_id;
		stream << chat_id;
		stream << progress;
	}
};
NET_MESSAGE_STREAM(ServerMessageFileMessageProgress);


struct ClientMessageFileMessageCancel : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageFileMessageCancel);
	ClientMessageFileMessageCancel(ChatMessageId message_id) : message_id(message_id) {}
	ChatMessageId message_id;

	void read(Base::IBinStream& stream) {
		stream >> message_id;
	}
	void write(Base::OBinStream& stream) const {
		stream << message_id;
	}
};
NET_MESSAGE_STREAM(ClientMessageFileMessageCancel);


struct ServerMessageFileMessageCancelResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageFileMessageCancelResponse);
	ServerMessageFileMessageCancelResponse(ChatMessageId message_id) : message_id(message_id) {}
	ChatMessageId message_id = 0;

	void read(Base::IBinStream& stream) {
		stream >> message_id;
	}
	void write(Base::OBinStream& stream) const {
		stream << message_id;
	}
};
NET_MESSAGE_STREAM(ServerMessageFileMessageCancelResponse);



struct ServerMessageContactChat : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageContactChat);
	ServerMessageContactChat(ChatId chat_id, UserId contact_id, Timestamp timestamp, ChatMessageId last_message_id, ChatMessageStamp last_message_stamp)
		: chat_id(chat_id), contact_id(contact_id), timestamp(timestamp), last_message_id(last_message_id), last_message_stamp(last_message_stamp) {}
	ChatId chat_id = 0;
	UserId contact_id = 0;
	Timestamp timestamp = 0;
	ChatMessageId last_message_id = 0;
	ChatMessageStamp last_message_stamp = 0;

	void read(Base::IBinStream& stream);
	void write(Base::OBinStream& stream) const;
};
NET_MESSAGE_STREAM(ServerMessageContactChat);


struct ClientMessageContactChatRequest : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageContactChatRequest);
	ClientMessageContactChatRequest(UserId _user_id) : user_id(_user_id) {}
	UserId user_id = 0;

	void read(Base::IBinStream& stream) {
		stream >> user_id;
	}
	void write(Base::OBinStream& stream) const {
		stream << user_id;
	}
};
NET_MESSAGE_STREAM(ClientMessageContactChatRequest);

struct ClientMessageContactChatCreate : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageContactChatCreate);
	ClientMessageContactChatCreate(UserId user_id) : user_id(user_id) {}
	UserId user_id = 0;

	void read(Base::IBinStream& stream) {
		stream >> user_id;
	}
	void write(Base::OBinStream& stream) const {
		stream << user_id;
	}
};
NET_MESSAGE_STREAM(ClientMessageContactChatCreate);


struct ServerMessageContactChatCreateResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageContactChatCreateResponse);
	ServerMessageContactChatCreateResponse(UserId user_id, ChatId chat_id) : user_id(user_id), chat_id(chat_id) {}

	UserId user_id = 0;	// !!! если чат не создан, то невозможно получить контакт
	ChatId chat_id = 0;

	void read(Base::IBinStream& stream) {
		stream >> user_id;
		stream >> chat_id;
	}
	void write(Base::OBinStream& stream) const {
		stream << user_id;
		stream << chat_id;
	}
};
NET_MESSAGE_STREAM(ServerMessageContactChatCreateResponse);


struct ServerMessageChatId : public NetMessage
{
	ServerMessageChatId(ChatId _chat_id = 0) : chat_id(_chat_id) {}
	ChatId chat_id = 0;

	void read(Base::IBinStream& stream) {
		stream >> chat_id;
	}
	void write(Base::OBinStream& stream) const {
		stream << chat_id;
	}
};


struct ClientMessageChatMoveFavorite : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageChatMoveFavorite);
	ClientMessageChatMoveFavorite(ChatId chat_id, bool favorite) : chat_id(chat_id), favorite(favorite) {}
	ChatId chat_id = 0;
	bool favorite = false;

	void read(Base::IBinStream& stream) {
		stream >> chat_id;
		stream >> favorite;
	}
	void write(Base::OBinStream& stream) const {
		stream << chat_id;
		stream << favorite;
	}
};
NET_MESSAGE_STREAM(ClientMessageChatMoveFavorite);


struct ClientMessageChatRemove : public ServerMessageChatId
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageChatRemove);
	ClientMessageChatRemove(ChatId chat_id) : ServerMessageChatId(chat_id) {}
};
NET_MESSAGE_STREAM(ClientMessageChatRemove);

struct ServerMessageChatAddRecent : public ServerMessageChatId
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageChatAddRecent);
	ServerMessageChatAddRecent(ChatId chat_id) : ServerMessageChatId(chat_id) {}
};
NET_MESSAGE_STREAM(ServerMessageChatAddRecent);

struct ServerMessageChatAddFavorite : public ServerMessageChatId
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageChatAddFavorite);
	ServerMessageChatAddFavorite(ChatId chat_id) : ServerMessageChatId(chat_id) {}
};
NET_MESSAGE_STREAM(ServerMessageChatAddFavorite);

struct ServerMessageChatRemoveResponse : public ServerMessageChatId
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageChatRemoveResponse);
	ServerMessageChatRemoveResponse(ChatId chat_id) : ServerMessageChatId(chat_id) {}
};
NET_MESSAGE_STREAM(ServerMessageChatRemoveResponse);

struct ClientMessageChatRequestHistoryMessages : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageChatRequestHistoryMessages);
	ClientMessageChatRequestHistoryMessages(ChatId chat_id, ChatMessageId message_id) : chat_id(chat_id), message_id(message_id) {}
	ChatId chat_id = 0;
	ChatMessageId message_id = 0;

	void read(Base::IBinStream& stream) {
		stream >> chat_id;
		stream >> message_id;
	}
	void write(Base::OBinStream& stream) const {
		stream << chat_id;
		stream << message_id;
	}
};
NET_MESSAGE_STREAM(ClientMessageChatRequestHistoryMessages);


struct ClientMessageGroupChatCreate : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageGroupChatCreate);
	std::vector<UserId> user_ids;

	void read(Base::IBinStream& stream);
	void write(Base::OBinStream& stream) const;
};
NET_MESSAGE_STREAM(ClientMessageGroupChatCreate);

struct ServerMessageGroupChatCreateResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageGroupChatCreateResponse);
	ServerMessageGroupChatCreateResponse(ChatId chat_id) : chat_id(chat_id) {}
	ChatId chat_id = 0;

	void read(Base::IBinStream& stream) {
		stream >> chat_id;
	}
	void write(Base::OBinStream& stream) const {
		stream << chat_id;
	}
};
NET_MESSAGE_STREAM(ServerMessageGroupChatCreateResponse);

struct ServerMessageGroupChat : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageGroupChat);
	ServerMessageGroupChat(ChatId chat_id, const QString& name, Timestamp timestamp, ChatMessageId last_message_id, ChatMessageStamp last_message_stamp) : chat_id(chat_id), name(name), timestamp(timestamp), last_message_id(last_message_id), last_message_stamp(last_message_stamp) {}
	ChatId chat_id = 0;
	QString name = 0;
	Timestamp timestamp = 0;
	ChatMessageId last_message_id = 0;
	ChatMessageStamp last_message_stamp = 0;

	void read(Base::IBinStream& stream);
	void write(Base::OBinStream& stream) const;
};
NET_MESSAGE_STREAM(ServerMessageGroupChat);


struct ServerMessageGroupChatSetUsers : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageGroupChatSetUsers);
	ServerMessageGroupChatSetUsers(ChatId chat_id, Timestamp timestamp) : chat_id(chat_id), timestamp(timestamp) {}
	ChatId chat_id = 0;
	Timestamp timestamp = 0;
	std::vector<UserId> users;	// участники чата
	std::vector<UserId> admins;

	void read(Base::IBinStream& stream);
	void write(Base::OBinStream& stream) const;
};
NET_MESSAGE_STREAM(ServerMessageGroupChatSetUsers);

struct ClientMessageGroupChatAddUsers : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageGroupChatAddUsers);
	ClientMessageGroupChatAddUsers(ChatId chat_id, const std::vector<UserId>& user_ids) : chat_id(chat_id), user_ids(user_ids) {}
	ChatId chat_id = 0;
	std::vector<UserId> user_ids;

	void read(Base::IBinStream& stream);
	void write(Base::OBinStream& stream) const;
};
NET_MESSAGE_STREAM(ClientMessageGroupChatAddUsers);

struct ClientMessageGroupChatRemoveUser : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageGroupChatRemoveUser);
	ClientMessageGroupChatRemoveUser(ChatId chat_id, UserId user_id) : chat_id(chat_id), user_id(user_id) {}
	ChatId chat_id = 0;
	UserId user_id = 0;

	void read(Base::IBinStream& stream);
	void write(Base::OBinStream& stream) const;
};
NET_MESSAGE_STREAM(ClientMessageGroupChatRemoveUser);

struct ClientMessageGroupSetOrRemoveAdmin : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageGroupSetOrRemoveAdmin);
	ClientMessageGroupSetOrRemoveAdmin(ChatId chat_id, UserId user_id, bool set_or_remove) : chat_id(chat_id), user_id(user_id), set_or_remove(set_or_remove) {}
	ChatId chat_id = 0;
	UserId user_id = 0;
	bool set_or_remove = false;

	void read(Base::IBinStream& stream)
	{
		stream >> chat_id;
		stream >> user_id;
		stream >> set_or_remove;

	}
	void write(Base::OBinStream& stream) const
	{
		stream << chat_id;
		stream << user_id;
		stream << set_or_remove;
	}
};
NET_MESSAGE_STREAM(ClientMessageGroupSetOrRemoveAdmin);

struct ClientMessageGroupChatRename : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageGroupChatRename);
	ClientMessageGroupChatRename(ChatId chat_id, const QString& name) : chat_id(chat_id), name(name) {}
	ChatId chat_id = 0;
	QString name;

	void read(Base::IBinStream& stream) {
		stream >> chat_id;
		stream >> name;
	}
	void write(Base::OBinStream& stream) const {
		stream << chat_id;
		stream << name;
	}
};
NET_MESSAGE_STREAM(ClientMessageGroupChatRename);


struct ClientMessageChatNewMessageRead : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageChatNewMessageRead);
	ClientMessageChatNewMessageRead(ChatId chat_id, ChatMessageId message_id) : chat_id(chat_id), message_id(message_id) {}
	ChatId chat_id = 0;
	ChatMessageId message_id = 0;

	void read(Base::IBinStream& stream) {
		stream >> chat_id;
		stream >> message_id;
	}
	void write(Base::OBinStream& stream) const {
		stream << chat_id;
		stream << message_id;
	}
};
NET_MESSAGE_STREAM(ClientMessageChatNewMessageRead);

struct ServerMessageChatMessageRead : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageChatMessageRead);
	ServerMessageChatMessageRead(ChatId chat_id, ChatMessageId message_id, const QDateTime& datetime) : chat_id(chat_id), message_id(message_id), datetime(datetime) {}
	ChatId chat_id = 0;
	ChatMessageId message_id = 0;
	QDateTime datetime;

	void read(Base::IBinStream& stream) {
		stream >> chat_id;
		stream >> message_id;
		stream >> datetime;
	}
	void write(Base::OBinStream& stream) const {
		stream << chat_id;
		stream << message_id;
		stream << datetime;
	}
};
NET_MESSAGE_STREAM(ServerMessageChatMessageRead);

struct ServerMessageChatNewMessageReadResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageChatNewMessageReadResponse);
	ServerMessageChatNewMessageReadResponse(bool success, ChatId chat_id, ChatMessageId message_id) : success(success), chat_id(chat_id), message_id(message_id) {}
	bool success = false;
	ChatId chat_id = 0;
	ChatMessageId message_id = 0;

	void read(Base::IBinStream& stream) {
		stream >> success;
		stream >> chat_id;
		stream >> message_id;
	}
	void write(Base::OBinStream& stream) const {
		stream << success;
		stream << chat_id;
		stream << message_id;
	}
};
NET_MESSAGE_STREAM(ServerMessageChatNewMessageReadResponse);

struct ClientMessageIsAccessClosedFolder : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageIsAccessClosedFolder);

	void read(Base::IBinStream& stream) {
		Q_UNUSED(stream);
	}
	void write(Base::OBinStream& stream) const {
		Q_UNUSED(stream);
	}
};
NET_MESSAGE_STREAM(ClientMessageIsAccessClosedFolder);

struct ServerMessageIsAccessClosedFolderResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageIsAccessClosedFolderResponse);
	ServerMessageIsAccessClosedFolderResponse(bool is_have_access) : is_have_access(is_have_access) {}
	bool is_have_access = false;

	void read(Base::IBinStream& stream) {
		stream >> is_have_access;
	}
	void write(Base::OBinStream& stream) const {
		stream << is_have_access;
	}
};
NET_MESSAGE_STREAM(ServerMessageIsAccessClosedFolderResponse);