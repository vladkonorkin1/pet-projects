#pragma once

#pragma pack(push,1)
struct PacketHeader
{
	static unsigned short marker;	// AP

	unsigned short metka;		//	маркер пакета
	unsigned int data_size;		//	размер данных

	PacketHeader(unsigned int size = 0) : data_size(size), metka(marker) {}
};
#pragma pack(pop)
/*
//BASE_MAKE_EXCEPTION(PacketFailure, Base::Exception);

class Packet
{
public:
#pragma pack(push,1)
	struct Header
	{
		unsigned short marker;		//	маркер пакета
		unsigned int data_size;		//	размер данных

		Header(unsigned int size = 0) : data_size(size), marker(Packet::marker) {}
	};
#pragma pack(pop)

	static const unsigned short marker;	//	маркер пакета
	//typedef std::vector<char> 

private:
	QByteArray m_data;				//	упакованные данные пакета


public:
	//explicit Packet(const QByteArray& buffer);
	explicit Packet(const std::string& buffer);
public:
	///	получить данные
	const QByteArray& data() const;

public:
	///	получить максимально возможное кол-во упаковаемых данных 
	//static unsigned int max_size();
};

//	получить данные
inline const QByteArray& Packet::data() const
{
	return m_data;
}*/