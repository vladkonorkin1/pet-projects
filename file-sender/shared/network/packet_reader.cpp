#include "base/precomp.h"
#include "packet_reader.h"

PacketReader::PacketReader()
: m_parser(&PacketReader::parse_marker)
, m_current(0)
{
}
void PacketReader::read(const QByteArray& buffer)
{
	//m_data.append(buffer);
	m_data.insert(m_data.end(), buffer.constData(), buffer.constData() + buffer.size());
	//m_data.insert(m_data.end(), buffer, buffer + size);
	while (m_current < m_data.size())
		if (!(this->*m_parser)())
			break;
}
unsigned int PacketReader::current_size() const
{
	return static_cast<unsigned int>(m_data.size()) - m_current;
}
//	разобрать маркер пакета
bool PacketReader::parse_marker()
{
	assert( m_current <= m_data.size() );
	if (current_size() >= sizeof(PacketHeader::marker))
	{
		if (memcmp(&m_data[m_current], &PacketHeader::marker, sizeof(PacketHeader::marker)) == 0)
		{
			m_current += sizeof(PacketHeader::marker);
			m_parser = &PacketReader::parse_header;
		}
		else
		{
			//throw std::runtime_error("PacketReader: fail parse data");
			//m_parser = &PacketReader::parse_failed_data;
			m_current = 0;
			return false;
		}
		return true;
	}
	return false;
}
//	разобрать заголовок пакета
bool PacketReader::parse_header()
{
	const unsigned int size = sizeof(m_header.data_size);

	assert( m_current <= m_data.size() );
	if (current_size() >= size)
	{
		memcpy(&m_header.data_size, &m_data[m_current], size);
		m_current += size;
		m_parser = &PacketReader::parse_data;
		if (m_header.data_size == 0)
			return (this->*m_parser)();
		return true;
	}
	return false;
}
//	разобрать данные
bool PacketReader::parse_data()
{
	assert( m_current <= m_data.size() );
	if (current_size() >= m_header.data_size)
	{
		//	уведомляем наблюдателя
		emit data_received(m_data.data() + m_current, m_header.data_size);
		//	удаляем разобранные данные из буфера
		//m_data = m_data.right(m_current + m_header.data_size); // erase(m_data.begin(), m_data.begin() + m_current + m_header.dataSize);
		m_data.erase(m_data.begin(), m_data.begin() + m_current + m_header.data_size);
		//	обнуляем позицию чтения 
		m_current = 0;
		//	выставляем функцию разбора на чтение нового пакета
		m_parser = &PacketReader::parse_marker;
		return true;
	}
	return false;
}
/*
///	разобрать испорченные данные
bool PacketReader::parse_failed_data()
{
	assert( m_current < m_data.size() );
	unsigned pos = find_marker();
	assert(pos <= m_data.size());
	//	проверяем на нулевую позицию маркера 
	if (!check_failed_data_marker_null_pos(pos))
	{
		assert(pos > 0);
		//	уведомляем наблюдателя
		emit on_wrong_data_received(&m_data[0], pos);
		//if (m_observer) m_observer->onPacketReaderWrongDataReceived(&m_data[0], pos);
		//	удаляем данные из буфера
		m_data.erase(m_data.begin(), m_data.begin() + pos);
		// выставляем функцию разбора и обнуляем позицию чтения 
		m_parser = &PacketReader::parse_marker;
		m_current = 0;
	}
	return true;
}*/
