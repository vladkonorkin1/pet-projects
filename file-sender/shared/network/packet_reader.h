#pragma once
#include "network/packet.h"

class PacketReader : public QObject
{
public:
	Q_OBJECT
private:
	typedef bool (PacketReader::*parser_t)();
	typedef std::vector<char> data_t;

	PacketHeader m_header;				//	текущие данные заголовка пакета
	parser_t m_parser;					//	указатель на функцию разбора данных
	int m_current;						//	текущая позиция чтения пакета
	data_t m_data;						//	накопленные данные

public:
	PacketReader();
	~PacketReader() {}

public:
	void read(const QByteArray& buffer);

signals:
	void data_received(const char* data, unsigned int size);
	//void wrong_data_received(const char* data, unsigned int size);

private:
	unsigned int current_size() const;
	///	1. разобрать маркер пакета
	bool parse_marker();
	///	2. разобрать заголовок пакета
	bool parse_header();
	///	3. разобрать данные
	bool parse_data();

	///	проверить и обработать нулевую позицию маркера в неправильных данных
	//bool check_failed_data_marker_null_pos(size_t pos);
	///	разобрать испорченные данные
	//bool parse_failed_data();
	///	пропустить неисзвестные данные заголовка
	//bool skip_header_data();
	///	найти позицию маркера в буфере данных
	//size_t find_marker();
};