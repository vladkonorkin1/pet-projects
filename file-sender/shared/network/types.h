#pragma once
#include <limits>
#include <QUuid>

using ConnectionId = quint64;

static quint16 NetProtocolVersion = 211;
static quint16 MessagesServerPort = 16000;
static quint16 FileServerPort = MessagesServerPort + 1;

using DepartmentId = unsigned int;
using UserId = unsigned int;
using ChatId = quint64;
using Timestamp = qint64;

enum class FileTransferResult
{
	Default,
	Success,
	ErrorRead,
	ErrorWrite,
	ErrorTransfer,
	Cancel,
	Max
};

using ChatMessageId = quint64;
using ChatMessageLocalId = QUuid;
using ChatMessageStamp = quint64;

enum class ChatSearchType
{
	TypeSearchContacts = 0,
	TypeModifyChatUsers,
};

enum class AutorizateResult : unsigned char
{
	Null,
	Success,
	WrongLogin,				// неправильный логин
};

struct DescDepartment
{
	DepartmentId id;
	QString name;
	Timestamp timestamp;
};

struct UserStatus
{
	UserId user_id;
	bool status;
};

struct DescUser
{
	UserId id;
	Timestamp timestamp;
	QString login;
	QString name;
	QString folder;
	DepartmentId department_id;
	bool deleted;
};

struct DescChatUser
{
	UserId user_id;
	bool admin;
};

struct DescChat
{
	ChatId id;
	Timestamp timestamp;
	QString name;
	UserId contact_id;
	ChatMessageId last_message_id;
	ChatMessageStamp last_message_stamp;
	ChatMessageStamp historical_message_stamp;
	ChatMessageId last_readed_message_id;

	std::vector<DescChatUser> chat_users;
};

struct DescChatListContact
{
	ChatId chat_id;
	bool favorite;
};

// структура о синхронизации чата
struct DescSyncChat
{
	ChatId chat_id;
	Timestamp timestamp;
	ChatMessageStamp last_chat_message_stamp;		// максимальный штамп
	//ChatMessageId last_chat_message_id;
	ChatMessageId last_readed_chat_message_id;	// последнее прочитанное сообщение
};

enum class ChatMessageType
{
	Text,
	File,
	System,	// Description	// пояснение
	//AddToGroup,
	//RemoveFromGroup,
	Audio,
	Video
};

enum class ChatMessageStatus
{
	Sending,
	Sended,
	Readed
};

enum class HistoryStatus
{
	Created,
	Changed,
	Deleted
};

struct DescChatMessage
{
	ChatMessageId id;
	ChatMessageLocalId local_id;
	ChatId chat_id;
	UserId sender_id;
	ChatMessageType type;
	ChatMessageStamp stamp;
	ChatMessageStatus status;
	HistoryStatus history_status;
	QDateTime datetime;
	QDateTime datetime_readed;

	bool historical;

	// text message
	QString text;

	// file message
	QString file_src_path;	// локальный путь к папке/файлу
	QString file_dest_name;	// имя файла/папки на сервере
	quint64 file_size;
	bool file_is_dir;
	FileTransferResult file_status;

	UserId target_contact_id;

	DescChatMessage(ChatMessageId id = 0, const ChatMessageLocalId& local_id = ChatMessageLocalId(), ChatId chat_id = 0, UserId sender_id = 0, ChatMessageStamp stamp = 0, ChatMessageType type = ChatMessageType::Text, ChatMessageStatus status = ChatMessageStatus::Sending, HistoryStatus history_status = HistoryStatus::Created, const QDateTime& datetime = QDateTime::currentDateTimeUtc(), const QDateTime& datetime_readed = QDateTime(), bool historical = false)
	: id(id) 
	, local_id(local_id)
	, chat_id(chat_id)
	, sender_id(sender_id)
	, stamp(stamp)
	, type(type)
	, status(status)
	, history_status(history_status)
	, datetime(datetime)
	, datetime_readed(datetime_readed)
	, historical(historical)
	, file_size(0)
	, file_is_dir(false)
	, file_status(FileTransferResult::Default)
	, target_contact_id(0)
	{
	}
};
