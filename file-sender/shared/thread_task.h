//=====================================================================================//
//   Author: open
//   Date:   02.07.2019
//=====================================================================================//
#pragma once

class ThreadTask : public QObject
{
	Q_OBJECT
public:
	// обработчик исполняемый в неглавном потоке приложения
	virtual void on_process() = 0;
	// обработчик исполняемый в главном потоке приложения
	virtual void on_result() {}
};

using SThreadTask = std::shared_ptr<ThreadTask>;