//=====================================================================================//
//   Author: open
//   Date:   02.07.2019
//=====================================================================================//
#include "base/precomp.h"
#include "thread_task_manager.h"
#include <QCoreApplication>
#include <QEvent>


ThreadTaskConnection::ThreadTaskConnection(ThreadTaskManager& thread_task_manager, const QString& name)
: m_thread_task_manager(thread_task_manager)
{
	m_thread.setObjectName("thread " + name);
}
ThreadTaskConnection::~ThreadTaskConnection()
{
	m_thread.quit();
	m_thread.wait();
}
void ThreadTaskConnection::start_thread()
{
	moveToThread(&m_thread);
	m_thread.start();
}
void ThreadTaskConnection::post()
{
	assert( QThread::currentThread() != &m_thread );
	QCoreApplication::postEvent(this, new QEvent(QEvent::User));
}
bool ThreadTaskConnection::event(QEvent* event)
{
	if (event->type() == QEvent::User)
	{
		assert( QThread::currentThread() == &m_thread );
		while (1)
		{
			SThreadTask task = m_thread_task_manager.next_task(this);
			if (!task) break;
			
			task->on_process();
			emit task_finished(task);
		}
	}
	return true;
}


Q_DECLARE_METATYPE(SThreadTask);

ThreadTaskManager::ThreadTaskManager(int num_threads)
{
	m_connections.reserve(num_threads);
	for (int i = 0; i < num_threads; ++i)
		m_connections.push_back(UThreadTaskConnection(new ThreadTaskConnection(*this, QString::number(i))));

	for (UThreadTaskConnection& connection : m_connections)
	{
		connection->start_thread();
		m_free_connections.push(connection.get());
		connect(connection.get(), &ThreadTaskConnection::task_finished, this, &ThreadTaskManager::slot_task_finished, Qt::QueuedConnection);
	}
}
void ThreadTaskManager::slot_task_finished(SThreadTask task)
{
	task->on_result();
}
void ThreadTaskManager::post_task(ThreadTask* task)
{
	post_task(SThreadTask(task));
}
void ThreadTaskManager::post_task(const SThreadTask& task)
{
	QMutexLocker locker(&m_mutex);
	m_tasks.push(task);

	if (!m_free_connections.empty())
	{
		ThreadTaskConnection* connection = m_free_connections.top();
		m_free_connections.pop();
		connection->post();
	}
}
SThreadTask ThreadTaskManager::next_task(ThreadTaskConnection* connection)
{
	QMutexLocker locker(&m_mutex);
	if (!m_tasks.empty())
	{
		SThreadTask task = m_tasks.front();
		m_tasks.pop();
		return task;
	}

	// иначе вносим в список свободных соединений и возвращаем пустую задачу
	m_free_connections.push(connection);
	static SThreadTask temp;
	return temp;
}