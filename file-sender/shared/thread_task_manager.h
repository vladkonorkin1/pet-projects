//=====================================================================================//
//   Author: open
//   Date:   02.07.2019
//=====================================================================================//
#pragma once
#include "thread_task.h"
#include <QThread>

class ThreadTaskManager;

class ThreadTaskConnection : public QObject
{
	Q_OBJECT
private:
	ThreadTaskManager& m_thread_task_manager;
	QThread m_thread;

public:
	ThreadTaskConnection(ThreadTaskManager& thread_task_manager, const QString& name);
	virtual ~ThreadTaskConnection();
	void start_thread();
	void post();

signals:
	void task_finished(SThreadTask task);

protected:
	virtual bool event(QEvent* event);

private:
	void close();
};

class ThreadTaskManager : public QObject
{
	Q_OBJECT
private:
	using UThreadTaskConnection = std::unique_ptr<ThreadTaskConnection>;
	std::vector<UThreadTaskConnection> m_connections;
	std::stack<ThreadTaskConnection*> m_free_connections;
	std::queue<SThreadTask> m_tasks;	// очередь задач
	QMutex m_mutex;

public:
	ThreadTaskManager(int num_threads = QThread::idealThreadCount());
	void post_task(ThreadTask* thread_task);
	void post_task(const SThreadTask& task);

private slots:
	void slot_task_finished(SThreadTask task);

private:
	SThreadTask next_task(ThreadTaskConnection* connection);
	friend class ThreadTaskConnection;
};