//=====================================================================================//
//   Author: open
//   Date:   24.08.2017
//=====================================================================================//
#include "precomp.h"
#include "download_file.h"
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrlQuery>
#include <QBuffer>

DownloadFile::DownloadFile(QObject* parent)
: QObject(parent)
{
	connect(&m_network, SIGNAL(finished(QNetworkReply*)), SLOT(slot_file_downloaded(QNetworkReply*)));
}
void DownloadFile::start(const QUrl& url)
{
	m_data.clear();
	QNetworkRequest request(url);
	if (QNetworkReply* reply = m_network.get(request))
		connect(reply, SIGNAL(downloadProgress(qint64, qint64)), SLOT(slot_download_file_progress(qint64, qint64)));
}
void DownloadFile::slot_file_downloaded(QNetworkReply* reply)
{
	m_data = reply->readAll();
	if (reply->error() != QNetworkReply::NoError)
	{
		/*QString error_id = QString::number(reply->error());
		QUrl failedUrl = reply->request().url();
		int httpStatus = 0;
		httpStatus = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
		QByteArray httpStatusMessage = reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toByteArray();
		QString str = reply->errorString();
		//qDebug() << reply->readAll();*/

		emit downloaded(false, m_data);
	}
	else
	{
		emit downloaded(true, m_data);
	}
	reply->deleteLater();
}
void DownloadFile::slot_download_file_progress(qint64 received_bytes, qint64 total_bytes)
{
	//std::cout << total_bytes << " " << received_bytes << std::endl;
	emit progress(received_bytes, total_bytes);
}
