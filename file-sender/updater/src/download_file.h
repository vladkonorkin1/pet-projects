//=====================================================================================//
//   Author: open
//   Date:   24.08.2017
//=====================================================================================//
#include <QNetworkAccessManager>

class DownloadFile : public QObject
{
	Q_OBJECT
private:
	QNetworkAccessManager m_network;
	QByteArray m_data;

public:
	DownloadFile(QObject* parent = 0);
	void start(const QUrl& url);

signals:
	void downloaded(bool ok, QByteArray& data);
	void progress(qint64 received_bytes, qint64 total_bytes);

private slots:
	void slot_file_downloaded(QNetworkReply* pReply);
	void slot_download_file_progress(qint64 received_bytes, qint64 total_bytes);
};