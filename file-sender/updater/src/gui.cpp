//=====================================================================================//
//   Author: open
//   Date:   17.12.2018
//=====================================================================================//
#include "precomp.h"
#include "gui.h"
#include <QMessageBox>

void Gui::show_window()
{
	m_main_window.reset(new MainWindow());
	m_main_window->show();
}
void Gui::set_progress(int value)
{
	if (m_main_window) m_main_window->set_progress(value);
}
void Gui::show_critical(const QString& msg)
{
	QMessageBox::critical(m_main_window.get(), tr("Updater"), msg);
}
