//=====================================================================================//
//   Author: open
//   Date:   25.08.2017
//=====================================================================================//
#include "precomp.h"
#include "main_window.h"

MainWindow::MainWindow()
: m_ui(new Ui::MainWindow())
{
	m_ui->setupUi(this);
	setFixedSize(size());
}
void MainWindow::set_progress(int progress)
{
	m_ui->progressbar->setValue(progress);
}