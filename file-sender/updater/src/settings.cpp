//=====================================================================================//
//   Author: open
//   Date:   24.08.2017
//=====================================================================================//
#include "precomp.h"
#include "settings.h"
#include "utility.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>

Settings::Settings()
: application_folder("project")
, application_name("build.exe")
, download_url("http://synccore.ru/downloads/")
, download_file("file")
, auto_close(false)
, gui(false)
{
	QFile file("settings.json");
	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
		QJsonObject obj = doc.object();
		application_folder = obj["application_folder"].toString();
		application_name = obj["application_name"].toString();
		application_arguments = obj["application_arguments"].toString();
		download_url = obj["download_url"].toString();
		download_file = obj["download_file"].toString();
		auto_close = obj["auto_close"].toBool();
		gui = obj["gui"].toBool();
	}
	append_separator(download_url);
}
