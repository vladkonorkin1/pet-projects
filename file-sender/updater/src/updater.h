//=====================================================================================//
//   Author: open
//   Date:   24.06.2017
//=====================================================================================//
#pragma once
#include "download_file.h"
#include "settings.h"

class Updater : public QObject
{
	Q_OBJECT
private:
	const Settings& m_settings;
	QStringList m_args;

	DownloadFile m_download_version;
	DownloadFile m_download_file;

	QString m_build_folder;

	QString m_path_app_data;	// c:/Users/kuz_ap/AppData/Roaming/
	QString m_path_build;		// c:/Users/kuz_ap/AppData/Roaming/sima-gaechka/build

	struct Data
	{
		int version = -1;
		QString guid;

		// c:/Users/kuz_ap/AppData/Roaming/sima-gaechka/build/D91BECFD-6640-4927-847F-C85DE2E86335
		QString path(const QString& path_build) const { return path_build + guid + '/'; }
	};
	Data m_old;
	Data m_new;

public:
	Updater(const Settings& settings);

signals:
	void download_started();
	void download_progress_updated(int progress);
	void alert_showed(const QString& msg);

private slots:
	void slot_download_version(bool ok, QByteArray& data);
	void slot_download_file(bool ok, QByteArray& data);
	void slot_download_file_progress(qint64 received_bytes, qint64 total_bytes);

private:
	QString get_app_data_folder() const;
	void run_process(bool downloaded);
	void check_version();
	void alert(const QString& msg);
	bool write_new_version() const;
	QStringList build_args(const Settings& settings) const;
	Data read_version() const;
	void remove_old_builds(const QString& dir_name);
	void write_updater_settings() const;
	void wait_and_exit();
	void run_process_delay(bool downloaded);
	// �������� ���������� � ������������
	void add_application_to_autostart();
};
