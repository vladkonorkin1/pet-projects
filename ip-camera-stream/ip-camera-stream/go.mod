module ip-camera-stream

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
