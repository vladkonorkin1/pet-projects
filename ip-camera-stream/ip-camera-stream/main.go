package main

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gopkg.in/yaml.v2"
)

type Error struct {
	Code string `json:"error"`
	Msg  string `json:"message"`
}

func NewErrorString(code string, msg string) *Error {
	return &Error{
		Code: code,
		Msg:  msg,
	}
}
func NewError(code string, err error) *Error {
	return &Error{
		Code: code,
		Msg:  err.Error(),
	}
}

func (e *Error) Error() string {
	byte, _ := json.Marshal(e)
	return string(byte)
}

type Stream struct {
	id                string
	rtsp              string
	filename          string
	cmd               *exec.Cmd
	done_channel      chan error
	timer             *time.Timer
	exit_channel      chan *Stream
	mutex             sync.Mutex
	recording         bool
	recording_channel chan bool
	start_time        time.Time
	finish_time       time.Time
}

func CreateCmd(rtsp_path string, login string, password string) *exec.Cmd {
	var p, _ = os.Executable()
	app_path := filepath.Dir(p)

	args := []string{"-V", "-4", "-D", "2", "-b", "320000", "-u", login, password, rtsp_path}

	//cmd := exec.Command(app_path+"/live-stream", args...)
	cmd := exec.Command("./live-stream", args...)
	cmd.Dir = app_path

	//cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd
}

func NewStream(desc DescStartStream, exit_channel chan *Stream) (*Stream, error) {
	now := time.Now()
	stream := Stream{
		id:           desc.Rtsp + " - " + desc.Filename,
		rtsp:         desc.Rtsp,
		filename:     desc.Filename,
		cmd:          CreateCmd(desc.Rtsp, desc.Login, desc.Password),
		exit_channel: exit_channel,
		start_time:   now,
		finish_time:  now.Add(time.Duration(desc.Duration) * time.Second),
	}

	// open the out file for writing
	outfile, err := os.Create(desc.Filename)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	stream.cmd.Stdout = outfile

	if err := stream.cmd.Start(); err != nil {
		log.Println(err)
		outfile.Close()
		return nil, err
	}

	stream.done_channel = make(chan error, 1)
	stream.timer = time.NewTimer(time.Duration(desc.Duration) * time.Second)
	stream.recording_channel = make(chan bool, 1)

	go func() {
		// wait for the process to finish or kill it after a timeout (whichever happens first):
		stream.done_channel <- stream.cmd.Wait()

		// закрыть файл
		if err := outfile.Sync(); err != nil {
			log.Println(err)
		}
		outfile.Close()

		// удалить пустой файл
		fi, err := os.Stat(stream.filename)
		if err != nil {
			log.Println("error file size")
			return
		}
		size := fi.Size()
		if size == 0 {
			os.Remove(stream.filename)
		}
	}()

	go func() {
		stream.WaitClose()

		stream.mutex.Lock()
		stream.timer.Stop()

		if !stream.recording {
			stream.recording_channel <- false
		}

		stream.mutex.Unlock()

		close(stream.done_channel)
		stream.exit_channel <- &stream
	}()

	stream.WaitStartRecord()
	if !stream.recording {
		return nil, errors.New("no connection to camera")
	}
	return &stream, nil
}

func (s *Stream) WaitClose() {
	for {
		select {
		case <-s.timer.C:
			s.mutex.Lock()
			err := s.cmd.Process.Signal(syscall.SIGHUP)
			s.mutex.Unlock()

			if err != nil {
				log.Println("error to timeout kill process stream rtsp=" + s.rtsp + " filename=" + s.filename)
			} else {
				log.Println("timeout kill process stream rtsp=" + s.rtsp + " filename=" + s.filename)
			}
		case err := <-s.done_channel:
			if err != nil {
				log.Println("error to finish process rtsp=" + s.rtsp + " filename=" + s.filename)
			} else {
				log.Println("finish process successfully rtsp=" + s.rtsp + " filename=" + s.filename)
			}
			return
		}
	}
}

func (s *Stream) WaitStartRecord() bool {
	for {
		select {
		case s.recording = <-s.recording_channel:
			close(s.recording_channel)
			return s.recording
			//default:
		case <-time.After(80 * time.Millisecond):
			fi, err := os.Stat(s.filename)
			if err != nil {
				log.Println("error file size")
				continue
			}
			// get the size
			size := fi.Size()
			log.Println("file size = " + strconv.FormatInt(size, 10))
			if size > 0 {
				s.recording_channel <- true
			}
		}
	}
}

func (s *Stream) ResetTimer(duration int) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	d := time.Duration(duration) * time.Second
	s.timer.Reset(d)
	s.finish_time = time.Now().Add(d)
}

func (s *Stream) Stop() {
	if s.cmd != nil {
		s.mutex.Lock()
		err := s.cmd.Process.Signal(syscall.SIGHUP)
		s.mutex.Unlock()
		if err != nil {
			log.Println("error to stop stream rtsp=" + s.rtsp + " filename=" + s.filename)
		} else {
			log.Println("stop stream rtsp=" + s.rtsp + " filename=" + s.filename)
		}
	}
}

type Service struct {
	e                    *echo.Echo
	close_stream_channel chan *Stream
	streams              map[string]*Stream
	mutex                sync.Mutex
	path                 string
	duration             int
}

func clearPath(path string) string {
	if len(path) == 0 {
		return path
	}
	return filepath.Dir(path) + string(filepath.Separator)
}

func NewService(path string, duration int) (*Service, error) {
	s := Service{
		close_stream_channel: make(chan *Stream),
		streams:              make(map[string]*Stream),
		path:                 clearPath(path),
		duration:             duration,
	}

	e := echo.New()
	s.e = e
	e.Use(middleware.Logger())
	//e.Pre(middleware.RemoveTrailingSlash())
	//e.Logger.SetLevel(log.DEBUG)

	stream := e.Group("/stream")
	stream.POST("/start", ApiStreamStart)
	stream.POST("/stop", ApiStreamStop)
	stream.GET("/stats", ApiStreamStats)
	return &s, nil
}
func (s *Service) Start(host string, port int) {
	go func() {
		for {
			select {
			//case request := <-s.start_channel:
			//	request.channel <- s.internalStartStream(request.desc)
			//case request := <-s.stop_channel:
			//	request.channel <- s.internalStopStream(request.desc)
			case stream := <-s.close_stream_channel:
				s.internalCloseStream(stream)
				//case request := <-s.stats_channel:
				//	request.result_channel <- s.internalStatStreams()
			}
		}
	}()
	//s.e.Logger.Fatal(s.e.Start(":6600"))
	address := host + ":" + strconv.Itoa(port)
	s.e.Logger.Fatal(s.e.Start(address))
}

type DescStartStream struct {
	Rtsp     string `json:"rtsp"`
	Filename string `json:"filename"`
	Duration int    `json:"duration"`
	Login    string `json:"login"`
	Password string `json:"password"`
}

type RequestStartStream struct {
	desc    DescStartStream
	channel chan *Error
}

func (s *Service) StartStream(desc DescStartStream) *Error {
	if s.duration > 0 {
		desc.Duration = s.duration
	}
	if desc.Duration <= 0 {
		return NewErrorString("BadParameters", "duration must be > 0")
	}

	desc.Filename = s.path + desc.Filename

	id := desc.Rtsp + " - " + desc.Filename

	s.mutex.Lock()
	stream, ok := s.streams[id]
	s.mutex.Unlock()

	if ok {
		stream.ResetTimer(desc.Duration)
		return nil
	}

	// проверяем существование файла
	if _, err := os.Stat(desc.Filename); !os.IsNotExist(err) {
		return NewErrorString("FileExist", "file="+desc.Filename+" exist")
	}

	// создаём новый стрим
	stream, err := NewStream(desc, s.close_stream_channel)
	if err != nil {
		return NewErrorString("ErrorCreateStream", "error to create a stream rtsp="+desc.Rtsp+" filename="+desc.Filename+" desc="+err.Error())
	}
	s.mutex.Lock()
	s.streams[id] = stream
	s.mutex.Unlock()
	return nil
}

type DescStopStream struct {
	Rtsp     string `json:"rtsp"`
	Filename string `json:"filename"`
}

type RequestStopStream struct {
	desc    DescStopStream
	channel chan *Error
}

func (s *Service) StopStream(desc DescStopStream) *Error {
	id := desc.Rtsp + " - " + desc.Filename

	s.mutex.Lock()
	stream, ok := s.streams[id]
	s.mutex.Unlock()

	if ok {
		stream.Stop()
		return nil
	}
	return NewErrorString("NoStream", "error to find stream rtsp="+desc.Rtsp+" filename="+desc.Filename)
}

func (s *Service) internalCloseStream(stream *Stream) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	delete(s.streams, stream.id)
}

type DescStream struct {
	Rtsp       string `json:"rtsp"`
	Filename   string `json:"filename"`
	StartTime  string `json:"start_time"`
	FinishTime string `json:"finish_time"`
}

type RequestStatStreams struct {
	result_channel chan []DescStream
}

func (s *Service) StatStreams() []DescStream {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	streams := []DescStream{}
	for _, stream := range s.streams {
		streams = append(streams, DescStream{
			Rtsp:       stream.rtsp,
			Filename:   stream.filename,
			StartTime:  stream.start_time.Format(time.RFC3339),
			FinishTime: stream.finish_time.Format(time.RFC3339),
		})
	}
	return streams
}

var (
	service *Service
)

func ApiStreamStart(c echo.Context) error {
	var desc DescStartStream
	{
		err := json.NewDecoder(c.Request().Body).Decode(&desc)
		if err != nil {
			return c.JSON(http.StatusBadRequest, NewError("BadJSON", err))
		}
	}

	err := service.StartStream(desc)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}
	return nil
}

func ApiStreamStop(c echo.Context) error {
	var desc DescStopStream
	{
		err := json.NewDecoder(c.Request().Body).Decode(&desc)
		if err != nil {
			return c.JSON(http.StatusBadRequest, NewError("BadJSON", err))
		}
	}

	err := service.StopStream(desc)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}
	return nil
}
func ApiStreamStats(c echo.Context) error {
	return c.JSON(http.StatusOK, service.StatStreams())
}

//https://superuser.com/questions/766437/capture-rtsp-stream-from-ip-camera-and-store
//https://manpages.debian.org/testing/livemedia-utils/openRTSP.1.en.html
func main() {
	config, err := NewConfig("config.yml")
	if err != nil {
		log.Println("error to load config" + err.Error())
	}

	service, _ = NewService(config.Path, config.Duration)
	service.Start(config.Host, config.Port)
}

type Config struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	Duration int    `yaml:"duration"`
	Path     string `yaml:"path"`
}

func NewConfig(configPath string) (*Config, error) {
	// Create config structure
	config := &Config{}

	// Open config file
	file, err := os.Open(configPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	// Init new YAML decode
	d := yaml.NewDecoder(file)

	// Start YAML decoding from file
	if err := d.Decode(&config); err != nil {
		return nil, err
	}
	return config, nil
}
