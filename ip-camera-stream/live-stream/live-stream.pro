TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += /home/open/work/external/live555/BasicUsageEnvironment/include/
INCLUDEPATH += /home/open/work/external/live555/UsageEnvironment/include/
INCLUDEPATH += /home/open/work/external/live555/groupsock/include/
INCLUDEPATH += /home/open/work/external/live555/liveMedia/include/



HEADERS +=              \
        playCommon.hh   \

SOURCES +=              \
        openRTSP.cpp    \
        playCommon.cpp  \


LIB4 = /home/open/work/external/live555/groupsock/libgroupsock.a
LIB2 = /home/open/work/external/live555/BasicUsageEnvironment/libBasicUsageEnvironment.a
LIB3 = /home/open/work/external/live555/UsageEnvironment/libUsageEnvironment.a
LIB1 = /home/open/work/external/live555/liveMedia/libliveMedia.a

LIBS += $$LIB1
LIBS += $$LIB2
LIBS += $$LIB3
LIBS += $$LIB4

LIBS += -lssl
LIBS += -lcrypto
