cd vrp-gate-unix/
cp ../../server-build/version version
chmod +x vrp*
zip -r ../../server-build/vrp-gate-unix *
cd ..
mv ../server-build/vrp-gate-unix.zip ../server-build/vrp-gate-unix

cd vrp-hub-unix/
cp ../../server-build/version version
chmod +x vrp*
zip -r ../../server-build/vrp-hub-unix *
cd ..
mv ../server-build/vrp-hub-unix.zip ../server-build/vrp-hub-unix

cd vrp-gate-win/
cp ../../server-build/version version
chmod +x vrp*
zip -r ../../server-build/vrp-gate-win *
cd ..
mv ../server-build/vrp-gate-win.zip ../server-build/vrp-gate-win

cd vrp-hub-win/
cp ../../server-build/version version
chmod +x vrp*
zip -r ../../server-build/vrp-hub-win *
cd ..
mv ../server-build/vrp-hub-win.zip ../server-build/vrp-hub-win