//=====================================================================================//
//   Author: Solari4555
//   Date:   24.09.2018
//=====================================================================================//
#include "precomp.h"
#include "config.h"

Config::Config()
: m_settings("config.ini", QSettings::IniFormat)
{
}
QString Config::value(const QString& name, const QString& default_value) const
{
	QString v = m_settings.value(name).toString();
	return v.isEmpty() ? default_value : v;
}

