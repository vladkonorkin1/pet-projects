//=====================================================================================//
//   Author: Solari4555
//   Date:   24.09.2018
//=====================================================================================//
#pragma once
#include <QSettings>

class Config
{
private:
	QSettings m_settings;

public:
	Config();

public:
	QString value(const QString& name, const QString& default_value=QString()) const;
};
