//=====================================================================================//
//   Author: open
//   Date:   02.08.2018
//=====================================================================================//
#ifndef DYNAMIC_MATRIX_H
#define DYNAMIC_MATRIX_H

template<typename T>
class DynamicMatrix
{
private:
	T* m_data;
	int m_dimension;

public:
	DynamicMatrix(int dimension = 0);
	DynamicMatrix(const DynamicMatrix& src);
	~DynamicMatrix();

	void set(int row, int col, const T& value);
	const T& get(int row, int col) const;
	const T& operator()(int row, int col) const;
	T& operator()(int row, int col);
	int dimension() const;
	const DynamicMatrix& operator = (const DynamicMatrix& src);

private:
	int count_elements() const;
};

template<typename T>
inline void DynamicMatrix<T>::set(int row, int col, const T& value)
{
	assert(row >= 0 && row < m_dimension && col >= 0 && col < m_dimension);
	*(m_data + row*m_dimension + col) = value;
}
template<typename T>
inline const T& DynamicMatrix<T>::get(int row, int col) const
{
	assert(row >= 0 && row < m_dimension && col >= 0 && col < m_dimension);
	return *(m_data + row*m_dimension + col);
}
template<typename T>
inline const T& DynamicMatrix<T>::operator()(int row, int col) const
{
	return get(row, col);
}
template<typename T>
inline T& DynamicMatrix<T>::operator()(int row, int col)
{
	assert(row >= 0 && row < m_dimension && col >= 0 && col < m_dimension);
	return *(m_data + row*m_dimension + col);
}
template<typename T>
inline int DynamicMatrix<T>::dimension() const
{
	return m_dimension;
}
template<typename T>
inline int DynamicMatrix<T>::count_elements() const
{
	return m_dimension*m_dimension;
}

template<typename T>
DynamicMatrix<T>::DynamicMatrix(int dimension)
: m_dimension(dimension)
, m_data(nullptr)
{
	if (m_dimension > 0)
	{
		int count = count_elements();
		m_data = new T[count];
	}
}
template<typename T>
DynamicMatrix<T>::~DynamicMatrix()
{
	delete[] m_data;
	m_data = nullptr;
}
template<typename T>
DynamicMatrix<T>::DynamicMatrix(const DynamicMatrix& src)
: m_dimension(src.m_dimension)
, m_data(nullptr)
{
	if (m_dimension > 0)
	{
		int count = src.count_elements();
		m_data = new T[count];
		for (int i = 0; i < count; ++i)
			m_data[i] = src.m_data[i];
	}
}
template<typename T>
const DynamicMatrix<T>& DynamicMatrix<T>::operator = (const DynamicMatrix<T>& src)
{
	delete[] m_data;
	m_data = nullptr;

	m_dimension = src.m_dimension;
	if (m_dimension > 0)
	{
		int count = src.count_elements();
		m_data = new T[count];
		for (int i = 0; i < count; ++i)
			m_data[i] = src.m_data[i];
	}
	return *this;
}

#endif