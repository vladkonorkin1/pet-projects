//=====================================================================================//
//   Author: open
//   Date:   22.09.2018
//=====================================================================================//
#pragma once
#include <base/dynamic_matrix.h>
#include <base/bin_stream.h>

typedef float distance_t;

struct DistanceDesc
{
	distance_t distance;
	float duration;
	
	static distance_t NotInitialize;

	explicit DistanceDesc(distance_t distance = NotInitialize, float duration = NotInitialize) : distance(distance), duration(duration) {}
	bool is_initialized() const { return distance != NotInitialize && duration != NotInitialize; }
};

class MatrixDistances : public DynamicMatrix<DistanceDesc>
{
public:
	MatrixDistances(int dimension = 0);
	bool is_initialized() const;
};

using SMatrixDistances = std::shared_ptr<MatrixDistances>;

struct MatrixDistanceIndex
{
	int start;
	int end;
};

using matrix_distance_indices_t = std::vector<MatrixDistanceIndex>;

inline Base::OBinStream& operator<<(Base::OBinStream& stream, const SMatrixDistances& matrix)
{
	assert( matrix );
	stream << static_cast<unsigned short int>(matrix->dimension());
	for (int i = 0; i < matrix->dimension(); ++i)
	{
		for (int j = 0; j < matrix->dimension(); ++j)
		{
			stream << (*matrix)(i, j).distance;
			stream << (*matrix)(i, j).duration;
		}
	}
	return stream;
}
inline Base::IBinStream& operator >> (Base::IBinStream& stream, SMatrixDistances& matrix)
{
	unsigned short int dimension = 0;
	stream >> dimension;
	matrix.reset(new MatrixDistances(dimension));
	for (int i = 0; i < dimension; ++i)
	{
		for (int j = 0; j < dimension; ++j)
		{
			DistanceDesc desc;
			stream >> desc.distance;
			stream >> desc.duration;
			matrix->set(i, j, desc);
		}
	}
	return stream;
}