//=====================================================================================//
//   Author: open
//   Date:   10.10.2017
//=====================================================================================//
#include "base/precomp.h"
#include "network_manager.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QAuthenticator>
#include <QNetworkReply>

NetworkManager::NetworkManager()
: m_reconnect_timer(0.5f)
{
	connect(&m_manager, SIGNAL(finished(QNetworkReply*)), SLOT(slot_download_finished(QNetworkReply*)));
	connect(&m_reconnect_timer, SIGNAL(updated(float)), SLOT(on_reconnect_timer(float)));
}
void NetworkManager::request(SNetworkObject obj, NetworkMethod method, float wait_duration)
{
	NetworkObject* pobj = add_obj(obj);
	QNetworkRequest request = get_request(pobj);
	QNetworkReply* reply = nullptr;
	switch (method)
	{
		case NetworkMethod::Post:
			reply = m_manager.post(request, pobj->send_buffer());
			break;
		case NetworkMethod::Patch:
			reply = m_manager.sendCustomRequest(request, "PATCH", pobj->send_buffer());
			break;
		case NetworkMethod::Get:
		default:
			reply = m_manager.get(request);
	}
	pobj->set_method(method);
	pobj->set_timeout(wait_duration);
	pobj->set_reply(reply);
}
QNetworkRequest NetworkManager::get_request(NetworkObject* obj) const
{
	QNetworkRequest request(obj->url());
	request.setAttribute(QNetworkRequest::User, QVariant::fromValue<NetworkObject*>(obj));
	//logs("net: " + obj->url());

	for (const auto& pair : m_headers)
		request.setRawHeader(pair.first, pair.second);

	return request;
}
NetworkObject* NetworkManager::add_obj(SNetworkObject obj)
{
	NetworkObject* pobj = obj.get();
	m_objects[pobj] = obj;
	return pobj;
}
void NetworkManager::slot_download_finished(QNetworkReply* reply)
{
#ifdef _DEBUG
	debug_parse_reply(reply);
#endif

	NetworkObject* pobj = reply->request().attribute(QNetworkRequest::User).value<NetworkObject*>();
	objects_t::iterator it = m_objects.find(pobj);
	if (it != m_objects.end())
	{
		if (!it->second.expired()) {
			SNetworkObject obj = it->second.lock();
			assert( pobj == obj.get() );

			if (reply->error() != QNetworkReply::NetworkError::NoError)
			{
				int http_status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
				 logs(QString("net:  !  %1  status: %2  error: %3").arg(pobj->url()).arg(http_status).arg(reply->errorString()));
			}
			//else logs(QString("net:  => %1  size: %2").arg(pobj->url()).arg(reply->size()));

			on_process_object(obj, reply);
			obj->set_reply(nullptr);
		}
		m_objects.erase(it);
		reply->deleteLater();
	}
}
void NetworkManager::on_process_object(SNetworkObject obj, QNetworkReply* reply)
{
	obj->process(reply, reply->error() == QNetworkReply::NetworkError::NoError);
}
#ifdef _DEBUG
void NetworkManager::debug_parse_reply(QNetworkReply* reply)
{
	QString error_id = QString::number(reply->error());
	QUrl url = reply->request().url();
	int http_status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
	QByteArray http_status_msg = reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toByteArray();
	QString error_str = reply->errorString();
	if (reply->error() != QNetworkReply::NetworkError::NoError)
	{
		//QByteArray data = reply->readAll();
		//qDebug() << data;
		__nop();
	}
}
#endif
void NetworkManager::reconnect(SNetworkObject obj, float delay)
{
	reconnected_items_t::iterator it = m_reconnected_items.find(obj.get());
	if (m_reconnected_items.end() == it) m_reconnected_items.insert(std::make_pair(obj.get(), ReconnectedItem{ obj, 0.f, delay }));
}
void NetworkManager::on_reconnect_timer(float dt)
{
	for (reconnected_items_t::iterator it = m_reconnected_items.begin(); it != m_reconnected_items.end(); )
	{
		ReconnectedItem& item = it->second;
		if (!item.obj.expired())
		{
			SNetworkObject sobj = item.obj.lock();
			item.timer_value += dt;
			if (item.timer_value < item.timer_delay) it++;
			else {
				request(sobj, sobj->method());
				reconnected_items_t::iterator it_erase = it++;
				m_reconnected_items.erase(it_erase);
			}
		}
		else {
			reconnected_items_t::iterator it_erase = it++;
			m_reconnected_items.erase(it_erase);
		}
	}
}
void NetworkManager::set_header(const QString& header, const QString& value)
{
	m_headers[header.toUtf8()] = value.toUtf8();
}