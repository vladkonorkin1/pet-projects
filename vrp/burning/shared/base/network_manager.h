//=====================================================================================//
//   Author: open
//   Date:   10.10.2017
//=====================================================================================//
#ifndef NETWORK_MANAGER
#define NETWORK_MANAGER
#include <QNetworkAccessManager>
#include "base/network_object.h"
#include "base/timer.h"

class NetworkManager : public QObject
{
	Q_OBJECT
private:
	QNetworkAccessManager m_manager;
	using objects_t = std::map<NetworkObject*, WNetworkObject>;
	objects_t m_objects;

	Base::Timer m_reconnect_timer;
	struct ReconnectedItem
	{
		WNetworkObject obj;
		float timer_value;
		float timer_delay;
	};
	using reconnected_items_t = std::map<NetworkObject*, ReconnectedItem>;
	reconnected_items_t m_reconnected_items;

	using headers_t = std::map<QByteArray, QByteArray>;
	headers_t m_headers;

public:
	NetworkManager();
	void request(SNetworkObject obj, NetworkMethod method, float wait_duration = 20.f);
	void reconnect(SNetworkObject obj, float delay = 6.f);
	void set_header(const QString& header, const QString& value);

protected:
	virtual void on_process_object(SNetworkObject obj, QNetworkReply* reply);

private slots:
	void slot_download_finished(QNetworkReply* reply);
	void on_reconnect_timer(float dt);

private:
	QNetworkRequest get_request(NetworkObject* obj) const;
	NetworkObject* add_obj(SNetworkObject obj);

#ifdef _DEBUG
	void debug_parse_reply(QNetworkReply* reply);
#endif
};

#endif