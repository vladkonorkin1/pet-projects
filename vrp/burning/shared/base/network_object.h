//=====================================================================================//
//   Author: open
//   Date:   04.05.2018
//=====================================================================================//
#ifndef NETWORK_OBJECT
#define NETWORK_OBJECT
#include <QTimer>

class QNetworkReply;

class NetworkObject;
using SNetworkObject = std::shared_ptr<NetworkObject>;
using WNetworkObject = std::weak_ptr<NetworkObject>;

enum class NetworkMethod
{
	Get,
	Post,
	Patch,
	MaxMethod
};

class NetworkObject : public QObject
{
	Q_OBJECT
private:
	NetworkMethod m_method;

	QNetworkReply* m_reply;

	QTimer m_timer_wait;
	float m_timeout;

public:
	NetworkObject();
	virtual ~NetworkObject();
	void set_timeout(float timeout);

	virtual QString url() const = 0;
	virtual void process(QNetworkReply* reply, bool success) = 0;

	void set_reply(QNetworkReply* reply);
	virtual const QByteArray& send_buffer() const;

	void set_method(NetworkMethod method);
	NetworkMethod method() const;

private:
	void abort();

private slots:
	void on_wait_timeout();
};


class PostNetworkObject : public NetworkObject
{
	Q_OBJECT
protected:
	QByteArray m_send_buffer;

public:
	//PostNetworkObject() {}
	//PostNetworkObject(const QByteArray& send_buffer) : m_send_buffer(send_buffer) {}
	virtual const QByteArray& send_buffer() const { return m_send_buffer; }
};


class DownloadData : public NetworkObject
{
	Q_OBJECT
private:
	QString m_url;

public:
	DownloadData(const QString& url);
	virtual void process(QNetworkReply* reply, bool success);
	virtual QString url() const;

signals:
	void finished(bool success, const QByteArray& data);
};

#endif
