//=====================================================================================//
//   Author: open
//   Date:   03.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "random_indices.h"

RandomIndices::RandomIndices(int start_index, int count, int seed)
: m_rand(seed)
, m_index(0)
{
	m_indices.resize(count);
	for (int i = 0; i < count; ++i)
		m_indices[i] = i + start_index;
}
// сбросить и перемешать индексы
void RandomIndices::reset_random()
{
	m_index = 0;
	unsigned int count = (int)m_indices.size();
	for (int i = 0; i < count; ++i)
		std::swap(m_indices[i], m_indices[m_rand() % count]);
}
// запросить следующий случайный индекс из последовательности
int RandomIndices::get_random_index()
{
	if (m_index < m_indices.size())
		return m_indices[m_index++];
	//assert( false );
	return RandomIndices::Null;
}
