//=====================================================================================//
//   Author: open
//   Date:   04.07.2018
//=====================================================================================//
#include "base/precomp.h"
#include "thread_task_manager.h"
#include <QCoreApplication>
#include <QEvent>


class ThreadStartTaskEvent : public QEvent
{
private:
	ThreadProvider* m_thread_provider;
	ThreadTask* m_thread_task;

public:
	ThreadStartTaskEvent(ThreadProvider* thread_provider, ThreadTask* thread_task) : m_thread_provider(thread_provider), m_thread_task(thread_task), QEvent(QEvent::User)
	{
	}
	//virtual ~ThreadStartTaskEvent()
	//{
	//	__nop();
	//}
	void process()
	{
		m_thread_task->on_process();
		m_thread_provider->emit_finished();
	}
};


ThreadProvider::ThreadProvider(const QString& thread_name)
{
	m_thread.setObjectName(thread_name);
}
ThreadProvider::~ThreadProvider()
{
	close();
}
void ThreadProvider::close()
{
	m_thread.quit();
	m_thread.wait();
}
void ThreadProvider::start_thread()
{
	moveToThread(&m_thread);
	m_thread.start();
}
void ThreadProvider::post_task(const SThreadTask& thread_task)
{
	assert( !m_thread_task );
	assert( QThread::currentThread() != &m_thread );

	m_thread_task = thread_task;
	ThreadStartTaskEvent* event = new ThreadStartTaskEvent(this, thread_task.get());
	QCoreApplication::postEvent(this, event);
}
void ThreadProvider::emit_finished()
{
	assert( QThread::currentThread() == &m_thread );
	emit finished(this);
}
void ThreadProvider::finish_task()
{
	assert( QThread::currentThread() != &m_thread );
	
	WThreadTask wtask(m_thread_task);
	m_thread_task.reset();
	if (!wtask.expired())
	{
		SThreadTask task = wtask.lock();
		task->on_finish_process();
	}
}
bool ThreadProvider::event(QEvent* event)
{
	if (event->type() == QEvent::User)
	{
		assert( QThread::currentThread() == &m_thread );
		if (ThreadStartTaskEvent* query_event = dynamic_cast<ThreadStartTaskEvent*>(event))
		{
			//std::this_thread::sleep_for(std::chrono::milliseconds(100));
			query_event->process();
		}
	}
	return true;
}


ThreadTaskManager::Batch::Batch(const tasks_t& tasks)
: count_processed(0)
, index(0)
, sended(0)
, tasks(tasks)
{
}
SThreadTask ThreadTaskManager::Batch::next_task()
{
	while (index < (int)tasks.size())
	{
		if (!tasks[index].expired())	// if alive
			return tasks[index++].lock();
		index++;
	}
	static SThreadTask temp;
	return temp;
}
bool ThreadTaskManager::Batch::is_finish() const {
	return sended == count_processed && index == tasks.size();
}


ThreadTaskManager::ThreadTaskManager()
{
	//int count = std::max(QThread::idealThreadCount() - 1, 1);
	int count = std::max(QThread::idealThreadCount(), 3);
	//count = 1;
	//count = std::min(count, 8);
	logs(QString("count threads: %1").arg(count));
	m_pool.reserve(count);
	for (int i = 0; i < count; ++i)
	{
		UThreadProvider thread_provider(new ThreadProvider(QString("thread task %1").arg(i)));
		connect(thread_provider.get(), SIGNAL(finished(ThreadProvider*)), SLOT(slot_thread_finished(ThreadProvider*)), Qt::ConnectionType::QueuedConnection);
		thread_provider->start_thread();
		m_free.push(thread_provider.get());
		m_pool.emplace_back(std::move(thread_provider));
	}
}
void ThreadTaskManager::add_tasks(const tasks_t& tasks)
{
	m_batches.emplace(Batch(tasks));
	start_batch_tasks();
}
ThreadTaskManager::Batch* ThreadTaskManager::get_last_batch()
{
	if (m_batches.empty()) return nullptr;
	return &(m_batches.front());
}
void ThreadTaskManager::add_thread_task(const SThreadTask& thread_task)
{
	assert( !m_free.empty() );
	ThreadProvider* thread_provider = m_free.top();
	m_free.pop();
	m_worked.push_back(thread_provider);
	thread_provider->post_task(thread_task);
}
void ThreadTaskManager::start_batch_tasks()
{
	while (Batch* batch = get_last_batch())
	{
		while (!m_free.empty())
		{
			if (SThreadTask thread_task = batch->next_task()) {
				add_thread_task(thread_task);
				batch->sended++;
			}
			else break;
		}

		// проверяем... все ли исполнены
		if (batch->is_finish())
			m_batches.pop();
		else break;
	}
}
void ThreadTaskManager::slot_thread_finished(ThreadProvider* thread_provider)
{
	thread_provider->finish_task();

	// удаляем из списка рабочих потоков
	for (worked_t::iterator it=m_worked.begin(); it!=m_worked.end(); it++)
	{
		if (*it == thread_provider) {
			m_worked.erase(it);
			break;
		}
	}
	// добавляем в список свободных
	m_free.push(thread_provider);

	// увеличиваем счетчик
	if (Batch* batch = get_last_batch())
		batch->count_processed++;
	// запускаем ещё раз задачи
	start_batch_tasks();
}
int ThreadTaskManager::count_threads() const
{
	return m_pool.size();
}
void ThreadTaskManager::wait_close()
{
	m_batches = {};
	m_worked.clear();
	m_free = {};
	m_pool.clear();
}