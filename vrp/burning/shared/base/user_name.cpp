//=====================================================================================//
//   Author: open
//   Date:   10.11.2018
//=====================================================================================//
#include "base/precomp.h"
#include "user_name.h"

#ifdef Q_OS_WIN
#include <windows.h>
#include <lmcons.h>
#elif defined Q_OS_UNIX
#include <pwd.h>
#include <unistd.h>
#endif

QString user_name()
{
#ifdef Q_OS_WIN
	WCHAR* lpszSystemInfo;
	DWORD cchBuff = UNLEN;
	WCHAR tchBuffer[UNLEN + 1];
	lpszSystemInfo = tchBuffer;
	GetUserNameW(lpszSystemInfo, &cchBuff);
	return QString::fromWCharArray(lpszSystemInfo);
#elif defined Q_OS_UNIX
	struct passwd* pw;
	pw = getpwuid(getuid());
	if (pw)
		return pw->pw_name;
	return QString();
#endif
}
