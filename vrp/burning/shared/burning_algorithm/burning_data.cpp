//=====================================================================================//
//   Author: open
//   Date:   17.10.2018
//=====================================================================================//
#include "base/precomp.h"
#include "burning_data.h"
#include <random> 

namespace Burning
{

Avto::Avto(const Penalties& penalties)
: index(0)
, max_weight(0)
, max_volume(0)
, cost_km(0)
, cost_point(0)
, cost_trip(0)
, speed_city(0)
, speed_highway(0)
, time_from(0)
, time_to(0)
, start_time_after_relax(0)
, available_count(0)
, num_nights(0)
, penalties(penalties)
{
}
void Avto::prepare_time_price_matrix(const destination_points_t& destination_points, const MatrixDistances& matrix)
{
	// создаём матрицу по длительности и стоимости проезда между точками 
	m_time_price_matrix.reset(new TimePriceMatrix(destination_points.size()));
	for (int i = 0; i < matrix.dimension(); ++i)
	{
		for (int j = 0; j < matrix.dimension(); ++j)
		{
			const DistanceDesc& desc = matrix(i, j);
			distance_t distance = desc.distance;
			TimePrice& time_price = (*m_time_price_matrix)(i, j);
			//time_price.price = destination_points[i].city == destination_points[j].city ? 0 : distance * cost_km;
			time_price.price = distance * cost_km;
			time_price.time = distance / (destination_points[i].city == destination_points[j].city ? speed_city : speed_highway) * 3600;
			//double pre_time = distance / (destination_points[i].city == destination_points[j].city ? speed_city : speed_highway) * 3600;
			//time_price.time = desc.duration;
			//logs(QString().sprintf("prepare_time_price_matrix: %.6f, %.6f - %.6f, %.6f  %.0f\t%.0f\t%d\t%.2f", destination_points[i].lat, destination_points[i].lon, destination_points[j].lat, destination_points[j].lon, pre_time, time_price.time, destination_points[i].city == destination_points[j].city ? 1 : 0, pre_time / time_price.time));
		}
	}
}
void Avto::add_pass_closed_city(const City& city)
{
	m_pass_closed_cities.insert(std::make_pair(city.id, city));
}
bool Avto::is_have_pass_closed_city(const City& city) const
{
	return m_pass_closed_cities.find(city.id) != m_pass_closed_cities.end();
}
double Avto::get_time_between_points(int start_index, int end_index) const
{
	return m_time_price_matrix->get(start_index, end_index).time;
}
double Avto::get_price_between_points(int start_index, int end_index) const
{
	return m_time_price_matrix->get(start_index, end_index).price;
}
void Avto::add_preferred_zone(const QUuid& zone_id)
{
	m_preferred_zones.insert(zone_id);
}
bool Avto::is_preferred_zone(const QUuid& zone_id) const
{
	return m_preferred_zones.find(zone_id) != m_preferred_zones.end();
}



Route::Route(const Avto* avto)
: avto(avto)
, volume(0)
, weight(0)
, price_total(0)
, price_clean(0)
, idle_time(0)
, idle_time_first_point(0)
//, penalty_good(0)
//, penalty_bad(0)
{
}
void Route::add_point(const DestinationPoint* point)
{
	points.emplace_back(RoutePoint(point));
}
void Route::insert_point(int insert_index, const DestinationPoint* point)
{
	points.insert(points.begin() + insert_index, RoutePoint(point));
}
void Route::remove_point(int index)
{
	points.erase(points.begin() + index);
}
void Route::remove_points(int index_start, int index_end)
{
	points.erase(points.begin() + index_start, points.begin() + index_end + 1);
}
int Route::insert_point_to_optimal_position(const DestinationPoint* point)
{
	double min_cost_distance = std::numeric_limits<double>::max();
	int insert_index = 1;
	for (int i = 0; i < points.size() - 1; ++i)
	{
		double cost_distance = avto->get_price_between_points(points[i].destination_point->index, point->index)
			+ avto->get_price_between_points(point->index, points[i + 1].destination_point->index);
		if (cost_distance < min_cost_distance)
		{
			min_cost_distance = cost_distance;
			insert_index = i + 1;
		}
	}
	insert_point(insert_index, point);
	return insert_index;
}
// обновить объём и вес
void Route::update_volume_weight()
{
	volume = 0;
	weight = 0;
	for (int i = 1; i < points.size() - 1; ++i)
	{
		const DestinationPoint* dest_point = points[i].destination_point;
		volume += dest_point->volume;
		weight += dest_point->weight;
	}
}
// обновить время и цену
void Route::update_time_price()
{
	// обновляем время
	update_time(0, 0, true);
	// ищем простой на первой точке
	find_idle_first_point();
	// обновляем стоимость
	update_price();
}
// обновить время с упаковкой и цену
void Route::update_pack_time_price()
{
	// обновляем время
	update_time(0, 0, true);
	// упаковываем время
	pack_timings();
	// ищем простой на первой точке
	find_idle_first_point();
	// обновляем цену
	update_price();
}
/*void Route::update_time()
{
	update_timings(0, 0, true);

	// упаковываем время
	//pack_timings();

	// ищем простой на первой точке
	find_idle_first_point();
}*/
// обновить тайминги на точках
void Route::update_time(int cur_time, int cur_day, bool move_to_first_point)
{
	idle_time = 0;
	idle_time_first_point = 0;

	if (points.size() > 1)
	{
		{
			// прибываем на первую точку к её открытию
			RoutePoint& route_point = points[1];
			if (move_to_first_point)
				cur_time = route_point.destination_point->time_from;
			update_route_point_time(cur_time, cur_day, route_point);

			// обновляем время выезда для первой точки
			double time_moving = get_time_between_points(0, 1);
			int first_time = cur_time - time_moving;

			points[0].time_arrival = first_time % (24 * 60 * 60);
			points[0].day_arrival = first_time / (24 * 60 * 60);
			/*if (first_time < 0)
			{
				points[0].time_arrival = (24 * 60 * 60) + points[0].time_arrival;
				points[0].day_arrival--;
			}*/
		}

		// для следующих точек расчитываем затраченое время на поездку
		for (int i = 2; i < points.size(); ++i)
		{
			RoutePoint& route_point = points[i];
			// машина едет к точке
			double time_moving = get_time_between_points(i - 1, i);
			cur_time += time_moving;

			update_route_point_time(cur_time, cur_day, route_point);
		}
	}
}
void Route::update_route_point_time(int& cur_time, int& cur_day, RoutePoint& route_point)
{
	int last_day = cur_day;
	// если машина приехала к окончанию смены, то плюсуем ночёвку
	while (cur_time > avto->time_to)
	{
		cur_day++;
		cur_time -= avto->time_to;
		cur_time += avto->start_time_after_relax;
	}

	route_point.idle_time = 0;
	// если машина приехала на точку раньше открытия
	if (cur_time < route_point.destination_point->time_from)
	{
		if (last_day == cur_day)
			route_point.idle_time = route_point.destination_point->time_from - cur_time;

		idle_time += route_point.idle_time;		//std::max(route_point.idle_time - avto->penalties.valid_idle_time, 0);
		cur_time = route_point.destination_point->time_from;
	}

	route_point.time_arrival = cur_time;
	route_point.day_arrival = cur_day;

	// производим разгрузку заказов
	cur_time += route_point.destination_point->time_discharge;
}
// упаковываем время
void Route::pack_timings()
{
	if (points.size() > 3)
	{
		for (int i = 2; i < points.size() - 1; ++i)
		{
			// ищем простой
			const RoutePoint& route_point = points[i];
			if (route_point.day_arrival != 0) break;
			if (route_point.idle_time > 0)
			{
				pack_point_times(i);
				break;
			}
		}
	}
}
void Route::pack_point_times(int start_point_index)
{
	const RoutePoint& route_start_point = points[start_point_index];
	// нашли первый простой
	int cur_time = route_start_point.time_arrival;

	for (int i = start_point_index - 1; i > 0; --i)
	{
		const RoutePoint& route_point = points[i];
		
		// вычитаем время поездки
		double time_moving = get_time_between_points(i, i + 1);
		cur_time -= time_moving;	// текущее время выезда с точки

		// вычитаем время разгрузки
		cur_time -= route_point.destination_point->time_discharge;		// текущее время приезда на точку

		// если приехали на точку позже закрытия
		if (cur_time > route_point.destination_point->time_to)
		{
			cur_time = route_point.destination_point->time_to;
		}
	}
	
	// если время сместилось
	if (cur_time > points[1].time_arrival)
	{
		// пересчитываем время
		update_time(cur_time, 0, false);
	}
}
// ищем простой на первой точке
void Route::find_idle_first_point()
{
	if (points.size() > 3)
	{
		int last_day = 0;
		int start_day_point_index = 1;
		int min_time = std::numeric_limits<int>::max();
		for (int i = 2; i < points.size() - 1; ++i)
		{
			RoutePoint& route_point = points[i];
			if (route_point.day_arrival != last_day)
			{
				last_day = route_point.day_arrival;

				if (min_time < points[start_day_point_index].destination_point->time_from)
					idle_time_first_point += points[start_day_point_index].destination_point->time_from - min_time;

				start_day_point_index = i;
				min_time = std::numeric_limits<int>::max();
			}

			if (min_time > route_point.destination_point->time_from)
				min_time = route_point.destination_point->time_from;
		}

		if (min_time < points[start_day_point_index].destination_point->time_from)
			idle_time_first_point += points[start_day_point_index].destination_point->time_from - min_time;
	}
}
void Route::update_price()
{
	price_clean = 0;
	price_total = 0;
	double penalty_bad = 0;
	double penalty_good = 0;

	if (points.size() <= 2) return;

	const double& penalty_over_volume = avto->penalties.penalty_over_volume;
	const double& penalty_over_weight = avto->penalties.penalty_over_weight;
	const double& penalty_delay = avto->penalties.penalty_delay;
	//const double& penalty_spend_night = avto->penalties.penalty_spend_night;
	const double& penalty_change_city = avto->penalties.penalty_change_city;
	const double& penalty_change_priority = avto->penalties.penalty_change_priority;
	const double& penalty_change_zone = avto->penalties.penalty_change_zone;
	const double& penalty_no_pass_in_closed_city = avto->penalties.penalty_no_pass_in_closed_city;
	//const double& penalty_idle = avto->penalties.penalty_idle;
	const double& penalty_idle_first_point = avto->penalties.penalty_idle_first_point;
	const double& penalty_time = avto->penalties.penalty_time;
	const double& penalty_small_volume = avto->penalties.penalty_small_volume;
	const double& penalty_percent_limit_small_volume = avto->penalties.penalty_percent_limit_small_volume;
	const double& penalty_unpreferred_zone = avto->penalties.penalty_unpreferred_zone;
	const double& penalty_volume_unloading = avto->penalties.penalty_volume_unloading;

	// стоимость пути - это стоимость рейса автомобиля
	price_clean = avto->cost_trip;
	// + стоимость посещения точек
	price_clean += avto->cost_point * (points.size() - 2);
	// + стоимость ночёвок водителей
	//price_clean += points.back().day_arrival * penalty_spend_night;

	// проходим по всем точкам, кроме первой - точки склада
	for (int i = 1; i < points.size(); ++i)
	{
		// + стоимость проезда за км
		price_clean += get_price_between_points(i - 1, i);
	}

	// проходим по всем точкам, кроме точек склада в начале и в конце
	for (int i = 1; i < points.size() - 1; ++i)
	{
		const RoutePoint& route_point = points[i];
		// штраф за посещение закрытого города без пропуска
		const City* city = route_point.destination_point->city;
		if (city->closed && !avto->is_have_pass_closed_city(*city))
			penalty_good += penalty_no_pass_in_closed_city;	// 100000

		// штраф за 
		assert( route_point.destination_point->zone );
		if (!avto->is_preferred_zone(route_point.destination_point->zone->id))
		{
			penalty_good += penalty_unpreferred_zone;
		}

		// + стоимость просрочки доставки груза
		if (avto->num_nights < route_point.day_arrival)
		{
			// задержку на сутки оцениваю пока в 72000 секунд вместо 86400
			penalty_bad += (20 * 3600 * (route_point.day_arrival - avto->num_nights) + route_point.time_arrival - route_point.destination_point->time_max) * penalty_delay;
		}
		else if (route_point.time_arrival > route_point.destination_point->time_max)
		{
			penalty_bad += (route_point.time_arrival - route_point.destination_point->time_max) * penalty_delay;
		}
	}

	for (int i = 1; i < points.size() - 2; ++i)
	{
		// штраф за смену города
		if (points[i].destination_point->city != points[i + 1].destination_point->city)
			penalty_good += penalty_change_city;	// 50
		// штраф за смену геозоны
		if (points[i].destination_point->zone != points[i + 1].destination_point->zone)
			penalty_good += penalty_change_zone;	// 1000
		// штраф за увеличение приоритета посещаемого города
		if (points[i].destination_point->city->priority < points[i + 1].destination_point->city->priority)
			penalty_good += penalty_change_priority;	// 100000
	}

	// стоимость перегруза за кг
	if (weight > avto->max_weight)
		penalty_bad += penalty_over_weight * (weight - avto->max_weight);
	
	// стоимость излишнего объема за м^3
	if (volume > avto->max_volume)
		penalty_bad += penalty_over_volume * (volume - avto->max_volume);

	// штраф за недообъём
	double percent = volume / avto->max_volume;
	if (percent < penalty_percent_limit_small_volume)	// 0.85
	{
		double nedo_volume = avto->max_volume - volume;
		penalty_good += nedo_volume * penalty_small_volume;	// 500
	}

	penalty_good += idle_time_first_point * penalty_idle_first_point;
	//penalty_good += idle_time * penalty_idle;

	if (points.size() > 2)
	{
		/*int start_time = points[1].day_arrival * 24 * 60 * 60 + points[1].time_arrival;
		const RoutePoint& finish_point = points[points.size() - 2];
		int finish_time = finish_point.day_arrival * 24 * 60 * 60 + finish_point.time_arrival;
		penalty_good += (finish_time - start_time) * penalty_time;*/

		// штраф за время - оцениваем по общей дистанции: от склада до склада
		int start_time = points[0].day_arrival * 24 * 60 * 60 + points[0].time_arrival;
		const RoutePoint& finish_point = points.back();	//points[points.size() - 1];
		int finish_time = finish_point.day_arrival * 24 * 60 * 60 + finish_point.time_arrival;
		penalty_good += (finish_time - start_time) * penalty_time;
	}

	// штраф за разгрузку
	if (volume > 0.)	// div zero
	{
		double price_unloading = 0.;
		double next_volume = volume;
		double max_volume = volume;
		// проходим по всем точкам, кроме точек склада
		for (int i = 1; i < points.size() - 1; ++i)
		{
			double percent_volume = next_volume / max_volume;
			// стоимость проезда за км * процент загрузки машины
			price_unloading += get_price_between_points(i - 1, i) * percent_volume;
			// разгружаем машину
			next_volume -= points[i].destination_point->volume;
		}
		price_unloading *= penalty_volume_unloading;
		penalty_good += price_unloading;
	}

	price_total = price_clean + penalty_bad + penalty_good;
}
double Route::get_time_between_points(int start_index, int end_index) const
{
	return avto->get_time_between_points(points[start_index].destination_point->index, points[end_index].destination_point->index);
}
double Route::get_price_between_points(int start_index, int end_index) const
{
	return avto->get_price_between_points(points[start_index].destination_point->index, points[end_index].destination_point->index);
}
double Route::get_distance_between_points(const MatrixDistances& matrix_distances, int start_index, int end_index) const
{
	return matrix_distances.get(points[start_index].destination_point->index, points[end_index].destination_point->index).distance;
}
double Route::calc_distance(const MatrixDistances& matrix_distances) const
{
	double distance = 0.;
	for (int i = 1; i < points.size(); ++i)
		distance += get_distance_between_points(matrix_distances, i - 1, i);
	return distance;
}


Plan::Plan()
: m_price_total(0)
, m_price_clean(0)
//, m_penalty_good(0)
//, m_penalty_bad(0)
{
}
Plan::Plan(const Plan& src)
: m_price_total(src.m_price_total)
, m_price_clean(src.m_price_clean)
//, m_penalty_good(src.m_penalty_good)
//, m_penalty_bad(src.m_penalty_bad)
{
	for (const Route* route : src.m_routes)
	{
		Route* new_route = new Route(route->avto);
		*new_route = *route;
		m_routes.push_back(new_route);
	}
}
Plan::~Plan()
{
	std::for_each(m_routes.begin(), m_routes.end(), [](Route* route) { delete route; });
}
Route* Plan::add_route(const Avto* avto)
{
	Route* route = new Route(avto);
	m_routes.push_back(route);
	return route;
}
void Plan::remove_route(int index)
{
	assert( index >= 0 && index < static_cast<int>(m_routes.size()) );
	routes_t::iterator it = m_routes.begin() + index;
	delete *it;
	//m_routes.erase(it);
	*it = m_routes.back();
	m_routes.resize(m_routes.size() - 1);
}
void Plan::replace_route(int index, Route* route)
{
	Route* old_route = m_routes[index];
	delete old_route;
	m_routes[index] = route;
}
void Plan::update_time_price(bool recalc)
{
	m_price_total = 0;
	m_price_clean = 0;
	//m_penalty_good = 0;
	//m_penalty_bad = 0;

	for (Route* route : m_routes)
	{
		if (recalc) route->update_time_price();
		m_price_total += route->price_total;
		m_price_clean += route->price_clean;
		//m_penalty_good += route->penalty_good;
		//m_penalty_bad += route->penalty_bad;
	}
}
double Plan::calc_distance(const MatrixDistances& matrix_distances) const
{
	double distance = 0.;
	for (Route* route : m_routes)
		distance += route->calc_distance(matrix_distances);
	return distance;
}


const Zone* Data::add_zone(const QUuid& zone_id)
{
	zones_t::const_iterator it = zones.find(zone_id);
	if (zones.end() == it)
		it = zones.insert(std::make_pair(zone_id, Zone{ zone_id })).first;
	return &(it->second);
}


Random::Random(int seed)
: m_rand(seed)
{
	assert( seed >= 0 );
}
int random_range(unsigned int r, unsigned int min, unsigned int max)
{
	return (r % (max - min + 1)) + min;
}
int Random::range(int min, int max)
{
	//std::uniform_int_distribution<int> range(min, max);	// don't use: no cross-platform
	//int v = range(m_rand);
	int v = random_range(m_rand(), min, max);
	//logs(QString("int Random::range(int min, int max) = %1 (%2, %3)").arg(v).arg(min).arg(max));
	return v; 
}
int Random::value()
{
	return m_rand();
}
int Random::value(int limit)
{
	return m_rand() % limit;
}
double Random::real_value(double min, double max)
{
	//std::uniform_real_distribution<> range(min, max);	// don't use: no cross-platform
	//return range(m_rand);
	unsigned int r = m_rand();
	double lenght = max - min;
	double percent = (double)r / (double)std::numeric_limits<uint32_t>::max();
	double v = min + percent * lenght;
	//logs(QString("double Random::real_value(double min, double max) = %1 (%2, %3)").arg(v).arg(min).arg(max));
	return v;
}

}
