//=====================================================================================//
//   Author: open
//   Date:   17.10.2018
//=====================================================================================//
#pragma once
#include "base/matrix_distances.h"
#include <QPointF>
#include <random>

namespace Burning
{

class City
{
public:
	int index;
	QString name;
	QUuid id;
	int priority;
	bool closed;

	City()
	: index(0)
	, priority(0)
	, closed(false)
	{
	}
};
using cities_t = std::vector<City>;


struct Zone
{
	QUuid id;
};
using zones_t = std::map<QUuid, Zone>;


class DestinationPoint
{
public:
	int index;
	const City* city;
	double lat;
	double lon;
	double weight;
	double volume;
	int time_from;
	int time_to;
	int time_max;
	int time_discharge;
	const Zone* zone;

	DestinationPoint()
	: index(0)
	, city(nullptr)
	, lat(0)
	, lon(0)
	, weight(0)
	, volume(0)
	, time_from(0)
	, time_to(0)
	, time_max(0)
	, time_discharge(0)
	, zone(nullptr)
	{
	}
};

using destination_points_t = std::vector<DestinationPoint>;

struct TimePrice
{
	double time;	// время передвижения между точками
	double price;	// стоимость передвижения между точками
};

using TimePriceMatrix = DynamicMatrix<TimePrice>;
using STimePriceMatrix = std::shared_ptr<TimePriceMatrix>;


struct Penalties
{
	double penalty_over_volume;			// штраф переполнения по объёму за м^3
	double penalty_over_weight;			// штраф перевеса на кг
	double penalty_delay;				// штраф опоздания на секунду
	double penalty_no_pass_in_closed_city;
	double penalty_change_city;
	double penalty_change_zone;
	double penalty_change_priority;
	double penalty_idle_first_point;
	double penalty_time;
	double penalty_percent_limit_small_volume;
	double penalty_small_volume;
	double penalty_unpreferred_zone;
	double penalty_volume_unloading;			// штраф за разгрузку

	Penalties()
	: penalty_over_volume(10000)
	, penalty_over_weight(30)
	, penalty_delay(0.5)
	, penalty_no_pass_in_closed_city(100000)
	, penalty_change_city(150)
	, penalty_change_zone(2000)
	, penalty_change_priority(100000)
	, penalty_idle_first_point(0.075)
	, penalty_time(150.0/3600.0)
	, penalty_percent_limit_small_volume(0.9)
	, penalty_small_volume(400)
	, penalty_unpreferred_zone(0)
	, penalty_volume_unloading(0)
	{
	}
};


class Avto
{
public:
	QUuid id;						// uuid машины
	int index;						// индекс машины
	double max_weight;				// грузоподъёмность с допустимым перегрузом
	double weight;					// грузоподъёмность
	double max_volume;				// максимальный объём
	double cost_km;					// стоимость км
	double cost_point;				// стоимость за точку
	double cost_trip;				// стоимость за рейс
	double speed_city;				// скорость в городе
	double speed_highway;			// скорость за городом
	int time_from;					// время начала работы (сек) выезд со склада
	int time_to;					// время окончания работы (сек)
	int start_time_after_relax;		// время начала работы после отдыха
	int available_count;			// кол-во доступных машин
	int num_nights;					// кол-во ночёвок без штрафа
	const Penalties& penalties;		// штрафы

private:
	STimePriceMatrix m_time_price_matrix;

	using pass_closed_cities_t = std::map<QUuid, const City&>;
	pass_closed_cities_t m_pass_closed_cities;		// пропуск в закрытые города

	std::set<QUuid> m_preferred_zones;

public:
	Avto(const Penalties& penalties);
	void prepare_time_price_matrix(const destination_points_t& destination_points, const MatrixDistances& matrix);
	void add_pass_closed_city(const City& city);
	bool is_have_pass_closed_city(const City& city) const;
	double get_time_between_points(int start_index, int end_index) const;
	double get_price_between_points(int start_index, int end_index) const;
	void add_preferred_zone(const QUuid& zone_id);
	bool is_preferred_zone(const QUuid& zone_id) const;
};
using SAvto = std::shared_ptr<Avto>;
using avtos_t = std::vector<SAvto>;


class RoutePoint
{
public:
	const DestinationPoint* destination_point;
	int time_arrival;
	int day_arrival;
	int idle_time;

	RoutePoint(const DestinationPoint* destination_point)
	: destination_point(destination_point)
	, time_arrival(0)
	, day_arrival(0)
	, idle_time(0)
	{
	}
};

class Route
{
public:
	using points_t = std::vector<RoutePoint>;

	const Avto* avto;
	double volume;			// текущая загрузка объёмом
	double weight;			// текущая загрузка весом
	
	double price_total;		// стоимость итоговая
	double price_clean;		// стоимость чистая
	//double penalty_good;	// штрафы допустимые (ночевки и проезд между городами)
	//double penalty_bad;		// штрафы недопустимые (перегруз, избыточный объем и опоздания)

	double idle_time;
	double idle_time_first_point;

	points_t points;

public:
	Route(const Avto* avto);
	void add_point(const DestinationPoint* point);
	void insert_point(int insert_index, const DestinationPoint* point);
	int insert_point_to_optimal_position(const DestinationPoint* point);
	void remove_point(int index);
	void remove_points(int index_start, int index_end);

	// обновить объём и вес
	void update_volume_weight();
	// обновить время и цену
	void update_time_price();
	// обновить время с упаковкой и цену
	void update_pack_time_price();
	// дистанция между точками
	double calc_distance(const MatrixDistances& matrix_distances) const;

private:
	void update_route_point_time(int& cur_time, int& cur_day, RoutePoint& route_point);
	void update_price();
	double get_time_between_points(int start_index, int end_index) const;
	double get_price_between_points(int start_index, int end_index) const;
	double get_distance_between_points(const MatrixDistances& matrix_distances, int start_index, int end_index) const;
	void pack_point_times(int start_point_index);
	// ищем простой на первой точке
	void find_idle_first_point();
	// обновить тайминги на точках
	void update_time(int cur_time, int cur_day, bool move_to_first_point);
	// упаковываем время
	void pack_timings();
};


class Plan
{
public:
	using routes_t = std::vector<Route*>;

private:
	routes_t m_routes;

	double m_price_total;
	double m_price_clean;
	//double m_penalty_good;
	//double m_penalty_bad;

public:
	Plan();
	Plan(const Plan& src);
	~Plan();
	Route* add_route(const Avto* avto);
	void remove_route(int index);
	void replace_route(int index, Route* route);
	const routes_t& routes() const { return m_routes; }
	
	void update_time_price(bool recalc);

	double price_total() const { return m_price_total; }
	double price_clean() const { return m_price_clean; }
	//double penalty_good() const { return m_penalty_good; }
	//double penalty_bad() const { return m_penalty_bad; }
	double calc_distance(const MatrixDistances& matrix_distances) const;
};
using SPlan = std::shared_ptr<Plan>;


struct Data
{
	QString name;
	Penalties penalties;
	cities_t cities;
	zones_t zones;
	avtos_t avtos;
	destination_points_t destination_points;
	SMatrixDistances matrix_distances;
	int random_seed = 0;
	int num_solutions = 1;			// кол-во решений
	bool show_all_results = false;	// отобразить все результаты решений
	int duration = 0;				// требуемое время просчёта
	int num_iterations = 0;
	bool use_osrm_cache = false;	// использовать osrm-кэш

	bool is_init() const { return cities.size() > 0 && destination_points.size() > 1 && avtos.size() > 0; }
	const Zone* add_zone(const QUuid& zone_id);
};


class Random
{
private:
	std::mt19937 m_rand;

public:
	Random(int seed);
	int range(int min, int max);
	int value();
	int value(int limit);
	double real_value(double min = 0., double max = 1.);
};

}
