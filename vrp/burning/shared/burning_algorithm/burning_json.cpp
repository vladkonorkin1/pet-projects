//=====================================================================================//
//   Author: open
//   Date:   17.10.2018
//=====================================================================================//
#include "base/precomp.h"
#include "burning_json.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

namespace Burning
{

bool json_read_data(const QString& filename, Data& data)
{
	QFile file(filename);
	if (file.open(QIODevice::ReadOnly))
	{
		json_parse_data(QJsonDocument::fromJson(file.readAll()), data);
		return true;
	}
	return false;
}
void json_read_value(const QJsonObject& json_obj, const QString& name, int& value)
{
	if (json_obj.contains(name))
		value = json_obj[name].toInt();
}
void json_read_value(const QJsonObject& json_obj, const QString& name, double& value)
{
	if (json_obj.contains(name))
		value = json_obj[name].toDouble();
}
void json_parse_data(const QJsonDocument& json_doc, Data& data)
{
	QJsonObject json_doc_obj = json_doc.object();

	QJsonObject json_settings = json_doc_obj["settings"].toObject();
	json_read_value(json_settings, "random_seed", data.random_seed);
	json_read_value(json_settings, "duration", data.duration);
	json_read_value(json_settings, "num_solutions", data.num_solutions);
	json_read_value(json_settings, "num_iterations", data.num_iterations);
	data.show_all_results = json_settings["show_all_results"].toBool();
	data.use_osrm_cache = json_settings["use_osrm_cache"].toBool();
	json_read_value(json_settings, "penalty_change_city", data.penalties.penalty_change_city);
	json_read_value(json_settings, "penalty_change_priority", data.penalties.penalty_change_priority);
	json_read_value(json_settings, "penalty_change_zone", data.penalties.penalty_change_zone);
	json_read_value(json_settings, "penalty_delay", data.penalties.penalty_delay);
	json_read_value(json_settings, "penalty_no_pass_in_closed_city", data.penalties.penalty_no_pass_in_closed_city);
	json_read_value(json_settings, "penalty_over_volume", data.penalties.penalty_over_volume);
	json_read_value(json_settings, "penalty_over_weight", data.penalties.penalty_over_weight);
	//json_read_value(json_settings, "penalty_spend_night", data.penalties.penalty_spend_night);
	json_read_value(json_settings, "penalty_idle_first_point", data.penalties.penalty_idle_first_point);
	json_read_value(json_settings, "penalty_time", data.penalties.penalty_time);
	json_read_value(json_settings, "penalty_small_volume", data.penalties.penalty_small_volume);
	json_read_value(json_settings, "penalty_percent_limit_small_volume", data.penalties.penalty_percent_limit_small_volume);
	json_read_value(json_settings, "penalty_unpreferred_zone", data.penalties.penalty_unpreferred_zone);
	json_read_value(json_settings, "penalty_volume_unloading", data.penalties.penalty_volume_unloading);

	data.name = json_doc_obj["name"].toString();

	QJsonArray cities_array = json_doc_obj["cities"].toArray();
	data.cities.reserve(cities_array.size());
	for (int i = 0; i < cities_array.size(); ++i)
	{
		QJsonObject obj = cities_array[i].toObject();
		City city;
		city.index = i;
		city.id = obj["id"].toString();
		city.name = obj["name"].toString();
		city.priority = obj["priority"].toInt();
		city.closed = obj["closed"].toBool();
		data.cities.push_back(city);
	}

	std::map<QUuid, const City*> cities;
	for (const City& city : data.cities)
		cities[city.id] = &city;

	QJsonArray cars_array = json_doc_obj["cars"].toArray();
	data.avtos.reserve(cars_array.size());
	for (int i = 0; i < cars_array.size(); ++i)
	{
		QJsonObject obj = cars_array[i].toObject();
		SAvto avto(new Avto(data.penalties));
		avto->index = i;
		avto->id = obj["id"].toString();
		avto->weight = obj["weight"].toDouble();
		double percent_overweight = obj["percent_overweight"].toDouble();
		avto->max_weight = avto->weight * (1 + percent_overweight);
		avto->max_volume = obj["volume"].toDouble();
		avto->cost_km = obj["cost_km"].toDouble();
		avto->cost_point = obj["cost_point"].toDouble();
		avto->cost_trip = obj["cost_trip"].toDouble();
		avto->speed_city = obj["speed_city"].toDouble();
		avto->speed_highway = obj["speed_highway"].toDouble();
		avto->time_from = obj["time_from"].toInt();
		avto->time_to = obj["time_to"].toInt();
		avto->start_time_after_relax = obj["start_time_after_relax"].toInt();
		avto->num_nights = obj["num_nights"].toInt();
		avto->available_count = obj["available_count"].toInt();
		
		for (const auto& json_pass : obj["passes"].toArray())
		{
			if (const City* city = cities[json_pass.toString()])
				avto->add_pass_closed_city(*city);
		}

		for (const auto& json_preferred_zone : obj["preferred_zones"].toArray())
		{
			avto->add_preferred_zone(json_preferred_zone.toString());
		}

		//if (data.avtos.size() < 5)
		data.avtos.push_back(avto);

		// проверки
		if (avto->cost_km <= 0)
			logs(QString("! avto %1: wrong cost km: %2").arg(avto->id.toString()).arg(avto->cost_km));
		if (avto->cost_point <= 0)
			logs(QString("! avto %1: wrong cost point: %2").arg(avto->id.toString()).arg(avto->cost_point));
		if (avto->cost_trip <= 0)
			logs(QString("! avto %1: wrong cost trip: %2").arg(avto->id.toString()).arg(avto->cost_trip));
		if (avto->available_count <= 0)
			logs(QString("! avto %1: wrong available count: %2").arg(avto->id.toString()).arg(avto->available_count));
		if (avto->max_weight <= 0)
			logs(QString("! avto %1: wrong max weight: %2").arg(avto->id.toString()).arg(avto->max_weight));
		if (avto->max_weight <= 0)
			logs(QString("! avto %1: wrong max weight: %2").arg(avto->id.toString()).arg(avto->max_weight));
		if (avto->max_volume <= 0)
			logs(QString("! avto %1: wrong max volume: %2").arg(avto->id.toString()).arg(avto->max_volume));
	}

	QJsonArray point_array = json_doc_obj["points"].toArray();
	data.destination_points.reserve(point_array.size());
	for (int i = 0; i < point_array.size(); ++i)
	{
		QJsonObject obj = point_array[i].toObject();
		DestinationPoint destination_point;
		destination_point.index = data.destination_points.size();
		destination_point.city = cities[obj["city"].toString()];
		destination_point.lat = obj["lat"].toDouble();
		destination_point.lon = obj["lon"].toDouble();
		destination_point.weight = obj["weight"].toDouble();
		destination_point.volume = obj["volume"].toDouble();
		destination_point.time_from = obj["time_from"].toDouble();
		destination_point.time_to = obj["time_to"].toDouble();
		destination_point.time_max = destination_point.time_to + obj["time_delayed"].toDouble();
		destination_point.time_discharge = obj["time_discharge"].toDouble();
		destination_point.zone = data.add_zone(obj["zone"].toString());

		//if (i == 0 || destination_point.lat < 62.361653)
		//	if (data.destination_points.size() < 179)
		data.destination_points.push_back(destination_point);

		if (i != 0)
		{
			if (destination_point.weight <= 0)
				logs(QString("! point %1: wrong weight: %2").arg(destination_point.index).arg(destination_point.weight));
			if (destination_point.volume <= 0)
				logs(QString("! point %1: wrong volume: %2").arg(destination_point.index).arg(destination_point.volume));
			if (destination_point.time_from > destination_point.time_to)
			{
				QTime time_to = QTime::fromMSecsSinceStartOfDay(destination_point.time_to * 1000);
				QTime time_from = QTime::fromMSecsSinceStartOfDay(destination_point.time_from * 1000);
				logs(QString("! point %1: wrong time: [%2-%3]").arg(destination_point.index).arg(time_from.toString("hh:mm")).arg(time_to.toString("hh:mm")));
			}
		}
	}
}
QJsonArray point_indices_to_json(const Route* route)
{
	QJsonArray json_array;
	for (int i = 1; i < route->points.size() - 1; ++i)
		json_array.push_back(route->points[i].destination_point->index);
	return json_array;
}
QJsonArray point_arrives_to_json(const Route* route)
{
	QJsonArray json_array;
	for (int i = 1; i < route->points.size() - 1; ++i)
	{
		QTime time = QTime::fromMSecsSinceStartOfDay(route->points[i].time_arrival * 1000);
		json_array.push_back(time.toString("h:mm"));
	}
	return json_array;
}
QJsonDocument plan_to_json(const SPlan& plan, const Data& data)
{
	QJsonDocument doc;
	QJsonObject json_plan;
	json_plan["result"] = (plan && !plan->routes().empty()) ? "ok" : "error";

	if (plan)
	{
		QJsonArray json_routes;
		for (const Route* route : plan->routes())
		{
			QJsonObject json_route;
			json_route["car_index"] = route->avto->index;
			json_route["volume"] = route->volume;
			json_route["weight"] = route->weight;
			json_route["price_total"] = route->price_total;
			json_route["price_clean"] = route->price_clean;
			json_route["distance"] = route->calc_distance(*data.matrix_distances);
			json_route["idle_time"] = route->idle_time;
			json_route["idle_time_first_point"] = route->idle_time_first_point;
			json_route["point_indices"] = point_indices_to_json(route);
			json_route["point_arrives"] = point_arrives_to_json(route);
			json_routes.append(json_route);
		}
		json_plan["routes"] = json_routes;
		json_plan["distance"] = plan->calc_distance(*data.matrix_distances);
		json_plan["fitness"] = plan->price_total();
		json_plan["price_total"] = plan->price_total();
		json_plan["price_clean"] = plan->price_clean();
	}
	doc.setObject(json_plan);
	return doc;
}
//double json_parse_plan_fitness(const QJsonDocument& json_doc)
//{
//	return json_doc["fitness"].toDouble();
//}
void json_parse_plan_price(const QJsonDocument& json_doc, double& price_total, double& price_clean)
{
	QJsonObject obj = json_doc.object();
	if (obj.contains("price_total"))
		price_total = obj["price_total"].toDouble();
	if (obj.contains("price_clean"))
		price_clean = obj["price_clean"].toDouble();
}

}
