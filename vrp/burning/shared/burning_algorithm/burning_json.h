//=====================================================================================//
//   Author: open
//   Date:   17.10.2018
//=====================================================================================//
#pragma once
#include "burning_algorithm/burning_data.h"
#include <QJsonDocument>

namespace Burning
{

extern bool json_read_data(const QString& filename, Data& data);
extern void json_parse_data(const QJsonDocument& json_doc, Data& data);
extern QJsonDocument plan_to_json(const SPlan& plan, const Data& data);
//extern double json_parse_plan_fitness(const QJsonDocument& json_doc);
extern void json_parse_plan_price(const QJsonDocument& json_doc, double& price_total, double& price_clean);

}
