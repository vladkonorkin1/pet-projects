//=====================================================================================//
//   Author: open
//   Date:   18.10.2018
//=====================================================================================//
#include "base/precomp.h"
#include "burning_optimizator.h"
#include "base/random_indices.h"

namespace Burning
{

Optimizator::Optimizator(const Data& data)
: m_data(data)
, m_work(true)
, m_temperature(0)
, m_rand(data.random_seed)
{
}
const SPlan& Optimizator::best_plan() const
{
	return m_plan;
}
void Optimizator::set_exit()
{
	m_work = false;
}
void Optimizator::start()
{
	init();
	main_loop();
}
template<typename T>
T transform_value(T v, T from_min, T from_max, T to_min, T to_max)
{
	double percent = (v - from_min)/(from_max - from_min);
	return to_min + (to_max - to_min) * percent;
}
// получить время расчёта
double Optimizator::get_time_calc() const
{
	if (m_data.duration > 0) return m_data.duration;

	double need_time = transform_value((double)m_data.destination_points.size(), 180., 450., 6.*60., 15.*60.);
	return std::max(need_time, 60.);
}
void Optimizator::main_loop()
{
	m_temperature = 256;
	double temperature_end = 0.0000001;
	double next_temperature = m_temperature * 0.5;
	double temperature_speed = m_temperature / 16;

	double time_calc = get_time_calc();
	double duration_iteration = time_calc / m_temperature / 0.001;
	bool iterating = m_data.num_iterations > 0;

	QTime time;
	while (m_work)
	{
		int iter = 0;
		if (iterating)
		{
			for (; iter < m_data.num_iterations; ++iter)
				modificate_plan();
		}
		else
		{
			time.restart();
			while (true)
			{
				double elapsed = time.elapsed();
				if (elapsed > duration_iteration) break;

				iter++;
				modificate_plan();
			}
		}
		logs(QString().sprintf("%.7f %d pt=%.0f pc=%.0f", m_temperature, iter, m_plan->price_total(), m_plan->price_clean()));
		emit cycle_calculated(m_plan);

		m_temperature -= temperature_speed;
		if (m_temperature <= next_temperature)
		{
			next_temperature *= 0.5;
			temperature_speed *= 0.5;

			if (m_temperature < temperature_end)
				break;
		}
	}

	//modificate_plan_optimize_route_points();
	//modificate_plan_optimize_swap_near_points();

	for (int i = 0; i < m_plan->routes().size(); )
	{
		const Route* route = m_plan->routes()[i];
		// если маршрут пустой
		if (route->points.size() <= 2)
		{
			m_plan->remove_route(i);
			continue;
		}
		++i;
	}

	for (Route* route : m_plan->routes())
		route->update_pack_time_price();
}
void Optimizator::init()
{
	for (SAvto& avto : m_data.avtos)
		avto->prepare_time_price_matrix(m_data.destination_points, *(m_data.matrix_distances));

	m_cars.reserve(m_data.avtos.size());
	for (const SAvto& avto : m_data.avtos)
		m_cars.push_back(Car(*avto));

	m_plan.reset(new Plan());

	RandomIndices random_indices_points(1, m_data.destination_points.size() - 1, m_rand.value());
	random_indices_points.reset_random();

	const DestinationPoint* stock_point = &(m_data.destination_points[0]);
	for (int index_avto = 0; index_avto < m_cars.size(); ++index_avto)
	{
		Car& car = m_cars[index_avto];
		if (car.num_remains > 0)
		{
			car.num_remains--;
			Route* route = m_plan->add_route(&car.avto);
			route->add_point(stock_point);

			for (int i = 1; i < m_data.destination_points.size(); ++i)
			{
				int index = random_indices_points.get_random_index();
				route->add_point(&m_data.destination_points[index]);
			}

			route->add_point(stock_point);
			route->update_volume_weight();
			break;
		}
	}

	check_empty_routes();
	m_plan->update_time_price(true);
}
bool Optimizator::accept_new_plan(double price_new, double price_old)
{
	if (price_new < price_old) return true;

	double exponent = (price_new - price_old) / (m_temperature * price_old);
	if (exponent < 500 && exponent > 0)
		return m_rand.real_value() < (1.0 / exp(exponent));
	
	return false;
}
// поиск двух случайных различных чисел
bool Optimizator::find_2_random_indices(Random& rand, int min, int max, int& v1, int& v2) const
{
	if ((max - min) < 1) return false;
	
	v1 = rand.range(min, max);
	v2 = v1;
	int i = 0;
	do {
		v2 = rand.range(min, max);
		if (++i > 10)
			return false;
	} while (v1 == v2);

	return true;
}
// проверить на пустые маршруты
void Optimizator::check_empty_routes()
{
	// отмечаем, какие тачки пустые
	for (Car& car : m_cars)
		car.have_empty = false;

	for (int i = 0; i < m_plan->routes().size(); )
	{
		const Route* route = m_plan->routes()[i];
		Car& car = m_cars[route->avto->index];
		// если маршрут пустой
		if (route->points.size() <= 2)
		{
			// если уже отмечен пустой
			if (car.have_empty)
			{
				// то удаляем его
				m_plan->remove_route(i);
				car.num_remains++;
				continue;
			}
			else
			{
				// отмечаем пустым
				car.have_empty = true;
			}
		}
		++i;
	}
	
	for (Car& car : m_cars)
	{
		// если требуется добавить машину
		if (!car.have_empty && car.num_remains > 0)
		{
			car.num_remains--;
			add_empty_route(&car.avto);
		}
	}
}
void Optimizator::add_empty_route(const Avto* avto)
{
	Route* route = m_plan->add_route(avto);
	const DestinationPoint* stock_point = &(m_data.destination_points[0]);
	route->add_point(stock_point);
	route->add_point(stock_point);
}
// получить дистанцию между точками для маршрута
double Optimizator::get_distance_between_points(const Route* route, int start_index, int end_index) const
{
	return m_data.matrix_distances->get(route->points[start_index].destination_point->index, route->points[end_index].destination_point->index).distance;
}
// получить минимальную дистанцию вокруг точки (без учёта точек склада)
double Optimizator::get_min_distance_route_around_point(const Route* route, int index) const
{
	double min_dist_left = std::numeric_limits<double>::max();
	double min_dist_right = std::numeric_limits<double>::max();
	
	if (index > 1)
		min_dist_left = get_distance_between_points(route, index - 1, index);

	if (index < route->points.size() - 2)
		min_dist_right = get_distance_between_points(route, index, index + 1);

	return min_dist_left < min_dist_right ? min_dist_left : min_dist_right;
}
// получить максимальную дистанцию в маршруте (без учёта точек склада)
double Optimizator::get_max_distance_route(const Route* route) const
{
	double max_dist = 0.;
	for (int i = 1; i < route->points.size() - 2; ++i)
	{
		double dist = get_distance_between_points(route, i, i + 1);
		if (dist > max_dist)
			max_dist = dist;
	}
	return max_dist;
}
// выбрать оптимальные границы точек
void Optimizator::select_optimal_boundaries_group_points(const Route* route, int& point_index_start, int& point_index_end, Random& rand) const
{
	int point_base_index = rand.range(1, route->points.size() - 2);
	point_index_start = point_base_index;
	point_index_end = point_base_index;

	// ищем минимальную дистанцию вокруг точки
	double min_dist = get_min_distance_route_around_point(route, point_base_index);
	// ищем максимальную дистанцию среди всех точек (исключая склад)
	double max_dist = get_max_distance_route(route);

	// выбираем критическую дистанцию, более которой не включаем точки в группу
	double connected_dist = min_dist + (max_dist - min_dist) * 1.25 * rand.real_value() * rand.real_value() * rand.real_value();

	// выбираем границы точек
	while (point_index_start > 1 && get_distance_between_points(route, point_index_start - 1, point_index_start) < connected_dist)
		--point_index_start;

	while (point_index_end < (route->points.size() - 2) && get_distance_between_points(route, point_index_end, point_index_end + 1) < connected_dist)
		++point_index_end;
}
// проверка на кол-во точек
void Optimizator::assert_count_points() const
{
#ifdef _DEBUG
	int count = 0;
	for (const Route* route : m_plan->routes())
	{
		int num_points = route->points.size();
		assert( num_points >= 2 );
		count += num_points - 2;
	}
	assert( count == m_data.destination_points.size() - 1 );
#endif
}
void Optimizator::modificate_plan()
{
	bool result = false;

	double modificate_percent = m_rand.real_value();
	if (modificate_percent < 0.03)
		result = modificate_swap_near_points();
	else if (modificate_percent < 0.08)
		result = modificate_swap_random_points();
	else if (modificate_percent < 0.13)
		result = modificate_move_point_from_time();
	else if (modificate_percent < 0.23)
		result = modificate_move_point_in_other_route();
	else
		result = modificate_move_group_points_in_other_route();

	if (result)
	{
		//modificate_optimize_route_points();
		check_empty_routes();
		m_plan->update_time_price(false);
	}

	// проверка на кол-во точек
	assert_count_points();
}
bool Optimizator::modificate_move_point_from_time()
{
	int route_index = m_rand.value(m_plan->routes().size());
	const Route* src_route = m_plan->routes()[route_index];
	// если кол-во точек достаточно для перестановки
	if (src_route->points.size() < 4) return false;

	for (int point_index = 1; point_index < src_route->points.size() - 2; ++point_index)
	{
		const RoutePoint& route_point = src_route->points[point_index];
		// если есть простой
		if (route_point.idle_time > 0)
		{
			// ищем точку, открывающуюся раньше
			for (int new_point_index = point_index + 1; new_point_index < src_route->points.size() - 1; ++new_point_index)
			{
				const RoutePoint& new_route_point = src_route->points[new_point_index];
				if (route_point.day_arrival != new_route_point.day_arrival) return false;
				// если точка открывается раньше, то всё...
				if (new_route_point.destination_point->time_from < route_point.destination_point->time_from)
				{
					// создаём копию маршрута
					std::unique_ptr<Route> route(new Route(*src_route));
					// получаем индекс точки
					//int point_index = m_rand.range(1, route->points.size() - 3);
					// меняем точки местами
					std::swap(route->points[point_index], route->points[new_point_index]);
					// обновляем время и стоимость
					route->update_time_price();

					if (accept_new_plan(route->price_total, src_route->price_total))
					{
						m_plan->replace_route(route_index, route.release());
						return true;
					}
				}
			}
		}
	}
	return false;
}
/*bool Optimizator::modificate_plan_optimize_route_points()
{
	for (int route_index = 0; route_index < m_plan->routes().size(); ++route_index)
	{
		if (m_plan->routes()[route_index]->points.size() < 4) break;

		std::vector<bool> indices(m_data.destination_points.size(), false);
		for (int i = 1; i < m_plan->routes()[route_index]->points.size() - 1; )
		{
			const Route* src_route = m_plan->routes()[route_index];
			if (indices[src_route->points[i].destination_point->index])
			{
				++i;
			}
			else
			{
				// создаём копию маршрута
				std::unique_ptr<Route> route(new Route(*src_route));
				// получаем индекс точки
				int point_index = i;
				
				const DestinationPoint* dest_point = route->points[point_index].destination_point;
				route->remove_point(point_index);

				// обновляем время и стоимость
				route->update_time_price();

				int new_point_index = route->insert_point_to_optimal_position(dest_point);
				indices[dest_point->index] = true;

				// обновляем время и стоимость
				route->update_time_price();

				//if (route->price_total < src_route->price_total)
				if (route->price_total < src_route->price_total && fabs(route->price_total - src_route->price_total) > 0.001)
				//if (abs(route->price_total - src_route->price_total) > 0.000001 && accept_new_plan(route->price_total, src_route->price_total))
				{
					logs(QString("find: route_index=%1 %2 (%3) < %4 (%5)").arg(route_index).arg(route->price_total).arg(new_point_index).arg(src_route->price_total).arg(point_index));
					m_plan->replace_route(route_index, route.release());
				}
			}
		}
	}
	m_plan->update_time_price(false);
	return true;
}
bool Optimizator::modificate_plan_optimize_swap_near_points()
{
	for (int route_index = 0; route_index < m_plan->routes().size(); ++route_index)
	{
		// если кол-во точек достаточно для перестановки
		if (m_plan->routes()[route_index]->points.size() < 4) break;

		for (int point_index = 1; point_index < m_plan->routes()[route_index]->points.size() - 2; ++point_index)
		{
			//if (route_index == 1 && (point_index == 14))
			//{
			//	__nop();
			//}

			const Route* src_route = m_plan->routes()[route_index];
			// создаём копию маршрута
			std::unique_ptr<Route> route(new Route(*src_route));
			// получаем индекс точки
			//int point_index = m_rand.range(1, route->points.size() - 3);
			// меняем точки местами
			std::swap(route->points[point_index], route->points[point_index + 1]);
			// обновляем время и стоимость
			route->update_time_price();

			//if (route->price_clean < src_route->price_clean && fabs(route->price_clean - src_route->price_clean) > 0.001)
			////if (abs(route->price_total - src_route->price_total) > 0.000001 && accept_new_plan(route->price_total, src_route->price_total))
			//{
			//	logs(QString("find swap: route_index=%1 %2 (%3) < %4 (%5)").arg(route_index).arg(route->price_total).arg(point_index + 1).arg(src_route-//>price_total).arg(point_index));
			//	m_plan->replace_route(route_index, route.release());
			//}
			//
			//if (route_index == 3 && (point_index == 3))
			//{
			//	logs(QString("find swap11111111: route_index=%1 %2 (%3) < %4 (%5)").arg(route_index).arg(route->price_total).arg(point_index + 1).arg(src_route-//>price_total).arg(point_index));
			//	m_plan->replace_route(route_index, route.release());
			//}
		}
	}
	m_plan->update_time_price(false);
	return true;
}*/
bool Optimizator::modificate_swap_near_points()
{
	int route_index = m_rand.value(m_plan->routes().size());
	const Route* src_route = m_plan->routes()[route_index];
	// если кол-во точек достаточно для перестановки
	if (src_route->points.size() >= 4)
	{
		// создаём копию маршрута
		std::unique_ptr<Route> route(new Route(*src_route));
		// получаем индекс точки
		int point_index = m_rand.range(1, route->points.size() - 3);
		// меняем точки местами
		std::swap(route->points[point_index], route->points[point_index + 1]);
		// обновляем время и стоимость
		route->update_time_price();

		if (accept_new_plan(route->price_total, src_route->price_total))
		{
			m_plan->replace_route(route_index, route.release());
			return true;
		}
	}
	return false;
}
bool Optimizator::modificate_swap_random_points()
{
	int route_index = m_rand.value(m_plan->routes().size());
	const Route* src_route = m_plan->routes()[route_index];
	
	// если есть 2 различных индекса
	int point_index1 = 0;
	int point_index2 = 0;
	if (find_2_random_indices(m_rand, 1, src_route->points.size() - 2, point_index1, point_index2))
	{
		// создаём копию маршрута
		std::unique_ptr<Route> route(new Route(*src_route));
		// меняем точки местами
		std::swap(route->points[point_index1], route->points[point_index2]);
		// обновляем время и стоимость
		route->update_time_price();

		if (accept_new_plan(route->price_total, src_route->price_total))
		{
			m_plan->replace_route(route_index, route.release());
			return true;
		}
	}
	return false;
}
bool Optimizator::modificate_move_point_in_other_route()
{
	// если найдены 2 случайных различных маршрута
	int route_index_src = 0;
	int route_index_dest = 0;
	if (find_2_random_indices(m_rand, 0, m_plan->routes().size()-1, route_index_src, route_index_dest))
	{
		const Route* route_src = m_plan->routes()[route_index_src];
		const Route* route_dest = m_plan->routes()[route_index_dest];

		// если есть хотя бы одна точка для перемещения
		if (route_src->points.size() >= 3)
		{
			// создаём копии маршрутов
			std::unique_ptr<Route> route_new_src(new Route(*route_src));
			std::unique_ptr<Route> route_new_dest(new Route(*route_dest));

			// индекс точки, которую переносим
			int point_index_src = m_rand.range(1, route_new_src->points.size() - 2);
			// точка для переноса
			const DestinationPoint* destination_point = route_new_src->points[point_index_src].destination_point;
			// удаляем эту точку
			route_new_src->remove_point(point_index_src);

			bool in_optimal_position = m_rand.real_value() > 0.5;
			if (in_optimal_position)
			{
				// вставляем в оптимальную позицию
				route_new_dest->insert_point_to_optimal_position(destination_point);
			}
			else
			{
				// вставляем в случайную позицию
				int point_index_dest = m_rand.range(1, route_new_dest->points.size() - 1);
				route_new_dest->insert_point(point_index_dest, destination_point);
			}

			// обновляем объем и вес
			route_new_src->update_volume_weight();
			route_new_dest->update_volume_weight();
			// обновляем время и стоимость
			route_new_src->update_time_price();
			route_new_dest->update_time_price();

			double price_old = (route_src->price_total + route_dest->price_total) / 2;
			double price_new = (route_new_src->price_total + route_new_dest->price_total) / 2;
			
			if (accept_new_plan(price_new, price_old))
			{
				m_plan->replace_route(route_index_src, route_new_src.release());
				m_plan->replace_route(route_index_dest, route_new_dest.release());
				return true;
			}
		}
	}
	return false;
}
bool Optimizator::modificate_move_group_points_in_other_route()
{
	// если найдены 2 случайных различных маршрута
	int route_index_src = 0;
	int route_index_dest = 0;
	if (find_2_random_indices(m_rand, 0, m_plan->routes().size() - 1, route_index_src, route_index_dest))
	{
		const Route* route_src = m_plan->routes()[route_index_src];
		const Route* route_dest = m_plan->routes()[route_index_dest];

		// если есть хотя бы одна точка для перемещения
		if (route_src->points.size() >= 3)
		{
			// создаём копии маршрутов
			std::unique_ptr<Route> route_new_src(new Route(*route_src));
			std::unique_ptr<Route> route_new_dest(new Route(*route_dest));

			// выбираем границы точек
			int point_index_start = 0;
			int point_index_end = 0;
			select_optimal_boundaries_group_points(route_new_src.get(), point_index_start, point_index_end, m_rand);

			bool in_optimal_position = m_rand.real_value() > 0.5;

			// копируем точки в другой маршрут
			double type_insert = m_rand.real_value();
			if (type_insert < 0.33)
			{
				if (in_optimal_position)
				{
					for (int i = point_index_start; i <= point_index_end; ++i)
					{
						const DestinationPoint* destination_point = route_new_src->points[i].destination_point;
						route_new_dest->insert_point_to_optimal_position(destination_point);
					}
				}
				else
				{
					for (int i = point_index_start; i <= point_index_end; ++i)
					{
						// вставляем в случайную позицию
						const DestinationPoint* destination_point = route_new_src->points[i].destination_point;
						int point_index_dest = m_rand.range(1, route_new_dest->points.size() - 1);
						route_new_dest->insert_point(point_index_dest, destination_point);
					}
				}
			}
			else if (type_insert < 0.66)
			{
				if (in_optimal_position)
				{
					for (int i = point_index_end; i >= point_index_start; --i)
					{
						const DestinationPoint* destination_point = route_new_src->points[i].destination_point;
						route_new_dest->insert_point_to_optimal_position(destination_point);
					}
				}
				else
				{
					for (int i = point_index_end; i >= point_index_start; --i)
					{
						// вставляем в случайную позицию
						const DestinationPoint* destination_point = route_new_src->points[i].destination_point;
						int point_index_dest = m_rand.range(1, route_new_dest->points.size() - 1);
						route_new_dest->insert_point(point_index_dest, destination_point);
					}
				}
			}
			else
			{
				RandomIndices random_indices(point_index_start, point_index_end - point_index_start + 1, m_rand.value());
				random_indices.reset_random();
				if (in_optimal_position)
				{
					for (int i = random_indices.get_random_index(); i != RandomIndices::Null; i = random_indices.get_random_index())
					{
						const DestinationPoint* destination_point = route_new_src->points[i].destination_point;
						route_new_dest->insert_point_to_optimal_position(destination_point);
					}
				}
				else
				{
					for (int i = random_indices.get_random_index(); i != RandomIndices::Null; i = random_indices.get_random_index())
					{
						// вставляем в случайную позицию
						const DestinationPoint* destination_point = route_new_src->points[i].destination_point;
						int point_index_dest = m_rand.range(1, route_new_dest->points.size() - 1);
						route_new_dest->insert_point(point_index_dest, destination_point);
					}
				}
			}

			// удаляем скопированные точки
			route_new_src->remove_points(point_index_start, point_index_end);

			// обновляем объем и вес
			route_new_src->update_volume_weight();
			route_new_dest->update_volume_weight();
			// обновляем время и стоимость
			route_new_src->update_time_price();
			route_new_dest->update_time_price();

			double price_old = (route_src->price_total + route_dest->price_total) / 2;
			double price_new = (route_new_src->price_total + route_new_dest->price_total) / 2;

			if (accept_new_plan(price_new, price_old))
			{
				m_plan->replace_route(route_index_src, route_new_src.release());
				m_plan->replace_route(route_index_dest, route_new_dest.release());
				return true;
			}
		}
	}
	return false;
}

}