//=====================================================================================//
//   Author: open
//   Date:   18.10.2018
//=====================================================================================//
#pragma once
#include "burning_algorithm/burning_data.h"

namespace Burning
{

class Optimizator : public QObject
{
	Q_OBJECT
private:
	Data m_data;
	bool m_work;
	SPlan m_plan;
	Random m_rand;
	double m_temperature;

	struct Car
	{
		const Avto& avto;
		int num_remains;
		bool have_empty;

		Car(const Avto& avto)
		: avto(avto)
		, num_remains(avto.available_count)
		, have_empty(false)
		{
		}
	};
	std::vector<Car> m_cars;
	
public:
	Optimizator(const Data& data);
	void set_exit();
	void start();
	const SPlan& best_plan() const;

signals:
	void cycle_calculated(const Burning::SPlan& plan);

private:
	void init();
	void main_loop();
	void modificate_plan();
	bool modificate_swap_near_points();
	bool modificate_swap_random_points();
	bool modificate_move_point_in_other_route();
	bool modificate_move_group_points_in_other_route();
	bool modificate_move_point_from_time();
	//bool modificate_plan_optimize_route_points();
	//bool modificate_plan_optimize_swap_near_points();
	// получить время расчёта
	double get_time_calc() const;
	// принять новый план
	bool accept_new_plan(double price_new, double price_old);
	// поиск двух случайных различных чисел
	bool find_2_random_indices(Random& rand, int min, int max, int& v1, int& v2) const;
	// проверить на пустые маршруты
	void check_empty_routes();
	// добавить пустой маршрут
	void add_empty_route(const Avto* avto);
	// получить дистанцию между точками для маршрута
	double get_distance_between_points(const Route* route, int start_index, int end_index) const;
	// получить минимальную дистанцию вокруг точки (без учёта точек склада)
	double get_min_distance_route_around_point(const Route* route, int index) const;
	// получить максимальную дистанцию в маршруте (без учёта точек склада)
	double get_max_distance_route(const Route* route) const;
	// выбрать оптимальные границы точек
	void select_optimal_boundaries_group_points(const Route* route, int& point_index_start, int& point_index_end, Random& rand) const;
	// проверка на кол-во точек
	void assert_count_points() const;
};

}