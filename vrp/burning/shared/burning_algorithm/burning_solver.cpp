//=====================================================================================//
//   Author: open
//   Date:   17.10.2018
//=====================================================================================//
#include "base/precomp.h"
#include "burning_solver.h"

namespace Burning
{

SolverTask::SolverTask(const Data& data, bool precalc)
: m_optimizator(data)
{
	if (precalc)
		connect(&m_optimizator, SIGNAL(cycle_calculated(const Burning::SPlan&)), SLOT(slot_cycle_calculated(const Burning::SPlan&)), Qt::ConnectionType::DirectConnection);
}
void SolverTask::on_process()
{
	m_optimizator.start();
}
void SolverTask::on_finish_process()
{
	emit finished(m_optimizator.best_plan());
}
void SolverTask::set_exit()
{
	m_optimizator.set_exit();
}
void SolverTask::slot_cycle_calculated(const Burning::SPlan& plan)
{
	Burning::SPlan new_plan(new Burning::Plan(*plan));
	emit cycle_calculated(new_plan);
}


Solver::Solver()
: m_thread_provider("solver_task")
{
	connect(&m_thread_provider, SIGNAL(finished(ThreadProvider*)), SLOT(slot_thread_finished(ThreadProvider*)), Qt::QueuedConnection);
	m_thread_provider.start_thread();
	qRegisterMetaType<Burning::SPlan>("Burning::SPlan");
}
Solver::~Solver()
{
	if (m_task) m_task->set_exit();
}
void Solver::slot_thread_finished(ThreadProvider* thread_provider)
{
	thread_provider->finish_task();
}
void Solver::calculate(const Data& data, bool precalc)
{
	m_task.reset(new SolverTask(data, precalc));
	connect(m_task.get(), SIGNAL(cycle_calculated(const Burning::SPlan&)), SIGNAL(cycle_calculated(const Burning::SPlan&)), Qt::ConnectionType::QueuedConnection);
	connect(m_task.get(), SIGNAL(finished(const Burning::SPlan&)), SIGNAL(finished(const Burning::SPlan&)));
	m_thread_provider.post_task(m_task);
}
}