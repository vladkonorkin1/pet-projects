//=====================================================================================//
//   Author: open
//   Date:   17.10.2018
//=====================================================================================//
#pragma once
#include "base/thread_task_manager.h"
#include "burning_algorithm/burning_data.h"
#include "burning_algorithm/burning_optimizator.h"

namespace Burning
{

class SolverTask : public ThreadTask
{
	Q_OBJECT
private:
	Optimizator m_optimizator;

public:
	SolverTask(const Data& data, bool precalc);
	void set_exit();

	// ThreadTask
	virtual void on_process();
	virtual void on_finish_process();

signals:
	void cycle_calculated(const Burning::SPlan& plan);
	void finished(const Burning::SPlan& best_plan);

private slots:
	void slot_cycle_calculated(const Burning::SPlan& plan);
};

using SSolverTask = std::shared_ptr<SolverTask>;


class Solver : public QObject
{
	Q_OBJECT
private:
	ThreadProvider m_thread_provider;
	SSolverTask m_task;

public:
	Solver();
	virtual ~Solver();
	void calculate(const Data& data, bool precalc = false);

signals:
	void cycle_calculated(const Burning::SPlan& plan);
	void finished(const Burning::SPlan& best_plan);

private slots:
	void slot_thread_finished(ThreadProvider* thread_provider);
};

}