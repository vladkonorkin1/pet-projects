//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#pragma once
#include "net/net_packet_reader.h"
#include "net/net_message.h"
#include <QAbstractSocket>
#include <QLocalSocket>

using ConnectionId = unsigned int;
class QTcpSocket;

class NetBaseConnection : public QObject
{
public:
	Q_OBJECT
private:
	static const quint64 m_block_size;
	using buffer_t = std::vector<char>;
	buffer_t m_buffer;
	QIODevice* m_socket;
	NetPacketReader m_packet_reader;
	bool m_write;

public:
	NetBaseConnection(QAbstractSocket* socket);
	NetBaseConnection(QLocalSocket* socket);
	//	отослать сообщение серверу
	void send_message(const NetMessage& message);
	//  закрыть соединение
	void close();

signals:
	// клиент подключился к серверу
	void connected();
	void disconnected();
	void data_received(const char* data, unsigned int size);

private slots:
	void slot_received();
	void slot_start_connection();
	void slot_close_connection(QAbstractSocket::SocketError error);
	void slot_close_connection(QLocalSocket::LocalSocketError);
	//void slot_close_connection();
	void slot_update_write_transfer(qint64 num_bytes);
	void slot_data_recieved(const char* data, unsigned int size);
	void slot_wrong_packet();

private:
	// записать в сеть из буфера
	void write();
	void update_write();
};