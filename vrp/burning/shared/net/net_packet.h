#pragma once

#pragma pack(push,1)
struct NetPacketHeader
{
	static unsigned short marker;	// AP

	unsigned short metka;			//	маркер пакета
	unsigned int data_size;			//	размер данных

	NetPacketHeader(unsigned int size = 0) : data_size(size), metka(marker) {}
};
#pragma pack(pop)