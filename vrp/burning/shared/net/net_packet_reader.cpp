#include "base/precomp.h"
#include "net_packet_reader.h"

NetPacketReader::NetPacketReader()
: m_parser(&NetPacketReader::parse_marker)
, m_current(0)
{
}
void NetPacketReader::read(const QByteArray& buffer)
{
	m_data.insert(m_data.end(), buffer.constData(), buffer.constData() + buffer.size());
	while (m_current < m_data.size())
		if (!(this->*m_parser)())
			break;
}
unsigned int NetPacketReader::current_size() const
{
	return static_cast<unsigned int>(m_data.size()) - m_current;
}
//	разобрать маркер пакета
bool NetPacketReader::parse_marker()
{
	assert( m_current <= m_data.size() );
	if (current_size() < sizeof(NetPacketHeader::marker)) return false;

	if (memcmp(&m_data[m_current], &NetPacketHeader::marker, sizeof(NetPacketHeader::marker)) != 0)
	{
		//throw std::runtime_error("NetPacketReader: fail parse data");
		//m_parser = &PacketReader::parse_failed_data;
		logs("net: wrong packet");
		emit wrong_packet();
		return false;
	}	
	m_current += sizeof(NetPacketHeader::marker);
	m_parser = &NetPacketReader::parse_header;
	return true;
}
//	разобрать заголовок пакета
bool NetPacketReader::parse_header()
{
	const unsigned int size = sizeof(m_header.data_size);

	assert( m_current <= m_data.size() );
	if (current_size() >= size)
	{
		memcpy(&m_header.data_size, &m_data[m_current], size);
		m_current += size;
		m_parser = &NetPacketReader::parse_data;
		if (m_header.data_size == 0)
			return (this->*m_parser)();
		return true;
	}
	return false;
}
//	разобрать данные
bool NetPacketReader::parse_data()
{
	assert( m_current <= m_data.size() );
	if (current_size() >= m_header.data_size)
	{
		//	уведомляем наблюдателя
		emit data_received(m_data.data() + m_current, m_header.data_size);
		//	удаляем разобранные данные из буфера
		m_data.erase(m_data.begin(), m_data.begin() + m_current + m_header.data_size);
		//	обнуляем позицию чтения 
		m_current = 0;
		//	выставляем функцию разбора на чтение нового пакета
		m_parser = &NetPacketReader::parse_marker;
		return true;
	}
	return false;
}
