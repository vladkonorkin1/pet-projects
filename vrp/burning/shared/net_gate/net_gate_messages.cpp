#include "base/precomp.h"
#include "net_gate/net_gate_messages.h"

namespace NetGate
{

NET_MESSAGE_IMPL(ClientMessageCheckProtocolVersion);
NET_MESSAGE_IMPL(ServerMessageCheckProtocolVersionResponse);
NET_MESSAGE_IMPL(ClientMessageAutorizate);
NET_MESSAGE_IMPL(ServerMessageAutorizateResponse);
NET_MESSAGE_IMPL(ClientMessageAlive);
NET_MESSAGE_IMPL(ServerMessageOpenTasks);
NET_MESSAGE_IMPL(ServerMessageReportSolvedTasks);
NET_MESSAGE_IMPL(ClientMessageTaskSolved);
NET_MESSAGE_IMPL(ServerMessageCloseTask);

}