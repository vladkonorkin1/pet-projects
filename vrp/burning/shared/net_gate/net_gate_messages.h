#pragma once
#include "net/net_message.h"
#include "net_gate/net_gate_types.h"
#include "base/matrix_distances.h"

namespace NetGate
{

enum class MessageProtocolId
{
	Empty = 0,
	ClientMessageCheckProtocolVersion,
	ServerMessageCheckProtocolVersionResponse,
	ClientMessageAutorizate,				// авторизация
	ServerMessageAutorizateResponse,
	ClientMessageAlive,						// сообщение серверу... "я жив!"
	ServerMessageOpenTasks,					// открыть задачи
	ServerMessageReportSolvedTasks,			// доложить о решённых задачах
	ClientMessageTaskSolved,				// задача решена
	ServerMessageCloseTask,					// закрыть задачу
	//ClientMessageCloseTaskResponse,			// потверждение о закрытии задачи
	MessageProtocolIdMax
};


struct ClientMessageCheckProtocolVersion : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageCheckProtocolVersion);
	ClientMessageCheckProtocolVersion(int protocol_version) : protocol_version(protocol_version) {}
	int protocol_version;

	void read(Base::IBinStream& stream) {
		stream >> protocol_version;
	}
	void write(Base::OBinStream& stream) const {
		stream << protocol_version;
	}
};
NET_MESSAGE_STREAM(ClientMessageCheckProtocolVersion);

struct ServerMessageCheckProtocolVersionResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageCheckProtocolVersionResponse);
	ServerMessageCheckProtocolVersionResponse(bool success) : success(success) {}
	bool success;

	void read(Base::IBinStream& stream)
	{
		stream >> success;
	}
	void write(Base::OBinStream& stream) const
	{
		stream << success;
	}
};
NET_MESSAGE_STREAM(ServerMessageCheckProtocolVersionResponse);


struct ClientMessageAutorizate : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageAutorizate);
	ClientMessageAutorizate(const QUuid& id, const QString& pc_name, const QString& user_name, unsigned short int number_threads) : id(id), pc_name(pc_name), user_name(user_name), number_threads(number_threads) {}
	QUuid id;
	QString pc_name;
	QString user_name;
	unsigned short int number_threads;

	void read(Base::IBinStream& stream) {
		stream >> id;
		stream >> pc_name;
		stream >> user_name;
		stream >> number_threads;
	}
	void write(Base::OBinStream& stream) const {
		stream << id;
		stream << pc_name;
		stream << user_name;
		stream << number_threads;
	}
};
NET_MESSAGE_STREAM(ClientMessageAutorizate);


struct ServerMessageAutorizateResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageAutorizateResponse);
	ServerMessageAutorizateResponse(bool auth_result, bool new_session) : auth_result(auth_result), new_session(new_session) {}
	bool auth_result;
	bool new_session;

	void read(Base::IBinStream& stream)
	{
		stream >> auth_result;
		stream >> new_session;
	}
	void write(Base::OBinStream& stream) const
	{
		stream << auth_result;
		stream << new_session;
	}
};
NET_MESSAGE_STREAM(ServerMessageAutorizateResponse);


struct ClientMessageAlive : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageAlive);
	void read(Base::IBinStream& stream) { Q_UNUSED(stream); }
	void write(Base::OBinStream& stream) const { Q_UNUSED(stream); }
};
NET_MESSAGE_STREAM(ClientMessageAlive);


struct ServerMessageOpenTasks : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageOpenTasks);
	ServerMessageOpenTasks(int claim_id, const QJsonDocument& json_doc, const SMatrixDistances& matrix_distances, const std::vector<int>& task_ids) : claim_id(claim_id), json_doc(json_doc), matrix_distances(matrix_distances), task_ids(task_ids) {}

	int claim_id;
	QJsonDocument json_doc;
	SMatrixDistances matrix_distances;
	//std::set<int> task_ids;
	std::vector<int> task_ids;

	void read(Base::IBinStream& stream)
	{
		stream >> claim_id;
		
		QByteArray buffer;
		stream >> buffer;
		json_doc = QJsonDocument::fromJson(buffer);

		stream >> matrix_distances;

		unsigned short int tasks_size;
		stream >> tasks_size;
		//task_ids.reserve(tasks_size);
		for (int i = 0; i < tasks_size; ++i)
		{
			int id;
			stream >> id;
			task_ids.push_back(id);
			//task_ids.insert(id);
		}
	}
	void write(Base::OBinStream& stream) const
	{
		stream << claim_id;
		stream << json_doc.toJson(QJsonDocument::Compact);
		stream << matrix_distances;
		stream << static_cast<unsigned short int>(task_ids.size());
		for (int id : task_ids)
			stream << id;
	}
};
NET_MESSAGE_STREAM(ServerMessageOpenTasks);


struct ServerMessageReportSolvedTasks : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageReportSolvedTasks);
	void read(Base::IBinStream& stream) { Q_UNUSED(stream); }
	void write(Base::OBinStream& stream) const { Q_UNUSED(stream); }
};
NET_MESSAGE_STREAM(ServerMessageReportSolvedTasks);


struct ClientMessageTaskSolved : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageTaskSolved);
	ClientMessageTaskSolved(int claim_id, int task_id, const QJsonDocument& json_result) : claim_id(claim_id), task_id(task_id), json_result(json_result) {}
	int claim_id;
	int task_id;

	QJsonDocument json_result;

	void read(Base::IBinStream& stream)
	{
		stream >> claim_id;
		stream >> task_id;
		
		QByteArray buffer;
		stream >> buffer;
		json_result = QJsonDocument::fromJson(buffer);
	}
	void write(Base::OBinStream& stream) const
	{
		stream << claim_id;
		stream << task_id;
		stream << json_result.toJson(QJsonDocument::Compact);
	}
};
NET_MESSAGE_STREAM(ClientMessageTaskSolved);


struct ServerMessageCloseTask : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageCloseTask);
	ServerMessageCloseTask(int claim_id, int task_id) : claim_id(claim_id), task_id(task_id) {}
	int claim_id;
	int task_id;

	void read(Base::IBinStream& stream)
	{
		stream >> claim_id;
		stream >> task_id;
	}
	void write(Base::OBinStream& stream) const
	{
		stream << claim_id;
		stream << task_id;
	}
};
NET_MESSAGE_STREAM(ServerMessageCloseTask);


/*struct ClientMessageCloseTaskResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageCloseTaskResponse);
	ClientMessageCloseTaskResponse(int claim_id, int task_id) : claim_id(claim_id), task_id(task_id) {}
	int claim_id;
	int task_id;

	void read(Base::IBinStream& stream)
	{
		stream >> claim_id;
		stream >> task_id;
	}
	void write(Base::OBinStream& stream) const
	{
		stream << claim_id;
		stream << task_id;
	}
};
NET_MESSAGE_STREAM(ClientMessageCloseTaskResponse);*/

}