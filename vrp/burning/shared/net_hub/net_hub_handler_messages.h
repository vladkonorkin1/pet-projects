#pragma once
#include "net_hub/net_hub_messages.h"

namespace NetHub
{

class ClientHandlerMessages
{
public:
	virtual ~ClientHandlerMessages() {}
	virtual void on_message(const NetHub::ServerMessageOpenTicket& msg) = 0;
	virtual void on_message(const NetHub::ServerMessageExitProcess& msg) = 0;
};

class ServerHandlerMessages
{
public:
	virtual ~ServerHandlerMessages() {}
	virtual void on_message(const NetHub::ClientMessageAuthorize& msg) = 0;
	virtual void on_message(const NetHub::ClientMessageTicketSolved& msg) = 0;
};

}