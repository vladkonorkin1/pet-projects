#include "base/precomp.h"
#include "net_hub/net_hub_messages.h"

namespace NetHub
{

NET_MESSAGE_IMPL(ClientMessageAuthorize);
NET_MESSAGE_IMPL(ServerMessageOpenTicket);
NET_MESSAGE_IMPL(ClientMessageTicketSolved);
NET_MESSAGE_IMPL(ServerMessageExitProcess);

}
