//=====================================================================================//
//   Author: open
//   Date:   22.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "net_request_osrm_distance.h"
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
//#include <math.h>

QString NetRequestOsrmDistance::s_server_address;

NetRequestOsrmDistance::NetRequestOsrmDistance(const QPointF& start_pos, const QPointF& end_pos)
: m_start_pos(start_pos)
, m_end_pos(end_pos)
{
}
// http://88.86.193.234:5000/route/v1/driving/60.754581,56.758808;59.819533,58.360418?overview=false
QString NetRequestOsrmDistance::url() const
{
	//if ((fabs(m_start_pos.x() - 80.351) < 0.001))
	//{
	//	int test = 0;
	//}
	QString test = "http://" + s_server_address + "/route/v1/driving/" + QString().sprintf("%.6f,%.6f;%.6f,%.6f?overview=false", m_start_pos.x(), m_start_pos.y(), m_end_pos.x(), m_end_pos.y());
	return test;
}
void NetRequestOsrmDistance::process(QNetworkReply* reply, bool success)
{
	DistanceDesc distance_desc;
	if (success)
	{
		QJsonDocument json_doc = QJsonDocument::fromJson(reply->readAll());
		QJsonObject json_obj = json_doc.object();
		if (json_obj["code"] == "Ok")
		{
			QJsonArray json_routes = json_obj["routes"].toArray();
			QJsonObject json_route = json_routes[0].toObject();
			distance_desc.distance = std::max(json_route["distance"].toDouble() * 0.001, 0.);		// переводим дистанцию из метров в км
			distance_desc.duration = std::max(json_route["duration"].toDouble(), 0.);
		}
		else logs("NetRequestOsrmDistance: code not OK");
	}
	emit finished(success, distance_desc);
}
void NetRequestOsrmDistance::set_server_address(const QString& server_address)
{
	s_server_address = server_address;
}