//=====================================================================================//
//   Author: open
//   Date:   22.09.2018
//=====================================================================================//
#pragma once
#include "base/network_object.h"
#include "base/matrix_distances.h"
#include <QPointF>

class NetRequestOsrmDistance : public PostNetworkObject
{
	Q_OBJECT
private:
	static QString s_server_address;
	QPointF m_start_pos;
	QPointF m_end_pos;

public:
	NetRequestOsrmDistance(const QPointF& start_pos, const QPointF& end_pos);
	virtual void process(QNetworkReply* reply, bool success);
	virtual QString url() const;
	static void set_server_address(const QString& server_address);

signals:
	void finished(bool success, const DistanceDesc& distance_desc);
};

using SNetRequestOsrmDistance = std::shared_ptr<NetRequestOsrmDistance>;
using WNetRequestOsrmDistance = std::weak_ptr<NetRequestOsrmDistance>;
