//=====================================================================================//
//   Author: open
//   Date:   02.08.2018
//=====================================================================================//
#pragma once
#include "base/network_object.h"
#include "base/types.h"

class NetRequestOsrmRoute : public PostNetworkObject
{
	Q_OBJECT
private:
	static QString s_server_address;
	QPointF m_start_pos;
	QPointF m_end_pos;

public:
	NetRequestOsrmRoute(const QPointF& start_pos, const QPointF& end_pos);
	virtual void process(QNetworkReply* reply, bool success);
	virtual QString url() const;
	static void set_server_address(const QString& server_address);

signals:
	void finished(bool success, const vpositions_t& positions);

private:
	void parse_coordinate(const QJsonArray& json_coordinate, vpositions_t& coordinate);
};

using SNetRequestOsrmRoute = std::shared_ptr<NetRequestOsrmRoute>;