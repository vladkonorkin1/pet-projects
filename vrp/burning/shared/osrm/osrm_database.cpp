//=====================================================================================//
//   Author: Solari4555
//   Date:   22.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "osrm_database.h"
#include <QTextStream>
#include <QSqlError>
#include <QSqlQuery>
#include <QFile>

OsrmDatabase::OsrmDatabase()
: m_database(connect_database("osrm_cache.s3db"))
{
}
QSqlDatabase OsrmDatabase::connect_database(const QString& filename) const
{
	QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE");
	database.setDatabaseName(filename);

	bool created = QFile::exists(filename);
	if (!database.open())
	{
		QString error("Cannot open database: " + m_database.databaseName());
		logs(error);
		throw std::runtime_error(qPrintable(error));
	}
	if (!created)
	{
		QFile file("://osrm_create_database.sql");
		if (file.open(QFile::ReadOnly | QFile::Text))
		{
			QTextStream stream(&file);
			stream.setCodec("UTF-8");
			QString text = stream.readAll();
			auto str_queries = text.split(';', QString::SplitBehavior::SkipEmptyParts);
			QSqlQuery query;
			if (database.transaction())
			{
				for (const QString& str_query : str_queries) {
					if (!query.exec(str_query))
						logs("error to creating database: " + str_query + "  error: " + query.lastError().text());
				}
				database.commit();
			}
		}
	}
	return database;
}
void OsrmDatabase::select_matrix_distances(const vpositions_t& points, SMatrixDistances matrix)
{
	QString str_positions;
	{
		char buffer[64];
		for (size_t i = 0; i < points.size(); ++i)
		{
			sprintf(buffer, (i + 1) < points.size() ? "\"%.6f %.6f\"," : "\"%.6f %.6f\"", points[i].y(), points[i].x());
			str_positions += buffer;
		}
	}
	QSqlQuery query("select src, dest, distance, duration from distances where src in (" + str_positions + ") and dest in (" + str_positions + ")");
	if (!query.exec()) return;
	
	using map_distances_t = std::map<QString, DistanceDesc*>;
	map_distances_t map_distances;
	for (size_t i = 0; i < points.size(); ++i)
	{
		for (size_t j = 0; j < points.size(); ++j)
			if (i != j) map_distances[QString().sprintf("%.6f %.6f %.6f %.6f", points[i].y(), points[i].x(), points[j].y(), points[j].x())] = &(*matrix)(i, j);
	}

	while (query.next())
	{
		QString key = query.value("src").toString() + " " + query.value("dest").toString();
		map_distances_t::iterator it = map_distances.find(key);
		if (map_distances.end() != it)
		{
			it->second->distance = query.value("distance").toDouble();
			it->second->duration = query.value("duration").toDouble();
		}
	}
}
void OsrmDatabase::update_distances(const std::vector<QPointF>& points, const matrix_distance_indices_t& indices, const SMatrixDistances& matrix)
{
	if (m_database.transaction())
	{
		QSqlQuery query;
		query.prepare("insert or replace into distances (src, dest, distance, duration) values (:src, :dest, :distance, :duration)");
		for (const MatrixDistanceIndex& index : indices)
		{
			query.bindValue(":src", QString().sprintf("%.6f %.6f", points[index.start].y(), points[index.start].x()));
			query.bindValue(":dest", QString().sprintf("%.6f %.6f", points[index.end].y(), points[index.end].x()));
			const DistanceDesc& desc = matrix->get(index.start, index.end);
			query.bindValue(":distance", desc.distance);
			query.bindValue(":duration", desc.duration);
			if (!query.exec())
			{
				logs("db: error update_distances: " + query.lastError().text());
				m_database.rollback();
				return;
			}
		}
		m_database.commit();
	}
}
