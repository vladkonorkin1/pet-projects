//=====================================================================================//
//   Author: Solari4555
//   Date:   22.09.2018
//=====================================================================================//
#pragma once
#include <QSqlDatabase>
#include "base/matrix_distances.h"
#include "base/types.h"

class OsrmDatabase 
{
private:
	QSqlDatabase m_database;

public:
	OsrmDatabase();
	//SMatrixDistances select_matrix_distances(const vpositions_t& points);
	void select_matrix_distances(const vpositions_t& points, SMatrixDistances matrix);
	void update_distances(const std::vector<QPointF>& points, const matrix_distance_indices_t& indices, const SMatrixDistances& matrix);

private:
	QSqlDatabase connect_database(const QString& filename) const;
};