//=====================================================================================//
//   Author: open
//   Date:   02.08.2018
//=====================================================================================//
#pragma once
#include "osrm/osrm_request_matrix_distances.h"
#include "osrm/osrm_request_routes.h"
#include "osrm/osrm_database.h"

class NetworkManager;

class OsrmManager : public QObject
{
	Q_OBJECT
private:
	NetworkManager& m_network_manager;
	OsrmDatabase m_database;

	// запросы
	//std::map<const OsrmRequestMatrixDistances*, SOsrmRequestMatrixDistances> m_matrix_distances_requests;

	// запрос маршрутов
	OsrmRequestRoutes m_request_routes;

public:
	OsrmManager(NetworkManager& network_manager, const QString& server_address);
	// запрос машрутов
	void request_routes(const routes_t& routes);
	// запрос дистанций
	SOsrmRequestMatrixDistances create_request_matrix_distances(const vpositions_t& points) const;
	void request_matrix_distances(const SOsrmRequestMatrixDistances& request, bool force_use_cache);

signals:
	void request_routes_finished(bool success, const routes_t& routes);

private slots:
	//void slot_request_matrix_distances_finished(const OsrmRequestMatrixDistances* request, bool success, const SMatrixDistances& matrix, const vpositions_t& points);
	void slot_request_matrix_distances_finished(const OsrmRequestMatrixDistances* request, bool success, const SMatrixDistances& matrix, const vpositions_t& points, const matrix_distance_indices_t& indices);

private:
	void set_server_address(const QString& server_address);
	float percent_matrix_initialized(const MatrixDistances& matrix) const;
};