//=====================================================================================//
//   Author: open
//   Date:   22.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "osrm_request_matrix_distances.h"
#include "base/network_manager.h"

int OsrmRequestMatrixDistances::s_batch_count = 4;

OsrmRequestMatrixDistances::OsrmRequestMatrixDistances(NetworkManager& network_manager, const vpositions_t& points)
: m_network_manager(network_manager)
, m_num_finished(0)
, m_count_queue(0)
, m_total_count(0)
, m_num_failed(0)
, m_count_sended(0)
, m_points(points)
, m_matrix(new MatrixDistances(points.size()))
{
}
void OsrmRequestMatrixDistances::request_by_indices(const matrix_distance_indices_t& indices)
{
	logs(QString("osrm: start request matrix distances by indices: %1 %2").arg(m_points.size()).arg(indices.size()));
	m_indices = indices;

	m_pool_requests.clear();
	for (const MatrixDistanceIndex& index : m_indices)
	{
		SNetRequestOsrmDistance request(new NetRequestOsrmDistance(m_points[index.start], m_points[index.end]));
		connect(request.get(), &NetRequestOsrmDistance::finished, [this, request = WNetRequestOsrmDistance(request), index](bool success, const DistanceDesc& desc) {slot_request_distance_finished(success, request, index, desc); });
		m_pool_requests.push_back(request);
	}

	m_total_count = m_pool_requests.size();
	m_num_finished = 0;
	m_count_queue = 0;
	m_num_failed = 0;
	m_count_sended = 0;
	send_requests();
}
void OsrmRequestMatrixDistances::send_requests()
{
	while (m_count_queue < s_batch_count && m_count_sended < m_total_count)//!m_pool_requests.empty())
	{
		m_count_queue++;
		//pool_requests_t::iterator it = --m_pool_requests.end();
		//m_network_manager.request(*it, NetworkMethod::Get);
		//m_pool_requests.erase(it);

		SNetRequestOsrmDistance& request = m_pool_requests[m_count_sended++];
		m_network_manager.request(request, NetworkMethod::Get);
	}
}
void OsrmRequestMatrixDistances::slot_request_distance_finished(bool success, const WNetRequestOsrmDistance& wrequest, const MatrixDistanceIndex& index, const DistanceDesc& desc)
{
	if (success)
	{
		m_num_failed = 0;
		m_count_queue--;
		send_requests();

		(*m_matrix)(index.start, index.end) = desc;

		m_num_finished++;

		if ((m_num_finished % 200) == 0 || (m_num_finished == m_total_count))
			logs(QString().sprintf("osrm: %.1f%%", (float)m_num_finished / (float)m_total_count*100.f));

		if (m_num_finished == m_total_count)
		{
			logs("osrm: finish");
			emit_finished(true);
		}
	}
	else
	{
		if (!wrequest.expired())
		{
			SNetRequestOsrmDistance request = wrequest.lock();
			logs("osrm: failed request distance: " + request->url());
			++m_num_failed;
			if (m_num_failed < s_batch_count * 10) m_network_manager.reconnect(request);
			else
			{
				emit_finished(false);

				for (SNetRequestOsrmDistance& request : m_pool_requests)
					disconnect(request.get(), SIGNAL(finished));
				m_pool_requests.clear();
			}
		}
	}
}
void OsrmRequestMatrixDistances::request_by_table()
{
	logs(QString("osrm: start request matrix distances by table: %1").arg(m_points.size()));

	m_indices.clear();
	for (int i = 0; i < m_matrix->dimension(); ++i)
		for (int j = 0; j < m_matrix->dimension(); ++j)
			if (i != j) m_indices.push_back(MatrixDistanceIndex{ i, j });

	m_request_table_distances.reset(new NetRequestOsrmTableDistances(m_points, m_matrix));
	connect(m_request_table_distances.get(), SIGNAL(finished(bool, const SMatrixDistances&)), SLOT(slot_request_table_distances_finished(bool)));
	m_network_manager.request(m_request_table_distances, NetworkMethod::Get, 12.f * 60.f);
}
void OsrmRequestMatrixDistances::slot_request_table_distances_finished(bool success)
{
	emit_finished(success);
}
//#include <QFile>
void OsrmRequestMatrixDistances::emit_finished(bool success)
{
	/*{
		QFile file("matrix450.txt");
		if (file.open(QIODevice::WriteOnly))
		{
			QTextStream stream;
			stream.setDevice(&file);
			stream.setCodec("UTF-8");

			stream.setRealNumberPrecision(6);
			stream << m_matrix->dimension() << '\n';

			for (int i = 0; i < m_matrix->dimension(); ++i)
			{
				for (int j = 0; j < m_matrix->dimension(); ++j)
				{
					stream << QString().sprintf("%.5f", m_matrix->get(i, j).distance) << '\n';
				}
			}
		}
	}*/
	emit finished(success, m_matrix, m_points, m_indices);
}
