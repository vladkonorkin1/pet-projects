//=====================================================================================//
//   Author: open
//   Date:   23.09.2018
//=====================================================================================//
#pragma once
#include "osrm/net_request_osrm_route.h"
#include "base/types.h"

class NetworkManager;

class OsrmRequestRoutes : public QObject
{
	Q_OBJECT
private:
	NetworkManager& m_network_manager;
	routes_t m_routes;

	int m_total_count;
	int m_num_finished;
	int m_num_failed;

	using pool_requests_t = std::vector<SNetRequestOsrmRoute>;
	pool_requests_t m_pool_requests;

	static int s_batch_count;
	int m_count_queue;

public:
	OsrmRequestRoutes(NetworkManager& network_manager);
	void request_routes(const routes_t& routes);

signals:
	void finished(bool success, const routes_t& points);

private:
	void slot_request_finished(bool success, const SNetRequestOsrmRoute& request, int route_index, int waypoint_index, const vpositions_t& waypoint_positions);

private:
	void send_requests();
};