//=====================================================================================//
//   Author: open
//   Date:   13.12.2018
//=====================================================================================//
#include "base/precomp.h"
#include "start_updater.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QProcess>
#include <QFile>

QString read_updater_path()
{
	QFile file("updater.settings");
	if (file.open(QIODevice::ReadOnly))
	{
		QJsonDocument json_doc = QJsonDocument::fromJson(file.readAll());
		return json_doc.object()["updater_path"].toString();
	}
	return QString();
}

bool start_updater()
{
	QString updater_path = read_updater_path();
	logs(QString("start updater: %1").arg(updater_path));
	QString working_dir = updater_path.left(updater_path.lastIndexOf('/'));
	QStringList args;
	return QProcess::startDetached(updater_path, args, working_dir);
}