//=====================================================================================//
//   Author: open
//   Date:   21.12.2018
//=====================================================================================//
//#include "base/precomp.h"
#include "version.h"
#include <QFile>

int read_version()
{
	QFile file("version");
	if (file.open(QIODevice::ReadOnly))
	{
		if (!file.atEnd())
			return QString(file.readLine()).toInt();
	}
	return -1;
}