//=====================================================================================//
//   Author: open
//   Date:   17.12.2018
//=====================================================================================//
#pragma once
#include "main_window.h"

class Gui : public QObject
{
	Q_OBJECT
private:
	std::unique_ptr<MainWindow> m_main_window;

public slots:
	void show_window();
	void set_progress(int value);
	void show_critical(const QString& msg);
};
