//=====================================================================================//
//   Author: open
//   Date:   24.28.2017
//=====================================================================================//
#include "precomp.h"
#include "single_run.h"
#include "updater.h"
#include <QDir>

#ifdef UPDATER_COMPILE_GUI
	#include <QApplication>
	#include "gui.h"
#else
	#include <QCoreApplication>
#endif

// проверить, что запущен один экземпляр приложения
// 1) прочитать настройки
	//Setting
//+ 2) сравнить версию
//+ 2.1) прочитать файл версии
//+ 2.2) скачать файл версии
//+ 2.3) сравнить их
//+ 3) скачать файл
	//DownloadFile
//+ 4) распаковать
	//Unzip
//+ 5) подменить папку
//+ 6) запустить exe


int main(int argc, char *argv[])
{
	//Q_INIT_RESOURCE(project);
	Settings settings;
	std::unique_ptr<QCoreApplication> app;

	SingleRun single(settings.application_folder);
	if (single.is_run()) return -1;

#ifdef UPDATER_COMPILE_GUI
	std::unique_ptr<Gui> gui;
	if (settings.gui)
	{
		app.reset(new QApplication(argc, argv));
		gui.reset(new Gui());
	}
	else
		app.reset(new QCoreApplication(argc, argv));
#else
	app.reset(new QCoreApplication(argc, argv));
#endif

	Updater updater(settings);
#ifdef UPDATER_COMPILE_GUI
	if (settings.gui)
	{
		QObject::connect(&updater, SIGNAL(download_started()), gui.get(), SLOT(show_window()));
		QObject::connect(&updater, SIGNAL(download_progress_updated(int)), gui.get(), SLOT(set_progress(int)));
		QObject::connect(&updater, SIGNAL(alert_showed(const QString&)), gui.get(), SLOT(show_critical(const QString&)));
	}
#endif
	return app->exec();
}
