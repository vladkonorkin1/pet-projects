//=====================================================================================//
//   Author: open
//   Date:   25.06.2017
//=====================================================================================//
#pragma once
#include "ui_main_window.h"

class MainWindow : public QWidget
{
	Q_OBJECT
private:
	std::unique_ptr<Ui::MainWindow> m_ui;

public:
	MainWindow();
	void set_progress(int progress);
};
