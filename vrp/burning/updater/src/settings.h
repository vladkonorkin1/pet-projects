//=====================================================================================//
//   Author: open
//   Date:   24.06.2017
//=====================================================================================//
#pragma once

class Settings
{
public:
	Settings();

	QString application_folder;
	QString application_name;
	QString application_arguments;
	QString download_url;
	QString download_file;
	bool auto_close;
	bool gui;
};
