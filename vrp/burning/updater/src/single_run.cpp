//=====================================================================================//
//   Author: open
//   Date:   24.08.2017
//=====================================================================================//
#include "precomp.h"
#include "single_run.h"
#include <QDate>
#include <QDir>

SingleRun::SingleRun(const QString& name)
: m_lock_file(path(name))
{
}
bool SingleRun::is_run()
{
	return !m_lock_file.tryLock(100);
}
QString SingleRun::path(const QString& name) const
{
	return QDir::tempPath() + '/' + name + "-sima-lock-" + QDate::currentDate().toString("dd.MM.yyyy");
}