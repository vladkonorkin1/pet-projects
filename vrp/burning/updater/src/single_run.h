//=====================================================================================//
//   Author: open
//   Date:   24.08.2017
//=====================================================================================//
#pragma once
#include <QLockFile>

class SingleRun : public QObject
{
	Q_OBJECT
private:
	QLockFile m_lock_file;

public:
	SingleRun(const QString& name);
	bool is_run();

private:
	QString path(const QString& name) const;
};