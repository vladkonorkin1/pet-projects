//=====================================================================================//
//   Author: open
//   Date:   24.08.2017
//=====================================================================================//
#include "precomp.h"
#include "updater.h"
#include "utility.h"
#include <quazip/JlCompress.h>
#include <QCoreApplication>
#include <QStandardPaths>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTextStream>
#include <QProcess>
#include <QBuffer>
#include <QTimer>
#include <QFile>
#include <QUuid>
#include <QDir>

/*void remove_dir(const QString& path)
{
	QDir dir(path);
	QStringList dirs = dir.entryList(QDir::Filter::Dirs | QDir::Filter::NoDotAndDotDot);
	for (const QString& entry : dirs)
		remove_dir(dir.absolutePath() + '/' + entry);

	QStringList files = dir.entryList(QDir::Files);
	for (const QString& entry : files)
	{
		QString entryAbsPath = dir.absolutePath() + '/' + entry;
		QFile::remove(entryAbsPath);
	}
	dir.rmdir(".");
}
void delete_dir(const QString& path)
{
	if (QDir(path).exists())
		remove_dir(path);
}*/

/*bool remove_dir(const QString &dirName)
{
	bool result = true;
	QDir dir(dirName);

	if (dir.exists(dirName))
	{
		Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst))
		{
			if (info.isDir()) {
				result = remove_dir(info.absoluteFilePath());
			}
			else {
				result = QFile::remove(info.absoluteFilePath());
			}

			if (!result) {
				return result;
			}
		}
		result = dir.rmdir(dirName);
	}
	return result;
}*/
QString uuid_to_str(const QUuid& uuid)
{
	QString s = uuid.toString();
	int size = s.size();
	if (size > 1 && s[0] == '{' && s[size - 1] == '}')
		return s.left(size - 1).right(size - 2);
	return s;
}


Updater::Updater(const Settings& settings)
: m_settings(settings)
, m_args(build_args(settings))
, m_build_folder("build")
, m_path_app_data(get_app_data_folder())
, m_path_build(m_path_app_data + m_settings.application_folder + '/' + m_build_folder + '/')
, m_old(read_version())
{
	//qDebug() << QDir::currentPath() << '\n' << QApplication::applicationDirPath() << '\n' << m_path_app_data << '\n' << m_path_project << '\n' << m_path_build << '\n';
	connect(&m_download_version, SIGNAL(downloaded(bool, QByteArray&)), SLOT(slot_download_version(bool, QByteArray&)));
	m_download_version.start(m_settings.download_url + "version");
}
void Updater::slot_download_version(bool ok, QByteArray& data)
{
	if (ok)
	{
		m_new.version = data.toInt();
		m_new.guid = uuid_to_str(QUuid::createUuid());
		check_version();
	}
	else run_process(false);
}
// убиваем процесс
void kill_process(const QString& process_name)
{
#ifdef Q_OS_WIN
	QProcess::execute("taskkill -f /IM " + process_name);
#elif defined Q_OS_UNIX
	QProcess::execute("pkill " + process_name);
#endif
}
void Updater::check_version()
{
	if (m_old.version != m_new.version)
	{
		// создаем папки
		if (!QDir(m_path_app_data).mkpath(m_settings.application_folder + '/' + m_build_folder))
		{
			alert(tr("Failed to create build folder: ") + m_path_build);
			return;
		}

		// убиваем процесс
		kill_process(m_settings.application_name);

		// загружаем новый файл
		QString download_url = m_settings.download_url + m_settings.download_file;
		std::cout << "start download file: " << download_url.toStdString() << std::endl;
		emit download_started();
		connect(&m_download_file, SIGNAL(downloaded(bool, QByteArray&)), SLOT(slot_download_file(bool, QByteArray&)));
		connect(&m_download_file, SIGNAL(progress(qint64, qint64)), SLOT(slot_download_file_progress(qint64, qint64)));
		m_download_file.start(download_url);
	}
	else run_process(false);
}
void Updater::slot_download_file_progress(qint64 received_bytes, qint64 total_bytes)
{
	int progress = total_bytes <= 0 ? 0 : (int)((float)received_bytes/(float)total_bytes * 100.f);
	std::cout << "download file progress: " << progress << "%" << std::endl;
	emit download_progress_updated(progress);
}
void Updater::slot_download_file(bool ok, QByteArray& data)
{
	if (ok)
	{
		// распаковываем
		QBuffer buffer(&data);
		auto files = JlCompress::extractDir(&buffer, m_new.path(m_path_build));
		if (!files.isEmpty())
		{
			write_updater_settings();
			remove_old_builds(m_new.guid);
			run_process(true);
		}
		else alert(tr("Failed to unpack build"));
	}
	else
	{
		emit download_progress_updated(0);
		alert(tr("Failed to download build"));
	}
}
void Updater::run_process(bool downloaded)
{
	if (m_settings.auto_close) kill_process(m_settings.application_name);

	QString path_working_dir = downloaded ? m_new.path(m_path_build) : m_old.path(m_path_build);
	QString path_exe = path_working_dir + m_settings.application_name;

	//QProcess::execute("ldd " + path_exe);
	//QFile(path_exe).setPermissions(QFile::ExeOwner | QFile::ExeUser | QFile::ExeGroup | QFile::ExeOther);

	if (QProcess::startDetached(path_exe, m_args, path_working_dir))
	{
		// процесс запустился и была закачка новой версии => записываем текущую версию
		if (downloaded && !write_new_version())
			alert(tr("Failed to write new version: ") + QString::number(m_new.version));
		//else QTimer::singleShot(400, this, []() { QCoreApplication::exit(0); });
	}
	else alert(tr("Failed to run: ") + path_exe);

	QTimer::singleShot(400, this, []() { qApp->exit(0); });
}
Updater::Data Updater::read_version() const
{
	Data data;
	QFile file(m_path_build + "data");
	if (file.open(QIODevice::ReadOnly))
	{
		if (!file.atEnd())
			data.version = QString(file.readLine()).toInt();
		if (!file.atEnd())
			data.guid = file.readLine();
	}
	return data;
}
bool Updater::write_new_version() const
{
	QFile file(m_path_build + "data");
	if (file.open(QIODevice::WriteOnly | QIODevice::Truncate))
	{
		QTextStream stream(&file);
		stream << m_new.version << '\n' << m_new.guid;
		return true;
	}
	return false;
}
void Updater::alert(const QString& msg)
{
	std::cout << "critical: " << msg.toStdString().c_str() << std::endl;
	emit alert_showed(msg);
	QCoreApplication::exit(-1);
}




/*#ifdef Q_OS_WIN
#include <windows.h>
#include <lmcons.h>
#elif defined Q_OS_UNIX
#include <pwd.h>
#include <unistd.h>
#endif

QString user_name()
{
#ifdef Q_OS_WIN
	WCHAR* lpszSystemInfo;
	DWORD cchBuff = UNLEN;
	WCHAR tchBuffer[UNLEN + 1];
	lpszSystemInfo = tchBuffer;
	GetUserNameW(lpszSystemInfo, &cchBuff);
	return QString::fromWCharArray(lpszSystemInfo);
#elif defined Q_OS_UNIX
	struct passwd* pw;
	pw = getpwuid(getuid());
	if (pw)
		return pw->pw_name;
	return QString();
#endif
}
*/


QString Updater::get_app_data_folder() const
{
	/*std::cout << user_name().toStdString() << std::endl;

	for (int i = 0; i < 19; ++i)
	{
		QStandardPaths::StandardLocation loc = static_cast<QStandardPaths::StandardLocation>(i);
		QString location = QStandardPaths::writableLocation(loc);
		std::cout << i << " " << location.toStdString() << std::endl;
	}*/
#ifdef Q_OS_LINUX
	QString path = qApp->applicationDirPath();
	append_separator(path);
	return path;
#elif defined Q_OS_WIN
	QString location = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
	QString name = QCoreApplication::applicationName();
	QString right = location.right(name.size());
	if (right == name)
		location = location.left(location.size() - name.size());

	append_separator(location);
	return location;
#endif
}
QStringList Updater::build_args(const Settings& settings) const
{
	QStringList args = QCoreApplication::arguments();
	args.removeFirst();
	return args + settings.application_arguments.split(" ", QString::SplitBehavior::SkipEmptyParts);
}
void Updater::remove_old_builds(const QString& dir_name)
{
	QDir dir(m_path_build);
	QStringList dirs = dir.entryList(QDir::Filter::Dirs | QDir::Filter::NoDotAndDotDot);
	for (const QString& entry : dirs)
	{
		if (entry != dir_name)
		{
			QString full_path = dir.absolutePath() + '/' + entry;
			if (!QDir(full_path).removeRecursively())
				std::cout << "error delete: " << full_path.toStdString() << std::endl;
		}
	}
}
void Updater::write_updater_settings() const
{
	QString path = m_new.path(m_path_build) + "updater.settings";
	QFile file(path);
	if (file.open((QFile::WriteOnly | QFile::Text)))
	{
		QJsonDocument json_doc;
		QJsonObject obj;
		obj["updater_path"] = QCoreApplication::applicationFilePath();
		json_doc.setObject(obj);
		file.write(json_doc.toJson());
	}
}
