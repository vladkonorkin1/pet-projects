//=====================================================================================//
//   Author: open
//   Date:   14.12.2018
//=====================================================================================//
#include "precomp.h"
#include "utility.h"

void append_separator(QString& path)
{
	if (!path.isEmpty()) {
		if (path[path.size() - 1] != '/')
			path.append("/");
	}
}