@echo off
SetLocal EnableDelayedExpansion
(set PATH=D:\work\external\x86\qt-everywhere-src-5.12.0-static\qtbase\bin;!PATH!)
if defined QT_PLUGIN_PATH (
    set QT_PLUGIN_PATH=D:\work\external\x86\qt-everywhere-src-5.12.0-static\qtbase\plugins;!QT_PLUGIN_PATH!
) else (
    set QT_PLUGIN_PATH=D:\work\external\x86\qt-everywhere-src-5.12.0-static\qtbase\plugins
)
D:\work\external\x86\qt-everywhere-src-5.12.0-static\qtbase\bin\uic.exe %*
EndLocal
