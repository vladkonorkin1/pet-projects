TARGET = updater
CONFIG -= flat
TEMPLATE = app

win32 {
    release: DESTDIR = tmp/release
    debug:   DESTDIR = tmp/debug
    OBJECTS_DIR = $$DESTDIR/.obj
    MOC_DIR = $$DESTDIR/.moc
    RCC_DIR = $$DESTDIR/.qrc
    UI_DIR = $$DESTDIR/.ui
}
unix {
    DESTDIR = .
    OBJECTS_DIR = $$DESTDIR/tmp/.obj
    MOC_DIR = $$DESTDIR/tmp/.moc
    RCC_DIR = $$DESTDIR/tmp/.qrc
    UI_DIR = $$DESTDIR/tmp/.ui
}
QT = core network
Debug:CONFIG += console

CONFIG += precompile_header
PRECOMPILED_HEADER = src/precomp.h

INCLUDEPATH += src
INCLUDEPATH += ../shared

#RESOURCES = src/ui/resource/project.qrc

win32 {
    DEFINES += UPDATER_COMPILE_GUI

	RC_FILE = src/ui/resource/myapp.rc
	Release:QMAKE_POST_LINK = copy tmp\release\*.exe bin\release
	QMAKE_CXXFLAGS += /MP
	
	#static build
	DEFINES += QUAZIP_BUILD
	INCLUDEPATH += "../../../../../../external/vs2017-x32/quazip-0.7.6-static-qt-5.12.0"
	INCLUDEPATH += "../../../../../../external/vs2017-x32/zlib-1.2.11-build"
	INCLUDEPATH += "../../../../../../external/vs2017-x32/zlib-1.2.11"
	CONFIG(debug, debug|release) {
		QMAKE_LIBDIR += "../../../../../../external/vs2017-x32/quazip-0.7.6-static-qt-5.12.0/quazip/Debug"
		QMAKE_LIBS += "quazipd.lib"
	} else {
		QMAKE_LIBDIR += "../../../../../../external/vs2017-x32/quazip-0.7.6-static-qt-5.12.0/quazip/Release"
		QMAKE_LIBS += "quazip.lib"
	}
	QMAKE_LIBS += "../../../../../../external/vs2017-x32/zlib-1.2.11-build/Release/zlibstatic.lib"
}
unix {
    LIBS += -lquazip

    # Provide relative path from application to library
    QMAKE_LFLAGS += -Wl,--rpath=\\\$\$ORIGIN
}

HEADERS =  	src/updater.h							\
			src/download_file.h						\
			src/single_run.h						\
            src/settings.h							\
            src/utility.h							\

SOURCES = 	src/main.cpp 							\
			src/updater.cpp							\
            src/download_file.cpp					\
			src/single_run.cpp						\
            src/settings.cpp						\
            src/utility.cpp							\

contains(DEFINES, UPDATER_COMPILE_GUI) {
    QT += widgets gui

    HEADERS	+=	src/main_window.h					\
                src/gui.h							\

    SOURCES	+= 	src/main_window.cpp					\
                src/gui.cpp							\

    FORMS	+= 	src/ui/main_window.ui				\

}
