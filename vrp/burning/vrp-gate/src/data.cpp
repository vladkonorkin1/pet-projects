//=====================================================================================//
//   Author: open
//   Date:   23.11.2018
//=====================================================================================//
#include "base/precomp.h"
#include "data.h"
#include "burning_algorithm/burning_json.h"
#include <QJsonObject>
#include <QJsonArray>

Task::Task(int id, Hub* hub)
: id (id)
, hub(hub)
, price_total(std::numeric_limits<double>::max())
, price_clean(std::numeric_limits<double>::max())
, start_time(QDateTime::currentDateTimeUtc())
{
}
bool Task::is_result() const
{
	return !json_result.isNull();
}
void Task::set_finish(const QJsonDocument& result)
{
	json_result = result;
	if (is_result())
		Burning::json_parse_plan_price(json_result, price_total, price_clean);

	quint64 msecs = start_time.msecsTo(QDateTime::currentDateTimeUtc());
	duration = QTime::fromMSecsSinceStartOfDay(msecs);
}


Claim::Claim(int id, const QJsonDocument& json_doc)
: id(id)
, json_doc(json_doc)
, num_finished(0)
, start_time(QDateTime::currentDateTimeUtc())
, state(State::NoInit)
{
	Burning::json_parse_data(json_doc, vrp_data);
}
void Claim::add_task(UTask task)
{
	m_tasks[task->id] = std::move(task);
}
/*void Claim::add_result(int task_id, const QJsonDocument& json_result)
{
	tasks_t::iterator it = m_tasks.find(task_id);
	if (m_tasks.end() != it)
		it->second.get()->set_finish(json_result);
}*/
Task* Claim::find_task(int task_id)
{
	auto it = m_tasks.find(task_id);
	if (m_tasks.end() != it) return it->second.get();
	return nullptr;
}
bool Claim::is_finished() const
{
	return num_finished == m_tasks.size();
}
// �������������� ������ �������
QJsonDocument Claim::convert_claim_results() const
{
	QJsonDocument doc;
	QJsonArray json_array;

	std::vector<const Task*> tasks;
	for (const auto& pair : m_tasks)
	{
		const Task* task = pair.second.get();
		if (task->is_result())
			tasks.push_back(task);
	}

	std::sort(tasks.begin(), tasks.end(), [](const Task* left_task, const Task* right_task) { return left_task->price_total < right_task->price_total; });

	for (const Task* task : tasks)
		json_array.append(task->json_result.object());

	if (!json_array.empty())
		doc.setArray(json_array);
	return doc;
}
// ����� ������ ���������
const Task* Claim::find_best_task() const
{
	const Task* best_task = nullptr;
	double best_fitness = std::numeric_limits<double>::max();
	for (const auto& pair : m_tasks)
	{
		const Task* task = pair.second.get();
		if (task->is_result())
		{
			if (task->price_total < best_fitness)
			{
				best_fitness = task->price_total;
				best_task = task;
			}
		}
	}
	return best_task;
}
// �������� ������ 
QString Claim::get_state_str() const
{
	if (state == State::NoInit) return "no init";
	if (state == State::Init) return "init";
	if (state == State::Runned) return "runned";
	return QString();
}
// �������� ��������� ������� ������
void Claim::add_result(int task_id, const QJsonDocument& json_result)
{
	if (Task* task = find_task(task_id))
	{
		if (task->hub)
		{
			task->set_finish(json_result);
			num_finished++;
			task->hub = nullptr;

			if (is_finished())
			{
				quint64 msecs = start_time.msecsTo(QDateTime::currentDateTimeUtc());
				duration = QTime::fromMSecsSinceStartOfDay(msecs);
			}
		}
	}
}