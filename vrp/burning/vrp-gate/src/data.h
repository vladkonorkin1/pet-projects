//=====================================================================================//
//   Author: open
//   Date:   23.11.2018
//=====================================================================================//
#pragma once
#include "osrm/osrm_request_matrix_distances.h"
#include "burning_algorithm/burning_data.h"
#include <QJsonDocument>

class Hub;

struct Task
{
	int id;
	Hub* hub;
	QDateTime start_time;
	QTime duration;
	QJsonDocument json_result;
	double price_total;
	double price_clean;

	Task(int id, Hub* hub);
	bool is_result() const;
	void set_finish(const QJsonDocument& result);
};

using UTask = std::unique_ptr<Task>;


struct Claim
{
	int id;
	QJsonDocument json_doc;
	Burning::Data vrp_data;
	int num_finished;
	QDateTime start_time;
	QTime duration;
	SOsrmRequestMatrixDistances request_matrix_distances;

	enum class State
	{
		NoInit,
		Init,
		Runned
	};
	State state;

	using tasks_t = std::map<int, UTask>;
private:
	tasks_t m_tasks;

public:
	Claim(int id, const QJsonDocument& json_doc);
	void add_task(UTask task);
	//void add_result(int task_id, const QJsonDocument& json_result);
	Task* find_task(int task_id);
	const tasks_t& tasks() const { return m_tasks; }
	int runned_task_count() const { return (int)m_tasks.size() - num_finished; }
	bool is_finished() const;
	// �������������� ������ �������
	QJsonDocument convert_claim_results() const;
	// ����� ������ ���������
	const Task* find_best_task() const;
	// �������� ������ 
	QString get_state_str() const;
	// �������� ��������� ������� ������
	void add_result(int task_id, const QJsonDocument& json_result);
};

//using SClaim = std::shared_ptr<Claim>;
using UClaim = std::unique_ptr<Claim>;