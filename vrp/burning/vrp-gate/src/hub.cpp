//=====================================================================================//
//   Author: open
//   Date:   23.11.2018
//=====================================================================================//
#include "base/precomp.h"
#include "hub.h"
#include "net_gate/net_gate_session.h"
#include "net_gate/net_gate_messages.h"
#include "net_gate/net_gate_connection.h"

Hub::Hub(NetGate::Session* session)
: m_session(session)
{
}
// ���������� ����������
void Hub::update_connection(NetGate::Connection* connection)
{
	if (connection)
		connect(connection, SIGNAL(task_solved(int, int, const QJsonDocument&)), SLOT(slot_task_solved(int, int, const QJsonDocument&)));
}
// ������� ������ � ��������
bool Hub::open_tasks(const Claim& claim, const std::set<int>& task_ids)
{
	if (m_claims.end() != m_claims.find(claim.id)) return false;
	auto it = m_claims.insert(std::make_pair(claim.id, HubClaim{ claim, task_ids })).first;
	send_open_claim_tasks(it->second);
	return true;
}
// ��������� ��������� �� �������� ������ �� �������
void Hub::send_open_claim_tasks(const HubClaim& hub_claim)
{
	std::vector<int> task_ids;
	for (int id : hub_claim.task_ids)
		task_ids.push_back(id);
	m_session->send_message(NetGate::ServerMessageOpenTasks(hub_claim.claim.id, hub_claim.claim.json_doc, hub_claim.claim.vrp_data.matrix_distances, task_ids));
}
// ��������� ��� ������
void Hub::send_all_tasks()
{
	for (const auto& pair : m_claims)
		send_open_claim_tasks(pair.second);
}
// ����������� ������ � �������� �������
void Hub::send_request_report_solved_tasks()
{
	m_session->send_message(NetGate::ServerMessageReportSolvedTasks());
}
void Hub::slot_task_solved(int claim_id, int task_id, const QJsonDocument& json_result)
{
	close_task(claim_id, task_id, json_result);
}
// ������� ������
void Hub::close_task(int claim_id, int task_id, const QJsonDocument& json_result)
{
	// ���������� ��������� � �������� ������
	m_session->send_message(NetGate::ServerMessageCloseTask(claim_id, task_id));

	auto claim_it = m_claims.find(claim_id);
	if (m_claims.end() != claim_it)
	{
		emit task_closed(claim_id, task_id, json_result);
		claim_it->second.task_ids.erase(task_id);
		if (claim_it->second.task_ids.empty())
			m_claims.erase(claim_it);
	}
}
// ������� ��� ������
void Hub::close_all_tasks()
{
	for (const auto& pair : m_claims)
	{
		const HubClaim& hub_claim = pair.second;
		for (int task_id : hub_claim.task_ids)
			emit task_closed(hub_claim.claim.id, task_id, QJsonDocument());
	}
	m_claims.clear();
}
// ���-�� ����� �� ����������
int Hub::task_size() const
{
	int num = 0;
	for (const auto& pair : m_claims)
		num += (int)pair.second.task_ids.size();
	return num;
}
// ����. ���-�� �����
int Hub::max_task_size() const
{
	return m_session->number_threads();
}
int Hub::id() const
{
	return m_session->id();
}
// ������� ������ � ����������
void Hub::remove_task(int claim_id, int task_id)
{
	close_task(claim_id, task_id, QJsonDocument());
}
// �������� ������
const NetGate::Session* Hub::session() const
{
	return m_session;
}