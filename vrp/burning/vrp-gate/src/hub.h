//=====================================================================================//
//   Author: open
//   Date:   23.11.2018
//=====================================================================================//
#pragma once
#include "data.h"

namespace NetGate
{
	class Session;
	class Connection;
}

class Hub : public QObject
{
	Q_OBJECT
private:
	NetGate::Session* m_session;

	struct HubClaim
	{
		const Claim& claim;
		std::set<int> task_ids;
	};
	using claims_t = std::map<int, HubClaim>;
	claims_t m_claims;

public:
	Hub(NetGate::Session* session);
	// обновление соединения
	void update_connection(NetGate::Connection* connection);
	// получить сессию
	const NetGate::Session* session() const;
	// открыть заявку с задачами
	bool open_tasks(const Claim& claim, const std::set<int>& task_ids);
	// удалить задачу с исполнения
	void remove_task(int claim_id, int task_id);
	// отправить все задачи
	void send_all_tasks();
	// потребовать отчёта о решённых задачах
	void send_request_report_solved_tasks();
	// кол-во задач на исполнении
	int task_size() const;
	// макс. кол-во задач
	int max_task_size() const;
	// закрыть все задачи
	void close_all_tasks();
	// id
	int id() const;

signals:
	void task_closed(int claim_id, int task_id, const QJsonDocument& json_result);

private slots:
	void slot_task_solved(int claim_id, int task_id, const QJsonDocument& json_result);

private:
	// закрыть задачу
	void close_task(int claim_id, int task_id, const QJsonDocument& json_result);
	// отправить сообщение об открытии заявки на решение
	void send_open_claim_tasks(const HubClaim& hub_claim);
};

using UHub = std::unique_ptr<Hub>;
