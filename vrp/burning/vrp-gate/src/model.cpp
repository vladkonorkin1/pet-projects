//=====================================================================================//
//   Author: open
//   Date:   23.11.2018
//=====================================================================================//
#include "base/precomp.h"
#include "model.h"
#include "net_gate/net_gate_server.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QDir>

Model::Model(NetGate::Server* server, const QString& osrm_server_address, int min_num_solutions)
: m_osrm_manager(m_network_manager, osrm_server_address)
, m_min_num_solutions(min_num_solutions)
, m_server(server)
{
	connect(server, SIGNAL(session_opened(NetGate::Session*)), SLOT(slot_session_opened(NetGate::Session*)));
	connect(server, SIGNAL(session_closed(NetGate::Session*)), SLOT(slot_session_closed(NetGate::Session*)));
	connect(server, SIGNAL(session_connection_created(NetGate::Session*, NetGate::Connection*)), SLOT(slot_session_connection_created(NetGate::Session*, NetGate::Connection*)));
}
// добавить заявку
void Model::add_claim(int claim_id, const QJsonDocument& json_doc)
{
	auto it = m_claims.find(claim_id);
	if (m_claims.end() == it)
	{
		UClaim claim(new Claim(claim_id, json_doc));
		logs(QString("claim %1 open -> count points: %2").arg(claim->id).arg(claim->vrp_data.destination_points.size()));
		save_request(claim.get());
		
		if (claim->vrp_data.is_init())
		{
			Claim* pclaim = claim.get();
			m_claims[claim_id] = std::move(claim);
			request_matrix_distances(pclaim);
		}
		else close_claim(claim->id, QJsonDocument());

		run_next_claim();	
	}
}
// запросить матрицу дистанций
void Model::request_matrix_distances(Claim* claim)
{
	vpositions_t points;
	points.reserve(claim->vrp_data.destination_points.size());
	for (const Burning::DestinationPoint& destination_point : claim->vrp_data.destination_points)
		points.push_back(QPointF(destination_point.lon, destination_point.lat));

	claim->request_matrix_distances = m_osrm_manager.create_request_matrix_distances(points);
	connect(claim->request_matrix_distances.get(), &OsrmRequestMatrixDistances::finished, [this, claim_id = claim->id](bool finished, const SMatrixDistances& matrix) { on_request_matrix_distances_finished(claim_id, finished, matrix); });
	m_osrm_manager.request_matrix_distances(claim->request_matrix_distances, claim->vrp_data.use_osrm_cache);
}
// событие получения матрицы дистанций
void Model::on_request_matrix_distances_finished(int claim_id, bool finished, const SMatrixDistances& matrix)
{
	// если заявка ещё существует
	auto it = m_claims.find(claim_id);
	if (m_claims.end() != it)
	{
		Claim* claim = it->second.get();

		if (finished)
		{
			assert( claim->state == Claim::State::NoInit );
			// заявка инициализирована
			claim->state = Claim::State::Init;
			claim->vrp_data.matrix_distances = matrix;
			// переносим заявку в очередь
			m_new_claims.enqueue(claim);
		}
		else close_claim(claim_id, QJsonDocument());
		
		// запускаем на исполнение
		run_next_claim();
	}
}
// запустить на исполнение следующую заявку из очереди
void Model::run_next_claim()
{
	while (!m_new_claims.empty())
	{
		Claim* claim = m_new_claims.first();
		if (start_solving_claim(claim))
			//m_new_claims.dequeue();
			m_new_claims.takeFirst();
		else
		{
			//close_claim(claim->id, QJsonDocument());
			break;
		}
	}
}
// общее кол-во тредов
int Model::num_total_threads() const
{
	int count = 0;
	for (const auto& pair : m_hubs)
	{
		Hub* hub = pair.second.get();
		count += hub->max_task_size();
	}
	return count;
}
// кол-во потоков, которые можно задействовать для вместимости новой задачи
int Model::get_num_releasable_threads() const
{
	int min_need_threads = 0;
	for (const auto& pair : m_runned_claims)
	{
		Claim* claim = pair.second;
		min_need_threads += std::min(claim->runned_task_count(), m_min_num_solutions);
	}
	int num_releasable = num_total_threads() - min_need_threads;
	assert( num_releasable >= 0 );
	return num_releasable;
}
// освободить место под новые задачи (от скольки решений нужно отказаться, чтобы вместилась новая задача)
void Model::kick_claim_tasks()
{
	std::vector<Claim*> claims;
	// кол-во тредов
	int num_all_threads = num_total_threads();

	// отсекаем заявки меньше или равные минимально допустимому кол-ву решений
	for (auto& pair : m_runned_claims)
	{
		Claim* claim = pair.second;
		if (claim->runned_task_count() > m_min_num_solutions)
			claims.push_back(claim);
		else
			num_all_threads -= claim->runned_task_count();
	}

	std::sort(claims.begin(), claims.end(), [this](const Claim* l, const Claim* r) { return l->runned_task_count() < r->runned_task_count(); });

	int cur_num_threads = num_all_threads;
	for (int i = 0; i < claims.size(); ++i)
	{
		Claim* claim = claims[i];
		int count_avg = cur_num_threads / ((int)claims.size() + 1 - i);
		int num_kicked = claim->runned_task_count() - count_avg;
		if (num_kicked > 0)
		{
			// удаляем лишние задачи
			kick_random_tasks(claim, num_kicked);
		}
		cur_num_threads -= claim->runned_task_count();// -num_kicked;
	}
}
// снимаем с исполнения случайные задачи
void Model::kick_random_tasks(Claim* claim, int count)
{
	// переносим в список незавершённые задачи
	std::vector<Task*> tasks;
	for (auto& pair : claim->tasks())
	{
		Task* task = pair.second.get();
		if (task->hub)
			tasks.push_back(task);
	}

	// перемешиваем рандомно
	std::random_shuffle(tasks.begin(), tasks.end());

	// снимаем с исполнения
	QJsonDocument doc_empty;
	for (int i = 0; i < count; ++i)
	{
		Task* task = tasks[i];
		assert( task->hub );
		task->hub->remove_task(claim->id, task->id);
		assert( !task->hub );
		//task->hub = nullptr;
	}
}
// ставим задачи на все свободные ядра
bool Model::distribute_claim(Claim* claim)
{
	int task_id = 0;
	for (auto& pair : m_hubs)
	{
		Hub* hub = pair.second.get();
		int free = hub->max_task_size() - hub->task_size();
		if (free > 0)
		{
			std::set<int> task_ids;
			for (int i = 0; i < free; ++i)
			{
				UTask task(new Task(task_id, hub));
				claim->add_task(std::move(task));
				task_ids.insert(task_id++);
			}
			hub->open_tasks(*claim, task_ids);
			log_setup_solvers(claim, hub, task_ids);
		}
	}
	if (claim->tasks().size() > 0)
	{
		if (claim->state == Claim::State::Init)
		{
			int b = 0;
		}
		else
		{
			int b = 0;
		}
		assert( claim->state == Claim::State::Init );
		claim->state = Claim::State::Runned;
		m_runned_claims[claim->id] = claim;
		return true;
	}
	assert( 0 );
	return false;
}
// вывести в лог назначенных решателей
void Model::log_setup_solvers(const Claim* claim, const Hub* hub, const std::set<int>& task_ids) const
{
	QString str;
	auto it = task_ids.begin();
	for (int i = 0; i < task_ids.size(); ++it, ++i)
	{
		str += QString::number(*it);
		if ((task_ids.size() - 1) != i) str += ", ";
	}
	logs(QString("claim %1  setup solvers hub=%2 tasks=%3").arg(claim->id).arg(hub->session()->name()).arg(str));
}
// запустить заявку на исполнение
bool Model::start_solving_claim(Claim* claim)
{
	// сколько ядер можем освободить?
	int num_releasable_threads = get_num_releasable_threads();

	int min_num_solutions = m_runned_claims.empty() ? 1 : m_min_num_solutions;
	if (num_releasable_threads >= min_num_solutions)
	{
		logs(QString("claim %1  start solving").arg(claim->id));
		// освободить место под новые задачи
		kick_claim_tasks();
		// ставим задачи на все свободные ядра
		return distribute_claim(claim);
	}
	return false;
}
// удалить заявку
void Model::remove_claim(int claim_id)
{
	logs(QString("claim %1 close").arg(claim_id));

	auto it = m_claims.find(claim_id);
	if (m_claims.end() != it)
	{
		Claim* claim = it->second.get();
		if (claim->state == Claim::State::Init)
		{
			auto it_new = std::find(m_new_claims.begin(), m_new_claims.end(), claim);
			if (m_new_claims.end() != it_new)
				m_new_claims.erase(it_new);
		}
		else if (claim->state == Claim::State::Runned)
		{
			m_runned_claims.erase(claim_id);
			// снять с исполнения со всех хабов
			for (auto& pair : claim->tasks())
			{
				Task* task = pair.second.get();
				if (task->hub)
					task->hub->remove_task(claim_id, task->id);
			}
		}
		m_claims.erase(it);
	}

	run_next_claim();
}
// закрыть заявку (emit claim_solved)
void Model::close_claim(int claim_id, const QJsonDocument& json_result)
{
	emit claim_solved(claim_id, json_result);
}
// открытие сессии
void Model::slot_session_opened(NetGate::Session* session)
{
	assert( m_hubs.find(session->id()) == m_hubs.end() );
	UHub hub(new Hub(session));
	connect(hub.get(), SIGNAL(task_closed(int, int, const QJsonDocument&)), SLOT(slot_hub_task_closed(int, int, const QJsonDocument&)));
	m_hubs[session->id()] = std::move(hub);
}
// закрытие сессии
void Model::slot_session_closed(NetGate::Session* session)
{
	auto it = m_hubs.find(session->id());
	if (m_hubs.end() != it)
	{
		// оповестить, что задачи не решены
		it->second->close_all_tasks();
		m_hubs.erase(it);
	}
}
// создание нового соединения для сессии
void Model::slot_session_connection_created(NetGate::Session* session, NetGate::Connection* connection)
{
	// произошло переподключение к сессии
	auto it = m_hubs.find(session->id());
	if (m_hubs.end() != it)
	{
		Hub* hub = it->second.get();
		hub->update_connection(connection);
		// заново отправляем все поставленные для данной сессии задачи
		hub->send_all_tasks();
		// запрос отчёта по выполненым задачам
		hub->send_request_report_solved_tasks();
	}
	run_next_claim();
}
// событие закрытия задачи
void Model::slot_hub_task_closed(int claim_id, int task_id, const QJsonDocument& json_result)
{
	auto it = m_claims.find(claim_id);
	if (m_claims.end() == it) return;
	Claim* claim = it->second.get();

	if (Task* task = claim->find_task(task_id))
	{
		const QString& hub_name = task->hub->session()->name();
		claim->add_result(task_id, json_result);
		QString task_duration = task->duration.toString("hh:mm:ss");
		if (json_result.isNull()) logs(QString("claim %1   [%2] error solving task=%3 hub=%4").arg(claim->id).arg(task_duration).arg(task->id).arg(hub_name));
		else logs(QString("claim %1   [%2] solved task=%3 hub=%4 price_total=%5 price_clean=%6").arg(claim->id).arg(task_duration).arg(task->id).arg(hub_name).arg(task->price_total).arg(task->price_clean));

	}
	/*if (Task* task = claim->find_task(task_id))
	{
		task->set_finish(json_result);
		claim->num_finished++;

		assert( task->hub );
		if (task->is_result()) logs(QString("claim %1  [%2] solved task=%3 hub=%4 price_total=%5 price_clean=%6").arg(claim->id).arg(task->duration.toString("hh:mm:ss")).arg(task->id).arg(task->hub->session()->name()).arg(task->price_total).arg(task->price_clean));
		else logs(QString("claim %1  [%2] error solving task=%3 hub=%4").arg(claim->id).arg(task->duration.toString("hh:mm:ss")).arg(task->id).arg(task->hub->session()->name()));
		task->hub = nullptr;
	}*/

	if (claim->is_finished())
	{
		// закрываем заявку
		close_claim(claim_id, choose_claim_result(claim));

		// запускаем следующую заявку на исполнение
		//run_next_claim();
	}
}
// выбрать результат для заявки
QJsonDocument Model::choose_claim_result(const Claim* claim) const
{
	QString claim_duration = claim->duration.toString("hh:mm:ss");
	if (const Task* task = claim->find_best_task())
	{
		logs(QString("claim %1  [%2] best result task=%3 price_total=%4 price_clean=%5").arg(claim->id).arg(claim_duration).arg(task->id).arg(task->price_total).arg(task->price_clean));
		if (claim->vrp_data.show_all_results) return claim->convert_claim_results();
		return task->json_result;
	}
	logs(QString("claim %1  [%2] error to find best result").arg(claim->id).arg(claim_duration));
	return QJsonDocument();
}
QJsonDocument Model::build_stats() const
{
	QJsonDocument doc;
	QJsonObject obj;
	int num_total_threads = 0;
	int num_total_tasks = 0;
	QJsonArray json_machines;
	for (const auto& pair : m_hubs)
	{
		Hub* hub = pair.second.get();
		num_total_threads += hub->max_task_size();
		num_total_tasks += hub->task_size();

		QJsonObject obj;
		obj["ip"] = hub->session()->ip();
		obj["pc_name"] = hub->session()->name();
		obj["user_name"] = hub->session()->user_name();
		obj["num_threads"] = hub->max_task_size();
		obj["num_tasks"] = (int)hub->task_size();
		json_machines.append(obj);
	}
	if (!json_machines.empty())
		obj["machines"] = json_machines;
	obj["num_total_threads"] = num_total_threads;
	obj["num_total_tasks"] = num_total_tasks;

	QJsonArray json_claims = build_json_claims();
	if (!json_claims.empty())
		obj["claims"] = json_claims;

	obj["version"] = m_server->protocol_version();

	doc.setObject(obj);
	return doc;
}
QJsonArray Model::build_json_claims() const
{
	QJsonArray json_claims;
	for (const auto& pair : m_claims)
	{
		const Claim* claim = pair.second.get();

		QJsonObject json_claim;
		json_claim["id"] = claim->id;
		json_claim["name"] = claim->vrp_data.name;
		json_claim["state"] = claim->get_state_str();

		struct HubRunnedCount
		{
			const Hub* hub;
			int count;
		};
		std::map<int, HubRunnedCount> hub_runned_tasks;
		for (const auto& pair : claim->tasks())
		{
			const Task* task = pair.second.get();
			if (task->hub)
			{
				int hub_id = task->hub->id();
				auto it = hub_runned_tasks.find(hub_id);
				if (hub_runned_tasks.end() == it)
					it = hub_runned_tasks.insert(std::make_pair(hub_id, HubRunnedCount{task->hub, 0})).first;
				it->second.count++;
			}
		}

		QJsonArray json_hubs;
		for (const auto& pair : hub_runned_tasks)
		{
			const HubRunnedCount& hub_runned_count = pair.second;
			QJsonObject obj;
			obj["name"] = hub_runned_count.hub->session()->name();
			obj["num_tasks"] = hub_runned_count.count;
			json_hubs.append(obj);
		}
		if (!json_hubs.empty())
			json_claim["hubs"] = json_hubs;

		json_claims.append(json_claim);
	}
	return json_claims;
}
// сохранить запрос
void Model::save_request(const Claim* claim) const
{
	QDir().mkdir("requests");
	QString path = QString("requests/%1 %2 %3.json").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss")).arg(claim->id).arg(claim->vrp_data.name);
	save_json(claim->json_doc, path);
}
// сохранить json-document
void Model::save_json(const QJsonDocument& json_doc, const QString& filename) const
{
	QFile file(filename);
	if (file.open(QIODevice::WriteOnly))
		file.write(json_doc.toJson());
}
