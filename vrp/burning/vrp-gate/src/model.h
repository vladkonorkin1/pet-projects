//=====================================================================================//
//   Author: open
//   Date:   23.11.2018
//=====================================================================================//
#pragma once
#include <QJsonDocument>
#include <QQueue>
#include "base/network_manager.h"
#include "osrm/osrm_manager.h"
#include "hub.h"

namespace NetGate
{
	class Session;
	class Server;
}

class Model : public QObject
{
	Q_OBJECT
private:
	NetworkManager m_network_manager;
	OsrmManager m_osrm_manager;
	NetGate::Server* m_server;

	// минимальное кол-во решений 
	int m_min_num_solutions;
	// хабы решатели
	using hubs_t = std::map<int, UHub>;
	hubs_t m_hubs;
	// все заявки
	std::map<int, UClaim> m_claims;
	// очередь новых заявок на решение
	QQueue<Claim*> m_new_claims;
	// запущенные на исполнение заявки
	std::map<int, Claim*> m_runned_claims;

public:
	Model(NetGate::Server* server, const QString& osrm_server_address, int min_num_solutions);
	// добавить заявку
	void add_claim(int claim_id, const QJsonDocument& json_doc);
	// удалить заявку
	void remove_claim(int claim_id);
	// собрать статистику
	QJsonDocument build_stats() const;

signals:
	void claim_solved(int claim_id, const QJsonDocument& json_result);

private slots:
	// открытие сессии
	void slot_session_opened(NetGate::Session* session);
	// закрытие сессии
	void slot_session_closed(NetGate::Session* session);
	// создание нового соединения для сессии
	void slot_session_connection_created(NetGate::Session* session, NetGate::Connection* connection);
	// событие закрытия задачи
	void slot_hub_task_closed(int claim_id, int task_id, const QJsonDocument& json_result);

private:
	// запустить на исполнение следующую заявку из очереди
	void run_next_claim();
	// запросить матрицу дистанций
	void request_matrix_distances(Claim* claim);
	// событие получения матрицы дистанций
	void on_request_matrix_distances_finished(int claim_id, bool finished, const SMatrixDistances& matrix);
	// запустить заявку на исполнение
	bool start_solving_claim(Claim* claim);
	// кол-во потоков, которые можно задействовать для вместимости новой задачи
	int get_num_releasable_threads() const;
	// освободить место под новые задачи
	void kick_claim_tasks();
	// ставим задачи на все свободные ядра
	bool distribute_claim(Claim* claim);
	// общее кол-во тредов
	int num_total_threads() const;
	// снимаем с исполнения случайные задачи
	void kick_random_tasks(Claim* claim, int count);
	// выбрать результат для заявки
	QJsonDocument choose_claim_result(const Claim* claim) const;
	// закрыть заявку (emit claim_solved)
	void close_claim(int claim_id, const QJsonDocument& json_result);
	// вывести в лог назначенных решателей
	void log_setup_solvers(const Claim* claim, const Hub* hub, const std::set<int>& task_ids) const;
	// создать статистику по заявкам
	QJsonArray build_json_claims() const;
	// сохранить запрос
	void save_request(const Claim* claim) const;
	// сохранить json-document
	void save_json(const QJsonDocument& json_doc, const QString& filename) const;
};
