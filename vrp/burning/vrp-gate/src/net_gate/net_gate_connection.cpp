﻿//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "net_gate_connection.h"
#include "net_gate_server.h"
#include <QHostAddress>
#include <QTcpSocket>

namespace NetGate
{

Connection::Connection(QTcpSocket* socket, ConnectionId id, Server* server)
: m_connection(socket)
, m_socket(socket)
, m_id(id)
, m_time_alive(0.f)
, m_close_session(true)
, m_server(server)
{
	connect(&m_connection, SIGNAL(disconnected()), SLOT(slot_disconnect()));
	connect(&m_connection, SIGNAL(data_received(const char*, unsigned int)), SLOT(slot_data_recieved(const char*, unsigned int)));
	m_message_dispatcher.set_observer(this);
	m_message_dispatcher.register_message<NetGate::ClientMessageCheckProtocolVersion>();
	m_message_dispatcher.register_message<NetGate::ClientMessageAutorizate>();
	m_message_dispatcher.register_message<NetGate::ClientMessageAlive>();
	m_message_dispatcher.register_message<NetGate::ClientMessageTaskSolved>();
}
void Connection::slot_disconnect()
{
	m_socket->deleteLater();
	emit disconnected(this, m_close_session);
}
void Connection::send_message(const NetMessage& message)
{
	m_connection.send_message(message);
}
void Connection::slot_data_recieved(const char* data, unsigned int size)
{
	m_message_dispatcher.process(data, size);
}
void Connection::close(bool close_session)
{
	m_close_session = close_session;
	m_connection.close();
}
ConnectionId Connection::id() const
{
	return m_id;
}
const QUuid& Connection::session_uuid() const
{
	return m_session_uuid;
}
QString Connection::address() const
{
	QString ip = m_socket->peerAddress().toString();
	ip.remove("::ffff:");
	return ip;
}
void Connection::on_message(const NetGate::ClientMessageCheckProtocolVersion& msg)
{
	bool success = m_server->protocol_version() == msg.protocol_version;
	send_message(ServerMessageCheckProtocolVersionResponse(success));
	if (!success)
	{
		logs(QString("net gate: connection %1 %2: wrong protocol version %3 (need %4)").arg(m_id).arg(address()).arg(msg.protocol_version).arg(m_server->protocol_version()));
		//m_connection.close();
		m_socket->close();
	}
}
void Connection::on_message(const NetGate::ClientMessageAutorizate& msg)
{
	m_session_uuid = msg.id;
	bool new_session = true;
	bool auth_result = !m_session_uuid.isNull();

	if (auth_result)
		emit authorized(this, m_session_uuid, msg.pc_name, msg.user_name, msg.number_threads, new_session);

	send_message(ServerMessageAutorizateResponse(auth_result, new_session));
}
void Connection::on_message(const NetGate::ClientMessageAlive& msg)
{
	Q_UNUSED(msg);
	m_time_alive = 0.f;
	//logs("keep alive");
}
bool Connection::check_destroy(float dt)
{
	m_time_alive += dt;
	float max_time = m_session_uuid.isNull() ? 4.f : 40.f;
	//float max_time = m_session_id.isNull() ? 4.f : 300.f;
	if (m_time_alive > max_time)
	{
		// считаем, что соединение потеряно
		close(false);	// сессию не закрываем!
		return true;
	}
	return false;
}
void Connection::on_message(const NetGate::ClientMessageTaskSolved& msg)
{
	emit task_solved(msg.claim_id, msg.task_id, msg.json_result);
}

}
