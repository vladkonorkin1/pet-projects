//=====================================================================================//
//   Author: open
//   Date:   12.09.2017
//=====================================================================================//
#include "base/precomp.h"
#include "net_gate_server.h"

namespace NetGate
{

Server::Server(int protocol_version)
: m_timer(1.f)
, m_generate_connection_id(0)
, m_session_id_generator(0)
, m_protocol_version(protocol_version)
{
	connect(&m_timer, SIGNAL(updated(float)), SLOT(slot_timer_updated(float)));
	connect(&m_tcp_server, SIGNAL(newConnection()), this, SLOT(slot_accept_connection()));
	m_tcp_server.listen(QHostAddress::Any, NetGate::ServerPort);
}
void Server::slot_accept_connection()
{
	UConnection uconnection(new Connection(m_tcp_server.nextPendingConnection(), ++m_generate_connection_id, this));
	Connection* connection = uconnection.get();
	connect(connection, SIGNAL(disconnected(Connection*, bool)), this, SLOT(slot_connection_disconnected(Connection*, bool)));
	connect(connection, SIGNAL(authorized(Connection*, const QUuid&, const QString&, const QString&, int, bool&)), SLOT(slot_connection_authorized(Connection*, const QUuid&, const QString&, const QString&, int, bool&)));
	m_connections[connection->id()] = std::move(uconnection);

	logs(QString("net gate: create connection=%1 ip=%2").arg(connection->id()).arg(connection->address()));
	emit connection_created(connection);
}
void Server::slot_connection_disconnected(Connection* connection, bool need_close_session)
{
	logs(QString("net gate: destroy connection=%1").arg(connection->id()));

	sessions_t::iterator it_session = m_sessions.find(connection->session_uuid());
	if (m_sessions.end() != it_session)
	{
		Session* session = it_session->second.get();
		session->set_connection(nullptr);
		emit session_connection_destroyed(session, connection);

		if (need_close_session)
			close_session(it_session);
	}

	connections_t::iterator it_connection = m_connections.find(connection->id());
	if (it_connection != m_connections.end())
	{
		emit connection_destroyed(connection);
		m_connections.erase(it_connection);
	}
}
void Server::slot_connection_authorized(Connection* connection, const QUuid& session_id, const QString& pc_name, const QString& user_name, int number_threads, bool& new_session)
{
	sessions_t::iterator it = m_sessions.find(session_id);
	new_session = m_sessions.end() == it;
	if (new_session)
	{
		it = m_sessions.insert(std::make_pair(session_id, USession(new Session(++m_session_id_generator, session_id, pc_name, user_name, number_threads)))).first;
		Session* session = it->second.get();
		logs(QString("net gate: open session=%1 connection=%2 machine=%3 threads=%4").arg(session->id()).arg(connection->id()).arg(pc_name).arg(number_threads));
		emit session_opened(session);
	}
	else logs(QString("net gate: restore session=%1 connection=%2 machine=%3").arg(it->second->id()).arg(connection->id()).arg(pc_name));

	Session* session = it->second.get();
	session->set_connection(connection);
	emit session_connection_created(session, connection);
}
void Server::slot_timer_updated(float dt)
{
	for (connections_t::iterator it = m_connections.begin(); it != m_connections.end(); )
	{
		Connection* connection = it->second.get();
		++it;
		connection->check_destroy(dt);
	}
	for (sessions_t::iterator it = m_sessions.begin(); it != m_sessions.end(); )
	{
		Session* session = it->second.get();
		sessions_t::iterator it_session = it++;
		if (session->check_destroy(dt))
			close_session(it_session);
	}
}
void Server::close_session(sessions_t::iterator it_session)
{
	Session* session = it_session->second.get();
	logs(QString("net gate: close session=%1 machine=%2").arg(session->id()).arg(session->name()));
	emit session_closed(session);
	m_sessions.erase(it_session);
}

}
