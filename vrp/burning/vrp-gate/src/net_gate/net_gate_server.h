//=====================================================================================//
//   Author: open
//   Date:   12.09.2017
//=====================================================================================//
#pragma once
#include "net_gate/net_gate_connection.h"
#include "net_gate/net_gate_session.h"
#include "base/timer.h"
#include <QTcpServer>

namespace NetGate
{

class Server : public QObject
{
	Q_OBJECT
private:
	Base::Timer m_timer;
	QTcpServer m_tcp_server;
	ConnectionId m_generate_connection_id;

	using connections_t = std::map<ConnectionId, UConnection>;
	connections_t m_connections;

	using sessions_t = std::map<QUuid, USession>;
	sessions_t m_sessions;

	int m_session_id_generator;

	int m_protocol_version;

public:
	Server(int protocol_version);
	int protocol_version() const { return m_protocol_version; }

signals:
	void connection_created(NetGate::Connection* connection);
	void connection_destroyed(NetGate::Connection* connection);

	void session_opened(NetGate::Session* session);
	void session_closed(NetGate::Session* session);

	void session_connection_created(NetGate::Session* session, NetGate::Connection* connection);
	void session_connection_destroyed(NetGate::Session* session, NetGate::Connection* connection);

private slots:
	void slot_accept_connection();
    void slot_connection_disconnected(Connection* connection, bool need_close_session);
	void slot_connection_authorized(Connection* connection, const QUuid& session_id, const QString& pc_name, const QString& user_name, int number_threads, bool& new_session);
	void slot_timer_updated(float dt);

private:
	void close_session(sessions_t::iterator it_session);
};

}
