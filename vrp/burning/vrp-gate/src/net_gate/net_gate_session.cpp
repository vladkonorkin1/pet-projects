//=====================================================================================//
//   Author: open
//   Date:   13.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "net_gate_session.h"
#include "net/net_message.h"
#include "net_gate/net_gate_connection.h"

namespace NetGate
{

#ifdef _DEBUG
float Session::s_destroy_timeout = 2 * 60;
#else
float Session::s_destroy_timeout = 1 * 60;
#endif

Session::Session(int id, const QUuid& uuid, const QString& name, const QString& user_name, int number_threads)
: m_id(id)
, m_uuid(uuid)
, m_name(name)
, m_user_name(user_name)
, m_number_threads(number_threads)
, m_connection(nullptr)
, m_time_alive(0.f)
{
}
int Session::id() const
{
	return m_id;
}
const QUuid& Session::uuid() const
{
	return m_uuid;
}
void Session::set_connection(Connection* connection)
{
	m_connection = connection;
}
bool Session::check_destroy(float dt)
{
	if (!m_connection)
	{
		m_time_alive += dt;
		if (m_time_alive > s_destroy_timeout)
			return true;
	}
	else m_time_alive = 0.f;
	return false;
}
void Session::send_message(const NetMessage& msg)
{
	if (m_connection)
		m_connection->send_message(msg);
}
Connection* Session::connection()
{
	return m_connection;
}
const QString& Session::name() const
{
	return m_name;
}
QString Session::ip() const
{
	if (m_connection)
		return m_connection->address();
	return QString();
}

}
