//=====================================================================================//
//   Author: open
//   Date:   13.09.2018
//=====================================================================================//
#pragma once

struct NetMessage;

namespace NetGate
{

class Connection;

class Session
{
private:
	int m_id;
	QUuid m_uuid;
	QString m_name;
	QString m_user_name;
	int m_number_threads;
	Connection* m_connection;
	float m_time_alive;
	static float s_destroy_timeout;

public:
	Session(int id, const QUuid& uuid, const QString& name, const QString& user_name, int number_threads);
	int id() const;
	const QUuid& uuid() const;
	const QString& name() const;
	const QString& user_name() const { return m_user_name; }
	QString ip() const;
	int number_threads() const { return m_number_threads; }
	void set_connection(Connection* connection);
	Connection* connection();
	bool check_destroy(float dt);
	void send_message(const NetMessage& msg);
};

using USession = std::unique_ptr<Session>;

}
