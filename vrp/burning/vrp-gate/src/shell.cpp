//=====================================================================================//
//   Author: open
//   Date:   23.11.2018
//=====================================================================================//
#include "base/precomp.h"
#include "shell.h"
#include "updater/version.h"

Shell::Shell()
: m_net_gate_server(read_version())
, m_model(&m_net_gate_server, m_config.value("vrp-gate/osrm-server-address", "127.0.0.1:5000"), m_config.value("vrp-gate/min-num-solutions", "1").toInt())
, m_web_server(m_model, m_config.value("vrp-gate/vrp-gate-port", "19000").toInt())
{
	logs("start");
}
