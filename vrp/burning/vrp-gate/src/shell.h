//=====================================================================================//
//   Author: open
//   Date:   23.11.2018
//=====================================================================================//
#pragma once
#include "net_gate/net_gate_server.h"
#include "web/web_server.h"
#include "base/config.h"
#include "model.h"

class Shell : public QObject
{
	Q_OBJECT
private:
	Config m_config;
	NetGate::Server m_net_gate_server;
	Model m_model;
	Web::Server m_web_server;

public:
	Shell();
};
