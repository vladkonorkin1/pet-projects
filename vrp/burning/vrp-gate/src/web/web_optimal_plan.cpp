//=====================================================================================//
//   Author: open
//   Date:   23.11.2018
//=====================================================================================//
#include "base/precomp.h"
#include "web_optimal_plan.h"
//#include <QCryptographicHash>
#include <HttpServerResponse>
#include <HttpServerRequest>
#include <QAbstractSocket>
#include <QJsonObject>
#include "model.h"

WebOptimalPlan::WebOptimalPlan(Model& model)
: m_model(model)
, m_last_id(0)
, m_bad_answer(create_bad_answer())
{
	connect(&m_model, SIGNAL(claim_solved(int, const QJsonDocument&)), SLOT(slot_send_result(int, const QJsonDocument&)));
}
QJsonDocument WebOptimalPlan::create_bad_answer() const
{
	QJsonDocument doc;
	QJsonObject obj;
	obj["result"] = "error";
	doc.setObject(obj);
	return doc;
}
// обработчик сетевого запроса
bool WebOptimalPlan::handleRequest(Tufao::HttpServerRequest& request, Tufao::HttpServerResponse& response)
{
	if (request.method() == "POST")
	{
		add_claim(Claim{ ++m_last_id, request, response }, request.readBody());
		return true;
	}
	return false;
}
// отправить результат
void WebOptimalPlan::slot_send_result(int claim_id, const QJsonDocument& json_result)
{
	auto it = m_claims.find(claim_id);
	if (m_claims.end() != it)
		build_response(it->second, json_result);
}
// отправить ответ по заявке
void WebOptimalPlan::build_response(Claim& claim, const QJsonDocument& json_result) const
{
	const QJsonDocument& json_doc = json_result.isNull() ? m_bad_answer : json_result;
	claim.response.writeHead(Tufao::HttpResponseStatus::OK);
	claim.response.end(json_doc.toJson());
	claim.request.socket().close();
}
// добавить заявку
void WebOptimalPlan::add_claim(const Claim& claim, const QByteArray& buffer)
{
	auto it = m_claims.find(claim.id);
	if (m_claims.end() == it)
	{
		m_claims.insert(std::make_pair(claim.id, claim));
		connect(&claim.request.socket(), &QAbstractSocket::disconnected, [this, id = claim.id]() { slot_request_closed(id); });
		m_model.add_claim(claim.id, QJsonDocument::fromJson(buffer));
	}
	else build_response(it->second, QJsonDocument());
}
// слот закрытия запроса
void WebOptimalPlan::slot_request_closed(int claim_id)
{
	auto it = m_claims.find(claim_id);
	if (m_claims.end() != it)
	{
		m_model.remove_claim(claim_id);
		m_claims.erase(it);
	}
}