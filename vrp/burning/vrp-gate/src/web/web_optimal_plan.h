//=====================================================================================//
//   Author: open
//   Date:   23.11.2018
//=====================================================================================//
#pragma once
#include <AbstractHttpServerRequestHandler>
#include <QJsonDocument>

class Model;

class WebOptimalPlan : public QObject, public Tufao::AbstractHttpServerRequestHandler
{
	Q_OBJECT
private:
	Model& m_model;
	int m_last_id;

	struct Claim
	{
		int id;
		Tufao::HttpServerRequest& request;
		Tufao::HttpServerResponse& response;
	};
	std::map<int, Claim> m_claims;
	QJsonDocument m_bad_answer;

public:
	WebOptimalPlan(Model& m_model);

public slots:
	// обработчик сетевого запроса
	bool handleRequest(Tufao::HttpServerRequest& request, Tufao::HttpServerResponse& response) override;

private slots:
	// слот закрытия запроса
	void slot_request_closed(int claim_id);
	// отправить результат
	void slot_send_result(int claim_id, const QJsonDocument& json_result);

private:
	QJsonDocument create_bad_answer() const;
	// добавить заявку
	void add_claim(const Claim& claim, const QByteArray& buffer);
	// отправить ответ по заявке
	void build_response(Claim& claim, const QJsonDocument& json_result) const;
};