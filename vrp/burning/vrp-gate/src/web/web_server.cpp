//=====================================================================================//
//   Author: open
//   Date:   15.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "web_server.h"
#include <HttpServerResponse>
#include <HttpServerRequest>
#include <NotFoundHandler>
#include <QTcpSocket>

namespace Web
{

Server::Server(Model& model, int port)
: m_web_optimal_plan(model)
, m_web_stats(model)
, m_router {
	//{ QRegularExpression{ "^/optimal_plan(?:/(\\w*))?$" }, [handler = &m_web_optimal_plan_tickets](Tufao::HttpServerRequest &request, Tufao::HttpServerResponse &response) { return true; } },
	{ QRegularExpression{ "^/start_updater(?:/(\\w*))?$" }, m_web_start_updater },
	{ QRegularExpression{ "^/optimal_plan(?:/(\\w*))?$" }, m_web_optimal_plan },
	{ QRegularExpression{ "^/stats(?:/(\\w*))?$" }, m_web_stats },
	{ QRegularExpression{ "" }, Tufao::NotFoundHandler::handler() } }
{
	listen(QHostAddress::Any, port);
	//QObject::connect(&m_server, &Tufao::HttpServer::requestReady, &m_router, &Tufao::HttpServerRequestRouter::handleRequest);
	//m_server.setTimeout(0);
	//m_server.listen(QHostAddress::Any, 19000);
}
void Server::incomingConnection(qintptr socket_descriptor)
{
	QTcpSocket *socket = new QTcpSocket;
	if (!socket->setSocketDescriptor(socket_descriptor)) {
		delete socket;
		return;
	}
	handleConnection(socket);
}
void Server::handleConnection(QAbstractSocket* socket)
{
	socket->setParent(this);
	Tufao::HttpServerRequest* request = new Tufao::HttpServerRequest(*socket, this);

	//if (priv->timeout)
	//	handle->setTimeout(priv->timeout);

	//connect(request, &Tufao::HttpServerRequest::data, this, &Web::Server::slot_request_downloaded);
	connect(request, SIGNAL(ready()), this, SLOT(slot_request_ready()));
	
	//connect(request, &Tufao::HttpServerRequest::upgrade, this, &Web::Server::onUpgrade);
	connect(socket, &QAbstractSocket::disconnected, request, &QObject::deleteLater);
	connect(socket, &QAbstractSocket::disconnected, socket, &QObject::deleteLater);
}
void Server::slot_request_ready()
{
	Tufao::HttpServerRequest* request = qobject_cast<Tufao::HttpServerRequest*>(sender());
	Q_ASSERT(request);

	QAbstractSocket &socket = request->socket();
	Tufao::HttpServerResponse* response = new Tufao::HttpServerResponse(socket, request->responseOptions(), this);

	connect(&socket, &QAbstractSocket::disconnected, response, &QObject::deleteLater);
	connect(response, &Tufao::HttpServerResponse::finished, request, &Tufao::HttpServerRequest::resume);
	connect(response, &Tufao::HttpServerResponse::finished, response, &QObject::deleteLater);

	connect(request, &Tufao::HttpServerRequest::end, this, [this, request, response]() {slot_request_downloaded(request, response); });
}
void Server::slot_request_downloaded(Tufao::HttpServerRequest* request, Tufao::HttpServerResponse* response)
{
	m_router.handleRequest(*request, *response);
}

}