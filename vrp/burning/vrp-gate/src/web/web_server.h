//=====================================================================================//
//   Author: open
//   Date:   15.09.2018
//=====================================================================================//
#pragma once
#include <HttpServerRequestRouter>
#include <QTcpServer>
//#include <HttpServer>
#include "web/web_start_updater.h"
#include "web/web_optimal_plan.h"
#include "web/web_stats.h"

namespace Web
{

class Server : public QTcpServer
{
	Q_OBJECT
private:
	WebStartUpdater m_web_start_updater;
	WebOptimalPlan m_web_optimal_plan;
	WebStats m_web_stats;
	Tufao::HttpServerRequestRouter m_router;

public:
	Server(Model& model, int port);
	WebOptimalPlan& web_optimal_plan() { return m_web_optimal_plan; }
	WebStats& web_stats() { return m_web_stats; }

private slots:
	void slot_request_ready();
	void slot_request_downloaded(Tufao::HttpServerRequest* request, Tufao::HttpServerResponse* response);

protected:
	virtual void incomingConnection(qintptr socket_descriptor);

private:
	void handleConnection(QAbstractSocket* socket);
};

}
