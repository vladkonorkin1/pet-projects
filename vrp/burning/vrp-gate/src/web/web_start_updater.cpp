//=====================================================================================//
//   Author: open
//   Date:   09.11.2018
//=====================================================================================//
#include "base/precomp.h"
#include "web_start_updater.h"
#include "updater/start_updater.h"
#include <HttpServerResponse>
#include <HttpServerRequest>
#include <QAbstractSocket>
#include <QJsonDocument>
#include <QJsonObject>

// обработчик сетевого запроса
bool WebStartUpdater::handleRequest(Tufao::HttpServerRequest& request, Tufao::HttpServerResponse& response)
{
	if (request.method() != "GET") return false;
	
	bool started = start_updater();

	QJsonDocument doc;
	QJsonObject obj;
	obj["result"] = started;
	doc.setObject(obj);

	response.writeHead(Tufao::HttpResponseStatus::OK);
	response.end(doc.toJson());
	request.socket().close();

	/*// если запуск успешный... то закрываемся
	if (started)
	{
		m_once = true;
		QTimer::singleShot(4000, qApp, SLOT(quit()));
	}*/
	
	return true;
}