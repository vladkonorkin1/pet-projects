//=====================================================================================//
//   Author: open
//   Date:   09.11.2018
//=====================================================================================//
#pragma once
#include <AbstractHttpServerRequestHandler>

class WebStartUpdater : public QObject, public Tufao::AbstractHttpServerRequestHandler
{
	Q_OBJECT
public:
	// обработчик сетевого запроса
	virtual bool handleRequest(Tufao::HttpServerRequest& request, Tufao::HttpServerResponse& response) override;
};