//=====================================================================================//
//   Author: open
//   Date:   09.11.2018
//=====================================================================================//
#include "base/precomp.h"
#include "web_stats.h"
#include <HttpServerResponse>
#include <HttpServerRequest>
#include <QAbstractSocket>
#include "model.h"

WebStats::WebStats(Model& model)
: m_model(model)
{
}
// обработчик сетевого запроса
bool WebStats::handleRequest(Tufao::HttpServerRequest& request, Tufao::HttpServerResponse& response)
{
	if (request.method() == "GET")
	{
		QJsonDocument doc = m_model.build_stats();
		response.writeHead(Tufao::HttpResponseStatus::OK);
		response.end(doc.toJson());
		request.socket().close();
		return true;
	}
	return false;
}