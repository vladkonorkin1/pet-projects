//=====================================================================================//
//   Author: open
//   Date:   09.11.2018
//=====================================================================================//
#pragma once
#include <AbstractHttpServerRequestHandler>

class Model;

class WebStats : public QObject, public Tufao::AbstractHttpServerRequestHandler
{
	Q_OBJECT
private:
	Model& m_model;

public:
	WebStats(Model& model);
	// обработчик сетевого запроса
	virtual bool handleRequest(Tufao::HttpServerRequest& request, Tufao::HttpServerResponse& response) override;
};