//=====================================================================================//
//   Author: open
//   Date:   31.07.2018
//=====================================================================================//
#include <QCoreApplication>
#include "shell.h"

#ifdef Q_OS_WIN
#include <Windows.h>

//#include <io.h>
//#include <stdlib.h>
//#include <stdio.h>
//#include <fcntl.h>     /* for _O_TEXT and _O_BINARY */
//#include <errno.h>     /* for EINVAL */
//#include <sys\stat.h>  /* for _S_IWRITE */
//#include <share.h>     /* for _SH_DENYNO */
#endif

//void BindStdHandlesToConsole()
//{
//	//Redirect unbuffered STDIN to the console
//	HANDLE stdInHandle = GetStdHandle(STD_INPUT_HANDLE);
//	if (stdInHandle != INVALID_HANDLE_VALUE)
//	{
//		int fileDescriptor = _open_osfhandle((intptr_t)stdInHandle, _O_TEXT);
//		if (fileDescriptor != -1)
//		{
//			FILE* file = _fdopen(fileDescriptor, "r");
//			if (file != NULL)
//			{
//				*stdin = *file;
//				setvbuf(stdin, NULL, _IONBF, 0);
//			}
//		}
//	}
//
//	//Redirect unbuffered STDOUT to the console
//	HANDLE stdOutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
//	if (stdOutHandle != INVALID_HANDLE_VALUE)
//	{
//		int fileDescriptor = _open_osfhandle((intptr_t)stdOutHandle, _O_TEXT);
//		if (fileDescriptor != -1)
//		{
//			FILE* file = _fdopen(fileDescriptor, "w");
//			if (file != NULL)
//			{
//				*stdout = *file;
//				setvbuf(stdout, NULL, _IONBF, 0);
//			}
//		}
//	}
//
//	//Redirect unbuffered STDERR to the console
//	HANDLE stdErrHandle = GetStdHandle(STD_ERROR_HANDLE);
//	if (stdErrHandle != INVALID_HANDLE_VALUE)
//	{
//		int fileDescriptor = _open_osfhandle((intptr_t)stdErrHandle, _O_TEXT);
//		if (fileDescriptor != -1)
//		{
//			FILE* file = _fdopen(fileDescriptor, "w");
//			if (file != NULL)
//			{
//				*stderr = *file;
//				setvbuf(stderr, NULL, _IONBF, 0);
//			}
//		}
//	}
//
//	//Clear the error state for each of the C++ standard stream objects. We need to do this, as
//	//attempts to access the standard streams before they refer to a valid target will cause the
//	//iostream objects to enter an error state. In versions of Visual Studio after 2005, this seems
//	//to always occur during startup regardless of whether anything has been read from or written to
//	//the console or not.
//	std::wcout.clear();
//	std::cout.clear();
//	std::wcerr.clear();
//	std::cerr.clear();
//	std::wcin.clear();
//	std::cin.clear();
//}

int main(int argc, char *argv[])
{
#ifdef Q_OS_WIN
	//FreeConsole();
	//AllocConsole();
	//AttachConsole(GetCurrentProcessId());
	//freopen("CON", "w", stdout);
	//freopen("CON", "w", stderr);
	//freopen("CON", "r", stdin);
	//freopen("CONOUT$", "w", stdout);
	//freopen("CONOUT$", "w", stderr);
	//freopen("CONIN$", "r", stdin);

	//FILE *file = nullptr;
	//freopen_s(&file, "CONIN$", "r", stdin);
	//freopen_s(&file, "CONOUT$", "w", stdout);

	/*if (::AllocConsole())
	{
		int hCrt = ::_open_osfhandle((intptr_t) ::GetStdHandle(STD_OUTPUT_HANDLE), _O_TEXT);
		FILE *hf = ::_fdopen(hCrt, "w");
		*stdout = *hf;
		::setvbuf(stdout, NULL, _IONBF, 0);

		hCrt = ::_open_osfhandle((intptr_t) ::GetStdHandle(STD_ERROR_HANDLE), _O_TEXT);
		hf = ::_fdopen(hCrt, "w");
		*stderr = *hf;
		::setvbuf(stderr, NULL, _IONBF, 0);
	}*/
#endif
	Log::init();
	QCoreApplication app(argc, argv);

	try
	{
		Shell shell;
		return app.exec();
	}
	catch (const std::exception& e) {
		logs(e.what());
	}
	return -1;
}