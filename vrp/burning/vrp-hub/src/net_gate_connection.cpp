//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "net_gate_connection.h"
#include <QHostAddress>

namespace NetGate
{

Connection::Connection()
: m_timer(12.f)
, m_connection(&m_socket)
{
	connect(&m_timer, SIGNAL(updated(float)), SLOT(slot_timer_updated(float)));
	connect(&m_connection, SIGNAL(connected()), SLOT(slot_connected()));
	connect(&m_connection, SIGNAL(disconnected()), SLOT(slot_disconnect()));
	connect(&m_connection, SIGNAL(data_received(const char*, unsigned int)), SLOT(slot_data_recieved(const char*, unsigned int)));

	m_message_dispatcher.register_message<NetGate::ServerMessageCheckProtocolVersionResponse>();
	m_message_dispatcher.register_message<NetGate::ServerMessageAutorizateResponse>();
	m_message_dispatcher.register_message<NetGate::ServerMessageReportSolvedTasks>();
	m_message_dispatcher.register_message<NetGate::ServerMessageOpenTasks>();
	m_message_dispatcher.register_message<NetGate::ServerMessageCloseTask>();
}
void Connection::set_message_listener(NetGate::ClientHandlerMessages* handler_messages)
{
	m_message_dispatcher.set_observer(handler_messages);
}
void Connection::connect_host(const QString& host_address)
{
	m_connecting_timer.reset(new QTimer());
	connect(m_connecting_timer.get(), SIGNAL(timeout()), SLOT(on_connecting_timeout()));
	m_connecting_timer->start(4000);
	m_socket.connectToHost(QHostAddress(host_address), NetGate::ServerPort);
}
void Connection::slot_connected()
{
	m_connecting_timer.reset();
	emit connected();
}
void Connection::send_message(const NetMessage& message)
{
	m_connection.send_message(message);
}
void Connection::slot_disconnect()
{
	m_connecting_timer.reset();
	emit disconnected();
}
void Connection::slot_data_recieved(const char* data, unsigned int size)
{
	m_message_dispatcher.process(data, size);
}
void Connection::close()
{
	m_connection.close();
}
void Connection::slot_timer_updated(float dt)
{
	if (m_socket.isValid())
		send_message(ClientMessageAlive());
}
void Connection::on_connecting_timeout()
{
	// ����� ����������� �����
	close();
}

}