//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#pragma once
#include "net_gate/net_gate_handler_messages.h"
#include "net/net_message_dispatcher.h"
#include "net/net_base_connection.h"
#include "base/timer.h"
#include <QTcpSocket>

namespace NetGate
{

class Connection : public QObject
{
	Q_OBJECT
private:
	Base::Timer m_timer;
	std::unique_ptr<QTimer> m_connecting_timer;
	QTcpSocket m_socket;
	NetBaseConnection m_connection;
	NetMessageDispatcher<NetGate::ClientHandlerMessages> m_message_dispatcher;
	
public:
	Connection();
	void connect_host(const QString& host_address);
	void send_message(const NetMessage& message);
	void set_message_listener(NetGate::ClientHandlerMessages* handler_messages);
	void close();

signals:
	void connected();
	void disconnected();

private slots:
	void slot_connected();
	void slot_disconnect();
	void slot_data_recieved(const char* data, unsigned int size);
	void slot_timer_updated(float dt);
	void on_connecting_timeout();
};

using UConnection = std::unique_ptr<Connection>;

}