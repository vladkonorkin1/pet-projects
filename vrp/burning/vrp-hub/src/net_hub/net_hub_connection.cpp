﻿//=====================================================================================//
//   Author: open
//   Date:   19.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "net_hub_connection.h"

namespace NetHub
{

Connection::Connection(QLocalSocket* socket, ConnectionId id)
: m_id(id)
, m_socket(socket)
, m_connection(socket)
{
	connect(&m_connection, SIGNAL(disconnected()), SLOT(slot_disconnect()));
	connect(&m_connection, SIGNAL(data_received(const char*, unsigned int)), SLOT(slot_data_recieved(const char*, unsigned int)));
	m_message_dispatcher.set_observer(this);
	m_message_dispatcher.register_message<NetHub::ClientMessageAuthorize>();
	m_message_dispatcher.register_message<NetHub::ClientMessageTicketSolved>();
}
void Connection::slot_disconnect()
{
	m_socket->deleteLater();
	emit disconnected(this);
}
void Connection::send_message(const NetMessage& message)
{
	m_connection.send_message(message);
}
void Connection::slot_data_recieved(const char* data, unsigned int size)
{
	m_message_dispatcher.process(data, size);
}
void Connection::close()
{
	m_connection.close();
}
ConnectionId Connection::id() const
{
	return m_id;
}
void Connection::on_message(const NetHub::ClientMessageTicketSolved& msg)
{
	emit ticket_solved(msg.json_result);
}
void Connection::on_message(const NetHub::ClientMessageAuthorize& msg)
{
	emit authorized(msg.task_uuid);
}
QString Connection::address() const
{
	return m_socket->serverName();
}

}
