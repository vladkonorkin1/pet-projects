﻿//=====================================================================================//
//   Author: open
//   Date:   19.09.2018
//=====================================================================================//
#pragma once
#include "net_hub/net_hub_handler_messages.h"
#include "net/net_base_connection.h"
#include "net/net_message_dispatcher.h"

namespace NetHub
{

class Connection : public QObject, public NetHub::ServerHandlerMessages
{
	Q_OBJECT
private:
	ConnectionId m_id;
	QLocalSocket* m_socket;
	NetBaseConnection m_connection;
	NetMessageDispatcher<NetHub::ServerHandlerMessages> m_message_dispatcher;

public:
	Connection(QLocalSocket* socket, ConnectionId id);
	ConnectionId id() const;
	void send_message(const NetMessage& message);
	void close();
	QString address() const;

public:
	virtual void on_message(const NetHub::ClientMessageTicketSolved& msg);
	virtual void on_message(const NetHub::ClientMessageAuthorize& msg);

signals:
	void disconnected(NetHub::Connection* connection);
	// авторизован
	void authorized(const QUuid& task_uuid);
	// тикет решён
	void ticket_solved(const QJsonDocument& json_result);

private slots:
	void slot_disconnect();
	void slot_data_recieved(const char* data, unsigned int size);
};

using UConnection = std::unique_ptr<Connection>;

}
