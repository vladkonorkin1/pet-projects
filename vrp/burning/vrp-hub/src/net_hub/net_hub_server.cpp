//=====================================================================================//
//   Author: open
//   Date:   19.09.2017
//=====================================================================================//
#include "base/precomp.h"
#include "net_hub_server.h"

namespace NetHub
{

Server::Server()
{
	connect(&m_server, SIGNAL(newConnection()), this, SLOT(slot_accept_connection()));
	m_server.listen(QUuid::createUuid().toString());
}
void Server::slot_accept_connection()
{
	UConnection uconnection(new Connection(m_server.nextPendingConnection(), ++m_generate_connection_id));
	Connection* connection = uconnection.get();
	connect(connection, SIGNAL(disconnected(NetHub::Connection*)), this, SLOT(slot_connection_disconnected(NetHub::Connection*)));
	m_connections[connection->id()] = std::move(uconnection);

	//logs(QString("net hub: create connection: %1 %2").arg(connection->id()).arg(connection->address()));
	emit connection_created(connection);
}
void Server::slot_connection_disconnected(NetHub::Connection* connection)
{
	//logs(QString("net hub: destroy connection: %1 %2").arg(connection->id()).arg(connection->address()));

	connections_t::iterator it_connection = m_connections.find(connection->id());
	if (it_connection != m_connections.end())
	{
		emit connection_destroyed(connection);
		m_connections.erase(it_connection);
	}
}
QString Server::server_name() const
{
	return m_server.serverName();
}

}
