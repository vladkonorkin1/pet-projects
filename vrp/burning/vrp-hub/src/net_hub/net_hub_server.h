//=====================================================================================//
//   Author: open
//   Date:   12.09.2017
//=====================================================================================//
#pragma once
#include "net_hub/net_hub_connection.h"
#include <QLocalServer>

namespace NetHub
{

class Server : public QObject
{
	Q_OBJECT
private:
	QLocalServer m_server;
	ConnectionId m_generate_connection_id = 0;

	using connections_t = std::map<ConnectionId, UConnection>;
	connections_t m_connections;

public:
	Server();
	QString server_name() const;

signals:
	void connection_created(NetHub::Connection* connection);
	void connection_destroyed(NetHub::Connection* connection);

private slots:
	void slot_accept_connection();
	void slot_connection_disconnected(NetHub::Connection* connection);
};

}
