//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "shell.h"
#include "base/user_name.h"
#include <QCoreApplication>
#include <QHostInfo>
#include <QThread>
#include "updater/start_updater.h"
#include "updater/version.h"

Shell::Shell()
: m_ticket_manager(m_config.value("vrp-hub/vrp-solver-path", "vrp-solver"))
, m_session_id(QUuid::createUuid())
, m_protocol_version(read_version())
, m_next_host_index(0)
{
	connect(&m_ticket_manager, SIGNAL(task_solved(int, int, const QJsonDocument&)), SLOT(slot_task_solved(int, int, const QJsonDocument&)));

	for (int i = 0; i < 10; ++i)
	{
		QString net_gate_address = m_config.value(QString("vrp-hub/vrp-gate-address%1").arg(i));
		if (!net_gate_address.isEmpty()) m_net_gate_hosts.push_back(net_gate_address);
	}
	if (m_net_gate_hosts.empty()) m_net_gate_hosts.push_back("127.0.0.1");

	reconnect();
}
void Shell::reconnect()
{
	//const QString& host = m_net_gate_hosts[std::rand() % m_net_gate_hosts.size()];
	const QString& host = m_net_gate_hosts[m_next_host_index++ % m_net_gate_hosts.size()];
	logs(QString("net gate: connecting to host %1...").arg(host));
	m_net_gate_connection.reset(new NetGate::Connection());
	m_net_gate_connection->set_message_listener(this);
	connect(m_net_gate_connection.get(), SIGNAL(disconnected()), SLOT(slot_net_gate_connection_disconnected()));
	connect(m_net_gate_connection.get(), SIGNAL(connected()), SLOT(slot_net_gate_connection_connected()));
	m_net_gate_connection->connect_host(host);
}
void Shell::slot_task_solved(int claim_id, int task_id, const QJsonDocument& json_result)
{
	send_message(NetGate::ClientMessageTaskSolved(claim_id, task_id, json_result));
}
void Shell::slot_net_gate_connection_disconnected()
{
	logs("net gate: disconnected");
	// удаляем соединение "вручную" с задержкой
	NetGate::Connection* connection = m_net_gate_connection.release();
	connection->deleteLater();

	// переподключаемся
	QTimer::singleShot(3000, this, SLOT(reconnect()));

	// очищаем все рабочие данные
	//clear_data();
}
void Shell::send_message(const NetMessage& msg)
{
	if (m_net_gate_connection)
		m_net_gate_connection->send_message(msg);
}
void Shell::slot_net_gate_connection_connected()
{
	logs("net gate: connected");
	send_message(NetGate::ClientMessageCheckProtocolVersion(m_protocol_version));
}
void Shell::on_message(const NetGate::ServerMessageCheckProtocolVersionResponse& msg)
{
	if (msg.success) send_message(NetGate::ClientMessageAutorizate(m_session_id, QHostInfo::localHostName(), user_name(), get_number_threads()));
	else
	{
		logs(QString("net gate: wrong protocol version %1").arg(m_protocol_version));
		// стартуем обновление
		start_updater();
		qApp->exit(-1);
	}
}
// сообщение о результате авторизации
void Shell::on_message(const NetGate::ServerMessageAutorizateResponse& msg)
{
	Q_UNUSED(msg);
	// если обнаружена новая сессия, то очищаем все данные
	//if (msg.new_session)
	//	clear_data();
}
// сообщение от сервера: передай результаты выполненных задач
void Shell::on_message(const NetGate::ServerMessageReportSolvedTasks& msg)
{
	Q_UNUSED(msg);
	m_ticket_manager.report_all_solved_tickets();
}
// сообщение от сервера об открытии тикета (после реконнекта придёт повторно!!!)
void Shell::on_message(const NetGate::ServerMessageOpenTasks& msg)
{
	QString str_tasks;
	for (int i = 0; i < msg.task_ids.size(); ++i)
	{
		str_tasks += QString::number(msg.task_ids[i]);
		if ((i + 1) != msg.task_ids.size())
			str_tasks += ", ";
	}
	logs(QString("net gate: open claim %1 tasks %2").arg(msg.claim_id).arg(str_tasks));
	m_ticket_manager.open_tasks(msg.claim_id, msg.task_ids, msg.json_doc, msg.matrix_distances);
}
// сообщение от сервера о закрытии тикетов
void Shell::on_message(const NetGate::ServerMessageCloseTask& msg)
{
	logs(QString("net gate: close claim %1 task %2").arg(msg.claim_id).arg(msg.task_id));
	m_ticket_manager.close_task(msg.claim_id, msg.task_id);
}
// получить кол-во тредов доступных для решателей
int Shell::get_number_threads() const
{
	int num_from_config = m_config.value("vrp-hub/number-threads").toInt();
	if (num_from_config != 0) return num_from_config;
	return QThread::idealThreadCount();
}
