//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#pragma once
#include "net_gate_connection.h"
#include "net_gate/net_gate_handler_messages.h"
#include "ticket_manager.h"
#include "base/config.h"

class Shell : public QObject, public NetGate::ClientHandlerMessages
{
	Q_OBJECT
private:
	Config m_config;
	TicketManager m_ticket_manager;
	QUuid m_session_id;
	std::vector<QString> m_net_gate_hosts;
	NetGate::UConnection m_net_gate_connection;
	int m_protocol_version;
	int m_next_host_index;

public:
	explicit Shell();

protected:
	virtual void on_message(const NetGate::ServerMessageCheckProtocolVersionResponse& msg);
	virtual void on_message(const NetGate::ServerMessageAutorizateResponse& msg);
	virtual void on_message(const NetGate::ServerMessageReportSolvedTasks& msg);
	virtual void on_message(const NetGate::ServerMessageOpenTasks& msg);
	virtual void on_message(const NetGate::ServerMessageCloseTask& msg);

private slots:
	void slot_net_gate_connection_disconnected();
	void slot_net_gate_connection_connected();
	void reconnect();
	void slot_task_solved(int claim_id, int task_id, const QJsonDocument& json_result);

private:
	void send_message(const NetMessage& msg);
	// получить кол-во тредов доступных для решателей
	int get_number_threads() const;
};
