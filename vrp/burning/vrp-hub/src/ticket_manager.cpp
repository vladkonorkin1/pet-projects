#include "base/precomp.h"
#include "ticket_manager.h"
#include <QJsonObject>
#include <QFileInfo>

TicketManager::TicketManager(const QString& solver_path)
: m_solver_path(solver_path)
{
	connect(&m_server, SIGNAL(connection_created(NetHub::Connection*)), SLOT(slot_connection_connected(NetHub::Connection*)));
}
void TicketManager::slot_connection_connected(NetHub::Connection* connection)
{
	connect(connection, &NetHub::Connection::authorized, [this, connection](const QUuid& task_uuid) { on_connection_authorized(connection, task_uuid); });
}
void TicketManager::on_connection_authorized(NetHub::Connection* connection, const QUuid& task_uuid)
{
	connect(connection, &NetHub::Connection::ticket_solved, [this, task_uuid](const QJsonDocument& json_result) { on_ticket_solved(task_uuid, json_result); });
	auto it = m_tasks.find(task_uuid);
	if (m_tasks.end() != it)
	{
		Task* task = it->second;
		assert( !task->connection );
		task->connection = connection;
		connection->send_message(NetHub::ServerMessageOpenTicket(task->json_task, task->matrix_distances));
	}
}
bool TicketManager::open_tasks(int claim_id, const std::vector<int>& task_ids, const QJsonDocument& json_doc, const SMatrixDistances& matrix_distances)
{
	if (m_claims.find(claim_id) != m_claims.end()) return false;
	auto it = m_claims.insert(std::make_pair(claim_id, UClaim(new Claim(claim_id)))).first;
	create_claim_tasks(it->second.get(), task_ids, json_doc, matrix_distances);
	return true;
}
// создать задачи для заявки
void TicketManager::create_claim_tasks(Claim* claim, const std::vector<int>& task_ids, const QJsonDocument& json_doc, const SMatrixDistances& matrix_distances)
{
	for (int task_id : task_ids)
	{
		UTask task(new Task(task_id, QUuid::createUuid(), *claim, build_json_task(json_doc, task_id), matrix_distances));
		m_tasks[task->uuid] = task.get();
		claim->tasks[task_id] = std::move(task);
	}
	for (auto& pair : claim->tasks)
		start_process(pair.second.get());
}
// собрать документ для задачи
QJsonDocument TicketManager::build_json_task(const QJsonDocument& json_doc_claim, int task_id) const
{
	QJsonObject json_doc_obj = json_doc_claim.object();
	QJsonObject json_settings = json_doc_obj["settings"].toObject();
	json_settings["random_seed"] = task_id;
	json_doc_obj["settings"] = json_settings;

	QJsonDocument json_doc;
	json_doc.setObject(json_doc_obj);
	return json_doc;
}
void TicketManager::close_task(int claim_id, int task_id)
{
	auto it_claim = m_claims.find(claim_id);
	if (m_claims.end() != it_claim)
	{
		Claim* claim = it_claim->second.get();
		auto it_task = claim->tasks.find(task_id);
		if (claim->tasks.end() != it_task)
		{
			Task* task = it_task->second.get();
			// закрываем запущенный процесс
			finish_task(task, QJsonDocument(), false);

			// удаляем из списка по уидам
			m_tasks.erase(task->uuid);
			// удаляем 
			claim->tasks.erase(it_task);
		}
		if (claim->tasks.empty())
			m_claims.erase(it_claim);
	}
}
// отчёт о всех решённых задачах
void TicketManager::report_all_solved_tickets()
{
	for (const auto& pair : m_tasks)
	{
		Task* task = pair.second;
		if (task->finished)
			emit task_solved(task->claim.id, task->id, task->json_result);
	}
}
// сообщение от solver-a о решении задачи
void TicketManager::on_ticket_solved(const QUuid& task_uuid, const QJsonDocument& json_result)
{
	finish_task(task_uuid, json_result);
}
void TicketManager::slot_process_finished(const QUuid& task_uuid, int result)
{
	finish_task(task_uuid, QJsonDocument());
}
void TicketManager::finish_task(const QUuid& task_uuid, const QJsonDocument& json_result)
{
	auto it = m_tasks.find(task_uuid);
	if (m_tasks.end() != it)
		finish_task(it->second, json_result, true);
}
void TicketManager::finish_task(Task* task, const QJsonDocument& json_result, bool is_emit)
{
	if (!task->finished)
	{
		task->finished = true;
		task->json_result = json_result;
		if (task->process)
			task->process->close();
		
		if (is_emit) emit task_solved(task->claim.id, task->id, task->json_result);
	}
}
void TicketManager::start_process(Task* task)
{
	SProcess process(new QProcess(), [this](QProcess* process) { process->deleteLater(); });
	QStringList arguments{ m_server.server_name(), task->uuid.toString() };
	QString working_dir = QFileInfo(m_solver_path).path();
	process->setWorkingDirectory(working_dir);
	process->start(m_solver_path, arguments);
	connect(process.get(), static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), [this, task_uuid = task->uuid](int code, QProcess::ExitStatus status) { slot_process_finished(task_uuid, code); });
	
	if (process->waitForStarted())
		task->process = process;
	else
		finish_task(task->uuid, QJsonDocument());
}