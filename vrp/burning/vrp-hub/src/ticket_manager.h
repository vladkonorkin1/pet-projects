#pragma once
#include "net_hub/net_hub_server.h"
#include "base/matrix_distances.h"
#include <QJsonDocument>
#include <QProcess>

using SProcess = std::shared_ptr<QProcess>;

struct Claim;

struct Task
{
	int id;
	QUuid uuid;
	const Claim& claim;
	const QJsonDocument json_task;
	SMatrixDistances matrix_distances;
	QJsonDocument json_result;
	bool finished;

	SProcess process;
	NetHub::Connection* connection;

	Task(int id, const QUuid& uuid, const Claim& claim, const QJsonDocument& json_task, const SMatrixDistances& matrix_distances)
	: id(id)
	, uuid(uuid)
	, claim(claim)
	, json_task(json_task)
	, matrix_distances(matrix_distances)
	, finished(false)
	, connection(nullptr)
	{
	}
};

using UTask = std::unique_ptr<Task>;

struct Claim
{
	int id;
	std::map<int, UTask> tasks;

	Claim(int id) : id(id) {}
};

using UClaim = std::unique_ptr<Claim>;


class TicketManager : public QObject
{
	Q_OBJECT
private:
	NetHub::Server m_server;
	const QString m_solver_path;
	std::map<int, UClaim> m_claims;
	std::map<QUuid, Task*> m_tasks;

public:
	TicketManager(const QString& solver_path);
	bool open_tasks(int claim_id, const std::vector<int>& task_ids, const QJsonDocument& json_doc, const SMatrixDistances& matrix_distances);
	void close_task(int claim_id, int task_id);
	void report_all_solved_tickets();

signals:
	void task_solved(int claim_id, int task_id, const QJsonDocument& json_result);

private slots:
	void slot_connection_connected(NetHub::Connection* connection);
	void slot_process_finished(const QUuid& task_uuid, int result);

private:
	void on_connection_authorized(NetHub::Connection* connection, const QUuid& task_uuid);
	void on_ticket_solved(const QUuid& task_uuid, const QJsonDocument& json_result);
	//void on_process_error(const QUuid& task_uuid);

private:
	// создать задачи для заявки
	void create_claim_tasks(Claim* claim, const std::vector<int>& task_ids, const QJsonDocument& json_doc, const SMatrixDistances& matrix_distances);
	// собрать документ для задачи
	QJsonDocument build_json_task(const QJsonDocument& json_doc_claim, int task_id) const;
	void finish_task(const QUuid& task_uuid, const QJsonDocument& json_result);
	void finish_task(Task* task, const QJsonDocument& json_result, bool is_emit);
	void start_process(Task* task);
};