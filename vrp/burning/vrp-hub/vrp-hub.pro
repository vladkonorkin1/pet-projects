TARGET = vrp-hub
CONFIG -= flat
TEMPLATE = app

win32 {
    release: DESTDIR = tmp/release
    debug:   DESTDIR = tmp/debug
    OBJECTS_DIR = $$DESTDIR/.obj
    MOC_DIR = $$DESTDIR/.moc
    RCC_DIR = $$DESTDIR/.qrc
    UI_DIR = $$DESTDIR/.ui
}
unix {
    DESTDIR = .
    OBJECTS_DIR = $$DESTDIR/tmp/.obj
    MOC_DIR = $$DESTDIR/tmp/.moc
    RCC_DIR = $$DESTDIR/tmp/.qrc
    UI_DIR = $$DESTDIR/tmp/.ui
}

QT -= gui
QT += network
CONFIG += console

CONFIG += precompile_header
PRECOMPILED_HEADER = ../shared/base/precomp.h

win32 {
	Release:QMAKE_POST_LINK = copy tmp\release\*.exe bin\release
	QMAKE_CXXFLAGS += /MP

	INCLUDEPATH += src
	INCLUDEPATH += ../shared
}
unix {
	INCLUDEPATH += src
	INCLUDEPATH += ../shared
}

HEADERS =  	../shared/base/precomp.h 								\
			../shared/base/timer.h 									\
			../shared/base/log.h 									\
			../shared/base/thread_task.h							\
			../shared/base/thread_task_manager.h					\
			../shared/base/bin_stream.h								\
			../shared/base/config.h									\
			../shared/base/matrix_distances.h						\
			../shared/base/user_name.h								\
			../shared/net/net_base_connection.h						\
			../shared/net/net_packet.h								\
			../shared/net/net_packet_reader.h						\
			../shared/net/net_message_dispatcher.h					\
			../shared/net/net_message.h								\
			../shared/net_gate/net_gate_handler_messages.h			\
			../shared/net_gate/net_gate_messages.h					\
			../shared/net_gate/net_gate_types.h						\
			../shared/net_hub/net_hub_messages.h					\
			../shared/updater/start_updater.h						\
			../shared/updater/version.h								\
			src/ticket_manager.h									\
			src/net_gate_connection.h								\
			src/shell.h												\
            src/net_hub/net_hub_server.h							\
            src/net_hub/net_hub_connection.h						\

SOURCES = 	../shared/base/precomp.cpp 								\
			../shared/base/timer.cpp 								\
			../shared/base/log.cpp 									\
			../shared/base/thread_task_manager.cpp					\
			../shared/base/config.cpp								\
			../shared/base/matrix_distances.cpp						\
			../shared/base/user_name.cpp							\
			../shared/net/net_base_connection.cpp					\
			../shared/net/net_packet.cpp							\
			../shared/net/net_packet_reader.cpp						\
			../shared/net_gate/net_gate_messages.cpp				\
			../shared/net_hub/net_hub_messages.cpp					\
			../shared/updater/start_updater.cpp						\
			../shared/updater/version.cpp							\
			src/ticket_manager.cpp									\
			src/net_gate_connection.cpp								\
			src/shell.cpp											\
			src/main.cpp											\
            src/net_hub/net_hub_server.cpp							\
            src/net_hub/net_hub_connection.cpp						\