//=====================================================================================//
//   Author: open
//   Date:   01.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "main_window.h"
#include "map_viewer/map_viewer.h"
#include <QPushButton>
#include <QSettings>

MainWindow::MainWindow(MapViewer* map_viewer)
: m_ui(create_ui(this))
{
	set_central_widget(new MapControlWidget(map_viewer));
	connect(m_ui->button_show_routes, SIGNAL(toggled(bool)), SLOT(slot_show_routes(bool)));
	load_layout();
}
std::auto_ptr<Ui::MainWindow> MainWindow::create_ui(MainWindow* main_window) const
{
	std::auto_ptr<Ui::MainWindow> ui(new Ui::MainWindow());
	ui->setupUi(main_window);
	return ui;
}
void MainWindow::slot_show_routes(bool checked)
{
	for (QPushButton* button : m_route_buttons)
		button->setChecked(checked);
}
void MainWindow::set_central_widget(QWidget* widget)
{
	widget->setParent(m_ui->centralwidget);
	m_ui->verticalLayout->addWidget(widget);
}
void MainWindow::add_msg(const QString& msg)
{
	m_ui->text_edit->appendPlainText(msg);
}
void MainWindow::clear_widgets()
{
	m_ui->text_edit->clear();
	for (QPushButton* button : m_route_buttons)
	{
		m_ui->verticalLayout_2->removeWidget(button);
		delete button;
	}
	m_route_buttons.clear();
}
QPushButton* MainWindow::add_route_button(const QString& text)
{
	QPushButton* button = new QPushButton(m_ui->buttons_widget);
	button->setText(text);
	m_ui->verticalLayout_2->insertWidget(m_ui->verticalLayout_2->count() - 1, button, 0, 0);
	m_route_buttons.push_back(button);
	return button;
}
void MainWindow::closeEvent(QCloseEvent* event)
{
	save_layout();
}
void MainWindow::save_layout()
{
	QSettings settings(QSettings::UserScope, "sima-land", QApplication::applicationName());
	//settings.setValue("splitter_horizontal", m_ui->splitter_horizontal->saveState());
	//settings.setValue("splitter_vertical", m_ui->splitter_vertical->saveState());
	//settings.setValue("claim_results_table_widget", m_ui->claim_results_table_widget->horizontalHeader()->saveState());
	settings.setValue("state", saveState());
	settings.setValue("geometry", saveGeometry());
	//settings.setValue("splitter_state", m_ui->splitter_messages->saveState());
}
void MainWindow::load_layout()
{
	QSettings settings(QSettings::UserScope, "sima-land", QApplication::applicationName());
	//m_ui->splitter_horizontal->restoreState(settings.value("splitter_horizontal").toByteArray());
	//m_ui->splitter_vertical->restoreState(settings.value("splitter_vertical").toByteArray());
	//m_ui->claim_results_table_widget->horizontalHeader()->restoreState(settings.value("claim_results_table_widget").toByteArray());
	restoreState(settings.value("state").toByteArray());
	restoreGeometry(settings.value("geometry").toByteArray());
	//m_ui->splitter_messages->restoreState(settings.value("splitter_state").toByteArray());
}


MapControlWidget::MapControlWidget(MapViewer* map_viewer)
: m_map_viewer(map_viewer)
{
	m_map_viewer->map_control()->setParent(this);
}
void MapControlWidget::resizeEvent(QResizeEvent* event)
{
	m_map_viewer->resize(event->size());
}
