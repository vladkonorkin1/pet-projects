//=====================================================================================//
//   Author: open
//   Date:   31.07.2018
//=====================================================================================//
#include "base/precomp.h"
#include "shell.h"
#include "burning_algorithm/burning_json.h"

Shell::Shell()
: m_main_window(new MainWindow(&m_map_viewer))
, m_osrm_manager(m_network_manager, "10.30.37.92:5000")
//, m_osrm_manager(m_network_manager, "88.86.193.234:5000")
, m_timer(0.5f)
{
	connect(&m_timer, SIGNAL(updated(float)), SLOT(slot_timer_updated(float)));
	connect(&m_osrm_manager, SIGNAL(request_routes_finished(bool, const routes_t&)), SLOT(slot_request_routes_finished(bool, const routes_t&)));
	
	if (Burning::json_read_data("data.json", m_data))
	{
		vpositions_t points;
		points.reserve(m_data.destination_points.size());
		for (const Burning::DestinationPoint& destination_point : m_data.destination_points)
			points.push_back(QPointF(destination_point.lon, destination_point.lat));

		m_request_matrix_distances = m_osrm_manager.create_request_matrix_distances(points);
		connect(m_request_matrix_distances.get(), SIGNAL(finished(bool, const SMatrixDistances&, const vpositions_t&, const matrix_distance_indices_t&)), SLOT(slot_request_matrix_distances_finished(bool, const SMatrixDistances&)));
		m_osrm_manager.request_matrix_distances(m_request_matrix_distances, m_data.use_osrm_cache);
	}
}
void Shell::activate()
{
	m_main_window->show();
}
void Shell::slot_timer_updated(float dt)
{
	m_map_viewer.update(dt);
}
void Shell::slot_request_matrix_distances_finished(bool success, const SMatrixDistances& matrix_distances)
{
	if (success) build_best_plan(matrix_distances);
	else logs("error request distances");
	m_request_matrix_distances.reset();
}
void Shell::build_best_plan(const SMatrixDistances& matrix_distances)
{
	m_data.matrix_distances = matrix_distances;

	m_solver.reset(new Burning::Solver());
	connect(m_solver.get(), SIGNAL(cycle_calculated(const Burning::SPlan&)), SLOT(slot_cycle_calculated(const Burning::SPlan&)));// , Qt::ConnectionType::QueuedConnection);
	connect(m_solver.get(), SIGNAL(finished(const Burning::SPlan&)), SLOT(slot_solver_finish(const Burning::SPlan&)));
	m_solver->calculate(m_data, true);
}
void Shell::slot_cycle_calculated(const Burning::SPlan& plan)
{
	routes_t routes = build_routes(plan);
	show_results(plan, routes, false);
}
void Shell::slot_solver_finish(const Burning::SPlan& best_plan)
{
	m_plan = best_plan;
	routes_t routes = build_routes(m_plan);
	m_osrm_manager.request_routes(routes);
}
routes_t Shell::build_routes(const Burning::SPlan& plan) const
{
	routes_t routes;
	for (const Burning::Route* annealing_route : plan->routes())
	{
		SRoute route(new Route());
		routes.push_back(route);
		for (int i = 0; i < annealing_route->points.size() - 1; ++i)
		{
			const Burning::DestinationPoint* destintaion_point = annealing_route->points[i].destination_point;
			route->points.push_back(QPointF(destintaion_point->lon, destintaion_point->lat));
		}	
	}
	return routes;
}
void Shell::slot_request_routes_finished(bool success, const routes_t& routes)
{
	if (success) show_results(m_plan, routes, true);
	else logs("error request routes");
}
struct RenderColor
{
	QString name;
	QColor color;
};
// https://www.jcsforyou.com/color-for-you/
static std::array<RenderColor, 21> s_colors = {
	RenderColor{ "Black",	QColor(  0,   0,   0) },
	RenderColor{ "Red",		QColor(229,  20,   0) },
	RenderColor{ "Blue",	QColor(  0,  80, 239) },
	RenderColor{ "Emerald",	QColor(0,   138,   0) },
	RenderColor{ "Orange",	QColor(250, 104,   0) },
	RenderColor{ "Amber",	QColor(240, 163,  10) },
	RenderColor{ "Brown",	QColor(175, 121,  59) },
	RenderColor{ "Green",	QColor(96, 169,  23) },
	RenderColor{ "Crimson", QColor(162,   0,  37) },
	RenderColor{ "Cyan",	QColor( 27, 161, 226) },
	RenderColor{ "Magenta",	QColor(216,   0, 115) },
	RenderColor{ "Lime",	QColor(164, 196,   0) },
	RenderColor{ "Indigo",	QColor( 63,   0, 158) },
	RenderColor{ "Mauve",	QColor(118,  96, 138) },
	RenderColor{ "Olive",	QColor(109, 135, 100) },
	RenderColor{ "Pink",	QColor(244, 114, 208) },
	RenderColor{ "Sienna",	QColor(122,  59,  63) },
	RenderColor{ "Steel",	QColor(119, 144, 168) },
	RenderColor{ "Teal",	QColor(  0, 181, 178) },
	RenderColor{ "Violet",	QColor(170,   0, 255) },
	RenderColor{ "Yellow",	QColor(216, 193,   0) }
};
void Shell::show_results(const Burning::SPlan& plan, const routes_t& data_routes, bool show_points)
{
	if (show_points)
	{
		m_main_window->clear_widgets();

		m_main_window->add_msg(QString("dest points: %1").arg(m_data.destination_points.size() - 1));
		//m_main_window->add_msg(QString("count points: %1").arg(plan->count_points()));
		m_main_window->add_msg(QString().sprintf("plan distance: %.2f", plan->calc_distance(*m_data.matrix_distances)));
		m_main_window->add_msg(QString().sprintf("plan price total: %.2f", plan->price_total()));
		m_main_window->add_msg(QString().sprintf("plan price clean: %.2f", plan->price_clean()));
		m_main_window->add_msg("");


		for (size_t i = 0; i < plan->routes().size(); ++i)
		{
			const Burning::Route* route = plan->routes()[i];
			m_main_window->add_msg(QString("route: %1 %2").arg(s_colors[i % s_colors.size()].name).arg(route->points.size() - 2));
			m_main_window->add_msg(QString().sprintf("  volume: %.2f / %.2f", route->volume, route->avto->max_volume));
			m_main_window->add_msg(QString().sprintf("  weight: %.2f / %.2f", route->weight, route->avto->weight));
			m_main_window->add_msg(QString().sprintf("  distance: %.2f", route->calc_distance(*m_data.matrix_distances)));

			QTime time_idle_first_point = QTime::fromMSecsSinceStartOfDay(route->idle_time_first_point * 1000);
			m_main_window->add_msg(QString("  idle from first point: %1").arg(time_idle_first_point.toString("hh:mm")));

			QTime time_idle = QTime::fromMSecsSinceStartOfDay(route->idle_time * 1000);
			m_main_window->add_msg(QString("  idle: %1").arg(time_idle.toString("hh:mm")));

			m_main_window->add_msg(QString().sprintf("  price total: %.2f", route->price_total));
			m_main_window->add_msg(QString().sprintf("  price clean: %.2f", route->price_clean));


			for (int i = 0; i < route->points.size(); ++i)
			{
				const Burning::RoutePoint& point = route->points[i];
				int day_arrival = point.day_arrival;
				int time_arrival = point.time_arrival;
				if (time_arrival < 0)
				{
					time_arrival = (24 * 60 * 60) + time_arrival;
					day_arrival--;
				}

				QTime qtime_arrival = QTime::fromMSecsSinceStartOfDay(time_arrival * 1000);

				QTime point_time_from = QTime::fromMSecsSinceStartOfDay(point.destination_point->time_from * 1000);
				QTime point_time_to = QTime::fromMSecsSinceStartOfDay(point.destination_point->time_to * 1000);
				QTime point_idle = QTime::fromMSecsSinceStartOfDay(point.idle_time * 1000);

				QString city_name = point.destination_point->city->name;
				city_name = city_name.left(city_name.lastIndexOf(' '));
				city_name += QString().fill(' ', std::max(14 - city_name.length(), 0));
				m_main_window->add_msg(QString("   %1: %2 %3-%4 [%5-%6] %7").arg(i, 2).arg(city_name).arg(day_arrival).arg(qtime_arrival.toString("hh:mm")).arg(point_time_from.toString("hh:mm")).arg(point_time_to.toString("hh:mm")).arg(point_idle.toString("hh:mm")));
			}
			m_main_window->add_msg("");
		}
	}

	m_render_routes.clear();
	for (size_t i = 0; i < data_routes.size(); ++i)
	{	
		SRenderRoute render_route(new RenderRoute());
		m_render_routes.push_back(render_route);
		const SRoute& data_route = data_routes[i];
		const Burning::Route* annealing_route = plan->routes()[i];
		// рисуем все сегменты путей
		for (const Waypoint& waypoint : data_route->waypoints)
		{
			SMapWaypoint map_waypoint = m_map_viewer.add_waypoint(waypoint.positions, s_colors[i % s_colors.size()].color, Qt::PenStyle::SolidLine, 3.f);// std::max(3.f, (9.5f - i*1.25f)));
			render_route->add_waypoint(map_waypoint);
		}
		// рисуем точки
		if (show_points)
		{ 
			for (size_t j = 1; j < data_route->points.size(); ++j)
			{
				const QPointF& point = data_route->points[j];
				if (j == 1)
				{
					SMapPoint map_little_point = m_map_viewer.add_point(point, s_colors[i % s_colors.size()].color, 6);
					render_route->add_point(map_little_point);

					SMapPoint map_black_point = m_map_viewer.add_point(point, QColor(0, 0, 0, 255), 10);
					render_route->add_point(map_black_point);

					map_black_point->set_name(QString::number(j));
					connect(&(map_black_point->point()), &qmapcontrol::CirclePoint::geometryClicked, [this](qmapcontrol::Geometry* geometry, QPoint point) { logs(geometry->name()); });
				}
				else
				{
					SMapPoint map_point = m_map_viewer.add_point(point, QColor(0, 0, 0, 255), 8);
					//SMapPoint map_point = m_map_viewer.add_point(point, s_colors[i % s_colors.size()].color, 8);
					render_route->add_point(map_point);
					map_point->set_name(QString::number(j));
					connect(&(map_point->point()), &qmapcontrol::CirclePoint::geometryClicked, [this](qmapcontrol::Geometry* geometry, QPoint point) { logs(geometry->name()); });
				}
			}
		}
		
		// рисуем направления пунктиром
		SMapWaypoint map_waypoint = m_map_viewer.add_waypoint(data_route->points, QColor(s_colors[i % s_colors.size()].color), Qt::PenStyle::DotLine, 1.6f);
		render_route->add_waypoint(map_waypoint);

		

		if (show_points)
		{
			// добавляем кнопки для скрытия маршрута
			QPushButton* button = m_main_window->add_route_button(QString("%1 %2").arg(i).arg(s_colors[i % s_colors.size()].name));
			button->setCheckable(true);
			button->setChecked(true);
			connect(button, &QPushButton::toggled, [this, button, render_route = render_route.get()]() { slot_route_button_clicked(button, render_route); });
			if (annealing_route->weight > annealing_route->avto->max_weight || annealing_route->volume > annealing_route->avto->max_volume)
			{
				//button->setFlat(true);
				button->setStyleSheet("background-color: #ff9a96");
			}
		}
	}
}
void Shell::slot_route_button_clicked(QPushButton* button, RenderRoute* render_route)
{
	render_route->set_visible(button->isChecked());
	//m_map_viewer.map_control()->update();
}


void RenderRoute::add_point(const SMapPoint& point)
{
	m_points.push_back(point);
}
void RenderRoute::add_waypoint(const SMapWaypoint& waypoint)
{
	m_waypoints.push_back(waypoint);
}
void RenderRoute::set_visible(bool visible)
{
	for (const SMapPoint& point : m_points)
		point->set_visible(visible);

	for (const SMapWaypoint& waypoint : m_waypoints)
		waypoint->set_visible(visible);
}