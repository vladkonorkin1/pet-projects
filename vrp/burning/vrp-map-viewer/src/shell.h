//=====================================================================================//
//   Author: open
//   Date:   31.07.2018
//=====================================================================================//
#pragma once
#include "burning_algorithm/burning_solver.h"
#include "map_viewer/map_viewer.h"
#include "base/network_manager.h"
#include "osrm/osrm_manager.h"
#include "main_window.h"
#include "base/timer.h"

class RenderRoute
{
private:
	QVector<SMapWaypoint> m_waypoints;
	QVector<SMapPoint> m_points;

public:
	void add_waypoint(const SMapWaypoint& waypoint);
	void add_point(const SMapPoint& point);
	void set_visible(bool visible);
};
using SRenderRoute = std::shared_ptr<RenderRoute>;


class Shell : public QObject
{
	Q_OBJECT
private:
	NetworkManager m_network_manager;
	OsrmManager m_osrm_manager;
	MapViewer m_map_viewer;
	std::unique_ptr<MainWindow> m_main_window;

	using render_routes_t = std::vector<SRenderRoute>;
	render_routes_t m_render_routes;

	Base::Timer m_timer;
	Burning::Data m_data;
	std::unique_ptr<Burning::Solver> m_solver;
	Burning::SPlan m_plan;

	SOsrmRequestMatrixDistances m_request_matrix_distances;

public:
	Shell();
	void activate();

private slots:
	void slot_timer_updated(float dt);
	void slot_request_matrix_distances_finished(bool success, const SMatrixDistances& matrix_distances);
	void slot_cycle_calculated(const Burning::SPlan& plan);
	void slot_solver_finish(const Burning::SPlan& best_plan);
	void slot_request_routes_finished(bool success, const routes_t& routes);

private:
	routes_t build_routes(const Burning::SPlan& plan) const;
	void slot_route_button_clicked(QPushButton* button, RenderRoute* render_route);
	void build_best_plan(const SMatrixDistances& matrix_distances);
	void show_results(const Burning::SPlan& plan, const routes_t& render_routes, bool show_points);
};