TARGET = vrp-map-viewer
CONFIG -= flat
TEMPLATE = app

release: DESTDIR = tmp/release
debug:   DESTDIR = tmp/debug

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.ui

QT += gui widgets network sql
QMAKE_CXXFLAGS += /MP
CONFIG += console
#Debug:CONFIG   += console

INCLUDEPATH += src
INCLUDEPATH += ../shared

CONFIG += precompile_header
PRECOMPILED_HEADER = ../shared/base/precomp.h

Release:QMAKE_POST_LINK = copy tmp\release\*.exe bin\release

win32:RC_FILE = src/resources/myapp.rc
#RESOURCES = src/resources/project.qrc
RESOURCES += ../shared/osrm/osrm_resources.qrc

win32 {
	LIBS += -Ld:\work\external\x86\QMapControl\Samples\bin\ -lqmapcontrol0
}
else {
	##LIBS += -L../../../external/qmapcontrol/src -lqmapcontrol
}
INCLUDEPATH += d:/work/external/x86/QMapControl/src/
INCLUDEPATH += ../../../../../../external/boost_1_68_0

HEADERS =  	../shared/base/precomp.h								\
			../shared/base/timer.h 									\
			../shared/base/log.h 									\
			../shared/base/network_object.h							\
			../shared/base/network_manager.h						\
			../shared/base/dynamic_matrix.h							\
			../shared/base/matrix_distances.h						\
			../shared/base/random_indices.h							\
			../shared/base/thread_task.h							\
			../shared/base/thread_task_manager.h					\
			../shared/base/types.h									\
			../shared/osrm/net_request_osrm_distance.h				\
			../shared/osrm/net_request_osrm_table_distances.h		\
			../shared/osrm/net_request_osrm_route.h					\
			../shared/osrm/osrm_manager.h							\
			../shared/osrm/osrm_database.h							\
			../shared/osrm/osrm_request_distances.h					\
			../shared/osrm/osrm_request_routes.h					\
			../shared/osrm/osrm_request_matrix_distances.h			\
			../shared/burning_algorithm/burning_solver.h			\
			../shared/burning_algorithm/burning_data.h				\
			../shared/burning_algorithm/burning_json.h				\
			../shared/burning_algorithm/burning_optimizator.h		\
			src/map_viewer/map_viewer.h								\
			src/main_window.h										\
			src/shell.h												\

SOURCES = 	../shared/base/precomp.cpp 								\
			../shared/base/timer.cpp 								\
			../shared/base/log.cpp 									\
			../shared/base/network_object.cpp						\
			../shared/base/network_manager.cpp						\
			../shared/base/matrix_distances.cpp						\
			../shared/base/random_indices.cpp						\
			../shared/base/thread_task_manager.cpp					\
			../shared/osrm/net_request_osrm_distance.cpp			\
			../shared/osrm/net_request_osrm_table_distances.cpp		\
			../shared/osrm/net_request_osrm_route.cpp				\
			../shared/osrm/osrm_manager.cpp							\
			../shared/osrm/osrm_database.cpp						\
			../shared/osrm/osrm_request_distances.cpp				\
			../shared/osrm/osrm_request_matrix_distances.cpp		\
			../shared/osrm/osrm_request_routes.cpp					\
			../shared/burning_algorithm/burning_solver.cpp			\
			../shared/burning_algorithm/burning_data.cpp			\
			../shared/burning_algorithm/burning_json.cpp			\
			../shared/burning_algorithm/burning_optimizator.cpp		\
			src/main_window.cpp										\
			src/map_viewer/map_viewer.cpp							\
			src/shell.cpp											\
			src/main.cpp											\
				
FORMS += 	src/ui/main_window.ui									\