﻿//=====================================================================================//
//   Author: open
//   Date:   30.10.2018
//=====================================================================================//
#include "base/precomp.h"
#include "shell.h"
#include "burning_algorithm/burning_json.h"
#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QDir>

Shell::Shell(const QString& hub_name, const QUuid& task_uuid)
: m_hub_name(hub_name)
, m_task_uuid(task_uuid)
{
	connect(&m_hub_connection, SIGNAL(connected()), this, SLOT(slot_connection_connected()));
	connect(&m_hub_connection, SIGNAL(disconnected()), this, SLOT(slot_connection_disconnected()), Qt::ConnectionType::QueuedConnection);
	connect(&m_hub_connection, SIGNAL(task_opened(const QJsonDocument&, const SMatrixDistances&)), this, SLOT(slot_task_opened(const QJsonDocument&, const SMatrixDistances&)));
	connect(&m_hub_connection, SIGNAL(exit_process()), this, SLOT(slot_exit_process()));
	m_hub_connection.connect_host(m_hub_name);
}
void Shell::slot_connection_connected()
{
	m_hub_connection.send_message(NetHub::ClientMessageAuthorize(m_task_uuid));
}
void Shell::slot_connection_disconnected()
{	
	QCoreApplication::exit(-2);
}
void Shell::slot_exit_process() 
{
	QCoreApplication::exit(0);
}
void Shell::slot_task_opened(const QJsonDocument& json_doc, const SMatrixDistances& matrix_distances)
{
	//QDir().mkdir("requests");
	//QString path = QString("requests/%1 %2.json").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss")).arg(m_task_uuid.toString());
	//save_json(json_doc, path);
	
	Burning::json_parse_data(json_doc, m_data);

	if (m_data.is_init())
	{
		m_data.matrix_distances = matrix_distances;
		build_best_plan();
	}
	else
	{
		send_result(Burning::SPlan());
	}
}
void Shell::build_best_plan()
{
	m_solver.reset(new Burning::Solver());
	connect(m_solver.get(), SIGNAL(finished(const Burning::SPlan&)), SLOT(slot_solver_finish(const Burning::SPlan&)));
	m_solver->calculate(m_data);
}
void Shell::slot_solver_finish(const Burning::SPlan& plan)
{
	send_result(plan);
}
void Shell::send_result(const Burning::SPlan& plan)
{
	QJsonDocument json_result = Burning::plan_to_json(plan, m_data);
	m_hub_connection.send_message(NetHub::ClientMessageTicketSolved(json_result));

	//QDir().mkdir("results");
	//QString path = QString("results/%1 %2.json").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss")).arg(m_task_uuid.toString());
	//save_json(json_result, );
}
void Shell::save_json(const QJsonDocument& json_doc, const QString& filename) const
{
	QFile file(filename);
	if (file.open(QIODevice::WriteOnly))
		file.write(json_doc.toJson());
}
