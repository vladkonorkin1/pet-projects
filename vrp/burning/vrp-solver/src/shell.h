//=====================================================================================//
//   Author: open
//   Date:   30.10.2018
//=====================================================================================//
#pragma once
#include "burning_algorithm/burning_solver.h"
#include "base/thread_task_manager.h"
#include "base/network_manager.h"
#include "net_hub_connection.h"

class Shell : public QObject
{
	Q_OBJECT
private:
	QString m_hub_name;
	QUuid m_task_uuid;
	NetHub::Connection m_hub_connection;
	NetworkManager m_network_manager;

	Burning::Data m_data;
	std::unique_ptr<Burning::Solver> m_solver;
	Burning::SPlan m_plan;

public:
	Shell(const QString& hub_name, const QUuid& task_uuid);

private slots:
	void slot_connection_connected();
	void slot_connection_disconnected();
	void slot_task_opened(const QJsonDocument& json_doc, const SMatrixDistances& matrix_distances);
	void slot_solver_finish(const Burning::SPlan& plan);
	void slot_exit_process();

private:
	void build_best_plan();
	void send_result(const Burning::SPlan& plan);
	void save_json(const QJsonDocument& json_doc, const QString& filename) const;
};
