TARGET = prototype_vrp_solver
CONFIG -= flat
TEMPLATE = app

release: DESTDIR = tmp/release
debug:   DESTDIR = tmp/debug

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.ui

QT += gui widgets network
QMAKE_CXXFLAGS += /MP
CONFIG += console
##Debug:CONFIG   += console

INCLUDEPATH += src

CONFIG += precompile_header
PRECOMPILED_HEADER = src/base/precomp.h

Release:QMAKE_POST_LINK = copy tmp\release\*.exe bin\release
RESOURCES = src/resources/project.qrc
RC_FILE = src/resources/myapp.rc

win32 {
	LIBS += -L../../external/qmapcontrol/Samples/bin -lqmapcontrol0
}
else {
	##LIBS += -L../../external/qmapcontrol/src -lqmapcontrol
}
INCLUDEPATH += ../../external/qmapcontrol/src
INCLUDEPATH += ../../../../external/boost_1_67_0
INCLUDEPATH += ../../../../external/boost_1_68_0

HEADERS =  	src/base/precomp.h 									\
			src/base/timer.h 									\
			src/base/log.h 										\
			src/base/network_object.h							\
			src/base/network_manager.h							\
			src/base/dynamic_matrix.h							\
			src/base/random_indices.h							\
			src/base/thread_task.h								\
			src/base/thread_task_manager.h						\
			src/map_viewer/map_viewer.h							\
			src/models/money.h									\
			src/main_window.h									\
			src/shell.h											\
			src/osrm_manager/osrm_route.h						\
			src/osrm_manager/net_request_osrm_route.h			\
			src/osrm_manager/osrm_manager.h						\
			src/osrm_manager/osrm_route_matrix.h				\
			src/vrp_solver/vrp_solver.h							\
			src/vrp_solver/vrp_data.h							\
			src/vrp_solver/vrp_create_population_task.h			\
			src/vrp_solver/vrp_crossover_task.h					\
			src/vrp_solver/vrp_mutation_task.h					\
			src/vrp_solver/vrp_insert_point.h					\
			src/vrp_solver/vrp_async_solver.h					\

SOURCES = 	src/base/precomp.cpp 								\
			src/base/timer.cpp 									\
			src/base/log.cpp 									\
			src/base/network_object.cpp							\
			src/base/network_manager.cpp						\
			src/base/random_indices.cpp							\
			src/base/thread_task_manager.cpp					\
			src/map_viewer/map_viewer.cpp						\
			src/models/money.cpp								\
			src/main_window.cpp									\
			src/shell.cpp										\
			src/main.cpp										\
			src/osrm_manager/net_request_osrm_route.cpp			\
			src/osrm_manager/osrm_manager.cpp					\
			src/osrm_manager/osrm_route_matrix.cpp				\
			src/vrp_solver/vrp_solver.cpp						\
			src/vrp_solver/vrp_data.cpp							\
			src/vrp_solver/vrp_create_population_task.cpp		\
			src/vrp_solver/vrp_crossover_task.cpp				\
			src/vrp_solver/vrp_mutation_task.cpp				\
			src/vrp_solver/vrp_insert_point.cpp					\
			src/vrp_solver/vrp_async_solver.cpp					\
			
FORMS += 	src/ui/main_window.ui								\