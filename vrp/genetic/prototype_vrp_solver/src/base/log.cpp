//=====================================================================================//
//   Author: open
//   Date:   08.09.2017
//=====================================================================================//
#include "base/precomp.h"
#include "log.h"
#include <QTextStream>
#include <QFile>
#include <QDir>

#include <Windows.h>

class FileLogOutput : public LogOutput
{
private:
	QFile m_file;
	QTextStream m_stream;

public:
	FileLogOutput()
	: m_file(build_path())
	{
		if (m_file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
		{
			m_stream.setDevice(&m_file);
			m_stream.setCodec("UTF-8");
		}
	}
	virtual void msg(const QString& str, const QDateTime& datetime)
	{
		QString printed_str = datetime.toString("[dd.MM.yyyy hh:mm:ss] %1").arg(str);

		m_stream << printed_str.toUtf8();
		m_stream << '\n';
		m_stream.flush();
	}

private:
	QString build_path() const	{
		const QString log_folder("logs");
		QDir dir(log_folder);
		dir.mkdir(".");
		return log_folder + '/' + QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss.log");
	}
};

class StdCoutLogOutput : public LogOutput
{
private:
	QDateTime m_start_time;
	HANDLE m_hstdout;

public:
	StdCoutLogOutput()
	: m_start_time(QDateTime::currentDateTime())
	, m_hstdout(GetStdHandle(STD_OUTPUT_HANDLE))
	{
		//QTextCodec::setCodecForLocale(QTextCodec::codecForName("IBM 866"));
	}
	virtual void msg(const QString& str, const QDateTime& datetime)
	{
		quint64 msec = m_start_time.msecsTo(datetime);

		//QString printed_str = QString().sprintf("[%1 %.2f] %2", msec/1000.).arg(datetime.toString("hh:mm:ss")).arg(str);
		//set_color(Log::Color::Black, Log::Color::Red);
		//std::cout << qPrintable(printed_str) << std::endl;



		QString stime = datetime.toString("hh:mm:ss ");
		set_color(Log::Color::Gray);
		std::cout << qPrintable(stime);

		set_color(Log::Color::Silver, Log::Color::Gray);
		QString smsec;
		smsec = smsec.sprintf("%.2f", msec / 1000.);
		std::cout << qPrintable(smsec);
		
		set_color(Log::Color::Black);
		std::cout << " ";

		set_color(Log::Color::Red);

		std::cout << qPrintable(str) << std::endl;

		set_color(Log::Color::Lime);
	}
private:
	void set_color(Log::Color foreground_color, Log::Color background_color = Log::Color::Black)
	{
		int back = static_cast<int>(background_color);
		int fore = static_cast<int>(foreground_color);
		WORD color = (back << 4) | fore;
		SetConsoleTextAttribute(m_hstdout, color);
	}
};


Log::Log(bool print_datetime)
: m_print_datetime(print_datetime)
{
	add_output(ULogOutput(new FileLogOutput()));
	add_output(ULogOutput(new StdCoutLogOutput()));
}
void Log::msg(const QString& str)
{
	QMutexLocker locker(&m_mutex);
	QDateTime datetime = QDateTime::currentDateTime();
	for (auto& output : m_outputs)
		output->msg(str, datetime);
}
/*QString Log::format_message(const QString& str) const
{
	if (m_print_datetime) return QDateTime::currentDateTime().toString("[dd.MM.yyyy hh:mm:ss] %1").arg(str);
	else {
		quint64 msec = m_start_time.msecsTo(QDateTime::currentDateTime());
		return QString().sprintf("[%.2f] %1", msec / 1000.).arg(str);
	}
}*/
void Log::add_output(ULogOutput output) {
	m_outputs.push_back(std::move(output));
}
#include "base/console.h"
void Log::init()
{
	SetConsoleWindow(150, 200);
	//s_log.reset(new Log(true));
}


SingleLog::ULog SingleLog::s_log;

void SingleLog::msg(const QString& str)
{
	if (!s_log.get()) s_log.reset(new Log(true));
	s_log->msg(str);
}