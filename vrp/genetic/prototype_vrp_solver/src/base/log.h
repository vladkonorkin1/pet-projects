//=====================================================================================//
//   Author: open
//   Date:   08.09.2017
//=====================================================================================//
#pragma once
#include <QDateTime>
#include <QMutex>

class LogOutput
{
public:
	virtual void msg(const QString& str, const QDateTime& datetime) = 0;
};

class Log
{
public:
	// http://www.wideskills.com/html/html-color-names
	enum class Color : unsigned char
	{
		Black,	// 0x0
		Navy,	// 0x1
		Green,	// 0x2
		Teal,	// 0x3
		Maroon,	// 0x4
		Purple,	// 0x5
		Olive,	// 0x6
		Silver,	// 0x7
		Gray,	// 0x8
		Blue,	// 0x9
		Lime,	// 0xA
		Aqua,	// 0xB
		Red,	// 0xC
		Fuchsia,// 0xD
		Yellow,	// 0xE
		White	// 0xF
	};

	enum class Level
	{
		Debug,
		Info,
		Warning,
		Critical
	};

private:
	using ULogOutput = std::unique_ptr<LogOutput>;
	std::vector<ULogOutput> m_outputs;
	QMutex m_mutex;
	bool m_print_datetime;

public:
	Log(bool print_datetime);
	virtual ~Log() {}
	void msg(const QString& str);
	static void init();

private:
	//QString format_message(const QString& str) const;
	void add_output(ULogOutput output);
};

class SingleLog
{
private:
	using ULog = std::unique_ptr<Log>;
	static ULog s_log;

public:
	static void msg(const QString& str);
};

#define logs(x) SingleLog::msg(x)