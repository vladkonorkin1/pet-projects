//=====================================================================================//
//   Author: open
//   Date:   04.07.2018
//=====================================================================================//
#pragma once

class ThreadTask : public QObject
{
	Q_OBJECT
public:
	// !!! not in main thread
	virtual void on_process() = 0;
	// in main thread!!!
	virtual void on_finish_process() = 0;
};

using SThreadTask = std::shared_ptr<ThreadTask>;
using WThreadTask = std::weak_ptr<ThreadTask>;