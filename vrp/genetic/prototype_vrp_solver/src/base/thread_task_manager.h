//=====================================================================================//
//   Author: open
//   Date:   04.07.2018
//=====================================================================================//
#ifndef THREAD_TASK_MANAGER_H
#define THREAD_TASK_MANAGER_H
#include "base/thread_task.h"
#include <QThread>
#include <queue>
#include <stack>


class ThreadProvider : public QObject
{
	Q_OBJECT
private:
	QThread m_thread;
	SThreadTask m_thread_task;		// store shared_ptr

public:
	ThreadProvider(const QString& thread_name);
	virtual ~ThreadProvider();
	void start_thread();
	void post_task(const SThreadTask& thread_task);
	void finish_task();

signals:
	void finished(ThreadProvider* thread_provider);

protected:
	virtual bool event(QEvent* event);

private:
	friend class ThreadStartTaskEvent;
	void emit_finished();
};


class ThreadTaskManager : public QObject
{
	Q_OBJECT
public:
	using tasks_t = std::vector<WThreadTask>;

private:
	using UThreadProvider = std::unique_ptr<ThreadProvider>;
	using thread_pool_t = std::vector<UThreadProvider>;
	thread_pool_t m_pool;

	using free_t = std::stack<ThreadProvider*>;
	free_t m_free;

	using worked_t = std::vector<ThreadProvider*>;
	worked_t m_worked;

	struct Batch
	{
		tasks_t tasks;
		int index;
		int sended;
		int count_processed;
		Batch(const tasks_t& tasks);

		SThreadTask next_task();
		bool is_finish() const;
	};

	using batches_t = std::queue<Batch>;
	batches_t m_batches;

	
public:
	ThreadTaskManager();
	void add_tasks(const tasks_t& tasks);
	int count_threads() const;

private slots:
	void slot_thread_finished(ThreadProvider* thread_provider);

private:
	void start_batch_tasks();
	Batch* get_last_batch();
	void add_thread_task(const SThreadTask& thread_task);
};

#endif