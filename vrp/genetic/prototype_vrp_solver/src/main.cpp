//=====================================================================================//
//   Author: open
//   Date:   31.07.2018
//=====================================================================================//
#include <QApplication>
#include "shell.h"
#include <time.h>
#include <QMessageBox>

int main(int argc, char *argv[])
{
	Log::init();

	// 75000 500
	//  0 -  99517.4
	//  1 -  99393.0
	//  2 -  99908.6
	//  3 - 101031
	//  4 -  99814.6
	//  5 -  97892.8   !!!!
	//  6 - 100203.0
	//  7 -  99387.1
	//  8 - 100156.0
	//  9 -  99907.5
	// 10 -  99366.3
	// 11 -  99216.1
	// 12 -  98895.7
	// 13 -  98710.9
	// 14 - 101038
	// 15 - 100108
	// 16 - 100255  - ������������ � ���.������ + ������� ����
	// 17 -  98998.8
	// 18 - 100414
	// 19 - 100398
	// 20 -  99646.5
	// 21 - 100861
	// 22 - 100137
	// 23 -  99444.9
	//srand(23);

	//int i = 5;
	//i = ++i + ++i;
	//std::cout << i << std::endl;

	Q_INIT_RESOURCE(project);
	QCoreApplication::addLibraryPath(".");
	//QApplication::setStyle("fusion");
	QApplication app(argc, argv);

	/*{
		float t = 100.0;
		float decrement = 0.9f;
		int i = 1;
		int N = 150;

		while (t > 0.005f)
		{
			float pp = powf(i, 1.f / (2 * N));
			t = expf(-decrement * pp) * 100.f;
			i++;
			std::cout << t << std::endl;
		}
	}*/

	try
	{
		Shell shell;
		shell.activate();
		return app.exec();
	}
	catch (const std::exception& e)	{
		QMessageBox::critical(0, "Exception", e.what());
	}
	return -1;
}