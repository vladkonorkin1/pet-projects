//=====================================================================================//
//   Author: open
//   Date:   01.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "main_window.h"
#include "map_viewer/map_viewer.h"

MainWindow::MainWindow(MapViewer* map_viewer)
: m_ui(create_ui(this))
, m_map_viewer(map_viewer)
{
	m_ui->verticalLayout->addWidget(m_map_viewer->map_control());
}
//MainWindow::~MainWindow()
//{
//	//m_ui->verticalLayout->removeWidget(m_map_viewer->map_control());
//}
std::auto_ptr<Ui::MainWindow> MainWindow::create_ui(MainWindow* main_window) const
{
	std::auto_ptr<Ui::MainWindow> ui(new Ui::MainWindow());
	ui->setupUi(main_window);
	return ui;
}
void MainWindow::resizeEvent(QResizeEvent* event)
{
	m_map_viewer->resize(event->size());
}
void MainWindow::add_msg(const QString& msg)
{
	m_ui->text_edit->appendPlainText(msg);
}