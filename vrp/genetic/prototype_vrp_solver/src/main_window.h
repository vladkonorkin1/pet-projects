//=====================================================================================//
//   Author: open
//   Date:   01.08.2018
//=====================================================================================//
#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H
#include "ui_main_window.h"

class MapViewer;

class MainWindow : public QMainWindow
{
	Q_OBJECT
private:
	std::auto_ptr<Ui::MainWindow> m_ui;
	MapViewer* m_map_viewer;

public:
	MainWindow(MapViewer* map_viewer);
	//virtual ~MainWindow();
	void add_msg(const QString& msg);

protected:
	virtual void resizeEvent(QResizeEvent* event);

private:
	std::auto_ptr<Ui::MainWindow> create_ui(MainWindow* main_window) const;
};

#endif