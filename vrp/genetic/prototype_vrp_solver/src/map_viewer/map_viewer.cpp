#include "base/precomp.h"
#include "map_viewer/map_viewer.h"

MapPoint::MapPoint(qmapcontrol::Layer* layer, const QPointF& pos, int radius)
: m_point(pos.x(), pos.y(), radius, QString(), qmapcontrol::Point::Middle)
, m_layer(layer)
{
	m_layer->addGeometry(&m_point);
}
MapPoint::~MapPoint()
{
	if (m_layer)
		m_layer->removeGeometry(&m_point, false);
}
void MapPoint::set_pos(const QPointF& pos)
{
	m_point.setCoordinate(pos);
}
void MapPoint::set_color(const QColor& color)
{
	QPen* pen = new QPen(color);
	pen->setWidth(4);
	m_point.setPen(pen);
}


MapWaypoint::MapWaypoint(qmapcontrol::Layer* layer, const vpositions_t& positions, const QColor& color, Qt::PenStyle pen_style, float width)
: m_layer(layer)
{
	for (const QPointF& pos : positions)
	{
		qmapcontrol::Point* point = new qmapcontrol::Point(pos.x(), pos.y());
		m_lines.addPoint(point);
	}
	m_lines.setPen(new QPen(color, width, pen_style));
	m_layer->addGeometry(&m_lines);
}
MapWaypoint::~MapWaypoint()
{
	if (m_layer)
		m_layer->removeGeometry(&m_lines, false);
}


MapViewer::MapViewer()
: m_map_control(new qmapcontrol::MapControl(QSize(380, 540)))
, m_map_adapter(new qmapcontrol::OSMMapAdapter())
, m_main_layer(new qmapcontrol::MapLayer("OpenStreetMap-Layer", m_map_adapter))
{
	//m_map_control->enablePersistentCache(QDir::homePath() + "/QMapControl.cache", 10000);
	m_map_control->enablePersistentCache(QDir::currentPath() + "/cache", 10000);
	m_map_control->showScale(true);
	m_map_control->addLayer(m_main_layer);

	m_map_control->setView(QPointF(60.754581, 56.758808));
	m_map_control->setZoom(7);

	connect(m_map_control, SIGNAL(viewChanged(const QPointF&, int)), SLOT(slot_view_changed(const QPointF&, int)));
	connect(m_map_control, SIGNAL(mouseEventCoordinate(const QMouseEvent*, const QPointF)), SLOT(slot_mouse_click(const QMouseEvent*, const QPointF)));
	//m_map_control->enableMouseWheelEvents(false);
}
void MapViewer::resize(const QSize& size)
{
	m_map_control->resize(size);
}
void MapViewer::update(float dt)
{
	Q_UNUSED(dt);
	//QPointF coordinate = m_map_control->currentCoordinate();
	//std::cout << coordinate.x() << "\t" << coordinate.y() << std::endl;
	//logs(QString("%1 %2").arg(coordinate.x()).arg(coordinate.y()));
}
void MapViewer::slot_view_changed(const QPointF& coordinate, int zoom)
{
	Q_UNUSED(coordinate);
	Q_UNUSED(zoom);
}
SMapPoint MapViewer::add_point(const QPointF& position, const QColor& color, int radius)
{
	//SMapPoint map_point(new MapPoint(m_main_layer, position), [this](MapPoint* map_point) { delete map_point; });
	SMapPoint map_point(new MapPoint(m_main_layer, position, radius));
	map_point->set_color(color);
	return map_point;
}
SMapWaypoint MapViewer::add_waypoint(const vpositions_t& positions, const QColor& color, Qt::PenStyle pen_style, float width)
{
	SMapWaypoint map_waypoint(new MapWaypoint(m_main_layer, positions, color, pen_style, width));
	return map_waypoint;
}
#include <QMouseEvent>
//#include <QMousePressEvent>

static std::vector<QPointF> s_test_distances;
QString build_distance_str()
{
	QString result;
	for (const QPointF& point : s_test_distances)
	{
		float volume = static_cast<float>(std::rand() % 1000) / 140.f;
		float weight = static_cast<float>(std::rand() % 1000) / 2.f;
		result += QString("m_destination_points.push_back(DestinationPoint(QPointF(%1, %2), %3, %4));\n").arg(point.x()).arg(point.y()).arg(volume).arg(weight);
	}
	return result;
}

void MapViewer::slot_mouse_click(const QMouseEvent* event, const QPointF coordinate)
{
	if (event->button() == Qt::MouseButton::RightButton && event->type() == QEvent::MouseButtonPress)
	{
		logs(QString("%1  %2 %3").arg(s_test_distances.size()).arg(coordinate.x()).arg(coordinate.y()));
		s_test_distances.push_back(coordinate);
		if (s_test_distances.size() > 20)
		{
			QString test = build_distance_str();
			__nop();
		}
	}
}