#ifndef MAP_VIEWER_H
#define MAP_VIEWER_H

#include <osmmapadapter.h>
#include <circlepoint.h>
#include <linestring.h>
#include <mapcontrol.h>
#include <maplayer.h>

class MapPoint
{
private:
	qmapcontrol::CirclePoint m_point;
	qmapcontrol::Layer* m_layer;

public:
	MapPoint(qmapcontrol::Layer* layer, const QPointF& pos, int radius);
	~MapPoint();
	void set_pos(const QPointF& pos);
	void set_color(const QColor& color);
};
using SMapPoint = std::shared_ptr<MapPoint>;


using vpositions_t = std::vector<QPointF>;


class MapWaypoint
{
private:
	qmapcontrol::LineString m_lines;
	qmapcontrol::Layer* m_layer;

public:
	MapWaypoint(qmapcontrol::Layer* layer, const vpositions_t& positions, const QColor& color, Qt::PenStyle pen_style, float width);
	~MapWaypoint();
};
using SMapWaypoint = std::shared_ptr<MapWaypoint>;


class MapViewer : public QObject
{
    Q_OBJECT
private:
	qmapcontrol::MapControl* m_map_control;	// main_window control memory!!!
	qmapcontrol::MapAdapter* m_map_adapter;
	qmapcontrol::Layer* m_main_layer;

public:
	MapViewer();
	void update(float dt);
	void resize(const QSize& size);
	qmapcontrol::MapControl* map_control();

	SMapPoint add_point(const QPointF& position, const QColor& color, int radius);
	SMapWaypoint add_waypoint(const vpositions_t& positions, const QColor& color, Qt::PenStyle pen_style, float width);

private slots:
	void slot_view_changed(const QPointF& coordinate, int zoom);
	void slot_mouse_click(const QMouseEvent* event, const QPointF coordinate);
	//void slot_geometry_clicked(qmapcontrol::Geometry* geometry, QPoint coord_px);
};

inline qmapcontrol::MapControl* MapViewer::map_control() {
	return m_map_control;
}

#endif