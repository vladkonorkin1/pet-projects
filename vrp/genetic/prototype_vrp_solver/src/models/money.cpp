//=====================================================================================//
//   Author: open
//   Date:   04.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "models/money.h"

Money::Money(qint64 value)
: value(value)
{
}
Money Money::from_double(double v)
{
	return Money(QString().sprintf("%.0f", v * 100).toLongLong());
}
#include <QLocale>
QString Money::to_string() const
{
	float f = float(value)*0.01f;
	return QString().sprintf("%.2f", f).replace('.', QLocale().decimalPoint());
}
double Money::to_double() const
{
	return double(value) * 0.01;
}
bool Money::operator < (const Money& other) const {
	return value < other.value;
}
bool Money::operator > (const Money& other) const {
	return value > other.value;
}
bool Money::operator <= (const Money& other) const {
	return value <= other.value;
}
bool Money::operator >= (const Money& other) const {
	return value >= other.value;
}
Money& Money::operator += (const Money& other)
{
	value += other.value;
	return *this;
}
static Money s_zero_money(0);
const Money& Money::zero()
{
	return s_zero_money;
}