//=====================================================================================//
//   Author: open
//   Date:   04.08.2018
//=====================================================================================//
#ifndef MONEY_H
#define MONEY_H

// ��� ��� �������� ������� (� ��������)
class Money
{
public:
	qint64 value;

public:
	explicit Money(qint64 value = 0);
	static Money from_double(double v);
	static const Money& zero();
	QString to_string() const;
	double to_double() const;

	bool operator < (const Money& other) const;
	bool operator > (const Money& other) const;
	bool operator >= (const Money& other) const;
	bool operator <= (const Money& other) const;
	Money& operator += (const Money& other);
};

inline Money operator * (const Money& money, int value) {
	return Money(money.value * value);
}
inline Money operator - (const Money& left, const Money& right) {
	return Money(left.value - right.value);
}

#endif