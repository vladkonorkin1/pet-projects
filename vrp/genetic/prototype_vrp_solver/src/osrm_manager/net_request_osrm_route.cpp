//=====================================================================================//
//   Author: open
//   Date:   02.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "net_request_osrm_route.h"
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

NetRequestOsrmRoute::NetRequestOsrmRoute(const QPointF& start_pos, const QPointF& end_pos)
: m_start_pos(start_pos)
, m_end_pos(end_pos)
{
}
// http://10.30.37.92:5000/table/v1/driving/60.754581,56.758808;61.754580,56.758800;60.754570,56.758700&annotations=distance
// http://10.30.37.92:5000/route/v1/driving/60.754581,56.758808;61.754580,56.758800;60.754570,56.758700?geometries=geojson&overview=full&generate_hints=false
QString NetRequestOsrmRoute::url() const {
	return "http://10.30.37.92:5000/route/v1/driving/"
		+ QString("%1,%2;%3,%4").arg(m_start_pos.x()).arg(m_start_pos.y()).arg(m_end_pos.x()).arg(m_end_pos.y())
		+ "?geometries=geojson&overview=full&generate_hints=false";
	//return Settings::api_url() + "users/";
}
void NetRequestOsrmRoute::process(QNetworkReply* reply, bool success)
{
	OsrmRoute osrm_route;
	osrm_route.start_point = m_start_pos;
	osrm_route.end_point = m_end_pos;

	if (success)
	{
		QJsonDocument json_doc = QJsonDocument::fromJson(reply->readAll());
		QJsonObject json_obj = json_doc.object();
		if (json_obj["code"] == "Ok")
		{
			QJsonArray json_routes = json_obj["routes"].toArray();
			QJsonObject json_route = json_routes[0].toObject();

			parse_coordinate(json_route["geometry"].toObject()["coordinates"].toArray(), osrm_route.waypoints);
			osrm_route.distance = json_route["distance"].toDouble();
		}
		else 
			logs("NetRequestOsrmRoute: code not OK");
	}
	emit finished(success, osrm_route);
}
void NetRequestOsrmRoute::parse_coordinate(const QJsonArray& json_coordinate, std::vector<QPointF>& coordinate)
{
	/*for (const auto& json : json_coordinate)
	{
		QJsonArray json_coord = json.toArray();
		coordinate.push_back(QPointF(json_coord[0].toDouble(), json_coord[1].toDouble()));
	}*/

	int step = std::max(1, json_coordinate.size() / 48);
	for (size_t k = 0; k < json_coordinate.size(); k += step)
	{
		QJsonArray json_coord = json_coordinate[k].toArray();
		coordinate.push_back(QPointF(json_coord[0].toDouble(), json_coord[1].toDouble()));
	}
}