//=====================================================================================//
//   Author: open
//   Date:   02.08.2018
//=====================================================================================//
#ifndef NET_REQUEST_OSRM_ROUTE
#define NET_REQUEST_OSRM_ROUTE
#include "base/network_object.h"
#include "osrm_manager/osrm_route.h"

class NetRequestOsrmRoute : public PostNetworkObject
{
	Q_OBJECT
private:
	QPointF m_start_pos;
	QPointF m_end_pos;

public:
	NetRequestOsrmRoute(const QPointF& start_pos, const QPointF& end_pos);
	virtual void process(QNetworkReply* reply, bool success);
	virtual QString url() const;

signals:
	void finished(bool success, const OsrmRoute& osrm_route);

private:
	void parse_coordinate(const QJsonArray& json_coordinate, std::vector<QPointF>& coordinate);
};

using SNetRequestOsrmRoute = std::shared_ptr<NetRequestOsrmRoute>;

#endif