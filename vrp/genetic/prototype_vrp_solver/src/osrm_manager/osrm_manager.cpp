//=====================================================================================//
//   Author: open
//   Date:   02.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "osrm_manager.h"
#include "base/network_manager.h"

int OsrmManager::s_batch_count = 12;	// 32

OsrmManager::OsrmManager(NetworkManager& network_manager)
: m_network_manager(network_manager)
, m_num_finished(0)
, m_count_queue(0)
, m_total_count(0)
{
}
void OsrmManager::request_routes(const std::vector<QPointF>& points)
{
	logs("osrm: start");

	m_matrix.reset(new OsrmMatrix(points.size()));
	m_num_finished = 0;

	int count = points.size();
	assert( count > 0 );
	m_total_count = count*(count - 1);
	
	m_pool_requests.clear();
	m_pool_requests.reserve(count*count);
	for (int i=0; i<count; ++i)
	{
		for (int j = 0; j < count; ++j)
		{
			if (i != j)
			{
				SNetRequestOsrmRoute request(new NetRequestOsrmRoute(points[i], points[j]));
				connect(request.get(), &NetRequestOsrmRoute::finished, [this, request, i, j](bool success, const OsrmRoute& osrm_route) {slot_request_finished(success, osrm_route, request, i, j); });
				m_pool_requests.push_back(request);
			}
		}
	}
	m_count_queue = 0;
	send_requests();
}
void OsrmManager::send_requests()
{
	while (m_count_queue < s_batch_count && !m_pool_requests.empty())
	{
		m_count_queue++;
		pool_requests_t::iterator it = --m_pool_requests.end();
		m_network_manager.request(*it, NetworkMethod::Get);
		m_pool_requests.erase(it);
	}
}
void OsrmManager::slot_request_finished(bool success, const OsrmRoute& osrm_route, const SNetRequestOsrmRoute& request, int row, int col)
{
	if (success)
	{
		m_count_queue--;
		send_requests();

		(*m_matrix)(row, col) = osrm_route;

		m_num_finished++;

		if ((m_num_finished % 200) == 0 || (m_num_finished == m_total_count))
			logs(QString().sprintf("osrm: %.1f%%", (float)m_num_finished / (float)m_total_count*100.f));
			//logs(QString("osrm: %1%").arg((float)m_num_finished/(float)m_total_count*100.f));

		if (m_num_finished == m_total_count)
		{
			logs("osrm: finish");
			emit request_routes_finished(true, m_matrix);
		}
	}
	else
	{
		logs("failed request to osrm-server: " + request->url());
		m_network_manager.reconnect(request);
	}
}