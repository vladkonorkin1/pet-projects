//=====================================================================================//
//   Author: open
//   Date:   02.08.2018
//=====================================================================================//
#ifndef OSRM_MANAGER_H
#define OSRM_MANAGER_H
#include "osrm_manager/net_request_osrm_route.h"
//#include "osrm_manager/osrm_route.h"
//#include "base/dynamic_matrix.h"
#include "osrm_manager/osrm_route_matrix.h"

class NetworkManager;

class OsrmManager : public QObject
{
	Q_OBJECT
private:
	NetworkManager& m_network_manager;

	//using WaypointsMatrix = DynamicMatrix<OsrmRoute>;
	//using UWaypointsMatrix = std::unique_ptr<WaypointsMatrix>;
	//UWaypointsMatrix m_waypoints_matrix;
	SOsrmMatrix m_matrix;

	int m_total_count;
	int m_num_finished;

	using pool_requests_t = std::vector<SNetRequestOsrmRoute>;
	pool_requests_t m_pool_requests;

	static int s_batch_count;
	int m_count_queue;

public:
	OsrmManager(NetworkManager& network_manager);
	void request_routes(const std::vector<QPointF>& points);

signals:
	void request_routes_finished(bool success, const SOsrmMatrix& matrix);

private:
	void slot_request_finished(bool success, const OsrmRoute& osrm_route, const SNetRequestOsrmRoute& request, int row, int col);

private:
	void send_requests();
};

#endif