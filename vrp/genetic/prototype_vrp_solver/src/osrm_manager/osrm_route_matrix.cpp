//=====================================================================================//
//   Author: open
//   Date:   09.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "osrm_route_matrix.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

SOsrmMatrix read_matrix(const QString& file_path, int dimension)
{
	QFile file(file_path);
	if (file.open(QIODevice::ReadOnly) && file.size() > 0)
	{
		QJsonDocument json_document = QJsonDocument::fromJson(file.readAll());
		QJsonArray json_array = json_document.array();
		SOsrmMatrix matrix(new DynamicMatrix<OsrmRoute>(dimension));
		QRegExp reg_exp("(\\,|\\;)");
		for (const auto& v : json_array)
		{
			QJsonObject json_obj = v.toObject();
			int i = json_obj["i"].toInt();
			int j = json_obj["j"].toInt();

			OsrmRoute& osrm_route = (*matrix)(i, j);
			osrm_route.distance = json_obj["distance"].toDouble();

			QStringList list = json_obj["waypoints"].toString().split(reg_exp, QString::SplitBehavior::SkipEmptyParts);
			for (int i = 0; i < list.size(); ++(++i))
				osrm_route.waypoints.push_back(QPointF(list[i].toFloat(), list[i + 1].toFloat()));
		}
		return matrix;
	}
	return SOsrmMatrix();
}

QString write_json_route_waypoints(const OsrmRoute::waypoints_t& positions)
{
	QString str;
	char buf[256];
	for (const QPointF& pos : positions)
	{
		sprintf(buf, "%.6f,%.6f;", pos.x(), pos.y());
		str += buf;
	}
	return str;
}
void write_matrix(const QString& file_path, const OsrmMatrix& matrix)
{
	QFile file(file_path);
	if (file.open(QIODevice::WriteOnly))
	{
		QJsonDocument json_document;
		QJsonArray json_array;

		for (int i = 0; i < matrix.dimension(); ++i)
		{
			for (int j = 0; j < matrix.dimension(); ++j)
			{
				const OsrmRoute& osrm_route = matrix(i, j);
				QJsonObject json_obj;
				json_obj["i"] = i;
				json_obj["j"] = j;
				if (i != j)
				{
					json_obj["distance"] = osrm_route.distance;
					json_obj["waypoints"] = write_json_route_waypoints(osrm_route.waypoints);
					json_array.push_back(json_obj);
				}
			}
		}
		json_document.setArray(json_array);
		file.write(json_document.toJson());
	}
}