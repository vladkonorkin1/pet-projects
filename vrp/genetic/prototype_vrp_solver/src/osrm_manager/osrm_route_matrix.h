//=====================================================================================//
//   Author: open
//   Date:   09.08.2018
//=====================================================================================//
#ifndef OSRM_ROUTE_MATRIX_H
#define OSRM_ROUTE_MATRIX_H
#include "osrm_manager/osrm_route.h"
#include "base/dynamic_matrix.h"

using OsrmMatrix = DynamicMatrix<OsrmRoute>;
using SOsrmMatrix = std::shared_ptr<OsrmMatrix>;

extern SOsrmMatrix read_matrix(const QString& file_path, int dimension);
extern void write_matrix(const QString& file_path, const OsrmMatrix& matrix);

#endif