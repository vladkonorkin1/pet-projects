//=====================================================================================//
//   Author: open
//   Date:   31.07.2018
//=====================================================================================//
#include "base/precomp.h"
#include "shell.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

Shell::Shell()
: m_main_window(new MainWindow(&m_map_viewer))
, m_timer(0.5f)
{
	connect(&m_timer, SIGNAL(updated(float)), SLOT(slot_timer_updated(float)));

	//m_available_cars.push_back(SCarDesc(new CarDesc(11.f, 1500.f, 10.5f, 250.f, 500.f, 10)));
	//m_available_cars.push_back(SCarDesc(new CarDesc(12.f, 1500.f, 10.5f, 250.f, 500.f, 10)));
	//m_available_cars.push_back(SCarDesc(new CarDesc(12.285f, 1500.f, 10.5f, 250.f, 500.f, 10)));
//	m_available_cars.push_back(SCarDesc(new CarDesc(10.000f, 1400.f, 10.5f, 250.f, 500.f, 1)));
//	m_available_cars.push_back(SCarDesc(new CarDesc(12.825f, 1410.f, 10.5f, 250.f, 500.f, 1)));
//	m_available_cars.push_back(SCarDesc(new CarDesc(12.000f, 1110.f, 10.5f, 250.f, 500.f, 1)));
//	m_available_cars.push_back(SCarDesc(new CarDesc(12.000f,  900.f, 10.5f, 250.f, 500.f, 1)));
//	m_available_cars.push_back(SCarDesc(new CarDesc(16.000f, 1870.f, 12.5f, 250.f, 660.f, 1)));
//	m_available_cars.push_back(SCarDesc(new CarDesc(17.123f, 1500.f, 12.5f, 250.f, 660.f, 1)));
//	m_available_cars.push_back(SCarDesc(new CarDesc(17.825f, 1380.f, 12.5f, 250.f, 660.f, 10)));

	m_available_cars.push_back(SCarDesc(new CarDesc(17.000f, 1500.f, 12.5f, 250.f, 660.f, 30)));
	m_available_cars.push_back(SCarDesc(new CarDesc(12.000f, 1500.f, 10.5f, 250.f, 500.f, 30)));

	read_points(m_destination_points);

	m_points.reserve(m_destination_points.size());
	for (const DestinationPoint& destination_point : m_destination_points)
		m_points.push_back(destination_point.position);

	SOsrmMatrix matrix = read_matrix("matrix.json", m_destination_points.size());
	if (matrix) build_best_plan(matrix);
	else
	{
		m_osrm_manager.reset(new OsrmManager(m_network_manager));
		connect(m_osrm_manager.get(), SIGNAL(request_routes_finished(bool, const SOsrmMatrix&)), SLOT(slot_request_routes_finished(bool, const SOsrmMatrix&)));
		m_osrm_manager->request_routes(m_points);
	}
}
void Shell::activate()
{
	m_main_window->show();
}
void Shell::slot_timer_updated(float dt)
{
	m_map_viewer.update(dt);
}
void Shell::slot_request_routes_finished(bool success, const SOsrmMatrix& matrix)
{
	write_matrix("matrix.json", *matrix);
	build_best_plan(matrix);
}
void Shell::read_points(destination_points_t& destination_points) const
{
	destination_points.push_back(DestinationPoint(QPointF(60.7546, 56.7588), 0.0f, 0.f));
	QFile file("points.json");
	if (file.open(QIODevice::ReadOnly))
	{
		QJsonDocument json_doc = QJsonDocument::fromJson(file.readAll());
		QJsonArray doc_array = json_doc.array();
		for (const auto& v : doc_array)
		{
			QJsonObject json_obj = v.toObject();
			float lat = json_obj["lat"].toDouble();
			float lon = json_obj["lon"].toDouble();
			float volume = json_obj["volume"].toDouble();
			float weight = json_obj["weight"].toDouble();
			destination_points.push_back(DestinationPoint(QPointF(lon, lat), volume, weight));
		}

		/*QString buffer;
		for (const DestinationPoint& dest_point : destination_points)
		{
			buffer += QString().sprintf("	solver.add_destination_point(%.6f, %.6f, %.4f, %.4f);\n", dest_point.position.x(), dest_point.position.y(), dest_point.volume, dest_point.weight);
		}
		int a = 0;*/
	}
}

static std::array<QColor, 12> s_colors = {
	Qt::red,
	Qt::yellow,
	Qt::blue,
	Qt::green,
	Qt::darkMagenta,
	Qt::magenta,
	Qt::black,
	Qt::cyan,
	Qt::darkGreen,
	Qt::darkRed,
	Qt::darkCyan,
	Qt::darkBlue,
};
static std::array<QString, 12> s_color_names = {
	"red",
	"yellow",
	"blue",
	"green",
	"darkGreen",
	"magenta",
	"black",
	"cyan",
	"darkMagenta",
	"darkRed",
	"darkCyan",
	"darkBlue",
};
void Shell::build_best_plan(const SOsrmMatrix& matrix)
{
	m_osrm_matrix = matrix;
	MatrixDistances matrix_distances = build_matrix_distances(*matrix);

	// # one thread
	//VrpSolver solver;
	//SPlan best_plan = solver.calculate(m_available_cars, m_destination_points, matrix_distances);
	//show_results(best_plan);

	// # many threads
	m_vrp_async_solver.reset(new VrpAsyncSolver(m_thread_task_manager, m_available_cars, m_destination_points, matrix_distances));
	connect(m_vrp_async_solver.get(), SIGNAL(finish(const SPlan&)), SLOT(slot_async_solver_finish(const SPlan&)));
	m_vrp_async_solver->calculate();
}
void Shell::slot_async_solver_finish(const SPlan& best_plan)
{
	show_results(best_plan);
}
void Shell::show_results(const SPlan& best_plan)
{
	m_main_window->add_msg(QString("count points: %1").arg(m_destination_points.size() - 1));
	m_main_window->add_msg(QString("plan fitness: %1").arg(best_plan->fitness()));
	m_main_window->add_msg(QString().sprintf("plan distance: %.3f", best_plan->distance() * 0.001f));
	m_main_window->add_msg("");

	for (size_t i = 0; i < best_plan->routes().size(); ++i)
	{
		const Route* route = best_plan->routes()[i];
		m_main_window->add_msg(QString("route: %1 %2").arg(s_color_names[i % s_color_names.size()]).arg(route->points().size()));
		//m_main_window->add_msg(QString("  fitness=%1\n  distance=%2\n  volume=%3\n  weight=%4").arg(route->fitness()).arg(route->distance()).arg(route->volume()).arg(route->weight()));
		//m_main_window->add_msg(QString("  car max_volume: %1").arg(route->car().car_desc->max_volume));
		//m_main_window->add_msg(QString("  car max_weight: %1").arg(route->car().car_desc->max_weight));
		//m_main_window->add_msg(QString("  num points: %1").arg(route->points().size()));
		m_main_window->add_msg(QString("  volume: %1 / %2").arg(route->volume()).arg(route->car().car_desc->max_volume));
		m_main_window->add_msg(QString("  weight: %1 / %2").arg(route->weight()).arg(route->car().car_desc->max_weight));
		m_main_window->add_msg(QString("  distance: %1").arg(route->distance() * 0.001f));
		m_main_window->add_msg(QString("  fitness: %1").arg(route->fitness()));
		m_main_window->add_msg("");
	}

	// render routes
	for (size_t i = 0; i < best_plan->routes().size(); ++i)
	{
		const Route* route = best_plan->routes()[i];

		vpositions_t positions = convert_route_points(route->points());
		SMapWaypoint map_waypoint = m_map_viewer.add_waypoint(positions, s_colors[i], Qt::PenStyle::DotLine, 1.6f);
		m_vmap_waypoints.push_back(map_waypoint);

		const Route::points_t& points = route->points();
		if (!points.empty())
		{
			// ������ ���� �� ������ �� ������ �����
			render_waypoint(*m_osrm_matrix, 0, points[0].index(), i);
			// � �����...
			for (size_t j = 0; j < points.size() - 1; ++j)
				render_waypoint(*m_osrm_matrix, points[j].index(), points[j + 1].index(), i);
		}
	}
	// render all points
	for (const QPointF& point : m_points) {
		SMapPoint map_point = m_map_viewer.add_point(point, QColor(0, 0, 0, 255), 8);
		m_map_points.push_back(map_point);
	}

	// render start points
	for (size_t i = 0; i < best_plan->routes().size(); ++i)
	{
		const Route* route = best_plan->routes()[i];
		const Route::points_t& points = route->points();
		if (!points.empty())
		{
			SMapPoint map_point = m_map_viewer.add_point(points[0].position(), s_colors[i], 11);
			m_map_points.push_back(map_point);
		}
	}
}
MatrixDistances Shell::build_matrix_distances(const OsrmMatrix& matrix) const
{
	int count = matrix.dimension();
	MatrixDistances matrix_distances(count);
	for (int i = 0; i < count; ++i)
		for (int j = 0; j < count; ++j)
			matrix_distances(i, j) = matrix(i, j).distance;
	return matrix_distances;
}
vpositions_t Shell::convert_route_points(const Route::points_t& route_points) const
{
	vpositions_t positions;
	positions.push_back(m_destination_points[0].position);
	for (const RoutePoint& route_point : route_points)
		positions.push_back(route_point.position());
	return positions;
}
void Shell::render_waypoint(const OsrmMatrix& matrix, int start_index, int end_index, int route_index)
{
	const OsrmRoute& osrm_route = matrix(start_index, end_index);

	OsrmRoute::waypoints_t waypoints;
	int step = osrm_route.waypoints.size() / 1;
	step = std::max(1, step);
	for (size_t k = 0; k < osrm_route.waypoints.size(); k += step)
		waypoints.push_back(osrm_route.waypoints[k]);

	SMapWaypoint map_waypoint = m_map_viewer.add_waypoint(osrm_route.waypoints, s_colors[route_index % s_colors.size()], Qt::PenStyle::SolidLine, std::max(3.f, (5.7f - route_index*1.1f)));
	m_vmap_waypoints.push_back(map_waypoint);
}