//=====================================================================================//
//   Author: open
//   Date:   31.07.2018
//=====================================================================================//
#ifndef SHELL_H
#define SHELL_H
#include "osrm_manager/osrm_manager.h"
#include "osrm_manager/osrm_route_matrix.h"
#include "vrp_solver/vrp_solver.h"
#include "map_viewer/map_viewer.h"
#include "base/network_manager.h"
#include "main_window.h"
#include "base/timer.h"
#include "vrp_solver/vrp_async_solver.h"
#include "base/thread_task_manager.h"

class Shell : public QObject
{
	Q_OBJECT
private:
	MapViewer m_map_viewer;
	std::unique_ptr<MainWindow> m_main_window;

	NetworkManager m_network_manager;
	ThreadTaskManager m_thread_task_manager;

	using UOsrmManager = std::unique_ptr<OsrmManager>;
	UOsrmManager m_osrm_manager;

	using vmap_points_t = std::vector<SMapPoint>;
	vmap_points_t m_map_points;

	using vmap_waypoints_t = std::vector<SMapWaypoint>;
	vmap_waypoints_t m_vmap_waypoints;

	Base::Timer m_timer;

	destination_points_t m_destination_points;
	available_cars_t m_available_cars;
	SOsrmMatrix m_osrm_matrix;

	vpositions_t m_points;

	using UVrpAsyncSolver = std::unique_ptr<VrpAsyncSolver>;
	UVrpAsyncSolver m_vrp_async_solver;

public:
	Shell();
	void activate();

private slots:
	void slot_timer_updated(float dt);
	void slot_request_routes_finished(bool success, const SOsrmMatrix& matrix);
	void slot_async_solver_finish(const SPlan& best_plan);

private:
	MatrixDistances build_matrix_distances(const OsrmMatrix& matrix) const;
	vpositions_t convert_route_points(const Route::points_t& route_points) const;
	void render_waypoint(const OsrmMatrix& matrix, int start_index, int end_index, int route_index);
	void build_best_plan(const SOsrmMatrix& matrix);
	void read_points(destination_points_t& destination_points) const;
	void show_results(const SPlan& best_plan);
};

#endif