//=====================================================================================//
//   Author: open
//   Date:   11.08.2018
//=====================================================================================//
#ifndef VRP_ASYNC_SOLVER_H
#define VRP_ASYNC_SOLVER_H
#include "vrp_solver/vrp_data.h"
#include "base/thread_task_manager.h"
#include "vrp_solver/vrp_create_population_task.h"
#include "vrp_solver/vrp_crossover_task.h"
#include "vrp_solver/vrp_mutation_task.h"

class VrpAsyncSolver : public QObject
{
	Q_OBJECT
private:
	ThreadTaskManager& m_thread_task_manager;
	available_cars_t m_available_cars;
	destination_points_t m_destination_points;
	MatrixDistances m_matrix_distances;
	boost::random::mt19937 m_rand_gen;

	using plans_t = std::vector<SPlan>;
	plans_t m_plans;
	using create_population_plans_t = std::vector<plans_t>;
	create_population_plans_t m_create_population_plans;
	using crossover_plans_t = std::vector<plans_t>;
	crossover_plans_t m_crossover_plans;
	using mutate_plans_t = std::vector<plans_t>;
	mutate_plans_t m_mutate_plans;

	using create_population_tasks_t = std::vector<SVrpCreatePopulationTask>;
	create_population_tasks_t m_create_population_tasks;
	int m_num_create_population_tasks_finished;
	int m_epoch_index;
	const int m_population_size;
	const int m_epoch_count;

	using crossover_tasks_t = std::vector<SVrpCrossoverTask>;
	crossover_tasks_t m_crossover_tasks;
	int m_num_crossover_tasks_finished;

	using mutation_tasks_t = std::vector<SVrpMutationTask>;
	mutation_tasks_t m_mutation_tasks;
	int m_num_mutation_tasks_finished;

	ThreadTaskManager::tasks_t m_thread_tasks;

	float m_last_best_fitness;
	int m_halt_count;

public:
	VrpAsyncSolver(ThreadTaskManager& thread_task_manager, const available_cars_t& available_cars, const destination_points_t& destination_points, const MatrixDistances& matrix_distances);
	void calculate();

signals:
	void finish(const SPlan& plan);

private:
	void slot_finish_create_population(int index, const VrpCreatePopulationTask::result_plans_t& result_plans);
	void slot_finish_crossover(int index, const VrpCrossoverTask::result_plans_t& result_plans);
	void slot_finish_mutation(int index, const VrpMutationTask::result_plans_t& result_plans);

private:
	void create_population();
	void next_epoch();
	void check_epoch_finished();
	bool is_finish(const SPlan& best_epoch_plan);
};

#endif