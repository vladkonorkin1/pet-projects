//=====================================================================================//
//   Author: open
//   Date:  11.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "vrp_create_population_task.h"
#include "vrp_insert_point.h"
#include "base/random_indices.h" 

VrpCreatePopulationTask::VrpCreatePopulationTask(std::uint32_t seed, int population_size, const available_cars_t& available_cars, const destination_points_t& destination_points, const MatrixDistances& matrix_distances)
: m_destination_points(destination_points)
, m_matrix_distances(matrix_distances)
, m_population_size(population_size)
, m_garage(available_cars, seed)
, m_seed(seed)
{
}
void VrpCreatePopulationTask::on_process()
{
	RandomIndices random_indices(1, m_destination_points.size() - 1, m_seed);
	for (int i = 0; i < m_population_size; ++i)
		m_result_plans.push_back(build_plan(random_indices));
}
void VrpCreatePopulationTask::on_finish_process()
{
	emit finish(m_result_plans);
}
SPlan VrpCreatePopulationTask::build_plan(RandomIndices& random_indices)
{
	// ���������� ������������������ ��������� �����
	m_garage.reset();
	// ���������� ����� ��������������� ������������������ �����
	random_indices.reset_random();

	SPlan plan(new Plan());
	// ���������� ��� �����
	for (int index = random_indices.get_random_index(); index != RandomIndices::Null; index = random_indices.get_random_index())
		vrp_insert_point(*plan, index, m_destination_points, m_garage, m_matrix_distances);
	plan->update_distance_and_fitness();
	return plan;
}