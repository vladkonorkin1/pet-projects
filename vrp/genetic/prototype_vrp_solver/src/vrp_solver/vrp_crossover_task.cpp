//=====================================================================================//
//   Author: open
//   Date:  11.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "vrp_crossover_task.h"
#include "vrp_insert_point.h"

VrpCrossoverTask::VrpCrossoverTask(const plans_t& all_plans, const Garage& garage, const destination_points_t& destination_points, const MatrixDistances& matrix_distances)
: m_all_plans(all_plans)
, m_garage(garage)
, m_destination_points(destination_points)
, m_matrix_distances(matrix_distances)
, m_processed_points(destination_points.size())
{
}
// ��������� ����� �� ���������
void VrpCrossoverTask::add_base_plan(const Plan* plan)
{
	m_base_plans.push_back(plan);
}
void VrpCrossoverTask::on_process()
{
	m_result_plans.clear();
	for (const Plan* plan : m_base_plans)
		m_result_plans.push_back(crossover(plan));

	// ������� ��� ������������ �����
	m_base_plans.clear();
}
void VrpCrossoverTask::on_finish_process()
{
	emit finish(m_result_plans);
}
// "�����������" �������
SPlan VrpCrossoverTask::crossover(const Plan* original_plan)
{
	m_garage.reset();
	SPlan new_plan(new Plan());

	// ���-�� �������� ��� ����������
	int num_mixing = 2;
	//int num_mixing = rand_gen() % 3 + 2;		// 2, 3, 4

	std::array<const Plan*, 4> merged_plans = { nullptr };
	merged_plans[0] = original_plan;
	for (int i = 1; i < num_mixing; ++i)
		merged_plans[i] = m_all_plans[m_rand_gen() % m_all_plans.size()].get();

	m_merged_routes.clear();
	// ���������� ��� �������
	for (int i = 0; i < num_mixing; ++i)
		for (const Route* route : merged_plans[i]->routes())
			m_merged_routes.push_back(route);

	// �������������, ��� ������ ����� �� ����������
	for (size_t i = 0; i < m_processed_points.size(); ++i)
		m_processed_points[i] = false;

	while (m_merged_routes.size() > 0)
	{
		// �������� ��������� �������
		int route_index = m_rand_gen() % m_merged_routes.size();
		const Route* route = m_merged_routes[route_index];
		// ����������� ������ �� ������
		m_garage.take_car(route->car());

		// ��������� ������� � ����
		Route* new_route = new_plan->add_route(route->car());
		//*new_route = *route;

		{
			int end = m_rand_gen() % route->points().size();
			end++;
			int start = m_rand_gen() % end;
			
			for (int i = start; i < end; ++i)
				new_route->add_point(route->points()[i]);

			new_route->update_distance(m_matrix_distances);
		}

		// �������� �����, ������� ����������
		for (const RoutePoint& route_point : new_route->points())
		{
			assert( m_processed_points[route_point.index()] == false );
			m_processed_points[route_point.index()] = true;
		}
		// ������� ���
		remove_merged_route(route_index);

		// ����������� ��� ��������, � ������� ���� ������ ������
		for (size_t i = 0; i < m_merged_routes.size(); )
		{
			if (new_route->car() == m_merged_routes[i]->car())
				remove_merged_route(i);
			else i++;
		}

		// ����������� ��� ��������, � ������� ����������� ����� �� �������� ��������
		for (size_t i = 0; i < m_merged_routes.size(); )
		{
			if (new_route->is_have_points(m_merged_routes[i]))
				remove_merged_route(i);
			else i++;
		}
	}

	// ������� �������������� ����� � ��������� �� � �������� �������� �����
	/*for (size_t i = 1; i < m_processed_points.size(); ++i)
	{
		if (!m_processed_points[i])
			vrp_insert_point(*new_plan, i, m_destination_points, m_garage, m_matrix_distances);
	}*/

	m_indices.clear();
	for (size_t i = 1; i < m_processed_points.size(); ++i)
	{
		if (!m_processed_points[i])
			m_indices.push_back(i);
			//vrp_insert_point(*new_plan, i, m_destination_points, m_garage, m_matrix_distances);
	}
	{
		size_t count = m_indices.size();
		for (size_t i = 0; i < count; ++i)
			std::swap(m_indices[i], m_indices[m_rand_gen() % count]);
	}
	for (int index : m_indices)
		vrp_insert_point(*new_plan, index, m_destination_points, m_garage, m_matrix_distances);

		
	new_plan->update_distance_and_fitness();
	return new_plan;
}
// ������� ������� �� ������������
void VrpCrossoverTask::remove_merged_route(int index)
{
	size_t new_size = m_merged_routes.size() - 1;
	// ������������ ��������� ������� �� ����� ���������
	m_merged_routes[index] = m_merged_routes[new_size];
	// ��������� ������
	m_merged_routes.resize(new_size);
}