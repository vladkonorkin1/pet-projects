//=====================================================================================//
//   Author: open
//   Date:   11.08.2018
//=====================================================================================//
#ifndef VRP_CROSSOVER_TASK_H
#define VRP_CROSSOVER_TASK_H
#include "vrp_solver/vrp_data.h"
#include "base/thread_task.h"

class VrpCrossoverTask : public ThreadTask
{
	Q_OBJECT
public:
	using result_plans_t = std::vector<SPlan>;

private:
	const destination_points_t& m_destination_points;
	const MatrixDistances& m_matrix_distances;
	
	using plans_t = std::vector<SPlan>;
	const plans_t& m_all_plans;					// ��� ����� ���������
	Garage m_garage;							// ����� � ��������
	std::vector<const Plan*> m_base_plans;		// ������� �����
	result_plans_t m_result_plans;				// ���������� �����
	std::vector<const Route*> m_merged_routes;	// ������������ ��������
	std::vector<bool> m_processed_points;		// ������������ �����
	boost::random::mt19937 m_rand_gen;

	std::vector<int> m_indices;

public:
	//VrpCrossoverTask(const plans_t& all_plans, const available_cars_t& available_cars, const destination_points_t& destination_points, const MatrixDistances& matrix_distances);
	VrpCrossoverTask(const plans_t& all_plans, const Garage& garage, const destination_points_t& destination_points, const MatrixDistances& matrix_distances);
	// ��������� ����� �� ���������
	void add_base_plan(const Plan* plan);
	// ���������� ��������� seed
	void set_seed(std::uint32_t seed) { m_rand_gen.seed(seed); }
	
	// !!! not in main thread
	virtual void on_process();
	// in main thread!!!
	virtual void on_finish_process();

signals:
	void finish(const VrpCrossoverTask::result_plans_t& result_plans);

private:
	// "�����������" �������
	SPlan crossover(const Plan* plan);
	// ������� ������� �� ������������
	void remove_merged_route(int index);
};

using SVrpCrossoverTask = std::shared_ptr<VrpCrossoverTask>;

#endif