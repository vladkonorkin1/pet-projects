//=====================================================================================//
//   Author: open
//   Date:   03.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "vrp_data.h"

bool Route::is_may_add_point(const DestinationPoint& destination_point) const
{
	if (m_points.size() >= 35)
		return false;

	float new_volume = m_volume + destination_point.volume;
	float new_weight = m_weight + destination_point.weight;
	return new_volume < m_car.car_desc->max_volume && new_weight < m_car.car_desc->max_weight;
}
void Route::insert_point(const RoutePoint& route_point, int insert_index)
{
	m_volume += route_point.volume();
	m_weight += route_point.weight();
	m_points.insert(m_points.begin() + insert_index, route_point);
}
void Route::add_point(const RoutePoint& route_point)
{
	m_volume += route_point.volume();
	m_weight += route_point.weight();
	m_points.push_back(route_point);
}
void Route::update_distance(const MatrixDistances& matrix_distances)
{
	m_distance = calculate_distance(matrix_distances);
	//no_stock_distance = calculate_distance(matrix_distances, true);
}
float Route::calculate_distance(const MatrixDistances& matrix_distances) const
{
	if (!m_points.empty())
	{
		// ������� ���� �� ������ �� ������ �����
		float distance = matrix_distances(0, m_points[0].index());
		//float distance = 0.f;

		// ������� ��������� ����� �������
		for (int i = 0; i < static_cast<int>(m_points.size()) - 1; ++i)
			distance += matrix_distances(m_points[i].index(), m_points[i + 1].index());

		// ���� ������ ����, �� ������� ���� �� ������
		//if (!ignore_our_car)
		//{
		//	if (m_car->our)
		distance += matrix_distances(m_points[m_points.size() - 1].index(), 0);
		//}
		return distance;
	}
	return 0.f;
}
float Route::fitness() const
{
	return fitness(m_distance);
}
float Route::fitness(float distance) const
{
	return (distance*0.001f * m_car.car_desc->cost_km) + (m_points.size() * m_car.car_desc->cost_destination_point) + m_car.car_desc->cost_trip;
}
bool Route::is_have_point_index(int index) const
{
	for (const RoutePoint& route_point : m_points)
	{
		if (route_point.index() == index)
			return true;
	}
	return false;
}
bool Route::is_have_points(const Route* route) const
{
	//for (size_t i = 1; i < m_points.size(); ++i)
	for (const RoutePoint& route_point : m_points)
	{
		//const RoutePoint& route_point = m_points[i];
		int index = route_point.index();

		if (route->is_have_point_index(index))
			return true;
	}
	return false;
}
void Route::remove_point(int index)
{
	m_points.erase(m_points.begin() + index);
}
float Route::weight() const
{
	float weight = 0.f;
	for (const RoutePoint& route_point : m_points)
		weight += route_point.weight();
	return weight;
}
float Route::volume() const
{
	float volume = 0.f;
	for (const RoutePoint& route_point : m_points)
		volume += route_point.volume();
	return volume;
}


Plan::~Plan()
{
	std::for_each(m_routes.begin(), m_routes.end(), [](Route* route) { delete route; });
}
Route* Plan::add_route(const Car& car)
{
	Route* route = new Route(car);
	m_routes.push_back(route);
	return route;
}
void Plan::remove_route(int index)
{
	assert( index >= 0 && index < static_cast<int>(m_routes.size()) );
	routes_t::iterator it = m_routes.begin() + index;
	delete *it;
	m_routes.erase(it);
}
void Plan::update_distance_and_fitness()
{
	m_distance = 0.f;
	for (const Route* route : m_routes)
		m_distance += route->distance();

	//no_stock_distance = 0.f;
	//for (const Route* route : m_routes)
	//	no_stock_distance += route->no_stock_distance;

	m_fitness = 0.f;
	for (const Route* route : m_routes)
		m_fitness += route->fitness();
}


Garage::Garage(const available_cars_t& available_cars, std::uint32_t seed)
: m_rand_gen(seed)
{
	for (const SCarDesc& car_desc : available_cars)
		if (car_desc->available_count > 0)
			m_available_cars.push_back(car_desc);
	
	for (size_t i = 0; i < m_available_cars.size(); ++i)
		m_available_cars[i]->type = i;
}
void Garage::reset()
{
	m_cars.clear();
	for (const SCarDesc& car_desc : m_available_cars)
		m_cars.push_back(CarCount{ car_desc, car_desc->available_count });
}
void Garage::take_car(const Car& car)
{
	int index = car.car_desc->type;

	//if (index >= m_cars.size())
	//	__nop();

	assert( index >= 0 );
	cars_t::iterator it = std::find_if(m_cars.begin(), m_cars.end(), [index](const CarCount& car_count) { return car_count.car_desc->type == index; });
	if (m_cars.end() != it)
	{
		//CarCount& car_count = m_cars[index];
		CarCount& car_count = *it;
		assert(car_count.count > 0);
		car_count.count--;

		if (car_count.count <= 0)
			m_cars.erase(it);
	}
	else
		assert( 0 );
}
bool Garage::is_have_car() const
{
	return !m_cars.empty();
}
Car Garage::get_random_car()
{
	if (!m_cars.empty())
	{
		int index = m_rand_gen() % m_cars.size();
		
		CarCount& car_count = m_cars[index];
		assert( car_count.count > 0 );
		
		Car car{ car_count.car_desc, car_count.car_desc->available_count - car_count.count };
		car_count.count--;

		if (car_count.count <= 0)
			m_cars.erase(m_cars.begin() + index);
		
		return car;
	}

	assert( 0 );
	static Car s_temp_car{ nullptr, -1 };
	return s_temp_car;
}