//=====================================================================================//
//   Author: open
//   Date:   03.08.2018
//=====================================================================================//
#ifndef VRP_DATA_H
#define VRP_DATA_H
#include "base/dynamic_matrix.h"
#include <QPointF>

using MatrixDistances = DynamicMatrix<float>;


class CarDesc
{
public:
	int type = -1;					// ��� ������
	float max_volume;				// ������������ �����
	float max_weight;				// ����������������
	float cost_km;					// ��������� ��
	float cost_destination_point;	// ��������� �����
	float cost_trip;				// ��������� �����
	int available_count;

	CarDesc(float max_volume, float max_weight, float cost_km, float cost_destination_point, float cost_trip, int available_count)
	: max_volume(max_volume)
	, max_weight(max_weight)
	, cost_km(cost_km)
	, cost_destination_point(cost_destination_point)
	, cost_trip(cost_trip)
	, available_count(available_count)
	{
	}
};

using SCarDesc = std::shared_ptr<CarDesc>;
using available_cars_t = std::vector<SCarDesc>;

class Car
{
public:
	SCarDesc car_desc;
	int index;
};

inline bool operator == (const Car& left_car, const Car& right_car) {
	return left_car.car_desc == right_car.car_desc && left_car.index == right_car.index;
}


class DestinationPoint
{
public:
	QPointF position;

	float volume;	// �����
	float weight;	// ���

	//float summa;	// �����
	// ��������� ����
	
	DestinationPoint() {}
	DestinationPoint(const QPointF& position, float volume, float weight) : position(position), volume(volume), weight(weight) {}
};
using destination_points_t = std::vector<DestinationPoint>;


class RoutePoint
{
private:
	int m_index;
	const DestinationPoint* m_destination_point;
	// temp vars
public:
	RoutePoint() {}
	RoutePoint(int index, const DestinationPoint* destination_point) : m_index(index), m_destination_point(destination_point)
	{
	}
	const QPointF& position() const { return m_destination_point->position; }
	int index() const { return m_index; }
	float volume() const { return m_destination_point->volume; }
	float weight() const { return m_destination_point->weight; }
};


class Route
{
public:
	using points_t = std::vector<RoutePoint>;

private:
	points_t m_points;

	Car m_car;
	float m_volume = 0.f;			// ������� �������� �������
	float m_weight = 0.f;			// ������� �������� �����
	float m_distance = 0.f;

public:
	Route(const Car& car) : m_car(car) {}
	
	bool is_may_add_point(const DestinationPoint& destination_point) const;
	void insert_point(const RoutePoint& route_point, int insert_index);
	void add_point(const RoutePoint& route_point);
	const points_t& points() const { return m_points; }
	bool is_have_points(const Route* route) const;
	void remove_point(int index);

	void update_distance(const MatrixDistances& matrix_distances);
	float distance() const { return m_distance; }
	
	float fitness(float distance) const;
	float fitness() const;

	float weight() const;
	float volume() const;

	const Car& car() const { return m_car; }

	bool is_have_point_index(int index) const;

private:
	float calculate_distance(const MatrixDistances& matrix_distances) const;
};


class Plan
{
public:
	using routes_t = std::vector<Route*>;
private:
	routes_t m_routes;
	float m_distance = 0.f;
	float m_fitness = 0.f;

//public:
	//float no_stock_distance = 0.f;

public:
	Plan() {}
	Plan(const Plan& src)
	: m_distance(src.m_distance)
	, m_fitness(src.m_fitness)
	{
		for (const Route* route : src.m_routes)
		{
			Route* new_route = new Route(route->car());
			*new_route = *route;
			m_routes.push_back(new_route);
		}
	}
	~Plan();
	Route* add_route(const Car& car);
	void remove_route(int index);
	const routes_t& routes() const { return m_routes; }
	void update_distance_and_fitness();
	float fitness() const { return m_fitness; }
	float distance() const { return m_distance; }
};
using SPlan = std::shared_ptr<Plan>;


class Garage
{
private:
	available_cars_t m_available_cars;

	struct CarCount
	{
		SCarDesc car_desc;
		int count;
	};
	using cars_t = std::vector<CarCount>;
	cars_t m_cars;

	boost::random::mt19937 m_rand_gen;

public:
	Garage(const available_cars_t& available_cars, std::uint32_t seed);
	void reset();
	
	bool is_have_car() const;
	Car get_random_car();

	void take_car(const Car& car);
};

#endif