//=====================================================================================//
//   Author: open
//   Date:  11.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "vrp_insert_point.h"

float calc_distance(int insert_index, int point_index, const Route* route, const float current_distance, const MatrixDistances& matrix_distances)
{
	const Route::points_t& points = route->points();

	float distance = current_distance;
	if (insert_index == 0)
	{
		int first_index = points[0].index();
		// ���� ����������� ������� ����� � ����� ������ ����� ������
		float old_distance = matrix_distances(0, first_index);
		distance -= old_distance;

		float new_distance = matrix_distances(0, point_index) + matrix_distances(point_index, first_index);
		distance += new_distance;
	}
	else if (insert_index == points.size())
	{
		int last_index = points[points.size() - 1].index();
		// ���� �������� � �����
		//if (route->car()->our)
		//{
		float old_distance = matrix_distances(last_index, 0);
		distance -= old_distance;

		float new_distance = matrix_distances(last_index, point_index) + matrix_distances(point_index, 0);
		distance += new_distance;
		//}
		//else
		//{
		//			float new_distance = m_matrix_distances(last_index, point_index);
		//			distance += new_distance;
		//}
	}
	else
	{
		int current_index = points[insert_index - 1].index();
		int next_index = points[insert_index].index();

		float old_distance = matrix_distances(current_index, next_index);
		distance -= old_distance;

		float new_distance = matrix_distances(current_index, point_index) + matrix_distances(point_index, next_index);
		distance += new_distance;
	}
	return distance;
}

// �������� ����� � ����
void vrp_insert_point(Plan& plan, int index, const destination_points_t& destination_points, Garage& garage, const MatrixDistances& matrix_distances)
{
	const DestinationPoint& destination_point = destination_points[index];
	Route* best_route = nullptr;
	float best_fitness_difference = std::numeric_limits<float>::max();
	int best_insert_index = 0;
	// �������� �� ���� ���������
	for (Route* route : plan.routes())
	{
		// ���� ����� ��� �������� ����
		if (route->is_may_add_point(destination_point))
		{
			// ���� ������� ������� �������-������� �� �������
			for (int i = 0; i < static_cast<int>(route->points().size()) + 1; ++i)
			{
				// ���� ������ ����� ��� ������� ����� ������� ��������
				float new_distance = calc_distance(i, index, route, route->distance(), matrix_distances);
				float new_fitness = route->fitness(new_distance);
				float fitness_difference = new_fitness - route->fitness();

				if (fitness_difference < best_fitness_difference)
				{
					best_fitness_difference = fitness_difference;
					best_insert_index = i;
					best_route = route;
				}
			}
		}
	}
	RoutePoint route_point(index, &destination_point);

	if (best_route)
		best_route->insert_point(route_point, best_insert_index);
	else
	{
		if (garage.is_have_car())
		{
			Car car = garage.get_random_car();
			// ���� �� ����� ������� � ������� ����� �������� �����, �� ������ ����� �������
			best_route = plan.add_route(car);
			// ��������� ����� ������
			//best_route->insert_point(RoutePoint(0, &destination_points[0]), 0);

			// ��������� �� �����������
			if (!best_route->is_may_add_point(destination_point))
				logs("order > car volume or weight");	// ��������� ���������� �����/��� ���� ������!!!

			best_route->insert_point(route_point, 0);
		}
		else
		{
			// ������ ���������
		}
	}
	if (best_route)
		best_route->update_distance(matrix_distances);
}