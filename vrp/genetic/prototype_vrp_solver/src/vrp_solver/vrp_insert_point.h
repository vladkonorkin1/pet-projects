//=====================================================================================//
//   Author: open
//   Date:   11.08.2018
//=====================================================================================//
#ifndef VRP_INSERT_POINT_H
#define VRP_INSERT_POINT_H
#include "vrp_solver/vrp_data.h"

// �������� ����� � ����
extern void vrp_insert_point(Plan& plan, int index, const destination_points_t& destination_points, Garage& garage, const MatrixDistances& matrix_distances);

#endif