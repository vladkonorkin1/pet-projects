//=====================================================================================//
//   Author: open
//   Date:  11.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "vrp_mutation_task.h"
#include "vrp_insert_point.h"

VrpMutationTask::VrpMutationTask(const Garage& garage, const destination_points_t& destination_points, const MatrixDistances& matrix_distances)
: m_garage(garage)
, m_destination_points(destination_points)
, m_matrix_distances(matrix_distances)
{
}
// ��������� ����� �� ���������
void VrpMutationTask::add_base_plan(const Plan* plan)
{
	m_base_plans.push_back(plan);
}
// !!! not in main thread
void VrpMutationTask::on_process()
{
	m_result_plans.clear();
	for (const Plan* plan : m_base_plans)
	{
		m_result_plans.push_back(mutate_remove_random_routes(plan));
		m_result_plans.push_back(mutate_remove_random_points(plan));
	}
	// ������� ��� ������������ �����
	m_base_plans.clear();
}
// in main thread!!!
void VrpMutationTask::on_finish_process()
{
	emit finish(m_result_plans);
}
SPlan VrpMutationTask::mutate_remove_random_routes(const Plan* original_plan)
{
	SPlan new_plan(new Plan(*original_plan));
	m_garage.reset();
	m_indices.clear();

	assert( !new_plan->routes().empty() );
	int num_deleted_routes = (m_rand_gen() % (new_plan->routes().size() - 1)) + 1;
	for (int i = 0; i < num_deleted_routes; ++i)
	{
		const Plan::routes_t& routes = new_plan->routes();
		// �������� ���� ��������� �������
		int route_index = m_rand_gen() % routes.size();
		// �������� ������� �����
		for (const RoutePoint& route_point : routes[route_index]->points())
			m_indices.push_back(route_point.index());
		// ������� �������
		new_plan->remove_route(route_index);
	}
	// ������� �� ������ ������
	for (const Route* route : new_plan->routes())
		m_garage.take_car(route->car());
	
	// ������������ �����
	{
		size_t count = m_indices.size();
		for (size_t i = 0; i < count; ++i)
			std::swap(m_indices[i], m_indices[m_rand_gen() % count]);
	}
	// ��������� ����� � ��������� �����
	for (int point_index : m_indices)
		vrp_insert_point(*new_plan, point_index, m_destination_points, m_garage, m_matrix_distances);

	new_plan->update_distance_and_fitness();
	return new_plan;
}
SPlan VrpMutationTask::mutate_remove_random_points(const Plan* original_plan)
{
	SPlan new_plan(new Plan(*original_plan));

	m_garage.reset();
	m_indices.clear();
	const Plan::routes_t& routes = new_plan->routes();

	// ���-�� ����� ��� ��������
	int num_remove_points = (m_rand_gen() % (routes.size() * 1) - 1) + 1;
	//int num_remove_points = (m_rand_gen() % num_average_points - 1) + 1;
	for (int i = 0; i < num_remove_points; ++i)
	{
		if (routes.empty()) break;

		int route_index = m_rand_gen() % (routes.size() * 1);
		Route* route = routes[route_index];
		const Route::points_t& points = route->points();
		assert( !points.empty() );

		int point_index = m_rand_gen() % points.size();
		m_indices.push_back(points[point_index].index());

		// ������� �����
		route->remove_point(point_index);
		if (route->points().empty())
		{
			// ���� ����� ������ ���, �� ������� �������
			new_plan->remove_route(route_index);
		}
	}

	// ������� �� ������ ������
	for (const Route* route : routes)
		m_garage.take_car(route->car());

	// ������������ �����
	{
		size_t count = m_indices.size();
		for (size_t i = 0; i < count; ++i)
			std::swap(m_indices[i], m_indices[m_rand_gen() % count]);
	}
	// ��������� ����� � ��������� �����
	for (int point_index : m_indices)
		vrp_insert_point(*new_plan, point_index, m_destination_points, m_garage, m_matrix_distances);

	new_plan->update_distance_and_fitness();
	return new_plan;
}