//=====================================================================================//
//   Author: open
//   Date:   03.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "vrp_solver.h"
#include "vrp_insert_point.h"

SPlan VrpSolver::calculate(const available_cars_t& available_cars, const destination_points_t& destination_points, const MatrixDistances& matrix_distances)
{
	SPlan best_plan;
	if (!available_cars.empty() && !destination_points.empty())
	{
		m_matrix_distances = matrix_distances;

		const int max_iteration = 500;
		const int population_size = 75000;
		m_plans.reserve(population_size * 3);
		Garage garage(available_cars, std::rand());
		create_population(population_size, garage, destination_points);
		std::sort(m_plans.begin(), m_plans.end(), [](const SPlan& left, const SPlan& right) { return left->fitness() < right->fitness(); });
		
		float last_fitness = 0.f;
		int halt_count = 0;
		plans_t crossover_plans;
		plans_t mutate_plans;
		for (int i = 0; i < max_iteration; ++i)
		{
			// ����������
			crossover_plans.clear();
			for (const SPlan& plan : m_plans)
				crossover_plans.push_back(crossover(plan, garage, destination_points));

			// �������� �������
			mutate_plans.clear();
			std::vector<int> cache_point_indices;	// ������ ��� ��� �����
			for (const SPlan& plan : m_plans)
			{
				SPlan mutate_plan1 = mutate1(plan, cache_point_indices, destination_points, garage);
				if (mutate_plan1) mutate_plans.push_back(mutate_plan1);

				SPlan mutate_plan2 = mutate2(plan, cache_point_indices, destination_points, garage);
				if (mutate_plan2) mutate_plans.push_back(mutate_plan2);
			}

			// ���������� �������� �������
			for (const SPlan& plan : crossover_plans)
				m_plans.push_back(plan);

			for (const SPlan& plan : mutate_plans)
				m_plans.push_back(plan);

			// ��������� �� �������-�������
			std::sort(m_plans.begin(), m_plans.end(), [](const SPlan& left, const SPlan& right) { return left->fitness() < right->fitness(); });
			// ��������������� ������ ��������� �� ��������������� �������! ������ �����... ���������������
			m_plans.resize(population_size);

			const SPlan& best_epoch_plan = m_plans[0];
			float percent_epoch = (float)i/(float)max_iteration*100.f;
			logs(QString().sprintf("epoch: %d %.0f%% %.1f %.2f", i, percent_epoch, best_epoch_plan->distance(), best_epoch_plan->fitness()));

			
			if (best_epoch_plan->fitness() != last_fitness)
			{
				last_fitness = best_epoch_plan->fitness();
				halt_count = 0;
			}
			else ++halt_count;

			if (halt_count > 40) break;
		}
	}
	best_plan = m_plans[0];
	return best_plan;
}
// ������� ���������
void VrpSolver::create_population(int population_size, Garage& garage, const destination_points_t& destination_points)
{
	RandomIndices random_indices_points(1, destination_points.size() - 1, std::rand());
	for (int i = 0; i < population_size; ++i)
	{
		m_plans.push_back(build_plan(garage, destination_points, random_indices_points));
		if ((i % 2000) == 0 || i == (population_size-1)) logs(QString().sprintf("create population: %.1f%%", ((float)i/(float)population_size*100.f)));
	}
}
SPlan VrpSolver::build_plan(Garage& garage, const destination_points_t& destination_points, RandomIndices& random_indices_points) const
{
	// ���������� ������������������ ��������� �����
	garage.reset();
	// ���������� ����� ��������������� ������������������ �����
	random_indices_points.reset_random();

	SPlan plan(new Plan());
	// ���������� ��� �����
	for (int index = random_indices_points.get_random_index(); index != RandomIndices::Null; index = random_indices_points.get_random_index())
		vrp_insert_point(*plan, index, destination_points, garage, m_matrix_distances);
	plan->update_distance_and_fitness();
	return plan;
}
// "�����������" �������
SPlan VrpSolver::crossover(const SPlan& plan, Garage& garage, const destination_points_t& destination_points)
{
	garage.reset();
	SPlan new_plan(new Plan());

	// ���-�� �������� ��� ����������
	int num_mixing = 2;
	//int num_mixing = std::rand() % 3 + 2;		// 2, 3, 4

	std::array<Plan*, 4> plans = { nullptr };
	plans[0] = plan.get();
	for (int i = 1; i < num_mixing; ++i)
		plans[i] = m_plans[std::rand() % m_plans.size()].get();

	// ���������� ������ ��� ������������� �������
	int merged_size = 0;
	for (int i = 0; i < num_mixing; ++i)
		merged_size += plans[i]->routes().size();

	std::vector<const Route*> merged_routes;
	merged_routes.reserve(merged_size);

	// ���������� ��� �������
	for (int i = 0; i < num_mixing; ++i)
		for (const Route* route : plans[i]->routes())
				merged_routes.push_back(route);

	// ������������ �����
	std::vector<bool> processed_points(destination_points.size(), false);

	while (merged_routes.size() > 0)
	{
		// �������� ��������� �������
		int route_index = std::rand() % merged_routes.size();
		const Route* route = merged_routes[route_index];
		// ����������� ������ �� ������
		garage.take_car(route->car());

		// ��������� ������� � ����
		const Route::points_t& route_points = route->points();
		Route* new_route = new_plan->add_route(route->car());
		*new_route = *route;

		// �������� �����, ������� ����������
		//for (const RoutePoint& route_point : route->points())
		for (size_t i = 0; i < route_points.size(); ++i)
		{
			assert( processed_points[route_points[i].index()] == false );
			processed_points[route_points[i].index()] = true;
		}

		// ��������� � ����� ������
		merged_routes[route_index] = merged_routes[merged_routes.size() - 1];
		merged_routes.resize(merged_routes.size() - 1);

		// ����������� ��� ��������, � ������� ���� ������ ������
		for (size_t i = 0; i < merged_routes.size(); )
		{
			//if (route->car()->type_id == merged_routes[i]->car()->type_id)
			if (route->car() == merged_routes[i]->car())
			{
				merged_routes[i] = merged_routes[merged_routes.size() - 1];
				merged_routes.resize(merged_routes.size() - 1);
			}
			else i++;
		}

		// ����������� ��� ��������, � ������� ����������� ����� �� �������� ��������
		for (size_t i = 0; i < merged_routes.size(); )
		{
			if (route->is_have_points(merged_routes[i]))
			{
				// ��������� � ����� ������
				merged_routes[i] = merged_routes[merged_routes.size() - 1];
				merged_routes.resize(merged_routes.size() - 1);
			}
			else i++;
		}
	}
	
	// ������� �������������� ����� � ��������� �� � �������� �������� �����
	for (size_t i = 1; i < processed_points.size(); ++i)
	{
		if (!processed_points[i])
			vrp_insert_point(*new_plan, i, destination_points, garage, m_matrix_distances);
	}
	new_plan->update_distance_and_fitness();
	return new_plan;
}
SPlan VrpSolver::mutate1(const SPlan& original_plan, std::vector<int>& cache_point_indices, const destination_points_t& destination_points, Garage& garage) const
{
	//if (plan->routes().size() <= 1)
	//	return;

	//const int percent_mutation = 25;
	//if (std::rand() % 100 < percent_mutation)
	{
		SPlan new_plan(new Plan(*original_plan));
		garage.reset();
		cache_point_indices.clear();
		
		assert( !new_plan->routes().empty() );
		int num_deleted_routes = (std::rand() % (new_plan->routes().size() - 1)) + 1;
		for (int i = 0; i < num_deleted_routes; ++i)
		{
			const Plan::routes_t& routes = new_plan->routes();
			// �������� ���� ��������� �������
			int route_index = std::rand() % routes.size();
			// �������� ������� �����
			for (const RoutePoint& route_point : routes[route_index]->points())
				cache_point_indices.push_back(route_point.index());
			// ������� �������
			new_plan->remove_route(route_index);
		}
		// ������� �� ������ ������
		for (const Route* route : new_plan->routes())
			garage.take_car(route->car());

		// ��������� ����� � ��������� �����
		for (int point_index : cache_point_indices)
			vrp_insert_point(*new_plan, point_index, destination_points, garage, m_matrix_distances);

		new_plan->update_distance_and_fitness();
		return new_plan;
	}
	return SPlan();
}
SPlan VrpSolver::mutate2(const SPlan& original_plan, std::vector<int>& cache_point_indices, const destination_points_t& destination_points, Garage& garage) const
{
	//int num_average_points = (destination_points.size() - 1) / plan->routes().size();
	//const int percent_mutation = 20;
	//if (std::rand() % 100 < percent_mutation)
	{
		SPlan new_plan(new Plan(*original_plan));

		garage.reset();
		cache_point_indices.clear();
		const Plan::routes_t& routes = new_plan->routes();

		// ���-�� ����� ��� ��������
		int num_remove_points = (std::rand() % (routes.size() * 1) - 1) + 1;
		//int num_remove_points = (std::rand() % num_average_points - 1) + 1;
		for (int i = 0; i < num_remove_points; ++i)
		{
			if (routes.empty()) break;
			
			int route_index = std::rand() % (routes.size() * 1);
			Route* route = routes[route_index];
			const Route::points_t& points = route->points();
			assert( !points.empty() );

			int point_index = std::rand() % points.size();
			cache_point_indices.push_back(points[point_index].index());

			// ������� �����
			route->remove_point(point_index);
			if (route->points().empty())
			{
				// ���� ����� ������ ���, �� ������� �������
				new_plan->remove_route(route_index);
			}
		}

		// ������� �� ������ ������
		for (const Route* route : routes)
			garage.take_car(route->car());

		// ��������� ����� � ��������� �����
		for (int point_index : cache_point_indices)
			vrp_insert_point(*new_plan, point_index, destination_points, garage, m_matrix_distances);

		new_plan->update_distance_and_fitness();
		
		//if (plan->fitness() < 26217.99f)
		//	__nop();
			//logs(QString("mutate2: %1").arg(plan->fitness()));

		return new_plan;
	}
	return SPlan();
}