#pragma once
#include <QDateTime>

namespace Base
{

//=====================================================================================//
//                                  class IBinStream                                   //
//=====================================================================================//
class IBinStream
{
private:
	std::istream& m_stream;

public:
	IBinStream(std::istream& stream);
	~IBinStream();
public:
	///	получить внутренний поток
	std::istream& stream();

};

inline IBinStream::IBinStream(std::istream& stream)
: m_stream(stream)
{
}
inline IBinStream::~IBinStream()
{
}
//	получить внутренний поток
inline std::istream& IBinStream::stream()
{
	return m_stream;
}

//=====================================================================================//
//                                  class OBinStream                                   //
//=====================================================================================//
class OBinStream
{
private:
	std::ostream& m_stream;

public:
	OBinStream(std::ostream& stream);
	~OBinStream();
public:
	///	получить внутренний поток
	std::ostream& stream();
};

inline OBinStream::OBinStream(std::ostream& stream)
: m_stream(stream)
{
}
inline OBinStream::~OBinStream()
{
}
//	получить внутренний поток
inline std::ostream& OBinStream::stream()
{
	return m_stream;
}

//=====================================================================================//
//                           inline IBinStream& doRawInput()                           //
//=====================================================================================//
template<typename T>
inline IBinStream& doRawInput(IBinStream& stream, T& t)
{
	stream.stream().read(reinterpret_cast<char*>(&t),sizeof(T));
	return stream;
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, unsigned char& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, unsigned int& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, unsigned short& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, wchar_t& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, unsigned long& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, signed char& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, signed int& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, signed short& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, signed long& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, char& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, long long& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, unsigned long long& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>();                                    //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, bool& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, float& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, double& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, long double& a)
{
	return doRawInput(stream,a);
}

//=====================================================================================//
//                          inline OBinStream& doRawOutput()                           //
//=====================================================================================//
template<typename T>
inline OBinStream& doRawOutput(OBinStream& stream, const T &t)
{
	stream.stream().write(reinterpret_cast<const char*>(&t),sizeof(T));
	return stream;
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, unsigned char a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, unsigned int a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, unsigned short a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, unsigned long a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, signed char a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, signed int a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, signed short a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, wchar_t a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, signed long a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, char a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, long long a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, unsigned long long a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<();                                    //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, bool a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, float a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, double a)
{
	return doRawOutput(stream,a);
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, long double a)
{
	return doRawOutput(stream,a);
}
//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, const char* str)
{
	stream.stream().write(str,strlen(str)+1);
	return stream;
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, const std::string& str)
{
	stream.stream().write(str.c_str(),str.length()+1);
	return stream;
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, std::string& str)
{
	str.clear();
	char ch = 1;

	while(stream.stream())
	{
		stream >> ch;
		if(!ch) break;
		str += ch;
	}

	return stream;
}


//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, const std::wstring& str)
{
	stream.stream().write(reinterpret_cast<const char*>(str.c_str()),(str.length()+1)*sizeof(wchar_t));
	return stream;
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, std::wstring& str)
{
	str.clear();
	wchar_t ch = 1;

	while(stream.stream())
	{
		stream >> ch;
		if(!ch) break;
		str += ch;
	}
	return stream;
}

inline OBinStream& operator<<(OBinStream& stream, const QByteArray& buffer)
{
	int size = buffer.size();
	stream << size;
	stream.stream().write(buffer.data(), size);
	return stream;
}
inline IBinStream& operator>>(IBinStream& stream, QByteArray& buffer)
{
	int size = 0;
	stream >> size;
	buffer.resize(size);
	stream.stream().read(buffer.data(), size);
	return stream;
}

//=====================================================================================//
//                                    operator<<()                                     //
//=====================================================================================//
inline OBinStream& operator<<(OBinStream& stream, const QString& str)
{
	stream << str.toUtf8();
	return stream;
}

//=====================================================================================//
//                                    operator>>()                                     //
//=====================================================================================//
inline IBinStream& operator>>(IBinStream& stream, QString& str)
{
	QByteArray buffer;
	stream >> buffer;
	str = QString(buffer);
	return stream;
}


inline OBinStream& operator<<(OBinStream& stream, const QDateTime& time)
{
	stream << time.toMSecsSinceEpoch();
	return stream;
}
inline IBinStream& operator>>(IBinStream& stream, QDateTime& time)
{
	qint64 t;
	stream >> t;
	time.setMSecsSinceEpoch(t);
	return stream;
}


inline OBinStream& operator<<(OBinStream& stream, const QUuid& uuid)
{
	stream << uuid.toByteArray();
	return stream;
}
inline IBinStream& operator>>(IBinStream& stream, QUuid& uuid)
{
	QByteArray buffer;
	stream >> buffer;
	uuid = QUuid(buffer);
	return stream;
}

}
