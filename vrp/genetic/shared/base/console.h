//=====================================================================================//
//   Author: open
//   Date:   23.06.2018
//=====================================================================================//
#pragma once

#ifdef Q_OS_WIN
#include <Windows.h>

void SetConsoleWindow(int Width, int Height)
{
	_COORD coord;
	coord.X = Width;
	coord.Y = Height;

	_SMALL_RECT Rect;
	Rect.Top = 0;
	Rect.Left = 0;
	Rect.Bottom = Height - 1;
	Rect.Right = Width - 1;

	HANDLE Handle = GetStdHandle(STD_OUTPUT_HANDLE);      // Get Handle 
	SetConsoleScreenBufferSize(Handle, coord);            // Set Buffer Size 
	SetConsoleWindowInfo(Handle, TRUE, &Rect);            // Set Window Size 

	RECT rect;
	GetWindowRect(GetConsoleWindow(), &rect);
	SetWindowPos(GetConsoleWindow(), 0, 0, 0, 1024, rect.bottom, 0);
	//ShowWindow(GetConsoleWindow(), SW_MAXIMIZE);
}
#endif
