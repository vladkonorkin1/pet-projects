//=====================================================================================//
//   Author: open
//   Date:   22.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "matrix_distances.h"

distance_t DistanceDesc::NotInitialize = std::numeric_limits<distance_t>::max();

MatrixDistances::MatrixDistances(int dimension)
: DynamicMatrix<DistanceDesc>(dimension)
{
	for (int i = 0; i < dimension; ++i)
		set(i, i, DistanceDesc(0.f, 0.f));
}
bool MatrixDistances::is_initialized() const
{
	for (int i = 0; i < dimension(); ++i)
		for (int j = 0; j < dimension(); ++j)
			if (!get(i, j).is_initialized())
				return false;
	return true;
}