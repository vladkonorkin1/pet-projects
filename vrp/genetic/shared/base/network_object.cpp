//=====================================================================================//
//   Author: open
//   Date:   04.05.2018
//=====================================================================================//
#include "base/precomp.h"
#include "network_object.h"
#include <QNetworkReply>

//=====================================================================================
NetworkObject::NetworkObject()
: m_method(NetworkMethod::Get)
, m_timeout(60.f)
, m_reply(nullptr)
{
	connect(&m_timer_wait, SIGNAL(timeout()), this, SLOT(on_wait_timeout()));
}
NetworkObject::~NetworkObject()
{
	abort();
}
void NetworkObject::abort()
{
	if (m_reply && m_reply->isRunning())
		m_reply->abort();
}
void NetworkObject::set_timeout(float timeout) {
	m_timeout = timeout;
}
void NetworkObject::set_reply(QNetworkReply* reply)
{
	m_reply = reply;
	if (m_reply) m_timer_wait.start(static_cast<int>(m_timeout*1000.f));
	else m_timer_wait.stop();
}
void NetworkObject::on_wait_timeout()
{
	Q_ASSERT( m_reply );
	m_timer_wait.stop();
	// after abort the timer might be destroyed
	abort();
	// <- !!! so "no-code!!!" after abort
}
const QByteArray& NetworkObject::send_buffer() const {
	static QByteArray temp;
	return temp;
}
void NetworkObject::set_method(NetworkMethod method) {
	m_method = method;
}
NetworkMethod NetworkObject::method() const {
	return m_method;
}

//=====================================================================================
DownloadData::DownloadData(const QString& url)
: m_url(url)
{
}
void DownloadData::process(QNetworkReply* reply, bool success) {
	emit finished(reply->error() == QNetworkReply::NetworkError::NoError, reply->readAll());
}
QString DownloadData::url() const {
	return m_url;
}