#pragma once

namespace Base {

//=====================================================================================//
//                                  class HasObserver                                  //
//=====================================================================================//
template<typename T>
class HasObserver
{
public:
	typedef T Observer;

protected:
	T* m_observer;		//	наблюдатель за классом

public:
	HasObserver();
	HasObserver(T* observer);
	virtual ~HasObserver();
public:
	///	установить наблюдатель за классом
	virtual void set_observer(Observer* observer);

};

template<typename T>
HasObserver<T>::HasObserver()
: m_observer(0)
{
}

template<typename T>
HasObserver<T>::HasObserver(T* observer)
: m_observer(observer)
{
}

template<typename T>
HasObserver<T>::~HasObserver()
{
}
//	установить наблюдатель за классом
template<typename T>
void HasObserver<T>::set_observer(Observer* observer)
{
	m_observer = observer;
}

}

