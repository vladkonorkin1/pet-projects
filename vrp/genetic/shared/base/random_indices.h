//=====================================================================================//
//   Author: open
//   Date:   03.08.2018
//=====================================================================================//
#ifndef RANDOM_INDICES_H
#define RANDOM_INDICES_H
#include <boost/random.hpp>

class RandomIndices
{
public:
	enum TypeIndex
	{
		Null = -1
	};

private:
	using indices_t = std::vector<int>;
	indices_t m_indices;
	size_t m_index;
	boost::random::mt19937 m_rand_gen;

public:
	RandomIndices(int start_index, int count, int seed);
	// сбросить и перемешать индексы
	void reset_random();
	// запросить следующий случайный индекс из последовательности
	int get_random_index();
};

#endif
