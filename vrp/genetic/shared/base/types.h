//=====================================================================================//
//   Author: open
//   Date:   22.09.2018
//=====================================================================================//
#pragma once
#include <QPointF>

using vpositions_t = std::vector<QPointF>;

struct Waypoint
{
	vpositions_t positions;
};

struct Route
{
	vpositions_t points;		// ����� ��������

	using waypoints_t = std::vector<Waypoint>;
	waypoints_t waypoints;		// ��������� ������� ����� �������
};

using SRoute = std::shared_ptr<Route>;

using routes_t = std::vector<SRoute>;