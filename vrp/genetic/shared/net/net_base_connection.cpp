//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "net_base_connection.h"

const quint64 NetBaseConnection::m_block_size = 64 * 1024; // 64 KB

NetBaseConnection::NetBaseConnection(QAbstractSocket* socket)
: m_socket(socket)
, m_write(false)
{
	connect(&m_packet_reader, SIGNAL(data_received(const char*, unsigned int)), SLOT(slot_data_recieved(const char*, unsigned int)));
	connect(m_socket, SIGNAL(connected()), SLOT(slot_start_connection()));
	connect(m_socket, SIGNAL(readyRead()), SLOT(slot_received()));
	connect(m_socket, SIGNAL(bytesWritten(qint64)), SLOT(slot_update_write_transfer(qint64)));
	connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(slot_close_connection(QAbstractSocket::SocketError)));
}
NetBaseConnection::NetBaseConnection(QLocalSocket* socket)
: m_socket(socket)
, m_write(false)
{
	connect(&m_packet_reader, SIGNAL(data_received(const char*, unsigned int)), SLOT(slot_data_recieved(const char*, unsigned int)));
	connect(m_socket, SIGNAL(connected()), SLOT(slot_start_connection()));
	connect(m_socket, SIGNAL(readyRead()), SLOT(slot_received()));
	connect(m_socket, SIGNAL(bytesWritten(qint64)), SLOT(slot_update_write_transfer(qint64)));
	connect(m_socket, SIGNAL(error(QLocalSocket::LocalSocketError)), SLOT(slot_close_connection(QLocalSocket::LocalSocketError)));
}
void NetBaseConnection::slot_update_write_transfer(qint64 num_bytes)
{
	Q_UNUSED(num_bytes);
	update_write();
}
void NetBaseConnection::update_write()
{
	if (!m_buffer.empty())
	{
		qint64 written = m_socket->write(m_buffer.data(), std::min<quint64>(m_buffer.size(), m_block_size));
		if (written > 0)
		{
			m_buffer.erase(m_buffer.begin(), m_buffer.begin() + written);
			if (m_buffer.empty())
				m_write = false;
		}
	}
}
void NetBaseConnection::write()
{
	if (!m_write)
	{
		m_write = true;
		update_write();
	}
}
void NetBaseConnection::slot_received()
{
	m_packet_reader.read(m_socket->readAll());
}
void NetBaseConnection::slot_close_connection(QAbstractSocket::SocketError error)
{
	//if (error != QAbstractSocket::SocketError::RemoteHostClosedError) {}
	close();
}
void NetBaseConnection::slot_close_connection(QLocalSocket::LocalSocketError error)
{
	//if (error != QAbstractSocket::SocketError::RemoteHostClosedError) {}
	close();
}
void NetBaseConnection::slot_start_connection()
{
	emit connected();
}
//	отослать сообщение серверу
void NetBaseConnection::send_message(const NetMessage& message)
{
	std::string data = message.save();

	NetPacketHeader header(static_cast<unsigned int>(data.size()));
	size_t cur = m_buffer.size();
	int header_size = sizeof(NetPacketHeader);
	m_buffer.resize(m_buffer.size() + header_size + data.size());
	memcpy(&m_buffer[cur], &header, header_size);
	memcpy(&m_buffer[cur] + header_size, data.c_str(), data.size());

	write();
}
void NetBaseConnection::slot_data_recieved(const char* data, unsigned int size)
{
	emit data_received(data, size);
}
//  закрыть соединение
void NetBaseConnection::close()
{
	m_socket->close();
	// нельзя удалять сокет! мы им не владеем...
	emit disconnected();
}