#pragma once
#include "base/bin_stream.h"

// protocol id + message
struct NetMessage
{
	virtual std::string save() const = 0;
};

#define NET_MESSAGE_DECL(protocol_id_t, cls)												\
	cls() {}																			\
	static unsigned short protocol_id() { return (unsigned short)protocol_id_t::cls; }	\
	virtual std::string save() const;													\
	std::string type_name() { return #cls; }

#define NET_MESSAGE_IMPL(cls)															\
	std::string cls::save() const {														\
		std::ostringstream ostream;														\
		Base::OBinStream bin(ostream);													\
		bin << protocol_id();															\
		bin << *this;																	\
		return ostream.str();															\
	}

#define NET_MESSAGE_STREAM(cls)															\
	inline Base::IBinStream& operator>>(Base::IBinStream& stream, cls& message) {		\
		message.read(stream);															\
		return stream;																	\
	}																					\
	inline Base::OBinStream& operator<<(Base::OBinStream& stream, const cls& message) {	\
		message.write(stream);															\
		return stream;																	\
	}

