#pragma once
#include <functional>
#include <strstream>
#include "base/observer.h"
#include "base/bin_stream.h"

template <typename THandlerMessages>
class NetMessageDispatcher : public Base::HasObserver<THandlerMessages>
{
private:
	using func_creator_t = std::function<void(Base::IBinStream&, THandlerMessages*)>;
	using creators_t = std::map<unsigned short, func_creator_t>;
	creators_t m_creators;

public:
	virtual ~NetMessageDispatcher() {}
	template <typename TMessage>
	void register_message()
	{
		assert( !m_creators[TMessage::protocol_id()] );
		m_creators[TMessage::protocol_id()] =
			[this](Base::IBinStream& stream, THandlerMessages* handles) {
			TMessage msg;
			stream >> msg;
			//std::cout << TMessage::protocol_id() << "\t" << msg.type_name() << std::endl;
			if (handles)
				handles->on_message(msg);
		};
	}
    void process(const char* data, unsigned int size)
    {
        std::istrstream istream(data, size);
        Base::IBinStream stream(istream);

        unsigned short protocol_id;
        stream >> protocol_id;

        auto it = m_creators.find(protocol_id);
        assert(it != m_creators.end());
        if (it != m_creators.end())
            it->second(stream, Base::HasObserver<THandlerMessages>::m_observer);
    }
};
