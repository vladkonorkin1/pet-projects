#pragma once
#include "net/net_packet.h"

class NetPacketReader : public QObject
{
public:
	Q_OBJECT
private:
	typedef bool (NetPacketReader::*parser_t)();
	typedef std::vector<char> data_t;

	NetPacketHeader m_header;	//	текущие данные заголовка пакета
	parser_t m_parser;			//	указатель на функцию разбора данных
	int m_current;				//	текущая позиция чтения пакета
	data_t m_data;				//	накопленные данные

public:
	NetPacketReader();

public:
	void read(const QByteArray& buffer);

signals:
	void data_received(const char* data, unsigned int size);

private:
	unsigned int current_size() const;
	///	1. разобрать маркер пакета
	bool parse_marker();
	///	2. разобрать заголовок пакета
	bool parse_header();
	///	3. разобрать данные
	bool parse_data();
};