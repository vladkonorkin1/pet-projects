#pragma once
#include "net_gate/net_gate_messages.h"

namespace NetGate
{

class ClientHandlerMessages
{
public:
	virtual ~ClientHandlerMessages() {}
	virtual void on_message(const NetGate::ServerMessageAutorizateResponse& msg) = 0;
	virtual void on_message(const NetGate::ServerMessageReportSolvedTasks& msg) = 0;
	virtual void on_message(const NetGate::ServerMessageOpenTask& msg) = 0;
	virtual void on_message(const NetGate::ServerMessageCloseTask& msg) = 0;
};

class ServerHandlerMessages
{
public:
	virtual ~ServerHandlerMessages() {}
	virtual void on_message(const NetGate::ClientMessageAutorizate& msg) = 0;
	virtual void on_message(const NetGate::ClientMessageAlive& msg) = 0;
	virtual void on_message(const NetGate::ClientMessageTaskSolved& msg) = 0;
	virtual void on_message(const NetGate::ClientMessageCloseTaskResponse& msg) = 0;
};

}