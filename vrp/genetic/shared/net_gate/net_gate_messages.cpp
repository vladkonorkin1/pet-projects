#include "base/precomp.h"
#include "net_gate/net_gate_messages.h"

namespace NetGate
{

NET_MESSAGE_IMPL(ClientMessageAutorizate);
NET_MESSAGE_IMPL(ServerMessageAutorizateResponse);
NET_MESSAGE_IMPL(ClientMessageAlive);
NET_MESSAGE_IMPL(ServerMessageOpenTask);
NET_MESSAGE_IMPL(ServerMessageReportSolvedTasks);
NET_MESSAGE_IMPL(ClientMessageTaskSolved);
NET_MESSAGE_IMPL(ServerMessageCloseTask);
NET_MESSAGE_IMPL(ClientMessageCloseTaskResponse);

}