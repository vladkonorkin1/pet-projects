#pragma once
#include "net/net_message.h"
#include "net_gate/net_gate_types.h"
#include "base/matrix_distances.h"

namespace NetGate
{

enum class MessageProtocolId
{
	Empty = 0,
	ClientMessageAutorizate,				// авторизация
	ServerMessageAutorizateResponse,
	ClientMessageAlive,						// сообщение серверу... "я жив!"
	ServerMessageOpenTask,					// открыть задачу
	ServerMessageReportSolvedTasks,			// доложить о решённых задачах
	ClientMessageTaskSolved,				// задача решена
	ServerMessageCloseTask,					// закрыть задачу
	ClientMessageCloseTaskResponse,			// потверждение о закрытии задачи
	MessageProtocolIdMax
};

struct ClientMessageAutorizate : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageAutorizate);
	ClientMessageAutorizate(const QUuid& id, const QString& name) : id(id), name(name) {}
	QUuid id;
	QString name;
	//QString mac_address;

	void read(Base::IBinStream& stream) {
		//stream >> mac_address;
		stream >> id;
		stream >> name;
	}
	void write(Base::OBinStream& stream) const {
		//stream << mac_address;
		stream << id;
		stream << name;
	}
};
NET_MESSAGE_STREAM(ClientMessageAutorizate);


struct ServerMessageAutorizateResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageAutorizateResponse);
	ServerMessageAutorizateResponse(bool auth_result, bool new_session) : auth_result(auth_result), new_session(new_session) {}
	bool auth_result;
	bool new_session;

	void read(Base::IBinStream& stream)
	{
		stream >> auth_result;
		stream >> new_session;
	}
	void write(Base::OBinStream& stream) const
	{
		stream << auth_result;
		stream << new_session;
	}
};
NET_MESSAGE_STREAM(ServerMessageAutorizateResponse);


struct ClientMessageAlive : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageAlive);
	void read(Base::IBinStream& stream) { Q_UNUSED(stream); }
	void write(Base::OBinStream& stream) const { Q_UNUSED(stream); }
};
NET_MESSAGE_STREAM(ClientMessageAlive);


struct ServerMessageOpenTask : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageOpenTask);
	ServerMessageOpenTask(const QUuid& task_id, const QJsonDocument& json_doc, const SMatrixDistances& matrix_distances) : task_id(task_id), json_doc(json_doc), matrix_distances(matrix_distances) {}
	QUuid task_id;
	QJsonDocument json_doc;
	SMatrixDistances matrix_distances;

	void read(Base::IBinStream& stream)
	{
		stream >> task_id;
		
		QByteArray buffer;
		stream >> buffer;
		json_doc = QJsonDocument::fromJson(buffer);

		stream >> matrix_distances;
	}
	void write(Base::OBinStream& stream) const
	{
		stream << task_id;
		stream << json_doc.toJson(QJsonDocument::Compact);
		stream << matrix_distances;
	}
};
NET_MESSAGE_STREAM(ServerMessageOpenTask);


struct ServerMessageReportSolvedTasks : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageReportSolvedTasks);
	void read(Base::IBinStream& stream) { Q_UNUSED(stream); }
	void write(Base::OBinStream& stream) const { Q_UNUSED(stream); }
};
NET_MESSAGE_STREAM(ServerMessageReportSolvedTasks);


struct ClientMessageTaskSolved : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageTaskSolved);
	ClientMessageTaskSolved(const QUuid& task_id, bool result, const QJsonDocument& json_result) : task_id(task_id), result(result), json_result(json_result) {}
	QUuid task_id;
	bool result;
	QJsonDocument json_result;

	void read(Base::IBinStream& stream)
	{
		stream >> task_id;
		stream >> result;
		
		QByteArray buffer;
		stream >> buffer;
		json_result = QJsonDocument::fromJson(buffer);
	}
	void write(Base::OBinStream& stream) const
	{
		stream << task_id;
		stream << result;
		stream << json_result.toJson(QJsonDocument::Compact);
	}
};
NET_MESSAGE_STREAM(ClientMessageTaskSolved);


struct ServerMessageCloseTask : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageCloseTask);
	ServerMessageCloseTask(const QUuid& task_id) : task_id(task_id) {}
	QUuid task_id;

	void read(Base::IBinStream& stream)
	{
		stream >> task_id;
	}
	void write(Base::OBinStream& stream) const
	{
		stream << task_id;
	}
};
NET_MESSAGE_STREAM(ServerMessageCloseTask);


struct ClientMessageCloseTaskResponse : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageCloseTaskResponse);
	ClientMessageCloseTaskResponse(const QUuid& task_id) : task_id(task_id) {}
	QUuid task_id;

	void read(Base::IBinStream& stream)
	{
		stream >> task_id;
	}
	void write(Base::OBinStream& stream) const
	{
		stream << task_id;
	}
};
NET_MESSAGE_STREAM(ClientMessageCloseTaskResponse);

}