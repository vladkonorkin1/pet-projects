#pragma once
#include "net/net_message.h"
#include "net_hub/net_hub_types.h"
#include "base/matrix_distances.h"

namespace NetHub
{

enum class MessageProtocolId
{
	Empty = 0,
	ClientMessageAuthorize,
	ServerMessageOpenTicket,
	ClientMessageTicketSolved,
	ServerMessageExitProcess,
	MessageProtocolIdMax
};

struct ClientMessageAuthorize : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageAuthorize);
	ClientMessageAuthorize(const QString& solver_key) : solver_key(solver_key) {}
	QString solver_key;

	void read(Base::IBinStream& stream)
	{
		stream >> solver_key;
	}
	void write(Base::OBinStream& stream) const
	{
		stream << solver_key;
	}
};
NET_MESSAGE_STREAM(ClientMessageAuthorize);

struct ServerMessageOpenTicket : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageOpenTicket);
	ServerMessageOpenTicket(const QJsonDocument& json_doc, const SMatrixDistances& matrix_distances) : json_doc(json_doc), matrix_distances(matrix_distances) {}
	QJsonDocument json_doc;
	SMatrixDistances matrix_distances;

	void read(Base::IBinStream& stream)
	{
		QByteArray buffer;
		stream >> buffer;
		json_doc = QJsonDocument::fromJson(buffer);
		stream >> matrix_distances;
	}
	void write(Base::OBinStream& stream) const
	{
		stream << json_doc.toJson(QJsonDocument::Compact);
		stream << matrix_distances;
	}
};
NET_MESSAGE_STREAM(ServerMessageOpenTicket);


struct ClientMessageTicketSolved : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ClientMessageTicketSolved);
	ClientMessageTicketSolved(const QJsonDocument& json_result) : json_result(json_result) {}

	QJsonDocument json_result;

	void read(Base::IBinStream& stream)
	{
		QByteArray buffer;
		stream >> buffer;
		json_result = QJsonDocument::fromJson(buffer);
	}
	void write(Base::OBinStream& stream) const
	{
		stream << json_result.toJson(QJsonDocument::Compact);
	}
};
NET_MESSAGE_STREAM(ClientMessageTicketSolved);


struct ServerMessageExitProcess : public NetMessage
{
	NET_MESSAGE_DECL(MessageProtocolId, ServerMessageExitProcess);

	void read(Base::IBinStream&) {}
	void write(Base::OBinStream&) const {}
};
NET_MESSAGE_STREAM(ServerMessageExitProcess);

}
