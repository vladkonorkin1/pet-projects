//=====================================================================================//
//   Author: open
//   Date:   02.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "net_request_osrm_route.h"
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

QString NetRequestOsrmRoute::s_server_address;

NetRequestOsrmRoute::NetRequestOsrmRoute(const QPointF& start_pos, const QPointF& end_pos)
: m_start_pos(start_pos)
, m_end_pos(end_pos)
{
}
// http://10.30.37.92:5000/table/v1/driving/60.754581,56.758808;61.754580,56.758800;60.754570,56.758700&annotations=distance
// http://10.30.37.92:5000/route/v1/driving/60.754581,56.758808;61.754580,56.758800;60.754570,56.758700?geometries=geojson&overview=full&generate_hints=false
QString NetRequestOsrmRoute::url() const {
	return "http://" + s_server_address + "/route/v1/driving/" + QString().sprintf("%.6f,%.6f;%.6f,%.6f?geometries=geojson&overview=full&generate_hints=false", m_start_pos.x(), m_start_pos.y(), m_end_pos.x(), m_end_pos.y());
	//return Settings::api_url() + "users/";
}
void NetRequestOsrmRoute::process(QNetworkReply* reply, bool success)
{
	//OsrmRoute osrm_route;
	//osrm_route.start_point = m_start_pos;
	//osrm_route.end_point = m_end_pos;

	vpositions_t positions;
	if (success)
	{
		QJsonDocument json_doc = QJsonDocument::fromJson(reply->readAll());
		QJsonObject json_obj = json_doc.object();
		if (json_obj["code"] == "Ok")
		{
			QJsonArray json_routes = json_obj["routes"].toArray();
			QJsonObject json_route = json_routes[0].toObject();
			parse_coordinate(json_route["geometry"].toObject()["coordinates"].toArray(), positions);
		}
		else logs("NetRequestOsrmRoute: code not OK");
	}
	emit finished(success, positions);
}
void NetRequestOsrmRoute::parse_coordinate(const QJsonArray& json_coordinate, vpositions_t& coordinate)
{
	int size = json_coordinate.size();
	int step = std::max(1, size/280);
	for (int k = 0; k < size; k = (((k + step) < size) ? (k + step) : (k + std::max((size - k) - 1, 1))))
	{
		QJsonArray json_coord = json_coordinate[k].toArray();
		coordinate.push_back(QPointF(json_coord[0].toDouble(), json_coord[1].toDouble()));
	}
}
void NetRequestOsrmRoute::set_server_address(const QString& server_address)
{
	s_server_address = server_address;
}