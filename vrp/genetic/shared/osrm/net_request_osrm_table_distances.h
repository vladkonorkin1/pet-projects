//=====================================================================================//
//   Author: open
//   Date:   22.09.2018
//=====================================================================================//
#pragma once
#include "base/network_object.h"
#include "base/matrix_distances.h"
#include "base/types.h"

class NetRequestOsrmTableDistances : public PostNetworkObject
{
	Q_OBJECT
private:
	static QString s_server_address;
	vpositions_t m_points;
	SMatrixDistances m_matrix_distances;

public:
	NetRequestOsrmTableDistances(const vpositions_t& points, const SMatrixDistances& matrix);
	virtual void process(QNetworkReply* reply, bool success);
	virtual QString url() const;
	static void set_server_address(const QString& server_address);

signals:
	void finished(bool success, const SMatrixDistances& matrix);
};

using SNetRequestOsrmTableDistances = std::shared_ptr<NetRequestOsrmTableDistances>;