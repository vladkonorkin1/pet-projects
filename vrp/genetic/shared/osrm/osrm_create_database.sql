create table distances (
	src text not null,
	dest text not null,
	distance real not null,
	duration real not null,
	primary key(src, dest)
);