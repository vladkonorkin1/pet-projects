//=====================================================================================//
//   Author: open
//   Date:   02.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "osrm_manager.h"
#include "base/network_manager.h"
 
OsrmManager::OsrmManager(NetworkManager& network_manager, const QString& server_address)
: m_network_manager(network_manager)
, m_request_routes(network_manager)
{
	connect(&m_request_routes, SIGNAL(finished(bool, const routes_t&)), SIGNAL(request_routes_finished(bool, const routes_t&)));
	set_server_address(server_address);
}
void OsrmManager::request_routes(const routes_t& routes)
{
	m_request_routes.request_routes(routes);
}
void OsrmManager::set_server_address(const QString& server_address)
{
	NetRequestOsrmDistance::set_server_address(server_address);
	NetRequestOsrmTableDistances::set_server_address(server_address);
	NetRequestOsrmRoute::set_server_address(server_address);
}
SOsrmRequestMatrixDistances OsrmManager::create_request_matrix_distances(const vpositions_t& points) const
{
	return SOsrmRequestMatrixDistances(new OsrmRequestMatrixDistances(m_network_manager, points));
}
void OsrmManager::request_matrix_distances(const SOsrmRequestMatrixDistances& request)
{
	SMatrixDistances matrix = request->matrix();
	m_database.select_matrix_distances(request->points(), matrix);
	if (matrix->is_initialized()) request->emit_finished(true);	//emit request->finished(true, matrix, request->points(), );
	else
	{
		m_matrix_distances_requests[request.get()] = request;
		connect(request.get(), &OsrmRequestMatrixDistances::finished, [this, request = request.get()](bool success, const SMatrixDistances& matrix, const vpositions_t& points, const matrix_distance_indices_t& indices) { slot_request_matrix_distances_finished(request, success, matrix, points, indices); });
		if (percent_matrix_initialized(*matrix) > 0.9f)
		//if (true)
		{
			matrix_distance_indices_t indices;
			for (int i = 0; i < matrix->dimension(); ++i)
			{
				for (int j = 0; j < matrix->dimension(); ++j)
				{
					if (i != j && !matrix->get(i, j).is_initialized())
						indices.push_back(MatrixDistanceIndex{ i, j });
				}
			}
			request->request_by_indices(indices);
		}
		else
		{
			request->request_by_table();
		}
	}
}
float OsrmManager::percent_matrix_initialized(const MatrixDistances& matrix) const
{
	int num_init = 0;
	int total_count = matrix.dimension() * matrix.dimension();
	for (int i = 0; i < matrix.dimension(); ++i)
	{
		for (int j = 0; j < matrix.dimension(); ++j)
		{
			if (matrix.get(i, j).is_initialized())
				++num_init;
		}
	}
	return num_init / total_count;
}
/*void OsrmManager::slot_request_matrix_distances_finished(const OsrmRequestMatrixDistances* request, bool success, const SMatrixDistances& matrix, const vpositions_t& points)
{
	if (success)
	{
		matrix_distance_indices_t indices;
		for (int i = 0; i < matrix->dimension(); ++i)
			for (int j = 0; j < matrix->dimension(); ++j)
				if (i != j) indices.push_back(MatrixDistanceIndex{ i, j });
		m_database.update_distances(points, indices, matrix);
	}
	m_matrix_distances_requests.erase(request);
}*/
void OsrmManager::slot_request_matrix_distances_finished(const OsrmRequestMatrixDistances* request, bool success, const SMatrixDistances& matrix, const vpositions_t& points, const matrix_distance_indices_t& indices)
{
	if (success) m_database.update_distances(points, indices, matrix);
	m_matrix_distances_requests.erase(request);
}