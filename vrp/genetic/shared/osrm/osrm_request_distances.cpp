//=====================================================================================//
//   Author: open
//   Date:   22.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "osrm_request_distances.h"
#include "base/network_manager.h"

int OsrmRequestDistances::s_batch_count = 4;

OsrmRequestDistances::OsrmRequestDistances(NetworkManager& network_manager)
: m_network_manager(network_manager)
, m_num_finished(0)
, m_count_queue(0)
, m_total_count(0)
, m_num_failed(0)
{
}
void OsrmRequestDistances::request_distances(const vpositions_t& points, const matrix_distance_indices_t& indices, const SMatrixDistances& matrix)
{
	logs(QString("osrm: start request distances: %1").arg(points.size()));
	m_points = points;
	m_indices = indices;
	m_matrix = matrix;

	m_pool_requests.clear();
	for (const MatrixDistanceIndex& index : indices)
	{
		SNetRequestOsrmDistance request(new NetRequestOsrmDistance(points[index.start], points[index.end]));
		connect(request.get(), &NetRequestOsrmDistance::finished, [this, request, index](bool success, const DistanceDesc& desc) {slot_request_finished(success, request, index, desc); });
		m_pool_requests.push_back(request);
	}

	m_total_count = m_pool_requests.size();
	m_num_finished = 0;
	m_count_queue = 0;
	m_num_failed = 0;
	send_requests();
}
void OsrmRequestDistances::send_requests()
{
	while (m_count_queue < s_batch_count && !m_pool_requests.empty())
	{
		m_count_queue++;
		pool_requests_t::iterator it = --m_pool_requests.end();
		m_network_manager.request(*it, NetworkMethod::Get);
		m_pool_requests.erase(it);
	}
}
void OsrmRequestDistances::slot_request_finished(bool success, const SNetRequestOsrmDistance& request, const MatrixDistanceIndex& index, const DistanceDesc& desc)
{
	if (success)
	{
		m_num_failed = 0;
		m_count_queue--;
		send_requests();

		(*m_matrix)(index.start, index.end) = desc;

		m_num_finished++;

		if ((m_num_finished % 200) == 0 || (m_num_finished == m_total_count))
			logs(QString().sprintf("osrm: %.1f%%", (float)m_num_finished / (float)m_total_count*100.f));

		if (m_num_finished == m_total_count)
		{
			logs("osrm: finish");
			emit finished(true, m_points, m_indices, m_matrix);
		}
	}
	else
	{
		logs("osrm: failed request distance: " + request->url());
		++m_num_failed;
		if (m_num_failed < s_batch_count * 10) m_network_manager.reconnect(request); 
		else
		{	
			emit finished(false, m_points, m_indices, m_matrix);

			for (SNetRequestOsrmDistance& request : m_pool_requests)
				disconnect(request.get(), SIGNAL(finished));
			m_pool_requests.clear();
		}
	}
}