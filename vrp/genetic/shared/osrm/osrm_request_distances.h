//=====================================================================================//
//   Author: open
//   Date:   22.09.2018
//=====================================================================================//
#pragma once
#include "osrm/net_request_osrm_distance.h"
#include "base/types.h"

class NetworkManager;

class OsrmRequestDistances : public QObject
{
	Q_OBJECT
private:
	NetworkManager& m_network_manager;

	vpositions_t m_points;
	matrix_distance_indices_t m_indices;
	SMatrixDistances m_matrix;

	int m_total_count;
	int m_num_finished;
	int m_num_failed;

	using pool_requests_t = std::vector<SNetRequestOsrmDistance>;
	pool_requests_t m_pool_requests;

	static int s_batch_count;
	int m_count_queue;

public:
	OsrmRequestDistances(NetworkManager& network_manager);
	void request_distances(const vpositions_t& points, const matrix_distance_indices_t& indices, const SMatrixDistances& matrix);

signals:
	void finished(bool success, const vpositions_t& points, const matrix_distance_indices_t& indices, const SMatrixDistances& matrix);

private:
	void slot_request_finished(bool success, const SNetRequestOsrmDistance& request, const MatrixDistanceIndex& index, const DistanceDesc& desc);

private:
	void send_requests();
};