//=====================================================================================//
//   Author: open
//   Date:   22.09.2018
//=====================================================================================//
#pragma once
#include "osrm/net_request_osrm_table_distances.h"
#include "osrm/net_request_osrm_distance.h"
#include "base/types.h"

class NetworkManager;

class OsrmRequestMatrixDistances : public QObject
{
	Q_OBJECT
private:
	NetworkManager& m_network_manager;
	vpositions_t m_points;
	matrix_distance_indices_t m_indices;
	SMatrixDistances m_matrix;

	int m_total_count;
	int m_num_finished;
	int m_num_failed;

	using pool_requests_t = std::vector<SNetRequestOsrmDistance>;
	pool_requests_t m_pool_requests;

	static int s_batch_count;
	int m_count_queue;

	// ������ ����� ����� ������� ���������
	SNetRequestOsrmTableDistances m_request_table_distances;

public:
	OsrmRequestMatrixDistances(NetworkManager& network_manager, const vpositions_t& points);
	void request_by_indices(const matrix_distance_indices_t& indices);
	void request_by_table();
	const vpositions_t& points() const { return m_points; }
	const SMatrixDistances& matrix() const { return m_matrix; }
	void emit_finished(bool success);

signals:
	void finished(bool success, const SMatrixDistances& matrix, const vpositions_t& points, const matrix_distance_indices_t& indices);
	//void indices_finished(bool success, const SMatrixDistances& matrix, const vpositions_t& points, const matrix_distance_indices_t& indices);

private slots:
	void slot_request_table_distances_finished(bool success);

private:
	void slot_request_distance_finished(bool success, const SNetRequestOsrmDistance& request, const MatrixDistanceIndex& index, const DistanceDesc& desc);

private:
	void send_requests();
};

using SOsrmRequestMatrixDistances = std::shared_ptr<OsrmRequestMatrixDistances>;