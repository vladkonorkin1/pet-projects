//=====================================================================================//
//   Author: open
//   Date:   22.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "osrm_request_routes.h"
#include "base/network_manager.h"

int OsrmRequestRoutes::s_batch_count = 8;

OsrmRequestRoutes::OsrmRequestRoutes(NetworkManager& network_manager)
: m_network_manager(network_manager)
, m_num_finished(0)
, m_count_queue(0)
, m_total_count(0)
, m_num_failed(0)
{
}
void OsrmRequestRoutes::request_routes(const routes_t& routes)
{
	logs(QString("osrm: start request routes: %1").arg(routes.size()));
	m_routes = routes;

	m_pool_requests.clear();
	for (size_t i = 0 ; i < m_routes.size(); ++i)
	{
		Route* route = m_routes[i].get();
		assert( route->points.size() > 1 );
		route->waypoints.resize(route->points.size() - 1);
		for (size_t j = 0; (j + 1) < route->points.size(); ++j)
		{
			SNetRequestOsrmRoute request(new NetRequestOsrmRoute(route->points[j], route->points[j+1]));
			connect(request.get(), &NetRequestOsrmRoute::finished, [this, request, route_index = i, waypoint_index = j](bool success, const vpositions_t& waypoint_positions) {slot_request_finished(success, request, route_index, waypoint_index, waypoint_positions); });
			m_pool_requests.push_back(request);
		}
	}

	m_total_count = m_pool_requests.size();
	m_num_finished = 0;
	m_count_queue = 0;
	m_num_failed = 0;
	send_requests();
}
void OsrmRequestRoutes::send_requests()
{
	while (m_count_queue < s_batch_count && !m_pool_requests.empty())
	{
		m_count_queue++;
		pool_requests_t::iterator it = --m_pool_requests.end();
		m_network_manager.request(*it, NetworkMethod::Get);
		m_pool_requests.erase(it);
	}
}
void OsrmRequestRoutes::slot_request_finished(bool success, const SNetRequestOsrmRoute& request, int route_index, int waypoint_index, const vpositions_t& waypoint_positions)
{
	if (success)
	{
		m_num_failed = 0;
		m_count_queue--;
		send_requests();

		m_routes[route_index]->waypoints[waypoint_index].positions = waypoint_positions;

		m_num_finished++;

		if ((m_num_finished % 30) == 0 || (m_num_finished == m_total_count))
			logs(QString().sprintf("osrm: %.1f%%", (float)m_num_finished / (float)m_total_count*100.f));

		if (m_num_finished == m_total_count)
		{
			logs("osrm: finish");
			emit finished(true, m_routes);
		}
	}
	else
	{
		logs("osrm: failed request route: " + request->url());
		++m_num_failed;
		if (m_num_failed < s_batch_count * 10) m_network_manager.reconnect(request);
		else
		{
			emit finished(false, m_routes);
			for (SNetRequestOsrmRoute& request : m_pool_requests)
				disconnect(request.get(), SIGNAL(finished));
			m_pool_requests.clear();
		}
	}
}