//=====================================================================================//
//   Author: open
//   Date:   11.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "vrp_async_solver.h"

namespace Vrp
{

VrpAsyncSolver::VrpAsyncSolver(const Data& data)
: m_available_cars(data.available_cars)
, m_destination_points(data.destination_points)
, m_matrix_distances(data.matrix_distances)
, m_num_create_population_tasks_finished(0)
, m_num_crossover_tasks_finished(0)
, m_population_size(data.population_size)	// 1024
, m_epoch_count(data.cycle_count)			// 1200
, m_halt_count_limit(data.halt_cycle_count)	// 170
, m_halt_time(data.halt_time)
, m_epoch_index(0)
, m_rand_gen(data.random_seed + data.base_random_seed)//(data.random_seed)
, m_num_mutation_tasks_finished(0)
, m_last_best_fitness(0.f)
, m_halt_count(0)
//, m_id(random_seed)
{
	int num_crossover_tasks = m_thread_task_manager.count_threads() * 2;
	m_crossover_plans.resize(num_crossover_tasks);
	m_crossover_tasks.reserve(num_crossover_tasks);
	for (int i = 0; i < num_crossover_tasks; ++i) {
		SVrpCrossoverTask task(new VrpCrossoverTask(m_plans, Garage(m_available_cars, m_rand_gen()), m_destination_points, *m_matrix_distances));
		connect(task.get(), &VrpCrossoverTask::finish, [this, index = i](const VrpCrossoverTask::result_plans_t& result_plans) { slot_finish_crossover(index, result_plans); });
		m_crossover_tasks.push_back(task);
		m_thread_tasks.push_back(task);
	}
	
	int num_mutation_tasks = m_thread_task_manager.count_threads() * 2;
	m_mutate_plans.resize(num_mutation_tasks);
	m_mutation_tasks.reserve(num_mutation_tasks);
	for (int i = 0; i < num_mutation_tasks; ++i)
	{
		SVrpMutationTask task(new VrpMutationTask(Garage(m_available_cars, m_rand_gen()), m_destination_points, *m_matrix_distances));
		connect(task.get(), &VrpMutationTask::finish, [this, index = i](const VrpMutationTask::result_plans_t& result_plans) { slot_finish_mutation(index, result_plans);  });
		m_mutation_tasks.push_back(task);
		m_thread_tasks.push_back(task);
	}
}
VrpAsyncSolver::~VrpAsyncSolver()
{
	m_thread_task_manager.wait_close();
}
void VrpAsyncSolver::calculate()
{
	m_start_time = QDateTime::currentDateTimeUtc();
	create_population();
}
void VrpAsyncSolver::create_population()
{
	m_num_create_population_tasks_finished = 0;
	m_plans.clear();
	m_plans.reserve(m_population_size);

	m_last_best_fitness = 0.f;
	m_halt_count = 0;

	ThreadTaskManager::tasks_t tasks;
	int step = ceil((float)m_population_size / (float)m_thread_task_manager.count_threads());
	for (int i = m_population_size; i > 0; i -= step)
	{
		SVrpCreatePopulationTask task(new VrpCreatePopulationTask(m_rand_gen(), std::min(i, step), m_available_cars, m_destination_points, *m_matrix_distances));
		connect(task.get(), &VrpCreatePopulationTask::finish, [this, index = m_create_population_tasks.size()](const VrpCreatePopulationTask::result_plans_t& result_plans) { slot_finish_create_population(index, result_plans); });
		m_create_population_tasks.push_back(task);
		tasks.push_back(task);
	}
	m_create_population_plans.resize(m_create_population_tasks.size());
	m_thread_task_manager.add_tasks(tasks);
}
void VrpAsyncSolver::slot_finish_create_population(int index, const VrpCreatePopulationTask::result_plans_t& result_plans)
{
	plans_t& create_population_plans = m_create_population_plans[index];
	for (const SPlan& plan : result_plans)
		create_population_plans.push_back(plan);

	if (++m_num_create_population_tasks_finished == m_create_population_tasks.size())
	{
		for (const plans_t& create_population_plans : m_create_population_plans)
		{
			for (const SPlan& plan : create_population_plans)
				m_plans.push_back(plan);
		}
		m_create_population_plans = create_population_plans_t();
		m_create_population_tasks.clear();

		// запускаем цикл по эпохам
		m_epoch_index = 0;
		next_epoch();
	}
}
void VrpAsyncSolver::next_epoch()
{
	// объединяем варианты решений
	for (plans_t& crossover_plans : m_crossover_plans)
	{
		for (const SPlan& plan : crossover_plans)
			m_plans.push_back(plan);
		crossover_plans.clear();
	}
	for (plans_t& mutate_plans : m_mutate_plans)
	{
		for (const SPlan& plan : mutate_plans)
			m_plans.push_back(plan);
		mutate_plans.clear();
	}

	// сортируем по фитнесс-функции
	std::sort(m_plans.begin(), m_plans.end(), [](const SPlan& left, const SPlan& right) { return left->fitness() < right->fitness(); });
//#define VRP_BAD
#ifdef VRP_BAD
	int start_poor_index = m_population_size;// +m_population_size / 2;
	int end_poor_index = m_plans.size();
	int poor_size = end_poor_index - start_poor_index;

	//int sz = m_plans.size() - m_population_size;
	if (poor_size > 0)
	{
		float percent = 0.95f;
		int start_index = percent * m_population_size;
		for (int i = start_index; i < m_population_size; ++i)
		{
			int index = start_poor_index + m_rand_gen() % poor_size;
			m_plans[i] = m_plans[index];
		}
	}
#endif
	// восстанавливаем размер популяции до первоначального размера! выжили самые... приспособленные
	m_plans.resize(m_population_size);

	// лучший план
	const SPlan& best_epoch_plan = m_plans[0];
	float percent_epoch = (float)m_epoch_index/(float)m_epoch_count*100.f;
	logs(QString().sprintf("%d: %.0f%% %.2f %d", m_epoch_index, percent_epoch, best_epoch_plan->fitness(), m_halt_count));
	
	if (!m_best_plan || (m_best_plan->fitness() > best_epoch_plan->fitness()))
	{
		m_best_plan = best_epoch_plan;
		emit logs_visual(m_best_plan);

	}

	if (is_finish(best_epoch_plan))
	{
		m_best_plan = best_epoch_plan;
		emit finished(m_best_plan);
		return;
	}
	
	// запускаем новую эпоху
	m_num_crossover_tasks_finished = 0;
	m_num_mutation_tasks_finished = 0;
	{
		int step = ceil((float)m_plans.size() / (float)m_crossover_tasks.size());
		size_t iplan = 0;
		for (size_t i = 0; iplan < m_plans.size(); ++i, iplan += step)
		{
			SVrpCrossoverTask& task = m_crossover_tasks[i];
			task->set_seed(m_rand_gen());

			int count = std::min<int>(iplan + step, m_plans.size());
			for (int j = iplan; j < count; ++j)
				task->add_base_plan(m_plans[j].get());
		}
	}
	{
		int step = ceil((float)m_plans.size() / (float)m_mutation_tasks.size());
		size_t iplan = 0;
		for (size_t i = 0; iplan < m_plans.size(); ++i, iplan += step)
		{
			SVrpMutationTask& task = m_mutation_tasks[i];
			task->set_seed(m_rand_gen());

			int count = std::min<int>(iplan + step, m_plans.size());
			for (int j = iplan; j < count; ++j)
				task->add_base_plan(m_plans[j].get());
		}
	}
	m_thread_task_manager.add_tasks(m_thread_tasks);
}
bool VrpAsyncSolver::is_finish(const SPlan& best_epoch_plan)
{
	if (m_halt_time != 0)
	{
		qint64 secs = m_start_time.secsTo(QDateTime::currentDateTimeUtc());
		if (secs > m_halt_time)
			return true;
	}

	if (best_epoch_plan->fitness() == m_last_best_fitness) ++m_halt_count;
	else
	{
		m_last_best_fitness = best_epoch_plan->fitness();
		m_halt_count = 0;
	}
	if (m_halt_count > m_halt_count_limit) return true;

	return m_epoch_index >= m_epoch_count;
}
void VrpAsyncSolver::slot_finish_crossover(int index, const VrpCrossoverTask::result_plans_t& result_plans)
{
	//std::cout << m_rand_gen() << " ";
	//std::cout << index << " ";// std::endl;

	plans_t& crossover_plans = m_crossover_plans[index];
	// копируем новые полученные планы
	for (const SPlan& plan : result_plans)
		crossover_plans.push_back(plan);

	m_num_crossover_tasks_finished++;
	check_epoch_finished();
}
void VrpAsyncSolver::check_epoch_finished()
{
	if (m_num_crossover_tasks_finished == m_crossover_tasks.size() &&
		m_num_mutation_tasks_finished == m_mutation_tasks.size())
	{
		{
			//for (const SVrpCrossoverTask& crossover_task : m_crossover_tasks)
			//{
			//	std::cout << crossover_task->m_index << ": seed=" << crossover_task->m_seed << " first=" << crossover_task->m_first_random << " last=" << crossover_task->m_last_random << std::endl;
			//}
			//std::cout << std::endl;
		}

		emit epoch_finished(m_epoch_index);
		m_epoch_index++;
		next_epoch();
	}
}
void VrpAsyncSolver::slot_finish_mutation(int index, const VrpMutationTask::result_plans_t& result_plans)
{
	plans_t& mutate_plans = m_mutate_plans[index];
	// копируем новые полученные планы
	for (const SPlan& plan : result_plans)
		mutate_plans.push_back(plan);

	m_num_mutation_tasks_finished++;
	check_epoch_finished();
}

}
