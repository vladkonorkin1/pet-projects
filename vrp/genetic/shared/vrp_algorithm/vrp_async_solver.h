//=====================================================================================//
//   Author: open
//   Date:   11.08.2018
//=====================================================================================//
#pragma once
#include "vrp_algorithm/vrp_data.h"
#include "base/thread_task_manager.h"
#include "vrp_algorithm/vrp_create_population_task.h"
#include "vrp_algorithm/vrp_crossover_task.h"
#include "vrp_algorithm/vrp_mutation_task.h"

namespace Vrp
{

class VrpAsyncSolver : public QObject
{
	Q_OBJECT
private:
	ThreadTaskManager m_thread_task_manager;
	available_cars_t m_available_cars;
	destination_points_t m_destination_points;
	SMatrixDistances m_matrix_distances;
	boost::random::mt19937 m_rand_gen;
	QDateTime m_start_time;

	using plans_t = std::vector<SPlan>;
	plans_t m_plans;
	using create_population_plans_t = std::vector<plans_t>;
	create_population_plans_t m_create_population_plans;
	using crossover_plans_t = std::vector<plans_t>;
	crossover_plans_t m_crossover_plans;
	using mutate_plans_t = std::vector<plans_t>;
	mutate_plans_t m_mutate_plans;

	using create_population_tasks_t = std::vector<SVrpCreatePopulationTask>;
	create_population_tasks_t m_create_population_tasks;
	int m_num_create_population_tasks_finished;
	int m_epoch_index;
	const int m_population_size;
	const int m_epoch_count;
	const int m_halt_count_limit;
	const int m_halt_time;

	using crossover_tasks_t = std::vector<SVrpCrossoverTask>;
	crossover_tasks_t m_crossover_tasks;
	int m_num_crossover_tasks_finished;

	using mutation_tasks_t = std::vector<SVrpMutationTask>;
	mutation_tasks_t m_mutation_tasks;
	int m_num_mutation_tasks_finished;

	ThreadTaskManager::tasks_t m_thread_tasks;

	distance_t m_last_best_fitness;
	int m_halt_count;

	//int m_id;

	SPlan m_best_plan;

public:
	VrpAsyncSolver(const Data& data);
	~VrpAsyncSolver();
	void calculate();
	int num_cycles() const { return m_epoch_index; }

signals:
	void finished(const Vrp::SPlan& plan);
	void epoch_finished(int epoch_index);
	void logs_visual(const Vrp::SPlan& plan);

private:
	void slot_finish_create_population(int index, const VrpCreatePopulationTask::result_plans_t& result_plans);
	void slot_finish_crossover(int index, const VrpCrossoverTask::result_plans_t& result_plans);
	void slot_finish_mutation(int index, const VrpMutationTask::result_plans_t& result_plans);

private:
	void create_population();
	void next_epoch();
	void check_epoch_finished();
	bool is_finish(const SPlan& best_epoch_plan);
};

}