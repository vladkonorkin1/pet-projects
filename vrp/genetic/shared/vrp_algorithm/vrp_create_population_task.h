//=====================================================================================//
//   Author: open
//   Date:   11.08.2018
//=====================================================================================//
#pragma once
#include "vrp_algorithm/vrp_data.h"
#include "base/thread_task.h"

class RandomIndices;

namespace Vrp
{

class VrpCreatePopulationTask : public ThreadTask
{
	Q_OBJECT
public:
	using result_plans_t = std::vector<SPlan>;

private:
	const destination_points_t& m_destination_points;
	const MatrixDistances& m_matrix_distances;
	Garage m_garage;							// гараж с машинами
	std::uint32_t m_seed;
	int m_population_size;
	result_plans_t m_result_plans;				// созданные планы
	

public:
	VrpCreatePopulationTask(std::uint32_t seed, int population_size, const available_cars_t& available_cars, const destination_points_t& destination_points, const MatrixDistances& matrix_distances);
	// !!! not in main thread
	virtual void on_process();
	// in main thread!!!
	virtual void on_finish_process();

signals:
	void finish(const VrpCreatePopulationTask::result_plans_t& result_plans);

private:
	SPlan build_plan(RandomIndices& random_indices);
};

using SVrpCreatePopulationTask = std::shared_ptr<VrpCreatePopulationTask>;

}