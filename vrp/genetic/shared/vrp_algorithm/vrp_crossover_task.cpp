//=====================================================================================//
//   Author: open
//   Date:  11.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "vrp_crossover_task.h"
#include "vrp_insert_point.h"

namespace Vrp
{

VrpCrossoverTask::VrpCrossoverTask(const plans_t& all_plans, const Garage& garage, const destination_points_t& destination_points, const MatrixDistances& matrix_distances)
: m_all_plans(all_plans)
, m_garage(garage)
, m_destination_points(destination_points)
, m_matrix_distances(matrix_distances)
, m_processed_points(destination_points.size())
{
}
// добавляем планы на обработку
void VrpCrossoverTask::add_base_plan(const Plan* plan)
{
	m_base_plans.push_back(plan);
}
void VrpCrossoverTask::on_process()
{
	m_result_plans.clear();
	for (const Plan* plan : m_base_plans)
		m_result_plans.push_back(crossover(plan));

	// удаляем все поставленные планы
	m_base_plans.clear();
}
void VrpCrossoverTask::on_finish_process()
{
	emit finish(m_result_plans);
}
// "скрещивание" решений
SPlan VrpCrossoverTask::crossover(const Plan* original_plan)
{
	m_garage.reset();
	SPlan new_plan(new Plan());

	// кол-во хромосом для смешивания
	//int num_mixing = 2;
	int num_mixing = m_rand_gen() % 4 + 2;		// 2, 3, 4

	std::array<const Plan*, 5> merged_plans = { nullptr };
	merged_plans[0] = original_plan;
	for (int i = 1; i < num_mixing; ++i)
		merged_plans[i] = m_all_plans[m_rand_gen() % m_all_plans.size()].get();

	m_merged_routes.clear();
	// объединяем все решения
	for (int i = 0; i < num_mixing; ++i)
		for (const Route* route : merged_plans[i]->routes())
			m_merged_routes.push_back(route);

	// устанавливаем, что ниодна точка не обработана
	for (size_t i = 0; i < m_processed_points.size(); ++i)
		m_processed_points[i] = false;

	while (m_merged_routes.size() > 0)
	{
		// выбираем случайный маршрут
		int route_index = m_rand_gen() % m_merged_routes.size();
		const Route* route = m_merged_routes[route_index];
		// выдергиваем машину из гаража
		m_garage.take_car(route->car());

		// добавляем маршрут в план
		Route* new_route = new_plan->add_route(route->car());
		//*new_route = *route;

		{
			//int end = m_rand_gen() % route->points().size();
			//end++;
			//int start = m_rand_gen() % end;

			int start;
			int end;
			int range_type = m_rand_gen() % 6;
			if (range_type == 0)
			{
				start = 0;
				end = (m_rand_gen() % route->points().size());
				++end;
			}
			else if (range_type == 1)
			{
				start = m_rand_gen() % route->points().size();
				end = route->points().size();
			}
			else
			{
				start = m_rand_gen() % route->points().size();
				end = m_rand_gen() % route->points().size();

				if (start > end) std::swap(start, end);
				else ++end;
			}
			
			for (int i = start; i < end; ++i)
				new_route->add_point(route->points()[i]);

			new_route->update_distance(m_matrix_distances);
		}

		// отмечаем точки, которые обработали
		for (const RoutePoint& route_point : new_route->points())
		{
			assert( m_processed_points[route_point.index()] == false );
			m_processed_points[route_point.index()] = true;
		}
		// удаляем его
		remove_merged_route(route_index);

		// вычеркиваем все маршурты, в которых есть данная машина
		for (size_t i = 0; i < m_merged_routes.size(); )
		{
			if (new_route->car() == m_merged_routes[i]->car())
				remove_merged_route(i);
			else i++;
		}

		// вычеркиваем все маршурты, в которых встречаются точки из текущего маршрута
		for (size_t i = 0; i < m_merged_routes.size(); )
		{
			if (new_route->is_have_points(m_merged_routes[i]))
				remove_merged_route(i);
			else i++;
		}
	}

	// находим необработанные точки и вставляем их в наиболее выгодное место
	// перед этим их случайно перемешиваем

	m_indices.clear();
	for (size_t i = 1; i < m_processed_points.size(); ++i)
	{
		if (!m_processed_points[i])
			m_indices.push_back(i);
			//vrp_insert_point(*new_plan, i, m_destination_points, m_garage, m_matrix_distances);
	}
	{
		size_t count = m_indices.size();
		for (size_t i = 0; i < count; ++i)
			std::swap(m_indices[i], m_indices[m_rand_gen() % count]);
	}
	for (int index : m_indices)
		vrp_insert_point(*new_plan, index, m_destination_points, m_garage, m_matrix_distances);

		
	new_plan->update_distance_and_fitness();
	return new_plan;
}
// удалить маршрут из объединённых
void VrpCrossoverTask::remove_merged_route(int index)
{
	size_t new_size = m_merged_routes.size() - 1;
	// переставляем последний элемент на место удалённого
	m_merged_routes[index] = m_merged_routes[new_size];
	// уменьшаем массив
	m_merged_routes.resize(new_size);
}

}