//=====================================================================================//
//   Author: open
//   Date:   11.08.2018
//=====================================================================================//
#pragma once
#include "vrp_algorithm/vrp_data.h"
#include "base/thread_task.h"

namespace Vrp
{

class VrpCrossoverTask : public ThreadTask
{
	Q_OBJECT
public:
	using result_plans_t = std::vector<SPlan>;

private:
	const destination_points_t& m_destination_points;
	const MatrixDistances& m_matrix_distances;
	
	using plans_t = std::vector<SPlan>;
	const plans_t& m_all_plans;					// все планы популяции
	Garage m_garage;							// гараж с машинами
	std::vector<const Plan*> m_base_plans;		// базовые планы
	result_plans_t m_result_plans;				// скрещенные планы
	std::vector<const Route*> m_merged_routes;	// объединЄнные маршруты
	std::vector<bool> m_processed_points;		// обработанные точки
	boost::random::mt19937 m_rand_gen;

	std::vector<int> m_indices;

public:
	//VrpCrossoverTask(const plans_t& all_plans, const available_cars_t& available_cars, const destination_points_t& destination_points, const MatrixDistances& matrix_distances);
	VrpCrossoverTask(const plans_t& all_plans, const Garage& garage, const destination_points_t& destination_points, const MatrixDistances& matrix_distances);
	// добавляем планы на обработку
	void add_base_plan(const Plan* plan);
	// установить рандомный seed
	void set_seed(std::uint32_t seed) { m_rand_gen.seed(seed); }
	
	// !!! not in main thread
	virtual void on_process();
	// in main thread!!!
	virtual void on_finish_process();

signals:
	void finish(const VrpCrossoverTask::result_plans_t& result_plans);

private:
	// "скрещивание" решений
	SPlan crossover(const Plan* plan);
	// удалить маршрут из объединЄнных
	void remove_merged_route(int index);
};

using SVrpCrossoverTask = std::shared_ptr<VrpCrossoverTask>;

}