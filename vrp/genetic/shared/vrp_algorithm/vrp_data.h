//=====================================================================================//
//   Author: open
//   Date:   03.08.2018
//=====================================================================================//
#pragma once
#include "base/matrix_distances.h"
#include <boost/random.hpp>
#include <QPointF>

namespace Vrp
{

class CarDesc
{
public:
	int type = -1;						// тип машины
	float max_volume;					// максимальный объём
	float max_weight;					// грузоподъёмность
	distance_t cost_km;					// стоимость км
	distance_t cost_destination_point;	// стоимость точки
	distance_t cost_trip;				// стоимость рейса
	int max_points;						// максимальное кол-во точек пути для данной машины
	int available_count;

	CarDesc(float max_volume, float max_weight, distance_t cost_km, distance_t cost_destination_point, distance_t cost_trip, int max_points, int available_count)
	: max_volume(max_volume)
	, max_weight(max_weight)
	, cost_km(cost_km)
	, cost_destination_point(cost_destination_point)
	, cost_trip(cost_trip)
	, max_points(max_points)
	, available_count(available_count)
	{
	}
};

using SCarDesc = std::shared_ptr<CarDesc>;
using available_cars_t = std::vector<SCarDesc>;

class Car
{
public:
	SCarDesc car_desc;
	int index;
};

inline bool operator == (const Car& left_car, const Car& right_car) {
	return left_car.car_desc == right_car.car_desc && left_car.index == right_car.index;
}


class DestinationPoint
{
public:
	QPointF position;
	
	float volume;	// объем
	float weight;	// вес

	int city_id;
	
	DestinationPoint() {}
	DestinationPoint(const QPointF& position, float volume, float weight, int city_id) : position(position), volume(volume), weight(weight), city_id(city_id) {}
};
using destination_points_t = std::vector<DestinationPoint>;


class RoutePoint
{
private:
	int m_index;
	const DestinationPoint* m_destination_point;
	// temp vars
public:
	RoutePoint() {}
	RoutePoint(int index, const DestinationPoint* destination_point) : m_index(index), m_destination_point(destination_point)
	{
	}
	const QPointF& position() const { return m_destination_point->position; }
	int index() const { return m_index; }
	float volume() const { return m_destination_point->volume; }
	float weight() const { return m_destination_point->weight; }
};


class Route
{
public:
	using points_t = std::vector<RoutePoint>;

private:
	points_t m_points;

	Car m_car;
	float m_volume = 0.f;			// текущая загрузка объёмом
	float m_weight = 0.f;			// текущая загрузка весом
	distance_t m_distance = 0.f;

public:

	Route(const Car& car) : m_car(car) { }
	
	bool is_may_add_point(const DestinationPoint& destination_point) const;
	const points_t& points() const { return m_points; }

	void insert_point(const RoutePoint& route_point, int insert_index);
	void add_point(const RoutePoint& route_point);
	void remove_point(int index);
	void clear_points() { m_points.clear(); }

	void update_distance(const MatrixDistances& matrix_distances);
	distance_t distance() const { return m_distance; }
	
	distance_t fitness(distance_t distance) const;
	distance_t fitness() const;

	//float weight() const;
	//float volume() const;

	float weight() const { return m_weight; }
	float volume() const { return m_volume; }

	const Car& car() const { return m_car; }

	// !!! крайне медленная функция
	bool is_have_points(const Route* route) const;
	// !!! крайне медленная функция
	bool is_have_point_index(int index) const;

private:
	distance_t calculate_distance(const MatrixDistances& matrix_distances) const;
};


class Plan
{
public:
	using routes_t = std::vector<Route*>;
private:
	routes_t m_routes;
	distance_t m_distance = 0.f;
	distance_t m_fitness = 0.f;

public:
	Plan() {}
	Plan(const Plan& src);
	~Plan();
	Route* add_route(const Car& car);
	void remove_route(int index);
	const routes_t& routes() const { return m_routes; }
	void update_distance_and_fitness();
	distance_t fitness() const { return m_fitness; }
	distance_t distance() const { return m_distance; }
	int count_points() const;
};
using SPlan = std::shared_ptr<Plan>;


class Garage
{
private:
	available_cars_t m_available_cars;

	struct CarCount
	{
		SCarDesc car_desc;
		int count;
	};
	using cars_t = std::vector<CarCount>;
	cars_t m_cars;

	boost::random::mt19937 m_rand_gen;

public:
	Garage(const available_cars_t& available_cars, unsigned int seed);
	void reset();
	
	bool is_have_car() const;
	Car get_random_car();

	void take_car(const Car& car);
};


struct Data
{
	available_cars_t available_cars;
	destination_points_t destination_points;
	SMatrixDistances matrix_distances;
	unsigned int base_random_seed = 0;
	int random_seed = 0;
	int num_challenges = 0;
	int population_size = 1024;
	int cycle_count = 1200;
	int halt_cycle_count = 160;
	int halt_time = 600;

	bool is_init() const { return destination_points.size() > 0 && available_cars.size() > 0; }
};

}