//=====================================================================================//
//   Author: open
//   Date:  11.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "vrp_insert_point.h"

namespace Vrp
{

distance_t calc_distance(int insert_index, int point_index, const Route* route, const distance_t current_distance, const MatrixDistances& matrix_distances)
{
	const Route::points_t& points = route->points();

	distance_t distance = current_distance;
	if (insert_index == 0)
	{
		int first_index = points[0].index();
		// если проверяется вставка точки в самое начало после склада
		distance_t old_distance = matrix_distances(0, first_index).distance;
		distance -= old_distance;

		distance_t new_distance = matrix_distances(0, point_index).distance + matrix_distances(point_index, first_index).distance;
		distance += new_distance;
	}
	else if (insert_index == points.size())
	{
		int last_index = points[points.size() - 1].index();
		// если проверка в конце
		//if (route->car()->our)
		//{
		distance_t old_distance = matrix_distances(last_index, 0).distance;
		distance -= old_distance;

		distance_t new_distance = matrix_distances(last_index, point_index).distance + matrix_distances(point_index, 0).distance;
		distance += new_distance;
		//}
		//else
		//{
		//			distance_t new_distance = m_matrix_distances(last_index, point_index);
		//			distance += new_distance;
		//}
	}
	else
	{
		int current_index = points[insert_index - 1].index();
		int next_index = points[insert_index].index();

		distance_t old_distance = matrix_distances(current_index, next_index).distance;
		distance -= old_distance;

		distance_t new_distance = matrix_distances(current_index, point_index).distance + matrix_distances(point_index, next_index).distance;
		distance += new_distance;
	}
	return distance;
}

// добавить точку в план
void vrp_insert_point(Plan& plan, int index, const destination_points_t& destination_points, Garage& garage, const MatrixDistances& matrix_distances)
{
	const DestinationPoint& destination_point = destination_points[index];
	Route* best_route = nullptr;
	distance_t best_fitness_difference = std::numeric_limits<distance_t>::max();
	int best_insert_index = 0;
	// проходим по всем маршрутам
	for (Route* route : plan.routes())
	{
		// если можно ещё добавить груз
		if (route->is_may_add_point(destination_point))
		{
			// ищем меньшую разницу фитнесс-функции на маршрут
			for (int i = 0; i < static_cast<int>(route->points().size()) + 1; ++i)
			{
				// ищем лучшее место для вставки между точками доставки
				distance_t new_distance = calc_distance(i, index, route, route->distance(), matrix_distances);
				distance_t new_fitness = route->fitness(new_distance);
				distance_t fitness_difference = new_fitness - route->fitness();

				if (fitness_difference < best_fitness_difference)
				{
					best_fitness_difference = fitness_difference;
					best_insert_index = i;
					best_route = route;
				}
			}
		}
	}

	RoutePoint route_point(index, &destination_point);
	if (best_route)
		best_route->insert_point(route_point, best_insert_index);
	else
	{
		// создаем новый маршрут
		while (garage.is_have_car())
		{
			Car car = garage.get_random_car();
			// если не нашли маршрут в который можно добавить точку, то создаём новый маршрут
			Route* route = plan.add_route(car);

			// проверяем на вместимость
			if (route->is_may_add_point(destination_point))
			{
				best_route = route;
				best_route->insert_point(route_point, 0);
				break;
			}

			// превышает допустимый объём/вес всей машины!!!
			
			
			// необходимо исправить!!!!
			plan.remove_route(plan.routes().size() - 1);
			//logs("order > car volume or weight");
		}

		if (!best_route)
		{
			// машины кончились
			logs("garage empty");
		}
	}
	if (best_route)
		best_route->update_distance(matrix_distances);
}

}