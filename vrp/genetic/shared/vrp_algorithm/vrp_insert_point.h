//=====================================================================================//
//   Author: open
//   Date:   11.08.2018
//=====================================================================================//
#pragma once
#include "vrp_algorithm/vrp_data.h"

namespace Vrp
{

// добавить точку в план
extern void vrp_insert_point(Plan& plan, int index, const destination_points_t& destination_points, Garage& garage, const MatrixDistances& matrix_distances);

}