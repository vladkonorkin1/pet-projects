﻿//=====================================================================================//
//   Author: Solari4555
//   Date:   22.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "vrp_json.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

namespace Vrp
{

bool json_read_data(const QString& filename, Data& data)
{
	QFile file(filename);
	if (file.open(QIODevice::ReadOnly))
	{
		json_parse_data(QJsonDocument::fromJson(file.readAll()), data);
		return true;
	}
	return false;
}
void json_parse_data(const QJsonDocument& json_doc, Data& data)
{
	QJsonObject json_doc_obj = json_doc.object();

	QJsonObject json_settings = json_doc_obj["settings"].toObject();
	data.base_random_seed = json_settings["base_random_seed"].toInt();
	if (json_settings.contains("num_challenges"))
		data.num_challenges = std::max(json_settings["num_challenges"].toInt(), 1);
	if (json_settings.contains("population_size"))
		data.population_size = std::max(json_settings["population_size"].toInt(), 1);
	if (json_settings.contains("cycle_count"))
		data.cycle_count = std::max(json_settings["cycle_count"].toInt(), 1);
	if (json_settings.contains("halt_cycle_count"))
		data.halt_cycle_count = std::max(json_settings["halt_cycle_count"].toInt(), 1);
	if (json_settings.contains("halt_time"))
		data.halt_time = std::max(json_settings["halt_time"].toInt(), 0);
	if (json_settings.contains("random_seed"))
		data.random_seed = std::max(json_settings["random_seed"].toInt(), 0);


	QJsonArray cars_array = json_doc_obj["cars"].toArray();
	for (const auto& v : cars_array)
	{
		QJsonObject json_obj = v.toObject();
		float max_volume = json_obj["max_volume"].toDouble();
		float max_weight = json_obj["max_weight"].toDouble();
		distance_t cost_km = json_obj["cost_km"].toDouble();
		distance_t cost_destination_point = json_obj["cost_destination_point"].toDouble();
		distance_t cost_trip = json_obj["cost_trip"].toDouble();
		int max_points = json_obj["max_points"].toInt();
		int available_count = json_obj["available_count"].toInt();
		data.available_cars.push_back(SCarDesc(new CarDesc(max_volume, max_weight, cost_km, cost_destination_point, cost_trip,max_points, available_count)));
	}

	QJsonArray point_array = json_doc_obj["points"].toArray();
	/*for (const auto& v : point_array)
	{
		QJsonObject json_obj = v.toObject();
		qreal lat = json_obj["lat"].toDouble();
		qreal lon = json_obj["lon"].toDouble();
		float volume = json_obj["volume"].toDouble();
		float weight = json_obj["weight"].toDouble();
		destination_points.push_back(DestinationPoint(QPointF(lon, lat), volume, weight));
	}*/
	for (int i = 0; i < point_array.size(); ++i)
	{
		const auto& v = point_array[i];
		QJsonObject json_obj = v.toObject();
		qreal lat = json_obj["lat"].toDouble();
		qreal lon = json_obj["lon"].toDouble();
		float volume = json_obj["volume"].toDouble();
		float weight = json_obj["weight"].toDouble();
		int city_id = json_obj["city"].toInt();
		//if (i == 0  || lat > 62.361653)
		data.destination_points.push_back(DestinationPoint(QPointF(lon, lat), volume, weight, city_id));
	}
}
QJsonArray point_indices_to_json(const Route* route) 
{
	QJsonArray json_array;
	for (const RoutePoint& route_point : route->points())
		json_array.push_back(route_point.index());
	return json_array;
}
QJsonDocument plan_to_json(const SPlan& plan, int num_cycles)
{
	QJsonDocument doc;
	QJsonObject json_plan;
	json_plan["result"] = (plan && !plan->routes().empty()) ? "ok" : "error";
	
	if (plan)
	{
		QJsonArray json_routes;
		for (const Route* route : plan->routes())
		{
			QJsonObject json_route;
			json_route["car_index"] = route->car().car_desc->type;
			json_route["volume"] = route->volume();
			json_route["weight"] = route->weight();
			json_route["distance"] = route->distance();
			json_route["fitness"] = route->fitness();
			json_route["point_indices"] = point_indices_to_json(route);
			json_routes.append(json_route);
		}
		json_plan["routes"] = json_routes;
		json_plan["count_points"] = plan->count_points();
		json_plan["fitness"] = plan->fitness();
		json_plan["distance"] = plan->distance();
		json_plan["num_cycles"] = num_cycles;
	}
	doc.setObject(json_plan);
	return doc;
}
void json_parse_route(const QJsonObject& json_route, const Data& data, SPlan& plan)
{
	int car_index = json_route["car_index"].toInt();

	Car car{ data.available_cars[car_index], car_index };
	Route* route = plan->add_route(car);
	for (const auto& json_indices : json_route["point_indices"].toArray())
	{
		int index = json_indices.toInt();
		route->add_point(RoutePoint(index, &(data.destination_points[index])));
	}

	route->update_distance(*data.matrix_distances);
}
SPlan json_parse_plan(const QJsonDocument& json_doc, const Data& data)
{
	SPlan plan(new Plan());
	QJsonArray json_routes = json_doc.object()["routes"].toArray();
	for (const auto& json_route : json_routes)
		json_parse_route(json_route.toObject(), data, plan);

	plan->update_distance_and_fitness();
	return plan;
}
SPlan json_read_plan(const QString& filename, const Data& data)
{
	QFile file(filename);
	if (file.open(QIODevice::ReadOnly))
		return json_parse_plan(QJsonDocument::fromJson(file.readAll()), data);
	return SPlan();
}
distance_t json_parse_plan_fitness(const QJsonDocument& json_doc)
{
	return json_doc["fitness"].toDouble();
}
int json_parse_plan_num_cycles(const QJsonDocument& json_doc)
{
	return json_doc["num_cycles"].toInt();
}

}