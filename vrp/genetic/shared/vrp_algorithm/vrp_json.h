//=====================================================================================//
//   Author: Solari4555
//   Date:   22.09.2018
//=====================================================================================//
#pragma once
#include "vrp_algorithm/vrp_data.h"
#include <QJsonDocument>

namespace Vrp
{

extern bool json_read_data(const QString& filename, Data& data);
extern void json_parse_data(const QJsonDocument& json_doc, Data& data);
//extern bool json_write_plan(const QString& filename, const SPlan& plan);
extern QJsonDocument plan_to_json(const SPlan& plan, int num_cycles);
SPlan json_read_plan(const QString& filename, const Data& data);
extern SPlan json_parse_plan(const QJsonDocument& json_doc, const Data& data);
extern distance_t json_parse_plan_fitness(const QJsonDocument& json_doc);
extern int json_parse_plan_num_cycles(const QJsonDocument& json_doc);

}
