//=====================================================================================//
//   Author: open
//   Date:  11.08.2018
//=====================================================================================//
#include "base/precomp.h"
#include "vrp_mutation_task.h"
#include "vrp_insert_point.h"

namespace Vrp
{

VrpMutationTask::VrpMutationTask(const Garage& garage, const destination_points_t& destination_points, const MatrixDistances& matrix_distances)
: m_garage(garage)
, m_destination_points(destination_points)
, m_matrix_distances(matrix_distances)
{
}
// добавляем планы на обработку
void VrpMutationTask::add_base_plan(const Plan* plan)
{
	m_base_plans.push_back(plan);
}
// !!! not in main thread
void VrpMutationTask::on_process()
{
	m_result_plans.clear();
	for (const Plan* plan : m_base_plans)
	{
		m_result_plans.push_back(mutate_remove_random_routes(plan));
		m_result_plans.push_back(mutate_remove_random_points(plan));
	}
	// удаляем все поставленные планы
	m_base_plans.clear();
}
// in main thread!!!
void VrpMutationTask::on_finish_process()
{
	emit finish(m_result_plans);
}
SPlan VrpMutationTask::mutate_remove_random_routes(const Plan* original_plan)
{
	SPlan new_plan(new Plan(*original_plan));
	m_garage.reset();
	m_indices.clear();

	assert( !new_plan->routes().empty() );

	//int tt = (new_plan->routes().size() - 1);// *0.75;
	int tt = new_plan->routes().size() * 0.8;
	tt = std::min<int>(new_plan->routes().size() - 1, tt);
	tt = std::max(1, tt);
	int num_deleted_routes = (m_rand_gen() % tt) + 1;
	for (int i = 0; i < num_deleted_routes; ++i)
	{
		const Plan::routes_t& routes = new_plan->routes();
		// выбираем один случайный маршрут
		int route_index = m_rand_gen() % routes.size();
		// копируем индексы точек
		for (const RoutePoint& route_point : routes[route_index]->points())
			m_indices.push_back(route_point.index());
		// удаляем маршрут
		new_plan->remove_route(route_index);
	}
	// удаляем из гаража машины
	for (const Route* route : new_plan->routes())
		m_garage.take_car(route->car());
	
	// перемешиваем точки
	{
		size_t count = m_indices.size();
		for (size_t i = 0; i < count; ++i)
			std::swap(m_indices[i], m_indices[m_rand_gen() % count]);
	}
	// размещаем точки в маршрутах плана
	for (int point_index : m_indices)
		vrp_insert_point(*new_plan, point_index, m_destination_points, m_garage, m_matrix_distances);

	new_plan->update_distance_and_fitness();
	return new_plan;
}
SPlan VrpMutationTask::mutate_remove_random_points(const Plan* original_plan)
{
	SPlan new_plan(new Plan(*original_plan));

	m_garage.reset();
	m_indices.clear();
	const Plan::routes_t& routes = new_plan->routes();

	// кол-во точек для удаления
	int num_remove_points = (m_rand_gen() % (routes.size() * 1)) + 1;
	//m_destination_points
	//int num_remove_points = (m_rand_gen() % (routes.size() * 4) - 1) + 1;

	//int num_remove_points = (m_rand_gen() % num_average_points - 1) + 1;
	for (int i = 0; i < num_remove_points; ++i)
	{
		if (routes.empty()) break;

		int route_index = m_rand_gen() % (routes.size() * 1);
		Route* route = routes[route_index];
		const Route::points_t& points = route->points();
		assert( !points.empty() );

		int point_index = m_rand_gen() % points.size();
		m_indices.push_back(points[point_index].index());

		// удаляем точку
		route->remove_point(point_index);
		if (route->points().empty())
		{
			// если точек больше нет, то удаляем маршрут
			new_plan->remove_route(route_index);
		}
	}

	// удаляем из гаража машины
	for (const Route* route : routes)
		m_garage.take_car(route->car());

	// перемешиваем точки
	{
		size_t count = m_indices.size();
		for (size_t i = 0; i < count; ++i)
			std::swap(m_indices[i], m_indices[m_rand_gen() % count]);
	}
	// размещаем точки в маршрутах плана
	for (int point_index : m_indices)
		vrp_insert_point(*new_plan, point_index, m_destination_points, m_garage, m_matrix_distances);

	new_plan->update_distance_and_fitness();
	return new_plan;
}

}