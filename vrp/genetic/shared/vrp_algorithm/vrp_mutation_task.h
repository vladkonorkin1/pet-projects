//=====================================================================================//
//   Author: open
//   Date:   11.08.2018
//=====================================================================================//
#pragma once
#include "vrp_algorithm/vrp_data.h"
#include "base/thread_task.h"

namespace Vrp
{

class VrpMutationTask : public ThreadTask
{
	Q_OBJECT
public:
	using result_plans_t = std::vector<SPlan>;

private:
	const destination_points_t& m_destination_points;
	const MatrixDistances& m_matrix_distances;
	std::vector<int> m_indices;
	Garage m_garage;							// гараж с машинами
	std::vector<const Plan*> m_base_plans;		// базовые планы
	result_plans_t m_result_plans;				// скрещенные планы
	boost::random::mt19937 m_rand_gen;

public:
	VrpMutationTask(const Garage& garage, const destination_points_t& destination_points, const MatrixDistances& matrix_distances);
	// добавляем планы на обработку
	void add_base_plan(const Plan* plan);
	// установить рандомный seed
	//void set_seed(std::uint32_t seed) { m_rand_gen.seed(seed); }
	void set_seed(std::uint32_t seed) { m_rand_gen = boost::random::mt19937{ seed }; }

	// !!! not in main thread
	virtual void on_process();
	// in main thread!!!
	virtual void on_finish_process();

signals:
	void finish(const VrpMutationTask::result_plans_t& result_plans);

private:
	SPlan mutate_remove_random_routes(const Plan* original_plan);
	SPlan mutate_remove_random_points(const Plan* original_plan);
};

using SVrpMutationTask = std::shared_ptr<VrpMutationTask>;

}