//=====================================================================================//
//   Author: open
//   Date:   03.08.2018
//=====================================================================================//
#pragma once
#include "vrp_algorithm/vrp_data.h"
#include "base/random_indices.h"

namespace Vrp
{

class VrpSolver
{
private:
	using plans_t = std::vector<SPlan>;
	plans_t m_plans;

	MatrixDistances m_matrix_distances;

public:
	// рассчитать оптимальный план
	SPlan calculate(const available_cars_t& available_cars, const destination_points_t& destination_points, const MatrixDistances& matrix_distances);

private:
	// создать попул¤цию
	void create_population(int population_size, Garage& garage, const destination_points_t& destination_points);
	SPlan build_plan(Garage& garage, const destination_points_t& destination_points, RandomIndices& random_indices_points) const;
	// расчЄт фитнес-функции при вставке между точек
	//float calc_distance(int insert_index, int point_index, const Route* route, const float current_distance) const;
	// "скрещивание" решений
	SPlan crossover(const SPlan& plan, Garage& garage, const destination_points_t& destination_points);
	// добавить точку в план
	//void insert_point_in_plan(SPlan& plan, int index, const destination_points_t& destination_points, Garage& garage) const;
	// мутаци¤ решени¤
	//void mutation(SPlan& plan, std::vector<int>& cache_point_indices, const destination_points_t& destination_points, Garage& garage) const;
	SPlan mutate1(const SPlan& original_plan, std::vector<int>& cache_point_indices, const destination_points_t& destination_points, Garage& garage) const;
	SPlan mutate2(const SPlan& original_plan, std::vector<int>& cache_point_indices, const destination_points_t& destination_points, Garage& garage) const;
};

}