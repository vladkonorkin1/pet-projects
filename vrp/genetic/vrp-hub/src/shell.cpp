//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "shell.h"

Shell::Shell()
//: m_solver_manager(m_config.value("solver/osrm-server-address", "127.0.0.1:5000"), m_config.value("hub/solver-path", "solver"))
: m_ticket_manager(m_config.value("hub/solver-path", "solver"))
, m_session_id(QUuid::createUuid())
{
	//connect(&m_solver_manager, SIGNAL(ticked_solved(const QUuid&, bool, const QJsonDocument&)), SLOT(slot_ticked_solved(const QUuid&, bool, const QJsonDocument&)));
	connect(&m_ticket_manager, SIGNAL(ticked_solved(const QUuid&, bool, const QJsonDocument&)), SLOT(slot_ticked_solved(const QUuid&, bool, const QJsonDocument&)));
	reconnect();
}
void Shell::reconnect()
{
	logs("net gate: connecting...");
	m_net_gate_connection.reset(new NetGate::Connection());
	m_net_gate_connection->set_message_listener(this);
	connect(m_net_gate_connection.get(), SIGNAL(disconnected()), SLOT(slot_net_gate_connection_disconnected()));
	connect(m_net_gate_connection.get(), SIGNAL(connected()), SLOT(slot_net_gate_connection_connected()));
	//m_net_gate_connection->connect_host("127.0.0.1");
	//m_net_gate_connection->connect_host("10.171.0.225");
	//m_net_gate_connection->connect_host("88.86.193.234");
	m_net_gate_connection->connect_host(m_config.value("hub/web-gate-address", "127.0.0.1"));
}
void Shell::slot_ticked_solved(const QUuid& ticket_id, bool result, const QJsonDocument& json_result)
{
	send_message(NetGate::ClientMessageTaskSolved(ticket_id, result, json_result));
}
void Shell::slot_net_gate_connection_disconnected()
{
	logs("net gate: disconnected");
	// удаляем соединение "вручную" с задержкой
	NetGate::Connection* connection = m_net_gate_connection.release();
	connection->deleteLater();

	// переподключаемся
	QTimer::singleShot(6000, this, SLOT(reconnect()));

	// очищаем все рабочие данные
	//clear_data();
}
#include <QHostInfo>
void Shell::slot_net_gate_connection_connected()
{
	logs("net gate: connected");
	send_message(NetGate::ClientMessageAutorizate(m_session_id, QHostInfo::localHostName()));
}
void Shell::send_message(const NetMessage& msg)
{
	if (m_net_gate_connection)
		m_net_gate_connection->send_message(msg);
}
// сообщение о результате авторизации
void Shell::on_message(const NetGate::ServerMessageAutorizateResponse& msg)
{
	Q_UNUSED(msg);
	// если обнаружена новая сессия, то очищаем все данные
	//if (msg.new_session)
	//	clear_data();
}
// сообщение от сервера: передай результаты выполненных задач
void Shell::on_message(const NetGate::ServerMessageReportSolvedTasks& msg)
{
	Q_UNUSED(msg);
	m_ticket_manager.report_all_solved_tickets();
	//m_solver_manager.report_all_solved();
}
// сообщение от сервера об открытии тикета (после реконнекта придёт повторно!!!)
void Shell::on_message(const NetGate::ServerMessageOpenTask& msg)
{
	logs("net gate: open task " + msg.task_id.toString());
	m_ticket_manager.add_ticket(msg.task_id, msg.json_doc, msg.matrix_distances);
	//m_solver_manager.add_task(msg.ticket_id, msg.json_doc);
	//QTimer::singleShot(10000, this, [this, ticket_id = msg.ticket_id]() { slot_send_ticket_solved(ticket_id); });
}
// сообщение от сервера о закрытии тикетов
void Shell::on_message(const NetGate::ServerMessageCloseTask& msg)
{
	logs("net gate: close task " + msg.task_id.toString());
	m_ticket_manager.close_ticket(msg.task_id);
	//m_solver_manager.close_ticket(msg.ticket_id);
}
