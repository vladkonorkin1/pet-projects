//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#pragma once
#include "net_gate_connection.h"
#include "net_gate/net_gate_handler_messages.h"
//#include "solvermanager.h"
#include "ticket_manager.h"
#include "base/config.h"

class Shell : public QObject, public NetGate::ClientHandlerMessages
{
	Q_OBJECT
private:
	Config m_config;
	//SolverManager m_solver_manager;
	TicketManager m_ticket_manager;
	QUuid m_session_id;
	NetGate::UConnection m_net_gate_connection;

public:
	explicit Shell();

protected:
	virtual void on_message(const NetGate::ServerMessageAutorizateResponse& msg);
	virtual void on_message(const NetGate::ServerMessageReportSolvedTasks& msg);
	virtual void on_message(const NetGate::ServerMessageOpenTask& msg);
	virtual void on_message(const NetGate::ServerMessageCloseTask& msg);

private slots:
	void slot_net_gate_connection_disconnected();
	void slot_net_gate_connection_connected();
	void reconnect();
	void slot_ticked_solved(const QUuid& ticket_id, bool result, const QJsonDocument& json_result);

private:
	void send_message(const NetMessage& msg);
};
