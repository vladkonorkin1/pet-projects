#include "base/precomp.h"
#include "ticket_manager.h"
#include <QFileInfo>

TicketManager::TicketManager(const QString& solver_path)
: m_solver_path(solver_path)
{
	connect(&m_server, SIGNAL(connection_created(NetHub::Connection*)), SLOT(slot_connection_connected(NetHub::Connection*)));
}
void TicketManager::slot_connection_connected(NetHub::Connection* connection)
{
	connect(connection, SIGNAL(authorized(const QString&)), this, SLOT(slot_connection_authorized(const QString&)));
	//connect(connection, SIGNAL(disconnected(NetHub::Connection*)), this, SLOT(slot_disconnected(NetHub::Connection*)));
}
void TicketManager::slot_connection_authorized(const QString& solver_key)
{
	NetHub::Connection* connection = qobject_cast<NetHub::Connection*>(sender());
	QUuid ticket_id = QUuid::fromString(solver_key);
	connect(connection, &NetHub::Connection::ticket_solved, [this, ticket_id](const QJsonDocument& json_result) { slot_ticket_solved(ticket_id, json_result); });
	runned_tickets_t::iterator it = m_runned_tickets.find(ticket_id);
	if (m_runned_tickets.end() != it)
	{
		it->connection = connection;
		connection->send_message(NetHub::ServerMessageOpenTicket(it->ticket->json_task, it->ticket->matrix_distances));
	}
}
void TicketManager::add_ticket(const QUuid& ticket_id, const QJsonDocument& json_task, const SMatrixDistances& matrix_distances)
{
	tickets_t::iterator it = m_tickets.find(ticket_id);
	if (m_tickets.end() == it)
	{
		STicket ticket(new Ticket(ticket_id, json_task, matrix_distances));
		m_tickets[ticket_id] = ticket;
		m_new_tickets.enqueue(ticket.get());

		run_next_ticket();
	}
}
void TicketManager::close_ticket(const QUuid& ticket_id)
{
	tickets_t::iterator it = m_tickets.find(ticket_id);
	if (m_tickets.end() != it)
	{
		Ticket* ticket = it->get();
		if (Ticket::State::New == ticket->state)
		{
			// убираем из очереди новых задач
			for (new_tickets_t::iterator it = m_new_tickets.begin(); it != m_new_tickets.end(); it++)
			{
				if (*it == ticket)
				{
					m_new_tickets.erase(it);
					break;
				}
			}
		}
		else if (Ticket::State::Runned == ticket->state)
		{
			// убираем из очереди выполняемых задач
			runned_tickets_t::iterator it = m_runned_tickets.find(ticket_id);
			if (m_runned_tickets.end() != it)
			{
				it->ticket->state = Ticket::State::Finished;
				it->process->close();
			}
		}
		m_tickets.erase(it);
	}
}
// отчёт о всех решённых задачах
void TicketManager::report_all_solved_tickets()
{
	for (const STicket& ticket : m_tickets)
	{
		if (ticket->state == Ticket::State::Finished)
			emit ticked_solved(ticket->id, ticket->ok, ticket->json_result);
	}
}
// сообщение от solver-a о решении задачи
void TicketManager::slot_ticket_solved(const QUuid& ticket_id, const QJsonDocument& json_result)
{
	finish_ticket(ticket_id, true, json_result);
	run_next_ticket();
}
void TicketManager::slot_process_finished(const QUuid& ticket_id, int result)
{
	finish_ticket(ticket_id, false, QJsonDocument());
	
	runned_tickets_t::iterator it = m_runned_tickets.find(ticket_id);
	if (m_runned_tickets.end() != it)
		m_runned_tickets.erase(it);

	run_next_ticket();
}
void TicketManager::finish_ticket(const QUuid& ticket_id, bool ok, const QJsonDocument& json_result)
{
	runned_tickets_t::iterator it = m_runned_tickets.find(ticket_id);
	if (m_runned_tickets.end() != it)
	{
		Ticket* ticket = it->ticket;
		if (ticket->state == Ticket::State::Runned)
		{
			ticket->json_result = json_result;
			ticket->ok = ok;
			emit ticked_solved(ticket_id, ok, json_result);

			ticket->state = Ticket::State::Finished;
			it->process->close();
			//if (ok) it->connection->send_message(NetHub::ServerMessageExitProcess());
		}
	}
}
void TicketManager::run_next_ticket()
{
	if (m_runned_tickets.size() < 4)
	{
		if (!m_new_tickets.empty())
			if (Ticket* ticket = m_new_tickets.dequeue())
				start_process(ticket);
	}
}
void TicketManager::start_process(Ticket* ticket)
{
	std::shared_ptr<QProcess> process(new QProcess(), [this](QProcess* process) { process->deleteLater(); });
	QStringList arguments{ m_server.server_name(), ticket->id.toString() };
	process->setWorkingDirectory(QFileInfo(m_solver_path).path());
	process->start(m_solver_path, arguments);
	connect(process.get(), static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), [this, ticket_id = ticket->id](int code, QProcess::ExitStatus status) { slot_process_finished(ticket_id, code); });
	if (process->waitForStarted())
	{
		ticket->state = Ticket::State::Runned;
		m_runned_tickets.insert(ticket->id, RunnedTicket(ticket, process));
	}
	else 
		emit ticked_solved(ticket->id, false, QJsonDocument());
}