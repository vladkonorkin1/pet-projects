#pragma once
#include "net_hub/net_hub_server.h"
#include "base/matrix_distances.h"
#include <QJsonDocument>
#include <QProcess>
#include <QQueue>

struct Ticket
{
	enum class State
	{
		New,
		Runned,
		Finished,
	};

	QUuid id;
	const QJsonDocument json_task;
	SMatrixDistances matrix_distances;
	bool ok;
	QJsonDocument json_result;
	State state;

	Ticket(const QUuid& id, const QJsonDocument& json_task, const SMatrixDistances& matrix_distances) : id(id), json_task(json_task), matrix_distances(matrix_distances), ok(false), state(State::New) {}
};
using STicket = std::shared_ptr<Ticket>;

struct RunnedTicket
{
	Ticket* ticket;
	std::shared_ptr<QProcess> process;
	NetHub::Connection* connection;

	RunnedTicket(Ticket* ticket, const std::shared_ptr<QProcess>& process) : ticket(ticket), process(process), connection(nullptr) {}
};


class TicketManager : public QObject
{
	Q_OBJECT
private:
	NetHub::Server m_server;
	const QString m_solver_path;

	using tickets_t = QMap<QUuid, STicket>;
	tickets_t m_tickets;

	using new_tickets_t = QQueue<Ticket*>;
	new_tickets_t m_new_tickets;

	using runned_tickets_t = QMap<QUuid, RunnedTicket>;
	runned_tickets_t m_runned_tickets;

public:
	TicketManager(const QString& solver_path);
	void add_ticket(const QUuid& ticket_id, const QJsonDocument& json_task, const SMatrixDistances& matrix_distances);
	void close_ticket(const QUuid& ticket_id);
	void report_all_solved_tickets();

signals:
	void ticked_solved(const QUuid& ticket_id, bool result, const QJsonDocument& json_result);

private slots:
	void slot_connection_connected(NetHub::Connection* connection);
	void slot_connection_authorized(const QString& str_solver_key);
	void slot_process_finished(const QUuid& ticket_id, int result);

private: // internal lambda
	void slot_ticket_solved(const QUuid& ticket_id, const QJsonDocument& json_result);

private:
	void finish_ticket(const QUuid& ticket_id, bool ok, const QJsonDocument& json_result);
	void start_process(Ticket* ticket);
	void run_next_ticket();
};