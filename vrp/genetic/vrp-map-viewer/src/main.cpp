//=====================================================================================//
//   Author: open
//   Date:   31.07.2018
//=====================================================================================//
#include <QApplication>
#include <QMessageBox>
#include "shell.h"
#include <time.h>

int main(int argc, char *argv[])
{
	Log::init();
	//Q_INIT_RESOURCE(project);
	//QCoreApplication::addLibraryPath(".");
	//QApplication::setStyle("fusion");
	QApplication app(argc, argv);
	try
	{
		Shell shell;
		shell.activate();
		return app.exec();
	}
	catch (const std::exception& e)	{
		QMessageBox::critical(0, "Exception", e.what());
	}
	return -1;
}