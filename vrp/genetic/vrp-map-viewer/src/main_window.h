//=====================================================================================//
//   Author: open
//   Date:   01.08.2018
//=====================================================================================//
#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H
#include "ui_main_window.h"
#include <QVector>

class QPushButton;
class MapViewer;

class MapControlWidget : public QWidget
{
private:
	MapViewer* m_map_viewer;

public:
	MapControlWidget(MapViewer* map_viewer);

protected:
	virtual void resizeEvent(QResizeEvent* event);
};

class MainWindow : public QMainWindow
{
	Q_OBJECT
private:
	std::auto_ptr<Ui::MainWindow> m_ui;
	QVector<QPushButton*> m_route_buttons;

public:
	MainWindow(MapViewer* map_viewer);
	void add_msg(const QString& msg);
	void clear_widgets();
	QPushButton* add_route_button(const QString& text);
	void set_central_widget(QWidget* widget);	

private slots:
	void slot_show_routes(bool checked);

private:
	std::auto_ptr<Ui::MainWindow> create_ui(MainWindow* main_window) const;
};

#endif