//=====================================================================================//
//   Author: open
//   Date:   31.07.2018
//=====================================================================================//
#include "base/precomp.h"
#include "shell.h"
#include <vrp_algorithm/vrp_json.h>
#include <QPushButton>

SortRoutePoints::SortRoutePoints(OsrmManager& osrm_manager, const Vrp::Data& plan_data, const Vrp::SPlan& plan)
: m_osrm_manager(osrm_manager)
, m_plan_data(plan_data)
, m_plan(plan)
, m_num_finished(0)
{
}
void SortRoutePoints::start()
{
	go_next();
}
void SortRoutePoints::go_next()
{
	if (m_num_finished < m_plan->routes().size())
	{
		Vrp::Route* route = m_plan->routes()[m_num_finished];
		m_data.available_cars.clear();
		Vrp::SCarDesc car_desc(new Vrp::CarDesc(*(route->car().car_desc)));
		car_desc->available_count = 10;
		car_desc->max_volume = 100000.f;
		car_desc->max_weight = 100000.f;
		car_desc->max_points = 100;
		m_data.available_cars.push_back(car_desc);

		m_data.cycle_count = 100;
		m_data.population_size = 512;

		m_data.destination_points.clear();
		m_data.destination_points.push_back(m_plan_data.destination_points[0]);
		for (const Vrp::RoutePoint& route_point : route->points())
			m_data.destination_points.push_back(m_plan_data.destination_points[route_point.index()]);

		m_points.clear();
		m_points.reserve(m_data.destination_points.size());
		for (const Vrp::DestinationPoint& destination_point : m_data.destination_points)
			m_points.push_back(destination_point.position);

		SOsrmRequestMatrixDistances request = m_osrm_manager.create_request_matrix_distances(m_points);
		connect(request.get(), &OsrmRequestMatrixDistances::finished, [this, request = request.get()](bool success, const SMatrixDistances& matrix, const vpositions_t& points) { slot_request_matrix_distances_finished(success, matrix); });
		m_osrm_manager.request_matrix_distances(request);
	}
	else
	{
		// finish ^u^
		m_plan->update_distance_and_fitness();
		emit finished(m_plan);
	}
}
void SortRoutePoints::slot_async_solver_finish(const Vrp::SPlan& plan)
{
	assert( plan->routes().size() == 1 );
	if (plan->routes().size() == 1)
	{
		Vrp::Route* route_dest = m_plan->routes()[m_num_finished];
		Vrp::Route* route_src = plan->routes()[0];

		Vrp::Route::points_t old_indices = route_dest->points();
		route_dest->clear_points();
		for (const Vrp::RoutePoint& route_point : route_src->points())
		{
			int index = old_indices[route_point.index()-1].index();
			route_dest->add_point(Vrp::RoutePoint(index, &(m_plan_data.destination_points[index])));
		}
		route_dest->update_distance(*m_plan_data.matrix_distances);
	}
	m_num_finished++;
	go_next();
}
void SortRoutePoints::slot_request_matrix_distances_finished(bool success, const SMatrixDistances& matrix)
{
	m_data.matrix_distances = matrix;
	m_vrp_async_solver.reset(new Vrp::VrpAsyncSolver(m_data));
	qRegisterMetaType<Vrp::SPlan>("Vrp::SPlan");
	connect(m_vrp_async_solver.get(), SIGNAL(finished(const Vrp::SPlan&)), SLOT(slot_async_solver_finish(const Vrp::SPlan&)), Qt::ConnectionType::QueuedConnection);
	m_vrp_async_solver->calculate();
}


Shell::Shell()
: m_main_window(new MainWindow(&m_map_viewer))
, m_osrm_manager(m_network_manager, "10.30.37.92:5000")
, m_timer(0.5f)
{
	connect(&m_timer, SIGNAL(updated(float)), SLOT(slot_timer_updated(float)));
	connect(&m_osrm_manager, SIGNAL(request_routes_finished(bool, const routes_t&)), SLOT(slot_request_routes_finished(bool, const routes_t&)));

	if (json_read_data("data.json", m_data))
	{
		//QString buffer;
		//for (const Vrp::DestinationPoint& destination_point : m_destination_points) 
		//{
		//	buffer += QString("%1,%2;").arg(destination_point.position.x()).arg(destination_point.position.y());
		//}
		m_points.reserve(m_data.destination_points.size());
		for (const Vrp::DestinationPoint& destination_point : m_data.destination_points)
			m_points.push_back(destination_point.position);
		
		SOsrmRequestMatrixDistances request = m_osrm_manager.create_request_matrix_distances(m_points);
		connect(request.get(), &OsrmRequestMatrixDistances::finished, [this, request = request.get()](bool success, const SMatrixDistances& matrix, const vpositions_t& points) { slot_request_matrix_distances_finished(success, matrix); });		
		m_osrm_manager.request_matrix_distances(request);
	}
}
void Shell::slot_request_matrix_distances_finished(bool success, const SMatrixDistances& matrix_distances)
{
	if (success) build_best_plan(matrix_distances);
	else logs("error request distances");
}
void Shell::build_best_plan(const SMatrixDistances& matrix_distances)
{
	m_data.matrix_distances = matrix_distances;

	Vrp::SPlan plan = Vrp::json_read_plan("result.txt", m_data);
	if (plan)
	{
		//ExportToAndrey export1(m_data);
		//ExportToAnnealing export1(m_data);
		//m_plan = plan;
		
		//m_sort_route_points.reset(new SortRoutePoints(m_osrm_manager, m_data, plan));
		//connect(m_sort_route_points.get(), SIGNAL(finished(const Vrp::SPlan&)), SLOT(slot_sort_route_points_finished(const Vrp::SPlan&)));
		//m_sort_route_points->start();

		slot_sort_route_points_finished(plan);
	}
	else
	{
		// # one thread
		//VrpSolver solver;
		//SPlan best_plan = solver.calculate(m_available_cars, m_destination_points, matrix_distances);
		//show_results(best_plan);

		// # many threads
		m_vrp_async_solver.reset(new Vrp::VrpAsyncSolver(m_data));
		connect(m_vrp_async_solver.get(), SIGNAL(logs_visual(const Vrp::SPlan&)), SLOT(slot_logs_visual(const Vrp::SPlan&)));
		connect(m_vrp_async_solver.get(), SIGNAL(finished(const Vrp::SPlan&)), SLOT(slot_async_solver_finish(const Vrp::SPlan&)));
		m_vrp_async_solver->calculate();
	}
}
void Shell::slot_sort_route_points_finished(const Vrp::SPlan& plan)
{
	m_plan = plan;
	routes_t routes = build_routes(m_plan);
	m_osrm_manager.request_routes(routes);
}
void Shell::slot_logs_visual(const Vrp::SPlan& logs_plan)
{
	routes_t routes = build_routes(logs_plan);
	show_results(logs_plan, routes, false);
}
void Shell::slot_async_solver_finish(const Vrp::SPlan& best_plan)
{
	m_plan = best_plan;
	routes_t routes = build_routes(m_plan);
	m_osrm_manager.request_routes(routes);
}
routes_t Shell::build_routes(const Vrp::SPlan& plan) const
{
	routes_t routes;
	for (size_t i = 0; i < plan->routes().size(); ++i)
	{
		SRoute route(new Route());
		routes.push_back(route);
		vpositions_t& points = route->points;
		points.push_back(m_points[0]);

		const Vrp::Route* vrp_route = plan->routes()[i];
		for (const Vrp::RoutePoint& vrp_point : vrp_route->points())
			points.push_back(m_points[vrp_point.index()]);
	}
	return routes;
}
void Shell::slot_request_routes_finished(bool success, const routes_t& routes)
{
	if (success) show_results(m_plan, routes, true);
	else logs("error request routes");
}
//static std::array<QString, 18> s_colors = {
//	"red",
//	"blue",
//	"green",
//	"magenta",
//	"black",
//	"darkMagenta",
//	"darkRed",
//	"darkCyan",
//	"darkBlue",
//	"yellow",
//	"darkGray",
//	"cyan",
//	//"lightGray",
//	"gray",
//	"darkGreen",
//	"white"
//};
struct RenderColor
{
	QString name;
	QColor color;
};
static std::array<RenderColor, 21> s_colors = {
	RenderColor{ "Black",	QColor(  0,   0,   0) },
	RenderColor{ "Red",		QColor(229,  20,   0) },
	RenderColor{ "Yellow",	QColor(216, 193,   0) },
	RenderColor{ "Blue",	QColor(  0,  80, 239) },
	RenderColor{ "Green",	QColor( 96, 169,  23) },
	RenderColor{ "Emerald",	QColor(  0, 138,   0) },
	RenderColor{ "Magenta",	QColor(216,   0, 115) },
	RenderColor{ "Amber",	QColor(240, 163,  10) },
	RenderColor{ "Brown",	QColor(175, 121,  59) },
	RenderColor{ "Crimson", QColor(162,   0,  37) },
	RenderColor{ "Cyan",	QColor( 27, 161, 226) },
	RenderColor{ "Lime",	QColor(164, 196,   0) },
	RenderColor{ "Indigo",	QColor( 63,   0, 158) },
	RenderColor{ "Mauve",	QColor(118,  96, 138) },
	RenderColor{ "Olive",	QColor(109, 135, 100) },
	RenderColor{ "Orange",	QColor(250, 104,   0) },
	RenderColor{ "Pink",	QColor(244, 114, 208) },
	RenderColor{ "Sienna",	QColor(122,  59,  63) },
	RenderColor{ "Steel",	QColor(119, 144, 168) },
	RenderColor{ "Teal",	QColor(  0, 181, 178) },
	RenderColor{ "Violet",	QColor(170,   0, 255) }
};
void Shell::show_results(const Vrp::SPlan& plan, const routes_t& data_routes, bool show_points)
{
	m_main_window->clear_widgets();
	m_main_window->add_msg(QString("dest points: %1").arg(m_data.destination_points.size() - 1));
	m_main_window->add_msg(QString("count points: %1").arg(plan->count_points()));
	m_main_window->add_msg(QString().sprintf("plan fitness: %.3f", plan->fitness()));
	m_main_window->add_msg(QString().sprintf("plan distance: %.3f", plan->distance()));
	m_main_window->add_msg("");

	for (size_t i = 0; i < plan->routes().size(); ++i)
	{
		const Vrp::Route* route = plan->routes()[i];
		m_main_window->add_msg(QString("route: %1 %2").arg(s_colors[i % s_colors.size()].name).arg(route->points().size()));
		m_main_window->add_msg(QString().sprintf("  volume: %.2f / %.2f", route->volume(), route->car().car_desc->max_volume));
		m_main_window->add_msg(QString().sprintf("  weight: %.2f / %.2f", route->weight(), route->car().car_desc->max_weight));
		m_main_window->add_msg(QString().sprintf("  distance: %.3f", route->distance()));
		m_main_window->add_msg(QString().sprintf("  fitness: %.3f", route->fitness()));
		m_main_window->add_msg("");
	}
	m_render_routes.clear();
	for (size_t i = 0; i < data_routes.size(); ++i)
	{	
		SRenderRoute render_route(new RenderRoute());
		m_render_routes.push_back(render_route);
		const SRoute& data_route = data_routes[i];
		const Vrp::Route* route = plan->routes()[i];
		// рисуем все сегменты путей
		for (const Waypoint& waypoint : data_route->waypoints)
		{
			SMapWaypoint map_waypoint = m_map_viewer.add_waypoint(waypoint.positions, s_colors[i % s_colors.size()].color, Qt::PenStyle::SolidLine, 3.f);// std::max(3.f, (9.5f - i*1.25f)));
			render_route->add_waypoint(map_waypoint);
		}
		// рисуем точки
		if (show_points == true)
		{ 
			for (size_t j = 1; j < data_route->points.size(); ++j)
			{
				const QPointF& point = data_route->points[j];
				if (j == 1)
				{
					SMapPoint map_little_point = m_map_viewer.add_point(point, s_colors[i % s_colors.size()].color, 5.5);
					render_route->add_point(map_little_point);

					SMapPoint map_black_point = m_map_viewer.add_point(point, QColor(0, 0, 0, 255), 10);
					render_route->add_point(map_black_point);
				}
				else
				{
					SMapPoint map_point = m_map_viewer.add_point(point, QColor(0, 0, 0, 255), 8);
					//SMapPoint map_point = m_map_viewer.add_point(point, s_colors[i % s_colors.size()].color, 8);
					render_route->add_point(map_point);
				}
			}
		}
		
		// рисуем направления пунктиром
		SMapWaypoint map_waypoint = m_map_viewer.add_waypoint(data_route->points, QColor(s_colors[i % s_colors.size()].color), Qt::PenStyle::DotLine, 1.6f);
		render_route->add_waypoint(map_waypoint);

		

		// добавляем кнопки для скрытия маршрута
		QPushButton* button = m_main_window->add_route_button(QString("%1 %2").arg(i).arg(s_colors[i % s_colors.size()].name));
		button->setCheckable(true);
		button->setChecked(true);
		connect(button, &QPushButton::toggled, [this, button, render_route = render_route.get()]() { slot_route_button_clicked(button, render_route); });
		if (route->weight() > route->car().car_desc->max_weight || route->volume() > route->car().car_desc->max_volume)
		{
			//button->setFlat(true);
			button->setStyleSheet("background-color: #ff9a96");
		}
	}
}
void Shell::slot_route_button_clicked(QPushButton* button, RenderRoute* render_route)
{
	render_route->set_visible(button->isChecked());
	//m_map_viewer.map_control()->update();
}
/*vpositions_t Shell::convert_route_points(const Vrp::Route::points_t& route_points) const
{
	vpositions_t positions;
	positions.push_back(m_destination_points[0].position);
	for (const Vrp::RoutePoint& route_point : route_points)
		positions.push_back(route_point.position());
	return positions;
}*/
void Shell::activate()
{
	m_main_window->show();
}
void Shell::slot_timer_updated(float dt)
{
	m_map_viewer.update(dt);
}


void RenderRoute::add_point(const SMapPoint& point)
{
	m_points.push_back(point);
}
void RenderRoute::add_waypoint(const SMapWaypoint& waypoint)
{
	m_waypoints.push_back(waypoint);
}
void RenderRoute::set_visible(bool visible)
{
	for (const SMapPoint& point : m_points)
		point->set_visible(visible);

	for (const SMapWaypoint& waypoint : m_waypoints)
		waypoint->set_visible(visible);
}

#include <QJsonObject>
#include <QJsonArray>
QJsonArray ExportToAndrey::export_points() const
{
	QJsonArray json_array;
	for (int i = 0; i < m_data.destination_points.size(); ++i)
	{
		const auto& point = m_data.destination_points[i];
		QJsonObject json_obj;
		json_obj["lat"] = point.position.x();
		json_obj["lon"] = point.position.y();
		json_obj["weight"] = point.weight;
		json_obj["volume"] = point.volume;
		json_obj["timefrom"] = i == 0 ? 0 : 8;
		json_obj["timeto"] = i == 0 ? 0 : 21;
		json_obj["delayed"] = 30;
		json_obj["city"] = m_cities[point.city_id].toString();
		json_obj["dischargetime"] = 12;
		json_array.append(json_obj);
	}
	return json_array;
}
QJsonArray ExportToAndrey::export_cities() const
{
	QJsonArray json_array;
	for (auto city : m_cities)
	{
		QJsonObject json_obj;
		json_obj["city"] = "noname";
		json_obj["guid"] = city.toString();
		json_obj["priority"] = 0;
		json_array.append(json_obj);
	}
	return json_array;
}
QJsonArray ExportToAndrey::export_cars() const
{
	QJsonArray json_array;
	for (auto car : m_data.available_cars)
	{
		QJsonObject json_obj;
		json_obj["weight"] = car->max_weight;
		json_obj["volume"] = car->max_volume;
		json_obj["costkm"] = car->cost_km;
		json_obj["costdestinationpoint"] = car->cost_destination_point;
		json_obj["costtrip"] = car->cost_trip;
		json_obj["count"] = car->available_count;
		json_obj["percentagexcess"] = 5;
		json_obj["speedcity"] = 40;
		json_obj["speedcountryside"] = 80;
		json_obj["timefrom"] = 0;
		json_obj["timeto"] = 22;
		json_obj["countpoint"] = 0;
		json_obj["splitorder"] = "false";
		json_obj["guid"] = QUuid::createUuid().toString();
		json_array.append(json_obj);
	}
	return json_array;
}
ExportToAndrey::ExportToAndrey(const Vrp::Data& data)
: m_data(data)
{
	for (const Vrp::DestinationPoint& dest_point : m_data.destination_points)
	{
		if (m_cities.end() == m_cities.find(dest_point.city_id))
			m_cities[dest_point.city_id] = QUuid::createUuid();
	}
	QJsonDocument doc;
	QJsonObject json_obj;
	json_obj["Точки"] = export_points();
	json_obj["Города"] = export_cities();
	json_obj["Машины"] = export_cars();
	doc.setObject(json_obj);
	auto buffer = doc.toJson(QJsonDocument::JsonFormat::Indented);
	QFile file("to_andrey.txt");
	if (file.open(QIODevice::WriteOnly))
		file.write(buffer);
	int a = 0;
}


QJsonArray ExportToAnnealing::export_points() const
{
	QJsonArray json_array;
	for (int i = 0; i < m_data.destination_points.size(); ++i)
	{
		const auto& point = m_data.destination_points[i];
		QJsonObject json_obj;
		//json_obj["id"] = QUuid::createUuid().toString();
		json_obj["lat"] = point.position.y();
		json_obj["lon"] = point.position.x();
		json_obj["weight"] = point.weight;
		json_obj["volume"] = point.volume;
		json_obj["time_from"] = i == 0 ? 0 : 8 * 3600;
		json_obj["time_to"] = i == 0 ? 0 : 21 * 3600;
		json_obj["time_delayed"] = i == 0 ? 0 : 30 * 60;
		json_obj["city"] = m_cities[point.city_id].toString();
		json_obj["time_discharge"] = i == 0 ? 0 : 12 * 60;	// 12 мин
		json_array.append(json_obj);
	}
	return json_array;
}
QJsonArray ExportToAnnealing::export_cities() const
{
	QJsonArray json_array;
	for (auto city : m_cities)
	{
		QJsonObject json_obj;
		json_obj["id"] = city.toString();
		json_obj["name"] = "noname - " + city.toString();
		json_obj["priority"] = 0;
		json_obj["closed"] = false;
		json_array.append(json_obj);
	}
	return json_array;
}
QJsonArray ExportToAnnealing::export_cars() const
{
	QJsonArray json_array;
	for (auto car : m_data.available_cars)
	{
		QJsonObject json_obj;
		json_obj["id"] = QUuid::createUuid().toString();
		json_obj["weight"] = car->max_weight;
		json_obj["volume"] = car->max_volume;
		json_obj["percent_overweight"] = 0.05;
		json_obj["cost_km"] = car->cost_km;
		json_obj["cost_point"] = car->cost_destination_point;
		json_obj["cost_trip"] = car->cost_trip;
		//json_obj["cost_over_weight"] = 0;
		//json_obj["cost_over_volume"] = 0;
		//json_obj["cost_spend_night"] = 0;
		//json_obj["cost_delay"] = 0;
		json_obj["available_count"] = car->available_count;
		json_obj["speed_city"] = 40;
		json_obj["speed_highway"] = 80;
		json_obj["time_from"] = 0;
		json_obj["time_to"] = 22 * 3600;
		json_obj["start_time_after_relax"] = 8 * 3600;
		json_obj["num_days_without_penalty"] = 0;
		json_array.append(json_obj);
	}
	return json_array;
}
ExportToAnnealing::ExportToAnnealing(const Vrp::Data& data)
: m_data(data)
{
	for (const Vrp::DestinationPoint& dest_point : m_data.destination_points)
	{
		if (m_cities.end() == m_cities.find(dest_point.city_id))
			m_cities[dest_point.city_id] = QUuid::createUuid();
	}
	QJsonDocument doc;
	QJsonObject json_obj;
	json_obj["points"] = export_points();
	json_obj["cities"] = export_cities();
	json_obj["cars"] = export_cars();
	doc.setObject(json_obj);
	auto buffer = doc.toJson(QJsonDocument::JsonFormat::Indented);
	int a = 0;
}