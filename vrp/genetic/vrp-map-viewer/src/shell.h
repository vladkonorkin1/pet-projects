//=====================================================================================//
//   Author: open
//   Date:   31.07.2018
//=====================================================================================//
#pragma once
#include "vrp_algorithm/vrp_async_solver.h"
#include "vrp_algorithm/vrp_solver.h"
#include "base/thread_task_manager.h"
#include "map_viewer/map_viewer.h"
#include "base/network_manager.h"
#include "osrm/osrm_manager.h"
#include "main_window.h"
#include "base/timer.h"

class SortRoutePoints : public QObject
{
	Q_OBJECT
private:
	OsrmManager& m_osrm_manager;
	Vrp::Data m_plan_data;
	Vrp::SPlan m_plan;
	int m_num_finished;
	vpositions_t m_points;
	Vrp::Data m_data;
	using UVrpAsyncSolver = std::unique_ptr<Vrp::VrpAsyncSolver>;
	UVrpAsyncSolver m_vrp_async_solver;

public:
	SortRoutePoints(OsrmManager& osrm_manager, const Vrp::Data& plan_data, const Vrp::SPlan& plan);
	void start();

signals:
	void finished(const Vrp::SPlan& plan);

private slots:
	void slot_async_solver_finish(const Vrp::SPlan& best_plan);
	void slot_request_matrix_distances_finished(bool success, const SMatrixDistances& matrix);

private:
	void go_next();
};


class RenderRoute
{
private:
	QVector<SMapWaypoint> m_waypoints;
	QVector<SMapPoint> m_points;

public:
	void add_waypoint(const SMapWaypoint& waypoint);
	void add_point(const SMapPoint& point);
	void set_visible(bool visible);
};
using SRenderRoute = std::shared_ptr<RenderRoute>;


class Shell : public QObject
{
	Q_OBJECT
private:
	NetworkManager m_network_manager;
	OsrmManager m_osrm_manager;
	MapViewer m_map_viewer;
	std::unique_ptr<MainWindow> m_main_window;

	using render_routes_t = std::vector<SRenderRoute>;
	render_routes_t m_render_routes;

	Base::Timer m_timer;

	Vrp::Data m_data;

	vpositions_t m_points;

	using UVrpAsyncSolver = std::unique_ptr<Vrp::VrpAsyncSolver>;
	UVrpAsyncSolver m_vrp_async_solver;
	Vrp::SPlan m_plan;

	std::unique_ptr<SortRoutePoints> m_sort_route_points;

public:
	Shell();
	void activate();

private slots:
	void slot_timer_updated(float dt);
	void slot_logs_visual(const Vrp::SPlan& logs_plan);
	void slot_request_matrix_distances_finished(bool success, const SMatrixDistances& matrix_distances);
	void slot_async_solver_finish(const Vrp::SPlan& best_plan);
	void slot_request_routes_finished(bool success, const routes_t& routes);
	void slot_sort_route_points_finished(const Vrp::SPlan& plan);

private:
	routes_t build_routes(const Vrp::SPlan& plan) const;
	void slot_route_button_clicked(QPushButton* button, RenderRoute* render_route);
	//vpositions_t convert_route_points(const Vrp::Route::points_t& route_points) const;
	void build_best_plan(const SMatrixDistances& matrix_distances);
	void show_results(const Vrp::SPlan& plan, const routes_t& render_routes, bool show_points);
};

class ExportToAndrey
{
private:
	Vrp::Data m_data;
	QMap<int, QUuid> m_cities;

public:
	ExportToAndrey(const Vrp::Data& data);

private:
	QJsonArray export_cities() const;
	QJsonArray export_points() const;
	QJsonArray export_cars() const;
};

class ExportToAnnealing
{
private:
	Vrp::Data m_data;
	QMap<int, QUuid> m_cities;

public:
	ExportToAnnealing(const Vrp::Data& data);

private:
	QJsonArray export_cities() const;
	QJsonArray export_points() const;
	QJsonArray export_cars() const;
};