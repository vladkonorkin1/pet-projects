//=====================================================================================//
//   Author: open
//   Date:   31.07.2018
//=====================================================================================//
#include <QCoreApplication>
#include "shell.h"

#ifdef Q_OS_WIN
#include <Windows.h>
#endif

int main(int argc, char *argv[])
{
#ifdef Q_OS_WIN
	FreeConsole();
	AllocConsole();
	freopen("CONOUT$", "w", stdout);
#endif

	Log::init();
	QCoreApplication app(argc, argv);
	if (argc <= 2)
	{
		logs("argc too small");
		return -1;
	}
	try
	{
		Shell shell(argv[1], argv[2]);
		return app.exec();
	}
	catch (const std::exception& e) {
		logs(e.what());
	}
	return -1;
}
