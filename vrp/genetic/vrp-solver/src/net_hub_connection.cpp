//=====================================================================================//
//   Author: open
//   Date:   18.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "net_hub_connection.h"

namespace NetHub
{

Connection::Connection()
: m_connection(&m_socket)
{
	connect(&m_connection, SIGNAL(connected()), SLOT(slot_connected()));
	connect(&m_connection, SIGNAL(disconnected()), SLOT(slot_disconnect()));
	connect(&m_connection, SIGNAL(data_received(const char*, unsigned int)), SLOT(slot_data_recieved(const char*, unsigned int)));

	m_message_dispatcher.register_message<NetHub::ServerMessageOpenTicket>();
	m_message_dispatcher.register_message<NetHub::ServerMessageExitProcess>();
	m_message_dispatcher.set_observer(this);
}
void Connection::connect_host(const QString& host_name)
{
	m_socket.connectToServer(host_name);
}
void Connection::slot_connected()
{
	emit connected();
}
void Connection::slot_disconnect()
{
	emit disconnected();
}
void Connection::send_message(const NetMessage& message)
{
	m_connection.send_message(message);
}
void Connection::slot_data_recieved(const char* data, unsigned int size)
{
	m_message_dispatcher.process(data, size);
}
void Connection::close()
{
	m_connection.close();
}
void Connection::on_message(const NetHub::ServerMessageOpenTicket& msg)
{
	emit task_opened(msg.json_doc, msg.matrix_distances);
}
void Connection::on_message(const NetHub::ServerMessageExitProcess& msg)
{
	emit exit_process();
}

}