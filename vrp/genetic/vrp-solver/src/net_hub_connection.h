//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#pragma once
#include "net_hub/net_hub_handler_messages.h"
#include "net/net_message_dispatcher.h"
#include "net/net_base_connection.h"
#include <QLocalSocket>

namespace NetHub
{

class Connection : public QObject, public ClientHandlerMessages
{
	Q_OBJECT
private:
	QLocalSocket m_socket;
	NetBaseConnection m_connection;
	NetMessageDispatcher<ClientHandlerMessages> m_message_dispatcher;
	
public:
	Connection();
	void connect_host(const QString& host_name);
	void send_message(const NetMessage& message);
	void close();

signals:
	void connected();
	void disconnected();
	void task_opened(const QJsonDocument& json_doc, const SMatrixDistances& matrix_distances);
	void exit_process();

public:
	virtual void on_message(const NetHub::ServerMessageOpenTicket& msg);
	virtual void on_message(const NetHub::ServerMessageExitProcess& msg);

private slots:
	void slot_connected();
	void slot_disconnect();
	void slot_data_recieved(const char* data, unsigned int size);
};

using UConnection = std::unique_ptr<Connection>;

}