﻿//=====================================================================================//
//   Author: open
//   Date:   31.07.2018
//=====================================================================================//
#include "base/precomp.h"
#include "shell.h"
#include <vrp_algorithm/vrp_json.h>
#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QDir>

Shell::Shell(const QString& hub_name, const QString& task_id)
: m_hub_name(hub_name)
, m_task_id(task_id)
//, m_num_solver_finished(0)
{
	connect(&m_hub_connection, SIGNAL(connected()), this, SLOT(slot_connection_connected()));
	connect(&m_hub_connection, SIGNAL(disconnected()), this, SLOT(slot_connection_disconnected()), Qt::ConnectionType::QueuedConnection);
	connect(&m_hub_connection, SIGNAL(task_opened(const QJsonDocument&, const SMatrixDistances&)), this, SLOT(slot_task_opened(const QJsonDocument&, const SMatrixDistances&)));
	connect(&m_hub_connection, SIGNAL(exit_process()), this, SLOT(slot_exit_process()));
	m_hub_connection.connect_host(m_hub_name);
}
void Shell::slot_connection_connected()
{
	m_hub_connection.send_message(NetHub::ClientMessageAuthorize(m_task_id));
}
void Shell::slot_connection_disconnected()
{	
	QCoreApplication::exit(-2);
}
void Shell::slot_exit_process() 
{
	QCoreApplication::exit(0);
}
void Shell::slot_task_opened(const QJsonDocument& json_doc, const SMatrixDistances& matrix_distances)
{
	QDir().mkdir("requests");
	save_json(json_doc, QString("requests/%1 %2.json").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss")).arg(m_task_id));
	
	Vrp::json_parse_data(json_doc, m_data);

	//if (m_data.destination_points.size() > 0 && m_data.available_cars.size() > 0)
	if (m_data.is_init())
	{
		m_data.matrix_distances = matrix_distances;
		build_best_plan();
	}
	else
	{
		send_result(Vrp::SPlan());
	}
}
void Shell::build_best_plan()
{
	// # one thread
	//VrpSolver solver;
	//SPlan best_plan = solver.calculate(m_available_cars, m_destination_points, matrix_distances);
	//show_results(best_plan);

	// # many threads
	m_vrp_async_solver.reset(new Vrp::VrpAsyncSolver(m_data));
	connect(m_vrp_async_solver.get(), SIGNAL(finished(const Vrp::SPlan&)), SLOT(slot_async_solver_finish(const Vrp::SPlan&)));
	m_vrp_async_solver->calculate();

	/*for (int i = 0; i < m_data.num_challenges; ++i)
	{
		SVrpAsyncSolver vrp_async_solver(new Vrp::VrpAsyncSolver(i, m_data));
		connect(vrp_async_solver.get(), &Vrp::VrpAsyncSolver::finished, [this, challenge_index=i](const Vrp::SPlan& plan) { slot_async_solver_finish(challenge_index, plan); });
		vrp_async_solver->calculate();
		m_vrp_async_solvers.push_back(vrp_async_solver);
	}*/
}
//void Shell::slot_async_solver_finish(int challenge_index, const Vrp::SPlan& plan)
void Shell::slot_async_solver_finish(const Vrp::SPlan& plan)
{
	send_result(plan);

	//logs(QString().sprintf("challenge %d = %.0f", challenge_index, plan->fitness()));

	/*m_plans.push_back(plan);
	m_num_solver_finished++;
	if (m_num_solver_finished == m_vrp_async_solvers.size())
	{
		Vrp::SPlan best_plan;
		for (const Vrp::SPlan& plan : m_plans)
		{
			if (!best_plan) best_plan = plan;
			else if (plan->fitness() < best_plan->fitness())
				best_plan = plan;
		}
		send_result(best_plan);
	}*/
}
void Shell::send_result(const Vrp::SPlan& plan)
{
	QJsonDocument json_result = Vrp::plan_to_json(plan, m_vrp_async_solver->num_cycles());
	QDir().mkdir("results");
	save_json(json_result, QString("results/%1 %2.json").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss")).arg(m_task_id));
	m_hub_connection.send_message(NetHub::ClientMessageTicketSolved(json_result));
}
void Shell::save_json(const QJsonDocument& json_doc, const QString& filename) const
{
	QFile file(filename);
	if (file.open(QIODevice::WriteOnly))
		file.write(json_doc.toJson());
}