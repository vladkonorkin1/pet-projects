//=====================================================================================//
//   Author: open
//   Date:   31.07.2018
//=====================================================================================//
#pragma once
#include "vrp_algorithm/vrp_async_solver.h"
#include "base/thread_task_manager.h"
#include "base/network_manager.h"
#include "net_hub_connection.h"

class Shell : public QObject
{
	Q_OBJECT
private:
	QString m_hub_name;
	QString m_task_id;
	NetHub::Connection m_hub_connection;
	NetworkManager m_network_manager;

	Vrp::Data m_data;

	using vpositions_t = std::vector<QPointF>;
	vpositions_t m_points;

	using SVrpAsyncSolver = std::shared_ptr<Vrp::VrpAsyncSolver>;
	SVrpAsyncSolver m_vrp_async_solver;
	//std::vector<SVrpAsyncSolver> m_vrp_async_solvers;
	//int m_num_solver_finished;
	
	std::vector<Vrp::SPlan> m_plans;

public:
	Shell(const QString& hub_name, const QString& task_id);

private slots:
	void slot_connection_connected();
	void slot_connection_disconnected();
	void slot_task_opened(const QJsonDocument& json_doc, const SMatrixDistances& matrix_distances);
	//void slot_async_solver_finish(int challenge_index, const Vrp::SPlan& plan);
	void slot_async_solver_finish(const Vrp::SPlan& plan);
	void slot_exit_process();

private:
	void build_best_plan();
	void send_result(const Vrp::SPlan& plan);
	void save_json(const QJsonDocument& json_doc, const QString& filename) const;
};
