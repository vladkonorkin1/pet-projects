TARGET = vrp-solver
CONFIG -= flat
TEMPLATE = app

win32 {
    release: DESTDIR = tmp/release
    debug:   DESTDIR = tmp/debug
    OBJECTS_DIR = $$DESTDIR/.obj
    MOC_DIR = $$DESTDIR/.moc
    RCC_DIR = $$DESTDIR/.qrc
    UI_DIR = $$DESTDIR/.ui
}
unix {
    DESTDIR = .
    OBJECTS_DIR = $$DESTDIR/tmp/.obj
    MOC_DIR = $$DESTDIR/tmp/.moc
    RCC_DIR = $$DESTDIR/tmp/.qrc
    UI_DIR = $$DESTDIR/tmp/.ui
}

QT -= gui
QT += network
CONFIG += console

CONFIG += precompile_header
PRECOMPILED_HEADER = ../shared/base/precomp.h

INCLUDEPATH += src
INCLUDEPATH += ../shared

win32 {
	Release:QMAKE_POST_LINK = copy tmp\release\*.exe bin\release
	QMAKE_CXXFLAGS += /MP
	INCLUDEPATH += ../../../../../external/boost_1_68_0
}
unix {
	INCLUDEPATH += /usr/local/include/boost
	LIBS += -lboost_random
}


HEADERS =  	../shared/base/precomp.h 								\
			../shared/base/timer.h 									\
			../shared/base/log.h 									\
			../shared/base/network_object.h							\
			../shared/base/network_manager.h						\
			../shared/base/dynamic_matrix.h							\
			../shared/base/matrix_distances.h						\
			../shared/base/random_indices.h							\
			../shared/base/thread_task.h							\
			../shared/base/thread_task_manager.h					\
			../shared/base/types.h									\
			../shared/vrp_algorithm/vrp_data.h						\
			../shared/vrp_algorithm/vrp_create_population_task.h	\
			../shared/vrp_algorithm/vrp_crossover_task.h			\
			../shared/vrp_algorithm/vrp_mutation_task.h				\
			../shared/vrp_algorithm/vrp_insert_point.h				\
			../shared/vrp_algorithm/vrp_async_solver.h				\
			../shared/vrp_algorithm/vrp_json.h						\
			../shared/net/net_base_connection.h						\
			../shared/net/net_packet.h								\
			../shared/net/net_packet_reader.h						\
			../shared/net/net_message_dispatcher.h					\
			../shared/net/net_message.h								\
			../shared/net_hub/net_hub_handler_messages.h			\
			../shared/net_hub/net_hub_messages.h					\
			../shared/net_hub/net_hub_types.h						\
			src/net_hub_connection.h								\
			src/shell.h												\

SOURCES = 	../shared/base/precomp.cpp 								\
			../shared/base/timer.cpp 								\
			../shared/base/log.cpp 									\
			../shared/base/network_object.cpp						\
			../shared/base/network_manager.cpp						\
			../shared/base/matrix_distances.cpp						\
			../shared/base/random_indices.cpp						\
			../shared/base/thread_task_manager.cpp					\
			../shared/vrp_algorithm/vrp_data.cpp					\
			../shared/vrp_algorithm/vrp_create_population_task.cpp	\
			../shared/vrp_algorithm/vrp_crossover_task.cpp			\
			../shared/vrp_algorithm/vrp_mutation_task.cpp			\
			../shared/vrp_algorithm/vrp_insert_point.cpp			\
			../shared/vrp_algorithm/vrp_async_solver.cpp			\
			../shared/vrp_algorithm/vrp_json.cpp					\
			../shared/net/net_base_connection.cpp					\
			../shared/net/net_packet.cpp							\
			../shared/net/net_packet_reader.cpp						\
			../shared/net_hub/net_hub_messages.cpp					\
			src/net_hub_connection.cpp								\
			src/shell.cpp											\
			src/main.cpp											\
