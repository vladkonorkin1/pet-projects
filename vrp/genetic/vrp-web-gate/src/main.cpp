//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#include <QCoreApplication>
#include "shell.h"

int main(int argc, char *argv[])
{
	Log::init();
	QCoreApplication app(argc, argv);

	try
	{
		Shell shell;
		return app.exec();
	}
	catch (const std::exception& e) {
		logs(e.what());
	}
	return -1;
}