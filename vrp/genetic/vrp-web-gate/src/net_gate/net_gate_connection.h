﻿//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#pragma once
#include "net_gate/net_gate_handler_messages.h"
#include "net/net_base_connection.h"
#include "net/net_message_dispatcher.h"

class QTcpSocket;

namespace NetGate
{

class Server;

class Connection : public QObject, public NetGate::ServerHandlerMessages
{
	Q_OBJECT
private:
	NetBaseConnection m_connection;
	QTcpSocket* m_socket;
	ConnectionId m_id;
	QUuid m_session_id;
	NetMessageDispatcher<NetGate::ServerHandlerMessages> m_message_dispatcher;
	float m_time_alive;
	bool m_close_session;

public:
	Connection(QTcpSocket* socket, ConnectionId id);
	ConnectionId id() const;
	const QUuid& session_id() const;
	void send_message(const NetMessage& message);
	void close(bool close_session);
	QString address() const;
	bool check_destroy(float dt);

public:
	virtual void on_message(const NetGate::ClientMessageAutorizate& msg);
	virtual void on_message(const NetGate::ClientMessageAlive& msg);
	virtual void on_message(const NetGate::ClientMessageTaskSolved& msg);
	virtual void on_message(const NetGate::ClientMessageCloseTaskResponse& msg);

signals:
	// соединение потеряно
	void disconnected(Connection* connection, bool close_session);
	// сообщение об авторизации
	void authorized(Connection* connection, const QUuid& session_id, const QString& session_name, bool& new_session);
	// задача решёна
	void task_solved(const QUuid& task_id, bool result, const QJsonDocument& json_result);
	// подтверждение о закрытии задачи
	void task_closed_response(const QUuid& task_id);

private slots:
	void slot_disconnect();
	void slot_data_recieved(const char* data, unsigned int size);
};

using UConnection = std::unique_ptr<Connection>;

}