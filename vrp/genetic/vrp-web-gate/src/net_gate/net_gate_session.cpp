//=====================================================================================//
//   Author: open
//   Date:   13.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "net_gate_session.h"
#include "net/net_message.h"
#include "net_gate/net_gate_connection.h"

namespace NetGate
{

float Session::s_destroy_timeout = 500.f;

Session::Session(const QUuid& id, const QString& name)
: m_id(id)
, m_name(name)
, m_connection(nullptr)
, m_time_alive(0.f)
{
}
const QUuid& Session::id() const
{
	return m_id;
}
void Session::set_connection(Connection* connection)
{
	m_connection = connection;
}
bool Session::check_destroy(float dt)
{
	if (!m_connection)
	{
		m_time_alive += dt;
		if (m_time_alive > s_destroy_timeout)
			return true;
	}
	else m_time_alive = 0.f;
	return false;
}
void Session::send_message(const NetMessage& msg)
{
	if (m_connection)
		m_connection->send_message(msg);
}
Connection* Session::connection()
{
	return m_connection;
}
const QString& Session::name() const
{
	return m_name;
}

}
