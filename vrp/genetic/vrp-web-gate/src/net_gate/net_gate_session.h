//=====================================================================================//
//   Author: open
//   Date:   13.09.2018
//=====================================================================================//
#pragma once

struct NetMessage;

namespace NetGate
{

class Connection;

class Session
{
private:
	QUuid m_id;
	QString m_name; 
	Connection* m_connection;
	float m_time_alive;
	static float s_destroy_timeout;

public:
	Session(const QUuid& id, const QString& name);
	const QUuid& id() const;
	const QString& name() const;
	void set_connection(Connection* connection);
	Connection* connection();
	bool check_destroy(float dt);
	void send_message(const NetMessage& msg);
};

using USession = std::unique_ptr<Session>;

}
