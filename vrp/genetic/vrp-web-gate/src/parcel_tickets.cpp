//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "parcel_tickets.h"
#include "net_gate/net_gate_server.h"

ParcelTickets::ParcelTickets(NetGate::Server* server, const QString& osrm_server_address, int num_challenges)
: m_osrm_manager(m_network_manager, osrm_server_address)
, m_num_challenges(num_challenges)
{
	connect(server, SIGNAL(session_opened(NetGate::Session*)), SLOT(slot_session_opened(NetGate::Session*)));
	connect(server, SIGNAL(session_closed(NetGate::Session*)), SLOT(slot_session_closed(NetGate::Session*)));
	connect(server, SIGNAL(session_connection_created(NetGate::Session*, NetGate::Connection*)), SLOT(slot_session_connection_created(NetGate::Session*, NetGate::Connection*)));
}
// добавить тикет
void ParcelTickets::add_ticket(const QUuid& ticket_id, const QString& hash, const QJsonDocument& json_doc)
{
	if (m_tickets.find(ticket_id) == m_tickets.end())
	{
		logs(QString("open ticket: %1").arg(ticket_id.toString()));

		STicket ticket(new Ticket(ticket_id, hash, json_doc));
		m_tickets[ticket_id] = ticket;

		vpositions_t points;
		for (const Vrp::DestinationPoint& destination_point : ticket->vrp_data.destination_points)
			points.push_back(destination_point.position);
		
		SOsrmRequestMatrixDistances request = m_osrm_manager.create_request_matrix_distances(points);
		connect(request.get(), &OsrmRequestMatrixDistances::finished, [this, ticket_id=ticket->id](bool finished, const SMatrixDistances& matrix) { on_request_matrix_distances_finished(finished, ticket_id, matrix); });
		m_osrm_manager.request_matrix_distances(request);
	}
}
// удалить тикет
void ParcelTickets::remove_ticket(const QUuid& ticket_id)
{
	tickets_t::iterator it = m_tickets.find(ticket_id);
	if (m_tickets.end() != it)
	{
		const Ticket& ticket = *(it->second);
		// копируем таски перед удалением
		Ticket::tasks_t tasks = ticket.tasks();
		for (const STask& task : tasks)
		{
			session_solvers_t::iterator it_session_solver = m_session_solvers.find(task->session_id);
			if (m_session_solvers.end() != it_session_solver)
			{
				it_session_solver->second->remove_task(*task);
			}
		}
		//m_tickets.erase(it);	// do not need to erase, because the slot_ticket_closed will be called
		assert( m_tickets.end() == m_tickets.find(ticket_id) );
	}
}
void ParcelTickets::slot_session_opened(NetGate::Session* session)
{
	assert( m_session_solvers.find(session->id()) == m_session_solvers.end() );
	USessionSolver session_solver(new SessionSolver(session));
	//connect(session_solver.get(), SIGNAL(ticket_closed(Ticket*, bool, const QJsonDocument*)), SLOT(slot_session_ticket_closed(Ticket*, bool, const QJsonDocument*)));
	connect(session_solver.get(), SIGNAL(task_closed(Task&, bool, const QJsonDocument*)), SLOT(slot_session_task_closed(Task&, bool, const QJsonDocument*)));
	m_session_solvers[session->id()] = std::move(session_solver);
}
void ParcelTickets::slot_session_closed(NetGate::Session* session)
{
	session_solvers_t::iterator it = m_session_solvers.find(session->id());
	if (m_session_solvers.end() != it)
	{
		// !!!!! оповестить, что задачи не решены
		it->second->remove_all_tasks();
		m_session_solvers.erase(it);
	}
}
void ParcelTickets::slot_session_connection_created(NetGate::Session* session, NetGate::Connection* connection)
{
	// произошло переподключение к сессии
	session_solvers_t::iterator it = m_session_solvers.find(session->id());
	if (m_session_solvers.end() != it)
	{
		SessionSolver* session_solver = it->second.get();
		session_solver->update_connection(connection);

		// заново отправляем все поставленные для данной сессии задачи
		session_solver->send_all_tasks();

		// запрос отчёта по выполненым задачам
		session_solver->send_request_report_solved_tasks();
	}
}
// закрыть тикет
void ParcelTickets::close_ticket(const Ticket& ticket, bool result, const QJsonDocument* json_result)
{
	logs(QString("close ticket: %1  [%2]").arg(ticket.id.toString()).arg(ticket.duration()));
	tickets_t::iterator it = m_tickets.find(ticket.id);
	if (m_tickets.end() != it)
	{
		emit ticket_solved(ticket.hash, result, json_result);
		m_tickets.erase(it);
	}
}
// слот окончания запроса матрицы дистанций
void ParcelTickets::on_request_matrix_distances_finished(bool success, const QUuid& ticket_id, const SMatrixDistances& matrix)
{
	// если заявка ещё существует
	tickets_t::iterator it = m_tickets.find(ticket_id);
	if (m_tickets.end() != it)
	{
		STicket ticket = it->second;
		ticket->vrp_data.matrix_distances = matrix;

		if (success)
		{
			if (!start_solving_ticket(ticket))
			{
				logs(QString("error to start solving ticket: %1").arg(ticket->id.toString()));
				close_ticket(*ticket, false, nullptr);
			}
		}
		else
		{
			logs(QString("error to request matrix distances for ticket: %1").arg(ticket->id.toString()));
			close_ticket(*ticket, false, nullptr);
		}
	}
}
// поставить тикет на решение
bool ParcelTickets::start_solving_ticket(const STicket& ticket)
{
	std::vector<SessionSolver*> session_solvers;
	for (auto& pair : m_session_solvers)
		session_solvers.push_back(pair.second.get());

	if (!session_solvers.empty())
	{
		for (int i = 0; i < m_num_challenges; ++i)
		{
			SessionSolver* session_solver = session_solvers[i % session_solvers.size()];
			session_solver->add_task(*ticket, i);
		}

		QString solvers;
		for (const STask& task : ticket->tasks())
			solvers += task->session_name + ' ';

		logs(QString("setup solvers: %1  %2").arg(ticket->id.toString()).arg(solvers));
		return true;
	}
	return false;
}
// слот закрытия задачи после обработки сессией
void ParcelTickets::slot_session_task_closed(Task& task, bool result, const QJsonDocument* json_result)
{
	Ticket& ticket = task.ticket;
	ticket.num_finished++;
	
	if (result && json_result)
		ticket.add_result(task.id, *json_result);

	logs(ticket.log_task(task));

	if (ticket.num_finished == m_num_challenges)
	{
		
		const Task* best_task = ticket.find_best_result();
		if (best_task)
		{
			logs("best result -> " + ticket.log_task(*best_task));
			close_ticket(ticket, true, &(best_task->json_result));
		}
		else
		{
			logs(QString("error to choose best plan for ticket: %1").arg(ticket.id.toString()));
			close_ticket(ticket, false, nullptr);
		}
	}
}
// найти лучшую сессия для вставки (по минимальному кол-ву задач)
/*bool ParcelTickets::insert_to_best_session(Ticket* ticket)
{
	if (SessionTickets* session_tickets = find_best_session())
	{
		session_tickets->add_ticket(ticket);
		return true;
	}
	return false;
}
SessionTickets* ParcelTickets::find_best_session()
{
	size_t min_ticket_count = std::numeric_limits<size_t>::max();
	SessionTickets* best_session_tickets = nullptr;
	for (auto& pair : m_session_tickets)
	{
		SessionTickets* session_tickets = pair.second.get();
		if (min_ticket_count > session_tickets->tickets_size())
		{
			min_ticket_count = session_tickets->tickets_size();
			best_session_tickets = session_tickets;
		}
	}
	return best_session_tickets;
}*/