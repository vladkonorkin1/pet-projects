//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#pragma once
#include "base/network_manager.h"
#include "osrm/osrm_manager.h"
#include "session_solver.h"
#include "ticket.h"

namespace NetGate
{
	class Session;
	class Server;
}

class ParcelTickets : public QObject
{
	Q_OBJECT
private:
	NetworkManager m_network_manager;
	OsrmManager m_osrm_manager;

	using session_solvers_t = std::map<QUuid, USessionSolver>;
	session_solvers_t m_session_solvers;

	using tickets_t = std::map<QUuid, STicket>;
	tickets_t m_tickets;

	const int m_num_challenges;

public:
	ParcelTickets(NetGate::Server* server, const QString& osrm_server_address, int num_challenges);

public slots:
	// добавить тикет
	void add_ticket(const QUuid& ticket_id, const QString& hash, const QJsonDocument& json_doc);
	// удалить тикет
	void remove_ticket(const QUuid& ticket_id);

signals:
	void ticket_solved(const QString& hash, bool result, const QJsonDocument* json_result);

private slots:
	void slot_session_opened(NetGate::Session* session);
	void slot_session_closed(NetGate::Session* session);
	void slot_session_connection_created(NetGate::Session* session, NetGate::Connection* connection);
	//void slot_session_connection_destroyed(NetGate::Session* session);
	// слот закрытия задачи после обработки сессией
	void slot_session_task_closed(Task& task, bool result, const QJsonDocument* json_result);
	// слот окончания запроса матрицы дистанций
	void on_request_matrix_distances_finished(bool success, const QUuid& ticket_id, const SMatrixDistances& matrix);

private:
	//bool insert_to_best_session(Ticket* ticket);
	//SessionTickets* find_best_session();
	// поставить тикет на решение
	bool start_solving_ticket(const STicket& ticket);
	// закрыть тикет
	void close_ticket(const Ticket& ticket, bool result, const QJsonDocument* json_result);
};