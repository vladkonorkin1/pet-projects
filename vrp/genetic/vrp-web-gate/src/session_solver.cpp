//=====================================================================================//
//   Author: open
//   Date:   14.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "session_solver.h"
#include "net_gate/net_gate_session.h"
#include "net_gate/net_gate_messages.h"
#include "net_gate/net_gate_connection.h"
#include <QJsonObject>
#include <QJsonValue>

SessionSolver::SessionSolver(NetGate::Session* session)
: m_session(session)
{
}
void SessionSolver::add_task(Ticket& ticket, int random_seed)
{
	QJsonDocument json_doc = ticket.json_doc;
	QJsonObject json_doc_obj = json_doc.object();
	QJsonObject json_settings = json_doc_obj["settings"].toObject();
	json_settings["random_seed"] = random_seed;
	json_doc_obj["settings"] = json_settings;
	json_doc.setObject(json_doc_obj);
	//auto test = json_doc.toJson();

	STask task(new Task(QUuid::createUuid(), ticket, random_seed, json_doc, m_session->id(), m_session->name()));
	m_tasks[task->id] = task;
	ticket.add_task(task);
	send_open_task(*task);
}
void SessionSolver::send_open_task(const Task& task)
{
	m_session->send_message(NetGate::ServerMessageOpenTask(task.id, task.json_doc, task.ticket.vrp_data.matrix_distances));
}
// удалить задачу
void SessionSolver::remove_task(const Task& task)
{
	close_task(task.id, false, nullptr);
}
// отправляем все поставленные задачи
void SessionSolver::send_all_tasks()
{
	for (const STask& task : m_tasks)
		send_open_task(*task);
}
// запрос отчёта по выполненым задачам
void SessionSolver::send_request_report_solved_tasks()
{
	m_session->send_message(NetGate::ServerMessageReportSolvedTasks());
}
// обновление соединения
void SessionSolver::update_connection(NetGate::Connection* connection)
{
	if (connection)
	{
		connect(connection, SIGNAL(task_solved(const QUuid&, bool, const QJsonDocument&)), SLOT(slot_task_solved(const QUuid&, bool, const QJsonDocument&)));
		//connect(connection, SIGNAL(ticket_closed_response(const QUuid&)), SLOT(slot_ticket_closed_response(const QUuid&)));
	}
}
// задача решёна
void SessionSolver::slot_task_solved(const QUuid& task_id, bool result, const QJsonDocument& json_result)
{
	close_task(task_id, result, &json_result);
}
void SessionSolver::close_task(const QUuid& task_id, bool result, const QJsonDocument* json_result)
{
	// отправляем сообщение о закрытии тикета
	m_session->send_message(NetGate::ServerMessageCloseTask(task_id));

	tasks_t::iterator it = m_tasks.find(task_id);
	if (m_tasks.end() != it)
	{
		Task& task = **it;
		//task.set_datetime_finish();
		task.ticket.remove_task(task.id);
		emit task_closed(task, result, json_result);
		m_tasks.erase(it);
	}
}
void SessionSolver::remove_all_tasks()
{
	for (const STask& task : m_tasks)
		emit task_closed(*task, false, nullptr);
	m_tasks.clear();
}