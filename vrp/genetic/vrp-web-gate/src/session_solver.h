//=====================================================================================//
//   Author: open
//   Date:   14.09.2018
//=====================================================================================//
#pragma once
#include "ticket.h"

namespace NetGate
{
	class Session;
	class Connection;
}

class SessionSolver : public QObject
{
	Q_OBJECT
private:
	NetGate::Session* m_session;
	using tasks_t = QMap<QUuid, STask>;
	tasks_t m_tasks;

public:
	SessionSolver(NetGate::Session* session);
	// кол-во задач на исполнении
	size_t task_size() const { return m_tasks.size(); }
	// добавить задачу
	void add_task(Ticket& ticket, int random_seed);
	// удалить задачу
	void remove_task(const Task& task);
	// удалить все задачи
	void remove_all_tasks();
	// отправляем все поставленные задачи
	void send_all_tasks();
	// запрос отчёта по выполненым задачам
	void send_request_report_solved_tasks();
	// обновление соединения
	void update_connection(NetGate::Connection* connection);
	// получить id-ceссии
	//const QUuid& session_id() const;

signals:
	void task_closed(Task& task, bool result, const QJsonDocument* json_result);

private slots:
	// задача решёна
	void slot_task_solved(const QUuid& ticket_id, bool result, const QJsonDocument& json_result);

private:
	// закрыть задачу
	void close_task(const QUuid& task_id, bool result, const QJsonDocument* json_result);
	// отправить запуск задачи
	void send_open_task(const Task& task);
};

using USessionSolver = std::unique_ptr<SessionSolver>;