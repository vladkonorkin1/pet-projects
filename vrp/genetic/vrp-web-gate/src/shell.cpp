//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "shell.h"

Shell::Shell()
: m_parcel_tickets(&m_gate_server, m_config.value("web-gate/osrm-server-address", "127.0.0.1:5000"), m_config.value("web-gate/num-challenges", "4").toInt())
{
	m_web_server.listen(QHostAddress::Any, 19000);
	WebOptimalPlanTickets& web_optimal_plan_tickets = m_web_server.web_optimal_plan_tickets();
	connect(&web_optimal_plan_tickets, SIGNAL(request_created(const QUuid&, const QString&, const QJsonDocument&)), &m_parcel_tickets, SLOT(add_ticket(const QUuid&, const QString&, const QJsonDocument&)));
	connect(&web_optimal_plan_tickets, SIGNAL(request_closed(const QUuid&)), &m_parcel_tickets, SLOT(remove_ticket(const QUuid&)));
	connect(&m_parcel_tickets, SIGNAL(ticket_solved(const QString&, bool, const QJsonDocument*)), &web_optimal_plan_tickets, SLOT(send_result(const QString&, bool, const QJsonDocument*)));
	logs("start");
}