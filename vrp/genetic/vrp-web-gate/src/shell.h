//=====================================================================================//
//   Author: open
//   Date:   12.09.2018
//=====================================================================================//
#pragma once
#include "net_gate/net_gate_server.h"
#include "parcel_tickets.h"
#include "web/web_server.h"
#include "base/config.h"

class Shell : public QObject
{
	Q_OBJECT
private:
	Config m_config;
	Web::Server m_web_server;
	NetGate::Server m_gate_server;
	ParcelTickets m_parcel_tickets;

public:
	Shell();
};
