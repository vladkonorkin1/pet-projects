//=====================================================================================//
//   Author: open
//   Date:   07.10.2018
//=====================================================================================//
#include "base/precomp.h"
#include "ticket.h"
#include "vrp_algorithm/vrp_json.h"


Task::Task(const QUuid& id, Ticket& ticket, int random_seed, const QJsonDocument& json_doc, const QUuid& session_id, const QString& session_name)
: id(id)
, ticket(ticket)
, random_seed(random_seed)
, json_doc(json_doc)
, session_id(session_id)
, session_name(QString("%1-%2").arg(session_name).arg(random_seed))
, start_time(QDateTime::currentDateTimeUtc())
, fitness(std::numeric_limits<distance_t>::max())
, num_cycles(0)
{
}
void Task::set_finish(const QJsonDocument& _json_result)
{
	json_result = _json_result;
	if (!json_result.isNull())
	{
		distance_t f = Vrp::json_parse_plan_fitness(json_result);
		if (f != 0) fitness = f;

		num_cycles = Vrp::json_parse_plan_num_cycles(json_result);
	}

	quint64 msecs = start_time.msecsTo(QDateTime::currentDateTimeUtc());
	duration = QTime::fromMSecsSinceStartOfDay(msecs);
}


Ticket::Ticket(const QUuid& id, const QString& hash, const QJsonDocument& json_doc)
: id(id)
, hash(hash)
, json_doc(json_doc)
, num_finished(0)
, start_time(QDateTime::currentDateTimeUtc())
{
	Vrp::json_parse_data(json_doc, vrp_data);
}
void Ticket::add_result(const QUuid& task_id, const QJsonDocument& json_result)
{
	tasks_t::iterator it = m_tasks.find(task_id);
	if (m_tasks.end() != it)
	{
		(*it)->set_finish(json_result);
	}
}
QString Ticket::log_task(const Task& task) const
{
	QString duration = task.duration.toString("hh:mm:ss");
	if (!task.json_result.isNull())
		return QString("[%1] %2:  %3  %4").arg(duration).arg(task.session_name).arg(task.fitness, 0, 'f', 0).arg(task.num_cycles);
	return QString("[%1] %2:  fail").arg(duration).arg(task.session_name);
}
void Ticket::log_results() const
{
	//logs(QString("result: %1").arg(id.toString()));
	//for (const STask& task : m_tasks)
//		log_task(*task);
}
const Task* Ticket::find_best_result() const
{
	//const QJsonDocument* best_json = nullptr;
	const Task* best_task = nullptr;
	distance_t best_fitness = std::numeric_limits<distance_t>::max();
	for (const STask& task : m_tasks)
	{
		if (!task->json_result.isNull())
		{
			if (task->fitness < best_fitness)
			{
				best_fitness = task->fitness;
				best_task = task.get();
				//best_json = &json_doc;
			}
		}
	}
	return best_task;
}
void Ticket::remove_task(const QUuid& task_id)
{
	tasks_t::iterator it = m_tasks.find(task_id);
	if (m_tasks.end() != it)
	{
		//m_tasks.erase(it);
	}
}
void Ticket::add_task(const STask& task)
{
	m_tasks[task->id] = task;
}
const Ticket::tasks_t& Ticket::tasks() const
{
	return m_tasks;
}
QString Ticket::duration() const
{
	quint64 msecs = start_time.msecsTo(QDateTime::currentDateTimeUtc());
	return QTime::fromMSecsSinceStartOfDay(msecs).toString("hh:mm:ss");
}