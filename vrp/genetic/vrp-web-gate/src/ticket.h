//=====================================================================================//
//   Author: open
//   Date:   07.10.2018
//=====================================================================================//
#pragma once
#include "vrp_algorithm/vrp_data.h"
#include "base/matrix_distances.h"
#include <QJsonDocument>

struct Ticket;

// ������ �� ������� (�������� ��������� ����� � ������ random_seed)
struct Task
{
	QUuid id;
	Ticket& ticket;
	int random_seed;
	QJsonDocument json_doc;
	//bool enable;
	QUuid session_id;
	QString session_name;
	QDateTime start_time;
	QTime duration;
	int num_cycles;
	QJsonDocument json_result;
	distance_t fitness;

	Task(const QUuid& id, Ticket& ticket, int random_seed, const QJsonDocument& json_doc, const QUuid& session_id, const QString& session_name);
	void set_finish(const QJsonDocument& _json_result);
};
using STask = std::shared_ptr<Task>;

// ������ �� �������
struct Ticket
{
public:
	QUuid id;
	QString hash;
	QJsonDocument json_doc;
	Vrp::Data vrp_data;
	int num_finished;
	QDateTime start_time;

	using tasks_t = QMap<QUuid, STask>;

private:
	tasks_t m_tasks;
	//std::vector<QJsonDocument> m_results;

public:
	Ticket(const QUuid& id, const QString& hash, const QJsonDocument& json_doc);
	void add_result(const QUuid& task_id, const QJsonDocument& json_result);
	//const QJsonDocument* find_best_result() const;
	const Task* find_best_result() const;
	void remove_task(const QUuid& task_id);
	void add_task(const STask& task);
	const tasks_t& tasks() const;
	void log_results() const;
	QString duration() const;
	QString log_task(const Task& task) const;
};
using STicket = std::shared_ptr<Ticket>;