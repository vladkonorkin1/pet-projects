//=====================================================================================//
//   Author: open
//   Date:   19.09.2018
//=====================================================================================//
#include "base/precomp.h"
#include "web_optimal_plan_tickets.h"
#include <HttpServerRequest>
#include <HttpServerResponse>
#include <QCryptographicHash>
#include <QAbstractSocket>
#include <QJsonObject>

WebOptimalPlanTickets::WebOptimalPlanTickets()
{
	QJsonObject json_obj;
	json_obj["result"] = "error";
	m_bad_answer.setObject(json_obj);
}
// обработчик сетевого запроса
bool WebOptimalPlanTickets::handleRequest(Tufao::HttpServerRequest& request, Tufao::HttpServerResponse& response)
{
	if (request.method() == "POST")
	{
		add_ticket(request.readBody(), TicketRequest{ request, response });
		return true;
	}
	return false;
}
// отправить результат
void WebOptimalPlanTickets::send_result(const QString& hash, bool result, const QJsonDocument* json_doc)
{
	tickets_t::iterator it = m_tickets.find(hash);
	if (m_tickets.end() != it)
	{
		ticket_requests_t copy_ticket_requests = it->second.ticket_requests;
		for (auto& pair : copy_ticket_requests)
		{
			build_response(pair.second.response, (result && json_doc) ? *json_doc : m_bad_answer);
			pair.second.request.socket().close();
		}
	}
}
// собрать ответ
void WebOptimalPlanTickets::build_response(Tufao::HttpServerResponse& response, const QJsonDocument& json_doc)
{
	response.writeHead(Tufao::HttpResponseStatus::OK);
	response.end(json_doc.toJson(QJsonDocument::Compact));
}
// добавить тикет
void WebOptimalPlanTickets::add_ticket(const QByteArray& buffer, const TicketRequest& ticket_request)
{
	QString hash = QCryptographicHash::hash(buffer, QCryptographicHash::Sha1).toHex();

	tickets_t::iterator it = m_tickets.find(hash);
	if (m_tickets.end() == it)
		it = m_tickets.insert(std::make_pair(hash, Ticket{ QUuid::createUuid(), ticket_requests_t() })).first;

	it->second.ticket_requests.insert(std::make_pair(&ticket_request.request, ticket_request));
	connect(&ticket_request.request.socket(), &QAbstractSocket::disconnected, [this, hash, request = &ticket_request.request]() { slot_request_closed(hash, request); });

	if (it->second.ticket_requests.size() == 1)
		emit request_created(it->second.id, hash, QJsonDocument::fromJson(buffer));
}
// слот закрытия сокета
void WebOptimalPlanTickets::slot_request_closed(const QString& hash, const Tufao::HttpServerRequest* request)
{
	tickets_t::iterator it = m_tickets.find(hash);
	if (m_tickets.end() != it)
	{
		ticket_requests_t& ticket_requests = it->second.ticket_requests;
		ticket_requests_t::iterator it_request = ticket_requests.find(request);
		if (ticket_requests.end() != it_request)
			ticket_requests.erase(it_request);

		if (it->second.ticket_requests.empty())
		{
			emit request_closed(it->second.id);
			m_tickets.erase(it);
		}
	}
}