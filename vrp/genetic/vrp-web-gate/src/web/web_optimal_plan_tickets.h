//=====================================================================================//
//   Author: open
//   Date:   19.09.2018
//=====================================================================================//
#pragma once
#include <AbstractHttpServerRequestHandler>
#include <QJsonDocument>

class WebOptimalPlanTickets : public QObject, public Tufao::AbstractHttpServerRequestHandler
{
	Q_OBJECT
private:
	struct TicketRequest
	{
		Tufao::HttpServerRequest& request;
		Tufao::HttpServerResponse& response;
	};
	using ticket_requests_t = std::map<const Tufao::HttpServerRequest*, TicketRequest>;

	struct Ticket
	{
		QUuid id;
		ticket_requests_t ticket_requests;
	};
	using tickets_t = std::map<QString, Ticket>;
	tickets_t m_tickets;

	QJsonDocument m_bad_answer;

public:
	WebOptimalPlanTickets();

public slots:
	// отправить результат
	void send_result(const QString& hash, bool result, const QJsonDocument* json_doc);
	// обработчик сетевого запроса
	bool handleRequest(Tufao::HttpServerRequest& request, Tufao::HttpServerResponse& response) override;

signals:
	// новый запрос
	void request_created(const QUuid& ticket_id, const QString& hash, const QJsonDocument& json_doc);
	// запрос закрыт
	void request_closed(const QUuid& ticket_id);

private:
	// слот закрытия сокета
	void slot_request_closed(const QString& hash, const Tufao::HttpServerRequest* request);

private:
	// добавить тикет
	void add_ticket(const QByteArray& buffer, const TicketRequest& ticket_request);
	// собрать ответ
	void build_response(Tufao::HttpServerResponse& response, const QJsonDocument& json_doc);
};