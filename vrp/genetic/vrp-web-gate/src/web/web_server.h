//=====================================================================================//
//   Author: open
//   Date:   15.09.2018
//=====================================================================================//
#pragma once
#include <HttpServerRequestRouter>
#include <QTcpServer>
//#include <HttpServer>
#include "web/web_optimal_plan_tickets.h"

namespace Web
{

class Server : public QTcpServer
{
	Q_OBJECT
private:
	WebOptimalPlanTickets m_web_optimal_plan_tickets;
	Tufao::HttpServerRequestRouter m_router;

public:
	Server(QObject* parent = nullptr);
	//bool listen(const QHostAddress& address = QHostAddress::Any, quint16 port = 0);

	WebOptimalPlanTickets& web_optimal_plan_tickets();

private slots:
	void slot_request_ready();
	void slot_request_downloaded(Tufao::HttpServerRequest* request, Tufao::HttpServerResponse* response);

protected:
	virtual void incomingConnection(qintptr socket_descriptor);

private:
	void handleConnection(QAbstractSocket* socket);
};

}
