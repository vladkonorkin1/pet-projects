TARGET = vrp-web-gate
CONFIG -= flat
TEMPLATE = app

win32 {
    release: DESTDIR = tmp/release
    debug:   DESTDIR = tmp/debug
    OBJECTS_DIR = $$DESTDIR/.obj
    MOC_DIR = $$DESTDIR/.moc
    RCC_DIR = $$DESTDIR/.qrc
    UI_DIR = $$DESTDIR/.ui
}
unix {
    DESTDIR = .
    OBJECTS_DIR = $$DESTDIR/tmp/.obj
    MOC_DIR = $$DESTDIR/tmp/.moc
    RCC_DIR = $$DESTDIR/tmp/.qrc
    UI_DIR = $$DESTDIR/tmp/.ui
}

QT -= gui
QT += network sql
CONFIG += console

CONFIG += precompile_header
PRECOMPILED_HEADER = ../shared/base/precomp.h

INCLUDEPATH += src
INCLUDEPATH += ../shared
RESOURCES += ../shared/osrm/osrm_resources.qrc

win32 {
	Release:QMAKE_POST_LINK = copy tmp\release\*.exe bin\release
	QMAKE_CXXFLAGS += /MP

	INCLUDEPATH += "../../../../../external/x86/tufao/include/"
	INCLUDEPATH += "../../../../../external/x86/tufao/src/"

	CONFIG(debug, debug|release) {
		QMAKE_LIBDIR += "../../../../../external/x86/tufao/build/src/Debug/"
		QMAKE_LIBS += tufao1d.lib
	} else {
		QMAKE_LIBDIR += "../../../../../external/x86/tufao/build/src/Release/"
		QMAKE_LIBS += tufao1.lib
	}
	
	INCLUDEPATH += ../../../../../external/boost_1_68_0
}
unix {
	INCLUDEPATH += /usr/local/include/tufao-1/Tufao
	LIBS += -ltufao1
	
	INCLUDEPATH += /usr/local/include/boost
	LIBS += -lboost_random
	
	#QMAKE_CXXFLAGS += -rpath='.'
}

HEADERS =  	../shared/base/precomp.h 								\
			../shared/base/timer.h 									\
			../shared/base/log.h 									\
			../shared/base/thread_task.h							\
			../shared/base/thread_task_manager.h					\
			../shared/base/bin_stream.h								\
			../shared/base/dynamic_matrix.h							\
			../shared/base/matrix_distances.h						\
			../shared/base/network_object.h							\
			../shared/base/network_manager.h						\
			../shared/base/config.h									\
			../shared/net/net_base_connection.h						\
			../shared/net/net_packet.h								\
			../shared/net/net_packet_reader.h						\
			../shared/net/net_message_dispatcher.h					\
			../shared/net/net_message.h								\
			../shared/net_gate/net_gate_handler_messages.h			\
			../shared/net_gate/net_gate_messages.h					\
			../shared/net_gate/net_gate_types.h						\
			../shared/vrp_algorithm/vrp_data.h						\
			../shared/vrp_algorithm/vrp_json.h						\
			../shared/osrm/osrm_route.h								\
			../shared/osrm/net_request_osrm_distance.h				\
			../shared/osrm/net_request_osrm_table_distances.h		\
			../shared/osrm/net_request_osrm_route.h					\
			../shared/osrm/osrm_manager.h							\
			../shared/osrm/osrm_database.h							\
			../shared/osrm/osrm_request_matrix_distances.h			\
			../shared/osrm/osrm_request_routes.h					\
			src/net_gate/net_gate_server.h							\
			src/net_gate/net_gate_connection.h						\
			src/net_gate/net_gate_session.h							\
			src/web/web_server.h									\
			src/web/web_optimal_plan_tickets.h						\
			src/shell.h												\
			src/ticket.h											\
			src/parcel_tickets.h									\
			src/session_solver.h									\

SOURCES = 	../shared/base/precomp.cpp 								\
			../shared/base/timer.cpp 								\
			../shared/base/log.cpp 									\
			../shared/base/thread_task_manager.cpp					\
			../shared/base/matrix_distances.cpp						\
			../shared/base/network_object.cpp						\
			../shared/base/network_manager.cpp						\
			../shared/base/config.cpp								\
			../shared/net/net_base_connection.cpp					\
			../shared/net/net_packet.cpp							\
			../shared/net/net_packet_reader.cpp						\
			../shared/net_gate/net_gate_messages.cpp				\
			../shared/vrp_algorithm/vrp_data.cpp					\
			../shared/vrp_algorithm/vrp_json.cpp					\
			../shared/osrm/net_request_osrm_distance.cpp			\
			../shared/osrm/net_request_osrm_table_distances.cpp		\
			../shared/osrm/net_request_osrm_route.cpp				\
			../shared/osrm/osrm_manager.cpp							\
			../shared/osrm/osrm_database.cpp						\
			../shared/osrm/osrm_request_matrix_distances.cpp		\
			../shared/osrm/osrm_request_routes.cpp					\
			src/net_gate/net_gate_server.cpp						\
			src/net_gate/net_gate_connection.cpp					\
			src/net_gate/net_gate_session.cpp						\
			src/web/web_server.cpp									\
			src/web/web_optimal_plan_tickets.cpp					\
			src/shell.cpp											\
			src/main.cpp											\
			src/ticket.cpp											\
			src/parcel_tickets.cpp									\
			src/session_solver.cpp									\
